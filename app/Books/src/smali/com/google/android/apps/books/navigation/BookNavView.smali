.class public Lcom/google/android/apps/books/navigation/BookNavView;
.super Landroid/widget/FrameLayout;
.source "BookNavView.java"


# instance fields
.field private final mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;"
        }
    .end annotation
.end field

.field private mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

.field private mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

.field private final mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

.field private mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

.field private mChapterTitle:Landroid/widget/TextView;

.field private final mChildPosition:[I

.field private mCurrentPage:Landroid/widget/TextView;

.field private mFadeViews:[Landroid/view/View;

.field private mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

.field private final mFullScreenSpreadSize:Landroid/graphics/Point;

.field private final mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

.field private mGrabbedSpreadIndex:I

.field private mLastAnnouncedChapter:Lcom/google/android/apps/books/model/Chapter;

.field private mLastAnnouncedPage:Lcom/google/android/apps/books/model/Page;

.field private mLastAnnouncedPositionMillis:J

.field private mLastChapterIndex:I

.field private mLastPageIndex:I

.field private final mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

.field private mNumericBookPages:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation
.end field

.field private mOnlyFallbackHandle:Z

.field private mPageCount:Landroid/widget/TextView;

.field private mPageCountText:Ljava/lang/String;

.field private mPageGrid:Landroid/view/View;

.field private mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

.field private mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

.field private final mPagePointF:Landroid/graphics/PointF;

.field private final mPageShadowPadding:Landroid/graphics/Rect;

.field private final mParentPosition:[I

.field private mPinchEndY:I

.field private final mPinchVelocityTracker:Landroid/view/VelocityTracker;

.field private mPinchedToFull:Z

.field private mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private mScaleEventTime:J

.field private mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

.field private final mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

.field private mScrollStartedByScrubber:Z

.field private final mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

.field private mSkimEnterSpreadId:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mSnapAnimator:Landroid/animation/Animator;

.field private mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

.field private final mViewPointF:Landroid/graphics/PointF;

.field private mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    const/4 v0, -0x1

    .line 633
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 126
    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastPageIndex:I

    .line 127
    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastChapterIndex:I

    .line 138
    new-instance v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;-><init>(ZLcom/google/android/apps/books/util/BitmapCache;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 140
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageShadowPadding:Landroid/graphics/Rect;

    .line 165
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$1;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 174
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$2;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 197
    new-instance v0, Lcom/google/android/apps/books/util/MultiViewAnimator;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

    .line 198
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    .line 199
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    .line 200
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;

    .line 201
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    .line 204
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;

    .line 206
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$3;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    .line 461
    new-instance v0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .line 607
    new-instance v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    new-instance v1, Lcom/google/android/apps/books/navigation/BookNavView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/navigation/BookNavView$6;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    .line 1070
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChildPosition:[I

    .line 1071
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mParentPosition:[I

    .line 634
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x2

    const/4 v0, -0x1

    .line 637
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 126
    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastPageIndex:I

    .line 127
    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastChapterIndex:I

    .line 138
    new-instance v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;-><init>(ZLcom/google/android/apps/books/util/BitmapCache;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 140
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageShadowPadding:Landroid/graphics/Rect;

    .line 165
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$1;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 174
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$2;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 197
    new-instance v0, Lcom/google/android/apps/books/util/MultiViewAnimator;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

    .line 198
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    .line 199
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    .line 200
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;

    .line 201
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    .line 204
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;

    .line 206
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$3;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    .line 461
    new-instance v0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .line 607
    new-instance v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    new-instance v1, Lcom/google/android/apps/books/navigation/BookNavView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/navigation/BookNavView$6;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    .line 1070
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChildPosition:[I

    .line 1071
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mParentPosition:[I

    .line 638
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v0, -0x1

    .line 641
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 126
    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastPageIndex:I

    .line 127
    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastChapterIndex:I

    .line 138
    new-instance v0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;-><init>(ZLcom/google/android/apps/books/util/BitmapCache;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 140
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageShadowPadding:Landroid/graphics/Rect;

    .line 165
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$1;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 174
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$2;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 197
    new-instance v0, Lcom/google/android/apps/books/util/MultiViewAnimator;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

    .line 198
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    .line 199
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    .line 200
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;

    .line 201
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    .line 204
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;

    .line 206
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$3;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    .line 461
    new-instance v0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGestureListener:Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .line 607
    new-instance v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    new-instance v1, Lcom/google/android/apps/books/navigation/BookNavView$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/navigation/BookNavView$6;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    .line 1070
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChildPosition:[I

    .line 1071
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mParentPosition:[I

    .line 642
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/BookNavView;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScaleEventTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/navigation/BookNavView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # J

    .prologue
    .line 79
    iput-wide p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScaleEventTime:J

    return-wide p1
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/navigation/BookNavView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/books/navigation/BookNavView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/navigation/BookNavView;Lcom/google/android/apps/books/render/PageHandle;)Lcom/google/android/apps/books/render/PageHandle;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/PageHandle;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookScrollController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/render/Renderer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/BitmapCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookGridView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/android/apps/books/navigation/BookNavView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchEndY:I

    return p1
.end method

.method static synthetic access$1902(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchedToFull:Z

    return p1
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->animateToGrid()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mOnlyFallbackHandle:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/navigation/BookNavView;)[Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/MultiViewAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/animation/Animator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/KeyboardAwareEditText;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->snapback()V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/books/navigation/BookNavView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/books/navigation/BookNavView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isScrolling()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->hidePageNumberEditor()V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollStartedByScrubber:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->showPageNumberEditor()V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/books/navigation/BookNavView;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/books/navigation/BookNavView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;
    .param p1, "x1"    # Z

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/BookNavView;->setPageNumberValid(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrubberUpdater:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->adjustScroll()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->syncScrubberWithFling()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/view/VelocityTracker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookNavView;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    return-object v0
.end method

.method private adjustScroll()V
    .locals 2

    .prologue
    .line 568
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mOnlyFallbackHandle:Z

    if-nez v1, :cond_2

    .line 573
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterSpreadIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->getValidPageHandle(I)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v0

    .line 575
    .local v0, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    if-eqz v0, :cond_2

    .line 576
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/navigation/BookNavView;->updatePageInfo(Lcom/google/android/apps/books/render/PageHandle;)Z

    goto :goto_0

    .line 581
    .end local v0    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    if-eqz v1, :cond_0

    .line 582
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->updatePageInfo(Lcom/google/android/apps/books/render/PageHandle;)Z

    goto :goto_0
.end method

.method private animateToGrid()V
    .locals 15

    .prologue
    const/4 v10, 0x4

    const/4 v14, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 374
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    invoke-virtual {v7, v11}, Landroid/view/View;->setAlpha(F)V

    .line 375
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

    invoke-virtual {v7}, Lcom/google/android/apps/books/util/MultiViewAnimator;->updateInitialStates()V

    .line 377
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v8, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onBeganTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;

    .local v1, "arr$":[Landroid/view/View;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v6, v1, v3

    .line 380
    .local v6, "view":Landroid/view/View;
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 379
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 383
    .end local v6    # "view":Landroid/view/View;
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget v8, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    invoke-virtual {v7, v8}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getView(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .line 385
    .local v5, "srcView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    invoke-virtual {v7}, Lcom/google/android/apps/books/navigation/BookGridView;->getCurrentSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v2

    .line 386
    .local v2, "dstView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    const v7, 0x7fffffff

    iput v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    .line 388
    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageBorder()Landroid/view/View;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    invoke-virtual {v5}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageBorder()Landroid/view/View;

    move-result-object v9

    invoke-static {v7, v8, v9, v12}, Lcom/google/android/apps/books/util/ViewUtils;->placeOverView(Landroid/view/View;Landroid/view/View;Landroid/view/View;Z)V

    .line 389
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setVisibility(I)V

    .line 391
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 392
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    .line 394
    new-array v7, v10, [Landroid/animation/Animator;

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    const-string v9, "scaleX"

    new-array v10, v12, [F

    aput v11, v10, v13

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v7, v13

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    const-string v9, "scaleY"

    new-array v10, v12, [F

    aput v11, v10, v13

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    aput-object v8, v7, v12

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    const-string v10, "translationX"

    new-array v11, v12, [F

    aput v14, v11, v13

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    const-string v10, "translationY"

    new-array v11, v12, [F

    aput v14, v11, v13

    invoke-static {v9, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 398
    new-instance v7, Lcom/google/android/apps/books/navigation/BookNavView$4;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/navigation/BookNavView$4;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 405
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v8, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onBeganTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 407
    const v7, 0x7fffffff

    iput v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    .line 409
    new-instance v7, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 410
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x10e0001

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v0, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 412
    new-array v7, v12, [Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    aput-object v8, v7, v13

    invoke-static {v0, v7}, Lcom/google/android/apps/books/util/ViewUtils;->useLayerWhenAnimating(Landroid/animation/Animator;[Landroid/view/View;)V

    .line 413
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->startAnimator(Landroid/animation/Animator;)V

    .line 414
    return-void
.end method

.method private announceCurrentPosition(Lcom/google/android/apps/books/render/PageHandle;)V
    .locals 10
    .param p1, "pageHandle"    # Lcom/google/android/apps/books/render/PageHandle;

    .prologue
    .line 490
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstChapter()Lcom/google/android/apps/books/model/Chapter;

    move-result-object v0

    .line 491
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPage()Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    .line 493
    .local v1, "page":Lcom/google/android/apps/books/model/Page;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 501
    .local v2, "now":J
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedChapter:Lcom/google/android/apps/books/model/Chapter;

    if-ne v0, v5, :cond_2

    iget-wide v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedPositionMillis:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x7530

    cmp-long v5, v6, v8

    if-lez v5, :cond_3

    .line 503
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCountText:Ljava/lang/String;

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/books/util/AccessibilityUtils;->getPositionDescription(Landroid/content/res/Resources;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 513
    .local v4, "position":Ljava/lang/String;
    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedPage:Lcom/google/android/apps/books/model/Page;

    .line 514
    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedChapter:Lcom/google/android/apps/books/model/Chapter;

    .line 516
    if-eqz v4, :cond_0

    .line 517
    iput-wide v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedPositionMillis:J

    .line 518
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p0, v4}, Lcom/google/android/apps/books/util/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 505
    .end local v4    # "position":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedPage:Lcom/google/android/apps/books/model/Page;

    if-ne v1, v5, :cond_4

    iget-wide v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedPositionMillis:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x3e8

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    .line 507
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0126

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "position":Ljava/lang/String;
    goto :goto_1

    .line 510
    .end local v4    # "position":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x0

    .restart local v4    # "position":Ljava/lang/String;
    goto :goto_1
.end method

.method private getCenterSpreadIndex()I
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getCenterPosition()I

    move-result v0

    return v0
.end method

.method private getValidPageHandle(I)Lcom/google/android/apps/books/render/PageHandle;
    .locals 5
    .param p1, "spreadPosition"    # I

    .prologue
    .line 555
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/navigation/BookScrollController;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v2

    .line 556
    .local v2, "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 557
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 558
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/PageHandle;

    .line 559
    .local v1, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 563
    .end local v1    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :goto_1
    return-object v1

    .line 557
    .restart local v1    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 563
    .end local v1    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private hidePageNumberEditor()V
    .locals 2

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->requestFocus()Z

    .line 741
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setVisibility(I)V

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 743
    return-void
.end method

.method private isActive()Z
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isScrolling()Z
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getScrollState()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setPageNumberValid(Z)V
    .locals 4
    .param p1, "isValid"    # Z

    .prologue
    .line 715
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_1

    const v1, 0x7f01019f

    :goto_0
    invoke-static {v2, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getThemeColor(Landroid/content/Context;I)I

    move-result v0

    .line 717
    .local v0, "color":I
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setTextColor(I)V

    .line 722
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 723
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 725
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, Lcom/google/android/ublib/view/ViewCompat;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 727
    :cond_0
    return-void

    .line 715
    .end local v0    # "color":I
    :cond_1
    const v1, 0x7f0101a4

    goto :goto_0
.end method

.method private setupPageEditor()V
    .locals 3

    .prologue
    .line 746
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 748
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    .line 749
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 750
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditBackground:Landroid/graphics/drawable/Drawable;

    .line 753
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/navigation/BookNavView$10;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookNavView$10;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    .line 760
    .local v0, "mEditPageListener":Landroid/view/View$OnClickListener;
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 761
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCount:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 763
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    new-instance v2, Lcom/google/android/apps/books/navigation/BookNavView$11;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/navigation/BookNavView$11;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 770
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    new-instance v2, Lcom/google/android/apps/books/navigation/BookNavView$12;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/navigation/BookNavView$12;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 789
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    new-instance v2, Lcom/google/android/apps/books/navigation/BookNavView$13;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/navigation/BookNavView$13;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 814
    return-void
.end method

.method private showPageNumberEditor()V
    .locals 2

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->initNumericPageMap()V

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setText(Ljava/lang/CharSequence;)V

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 734
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/navigation/BookNavView;->setPageNumberValid(Z)V

    .line 735
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setVisibility(I)V

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->requestFocus()Z

    .line 737
    return-void
.end method

.method private snapback()V
    .locals 4

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;->getSnapbackAnimator()Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    new-instance v1, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v1}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/apps/books/navigation/BookNavView$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/navigation/BookNavView$5;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 456
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->startAnimator(Landroid/animation/Animator;)V

    .line 458
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I

    .line 459
    return-void
.end method

.method private syncScrubberWithFling()V
    .locals 4

    .prologue
    .line 588
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getScrollState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollStartedByScrubber:Z

    if-eqz v2, :cond_1

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 595
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getScrollProgress()F

    move-result v1

    .line 597
    .local v1, "progress":F
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    .line 600
    .local v0, "endHandle":Lcom/google/android/apps/books/render/PageHandle;
    :goto_1
    if-eqz v0, :cond_0

    .line 601
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V

    goto :goto_0

    .line 597
    .end local v0    # "endHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getFinalCenterPosition()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->getValidPageHandle(I)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v0

    goto :goto_1
.end method

.method private updatePageInfo(Lcom/google/android/apps/books/render/PageHandle;)Z
    .locals 12
    .param p1, "handle"    # Lcom/google/android/apps/books/render/PageHandle;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 523
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v4

    .line 524
    .local v4, "pageIndex":I
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstChapterIndex()I

    move-result v2

    .line 526
    .local v2, "chapterIndex":I
    if-eq v4, v7, :cond_0

    if-ne v2, v7, :cond_2

    :cond_0
    move v5, v6

    .line 547
    :cond_1
    :goto_0
    return v5

    .line 530
    :cond_2
    iget v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastPageIndex:I

    if-ne v4, v7, :cond_3

    iget v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastChapterIndex:I

    if-eq v2, v7, :cond_1

    .line 534
    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Page;

    .line 535
    .local v3, "page":Lcom/google/android/apps/books/model/Page;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Chapter;

    .line 537
    .local v1, "chapter":Lcom/google/android/apps/books/model/Chapter;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 538
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 540
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0f0123

    new-array v10, v5, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v6

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 543
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f0124

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCountText:Ljava/lang/String;

    aput-object v6, v9, v5

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 545
    .local v0, "caption":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public announceCurrentPosition()Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 469
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v4

    if-nez v4, :cond_0

    move v4, v5

    .line 486
    :goto_0
    return v4

    .line 473
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterSpreadIndex()I

    move-result v3

    .line 474
    .local v3, "spreadPosition":I
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v2

    .line 477
    .local v2, "pageRangeLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPages(ILcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 478
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 479
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/navigation/PageViewContent;

    invoke-interface {v4}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageHandle()Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v1

    .line 480
    .local v1, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_1

    .line 481
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v4, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V

    .line 482
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/BookNavView;->announceCurrentPosition(Lcom/google/android/apps/books/render/PageHandle;)V

    .line 483
    const/4 v4, 0x1

    goto :goto_0

    .line 478
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .end local v1    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_2
    move v4, v5

    .line 486
    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-nez v0, :cond_0

    .line 921
    :goto_0
    return-void

    .line 911
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/BitmapCache;->clear()V

    .line 912
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->destroy()V

    .line 913
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookGridView;->clear()V

    .line 915
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 916
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    .line 917
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchedToFull:Z

    .line 918
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedChapter:Lcom/google/android/apps/books/model/Chapter;

    .line 919
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mLastAnnouncedPage:Lcom/google/android/apps/books/model/Page;

    .line 920
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->removeBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    goto :goto_0
.end method

.method public configureZoomToFullAnimator(Landroid/animation/Animator;)Z
    .locals 8
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    const/4 v4, 0x1

    .line 928
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x10e0000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 931
    .local v1, "shortDuration":I
    iget-boolean v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchedToFull:Z

    if-nez v5, :cond_0

    .line 932
    int-to-long v4, v1

    invoke-virtual {p1, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 933
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {p1, v4}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 934
    const/4 v4, 0x0

    .line 945
    :goto_0
    return v4

    .line 937
    :cond_0
    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {p1, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 938
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 940
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 941
    .local v3, "velocity":F
    iget v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPinchEndY:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v0, v5

    .line 943
    .local v0, "distance":F
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v5, v3}, Ljava/lang/Math;->max(FF)F

    move-result v5

    div-float v2, v0, v5

    .line 944
    .local v2, "time":F
    mul-int/lit8 v5, v1, 0x2

    int-to-float v5, v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    float-to-long v6, v5

    invoke-virtual {p1, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    goto :goto_0
.end method

.method public getCenterPageRenderings()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1186
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v5}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v5}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getScrollState()I

    move-result v5

    if-eqz v5, :cond_2

    .line 1188
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 1204
    :cond_1
    return-object v4

    .line 1191
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterSpreadIndex()I

    move-result v0

    .line 1192
    .local v0, "centerSpread":I
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v5}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v2

    .line 1193
    .local v2, "pageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPages(ILcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 1195
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1197
    .local v4, "pageRenderings":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 1198
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/navigation/PageViewContent;

    invoke-interface {v5}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageRendering()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v3

    .line 1199
    .local v3, "pageRendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v3, :cond_3

    .line 1200
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1197
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCurrentSpread()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1080
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-nez v3, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return-object v2

    .line 1086
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->inGridMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1087
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/BookGridView;->getSelectedSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v2

    .local v2, "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    goto :goto_0

    .line 1090
    .end local v2    # "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/BookScrollController;->getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    .line 1091
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1095
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getSelectedPosition()I

    move-result v1

    .line 1097
    .local v1, "position":I
    const v3, 0x7fffffff

    if-ne v1, v3, :cond_3

    .line 1098
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterSpreadIndex()I

    move-result v1

    .line 1101
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v3, v1}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getView(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .restart local v2    # "spreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    goto :goto_0
.end method

.method public getLabelAlphaAnimator(Z)Landroid/animation/AnimatorSet;
    .locals 12
    .param p1, "toShow"    # Z

    .prologue
    .line 1153
    if-eqz p1, :cond_0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1155
    .local v5, "endAlpha":F
    :goto_0
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1156
    .local v1, "animatorSet":Landroid/animation/AnimatorSet;
    const/4 v3, 0x0

    .line 1157
    .local v3, "builder":Landroid/animation/AnimatorSet$Builder;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;

    .local v2, "arr$":[Landroid/view/View;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v8, v2, v6

    .line 1158
    .local v8, "view":Landroid/view/View;
    const-string v9, "alpha"

    const/4 v10, 0x1

    new-array v10, v10, [F

    const/4 v11, 0x0

    aput v5, v10, v11

    invoke-static {v8, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1159
    .local v0, "animator":Landroid/animation/Animator;
    if-nez v3, :cond_1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    .line 1157
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1153
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v1    # "animatorSet":Landroid/animation/AnimatorSet;
    .end local v2    # "arr$":[Landroid/view/View;
    .end local v3    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v5    # "endAlpha":F
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "view":Landroid/view/View;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 1159
    .restart local v0    # "animator":Landroid/animation/Animator;
    .restart local v1    # "animatorSet":Landroid/animation/AnimatorSet;
    .restart local v2    # "arr$":[Landroid/view/View;
    .restart local v3    # "builder":Landroid/animation/AnimatorSet$Builder;
    .restart local v5    # "endAlpha":F
    .restart local v6    # "i$":I
    .restart local v7    # "len$":I
    .restart local v8    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    goto :goto_2

    .line 1161
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v8    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x10e0000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 1162
    .local v4, "duration":I
    int-to-long v10, v4

    invoke-virtual {v1, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v9

    new-instance v10, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v10}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1163
    return-object v1
.end method

.method public getSkimView()Lcom/google/android/ublib/widget/AbsWarpListView;
    .locals 1

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    return-object v0
.end method

.method public getStableSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1115
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-nez v4, :cond_1

    .line 1139
    :cond_0
    :goto_0
    return-object v3

    .line 1118
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/navigation/BookScrollController;->getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    .line 1119
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1122
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getScrollState()I

    move-result v4

    if-nez v4, :cond_0

    .line 1126
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v5}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getCenterPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    .line 1129
    .local v3, "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v4, v3, v5}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 1131
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_0

    .line 1132
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v4}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v2

    .line 1134
    .local v2, "normalizedId":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v2, :cond_2

    .line 1135
    iget-object v3, v2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    goto :goto_0

    .line 1131
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 2
    .param p1, "pageRendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 1060
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookScrollController;->getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    .line 1061
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    if-eqz v0, :cond_0

    .line 1062
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getPageLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 1064
    :cond_0
    return-void
.end method

.method public inGridMode()Z
    .locals 2

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method initNumericPageMap()V
    .locals 9

    .prologue
    .line 822
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;

    if-eqz v5, :cond_0

    .line 839
    :goto_0
    return-void

    .line 825
    :cond_0
    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;

    .line 827
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v3

    .line 828
    .local v3, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    const/4 v1, 0x0

    .line 829
    .local v1, "maxLength":I
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    .line 830
    .local v2, "page":Lcom/google/android/apps/books/model/Page;
    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 831
    .local v4, "title":Ljava/lang/String;
    const-string v5, "[0-9]+"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 834
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 835
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mNumericBookPages:Ljava/util/TreeMap;

    invoke-virtual {v5, v4, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 838
    .end local v2    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v4    # "title":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    const/4 v6, 0x1

    new-array v6, v6, [Landroid/text/InputFilter;

    const/4 v7, 0x0

    new-instance v8, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v8, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method public onBookmarksChanged()V
    .locals 2

    .prologue
    .line 1171
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-eqz v1, :cond_0

    .line 1172
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v0

    .line 1173
    .local v0, "skimPageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    if-eqz v0, :cond_0

    .line 1174
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->onBookmarksChanged()V

    .line 1178
    .end local v0    # "skimPageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookGridView;->onBookmarksChanged()V

    .line 1179
    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 646
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 647
    const v2, 0x7f0e00c4

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    .line 648
    const v2, 0x7f0e00c6

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCount:Landroid/widget/TextView;

    .line 649
    const v2, 0x7f0e00c5

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    .line 650
    const v2, 0x7f0e00c7

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    .line 652
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 653
    .local v0, "pageShadow":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageShadowPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 655
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    new-instance v3, Lcom/google/android/apps/books/navigation/BookNavView$7;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/navigation/BookNavView$7;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setKeyboardListener(Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;)V

    .line 662
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    new-instance v3, Lcom/google/android/apps/books/navigation/BookNavView$8;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/navigation/BookNavView$8;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 671
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    const-string v3, "9999"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 672
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 674
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 675
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 676
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 677
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 678
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 680
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 681
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 684
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 686
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->setTranslationX(F)V

    .line 689
    const v2, 0x7f0e00c8

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .line 690
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2, v5}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setNeedsScrollFilter(Z)V

    .line 691
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getScrollFriction()F

    move-result v3

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setScrollFriction(F)V

    .line 693
    const v2, 0x7f0e00c2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/navigation/BookGridView;

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    .line 694
    const v2, 0x7f0e00c3

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    .line 696
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCount:Landroid/widget/TextView;

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCurrentPage:Landroid/widget/TextView;

    aput-object v3, v2, v6

    iput-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;

    .line 698
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2, v6}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setSnapMode(I)V

    .line 699
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 701
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    new-instance v3, Lcom/google/android/apps/books/navigation/BookNavView$9;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/navigation/BookNavView$9;-><init>(Lcom/google/android/apps/books/navigation/BookNavView;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 711
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->setupPageEditor()V

    .line 712
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 950
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 952
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 953
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 954
    .local v2, "y":F
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getHeight()I

    move-result v0

    .line 956
    .local v0, "margin":I
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getLeft()I

    move-result v4

    sub-int/2addr v4, v0

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getRight()I

    move-result v4

    add-int/2addr v4, v0

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-gtz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getTop()I

    move-result v4

    sub-int/2addr v4, v0

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getBottom()I

    move-result v4

    add-int/2addr v4, v0

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-lez v4, :cond_2

    .line 960
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->hidePageNumberEditor()V

    .line 965
    .end local v0    # "margin":I
    .end local v1    # "x":F
    .end local v2    # "y":F
    :cond_1
    :goto_0
    return v3

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    invoke-virtual {v4}, Landroid/animation/Animator;->isRunning()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->isInProgress()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-le v4, v3, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    invoke-virtual {v4, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 1183
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    .line 972
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mMultiTouchDetector:Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepare(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    .locals 17
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p4, "centerSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 847
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/BookNavView;->clear()V

    .line 849
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 850
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 851
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 852
    new-instance v2, Lcom/google/android/apps/books/render/SpreadItems;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 853
    new-instance v2, Lcom/google/android/apps/books/render/SpreadItems;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadPages:Lcom/google/android/apps/books/render/SpreadItems;

    .line 854
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/navigation/BookNavView;->mSkimEnterSpreadId:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 856
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/BookNavView;->hidePageNumberEditor()V

    .line 858
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/model/Page;

    .line 860
    .local v10, "lastPage":Lcom/google/android/apps/books/model/Page;
    invoke-interface {v10}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCountText:Ljava/lang/String;

    .line 861
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCount:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCountText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 863
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    if-nez v2, :cond_0

    .line 865
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v15

    .line 866
    .local v15, "totalHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getWidth()I

    move-result v16

    .line 867
    .local v16, "totalWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getHeight()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageShadowPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageShadowPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v14, v2, v3

    .line 870
    .local v14, "skimHeight":I
    mul-int v2, v16, v14

    div-int v13, v2, v15

    .line 873
    .local v13, "pageWidth":I
    div-int/lit8 v2, v16, 0x4

    sub-int v3, v16, v13

    div-int/lit8 v3, v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v12

    .line 875
    .local v12, "margin":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 877
    .local v11, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput v12, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 878
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mChapterTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 880
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCount:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .end local v11    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 881
    .restart local v11    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput v12, v11, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 882
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageCount:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 885
    .end local v11    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "margin":I
    .end local v13    # "pageWidth":I
    .end local v14    # "skimHeight":I
    .end local v15    # "totalHeight":I
    .end local v16    # "totalWidth":I
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->weaklyAddBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 887
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setRTL(Z)V

    .line 888
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setVisibility(I)V

    .line 890
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->setLabelsVisible(Z)V

    .line 892
    new-instance v2, Lcom/google/android/apps/books/navigation/BookScrollController;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v6, p3

    move-object/from16 v8, p4

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/books/navigation/BookScrollController;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/ublib/widget/AbsWarpListView;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/render/SpreadIdentifier;Landroid/graphics/Point;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    .line 896
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setStasis(Z)V

    .line 898
    move-object/from16 v0, p4

    iget v2, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-nez v2, :cond_1

    .line 899
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    .line 903
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/BookNavView;->adjustScroll()V

    .line 904
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    .line 905
    return-void
.end method

.method public redraw()V
    .locals 1

    .prologue
    .line 1208
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1209
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->redraw()V

    .line 1210
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->onDecorationsChanged()V

    .line 1212
    :cond_0
    return-void
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->invalidateViews()V

    .line 1056
    return-void
.end method

.method public scrollPage(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 5
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 1033
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-nez v3, :cond_1

    .line 1052
    :cond_0
    :goto_0
    return-void

    .line 1036
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/BookScrollController;->getCurrentAdapter()Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-result-object v0

    .line 1037
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterSpreadIndex()I

    move-result v1

    .line 1040
    .local v1, "center":I
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v2, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 1043
    .local v2, "forward":Lcom/google/android/apps/books/util/ScreenDirection;
    :goto_1
    if-ne p1, v2, :cond_3

    .line 1044
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    .line 1045
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->flingToCenter(I)V

    goto :goto_0

    .line 1040
    .end local v2    # "forward":Lcom/google/android/apps/books/util/ScreenDirection;
    :cond_2
    sget-object v2, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    goto :goto_1

    .line 1048
    .restart local v2    # "forward":Lcom/google/android/apps/books/util/ScreenDirection;
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getNegativeCount()I

    move-result v3

    neg-int v3, v3

    if-le v1, v3, :cond_0

    .line 1049
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->flingToCenter(I)V

    goto :goto_0
.end method

.method public scrollToLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V
    .locals 9
    .param p1, "location"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "fromScrubber"    # Z

    .prologue
    const/4 v6, 0x0

    .line 981
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isActive()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1030
    :cond_0
    :goto_0
    return-void

    .line 985
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v7}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->isScrubbing()Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v5, 0x1

    .line 987
    .local v5, "toCommit":Z
    :goto_1
    const/4 v1, 0x0

    .line 989
    .local v1, "fallbackHandle":Lcom/google/android/apps/books/render/PageHandle;
    iget v7, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-nez v7, :cond_4

    .line 992
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v8, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/render/Renderer;->isPositionEnabled(Lcom/google/android/apps/books/common/Position;)Z
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_0

    .line 1005
    iget-object v7, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-static {v7, v6}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    .line 1006
    .local v4, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v6, v4}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v3

    .line 1007
    .local v3, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/navigation/BookNavView;->updatePageInfo(Lcom/google/android/apps/books/render/PageHandle;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1008
    move-object v1, v3

    .line 1022
    .end local v3    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v4    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v6, p1, v5}, Lcom/google/android/apps/books/navigation/BookScrollController;->scrollToLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    .line 1024
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/BookNavView;->isScrolling()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1027
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFallbackPageHandle:Lcom/google/android/apps/books/render/PageHandle;

    .line 1028
    iput-boolean p2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollStartedByScrubber:Z

    goto :goto_0

    .end local v1    # "fallbackHandle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v5    # "toCommit":Z
    :cond_3
    move v5, v6

    .line 985
    goto :goto_1

    .line 995
    .restart local v1    # "fallbackHandle":Lcom/google/android/apps/books/render/PageHandle;
    .restart local v5    # "toCommit":Z
    :catch_0
    move-exception v0

    .line 996
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v6, "BookNavView"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 997
    const-string v6, "BookNavView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to check if position is enabled "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1011
    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v6, p1, v7}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 1013
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    .local v2, "i":I
    :goto_3
    if-ltz v2, :cond_2

    .line 1014
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/render/PageHandle;

    .line 1015
    .restart local v3    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/navigation/BookNavView;->updatePageInfo(Lcom/google/android/apps/books/render/PageHandle;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1016
    move-object v1, v3

    .line 1017
    goto :goto_2

    .line 1013
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_3
.end method

.method public scrollToSkimEnterPosition()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1219
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-nez v3, :cond_1

    .line 1236
    :cond_0
    :goto_0
    return v2

    .line 1223
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v4}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getFinalCenterPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/navigation/BookScrollController;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 1226
    .local v0, "currentFinalCenter":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSkimEnterSpreadId:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-interface {v3, v0, v4}, Lcom/google/android/apps/books/render/Renderer;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I

    move-result v1

    .line 1229
    .local v1, "spreadDistance":I
    if-eqz v1, :cond_0

    .line 1232
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mSkimEnterSpreadId:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/books/navigation/BookNavView;->scrollToLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    .line 1233
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setLabelsVisible(Z)V
    .locals 5
    .param p1, "toShow"    # Z

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 1148
    .local v3, "view":Landroid/view/View;
    if-eqz p1, :cond_0

    const/high16 v4, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1147
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1148
    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 1150
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public showTOC()V
    .locals 13

    .prologue
    const/16 v12, 0x8

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getVisibility()I

    move-result v0

    if-eq v0, v12, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    if-nez v0, :cond_1

    .line 436
    :cond_0
    return-void

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v10

    .line 423
    .local v10, "pageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getCenterPosition()I

    move-result v0

    invoke-virtual {v10, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    .line 426
    .local v3, "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->onFinishedTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v6, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/navigation/BookGridView;->prepare(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Landroid/graphics/Point;)V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->setPaused(Z)V

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;

    invoke-virtual {v0, v12}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 433
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;

    .local v7, "arr$":[Landroid/view/View;
    array-length v9, v7

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v11, v7, v8

    .line 434
    .local v11, "view":Landroid/view/View;
    const/4 v0, 0x4

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 433
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method
