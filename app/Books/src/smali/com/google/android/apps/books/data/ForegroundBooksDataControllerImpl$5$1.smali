.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;

.field final synthetic val$progress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

.field final synthetic val$result:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;Lcom/google/android/apps/books/model/VolumeDownloadProgress;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->val$progress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    iput-object p3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->val$result:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 328
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$100(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;

    iget-object v3, v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->val$volumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->val$progress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->this$1:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # invokes: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$200(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 330
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;->val$result:Ljava/util/Map;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeDownloadProgress(Ljava/util/Map;)V

    goto :goto_0

    .line 332
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method
