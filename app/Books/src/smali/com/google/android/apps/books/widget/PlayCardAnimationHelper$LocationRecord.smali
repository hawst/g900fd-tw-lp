.class public Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;
.super Ljava/lang/Object;
.source "PlayCardAnimationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocationRecord"
.end annotation


# instance fields
.field public final location:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;Landroid/view/View;)V
    .locals 5
    .param p2, "child"    # Landroid/view/View;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iget-object v2, p1, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v2, p2}, Lcom/google/android/ublib/view/TranslationHelper;->getLeft(Landroid/view/View;)I

    move-result v0

    .line 202
    .local v0, "left":I
    iget-object v2, p1, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    invoke-virtual {v2, p2}, Lcom/google/android/ublib/view/TranslationHelper;->getTop(Landroid/view/View;)I

    move-result v1

    .line 203
    .local v1, "top":I
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$LocationRecord;->location:Landroid/graphics/Rect;

    .line 204
    return-void
.end method
