.class final Lcom/google/android/apps/books/data/SessionKeySubcontroller$7;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public process(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 247
    .local p2, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/SessionKeyId;->update(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/model/BooksDataStore;)V

    .line 248
    return-object p2
.end method
