.class public Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReadAlongController$DataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;,
        Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;,
        Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;

.field private final mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private mRequestId:I

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/widget/TtsLoadListener;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "listener"    # Lcom/google/android/apps/books/widget/TtsLoadListener;
    .param p4, "backgroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "uiExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mRequestId:I

    .line 63
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 65
    iput-object p3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;

    .line 66
    iput-object p4, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 67
    iput-object p5, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/widget/TtsLoadListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mUiExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public isPassageForbidden(I)Z
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFirstSegmentForPassageIndex(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v0

    .line 91
    .local v0, "segment":Lcom/google/android/apps/books/model/Segment;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 2
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mRequestId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mRequestId:I

    .line 73
    .local v0, "nextRequestId":I
    instance-of v1, p1, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    check-cast p1, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    .end local p1    # "request":Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->submit()V

    .line 80
    :goto_0
    return v0

    .line 78
    .restart local p1    # "request":Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->submit()V

    goto :goto_0
.end method

.method public shutDown()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;

    .line 103
    return-void
.end method
