.class final Lcom/google/android/apps/books/model/VolumeMetadataImpl$2;
.super Ljava/lang/Object;
.source "VolumeMetadataImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/VolumeMetadataImpl;->productionMyEbooksChecker(Landroid/content/ContentResolver;)Lcom/google/android/apps/books/model/VolumeMetadataImpl$MyEbooksChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$resolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$2;->val$resolver:Landroid/content/ContentResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isInMyEbooks(Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 184
    const/4 v6, 0x0

    .line 186
    .local v6, "collectionItemCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-wide/16 v4, 0x7

    invoke-static {v0, v4, v5, p2}, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->itemUri(Ljava/lang/String;JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 188
    .local v1, "itemUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "COUNT(*)"

    aput-object v3, v2, v0

    .line 189
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadataImpl$2;->val$resolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 190
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    move v0, v7

    .line 195
    :goto_0
    if-eqz v6, :cond_0

    .line 196
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v8

    .line 191
    goto :goto_0

    .line 195
    :cond_2
    if-eqz v6, :cond_3

    .line 196
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v8

    goto :goto_1

    .line 195
    .end local v1    # "itemUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 196
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method
