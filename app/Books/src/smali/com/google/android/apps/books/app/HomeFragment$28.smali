.class Lcom/google/android/apps/books/app/HomeFragment$28;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->onBookSelected(Ljava/lang/String;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$pressedView:Landroid/view/View;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2437
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$pressedView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2440
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2441
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 2497
    :cond_0
    :goto_0
    return-void

    .line 2445
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v5

    .line 2446
    .local v5, "volume":Lcom/google/android/apps/books/model/VolumeData;
    if-nez v5, :cond_2

    .line 2447
    const-string v6, "HomeFragment"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2448
    const-string v6, "HomeFragment"

    const-string v7, "missing metadata for volume"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2454
    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/HomeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/util/LoaderParams;->getCreatingShortcut(Landroid/os/Bundle;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 2455
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/app/HomeFragment;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2456
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->isExpiredNonSampleRental(Ljava/lang/String;)Z
    invoke-static {v6, v7}, Lcom/google/android/apps/books/app/HomeFragment;->access$4300(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2457
    const v6, 0x7f0f016c

    invoke-static {v0, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2464
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z
    invoke-static {v6}, Lcom/google/android/apps/books/app/HomeFragment;->access$4400(Lcom/google/android/apps/books/app/HomeFragment;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->hasOfflineLicense(Ljava/lang/String;)Z
    invoke-static {v6, v7}, Lcom/google/android/apps/books/app/HomeFragment;->access$4500(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 2466
    const v6, 0x7f0f016b

    invoke-static {v0, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2470
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z
    invoke-static {v6}, Lcom/google/android/apps/books/app/HomeFragment;->access$4400(Lcom/google/android/apps/books/app/HomeFragment;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 2471
    const v4, 0x7f0f016a

    .line 2472
    .local v4, "stringId":I
    const v6, 0x7f0f016a

    invoke-static {v0, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2474
    .end local v4    # "stringId":I
    :cond_5
    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 2475
    sget-object v6, Lcom/google/android/apps/books/R$styleable;->AppTheme:[I

    invoke-virtual {v0, v10, v6, v9, v9}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 2477
    .local v2, "attrs":Landroid/content/res/TypedArray;
    const/4 v6, 0x3

    const v7, 0x1080027

    invoke-virtual {v2, v6, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 2480
    .local v3, "iconResource":I
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 2482
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2483
    .local v1, "args":Landroid/os/Bundle;
    new-instance v6, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v6, v1}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const v8, 0x7f0f016e

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const v8, 0x7f0f016f

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const/high16 v8, 0x1040000

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setCancelLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    const v8, 0x7f0f016d

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setIconResource(I)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 2489
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v6, v5, v1}, Lcom/google/android/apps/books/app/HomeFragment;->addVolumeArgs(Lcom/google/android/apps/books/model/VolumeData;Landroid/os/Bundle;)V

    .line 2490
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    invoke-static {v6}, Lcom/google/android/apps/books/app/HomeFragment;->access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v6

    const-class v7, Lcom/google/android/apps/books/app/HomeFragment$OfferOverrideOpenDialog;

    invoke-interface {v6, v7, v1, v10}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2491
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$pressedView:Landroid/view/View;

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->setTransitionView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2496
    .end local v1    # "args":Landroid/os/Bundle;
    .end local v2    # "attrs":Landroid/content/res/TypedArray;
    .end local v3    # "iconResource":I
    :cond_6
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$volumeId:Ljava/lang/String;

    sget-object v8, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_LIBRARY_VIEW:Lcom/google/android/apps/books/app/BookOpeningFlags;

    iget-object v9, p0, Lcom/google/android/apps/books/app/HomeFragment$28;->val$pressedView:Landroid/view/View;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->openBook(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/books/app/HomeFragment;->access$4600(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    goto/16 :goto_0
.end method
