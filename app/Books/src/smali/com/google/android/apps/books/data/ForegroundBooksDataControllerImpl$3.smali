.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->setForceDownload(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

.field final synthetic val$forceDownload:Z

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;->val$volumeId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;->val$forceDownload:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$000(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;->val$volumeId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;->val$forceDownload:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->setForceDownload(Ljava/lang/String;Z)V

    .line 258
    return-void
.end method
