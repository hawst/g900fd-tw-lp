.class Lcom/google/android/apps/books/data/PageContentSubcontroller$11;
.super Lcom/google/android/apps/books/data/BasePendingAction;
.source "PageContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$result:Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$result:Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-direct {p0}, Lcom/google/android/apps/books/data/BasePendingAction;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 364
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$result:Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;

    iget-object v2, v2, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v2}, Lcom/google/android/apps/books/data/DataControllerBlob;->save()V

    .line 365
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$result:Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;

    iget-object v2, v2, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->ccBox:Lcom/google/common/base/Supplier;

    invoke-interface {v2}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/CcBox;

    .line 367
    .local v0, "ccBox":Lcom/google/android/apps/books/model/CcBox;
    if-eqz v0, :cond_0

    .line 368
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, v3, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v4, v4, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->setCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/CcBox;)V

    .line 371
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$1000(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    sget-object v4, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 375
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$1100(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    .end local v0    # "ccBox":Lcom/google/android/apps/books/model/CcBox;
    :goto_0
    return-void

    .line 372
    :catch_0
    move-exception v1

    .line 373
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$1000(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$1100(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageContentSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$1100(Lcom/google/android/apps/books/data/PageContentSubcontroller;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$11;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v2
.end method
