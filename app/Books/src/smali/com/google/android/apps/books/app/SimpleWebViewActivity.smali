.class public abstract Lcom/google/android/apps/books/app/SimpleWebViewActivity;
.super Landroid/app/Activity;
.source "SimpleWebViewActivity.java"


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getUrl()Ljava/lang/String;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v4, 0x7f0400c0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->setContentView(I)V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 27
    .local v0, "extras":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "url":Ljava/lang/String;
    const-string v4, "android.intent.extra.TITLE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 32
    const v4, 0x7f0e01d5

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/webkit/WebView;

    iput-object v4, p0, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->mWebView:Landroid/webkit/WebView;

    .line 33
    iget-object v4, p0, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    .line 34
    .local v3, "webSettings":Landroid/webkit/WebSettings;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    .line 35
    iget-object v4, p0, Lcom/google/android/apps/books/app/SimpleWebViewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 36
    return-void
.end method
