.class public Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;
.super Ljava/lang/Object;
.source "ApiaryAnnotationServer.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationServer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

.field private final mConfig:Lcom/google/android/apps/books/util/Config;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "BooksApiaryTraffic"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "com.google.api.client"

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 51
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lcom/google/android/apps/books/api/ApiaryClient;Lcom/google/android/apps/books/util/Config;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "apiaryClient"    # Lcom/google/android/apps/books/api/ApiaryClient;
    .param p3, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mAccount:Landroid/accounts/Account;

    .line 94
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    .line 95
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 96
    return-void
.end method

.method private buildResponse(Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 2
    .param p1, "annotations"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;->error:Lcom/google/android/apps/books/api/data/JsonError;

    if-nez v0, :cond_1

    .line 170
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;->nextPageToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;->nextPageToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;->items:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/PaginatedResponse;->partialResponse(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v0

    .line 174
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;->items:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/PaginatedResponse;->finalResponse(Ljava/util/List;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v0

    goto :goto_0

    .line 177
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;->error:Lcom/google/android/apps/books/api/data/JsonError;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;-><init>(Lcom/google/android/apps/books/api/data/JsonError;)V

    throw v0
.end method

.method private getPaginatedResponse(Lcom/google/api/client/http/GenericUrl;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 3
    .param p1, "url"    # Lcom/google/api/client/http/GenericUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/http/GenericUrl;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 163
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->getResponse(Lcom/google/api/client/http/HttpRequest;Z)Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    .line 164
    .local v0, "response":Lcom/google/api/client/http/HttpResponse;
    const-class v1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->buildResponse(Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v1

    return-object v1
.end method

.method private getResponse(Lcom/google/api/client/http/HttpRequest;Z)Lcom/google/api/client/http/HttpResponse;
    .locals 4
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "ignore403ErrorCode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    if-eqz p2, :cond_0

    .line 185
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v2}, Lcom/google/api/client/http/HttpRequest;->setThrowExceptionOnExecuteError(Z)Lcom/google/api/client/http/HttpRequest;

    .line 187
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mAccount:Landroid/accounts/Account;

    invoke-interface {v2, p1, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->executeRaw(Lcom/google/api/client/http/HttpRequest;Landroid/accounts/Account;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    .line 188
    .local v1, "response":Lcom/google/api/client/http/HttpResponse;
    invoke-virtual {v1}, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/google/api/client/http/HttpResponse;->getStatusCode()I

    move-result v2

    const/16 v3, 0x193

    if-ne v2, v3, :cond_2

    .line 189
    :cond_1
    return-object v1

    .line 191
    :cond_2
    new-instance v2, Lcom/google/api/client/http/HttpResponseException;

    invoke-direct {v2, v1}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/HttpResponse;)V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    .end local v1    # "response":Lcom/google/api/client/http/HttpResponse;
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/IOException;
    invoke-static {p1}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->getLogString(Lcom/google/api/client/http/HttpRequest;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/books/net/HttpHelper;->wrapException(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v2

    throw v2
.end method

.method private onlyReturnSummaryForAddAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Z
    .locals 2
    .param p1, "a"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 158
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    .locals 7
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "a"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->onlyReturnSummaryForAddAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forAddAnnotation(Lcom/google/android/apps/books/util/Config;Z)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 103
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    sget-object v3, Lcom/google/android/apps/books/annotations/AnnotationUtils;->SERVING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    invoke-static {p1, p2, v3}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->asJsonAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    move-result-object v0

    .line 105
    .local v0, "json":Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->serverId:Ljava/lang/String;

    .line 106
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-string v4, "POST"

    invoke-interface {v3, v4, v2, v0}, Lcom/google/android/apps/books/api/ApiaryClient;->makeJsonRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 107
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v1, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    return-object v3
.end method

.method public deleteAnnotation(Ljava/lang/String;)V
    .locals 6
    .param p1, "serverId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forEditOrDeleteAnnotation(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    .line 124
    .local v1, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v2, v1}, Lcom/google/android/apps/books/api/ApiaryClient;->makeDeleteRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 125
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v3, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mAccount:Landroid/accounts/Account;

    const/4 v5, 0x0

    new-array v5, v5, [I

    invoke-interface {v2, v0, v3, v4, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 126
    return-void
.end method

.method public editAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    .locals 7
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "serverId"    # Ljava/lang/String;
    .param p3, "a"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v3, p2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forEditOrDeleteAnnotation(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 114
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    sget-object v3, Lcom/google/android/apps/books/annotations/AnnotationUtils;->SERVING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    invoke-static {p1, p3, v3}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->asJsonAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    move-result-object v0

    .line 116
    .local v0, "json":Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    iput-object p2, v0, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->serverId:Ljava/lang/String;

    .line 117
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-string v4, "PUT"

    invoke-interface {v3, v4, v2, v0}, Lcom/google/android/apps/books/api/ApiaryClient;->makeJsonRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 118
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v4, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    iget-object v5, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v3, v1, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    return-object v3
.end method

.method public getAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;)Lcom/google/android/apps/books/annotations/AnnotationData;
    .locals 6
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "key"    # Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v4, p1, p2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchVolumeAnnotationData(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v3

    .line 234
    .local v3, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v4, v3}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 235
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    const/4 v4, 0x0

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->getResponse(Lcom/google/api/client/http/HttpRequest;Z)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    .line 236
    .local v1, "response":Lcom/google/api/client/http/HttpResponse;
    invoke-virtual {v1}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 238
    .local v2, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->getUserLocaleString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->getEntityLocaleString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAnnotationData(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 241
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    return-object v4

    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v4
.end method

.method public getAnnotationDatas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Collection;II)Ljava/io/InputStream;
    .locals 5
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p3, "imageW"    # I
    .param p4, "imageH"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            ">;II)",
            "Ljava/io/InputStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    .local p2, "ids":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/AnnotationData$Key;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {p2}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v3, p1, v4, p3, p4}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchVolumeAnnotationDatas(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;II)Lcom/google/api/client/http/GenericUrl;

    move-result-object v2

    .line 225
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 226
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->getResponse(Lcom/google/api/client/http/HttpRequest;Z)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    .line 227
    .local v1, "response":Lcom/google/api/client/http/HttpResponse;
    invoke-virtual {v1}, Lcom/google/api/client/http/HttpResponse;->getContent()Ljava/io/InputStream;

    move-result-object v3

    return-object v3
.end method

.method public getCharacterQuotas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 200
    .local p2, "layerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v6, p1, p2}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchAnnotationsSummary(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v5

    .line 201
    .local v5, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    sget-object v7, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v6, v5, v7}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v3

    .line 203
    .local v3, "request":Lcom/google/api/client/http/HttpRequest;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    const-class v7, Lcom/google/android/apps/books/annotations/data/JsonAnnotationsSummaryResponse;

    iget-object v8, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mAccount:Landroid/accounts/Account;

    const/4 v9, 0x0

    new-array v9, v9, [I

    invoke-interface {v6, v3, v7, v8, v9}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/data/JsonAnnotationsSummaryResponse;

    .line 205
    .local v4, "summary":Lcom/google/android/apps/books/annotations/data/JsonAnnotationsSummaryResponse;
    if-eqz v4, :cond_0

    iget-object v6, v4, Lcom/google/android/apps/books/annotations/data/JsonAnnotationsSummaryResponse;->layers:Ljava/util/List;

    if-nez v6, :cond_1

    .line 206
    :cond_0
    new-instance v6, Lcom/google/android/apps/books/annotations/AnnotationServerException;

    const-string v7, "failed to parse annotations summary"

    invoke-direct {v6, v7}, Lcom/google/android/apps/books/annotations/AnnotationServerException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 209
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 210
    .local v2, "quotas":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    iget-object v6, v4, Lcom/google/android/apps/books/annotations/data/JsonAnnotationsSummaryResponse;->layers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/data/JsonLayer;

    .line 211
    .local v1, "layer":Lcom/google/android/apps/books/annotations/data/JsonLayer;
    invoke-static {v1}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->fromJson(Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    .end local v1    # "layer":Lcom/google/android/apps/books/annotations/data/JsonLayer;
    :cond_2
    return-object v2
.end method

.method public getUserAnnotations(Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 9
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
    .param p2, "pageSize"    # I
    .param p3, "continuation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/LastSyncTooOldException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->layerId:Ljava/lang/String;

    iget-wide v4, p1, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;->lastSyncDate:J

    move v6, p2

    move-object v7, p3

    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forGetNewAnnotationOperations(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;JILjava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v8

    .line 136
    .local v8, "url":Lcom/google/api/client/http/GenericUrl;
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->getPaginatedResponse(Lcom/google/api/client/http/GenericUrl;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    :try_end_0
    .catch Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 137
    .end local v8    # "url":Lcom/google/api/client/http/GenericUrl;
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
    iget-object v1, v0, Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;->error:Lcom/google/android/apps/books/api/data/JsonError;

    const/16 v2, 0x193

    const-string v3, "updatedMinTooFarInPast"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/api/data/JsonError;->hasCodeAndReason(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    new-instance v1, Lcom/google/android/apps/books/annotations/LastSyncTooOldException;

    invoke-direct {v1}, Lcom/google/android/apps/books/annotations/LastSyncTooOldException;-><init>()V

    throw v1

    .line 141
    :cond_0
    throw v0
.end method

.method public getVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 9
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .param p2, "layerVersion"    # Ljava/lang/String;
    .param p3, "pageSize"    # I
    .param p4, "continuation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->getLayerId()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->startPosition:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->endPosition:Ljava/lang/String;

    move-object v3, p2

    move v6, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchVolumeAnnotations(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v8

    .line 153
    .local v8, "url":Lcom/google/api/client/http/GenericUrl;
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->getPaginatedResponse(Lcom/google/api/client/http/GenericUrl;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;)Ljava/util/List;
    .locals 10
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-static {v7, p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forFetchVolumeAnnotationSummary(Lcom/google/android/apps/books/util/Config;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v6

    .line 249
    .local v6, "url":Lcom/google/api/client/http/GenericUrl;
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->mApiaryClient:Lcom/google/android/apps/books/api/ApiaryClient;

    invoke-interface {v7, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v3

    .line 250
    .local v3, "request":Lcom/google/api/client/http/HttpRequest;
    const/4 v7, 0x0

    invoke-direct {p0, v3, v7}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer;->getResponse(Lcom/google/api/client/http/HttpRequest;Z)Lcom/google/api/client/http/HttpResponse;

    move-result-object v4

    .line 251
    .local v4, "response":Lcom/google/api/client/http/HttpResponse;
    const-class v7, Lcom/google/android/apps/books/annotations/data/JsonLayerSummaryResponse;

    invoke-virtual {v4, v7}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/data/JsonLayerSummaryResponse;

    .line 252
    .local v1, "json":Lcom/google/android/apps/books/annotations/data/JsonLayerSummaryResponse;
    if-eqz v1, :cond_0

    iget-object v7, v1, Lcom/google/android/apps/books/annotations/data/JsonLayerSummaryResponse;->items:Ljava/util/List;

    if-nez v7, :cond_1

    .line 253
    :cond_0
    new-instance v7, Lcom/google/android/apps/books/annotations/AnnotationServerException;

    const-string v8, "failed to parse layer summary response"

    invoke-direct {v7, v8}, Lcom/google/android/apps/books/annotations/AnnotationServerException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 255
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 256
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    iget-object v7, v1, Lcom/google/android/apps/books/annotations/data/JsonLayerSummaryResponse;->items:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/data/JsonLayer;

    .line 257
    .local v2, "layer":Lcom/google/android/apps/books/annotations/data/JsonLayer;
    iget-object v7, v2, Lcom/google/android/apps/books/annotations/data/JsonLayer;->layerId:Ljava/lang/String;

    if-nez v7, :cond_2

    .line 258
    new-instance v7, Lcom/google/android/apps/books/annotations/AnnotationServerException;

    const-string v8, "missing layer ID"

    invoke-direct {v7, v8}, Lcom/google/android/apps/books/annotations/AnnotationServerException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 260
    :cond_2
    iget-object v7, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    iget-object v8, p1, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    sget-object v9, Lcom/google/android/apps/books/annotations/Layer$Type;->VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-static {v7, v8, v9, v2}, Lcom/google/android/apps/books/annotations/Layer;->fromJson(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    .end local v2    # "layer":Lcom/google/android/apps/books/annotations/data/JsonLayer;
    :cond_3
    return-object v5
.end method
