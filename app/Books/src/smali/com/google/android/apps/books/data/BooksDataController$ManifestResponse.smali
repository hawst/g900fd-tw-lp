.class public Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
.super Ljava/lang/Object;
.source "BooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/BooksDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ManifestResponse"
.end annotation


# instance fields
.field public final fromServer:Z

.field public final localPageIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final localResourceIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final localSegmentIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final localStructureIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final manifest:Lcom/google/android/apps/books/model/VolumeManifest;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Z)V
    .locals 0
    .param p1, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p6, "fromServer"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p2, "localSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "localResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "localPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p5, "localStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p1, p0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    .line 164
    iput-object p2, p0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localSegmentIds:Ljava/util/Set;

    .line 165
    iput-object p3, p0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localResourceIds:Ljava/util/Set;

    .line 166
    iput-object p4, p0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localPageIds:Ljava/util/Set;

    .line 167
    iput-object p5, p0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localStructureIds:Ljava/util/Set;

    .line 168
    iput-boolean p6, p0, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->fromServer:Z

    .line 169
    return-void
.end method
