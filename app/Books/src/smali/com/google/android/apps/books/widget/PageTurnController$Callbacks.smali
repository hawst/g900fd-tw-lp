.class public interface abstract Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
.super Ljava/lang/Object;
.source "PageTurnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract canTurn(Lcom/google/android/apps/books/util/ScreenDirection;)Z
.end method

.method public abstract directionTowardPosition(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/util/ScreenDirection;
.end method

.method public abstract finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V
.end method

.method public abstract onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
.end method

.method public abstract onIsTurningChanged(Z)V
.end method

.method public abstract onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
.end method

.method public abstract setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;F)V
.end method
