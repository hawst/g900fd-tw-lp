.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->onSuccessfulParse()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

.field final synthetic val$altTextMap:Lcom/google/android/apps/books/model/LabelMap;

.field final synthetic val$positionMap:Lcom/google/android/apps/books/model/LabelMap;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->val$positionMap:Lcom/google/android/apps/books/model/LabelMap;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->val$altTextMap:Lcom/google/android/apps/books/model/LabelMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/widget/TtsLoadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mListener:Lcom/google/android/apps/books/widget/TtsLoadListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/widget/TtsLoadListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mRequestId:I
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$600(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$700(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPassage()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mTextContent:Ljava/lang/StringBuilder;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$800(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->val$positionMap:Lcom/google/android/apps/books/model/LabelMap;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;->val$altTextMap:Lcom/google/android/apps/books/model/LabelMap;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/widget/TtsLoadListener;->setTtsPassageText(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 207
    :cond_0
    return-void
.end method
