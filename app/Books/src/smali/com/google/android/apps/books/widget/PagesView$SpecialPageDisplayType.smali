.class public final enum Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
.super Ljava/lang/Enum;
.source "PagesView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpecialPageDisplayType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

.field public static final enum BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

.field public static final enum GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;


# instance fields
.field private final mDisplayAtFullSize:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    const-string v1, "GAP"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 25
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    const-string v1, "BLANK"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->$VALUES:[Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p3, "displayAtFullSize"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->mDisplayAtFullSize:Z

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->$VALUES:[Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    return-object v0
.end method


# virtual methods
.method public displayAtFullSize()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->mDisplayAtFullSize:Z

    return v0
.end method
