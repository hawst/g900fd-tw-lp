.class public Lcom/google/android/apps/books/api/data/RequestAccessResponse;
.super Ljava/lang/Object;
.source "RequestAccessResponse.java"


# instance fields
.field public concurrentAccess:Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
