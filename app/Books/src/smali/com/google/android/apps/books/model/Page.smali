.class public interface abstract Lcom/google/android/apps/books/model/Page;
.super Ljava/lang/Object;
.source "Page.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Identifiable;


# virtual methods
.method public abstract getPageOrder()I
.end method

.method public abstract getRemoteUrl()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract isViewable()Z
.end method
