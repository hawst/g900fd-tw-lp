.class public Lcom/google/android/apps/books/widget/TranslateViewController;
.super Ljava/lang/Object;
.source "TranslateViewController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;,
        Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;
    }
.end annotation


# instance fields
.field private mActionMode:Landroid/support/v7/view/ActionMode;

.field private final mActionModeCallback:Landroid/support/v7/view/ActionMode$Callback;

.field private final mActionModeTitle:Ljava/lang/String;

.field private final mContainer:Landroid/view/ViewGroup;

.field private final mDelegate:Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;

.field private final mDetectLanguageString:Ljava/lang/String;

.field private final mFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private mLanguageNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLanguages:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOnDismissListener:Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;

.field private final mParent:Landroid/view/ViewGroup;

.field private final mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

.field private final mSourceAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSourceLanguageSpinner:Landroid/widget/Spinner;

.field private final mTargetAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTargetLanguageSpinner:Landroid/widget/Spinner;

.field private mText:Ljava/lang/CharSequence;

.field private final mTranslateView:Landroid/view/View;

.field private final mTranslatedTextView:Landroid/widget/TextView;

.field private final mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "delegate"    # Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;

    .prologue
    const/4 v5, 0x0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v2, Lcom/google/android/apps/books/widget/TranslateViewController$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/TranslateViewController$1;-><init>(Lcom/google/android/apps/books/widget/TranslateViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    .line 130
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iput-object p2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mParent:Landroid/view/ViewGroup;

    .line 135
    iput-object p3, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mDelegate:Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;

    .line 137
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0400d4

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0e01fc

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslateView:Landroid/view/View;

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslateView:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0e0200

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslatedTextView:Landroid/widget/TextView;

    .line 150
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslatedTextView:Landroid/widget/TextView;

    new-instance v3, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v3}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0e01fe

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x7f0e01ff

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->setupSpinner(Landroid/content/Context;Landroid/widget/Spinner;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceAdapter:Landroid/widget/ArrayAdapter;

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->setupSpinner(Landroid/content/Context;Landroid/widget/Spinner;)Landroid/widget/ArrayAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetAdapter:Landroid/widget/ArrayAdapter;

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 163
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 165
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 166
    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 168
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mParent:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mParent:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    .line 171
    new-instance v2, Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    invoke-direct {v2, v3}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 173
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 174
    .local v0, "app":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getTranslationServerController()Lcom/google/android/apps/books/api/TranslationServerController;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

    .line 176
    const v2, 0x7f0f0084

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mDetectLanguageString:Ljava/lang/String;

    .line 177
    const v2, 0x7f0f0085

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionModeTitle:Ljava/lang/String;

    .line 181
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v2, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 182
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionModeTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/TranslateViewController;)Lcom/google/common/collect/BiMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguages:Lcom/google/common/collect/BiMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mDetectLanguageString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/widget/TranslateViewController;Lcom/google/common/collect/BiMap;)Lcom/google/common/collect/BiMap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Lcom/google/common/collect/BiMap;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguages:Lcom/google/common/collect/BiMap;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/widget/TranslateViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->askServerToTranslateText()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->setTranslationText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->setLastUsedTargetLanguageCode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/TranslateViewController;Lcom/google/common/collect/BiMap;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Lcom/google/common/collect/BiMap;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->sortedListOfLangaugeNames(Lcom/google/common/collect/BiMap;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/TranslateViewController;Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Landroid/widget/ArrayAdapter;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/TranslateViewController;->addLanguagesToAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->lastUsedTargetLanguageCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/widget/TranslateViewController;Landroid/widget/Spinner;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;
    .param p1, "x1"    # Landroid/widget/Spinner;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/TranslateViewController;->setSpinnerToLanguageCode(Landroid/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TranslateViewController;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private addLanguagesToAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .param p3, "specialFirstString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 278
    .local p1, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .local p2, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 279
    invoke-virtual {p1, p3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 281
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 282
    .local v1, "language":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 284
    .end local v1    # "language":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private askServerToTranslateText()V
    .locals 7

    .prologue
    .line 432
    const-string v2, ""

    .line 433
    .local v2, "sourceLanguageCode":Ljava/lang/String;
    const-string v3, ""

    .line 436
    .local v3, "targetLanguageCode":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 437
    .local v0, "index":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->languageCodeForNameAtIndex(I)Ljava/lang/String;

    move-result-object v3

    .line 443
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;

    invoke-virtual {v4}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 444
    if-lez v0, :cond_0

    .line 446
    add-int/lit8 v4, v0, -0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/TranslateViewController;->languageCodeForNameAtIndex(I)Ljava/lang/String;

    move-result-object v2

    .line 449
    :cond_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 451
    const-string v4, "TranslateViewController"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 452
    const-string v4, "TranslateViewController"

    const-string v5, "no target language was selected"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_1
    :goto_0
    return-void

    .line 460
    :cond_2
    move-object v1, v3

    .line 462
    .local v1, "lastUsedTargetLanguageCode":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mText:Ljava/lang/CharSequence;

    new-instance v6, Lcom/google/android/apps/books/widget/TranslateViewController$3;

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/books/widget/TranslateViewController$3;-><init>(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3, v5, v6}, Lcom/google/android/apps/books/api/TranslationServerController;->translateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;)V

    goto :goto_0
.end method

.method private discoverSupportedLanguages()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslationServerController:Lcom/google/android/apps/books/api/TranslationServerController;

    new-instance v1, Lcom/google/android/apps/books/widget/TranslateViewController$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/TranslateViewController$2;-><init>(Lcom/google/android/apps/books/widget/TranslateViewController;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/api/TranslationServerController;->discoverLanguages(Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;)V

    .line 415
    return-void
.end method

.method private findIndexOfLanguageName(Ljava/lang/String;)I
    .locals 1
    .param p1, "languageName"    # Ljava/lang/String;

    .prologue
    .line 299
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private languageCodeForName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguages:Lcom/google/common/collect/BiMap;

    invoke-interface {v0, p1}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private languageCodeForNameAtIndex(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 425
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 426
    :cond_0
    const-string v0, ""

    .line 428
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->languageCodeForName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private languageNameForCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguages:Lcom/google/common/collect/BiMap;

    invoke-interface {v0}, Lcom/google/common/collect/BiMap;->inverse()Lcom/google/common/collect/BiMap;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private languagesAlreadyDiscovered()Z
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lastUsedTargetLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getTranslateLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setLastUsedTargetLanguageCode(Ljava/lang/String;)V
    .locals 1
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setTranslateLanguage(Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method private setSpinnerToLanguageCode(Landroid/widget/Spinner;Ljava/lang/String;)V
    .locals 5
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    .line 349
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/TranslateViewController;->languageNameForCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 351
    .local v1, "name":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->findIndexOfLanguageName(Ljava/lang/String;)I

    move-result v0

    .line 352
    .local v0, "index":I
    if-gez v0, :cond_1

    .line 353
    const-string v2, "TranslateViewController"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 354
    const-string v2, "TranslateViewController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Index for language \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' not found."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;

    if-ne p1, v2, :cond_2

    .line 362
    add-int/lit8 v0, v0, 0x1

    .line 365
    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method private setTranslationText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslatedTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mTranslatedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/TextView;->scrollTo(II)V

    .line 519
    return-void
.end method

.method private setupSpinner(Landroid/content/Context;Landroid/widget/Spinner;)Landroid/widget/ArrayAdapter;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "spinner"    # Landroid/widget/Spinner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/Spinner;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 263
    .local v1, "emptyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v2, 0x1090008

    invoke-direct {v0, p1, v2, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 265
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 266
    invoke-virtual {p2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 269
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 271
    return-object v0
.end method

.method private sortedListOfLangaugeNames(Lcom/google/common/collect/BiMap;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "langauges":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Lcom/google/common/collect/BiMap;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayListWithCapacity(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 334
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Lcom/google/common/collect/BiMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 335
    .local v1, "name":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 337
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 338
    return-object v2
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->dismiss()V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mParent:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 235
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->getVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->setVisible(Z)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionMode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mOnDismissListener:Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mOnDismissListener:Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;->onDismiss()V

    .line 227
    :cond_1
    return-void
.end method

.method public getVisible()Z
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0}, Lcom/google/android/ublib/view/FadeAnimationController;->getVisible()Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mContainer:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_0

    .line 501
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->dismiss()V

    .line 503
    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 507
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->askServerToTranslateText()V

    .line 508
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method public setOnDismissListener(Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mOnDismissListener:Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;

    .line 244
    return-void
.end method

.method public setVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mFadeAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 254
    return-void
.end method

.method public translateText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mText:Ljava/lang/CharSequence;

    .line 195
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->setTranslationText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->setVisible(Z)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mDelegate:Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;->startActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 205
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->languagesAlreadyDiscovered()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->discoverSupportedLanguages()V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TranslateViewController;->askServerToTranslateText()V

    goto :goto_0
.end method
