.class Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;
.super Ljava/lang/Object;
.source "SingleBookUploaderImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mFirstRunStart:J

.field mHasLoggedError:Z

.field final synthetic this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V
    .locals 2

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mFirstRunStart:J

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mHasLoggedError:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v12, 0x1

    .line 67
    iget-object v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mProcessingDone:Z
    invoke-static {v7}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$000(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 102
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-wide v8, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mFirstRunStart:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gez v7, :cond_1

    .line 71
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mFirstRunStart:J

    .line 76
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$100(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mAccount:Landroid/accounts/Account;
    invoke-static {v8}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$200(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Landroid/accounts/Account;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v5

    .line 80
    .local v5, "server":Lcom/google/android/apps/books/net/BooksServer;
    :try_start_0
    invoke-interface {v5}, Lcom/google/android/apps/books/net/BooksServer;->getFailedCloudeloadedVolumes()Ljava/util/List;

    move-result-object v4

    .line 82
    .local v4, "response":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .line 83
    .local v6, "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    iget-object v7, v6, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;
    invoke-static {v8}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/books/upload/Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 84
    iget-object v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    const/4 v8, 0x1

    # setter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mProcessingDone:Z
    invoke-static {v7, v8}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$002(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Z)Z

    .line 85
    iget-object v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    new-instance v8, Ljava/lang/Exception;

    invoke-direct {v8}, Ljava/lang/Exception;-><init>()V

    const/4 v9, 0x1

    # invokes: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V
    invoke-static {v7, v8, v9}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$400(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Ljava/lang/Exception;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "response":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    .end local v6    # "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    :catch_0
    move-exception v2

    .line 90
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "SingleBookUploaderImpl"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mHasLoggedError:Z

    if-nez v7, :cond_3

    .line 91
    const-string v7, "SingleBookUploaderImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception in backoffSync: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iput-boolean v12, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mHasLoggedError:Z

    .line 98
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    const-wide/16 v8, 0x1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->mFirstRunStart:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x7d0

    div-long/2addr v10, v12

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 101
    .local v0, "delay":J
    iget-object v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;->this$0:Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    # getter for: Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;
    invoke-static {v7}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->access$500(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, p0, v0, v1, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_0
.end method
