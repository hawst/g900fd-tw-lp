.class public Lcom/google/android/apps/books/util/AnimatorUtils;
.super Ljava/lang/Object;
.source "AnimatorUtils.java"


# direct methods
.method public static getCurrentValue(JJJFFLandroid/animation/TimeInterpolator;)F
    .locals 4
    .param p0, "started"    # J
    .param p2, "duration"    # J
    .param p4, "now"    # J
    .param p6, "startValue"    # F
    .param p7, "endValue"    # F
    .param p8, "interpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 18
    sub-long v2, p4, p0

    long-to-float v2, v2

    long-to-float v3, p2

    div-float v1, v2, v3

    .line 19
    .local v1, "fractionElapsed":F
    sub-float v0, p7, p6

    .line 20
    .local v0, "change":F
    invoke-interface {p8, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v2, v0

    add-float/2addr v2, p6

    return v2
.end method
