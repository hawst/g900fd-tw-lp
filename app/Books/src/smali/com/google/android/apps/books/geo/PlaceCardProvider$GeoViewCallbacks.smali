.class Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;
.super Ljava/lang/Object;
.source "PlaceCardProvider.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/GeoAnnotationView$GeoViewOwner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/geo/PlaceCardProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GeoViewCallbacks"
.end annotation


# instance fields
.field private final mCallbacksSequenceNumber:I

.field final synthetic this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/geo/PlaceCardProvider;I)V
    .locals 0
    .param p2, "sequenceNumber"    # I

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput p2, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->mCallbacksSequenceNumber:I

    .line 109
    return-void
.end method


# virtual methods
.method public onFinishedLoading(Ljava/lang/Exception;)V
    .locals 7
    .param p1, "error"    # Ljava/lang/Exception;

    .prologue
    .line 113
    iget v5, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->mCallbacksSequenceNumber:I

    iget-object v6, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    # getter for: Lcom/google/android/apps/books/geo/PlaceCardProvider;->mRequestSequenceNumber:I
    invoke-static {v6}, Lcom/google/android/apps/books/geo/PlaceCardProvider;->access$000(Lcom/google/android/apps/books/geo/PlaceCardProvider;)I

    move-result v6

    if-ne v5, v6, :cond_5

    .line 114
    const-string v5, "PlaceCardProvider"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 115
    const-string v5, "PlaceCardProvider"

    const-string v6, "View loaded"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    const/4 v2, 0x0

    .line 118
    .local v2, "someViewNotDoneLoading":Z
    iget-object v5, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    # getter for: Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;
    invoke-static {v5}, Lcom/google/android/apps/books/geo/PlaceCardProvider;->access$100(Lcom/google/android/apps/books/geo/PlaceCardProvider;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;

    .line 119
    .local v4, "wrapper":Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    invoke-virtual {v4}, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->getGeoAnnotationView()Lcom/google/android/apps/books/widget/GeoAnnotationView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getLoadResult()Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 121
    .local v0, "doneLoading":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    if-nez v0, :cond_1

    .line 122
    const/4 v2, 0x1

    .line 126
    .end local v0    # "doneLoading":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<*>;"
    .end local v4    # "wrapper":Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    :cond_2
    if-nez v2, :cond_5

    .line 128
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 129
    .local v3, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    # getter for: Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;
    invoke-static {v5}, Lcom/google/android/apps/books/geo/PlaceCardProvider;->access$100(Lcom/google/android/apps/books/geo/PlaceCardProvider;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;

    .line 130
    .restart local v4    # "wrapper":Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    invoke-virtual {v4}, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->getGeoAnnotationView()Lcom/google/android/apps/books/widget/GeoAnnotationView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getLoadResult()Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess(Lcom/google/android/apps/books/util/ExceptionOr;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 131
    invoke-virtual {v4}, Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;->getCardView()Landroid/view/View;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    .end local v4    # "wrapper":Lcom/google/android/apps/books/geo/PlaceCardProvider$PlaceCardWrapper;
    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    iget-object v5, v5, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 135
    iget-object v5, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/android/apps/books/geo/PlaceCardProvider;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 136
    iget-object v5, p0, Lcom/google/android/apps/books/geo/PlaceCardProvider$GeoViewCallbacks;->this$0:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    # getter for: Lcom/google/android/apps/books/geo/PlaceCardProvider;->mLoadingViews:Ljava/util/Set;
    invoke-static {v5}, Lcom/google/android/apps/books/geo/PlaceCardProvider;->access$100(Lcom/google/android/apps/books/geo/PlaceCardProvider;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->clear()V

    .line 139
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "someViewNotDoneLoading":Z
    .end local v3    # "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_5
    return-void
.end method
