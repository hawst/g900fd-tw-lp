.class public Lcom/google/android/apps/books/app/StubActionBar;
.super Landroid/support/v7/app/ActionBar;
.source "StubActionBar.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/support/v7/app/ActionBar;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisplayOptions()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    return v0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 125
    return-void
.end method

.method public setDisplayOptions(II)V
    .locals 0
    .param p1, "options"    # I
    .param p2, "mask"    # I

    .prologue
    .line 95
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "subtitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 80
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 70
    return-void
.end method

.method public show()V
    .locals 0

    .prologue
    .line 225
    return-void
.end method
