.class public Lcom/google/android/apps/books/data/LargeBlobFromServer;
.super Ljava/lang/Object;
.source "LargeBlobFromServer.java"

# interfaces
.implements Lcom/google/android/apps/books/data/DataControllerBlob;


# instance fields
.field private final mByteArrayCache:Lcom/google/android/apps/books/data/ByteArrayCache;

.field private final mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

.field private mSavedContent:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/BooksDataStore$Committer;)V
    .locals 1
    .param p1, "committer"    # Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    .line 28
    new-instance v0, Lcom/google/android/apps/books/data/ByteArrayCache;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/data/ByteArrayCache;-><init>(Lcom/google/android/apps/books/data/InputStreamSource;)V

    iput-object v0, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mByteArrayCache:Lcom/google/android/apps/books/data/ByteArrayCache;

    .line 29
    return-void
.end method


# virtual methods
.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mByteArrayCache:Lcom/google/android/apps/books/data/ByteArrayCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/data/ByteArrayCache;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public save()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mSavedContent:Z

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->commit()V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/data/LargeBlobFromServer;->mSavedContent:Z

    .line 47
    :cond_0
    return-void
.end method
