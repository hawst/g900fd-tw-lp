.class public Lcom/google/android/apps/books/util/BookCoverImageProvider;
.super Ljava/lang/Object;
.source "BookCoverImageProvider.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;
    }
.end annotation


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;

.field private mFuture:Lcom/google/android/apps/books/common/ImageFuture;

.field private final mImageCallback:Lcom/google/android/apps/books/common/ImageCallback;

.field private mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;)V
    .locals 1
    .param p1, "callbacks"    # Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;-><init>(Lcom/google/android/apps/books/util/BookCoverImageProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mImageCallback:Lcom/google/android/apps/books/common/ImageCallback;

    .line 38
    iput-object p1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mCallbacks:Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/BookCoverImageProvider;)Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/BookCoverImageProvider;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/util/BookCoverImageProvider;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/BookCoverImageProvider;
    .param p1, "x1"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/util/BookCoverImageProvider;Lcom/google/android/apps/books/common/ImageFuture;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/BookCoverImageProvider;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/ImageFuture;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    return-object p1
.end method


# virtual methods
.method public cancelFetch()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->cancel()V

    .line 59
    iput-object v1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 61
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .line 62
    return-void
.end method

.method public fetchImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "listener"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->cancel()V

    .line 46
    iput-object v2, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 48
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mCallbacks:Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mImageCallback:Lcom/google/android/apps/books/common/ImageCallback;

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/books/util/BookCoverImageProvider$Callbacks;->getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/ImageFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    iput-object v2, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;

    .line 53
    :cond_1
    return-void
.end method
