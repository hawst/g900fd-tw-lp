.class Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;
.super Landroid/os/AsyncTask;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadZwiebackTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/google/android/apps/books/util/Nothing;",
        "Lcom/google/android/apps/books/util/Nothing;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2818
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2819
    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->mContext:Landroid/content/Context;

    .line 2820
    return-void
.end method


# virtual methods
.method protected doInBackground([Lcom/google/android/apps/books/util/Nothing;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 6
    .param p1, "params"    # [Lcom/google/android/apps/books/util/Nothing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/apps/books/util/Nothing;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2824
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/BooksApplication;->getCookieStore()Lorg/apache/http/client/CookieStore;

    move-result-object v1

    .line 2826
    .local v1, "cookieStore":Lorg/apache/http/client/CookieStore;
    invoke-interface {v1}, Lorg/apache/http/client/CookieStore;->getCookies()Ljava/util/List;

    move-result-object v2

    .line 2827
    .local v2, "cookies":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 2829
    .local v0, "cookie":Lorg/apache/http/cookie/Cookie;
    const-string v4, "NID"

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2830
    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v4

    .line 2833
    .end local v0    # "cookie":Lorg/apache/http/cookie/Cookie;
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v4

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2814
    check-cast p1, [Lcom/google/android/apps/books/util/Nothing;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->doInBackground([Lcom/google/android/apps/books/util/Nothing;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2837
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    const-string v0, "HomeFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2838
    const-string v0, "HomeFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Zwieback cookie result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2840
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # setter for: Lcom/google/android/apps/books/app/HomeFragment;->mLoadZwiebackCookieResult:Lcom/google/android/apps/books/util/ExceptionOr;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->access$4902(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;

    .line 2841
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->maybeAdvanceHats()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$5000(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 2842
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2814
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;->onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
