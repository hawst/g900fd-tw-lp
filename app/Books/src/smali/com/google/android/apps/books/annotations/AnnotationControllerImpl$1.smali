.class final Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$1;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public shouldStoreSyncedAnnotations(Ljava/lang/String;)Z
    .locals 4
    .param p1, "layer"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/LayerId;->fromString(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/LayerId;

    move-result-object v0

    .line 111
    .local v0, "layerId":Lcom/google/android/apps/books/annotations/LayerId;
    if-nez v0, :cond_1

    .line 112
    const-string v1, "AnnotationC"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    const-string v1, "AnnotationC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not downloading updates for unknown layer id \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x22

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_0
    const/4 v1, 0x0

    .line 118
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->storeSyncedAnnotations()Z

    move-result v1

    goto :goto_0
.end method
