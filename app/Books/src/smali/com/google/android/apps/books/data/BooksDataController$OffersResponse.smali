.class public Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
.super Ljava/lang/Object;
.source "BooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/BooksDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OffersResponse"
.end annotation


# instance fields
.field public final fromServer:Z

.field public final lastUpdateTimeMillis:J

.field public final offers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ZJ)V
    .locals 1
    .param p2, "fromServer"    # Z
    .param p3, "lastUpdateTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;ZJ)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput-object p1, p0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    .line 220
    iput-boolean p2, p0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->fromServer:Z

    .line 221
    iput-wide p3, p0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->lastUpdateTimeMillis:J

    .line 222
    return-void
.end method
