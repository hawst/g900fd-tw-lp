.class public Lcom/google/android/apps/books/app/BooksFragment;
.super Lcom/google/android/ublib/actionbar/UBLibFragment;
.source "BooksFragment.java"


# instance fields
.field private mDestroyed:Z

.field private mPaused:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected isDestroyed()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BooksFragment;->mDestroyed:Z

    return v0
.end method

.method protected isEffectivelyResumed()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/BooksFragment;->mPaused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BooksFragment;->isResumed()Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/BooksFragment;->mDestroyed:Z

    .line 21
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->onDestroy()V

    .line 22
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/BooksFragment;->mPaused:Z

    .line 40
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->onPause()V

    .line 41
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/BooksFragment;->mPaused:Z

    .line 34
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibFragment;->onResume()V

    .line 35
    return-void
.end method
