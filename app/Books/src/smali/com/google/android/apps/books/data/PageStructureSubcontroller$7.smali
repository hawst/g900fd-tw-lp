.class Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;
.super Ljava/lang/Object;
.source "PageStructureSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageStructureSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/Page;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$page:Lcom/google/android/apps/books/model/Page;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 257
    iget-object v3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v5

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$100(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 268
    :goto_0
    return-void

    .line 261
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/apps/books/data/LocalBlob;

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-interface {v3}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v4, v4, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/books/model/BooksDataStore;->getPageStructureFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/data/LocalBlob;-><init>(Lcom/google/android/apps/books/data/InputStreamSource;)V

    .line 263
    .local v0, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    new-instance v2, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/LocalSessionKey;

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/PageStructureSubcontroller$1;)V

    .line 264
    .local v2, "result":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    iget-object v3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v5, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    const/4 v6, 0x0

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Z)V
    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$700(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 265
    .end local v0    # "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    .end local v2    # "result":Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
    :catch_0
    move-exception v1

    .line 266
    .local v1, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v3, v4, v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$100(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 253
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$7;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
