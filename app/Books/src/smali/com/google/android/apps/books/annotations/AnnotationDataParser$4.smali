.class final Lcom/google/android/apps/books/annotations/AnnotationDataParser$4;
.super Ljava/lang/Object;
.source "AnnotationDataParser.java"

# interfaces
.implements Lcom/google/android/apps/books/util/JsonUtils$NodeParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationDataParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
        "<",
        "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;
    .locals 3
    .param p1, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 271
    const-string v2, "text"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, "text":Ljava/lang/String;
    const-string v2, "source"

    invoke-virtual {p1, v2}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAttribution(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$200(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    move-result-object v0

    .line 273
    .local v0, "attribution":Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    return-object v2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 268
    check-cast p1, Lorg/codehaus/jackson/JsonNode;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$4;->apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;

    move-result-object v0

    return-object v0
.end method
