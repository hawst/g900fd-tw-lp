.class public Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;
.super Ljava/lang/Object;
.source "LoadRecommendationsTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LoadRecommendationsTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecommendationsContext"
.end annotation


# instance fields
.field private final mAssociationType:Ljava/lang/String;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "associationType"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->mVolumeId:Ljava/lang/String;

    .line 119
    iput-object p2, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->mAssociationType:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public static forHomePage()Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    new-instance v0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forLastPage(Ljava/lang/String;Z)Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;
    .locals 2
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "isSample"    # Z

    .prologue
    .line 113
    if-eqz p1, :cond_0

    const-string v0, "end-of-sample"

    .line 114
    .local v0, "type":Ljava/lang/String;
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 113
    .end local v0    # "type":Ljava/lang/String;
    :cond_0
    const-string v0, "end-of-volume"

    goto :goto_0
.end method


# virtual methods
.method public getUrl(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 2
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->mVolumeId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 124
    invoke-static {p1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forGetFrontPageRecommendations(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->mVolumeId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->mAssociationType:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forGetRelatedBooks(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    goto :goto_0
.end method

.method public isForHome()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->mVolumeId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
