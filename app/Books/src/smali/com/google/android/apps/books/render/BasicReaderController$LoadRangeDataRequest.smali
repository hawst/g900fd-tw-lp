.class Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadRangeDataRequest"
.end annotation


# instance fields
.field final mDeltaX:I

.field final mDeltaY:I

.field final mHandle:I

.field final mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIIZ)V
    .locals 7
    .param p2, "passageIndex"    # I
    .param p3, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p4, "needSelectionData"    # Z
    .param p5, "handle"    # I
    .param p6, "deltaX"    # I
    .param p7, "deltaY"    # I
    .param p8, "externalRequestId"    # I
    .param p9, "taskRequestId"    # I
    .param p10, "reposted"    # Z

    .prologue
    .line 1163
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p4

    move v4, p8

    move/from16 v5, p9

    move/from16 v6, p10

    .line 1165
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IZIIZ)V

    .line 1166
    iput-object p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 1167
    iput p5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mHandle:I

    .line 1168
    iput p6, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mDeltaX:I

    .line 1169
    iput p7, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mDeltaY:I

    .line 1170
    return-void
.end method


# virtual methods
.method protected createRepostRequest()Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;
    .locals 11

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$300(Lcom/google/android/apps/books/render/BasicReaderController;)I

    move-result v9

    .line 1186
    .local v9, "newRequestId":I
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mPassageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-boolean v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mNeedSelectionData:Z

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mHandle:I

    iget v6, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mDeltaX:I

    iget v7, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mDeltaY:I

    iget v8, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mExternalRequestId:I

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIIZ)V

    return-object v0
.end method

.method protected executeReaderCall()V
    .locals 8

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mTaskRequestId:I

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mPassageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    iget-boolean v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mNeedSelectionData:Z

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mHandle:I

    iget v6, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mDeltaX:I

    iget v7, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequest;->mDeltaY:I

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/render/TextReader;->loadRangeData(IILcom/google/android/apps/books/annotations/TextLocationRange;ZIII)V

    .line 1176
    return-void
.end method
