.class Lcom/google/android/apps/books/data/OffersSubcontroller$4;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "OffersSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/OffersSubcontroller;->getOffersOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;
    invoke-static {v3}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$100(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/app/DeviceInfo;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getOffers(Lcom/google/android/apps/books/app/DeviceInfo;)Lcom/google/android/apps/books/api/data/ApiaryOffers;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->saveServerOffersOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    invoke-static {v1, p1, v2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$500(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_0
    return-void

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    invoke-static {v1, p1, v0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$600(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V

    goto :goto_0
.end method
