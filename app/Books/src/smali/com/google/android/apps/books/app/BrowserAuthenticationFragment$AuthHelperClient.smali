.class Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;
.super Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelperClient;
.source "BrowserAuthenticationFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AuthHelperClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;->this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    invoke-direct {p0}, Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelperClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;-><init>(Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;)V

    return-void
.end method


# virtual methods
.method public finished(Landroid/content/Intent;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 66
    iget-object v2, p0, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;->this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    # invokes: Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->getCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->access$400(Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;->this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->getRequestId(Landroid/os/Bundle;)I
    invoke-static {v3}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->access$300(Landroid/os/Bundle;)I

    move-result v3

    invoke-interface {v2, p1, p2, v3}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->authenticationFinished(Landroid/content/Intent;Ljava/lang/Exception;I)V

    .line 69
    iget-object v2, p0, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;->this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 74
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 76
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v2, p0, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;->this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 83
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 85
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;->this$0:Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method
