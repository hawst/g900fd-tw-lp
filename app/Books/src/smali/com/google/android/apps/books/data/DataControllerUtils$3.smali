.class final Lcom/google/android/apps/books/data/DataControllerUtils$3;
.super Ljava/lang/Object;
.source "DataControllerUtils.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;->getNotificationThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/res/Resources;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/InputStreamSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$bitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$res:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$3;->val$res:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/google/android/apps/books/data/DataControllerUtils$3;->val$bitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;"
    const/4 v10, 0x6

    .line 490
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 491
    const/4 v3, 0x0

    .line 493
    .local v3, "stream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-interface {v7}, Lcom/google/android/apps/books/data/InputStreamSource;->openInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 494
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 495
    .local v4, "thumbnail":Landroid/graphics/Bitmap;
    if-nez v4, :cond_1

    .line 496
    const-string v7, "DataControllerUtils"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 497
    const-string v7, "DataControllerUtils"

    const-string v8, "thumbnail image decoded to null"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    if-eqz v3, :cond_0

    .line 518
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 527
    .end local v3    # "stream":Ljava/io/InputStream;
    .end local v4    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 519
    .restart local v3    # "stream":Ljava/io/InputStream;
    .restart local v4    # "thumbnail":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 520
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "DataControllerUtils"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 521
    const-string v7, "DataControllerUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "can\'t close thumbnail stream:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 501
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_2
    iget-object v7, p0, Lcom/google/android/apps/books/data/DataControllerUtils$3;->val$res:Landroid/content/res/Resources;

    const v8, 0x1050005

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 503
    .local v6, "thumbnailWidth":I
    iget-object v7, p0, Lcom/google/android/apps/books/data/DataControllerUtils$3;->val$res:Landroid/content/res/Resources;

    const v8, 0x1050006

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 505
    .local v5, "thumbnailHeight":I
    new-instance v0, Lcom/google/android/ublib/util/ImageConstraints;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v0, v7, v8}, Lcom/google/android/ublib/util/ImageConstraints;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 507
    .local v0, "constraints":Lcom/google/android/ublib/util/ImageConstraints;
    invoke-static {v4, v0}, Lcom/google/android/apps/books/util/BitmapUtils;->shrinkToConstraints(Landroid/graphics/Bitmap;Lcom/google/android/ublib/util/ImageConstraints;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 509
    .local v2, "reducedThumbnail":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/google/android/apps/books/data/DataControllerUtils$3;->val$bitmapConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v7}, Lcom/google/android/apps/books/util/UiThreadConsumer;->proxyTo(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/apps/books/util/UiThreadConsumer;

    move-result-object v7

    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/util/UiThreadConsumer;->take(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 516
    if-eqz v3, :cond_0

    .line 518
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 519
    :catch_1
    move-exception v1

    .line 520
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "DataControllerUtils"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 521
    const-string v7, "DataControllerUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "can\'t close thumbnail stream:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 511
    .end local v0    # "constraints":Lcom/google/android/ublib/util/ImageConstraints;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "reducedThumbnail":Landroid/graphics/Bitmap;
    .end local v4    # "thumbnail":Landroid/graphics/Bitmap;
    .end local v5    # "thumbnailHeight":I
    .end local v6    # "thumbnailWidth":I
    :catch_2
    move-exception v1

    .line 512
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v7, "DataControllerUtils"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 513
    const-string v7, "DataControllerUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "can\'t open thumbnail stream:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 516
    :cond_2
    if-eqz v3, :cond_0

    .line 518
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 519
    :catch_3
    move-exception v1

    .line 520
    const-string v7, "DataControllerUtils"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 521
    const-string v7, "DataControllerUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "can\'t close thumbnail stream:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 516
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    if-eqz v3, :cond_3

    .line 518
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 523
    :cond_3
    :goto_1
    throw v7

    .line 519
    :catch_4
    move-exception v1

    .line 520
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v8, "DataControllerUtils"

    invoke-static {v8, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 521
    const-string v8, "DataControllerUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "can\'t close thumbnail stream:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 487
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/DataControllerUtils$3;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
