.class public Lcom/google/android/apps/books/service/VolumeSyncer;
.super Ljava/lang/Object;
.source "VolumeSyncer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;
    }
.end annotation


# instance fields
.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

.field private volatile mEncounteredIoException:Z

.field private final mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

.field private final mLocalResourceIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mShouldNotify:Z

.field private mShowedStartNotification:Z

.field private final mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

.field private final mVolumeSyncUi:Lcom/google/android/apps/books/service/SingleVolumeSyncUi;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;Lcom/google/android/apps/books/service/BooksSafeFetchController;Lcom/google/android/apps/books/service/SingleVolumeSyncUi;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 1
    .param p1, "volume"    # Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;
    .param p2, "ensurer"    # Lcom/google/android/apps/books/service/BooksSafeFetchController;
    .param p3, "volumeSyncUi"    # Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    .param p4, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p5, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lcom/google/api/client/util/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mLocalResourceIds:Ljava/util/Set;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mShowedStartNotification:Z

    .line 64
    iput-object p1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolumeSyncUi:Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    .line 67
    iput-object p4, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    iget-object v0, v0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->localData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mShouldNotify:Z

    .line 71
    iput-object p5, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planContentFromManifest(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/service/VolumeSyncer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->planStragglingResources()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/service/VolumeSyncer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mShowedStartNotification:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/service/VolumeSyncer;)Lcom/google/android/apps/books/service/SingleVolumeSyncUi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolumeSyncUi:Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/service/VolumeSyncer;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/service/VolumeSyncer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer;->fetchNewResources(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/util/Collection;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planResources(Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/service/VolumeSyncer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEncounteredIoException:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/service/VolumeSyncer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/service/VolumeSyncer;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEncounteredIoException:Z

    return p1
.end method

.method private downloadVolumeContent(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
    .locals 6
    .param p1, "response"    # Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->numIoExceptions()J

    move-result-wide v2

    .line 100
    .local v2, "numIoExceptionsBefore":J
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    new-instance v4, Lcom/google/android/apps/books/service/VolumeSyncer$1;

    invoke-direct {v4, p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer$1;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->enqueuePlan(Ljava/lang/Runnable;)V

    .line 106
    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->drainEnsurer()V

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    new-instance v4, Lcom/google/android/apps/books/service/VolumeSyncer$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/service/VolumeSyncer$2;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->enqueuePlan(Ljava/lang/Runnable;)V

    .line 132
    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->drainEnsurer()V

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->numIoExceptions()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-nez v1, :cond_2

    const/4 v0, 0x1

    .line 138
    .local v0, "noIoExceptions":Z
    :goto_0
    const-string v1, "VolumeSyncer"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    const-string v1, "VolumeSyncer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sending download complete notification for volume "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_0
    if-nez v0, :cond_1

    .line 143
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->SYNC_FAILURE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    iget-object v4, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->getFirstException()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logBookDownloadFailed(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;Ljava/lang/Throwable;)V

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    new-instance v4, Lcom/google/android/apps/books/service/VolumeSyncer$3;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/books/service/VolumeSyncer$3;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Z)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->enqueuePlan(Ljava/lang/Runnable;)V

    .line 160
    return-void

    .line 137
    .end local v0    # "noIoExceptions":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private drainEnsurer()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->drain()Ljava/util/List;

    move-result-object v0

    .line 165
    .local v0, "exceptions":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/ExecutionException;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 166
    const-string v1, "VolumeSyncer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "VolumeSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending download failure notification for volume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->SYNC_FAILURE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    invoke-static {v2, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logBookDownloadFailed(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;Ljava/lang/Throwable;)V

    .line 174
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolumeSyncUi:Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->finishedVolumeDownload(Z)V

    .line 175
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutionException;

    throw v1

    .line 177
    :cond_1
    return-void
.end method

.method private fetchNewResources(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    new-instance v1, Lcom/google/android/apps/books/service/VolumeSyncer$7;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer$7;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->enqueuePlan(Ljava/lang/Runnable;)V

    .line 341
    :cond_0
    return-void
.end method

.method private getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    iget-object v0, v0, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->volumeData:Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    invoke-virtual {v0}, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private planContentFromManifest(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
    .locals 14
    .param p1, "response"    # Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    .prologue
    const/4 v13, 0x3

    .line 180
    iget-object v5, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    .line 182
    .local v5, "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeManifest;->getPreferredMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v9

    .line 184
    .local v9, "volumeMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    iget-object v10, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    iget-object v10, v10, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->localData:Lcom/google/android/apps/books/model/LocalVolumeData;

    invoke-interface {v10}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v8

    .line 185
    .local v8, "userLastMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-static {v9, v8}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->computeMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v6

    .line 186
    .local v6, "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v1

    .line 187
    .local v1, "hasImageMode":Z
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v2

    .line 190
    .local v2, "hasTextMode":Z
    iget-object v10, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mLocalResourceIds:Ljava/util/Set;

    iget-object v11, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localResourceIds:Ljava/util/Set;

    invoke-interface {v10, v11}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 193
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/books/util/IdentifiableCollection;->getValues()Ljava/util/Collection;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/apps/books/service/VolumeSyncer;->planResources(Ljava/util/Collection;)V

    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "foundContent":Z
    if-eqz v2, :cond_1

    .line 197
    const-string v10, "VolumeSyncer"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 198
    const-string v10, "VolumeSyncer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ensuring text for volume "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_0
    const/4 v0, 0x1

    .line 207
    :cond_1
    iget-object v10, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    iget-object v10, v10, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->downloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    invoke-virtual {v10}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getEpubProgress()Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v7

    .line 209
    .local v7, "textProgress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isFullyDownloaded()Z

    move-result v10

    if-nez v10, :cond_2

    .line 213
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v10

    if-nez v10, :cond_6

    const/4 v3, 0x1

    .line 214
    .local v3, "ignoreSegmentResources":Z
    :goto_0
    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/books/service/VolumeSyncer;->planVolumeSegments(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;Z)V

    .line 216
    .end local v3    # "ignoreSegmentResources":Z
    :cond_2
    if-eqz v1, :cond_4

    sget-object v10, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v6, v10, :cond_4

    .line 217
    const-string v10, "VolumeSyncer"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 218
    const-string v10, "VolumeSyncer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Ensuring pages of volume "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_3
    const/4 v0, 0x1

    .line 221
    iget-object v10, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolume:Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;

    iget-object v10, v10, Lcom/google/android/apps/books/service/SyncPlan$VolumeSyncData;->downloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    invoke-virtual {v10}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getImageProgress()Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v4

    .line 224
    .local v4, "imageProgress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isFullyDownloaded()Z

    move-result v10

    if-nez v10, :cond_4

    .line 225
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planVolumePages(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V

    .line 228
    .end local v4    # "imageProgress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    :cond_4
    if-nez v0, :cond_5

    .line 229
    const-string v10, "VolumeSyncer"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "no content modes available for fetching "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_5
    return-void

    .line 213
    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method private planEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V
    .locals 2
    .param p1, "ensureOperation"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mShouldNotify:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mShowedStartNotification:Z

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mVolumeSyncUi:Lcom/google/android/apps/books/service/SingleVolumeSyncUi;

    invoke-virtual {v0}, Lcom/google/android/apps/books/service/SingleVolumeSyncUi;->startingVolumeDownload()V

    .line 379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mShowedStartNotification:Z

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mEnsurer:Lcom/google/android/apps/books/service/BooksSafeFetchController;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer;->wrapEnsureOperation(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController;->enqueueEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    .line 382
    return-void
.end method

.method private planPageImage(Lcom/google/android/apps/books/model/Page;)V
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "volumeId":Ljava/lang/String;
    const-string v1, "VolumeSyncer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    const-string v1, "VolumeSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enqueuePageImage() vid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/service/VolumeSyncer$4;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer$4;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    .line 263
    return-void
.end method

.method private planPageStructure(Lcom/google/android/apps/books/model/Page;)V
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 267
    .local v0, "volumeId":Ljava/lang/String;
    const-string v1, "VolumeSyncer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    const-string v1, "VolumeSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enqueuePageStructure() vid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/service/VolumeSyncer$5;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer$5;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    .line 281
    return-void
.end method

.method private planResource(Lcom/google/android/apps/books/model/Resource;)V
    .locals 4
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;

    .prologue
    .line 353
    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v0

    .line 354
    .local v0, "resId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mLocalResourceIds:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    :goto_0
    return-void

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mLocalResourceIds:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 358
    const-string v1, "VolumeSyncer"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 359
    const-string v1, "VolumeSyncer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enqueueResource() vid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", rid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/books/util/StringUtils;->summarizeForLogging(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_1
    new-instance v1, Lcom/google/android/apps/books/service/VolumeSyncer$8;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer$8;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/model/Resource;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    goto :goto_0
.end method

.method private planResources(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "resources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Resource;

    .line 345
    .local v1, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planResource(Lcom/google/android/apps/books/model/Resource;)V

    goto :goto_0

    .line 347
    .end local v1    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_0
    return-void
.end method

.method private planSegment(Lcom/google/android/apps/books/model/Segment;Z)V
    .locals 3
    .param p1, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p2, "ignoreResources"    # Z

    .prologue
    .line 300
    const-string v0, "VolumeSyncer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "VolumeSyncer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enqueueSection() vid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/service/VolumeSyncer$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/service/VolumeSyncer$6;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/model/Segment;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/service/VolumeSyncer;->planEnsure(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    .line 316
    return-void
.end method

.method private planStragglingResources()V
    .locals 3

    .prologue
    .line 326
    const-string v0, "VolumeSyncer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const-string v0, "VolumeSyncer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "syncStragglingResources for vid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolumeNetworkResources(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/service/VolumeSyncer;->planResources(Ljava/util/Collection;)V

    .line 330
    return-void
.end method

.method private planVolumePages(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    .prologue
    .line 235
    iget-object v2, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Page;

    .line 236
    .local v1, "page":Lcom/google/android/apps/books/model/Page;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 237
    iget-object v2, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localPageIds:Ljava/util/Set;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 238
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planPageImage(Lcom/google/android/apps/books/model/Page;)V

    .line 240
    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localStructureIds:Ljava/util/Set;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 241
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->planPageStructure(Lcom/google/android/apps/books/model/Page;)V

    goto :goto_0

    .line 245
    .end local v1    # "page":Lcom/google/android/apps/books/model/Page;
    :cond_2
    return-void
.end method

.method private planVolumeSegments(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;Z)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    .param p2, "ignoreResources"    # Z

    .prologue
    .line 292
    iget-object v2, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Segment;

    .line 293
    .local v1, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->localSegmentIds:Ljava/util/Set;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 294
    invoke-direct {p0, v1, p2}, Lcom/google/android/apps/books/service/VolumeSyncer;->planSegment(Lcom/google/android/apps/books/model/Segment;Z)V

    goto :goto_0

    .line 297
    .end local v1    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_1
    return-void
.end method

.method private wrapEnsureOperation(Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;
    .locals 1
    .param p1, "operation"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    .prologue
    .line 421
    new-instance v0, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;-><init>(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V

    return-object v0
.end method


# virtual methods
.method public downloadVolumeContent()Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/service/VolumeSyncer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/DataControllerUtils;->getVolumeManifest(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Ljava/util/Set;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    move-result-object v6

    .line 87
    .local v6, "response":Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/service/VolumeSyncer;->downloadVolumeContent(Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V

    .line 88
    new-instance v0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    invoke-direct {p0}, Lcom/google/android/apps/books/service/VolumeSyncer;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v6, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;->manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
