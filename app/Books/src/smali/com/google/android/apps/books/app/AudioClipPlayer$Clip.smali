.class public Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Clip"
.end annotation


# instance fields
.field public final batchId:I

.field public final endTime:I

.field public final index:I

.field private mResource:Lcom/google/android/apps/books/model/Resource;

.field public final startTime:I


# direct methods
.method public constructor <init>(IIIILcom/google/android/apps/books/model/Resource;)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "index"    # I
    .param p4, "batchId"    # I
    .param p5, "resource"    # Lcom/google/android/apps/books/model/Resource;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    .line 70
    iput p2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    .line 71
    iput p3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->index:I

    .line 72
    iput p4, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->batchId:I

    .line 73
    iput-object p5, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    .line 74
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)Lcom/google/android/apps/books/model/Resource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    return-object v0
.end method


# virtual methods
.method public duration()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    iget v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 94
    instance-of v2, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 95
    check-cast v0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .line 96
    .local v0, "clip":Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    iget v2, v0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    iget v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    iget v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    invoke-static {v2, v3}, Lcom/google/android/apps/books/model/ResourceUtils;->equals(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/Resource;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 99
    .end local v0    # "clip":Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 104
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isSilentClip()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeClipSilent()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    .line 119
    return-void
.end method

.method public sameAudioSource(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)Z
    .locals 2
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 88
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->isSilentClip()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/model/ResourceUtils;->equals(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/Resource;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->isSilentClip()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "Silent Clip"

    .line 110
    .local v0, "resourceId":Ljava/lang/String;
    :goto_0
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "resourceId"

    invoke-virtual {v1, v2, v0}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "startTime"

    iget v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "endTime"

    iget v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 109
    .end local v0    # "resourceId":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
