.class Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;
.super Ljava/lang/Object;
.source "ImageModeRenderer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;Lcom/google/android/apps/books/util/ExceptionOr;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

.field final synthetic val$page:Lcom/google/android/apps/books/util/ExceptionOr;

.field final synthetic val$structure:Lcom/google/android/apps/books/util/ExceptionOr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;Lcom/google/android/apps/books/util/ExceptionOr;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0

    .prologue
    .line 782
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    iput-object p2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->val$page:Lcom/google/android/apps/books/util/ExceptionOr;

    iput-object p3, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->val$structure:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 785
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    iget-object v0, v0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    iget-object v1, v1, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->canAbandon(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$900(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 786
    const-string v0, "ImageModeRenderer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    const-string v0, "ImageModeRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Abandoning request before consuming content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    iget-object v2, v2, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    iget-object v2, v2, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->val$page:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 795
    :goto_0
    return-void

    .line 794
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    iget-object v2, v0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->this$1:Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;

    iget-object v3, v0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->val$page:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;->val$structure:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->handleRequestLoadResults(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/io/InputStream;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V
    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$1100(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/io/InputStream;Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;)V

    goto :goto_0
.end method
