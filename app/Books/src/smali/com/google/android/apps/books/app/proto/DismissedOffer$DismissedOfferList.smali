.class public final Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DismissedOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/proto/DismissedOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DismissedOfferList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;


# instance fields
.field private dismissedOffers_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 557
    new-instance v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    .line 558
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedOffer;->internalForceInit()V

    .line 559
    sget-object v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->initFields()V

    .line 560
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 285
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->dismissedOffers_:Ljava/util/List;

    .line 309
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->memoizedSerializedSize:I

    .line 270
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->initFields()V

    .line 271
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/proto/DismissedOffer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedOffer$1;

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 285
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->dismissedOffers_:Ljava/util/List;

    .line 309
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->memoizedSerializedSize:I

    .line 272
    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->dismissedOffers_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->dismissedOffers_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 296
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;
    .locals 1

    .prologue
    .line 390
    # invokes: Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->create()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->access$600()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->newBuilder()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;

    # invokes: Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->buildParsed()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->access$500(Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;)Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDismissedOffersList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->dismissedOffers_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 311
    iget v2, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->memoizedSerializedSize:I

    .line 312
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 320
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 314
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->getDismissedOffersList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;

    .line 316
    .local v0, "element":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 318
    goto :goto_1

    .line 319
    .end local v0    # "element":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    :cond_1
    iput v2, p0, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->memoizedSerializedSize:I

    move v3, v2

    .line 320
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->getSerializedSize()I

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->getDismissedOffersList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;

    .line 305
    .local v0, "element":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 307
    .end local v0    # "element":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    :cond_0
    return-void
.end method
