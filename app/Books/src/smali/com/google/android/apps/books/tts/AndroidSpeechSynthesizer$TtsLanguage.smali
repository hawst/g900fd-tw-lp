.class public interface abstract Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;
.super Ljava/lang/Object;
.source "AndroidSpeechSynthesizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TtsLanguage"
.end annotation


# virtual methods
.method public abstract getLanguage()Ljava/util/Locale;
.end method

.method public abstract isLanguageAvailable(Ljava/util/Locale;)I
.end method
