.class public abstract Lcom/google/android/apps/books/app/ApiaryLoadTask;
.super Landroid/os/AsyncTask;
.source "ApiaryLoadTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ApiaryType:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TResult;>;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mApiaryClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TApiaryType;>;"
        }
    .end annotation
.end field

.field private final mApplication:Lcom/google/android/apps/books/app/BooksApplication;

.field private final mConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<TResult;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/Class;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            "Ljava/lang/Class",
            "<TApiaryType;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TResult;>;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    .local p3, "apiaryClass":Ljava/lang/Class;, "Ljava/lang/Class<TApiaryType;>;"
    .local p4, "resultConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TResult;>;"
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/android/apps/books/app/ApiaryLoadTask;-><init>(Lcom/google/android/apps/books/app/BooksApplication;Landroid/accounts/Account;Ljava/lang/Class;Lcom/google/android/ublib/utils/Consumer;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/app/BooksApplication;Landroid/accounts/Account;Ljava/lang/Class;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p1, "app"    # Lcom/google/android/apps/books/app/BooksApplication;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/app/BooksApplication;",
            "Landroid/accounts/Account;",
            "Ljava/lang/Class",
            "<TApiaryType;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TResult;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    .local p3, "apiaryClass":Ljava/lang/Class;, "Ljava/lang/Class<TApiaryType;>;"
    .local p4, "resultConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TResult;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mApplication:Lcom/google/android/apps/books/app/BooksApplication;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mAccount:Landroid/accounts/Account;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mApiaryClass:Ljava/lang/Class;

    .line 35
    iput-object p4, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 36
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ApiaryLoadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")TResult;"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    const-string v5, "ApiaryLoadTask"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    .line 57
    .local v3, "isDebugLoggable":Z
    iget-object v5, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mApplication:Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/BooksApplication;->getApiaryClient()Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v1

    .line 59
    .local v1, "client":Lcom/google/android/apps/books/api/ApiaryClient;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mApplication:Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v5}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/ApiaryLoadTask;->getUrl(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v5

    invoke-interface {v1, v5}, Lcom/google/android/apps/books/api/ApiaryClient;->makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v4

    .line 61
    .local v4, "request":Lcom/google/api/client/http/HttpRequest;
    if-eqz v3, :cond_0

    .line 62
    const-string v5, "ApiaryLoadTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "executing fetch of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/api/client/http/HttpRequest;->getUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mApiaryClass:Ljava/lang/Class;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ApiaryLoadTask;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [I

    invoke-interface {v1, v4, v5, v6, v7}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v0

    .line 65
    .local v0, "apiaryResult":Ljava/lang/Object;, "TApiaryType;"
    if-eqz v3, :cond_1

    .line 66
    const-string v5, "ApiaryLoadTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "successful fetch of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ApiaryLoadTask;->process(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 73
    .end local v0    # "apiaryResult":Ljava/lang/Object;, "TApiaryType;"
    .end local v4    # "request":Lcom/google/api/client/http/HttpRequest;
    :goto_0
    return-object v5

    .line 69
    :catch_0
    move-exception v2

    .line 70
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "ApiaryLoadTask"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 71
    const-string v5, "ApiaryLoadTask"

    const-string v6, "exception on fetch"

    invoke-static {v5, v6, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/ApiaryLoadTask;->process(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    goto :goto_0
.end method

.method protected getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 87
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method protected getBooksApplication()Lcom/google/android/apps/books/app/BooksApplication;
    .locals 1

    .prologue
    .line 83
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mApplication:Lcom/google/android/apps/books/app/BooksApplication;

    return-object v0
.end method

.method protected abstract getUrl(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/google/android/apps/books/app/ApiaryLoadTask;, "Lcom/google/android/apps/books/app/ApiaryLoadTask<TApiaryType;TResult;>;"
    .local p1, "result":Ljava/lang/Object;, "TResult;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ApiaryLoadTask;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method protected abstract process(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TApiaryType;)TResult;"
        }
    .end annotation
.end method
