.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HomeAccountAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

.field public static final enum HOME_ACCOUNT_CHANGE_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

.field public static final enum HOME_ACCOUNT_LAUNCH_ACCOUNTS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

.field public static final enum HOME_ACCOUNT_PROMPT_ACCOUNT_CHOICE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

.field public static final enum HOME_ACCOUNT_RESELECT_SAME_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 404
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    const-string v1, "HOME_ACCOUNT_PROMPT_ACCOUNT_CHOICE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_PROMPT_ACCOUNT_CHOICE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    .line 408
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    const-string v1, "HOME_ACCOUNT_LAUNCH_ACCOUNTS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_LAUNCH_ACCOUNTS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    .line 412
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    const-string v1, "HOME_ACCOUNT_CHANGE_ACCOUNT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_CHANGE_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    .line 416
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    const-string v1, "HOME_ACCOUNT_RESELECT_SAME_ACCOUNT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_RESELECT_SAME_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    .line 400
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_PROMPT_ACCOUNT_CHOICE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_LAUNCH_ACCOUNTS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_CHANGE_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->HOME_ACCOUNT_RESELECT_SAME_ACCOUNT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 400
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 400
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;
    .locals 1

    .prologue
    .line 400
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeAccountAction;

    return-object v0
.end method
