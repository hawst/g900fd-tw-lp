.class Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;
.super Ljava/lang/Object;
.source "SentenceSearchTextProcessor.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OnlyMatchingSentence"
.end annotation


# instance fields
.field private mCurrentMatchingSentenceIsIncomplete:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;->mCurrentMatchingSentenceIsIncomplete:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;-><init>()V

    return-void
.end method


# virtual methods
.method public needsMoreSentencesToFinishCurrentMatch()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;->mCurrentMatchingSentenceIsIncomplete:Z

    return v0
.end method

.method public onSentence(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;Ljava/util/Collection;Z)V
    .locals 3
    .param p1, "sentence"    # Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    .param p3, "endOfDocument"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p2, "queue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 160
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;->mCurrentMatchingSentenceIsIncomplete:Z

    .line 161
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->hasMatch()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->isComplete()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_2

    .line 163
    :cond_0
    new-array v0, v2, [Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->buildSearchResult([Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;)Lcom/google/android/apps/books/model/SearchResult;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_1
    :goto_0
    return-void

    .line 165
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;->mCurrentMatchingSentenceIsIncomplete:Z

    goto :goto_0
.end method
