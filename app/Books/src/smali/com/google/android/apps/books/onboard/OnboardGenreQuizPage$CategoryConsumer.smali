.class Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;
.super Ljava/lang/Object;
.source "OnboardGenreQuizPage.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CategoryConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;
    .param p2, "x1"    # Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$1;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;)V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "listExceptionOr":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;>;>;"
    const/4 v8, 0x5

    .line 77
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 78
    const-string v7, "OnboardGenreQuizPage"

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 79
    const-string v7, "OnboardGenreQuizPage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "categoriesConsumer take failed:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v7}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->handleNoConnection()V

    .line 84
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v7}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->exitOnboardFlow()V

    .line 114
    :goto_0
    return-void

    .line 86
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v0, "datasWithoutIconUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/libraries/bind/data/Data;>;"
    const/4 v4, 0x0

    .line 88
    .local v4, "serverOrderIndex":I
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 90
    .local v2, "sampleCategories":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;>;"
    if-nez v2, :cond_3

    .line 92
    const-string v7, "OnboardGenreQuizPage"

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 93
    const-string v7, "OnboardGenreQuizPage"

    const-string v8, "genres list is null"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v7}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->handleNoConnection()V

    .line 96
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v7}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->exitOnboardFlow()V

    goto :goto_0

    .line 99
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;

    .line 100
    .local v3, "sampleCategory":Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;
    iget-object v7, v3, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->badgeUrl:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 101
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    iget-object v7, v7, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mController:Lcom/google/android/apps/books/app/OnboardingController;

    iget-object v8, v3, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->badgeUrl:Ljava/lang/String;

    new-instance v9, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;

    iget-object v10, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "serverOrderIndex":I
    .local v5, "serverOrderIndex":I
    invoke-direct {v9, v10, v3, v4}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;I)V

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/books/app/OnboardingController;->getCategoryImage(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    move v4, v5

    .end local v5    # "serverOrderIndex":I
    .restart local v4    # "serverOrderIndex":I
    goto :goto_1

    .line 104
    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    iget-object v8, v3, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->categoryId:Ljava/lang/String;

    iget-object v9, v3, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->name:Ljava/lang/String;

    const/4 v10, 0x0

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "serverOrderIndex":I
    .restart local v5    # "serverOrderIndex":I
    # invokes: Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v7, v8, v9, v10, v4}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->access$000(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v5

    .end local v5    # "serverOrderIndex":I
    .restart local v4    # "serverOrderIndex":I
    goto :goto_1

    .line 109
    .end local v3    # "sampleCategory":Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;
    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    iget-object v7, v7, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v7}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-static {v7}, Lcom/google/android/play/utils/collections/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v6

    .line 111
    .local v6, "snapshotList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/libraries/bind/data/Data;>;"
    invoke-interface {v6, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v7, v6}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->serverOrderSortAndUpdateSnapshot(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 72
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
