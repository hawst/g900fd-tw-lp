.class Lcom/google/android/apps/books/widget/MyLibraryHomeView$3;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "MyLibraryHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$3;->this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocalVolumeData(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 302
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$3;->this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    # getter for: Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->access$100(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 303
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/model/BooksDataListener;->onLocalVolumeData(Ljava/util/Map;)V

    goto :goto_0

    .line 305
    .end local v0    # "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    :cond_0
    return-void
.end method

.method public onVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$3;->this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    # getter for: Lcom/google/android/apps/books/widget/MyLibraryHomeView;->mFilterToAdapter:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->access$100(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 296
    .local v0, "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeDownloadProgress(Ljava/util/Map;)V

    goto :goto_0

    .line 298
    .end local v0    # "adapter":Lcom/google/android/apps/books/widget/BooksCardsAdapter;
    :cond_0
    return-void
.end method
