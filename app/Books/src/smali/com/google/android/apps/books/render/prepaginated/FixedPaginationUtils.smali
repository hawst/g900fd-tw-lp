.class public Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;
.super Ljava/lang/Object;
.source "FixedPaginationUtils.java"


# direct methods
.method public static getScreenPageDifference(Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;IIII)I
    .locals 5
    .param p0, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p1, "bookPageIndex1"    # I
    .param p2, "screenPageOffset1"    # I
    .param p3, "bookPageIndex2"    # I
    .param p4, "screenPageOffset2"    # I

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getLastViewableBookPageIndex()I

    move-result v0

    .line 90
    .local v0, "lastViewableBookPageIndex":I
    if-gez p1, :cond_2

    .line 91
    add-int/lit8 p2, p2, -0x1

    .line 92
    const/4 p1, 0x0

    .line 97
    :cond_0
    :goto_0
    if-gez p3, :cond_3

    .line 98
    add-int/lit8 p4, p4, -0x1

    .line 99
    const/4 p3, 0x0

    .line 104
    :cond_1
    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToScreenPageIndex(I)I

    move-result v1

    .line 105
    .local v1, "p1ScreenPageIndex":I
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToScreenPageIndex(I)I

    move-result v2

    .line 106
    .local v2, "p2ScreenPageIndex":I
    sub-int v3, v1, v2

    sub-int v4, p2, p4

    add-int/2addr v3, v4

    return v3

    .line 93
    .end local v1    # "p1ScreenPageIndex":I
    .end local v2    # "p2ScreenPageIndex":I
    :cond_2
    if-le p1, v0, :cond_0

    .line 94
    add-int/lit8 p2, p2, 0x1

    .line 95
    move p1, v0

    goto :goto_0

    .line 100
    :cond_3
    if-le p3, v0, :cond_1

    .line 101
    add-int/lit8 p4, p4, 0x1

    .line 102
    move p3, v0

    goto :goto_1
.end method

.method public static getScreenPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/common/Position;)I
    .locals 4
    .param p0, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p1, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual {p2}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0, v3}, Lcom/google/android/apps/books/common/Position$PageOrdering;->getPageIndex(Ljava/lang/String;)I

    move-result v0

    .line 72
    .local v0, "bookPageIndex":I
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getLastViewableBookPageIndex()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 74
    .local v1, "clampedBookPageIndex":I
    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToScreenPageIndex(I)I

    move-result v2

    .line 75
    .local v2, "result":I
    if-ge v1, v0, :cond_0

    .line 76
    add-int/lit8 v2, v2, 0x1

    .line 78
    :cond_0
    return v2
.end method

.method public static getScreenSpreadDifference(Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;IIIIZ)I
    .locals 9
    .param p0, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p1, "bookPageIndex1"    # I
    .param p2, "spreadOffset1"    # I
    .param p3, "bookPageIndex2"    # I
    .param p4, "spreadOffset2"    # I
    .param p5, "twoPageSpreads"    # Z

    .prologue
    const/4 v8, 0x2

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getLastViewableBookPageIndex()I

    move-result v0

    .line 121
    .local v0, "lastViewableBookPageIndex":I
    const/4 v5, 0x0

    .line 122
    .local v5, "screenPageOffset1":I
    const/4 v6, 0x0

    .line 123
    .local v6, "screenPageOffset2":I
    if-gez p1, :cond_2

    .line 124
    add-int/lit8 v5, v5, -0x1

    .line 125
    const/4 p1, 0x0

    .line 130
    :cond_0
    :goto_0
    if-gez p3, :cond_3

    .line 131
    add-int/lit8 v6, v6, -0x1

    .line 132
    const/4 p3, 0x0

    .line 137
    :cond_1
    :goto_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToScreenPageIndex(I)I

    move-result v7

    add-int v1, v7, v5

    .line 139
    .local v1, "p1ScreenPageIndex":I
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->bookPageIndexToScreenPageIndex(I)I

    move-result v7

    add-int v3, v7, v6

    .line 142
    .local v3, "p2ScreenPageIndex":I
    if-eqz p5, :cond_4

    .line 143
    invoke-static {v1, v8}, Lcom/google/android/apps/books/util/MathUtils;->divideRoundingDown(II)I

    move-result v2

    .line 144
    .local v2, "p1SpreadIndex":I
    invoke-static {v3, v8}, Lcom/google/android/apps/books/util/MathUtils;->divideRoundingDown(II)I

    move-result v4

    .line 149
    .local v4, "p2SpreadIndex":I
    :goto_2
    sub-int v7, v2, v4

    sub-int v8, p2, p4

    add-int/2addr v7, v8

    return v7

    .line 126
    .end local v1    # "p1ScreenPageIndex":I
    .end local v2    # "p1SpreadIndex":I
    .end local v3    # "p2ScreenPageIndex":I
    .end local v4    # "p2SpreadIndex":I
    :cond_2
    if-le p1, v0, :cond_0

    .line 127
    add-int/lit8 v5, v5, 0x1

    .line 128
    move p1, v0

    goto :goto_0

    .line 133
    :cond_3
    if-le p3, v0, :cond_1

    .line 134
    add-int/lit8 v6, v6, 0x1

    .line 135
    move p3, v0

    goto :goto_1

    .line 146
    .restart local v1    # "p1ScreenPageIndex":I
    .restart local v3    # "p2ScreenPageIndex":I
    :cond_4
    move v2, v1

    .line 147
    .restart local v2    # "p1SpreadIndex":I
    move v4, v3

    .restart local v4    # "p2SpreadIndex":I
    goto :goto_2
.end method

.method public static spreadIdentifierToPageIdentifiers(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/SpreadIdentifier;Z[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 7
    .param p0, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p1, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p2, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p3, "twoPageSpreads"    # Z
    .param p4, "result"    # [Lcom/google/android/apps/books/render/PageIdentifier;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 23
    iget-object v2, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    .line 24
    .local v2, "position":Lcom/google/android/apps/books/common/Position;
    invoke-static {p0, p1, v2}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/common/Position;)I

    move-result v3

    .line 25
    .local v3, "positionScreenPageIndex":I
    if-eqz p3, :cond_0

    div-int/lit8 v4, v3, 0x2

    .line 27
    .local v4, "spreadIndex":I
    :goto_0
    iget v5, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int v1, v4, v5

    .line 28
    .local v1, "offsetSpreadIndex":I
    if-eqz p3, :cond_1

    .line 29
    mul-int/lit8 v5, v1, 0x2

    sub-int v0, v5, v3

    .line 30
    .local v0, "firstPageOffset":I
    invoke-static {v2, v0}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v5

    aput-object v5, p4, v6

    .line 31
    const/4 v5, 0x1

    add-int/lit8 v6, v0, 0x1

    invoke-static {v2, v6}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    aput-object v6, p4, v5

    .line 36
    .end local v0    # "firstPageOffset":I
    :goto_1
    return-object p4

    .end local v1    # "offsetSpreadIndex":I
    .end local v4    # "spreadIndex":I
    :cond_0
    move v4, v3

    .line 25
    goto :goto_0

    .line 33
    .restart local v1    # "offsetSpreadIndex":I
    .restart local v4    # "spreadIndex":I
    :cond_1
    sub-int v5, v1, v3

    invoke-static {v2, v5}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v5

    aput-object v5, p4, v6

    goto :goto_1
.end method

.method public static spreadPageIdentifierToPageIdentifier(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 7
    .param p0, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p1, "pageMap"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    .param p2, "spreadPageIdentifier"    # Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .param p3, "twoPageSpreads"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v6, p2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v2, v6, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    .line 52
    .local v2, "position":Lcom/google/android/apps/books/common/Position;
    invoke-static {p0, p1, v2}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationUtils;->getScreenPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;Lcom/google/android/apps/books/common/Position;)I

    move-result v3

    .line 53
    .local v3, "positionScreenPageIndex":I
    if-eqz p3, :cond_0

    div-int/lit8 v4, v3, 0x2

    .line 55
    .local v4, "positionSpreadIndex":I
    :goto_0
    iget-object v6, p2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget v6, v6, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int v1, v4, v6

    .line 57
    .local v1, "offsetSpreadIndex":I
    if-eqz p3, :cond_1

    mul-int/lit8 v0, v1, 0x2

    .line 59
    .local v0, "firstPageInSpreadScreenPageIndex":I
    :goto_1
    iget v6, p2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    add-int v5, v0, v6

    .line 61
    .local v5, "resultScreenPageIndex":I
    sub-int v6, v5, v3

    invoke-static {v2, v6}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    return-object v6

    .end local v0    # "firstPageInSpreadScreenPageIndex":I
    .end local v1    # "offsetSpreadIndex":I
    .end local v4    # "positionSpreadIndex":I
    .end local v5    # "resultScreenPageIndex":I
    :cond_0
    move v4, v3

    .line 53
    goto :goto_0

    .restart local v1    # "offsetSpreadIndex":I
    .restart local v4    # "positionSpreadIndex":I
    :cond_1
    move v0, v1

    .line 57
    goto :goto_1
.end method
