.class final enum Lcom/google/android/apps/books/app/LibraryComparator$2;
.super Lcom/google/android/apps/books/app/LibraryComparator;
.source "LibraryComparator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LibraryComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V
    .locals 6
    .param p3, "x0"    # I
    .param p4, "x1"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    .prologue
    .line 32
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/LibraryComparator;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;Lcom/google/android/apps/books/app/LibraryComparator$1;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    .locals 1
    .param p1, "lhs"    # Lcom/google/android/apps/books/widget/CardData;
    .param p2, "rhs"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 36
    # invokes: Lcom/google/android/apps/books/app/LibraryComparator;->compareByTitle(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    invoke-static {p1, p2}, Lcom/google/android/apps/books/app/LibraryComparator;->access$100(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I

    move-result v0

    .line 37
    .local v0, "firstOrderResult":I
    if-eqz v0, :cond_0

    .end local v0    # "firstOrderResult":I
    :goto_0
    return v0

    .restart local v0    # "firstOrderResult":I
    :cond_0
    # invokes: Lcom/google/android/apps/books/app/LibraryComparator;->compareByAuthor(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I
    invoke-static {p1, p2}, Lcom/google/android/apps/books/app/LibraryComparator;->access$200(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/apps/books/widget/CardData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/widget/CardData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/LibraryComparator$2;->compare(Lcom/google/android/apps/books/widget/CardData;Lcom/google/android/apps/books/widget/CardData;)I

    move-result v0

    return v0
.end method
