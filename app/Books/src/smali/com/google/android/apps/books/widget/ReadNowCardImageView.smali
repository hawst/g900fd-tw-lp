.class public Lcom/google/android/apps/books/widget/ReadNowCardImageView;
.super Lcom/google/android/apps/books/widget/BannerCardImageView;
.source "ReadNowCardImageView.java"


# instance fields
.field private mMenuView:Landroid/view/View;

.field private mPinView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BannerCardImageView;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/BannerCardImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lcom/google/android/apps/books/widget/BannerCardImageView;->onFinishInflate()V

    .line 30
    const v0, 0x7f0e00e2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mPinView:Landroid/view/View;

    .line 31
    const v0, 0x7f0e00e1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mMenuView:Landroid/view/View;

    .line 32
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 43
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/books/widget/BannerCardImageView;->onLayout(ZIIII)V

    .line 44
    sub-int v6, p4, p2

    .line 45
    .local v6, "width":I
    sub-int v0, p5, p3

    .line 48
    .local v0, "height":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mPinView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 49
    .local v4, "pinParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v6, v7

    .line 50
    .local v5, "pinRight":I
    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v0, v7

    .line 51
    .local v3, "pinBottom":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mPinView:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mPinView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v5, v8

    iget-object v9, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mPinView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    sub-int v9, v3, v9

    invoke-virtual {v7, v8, v9, v5, v3}, Landroid/view/View;->layout(IIII)V

    .line 55
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mMenuView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 56
    .local v1, "menuParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v2, v6, v7

    .line 57
    .local v2, "menuRight":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mMenuView:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mMenuView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v2, v8

    iget v9, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v10, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v11, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mMenuView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v7, v8, v9, v2, v10}, Landroid/view/View;->layout(IIII)V

    .line 59
    return-void
.end method

.method public onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 36
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/widget/BannerCardImageView;->onMeasure(II)V

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowCardImageView;->mPinView:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 39
    return-void
.end method
