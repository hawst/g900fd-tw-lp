.class Lcom/google/android/apps/books/widget/PassageHighlightRects$1;
.super Ljava/lang/Object;
.source "PassageHighlightRects.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/PassageHighlightRects;->getWalker()Lcom/google/android/apps/books/widget/Walker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
        ">;"
    }
.end annotation


# instance fields
.field private mColorIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentColor:I

.field private final mHighlightRects:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field private mServingSelected:Z

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PassageHighlightRects;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PassageHighlightRects;)V
    .locals 1

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->this$0:Lcom/google/android/apps/books/widget/PassageHighlightRects;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mServingSelected:Z

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    .line 90
    new-instance v0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1$1;-><init>(Lcom/google/android/apps/books/widget/PassageHighlightRects$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mHighlightRects:Lcom/google/android/apps/books/widget/Walker;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/PassageHighlightRects$1;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PassageHighlightRects$1;

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mCurrentColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/PassageHighlightRects$1;)Lcom/google/common/collect/SetMultimap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PassageHighlightRects$1;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->currentMap()Lcom/google/common/collect/SetMultimap;

    move-result-object v0

    return-object v0
.end method

.method private currentMap()Lcom/google/common/collect/SetMultimap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/SetMultimap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mServingSelected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->this$0:Lcom/google/android/apps/books/widget/PassageHighlightRects;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToSelectedRects:Lcom/google/common/collect/SetMultimap;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->this$0:Lcom/google/android/apps/books/widget/PassageHighlightRects;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToRects:Lcom/google/common/collect/SetMultimap;

    goto :goto_0
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Z
    .locals 4
    .param p1, "item"    # Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .prologue
    const/4 v1, 0x1

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    if-nez v0, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->currentMap()Lcom/google/common/collect/SetMultimap;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/SetMultimap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mCurrentColor:I

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mHighlightRects:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 126
    iget v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mCurrentColor:I

    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mServingSelected:Z

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mHighlightRects:Lcom/google/android/apps/books/widget/Walker;

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->set(IZLcom/google/android/apps/books/widget/Walker;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    move v0, v1

    .line 135
    :goto_0
    return v0

    .line 129
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mServingSelected:Z

    if-nez v0, :cond_2

    .line 130
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mServingSelected:Z

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    .line 133
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->next(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Z

    move-result v0

    goto :goto_0

    .line 135
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 85
    check-cast p1, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->next(Lcom/google/android/apps/books/widget/HighlightsSharingColor;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mServingSelected:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;->mColorIterator:Ljava/util/Iterator;

    .line 116
    return-void
.end method
