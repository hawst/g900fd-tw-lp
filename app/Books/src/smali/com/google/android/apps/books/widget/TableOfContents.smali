.class public Lcom/google/android/apps/books/widget/TableOfContents;
.super Ljava/lang/Object;
.source "TableOfContents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/TableOfContents$1;,
        Lcom/google/android/apps/books/widget/TableOfContents$ViewCallbacks;,
        Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;,
        Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;,
        Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;
    }
.end annotation


# instance fields
.field private mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

.field mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

.field mCallbacks:Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;

.field private mContentsView:Lcom/google/android/apps/books/app/ContentsView;

.field mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private mMyPopupSelector:Lcom/google/android/ublib/widget/Dropup;

.field private mNavigator:Lcom/google/android/apps/books/util/Navigator;

.field mPosition:Lcom/google/android/apps/books/common/Position;

.field mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

.field private mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field private final mViewCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    new-instance v0, Lcom/google/android/apps/books/widget/TableOfContents$ViewCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/TableOfContents$ViewCallbacks;-><init>(Lcom/google/android/apps/books/widget/TableOfContents;Lcom/google/android/apps/books/widget/TableOfContents$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mViewCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .line 355
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/TableOfContents;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TableOfContents;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "x2"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/TableOfContents;->navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/TableOfContents;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TableOfContents;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/TableOfContents;->onAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;)V

    return-void
.end method

.method private makeContentsViewArguments()Lcom/google/android/apps/books/app/ContentsView$Arguments;
    .locals 6

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mPosition:Lcom/google/android/apps/books/common/Position;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ContentsView$Arguments;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Lcom/google/android/apps/books/render/ReaderSettings;)V

    return-object v0
.end method

.method private maybeEnableAndUnHideActionItem()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    if-eqz v2, :cond_0

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 197
    .local v0, "enable":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->setEnabled(Z)V

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->setVisibility(I)V

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->invalidate()V

    .line 201
    .end local v0    # "enable":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 196
    goto :goto_0

    .line 198
    .restart local v0    # "enable":Z
    :cond_2
    const/4 v1, 0x4

    goto :goto_1
.end method

.method private navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 1
    .param p1, "readingPos"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "action"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/TableOfContents;->navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 303
    return-void
.end method

.method private navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 3
    .param p1, "readingPos"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "action"    # Lcom/google/android/apps/books/app/MoveType;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mNavigator:Lcom/google/android/apps/books/util/Navigator;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mNavigator:Lcom/google/android/apps/books/util/Navigator;

    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/apps/books/util/Navigator;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents;->dismiss()V

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    const-string v0, "TOCActionItem"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    const-string v0, "TOCActionItem"

    const-string v1, "missing NavigationListener"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 297
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 298
    .local v0, "readingPos":Lcom/google/android/apps/books/common/Position;
    sget-object v1, Lcom/google/android/apps/books/app/MoveType;->CHOSE_TOC_ANNOTATION:Lcom/google/android/apps/books/app/MoveType;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/apps/books/widget/TableOfContents;->navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 299
    return-void
.end method


# virtual methods
.method protected canShowPopup()Z
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v0, :cond_1

    .line 244
    const-string v0, "TOCActionItem"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    const-string v0, "TOCActionItem"

    const-string v1, "missing volumemetadata when trying to show chapterlist"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_0
    const/4 v0, 0x0

    .line 249
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMyPopupSelector:Lcom/google/android/ublib/widget/Dropup;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/Dropup;->dismiss()V

    .line 349
    :cond_0
    return-void
.end method

.method protected isShowing()Z
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMyPopupSelector:Lcom/google/android/ublib/widget/Dropup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMyPopupSelector:Lcom/google/android/ublib/widget/Dropup;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/Dropup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needsActionItem()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mContentsView:Lcom/google/android/apps/books/app/ContentsView;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mContentsView:Lcom/google/android/apps/books/app/ContentsView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ContentsView;->onDestroy()V

    .line 372
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mContentsView:Lcom/google/android/apps/books/app/ContentsView;

    .line 374
    :cond_0
    return-void
.end method

.method public setActionItem(Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;)V
    .locals 1
    .param p1, "actionItem"    # Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    .prologue
    .line 208
    const-string v0, "missing actionItem"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    if-nez v0, :cond_0

    .line 211
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    .line 212
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TableOfContents;->maybeEnableAndUnHideActionItem()V

    .line 214
    :cond_0
    return-void
.end method

.method public setNavigator(Lcom/google/android/apps/books/util/Navigator;)V
    .locals 0
    .param p1, "navigator"    # Lcom/google/android/apps/books/util/Navigator;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mNavigator:Lcom/google/android/apps/books/util/Navigator;

    .line 171
    return-void
.end method

.method public setPosition(Lcom/google/android/apps/books/common/Position;)V
    .locals 0
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mPosition:Lcom/google/android/apps/books/common/Position;

    .line 222
    return-void
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 0
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mReadingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 218
    return-void
.end method

.method public setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "annotationSet"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;

    .prologue
    .line 182
    if-eqz p1, :cond_1

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v0, :cond_0

    .line 184
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 185
    iput-object p2, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .line 190
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TableOfContents;->maybeEnableAndUnHideActionItem()V

    .line 191
    iput-object p3, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mCallbacks:Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;

    .line 192
    return-void

    .line 188
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    goto :goto_0
.end method

.method public show(Landroid/view/View;)V
    .locals 2
    .param p1, "anchor"    # Landroid/view/View;

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/TableOfContents;->canShowPopup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mCallbacks:Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TableOfContents;->makeContentsViewArguments()Lcom/google/android/apps/books/app/ContentsView$Arguments;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/TableOfContents$Callbacks;->startTableOfContentsActivity(Lcom/google/android/apps/books/app/ContentsView$Arguments;)V

    goto :goto_0
.end method

.method public updateSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 0
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 316
    return-void
.end method
