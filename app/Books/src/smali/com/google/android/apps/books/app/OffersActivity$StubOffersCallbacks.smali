.class Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;
.super Lcom/google/android/apps/books/app/StubFragmentCallbacks;
.source "OffersActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/OffersFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/OffersActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StubOffersCallbacks"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/OffersActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/OffersActivity$1;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p2, "onResult":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;->maybeLogMethodName()V

    .line 112
    return-void
.end method

.method public getSystemUi()Lcom/google/android/ublib/view/SystemUi;
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;->maybeLogMethodName()V

    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "coverView"    # Landroid/view/View;

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;->maybeLogMethodName()V

    .line 117
    return-void
.end method

.method public onOfferAccepted()V
    .locals 0

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;->maybeLogMethodName()V

    .line 106
    return-void
.end method

.method public onOfferDismissed(Ljava/lang/String;)V
    .locals 0
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;->maybeLogMethodName()V

    .line 101
    return-void
.end method

.method public onUserAcknowledgesFailure()V
    .locals 0

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;->maybeLogMethodName()V

    .line 128
    return-void
.end method
