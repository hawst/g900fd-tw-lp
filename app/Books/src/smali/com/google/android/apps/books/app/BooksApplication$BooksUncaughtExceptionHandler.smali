.class Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;
.super Ljava/lang/Object;
.source "BooksApplication.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BooksUncaughtExceptionHandler"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nextHandler"    # Ljava/lang/Thread$UncaughtExceptionHandler;

    .prologue
    .line 841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 842
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mContext:Landroid/content/Context;

    .line 843
    iput-object p2, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 844
    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 849
    :try_start_0
    # getter for: Lcom/google/android/apps/books/app/BooksApplication;->sEnableHeapDump:Z
    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/OutOfMemoryError;

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mContext:Landroid/content/Context;

    # invokes: Lcom/google/android/apps/books/app/BooksApplication;->report(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->access$100(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 854
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 855
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 858
    :cond_1
    :goto_0
    return-void

    .line 852
    :catch_0
    move-exception v0

    .line 854
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 855
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 854
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v1, :cond_2

    .line 855
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksApplication$BooksUncaughtExceptionHandler;->mNextHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v1, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    :cond_2
    throw v0
.end method
