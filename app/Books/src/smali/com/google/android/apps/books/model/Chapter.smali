.class public interface abstract Lcom/google/android/apps/books/model/Chapter;
.super Ljava/lang/Object;
.source "Chapter.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Identifiable;


# virtual methods
.method public abstract getDepth()I
.end method

.method public abstract getReadingPosition()Ljava/lang/String;
.end method

.method public abstract getStartPageIndex()I
.end method

.method public abstract getStartSegmentIndex()I
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method
