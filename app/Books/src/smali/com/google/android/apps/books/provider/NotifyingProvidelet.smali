.class abstract Lcom/google/android/apps/books/provider/NotifyingProvidelet;
.super Ljava/lang/Object;
.source "NotifyingProvidelet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/NotifyingProvidelet$1;,
        Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NotifyingProvidelet"


# instance fields
.field private final mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/pool/Pool",
            "<",
            "Lcom/google/android/apps/books/util/SelectionBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

.field private final mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V
    .locals 1
    .param p1, "notifier"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
    .param p2, "dbHelper"    # Lcom/google/android/apps/books/provider/database/BooksDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;",
            "Lcom/google/android/apps/books/provider/database/BooksDatabase;",
            "Lcom/google/android/apps/books/util/pool/Pool",
            "<",
            "Lcom/google/android/apps/books/util/SelectionBuilder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p3, "builderPool":Lcom/google/android/apps/books/util/pool/Pool;, "Lcom/google/android/apps/books/util/pool/Pool<Lcom/google/android/apps/books/util/SelectionBuilder;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const-string v0, "missing database"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v0, "missing notifier"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iput-object p1, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    .line 109
    iput-object p2, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    .line 110
    iput-object p3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    .line 111
    return-void
.end method

.method private acquireBuilder(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;
    .locals 2
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 176
    iget-object v1, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v1}, Lcom/google/android/apps/books/util/pool/Pool;->acquire()Lcom/google/android/apps/books/util/pool/Poolable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 177
    .local v0, "builder":Lcom/google/android/apps/books/util/SelectionBuilder;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 178
    return-object v0
.end method


# virtual methods
.method public final delete(ILandroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 162
    invoke-direct {p0, p3, p4}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->acquireBuilder(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v0

    .line 164
    .local v0, "builder":Lcom/google/android/apps/books/util/SelectionBuilder;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v3}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 165
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0, v2, p1, p2, v0}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->deleteWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Lcom/google/android/apps/books/util/SelectionBuilder;)I

    move-result v1

    .line 166
    .local v1, "count":I
    if-lez v1, :cond_0

    .line 167
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    # invokes: Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->notify(Landroid/net/Uri;)V
    invoke-static {v3, p2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->access$100(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/util/pool/Pool;->release(Lcom/google/android/apps/books/util/pool/Poolable;)V

    return v1

    .end local v1    # "count":I
    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v4, v0}, Lcom/google/android/apps/books/util/pool/Pool;->release(Lcom/google/android/apps/books/util/pool/Poolable;)V

    throw v3
.end method

.method protected abstract deleteWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Lcom/google/android/apps/books/util/SelectionBuilder;)I
.end method

.method public final insert(ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 116
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v3}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 117
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->insertWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 119
    .local v2, "result":Landroid/net/Uri;
    iget-object v3, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    # invokes: Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->notify(Landroid/net/Uri;)V
    invoke-static {v3, p2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->access$100(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    return-object v2

    .line 121
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "result":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Landroid/database/sqlite/SQLiteConstraintException;
    const-string v3, "NotifyingProvidelet"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    const-string v3, "NotifyingProvidelet"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "while insert(uri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", values="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Landroid/content/ContentValues;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    throw v1
.end method

.method protected abstract insertWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method public final query(ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "projection"    # [Ljava/lang/String;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p4, p5}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->acquireBuilder(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v6

    .line 134
    .local v6, "builder":Lcom/google/android/apps/books/util/SelectionBuilder;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v0}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p6

    .line 135
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->queryWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/util/SelectionBuilder;)Landroid/database/Cursor;

    move-result-object v7

    .line 137
    .local v7, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    # invokes: Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->getContentResolver()Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->access$200(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;)Landroid/content/ContentResolver;

    move-result-object v0

    invoke-interface {v7, v0, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v0, v6}, Lcom/google/android/apps/books/util/pool/Pool;->release(Lcom/google/android/apps/books/util/pool/Poolable;)V

    return-object v7

    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v2, v6}, Lcom/google/android/apps/books/util/pool/Pool;->release(Lcom/google/android/apps/books/util/pool/Poolable;)V

    throw v0
.end method

.method protected abstract queryWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/util/SelectionBuilder;)Landroid/database/Cursor;
.end method

.method public final update(ILandroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # Landroid/content/ContentValues;
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 147
    invoke-direct {p0, p4, p5}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->acquireBuilder(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v5

    .line 149
    .local v5, "builder":Lcom/google/android/apps/books/util/SelectionBuilder;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mDbHelper:Lcom/google/android/apps/books/provider/database/BooksDatabase;

    invoke-virtual {v0}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 150
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->updateWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I

    move-result v6

    .line 151
    .local v6, "count":I
    if-lez v6, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mNotifier:Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;

    # invokes: Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->notify(Landroid/net/Uri;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;->access$100(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Landroid/net/Uri;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v0, v5}, Lcom/google/android/apps/books/util/pool/Pool;->release(Lcom/google/android/apps/books/util/pool/Poolable;)V

    return v6

    .end local v1    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v6    # "count":I
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet;->mBuilderPool:Lcom/google/android/apps/books/util/pool/Pool;

    invoke-interface {v2, v5}, Lcom/google/android/apps/books/util/pool/Pool;->release(Lcom/google/android/apps/books/util/pool/Poolable;)V

    throw v0
.end method

.method protected abstract updateWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I
.end method
