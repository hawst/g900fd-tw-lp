.class final Lcom/google/android/apps/books/app/ReaderFragment$1;
.super Lcom/google/android/apps/books/util/LeakSafeCallback;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->createExpireRentalHandler(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/LeakSafeCallback",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 525
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/LeakSafeCallback;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected handleMessage(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/os/Message;)Z
    .locals 1
    .param p1, "target"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 528
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 529
    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeExpireRental()Z
    invoke-static {p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    .line 531
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic handleMessage(Ljava/lang/Object;Landroid/os/Message;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/os/Message;

    .prologue
    .line 525
    check-cast p1, Lcom/google/android/apps/books/app/ReaderFragment;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment$1;->handleMessage(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/os/Message;)Z

    move-result v0

    return v0
.end method
