.class Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;
.super Landroid/os/AsyncTask;
.source "SyncDisabledDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SyncDisabledDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SetSyncAutomaticallyTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mIsAutomatic:Z

.field private final mSyncController:Lcom/google/android/apps/books/sync/SyncController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/sync/SyncController;Z)V
    .locals 0
    .param p1, "syncController"    # Lcom/google/android/apps/books/sync/SyncController;
    .param p2, "isAutomatic"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    .line 30
    iput-boolean p2, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;->mIsAutomatic:Z

    .line 31
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/SyncDisabledDialogFragment$SetSyncAutomaticallyTask;->mIsAutomatic:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/sync/SyncController;->setSyncAutomatically(Z)V

    .line 36
    const/4 v0, 0x0

    return-object v0
.end method
