.class public Lcom/google/android/apps/books/app/ContentsView;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.source "ContentsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ContentsView$Arguments;,
        Lcom/google/android/apps/books/app/ContentsView$Callbacks;
    }
.end annotation


# instance fields
.field private mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

.field mBookmarksAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

.field private mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

.field private mHeaderBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mHomeAsUpIndicator:I

.field mNotesAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;)V

    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ContentsView;->init(Landroid/content/Context;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ContentsView;->init(Landroid/content/Context;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ContentsView;->init(Landroid/content/Context;)V

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ContentsView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ContentsView;->createTabFactories()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ContentsView;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ContentsView;->createTocView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ContentsView;Landroid/view/View;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ContentsView;->logTabListView(Landroid/view/View;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/ContentsView;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ContentsView;->createBookmarkView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/ContentsView;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ContentsView;->createNotesView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Arguments;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/ContentsView;)Lcom/google/android/apps/books/app/ContentsView$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ContentsView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    return-object v0
.end method

.method private createBookmarkView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x0

    .line 369
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 370
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f040036

    invoke-virtual {v7, v0, p1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 371
    .local v6, "bookmarkView":Landroid/view/View;
    const v0, 0x7f0e00d7

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 372
    .local v2, "zeroBookmarksView":Landroid/view/View;
    const v0, 0x7f0e00d8

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 374
    .local v3, "loadingView":Landroid/view/View;
    const v0, 0x7f0e00d9

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 375
    .local v1, "bookmarkList":Landroid/widget/ListView;
    new-instance v0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v5, v5, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v8, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v8, v8, Lcom/google/android/apps/books/app/ContentsView$Arguments;->annotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    sget-object v9, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    invoke-direct {v0, v4, v5, v8, v9}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mBookmarksAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    .line 378
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView;->getHeaderHeight()I

    move-result v0

    invoke-virtual {v6, v10, v0, v10, v10}, Landroid/view/View;->setPadding(IIII)V

    .line 380
    new-instance v0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ContentsView;->mBookmarksAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ContentsView;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;-><init>(Landroid/widget/ListView;Landroid/view/View;Landroid/view/View;Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->setUp()V

    .line 382
    return-object v6
.end method

.method private createNotesView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v1, 0x0

    .line 398
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 399
    .local v2, "context":Landroid/content/Context;
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 400
    .local v9, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f04006f

    invoke-virtual {v9, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    .line 401
    .local v11, "notesView":Landroid/view/View;
    const v0, 0x7f0e014a

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 402
    .local v12, "zeroNotesView":Landroid/view/View;
    const v0, 0x7f0e00d8

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 403
    .local v6, "loadingView":Landroid/view/View;
    const v0, 0x7f0e014b

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ListView;

    .line 405
    .local v10, "notesList":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView;->getHeaderHeight()I

    move-result v0

    invoke-virtual {v11, v1, v0, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 406
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ContentsView;->makeHeaderOrFooterView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 408
    new-instance v0, Lcom/google/android/apps/books/app/ContentsView$7;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v3, v1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v4, v1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->annotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    sget-object v5, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ContentsView$7;-><init>(Lcom/google/android/apps/books/app/ContentsView;Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mNotesAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    .line 458
    new-instance v3, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ContentsView;->mNotesAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    iget-object v8, p0, Lcom/google/android/apps/books/app/ContentsView;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    move-object v4, v10

    move-object v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;-><init>(Landroid/widget/ListView;Landroid/view/View;Landroid/view/View;Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->setUp()V

    .line 460
    return-object v11
.end method

.method private createTabFactories()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/widget/TabFactory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 255
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 256
    .local v0, "specs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/widget/TabFactory;>;"
    new-instance v1, Lcom/google/android/apps/books/app/ContentsView$2;

    const v2, 0x7f0f0140

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/app/ContentsView$2;-><init>(Lcom/google/android/apps/books/app/ContentsView;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    new-instance v1, Lcom/google/android/apps/books/app/ContentsView$3;

    const v2, 0x7f0f0141

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/app/ContentsView$3;-><init>(Lcom/google/android/apps/books/app/ContentsView;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v1

    if-nez v1, :cond_0

    .line 283
    new-instance v1, Lcom/google/android/apps/books/app/ContentsView$4;

    const v2, 0x7f0f0142

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/app/ContentsView$4;-><init>(Lcom/google/android/apps/books/app/ContentsView;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_0
    return-object v0
.end method

.method private createTocView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 312
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 313
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0400d0

    invoke-virtual {v7, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 314
    .local v8, "wrap":Landroid/view/View;
    const v1, 0x7f0e01f8

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ExpandableListView;

    .line 316
    .local v5, "list":Landroid/widget/ExpandableListView;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView;->getHeaderHeight()I

    move-result v1

    invoke-virtual {v8, v2, v1, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 318
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ContentsView;->getChapterIndex()I

    move-result v3

    .line 319
    .local v3, "chapterIndex":I
    new-instance v0, Lcom/google/android/apps/books/app/ChaptersAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ContentsView$Arguments;->readingMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v4, v4, Lcom/google/android/apps/books/app/ContentsView$Arguments;->position:Lcom/google/android/apps/books/common/Position;

    new-instance v6, Lcom/google/android/apps/books/app/ContentsView$5;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/app/ContentsView$5;-><init>(Lcom/google/android/apps/books/app/ContentsView;)V

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ChaptersAdapter;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;ILcom/google/android/apps/books/common/Position;Landroid/widget/ExpandableListView;Lcom/google/android/apps/books/app/ChaptersAdapter$Callbacks;)V

    .line 344
    .local v0, "adapter":Lcom/google/android/apps/books/app/ChaptersAdapter;
    invoke-virtual {v5, v0}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 346
    const/4 v1, -0x1

    if-eq v3, v1, :cond_0

    .line 349
    invoke-virtual {v5}, Landroid/widget/ExpandableListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/ContentsView$6;

    invoke-direct {v2, p0, v5, v3}, Lcom/google/android/apps/books/app/ContentsView$6;-><init>(Lcom/google/android/apps/books/app/ContentsView;Landroid/widget/ExpandableListView;I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 359
    :cond_0
    return-object v8
.end method

.method private findToolbar(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v7/widget/Toolbar;
    .locals 1
    .param p1, "headerView"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 243
    const v0, 0x7f0e0176

    invoke-virtual {p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method private getChapterIndex()I
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->position:Lcom/google/android/apps/books/common/Position;

    if-nez v0, :cond_0

    .line 364
    const/4 v0, -0x1

    .line 365
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/R$styleable;->ActionBar:[I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 101
    .local v0, "ta":Landroid/content/res/TypedArray;
    const/4 v1, 0x2

    const v2, 0x7f02008d

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mHomeAsUpIndicator:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 109
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0b00be

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ContentsView;->mHeaderBackgroundDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 112
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 114
    return-void

    .line 104
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1

    .line 112
    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method private logTabListView(Landroid/view/View;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    .prologue
    .line 301
    instance-of v1, p1, Landroid/widget/ListView;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 302
    check-cast v0, Landroid/widget/ListView;

    .line 303
    .local v0, "list":Landroid/widget/ListView;
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTocAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;Ljava/lang/Long;)V

    .line 309
    .end local v0    # "list":Landroid/widget/ListView;
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    const-string v1, "ContentsView"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 306
    const-string v1, "ContentsView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected ListView in TabFactory, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private makeHeaderOrFooterView(Landroid/content/Context;)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 390
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 391
    .local v0, "result":Landroid/view/View;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 392
    return-object v0
.end method


# virtual methods
.method public onDestroy()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mBookmarksAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mBookmarksAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->onDestroy()V

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mNotesAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    if-eqz v0, :cond_1

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView;->mNotesAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->onDestroy()V

    .line 470
    :cond_1
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 236
    if-eqz p1, :cond_0

    .line 237
    invoke-direct {p0, p0}, Lcom/google/android/apps/books/app/ContentsView;->findToolbar(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    .line 238
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 240
    .end local v0    # "toolbar":Landroid/support/v7/widget/Toolbar;
    :cond_0
    return-void
.end method

.method public setup(Lcom/google/android/apps/books/app/ContentsView$Arguments;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V
    .locals 4
    .param p1, "arguments"    # Lcom/google/android/apps/books/app/ContentsView$Arguments;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .prologue
    .line 120
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iput-object v2, p0, Lcom/google/android/apps/books/app/ContentsView;->mArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .line 121
    iput-object p2, p0, Lcom/google/android/apps/books/app/ContentsView;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 124
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/google/android/apps/books/app/ContentsView$1;

    invoke-direct {v2, p0, v0, v0}, Lcom/google/android/apps/books/app/ContentsView$1;-><init>(Lcom/google/android/apps/books/app/ContentsView;Landroid/content/Context;Landroid/content/Context;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ContentsView;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/books/app/ContentsView;->mHeaderBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ContentsView;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 229
    invoke-direct {p0, p0}, Lcom/google/android/apps/books/app/ContentsView;->findToolbar(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v7/widget/Toolbar;

    move-result-object v1

    .line 230
    .local v1, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-static {v1}, Lcom/google/android/apps/books/util/StyleUtils;->flattenToolbar(Landroid/support/v7/widget/Toolbar;)V

    .line 231
    iget v2, p0, Lcom/google/android/apps/books/app/ContentsView;->mHomeAsUpIndicator:I

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f01d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    .line 233
    return-void
.end method
