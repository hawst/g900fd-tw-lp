.class public Lcom/google/android/apps/books/net/BooksResponseGetter;
.super Ljava/lang/Object;
.source "BooksResponseGetter.java"

# interfaces
.implements Lcom/google/android/apps/books/net/ResponseGetter;


# static fields
.field public static final AUTHORIZATION_HEADER_KEY:Ljava/lang/String; = "Authorization"

.field private static final sDefaultCodes:[I


# instance fields
.field private final mConfig:Lcom/google/android/apps/books/util/Config;

.field private final mHttpClient:Lorg/apache/http/client/HttpClient;

.field private final mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xc8

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/net/BooksResponseGetter;->sDefaultCodes:[I

    return-void
.end method

.method public constructor <init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/books/net/HttpHelper;Lcom/google/android/apps/books/util/Config;)V
    .locals 1
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p2, "httpHelper"    # Lcom/google/android/apps/books/net/HttpHelper;
    .param p3, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-string v0, "missing httpHelper"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/net/HttpHelper;

    iput-object v0, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    .line 76
    const-string v0, "missing httpClient"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 77
    const-string v0, "missing config"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/Config;

    iput-object v0, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mConfig:Lcom/google/android/apps/books/util/Config;

    .line 78
    return-void
.end method

.method private getAuthToken(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 4
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v2, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mConfig:Lcom/google/android/apps/books/util/Config;

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/Config;->usingCustomFrontend()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 227
    const/4 v0, 0x1

    .line 232
    .local v0, "allowAuth":Z
    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/net/HttpHelper;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    return-object v2

    .line 229
    .end local v0    # "allowAuth":Z
    :cond_0
    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, "scheme":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v2, "https"

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .restart local v0    # "allowAuth":Z
    :goto_2
    goto :goto_0

    .end local v0    # "allowAuth":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 232
    .end local v1    # "scheme":Ljava/lang/String;
    .restart local v0    # "allowAuth":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static maybeGetPostBody(Lorg/apache/http/HttpRequest;)Ljava/lang/String;
    .locals 4
    .param p0, "request"    # Lorg/apache/http/HttpRequest;

    .prologue
    .line 243
    instance-of v3, p0, Lorg/apache/http/client/methods/HttpPost;

    if-eqz v3, :cond_0

    move-object v2, p0

    .line 244
    check-cast v2, Lorg/apache/http/client/methods/HttpPost;

    .line 245
    .local v2, "post":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 246
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->isRepeatable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 248
    :try_start_0
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 254
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    .end local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    :goto_0
    return-object v3

    .line 249
    .restart local v1    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Ljava/io/IOException;
    const-string v3, ""

    goto :goto_0

    .line 254
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    .end local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    :cond_0
    const-string v3, ""

    goto :goto_0
.end method

.method private static toVerboseString(Lorg/apache/http/HttpRequest;)Ljava/lang/String;
    .locals 2
    .param p0, "request"    # Lorg/apache/http/HttpRequest;

    .prologue
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/books/util/LogUtil;->maybeGetHeaders(Lorg/apache/http/HttpMessage;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/books/net/BooksResponseGetter;->maybeGetPostBody(Lorg/apache/http/HttpRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toVerboseString(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 2
    .param p0, "request"    # Lorg/apache/http/HttpRequest;
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/books/net/BooksResponseGetter;->toVerboseString(Lorg/apache/http/HttpRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nResponse\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/books/net/BooksResponseGetter;->toVerboseString(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static toVerboseString(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 2
    .param p0, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/books/util/LogUtil;->maybeGetHeaders(Lorg/apache/http/HttpMessage;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public varargs execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    .locals 7
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "okCodes"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    if-eqz p3, :cond_0

    array-length v4, p3

    if-nez v4, :cond_1

    .line 128
    :cond_0
    sget-object p3, Lcom/google/android/apps/books/net/BooksResponseGetter;->sDefaultCodes:[I

    .line 132
    :cond_1
    const/4 v0, 0x0

    .line 135
    .local v0, "errCount":I
    :cond_2
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/books/net/BooksResponseGetter;->executeOnce(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 136
    :catch_0
    move-exception v2

    .line 138
    .local v2, "ioe":Ljava/io/IOException;
    add-int/lit8 v0, v0, 0x1

    .line 139
    move-object v1, v2

    .line 140
    .local v1, "ioException":Ljava/io/IOException;
    const-string v4, "ResponseGetter"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 141
    const-string v4, "ResponseGetter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "errCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-interface {p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/books/net/HttpHelper;->shouldSkipRetry(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    .line 146
    .local v3, "wrapException":Ljava/io/IOException;
    if-eqz v3, :cond_4

    .line 147
    throw v3

    .line 150
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v4}, Lcom/google/android/apps/books/net/HttpHelper;->sleep()V

    .line 152
    const/4 v4, 0x3

    if-lt v0, v4, :cond_2

    .line 154
    throw v1
.end method

.method protected executeOnce(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    .locals 21
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "okCodes"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/books/net/BooksResponseGetter;->getAuthToken(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 169
    .local v4, "authToken":Ljava/lang/String;
    const-string v18, "Authorization"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->removeHeaders(Ljava/lang/String;)V

    .line 170
    if-eqz v4, :cond_0

    .line 171
    const-string v18, "Authorization"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "GoogleLogin auth="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    const-string v18, "ForwardedForIp"

    const/16 v19, 0x0

    const-string v20, "ResponseGetter"

    invoke-static/range {v18 .. v20}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 175
    .local v9, "forwardIp":Ljava/lang/String;
    const-string v18, "X-Forwarded-For"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->removeHeaders(Ljava/lang/String;)V

    .line 176
    invoke-static {v9}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 177
    const-string v18, "X-Forwarded-For"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v9}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpClient:Lorg/apache/http/client/HttpClient;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/net/HttpUtils;->execute(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 187
    .local v15, "resp":Lorg/apache/http/HttpResponse;
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v17

    .line 188
    .local v17, "statusLine":Lorg/apache/http/StatusLine;
    invoke-interface/range {v17 .. v17}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v16

    .line 192
    .local v16, "status":I
    move-object/from16 v3, p3

    .local v3, "arr$":[I
    array-length v12, v3

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v12, :cond_4

    aget v14, v3, v10

    .line 193
    .local v14, "okCode":I
    move/from16 v0, v16

    if-ne v0, v14, :cond_3

    .line 213
    .end local v14    # "okCode":I
    :cond_2
    return-object v15

    .line 183
    .end local v3    # "arr$":[I
    .end local v10    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "resp":Lorg/apache/http/HttpResponse;
    .end local v16    # "status":I
    .end local v17    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_0
    move-exception v7

    .line 184
    .local v7, "e":Ljava/io/IOException;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/books/net/BooksResponseGetter;->toVerboseString(Lorg/apache/http/HttpRequest;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Lcom/google/android/apps/books/net/HttpHelper;->wrapException(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v18

    throw v18

    .line 192
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v3    # "arr$":[I
    .restart local v10    # "i$":I
    .restart local v12    # "len$":I
    .restart local v14    # "okCode":I
    .restart local v15    # "resp":Lorg/apache/http/HttpResponse;
    .restart local v16    # "status":I
    .restart local v17    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 200
    .end local v14    # "okCode":I
    :cond_4
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ": "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 201
    .local v6, "codePlusReason":Ljava/lang/String;
    new-instance v5, Lorg/apache/http/client/HttpResponseException;

    move/from16 v0, v16

    invoke-direct {v5, v0, v6}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    .line 202
    .local v5, "cause":Lorg/apache/http/client/HttpResponseException;
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    .line 204
    .local v8, "entity":Lorg/apache/http/HttpEntity;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/android/apps/books/net/BooksResponseGetter;->toVerboseString(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 205
    .local v13, "msg":Ljava/lang/String;
    move/from16 v0, v16

    invoke-static {v0, v13, v5}, Lcom/google/android/apps/books/net/HttpHelper;->wrapExceptionBasedOnStatus(ILjava/lang/String;Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v11

    .line 206
    .local v11, "ioException":Ljava/io/IOException;
    if-eqz v11, :cond_2

    .line 207
    invoke-static {v8}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    .line 208
    instance-of v0, v11, Lcom/google/android/apps/books/net/HttpHelper$AuthIoException;

    move/from16 v18, v0

    if-eqz v18, :cond_5

    .line 209
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/net/BooksResponseGetter;->invalidateAuthToken(Ljava/lang/String;)V

    .line 211
    :cond_5
    throw v11
.end method

.method public varargs get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "okCodes"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/apps/books/net/BooksResponseGetter;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 97
    .end local v0    # "get":Lorg/apache/http/client/methods/HttpGet;
    :catch_0
    move-exception v1

    .line 105
    .local v1, "iae":Ljava/lang/IllegalArgumentException;
    const-string v2, "ResponseGetter"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    const-string v2, "ResponseGetter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException from HttpGet(\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    new-instance v2, Lcom/google/android/ublib/utils/WrappedIoException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected invalidateAuthToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/net/BooksResponseGetter;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/net/HttpHelper;->invalidateAuthToken(Ljava/lang/String;)V

    .line 237
    return-void
.end method
