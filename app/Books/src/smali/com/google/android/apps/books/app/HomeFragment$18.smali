.class Lcom/google/android/apps/books/app/HomeFragment$18;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 1444
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$18;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1447
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;"
    const-string v2, "HomeFragment"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1448
    const-string v2, "HomeFragment"

    const-string v3, "mMyEbooksConsumer:take"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$18;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    .line 1451
    .local v1, "fragment":Lcom/google/android/apps/books/app/HomeFragment;
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1453
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    instance-of v2, v2, Landroid/database/sqlite/SQLiteException;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_2

    .line 1455
    invoke-static {v0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->maybeRecommendUninstallApp(Landroid/app/Activity;)Z

    .line 1460
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_1
    :goto_0
    return-void

    .line 1456
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1457
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->setVolumes(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2700(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1444
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$18;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
