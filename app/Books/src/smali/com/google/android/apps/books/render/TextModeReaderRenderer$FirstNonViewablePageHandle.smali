.class public Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;
.super Lcom/google/android/apps/books/render/EmptyPageHandle;
.source "TextModeReaderRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/TextModeReaderRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FirstNonViewablePageHandle"
.end annotation


# instance fields
.field private final mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

.field final synthetic this$0:Lcom/google/android/apps/books/render/TextModeReaderRenderer;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/render/TextModeReaderRenderer;I)V
    .locals 1
    .param p2, "firstForbiddenPassageIndex"    # I

    .prologue
    const/4 v0, 0x0

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;->this$0:Lcom/google/android/apps/books/render/TextModeReaderRenderer;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/EmptyPageHandle;-><init>()V

    .line 36
    invoke-static {p2, v0, v0}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 37
    return-void
.end method


# virtual methods
.method public getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;->mPageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    return-object v0
.end method
