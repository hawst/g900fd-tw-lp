.class public Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;
.super Ljava/lang/Object;
.source "ContentWithResourcesResponseImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/net/BooksServer$ContentWithResourcesResponse;


# static fields
.field private static final NO_RESOURCES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContent:Lcom/google/android/apps/books/net/EncryptedContentResponse;

.field private final mResources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->NO_RESOURCES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/net/EncryptedContentResponse;Ljava/util/List;)V
    .locals 0
    .param p1, "content"    # Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/net/EncryptedContentResponse;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->mContent:Lcom/google/android/apps/books/net/EncryptedContentResponse;

    .line 21
    if-nez p2, :cond_0

    sget-object p2, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->NO_RESOURCES:Ljava/util/List;

    .end local p2    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->mResources:Ljava/util/List;

    .line 22
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->mContent:Lcom/google/android/apps/books/net/EncryptedContentResponse;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->mContent:Lcom/google/android/apps/books/net/EncryptedContentResponse;

    invoke-interface {v0}, Lcom/google/android/apps/books/net/EncryptedContentResponse;->close()V

    .line 39
    :cond_0
    return-void
.end method

.method public getContent()Lcom/google/android/apps/books/net/EncryptedContentResponse;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->mContent:Lcom/google/android/apps/books/net/EncryptedContentResponse;

    return-object v0
.end method

.method public getResources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/net/ContentWithResourcesResponseImpl;->mResources:Ljava/util/List;

    return-object v0
.end method
