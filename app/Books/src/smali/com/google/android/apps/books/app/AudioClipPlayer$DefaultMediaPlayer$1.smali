.class Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->setDataSourceAndPreparePlayer(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/InputStreamSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

.field final synthetic val$clip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

.field final synthetic val$requestId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;ILcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    iput p2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$requestId:I

    iput-object p3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$clip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 254
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-interface {v3}, Lcom/google/android/apps/books/data/InputStreamSource;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 262
    .local v2, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    :goto_0
    move-object v1, v2

    .line 263
    .local v1, "finalResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 309
    return-void

    .line 256
    .end local v1    # "finalResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    .end local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .line 258
    .restart local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    goto :goto_0

    .line 260
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .restart local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/os/ParcelFileDescriptor;>;"
    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 243
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
