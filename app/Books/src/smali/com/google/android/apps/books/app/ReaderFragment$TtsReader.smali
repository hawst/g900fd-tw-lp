.class Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TtsReader"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 7656
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 7656
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public beganSpeakingPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/annotations/TextLocationRange;)V
    .locals 3
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;

    .prologue
    .line 7660
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7661
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TTS is speaking phrase "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with range "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7663
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 7664
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->getPassageIndex()I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->highlightSpokenText(ILcom/google/android/apps/books/annotations/TextLocationRange;)V

    .line 7665
    return-void
.end method

.method public finishedSpeaking(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 5
    .param p1, "reason"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    .line 7673
    const-string v2, "ReaderFragment"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7674
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Finished speaking due to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7677
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->COMPLETED_ITEM:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-ne p1, v2, :cond_2

    .line 7695
    :cond_1
    :goto_0
    return-void

    .line 7682
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 7683
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStopped()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10400(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 7685
    sget-object v2, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->OTHER_ERROR:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-ne p1, v2, :cond_3

    .line 7686
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    .line 7687
    .local v1, "scene":Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    if-eqz v1, :cond_1

    .line 7688
    new-instance v2, Lcom/google/android/apps/books/app/ErrorFragment;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const v4, 0x7f0f0137

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/app/ErrorFragment;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->addFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 7690
    .end local v1    # "scene":Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    :cond_3
    sget-object v2, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->END_OF_BOOK:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-ne p1, v2, :cond_1

    .line 7691
    sget-object v2, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/ScreenDirection;->fromReadingDirection(Lcom/google/android/apps/books/util/ReadingDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    .line 7693
    .local v0, "forward":Lcom/google/android/apps/books/util/ScreenDirection;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v3, 0x1

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    invoke-static {v2, v0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    goto :goto_0
.end method
