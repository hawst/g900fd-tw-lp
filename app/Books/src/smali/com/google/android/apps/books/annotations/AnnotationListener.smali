.class public interface abstract Lcom/google/android/apps/books/annotations/AnnotationListener;
.super Ljava/lang/Object;
.source "AnnotationListener.java"


# virtual methods
.method public abstract annotationAdded(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/common/collect/ImmutableList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract characterQuotaLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract volumeAnnotationsLoaded(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation
.end method
