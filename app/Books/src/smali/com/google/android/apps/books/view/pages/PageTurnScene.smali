.class Lcom/google/android/apps/books/view/pages/PageTurnScene;
.super Ljava/lang/Object;
.source "PageTurnScene.java"


# instance fields
.field animating:Z

.field animationDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field final animationDurationMillisLandscape:J

.field final animationDurationMillisPortrait:J

.field animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

.field animationStartTimeMillis:J

.field fractionTurned:F

.field private initializationCompletionSignaled:Z

.field private mBackgroundColor:I

.field private final mBackgroundFloats:[F

.field private mCachedBorderBitmap:Landroid/graphics/Bitmap;

.field private mCallback:Lcom/google/android/apps/books/view/pages/PageTurnCallback;

.field private mDisplayTwoPages:Z

.field private mEmptyTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

.field private mExecutingInitialLoadTransition:Z

.field private mLoadingPageVisible:Z

.field private mLoadingTextureDimensions:Landroid/graphics/Point;

.field private mLoadingTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

.field private mPublishedCallbackForThisTurn:Z

.field final mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

.field final mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

.field private final mSizeBack:Landroid/graphics/Point;

.field private final mSizeBegin:Landroid/graphics/Point;

.field private final mSizeCurl:Landroid/graphics/Point;

.field private final mSizeEnd:Landroid/graphics/Point;

.field private final mSizeFront:Landroid/graphics/Point;

.field final mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

.field private mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

.field mainPageIndex:I

.field final pageAnimator:Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

.field final pageMesh:Lcom/google/android/apps/books/view/pages/PageMesh;

.field final pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

.field points:Ljava/util/ArrayList;

.field readerTheme:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnRenderer;Lcom/google/android/apps/books/view/pages/PageTurnSetting;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 12
    .param p1, "renderer"    # Lcom/google/android/apps/books/view/pages/PageTurnRenderer;
    .param p2, "setting"    # Lcom/google/android/apps/books/view/pages/PageTurnSetting;
    .param p3, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->initializationCompletionSignaled:Z

    .line 62
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    .line 63
    const-string v3, "0"

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->readerTheme:Ljava/lang/String;

    .line 65
    sget-object v3, Lcom/google/android/apps/books/util/WritingDirection;->LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    .line 70
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 71
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 73
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->points:Ljava/util/ArrayList;

    .line 77
    const/4 v3, 0x6

    new-array v3, v3, [Lcom/google/android/apps/books/view/pages/Page;

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    .line 83
    const-wide/16 v8, 0xfa

    iput-wide v8, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationDurationMillisLandscape:J

    .line 84
    const-wide/16 v8, 0x190

    iput-wide v8, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationDurationMillisPortrait:J

    .line 89
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 90
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mEmptyTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 92
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    .line 93
    const v3, -0x333334

    iput v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundColor:I

    .line 102
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mExecutingInitialLoadTransition:Z

    .line 209
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    .line 210
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    .line 211
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeEnd:Landroid/graphics/Point;

    .line 212
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBegin:Landroid/graphics/Point;

    .line 213
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeCurl:Landroid/graphics/Point;

    .line 727
    const/4 v3, 0x4

    new-array v3, v3, [F

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundFloats:[F

    .line 112
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 113
    .local v4, "t0":J
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    .line 114
    iput-object p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    .line 115
    new-instance v3, Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;)V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    .line 116
    new-instance v3, Lcom/google/android/apps/books/view/pages/PageMesh;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/view/pages/PageMesh;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;)V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMesh:Lcom/google/android/apps/books/view/pages/PageMesh;

    .line 117
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMesh:Lcom/google/android/apps/books/view/pages/PageMesh;

    const/16 v8, 0x64

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Lcom/google/android/apps/books/view/pages/PageMesh;->setSampling(II)V

    .line 119
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v3, 0x6

    if-ge v1, v3, :cond_0

    .line 120
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    new-instance v8, Lcom/google/android/apps/books/view/pages/Page;

    invoke-direct {v8, p0, p3}, Lcom/google/android/apps/books/view/pages/Page;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    aput-object v8, v3, v1

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_0
    const-string v3, "PageTurnScene"

    const/4 v8, 0x2

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 124
    .local v6, "t1":J
    const-string v3, "PageTurnScene"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Initializing pageWheel in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v10, v6, v4

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    move-wide v4, v6

    .line 128
    .end local v6    # "t1":J
    :cond_1
    new-instance v3, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;-><init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;)V

    iput-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageAnimator:Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

    .line 129
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageAnimator:Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

    invoke-virtual {v3}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->needsCurves()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 131
    iget-object v3, p1, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 132
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f100006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 136
    .local v0, "bezierCurveData":[I
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageAnimator:Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->loadCurves([I)V

    .line 137
    const-string v3, "PageTurnScene"

    const/4 v8, 0x2

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 138
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 139
    .restart local v6    # "t1":J
    const-string v3, "PageTurnScene"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Initializing beziers in "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v10, v6, v4

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v0    # "bezierCurveData":[I
    .end local v2    # "res":Landroid/content/res/Resources;
    .end local v6    # "t1":J
    :cond_2
    return-void
.end method

.method private allPagesAreFlat()Z
    .locals 2

    .prologue
    .line 435
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animating:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private computeFractionTurned()F
    .locals 11

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    .line 496
    iget-boolean v7, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animating:Z

    if-eqz v7, :cond_4

    .line 497
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 498
    .local v4, "now":J
    iget-wide v8, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationStartTimeMillis:J

    sub-long v2, v4, v8

    .line 499
    .local v2, "elapsedTimeMillis":J
    iget-boolean v7, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v7, :cond_3

    const-wide/16 v0, 0xfa

    .line 502
    .local v0, "animationDurationMillis":J
    :goto_0
    long-to-float v7, v2

    long-to-float v8, v0

    div-float v6, v7, v8

    .line 504
    .local v6, "result":F
    cmpl-float v7, v6, v10

    if-lez v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mPublishedCallbackForThisTurn:Z

    if-nez v7, :cond_0

    .line 507
    iget-object v7, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCallback:Lcom/google/android/apps/books/view/pages/PageTurnCallback;

    invoke-interface {v7}, Lcom/google/android/apps/books/view/pages/PageTurnCallback;->onTurnAnimationFinished()V

    .line 508
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mPublishedCallbackForThisTurn:Z

    .line 510
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    sget-object v8, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne v7, v8, :cond_1

    .line 511
    sub-float v6, v10, v6

    .line 514
    :cond_1
    const-string v7, "PageTurnScene"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 515
    const-string v7, "PageTurnScene"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "computeFractionTurned: now="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; result="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    .end local v0    # "animationDurationMillis":J
    .end local v2    # "elapsedTimeMillis":J
    .end local v4    # "now":J
    :cond_2
    :goto_1
    const/4 v7, 0x0

    invoke-static {v6, v7, v10}, Lcom/google/android/apps/books/util/MathUtils;->constrain(FFF)F

    move-result v6

    .line 522
    return v6

    .line 499
    .end local v6    # "result":F
    .restart local v2    # "elapsedTimeMillis":J
    .restart local v4    # "now":J
    :cond_3
    const-wide/16 v0, 0x190

    goto :goto_0

    .line 518
    .end local v2    # "elapsedTimeMillis":J
    .end local v4    # "now":J
    :cond_4
    iget v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    .restart local v6    # "result":F
    goto :goto_1
.end method

.method private drawCurlingPage(ILandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;J)Z
    .locals 8
    .param p1, "pageIndex"    # I
    .param p2, "size"    # Landroid/graphics/Point;
    .param p3, "mesh"    # Lcom/google/android/apps/books/view/pages/PageMesh;
    .param p4, "now"    # J

    .prologue
    .line 446
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;

    move-result-object v1

    .line 449
    .local v1, "page":Lcom/google/android/apps/books/view/pages/Page;
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/view/pages/Page;->isOccluded(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    const/4 v0, 0x0

    .line 452
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v3, p2

    move-object v4, p3

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/books/view/pages/Page;->draw(ZLandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;FJ)Z

    move-result v0

    goto :goto_0
.end method

.method private drawFlatPage(ILandroid/graphics/Point;FJ)Z
    .locals 8
    .param p1, "pageIndex"    # I
    .param p2, "size"    # Landroid/graphics/Point;
    .param p3, "alphaInOut"    # F
    .param p4, "now"    # J

    .prologue
    .line 461
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;

    move-result-object v1

    .line 462
    .local v1, "page":Lcom/google/android/apps/books/view/pages/Page;
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v2

    const/4 v4, 0x0

    move-object v3, p2

    move v5, p3

    move-wide v6, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/books/view/pages/Page;->draw(ZLandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;FJ)Z

    move-result v0

    return v0
.end method

.method private drawShadow(ILandroid/graphics/Point;)V
    .locals 3
    .param p1, "pageIndex"    # I
    .param p2, "size"    # Landroid/graphics/Point;

    .prologue
    .line 470
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;

    move-result-object v0

    .line 471
    .local v0, "page":Lcom/google/android/apps/books/view/pages/Page;
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageAnimator:Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    invoke-virtual {v1, v0, p2, v2}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->shadowDraw(Lcom/google/android/apps/books/view/pages/Page;Landroid/graphics/Point;F)V

    .line 472
    return-void
.end method

.method private getPageSize(ILandroid/graphics/Point;)V
    .locals 2
    .param p1, "pagewheelIndex"    # I
    .param p2, "size"    # Landroid/graphics/Point;

    .prologue
    .line 745
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;

    move-result-object v0

    .line 746
    .local v0, "page":Lcom/google/android/apps/books/view/pages/Page;
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/view/pages/Page;->getSize(Landroid/graphics/Point;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 751
    :goto_0
    return-void

    .line 750
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getPageSize(Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method private interpolateCurlSize(Landroid/graphics/Point;FLandroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 3
    .param p1, "result"    # Landroid/graphics/Point;
    .param p2, "fraction"    # F
    .param p3, "first"    # Landroid/graphics/Point;
    .param p4, "last"    # Landroid/graphics/Point;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 227
    sub-float v0, v2, p2

    iget v1, p3, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p4, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p1, Landroid/graphics/Point;->x:I

    .line 228
    sub-float v0, v2, p2

    iget v1, p3, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p4, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p1, Landroid/graphics/Point;->y:I

    .line 229
    return-void
.end method

.method private interpolateFlatOpacity(FLandroid/graphics/Point;Landroid/graphics/Point;)F
    .locals 3
    .param p1, "fraction"    # F
    .param p2, "front"    # Landroid/graphics/Point;
    .param p3, "back"    # Landroid/graphics/Point;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 238
    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p3, Landroid/graphics/Point;->x:I

    if-ne v1, v2, :cond_0

    iget v1, p2, Landroid/graphics/Point;->y:I

    iget v2, p3, Landroid/graphics/Point;->y:I

    if-ne v1, v2, :cond_0

    .line 246
    :goto_0
    return v0

    :cond_0
    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method private invalidateTexture(IZZLandroid/graphics/Point;)V
    .locals 4
    .param p1, "slot"    # I
    .param p2, "showLoading"    # Z
    .param p3, "loading"    # Z
    .param p4, "loadingDisplaySize"    # Landroid/graphics/Point;

    .prologue
    const-wide/16 v2, 0x0

    .line 613
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    aget-object v0, v1, p1

    .line 614
    .local v0, "page":Lcom/google/android/apps/books/view/pages/Page;
    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/Page;->invalidateTexture()V

    .line 615
    iput-wide v2, v0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    .line 616
    iput-wide v2, v0, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 617
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/view/pages/Page;->setShowSpinnerWhenLoading(Z)V

    .line 618
    invoke-virtual {v0, p3}, Lcom/google/android/apps/books/view/pages/Page;->setLoading(Z)V

    .line 619
    invoke-virtual {v0, p4}, Lcom/google/android/apps/books/view/pages/Page;->setDisplaySize(Landroid/graphics/Point;)V

    .line 620
    return-void
.end method

.method private invalidateTextures()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 626
    const/4 v0, 0x0

    .local v0, "slot":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 628
    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mExecutingInitialLoadTransition:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_1
    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->invalidateTexture(IZZLandroid/graphics/Point;)V

    .line 626
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 628
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 630
    :cond_1
    return-void
.end method

.method static pageMod(II)I
    .locals 1
    .param p0, "a"    # I
    .param p1, "n"    # I

    .prologue
    .line 566
    rem-int v0, p0, p1

    .line 567
    .local v0, "t":I
    if-gez v0, :cond_0

    .line 568
    add-int/2addr v0, p1

    .line 569
    :cond_0
    return v0
.end method

.method private shouldFlipSpinnerImage()Z
    .locals 2

    .prologue
    .line 690
    iget v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundColor:I

    const/high16 v1, -0x1000000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updatedMesh()Lcom/google/android/apps/books/view/pages/PageMesh;
    .locals 6

    .prologue
    .line 419
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 420
    .local v0, "t0":J
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMesh:Lcom/google/android/apps/books/view/pages/PageMesh;

    iget v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/view/pages/PageMesh;->updateMesh(F)V

    .line 427
    const-string v2, "PageTurnScene"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 428
    const-string v2, "PageTurnScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "### updateMesh() @ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMesh:Lcom/google/android/apps/books/view/pages/PageMesh;

    return-object v2
.end method


# virtual methods
.method public animateTransitionToNextPage(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 10
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 634
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animating:Z

    .line 635
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mPublishedCallbackForThisTurn:Z

    .line 636
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 645
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v3, :cond_1

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    .line 647
    .local v2, "fractionComplete":F
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v3, :cond_2

    const-wide/16 v0, 0xfa

    .line 649
    .local v0, "animationDurationMillis":J
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 654
    .local v4, "now":J
    long-to-float v3, v0

    mul-float/2addr v3, v2

    float-to-long v6, v3

    sub-long v6, v4, v6

    iput-wide v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationStartTimeMillis:J

    .line 655
    const-string v3, "PageTurnScene"

    const/4 v6, 0x3

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 656
    const-string v3, "PageTurnScene"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "animationTransitionToNextPage: fraction="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; now="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; animationStartTimeMillis="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationStartTimeMillis:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    :cond_0
    return-void

    .line 645
    .end local v0    # "animationDurationMillis":J
    .end local v2    # "fractionComplete":F
    .end local v4    # "now":J
    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    sub-float v2, v3, v6

    goto :goto_0

    .line 647
    .restart local v2    # "fractionComplete":F
    :cond_2
    const-wide/16 v0, 0x190

    goto :goto_1
.end method

.method public displayTwoPages()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    return v0
.end method

.method public getBackgroundColorFloats()[F
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundFloats:[F

    return-object v0
.end method

.method getBorderBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 710
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v1, p2, :cond_2

    .line 712
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 713
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 715
    :cond_1
    const-string v1, "PageTurnScene#getBorderBitmap"

    invoke-static {v1, p1, p2, p3}, Lcom/google/android/apps/books/util/BitmapUtils;->createBitmapInReader(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    .line 718
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 720
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    .line 721
    .local v0, "borderBitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public getEmptyTexturePlacement()Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .locals 3

    .prologue
    .line 694
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mEmptyTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    if-nez v1, :cond_0

    .line 697
    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundColor:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 698
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v0, v1, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptyLight:I

    .line 704
    .local v0, "texture":I
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/view/pages/TexturePlacement;

    const-string v2, "Empty"

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mEmptyTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 706
    .end local v0    # "texture":I
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mEmptyTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    return-object v1

    .line 699
    :cond_1
    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundColor:I

    const/high16 v2, -0x1000000

    if-ne v1, v2, :cond_2

    .line 700
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v0, v1, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptyDark:I

    .restart local v0    # "texture":I
    goto :goto_0

    .line 702
    .end local v0    # "texture":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v0, v1, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptySepia:I

    .restart local v0    # "texture":I
    goto :goto_0
.end method

.method public getLoadingTexturePlacement(Landroid/graphics/Point;)Lcom/google/android/apps/books/view/pages/TexturePlacement;
    .locals 6
    .param p1, "targetSize"    # Landroid/graphics/Point;

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTextureDimensions:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 663
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v2, v2, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTextureDimensions:Landroid/graphics/Point;

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    if-nez v0, :cond_1

    .line 668
    new-instance v0, Lcom/google/android/apps/books/view/pages/TexturePlacement;

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    iget v1, v1, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mLoading:I

    const-string v2, "Loading"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 670
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->getMeasurements()Lcom/google/android/apps/books/view/pages/PageMeasurements;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTextureDimensions:Landroid/graphics/Point;

    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->shouldFlipSpinnerImage()Z

    move-result v3

    const/16 v4, 0x6d6

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mutableTransformMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->calculateCenteredTextureTransformationMatrix(Landroid/graphics/Point;Landroid/graphics/Point;ZILandroid/graphics/Matrix;)V

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    return-object v0
.end method

.method getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;
    .locals 2
    .param p1, "pageWheelIndex"    # I

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    const/4 v1, 0x6

    invoke-static {p1, v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMod(II)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    return-object v0
.end method

.method initialize()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mTextures:Lcom/google/android/apps/books/view/pages/PageTurnTexture;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->initialize()V

    .line 479
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->invalidateTextures()V

    .line 480
    return-void
.end method

.method public isExecutingInitialLoadTransition()Z
    .locals 1

    .prologue
    .line 763
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mExecutingInitialLoadTransition:Z

    return v0
.end method

.method public isRightToLeft()Z
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    sget-object v1, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRtlOneUp()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRightToLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public liftingLeftPageOfTwo()Z
    .locals 2

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

    sget-object v1, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public liftingPageFromLeft()Z
    .locals 2

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

    sget-object v1, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRightToLeft()Z

    move-result v0

    goto :goto_0
.end method

.method onDrawFrame()Z
    .locals 43

    .prologue
    .line 253
    const-string v2, "gl detect error"

    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 256
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getBackgroundColorFloats()[F

    move-result-object v40

    .line 257
    .local v40, "rgb":[F
    const/4 v2, 0x0

    aget v2, v40, v2

    const/4 v4, 0x1

    aget v4, v40, v4

    const/4 v8, 0x2

    aget v8, v40, v8

    const/4 v10, 0x3

    aget v10, v40, v10

    invoke-static {v2, v4, v8, v10}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 258
    const/16 v2, 0x4000

    invoke-static {v2}, Landroid/opengl/GLES20;->glClear(I)V

    .line 259
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->computeFractionTurned()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    .line 261
    const/16 v35, 0x0

    .line 262
    .local v35, "loadingPageVisible":Z
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->allPagesAreFlat()Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v36, 0x1

    .line 263
    .local v36, "needsCurl":Z
    :goto_0
    if-eqz v36, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    float-to-double v12, v2

    const-wide v24, 0x3f747ae147ae147bL    # 0.005

    cmpl-double v2, v12, v24

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    float-to-double v12, v2

    const-wide v24, 0x3fefd70a3d70a3d7L    # 0.995

    cmpg-double v2, v12, v24

    if-gez v2, :cond_1

    const/16 v37, 0x1

    .line 265
    .local v37, "needsShadow":Z
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 267
    .local v6, "now":J
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v2, :cond_8

    .line 273
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    and-int/lit8 v33, v2, 0x1

    .line 274
    .local v33, "frontSlot":I
    xor-int/lit8 v28, v33, 0x1

    .line 275
    .local v28, "backSlot":I
    const/16 v41, 0x0

    .local v41, "slot":I
    :goto_2
    const/4 v2, 0x6

    move/from16 v0, v41

    if-ge v0, v2, :cond_2

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    add-int v4, v28, v41

    aget-object v2, v2, v4

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/view/pages/Page;->setBackFacing(Z)V

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    add-int v4, v33, v41

    aget-object v2, v2, v4

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/view/pages/Page;->setBackFacing(Z)V

    .line 275
    add-int/lit8 v41, v41, 0x2

    goto :goto_2

    .line 262
    .end local v6    # "now":J
    .end local v28    # "backSlot":I
    .end local v33    # "frontSlot":I
    .end local v36    # "needsCurl":Z
    .end local v37    # "needsShadow":Z
    .end local v41    # "slot":I
    :cond_0
    const/16 v36, 0x0

    goto :goto_0

    .line 263
    .restart local v36    # "needsCurl":Z
    :cond_1
    const/16 v37, 0x0

    goto :goto_1

    .line 279
    .restart local v6    # "now":J
    .restart local v28    # "backSlot":I
    .restart local v33    # "frontSlot":I
    .restart local v37    # "needsShadow":Z
    .restart local v41    # "slot":I
    :cond_2
    if-eqz v36, :cond_7

    .line 293
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingLeftPageOfTwo()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 294
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v9, v2, 0x0

    .line 295
    .local v9, "begin":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v23, v2, -0x1

    .line 296
    .local v23, "front":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v17, v2, -0x2

    .line 297
    .local v17, "back":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v3, v2, -0x3

    .line 298
    .local v3, "end":I
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    sub-float v31, v2, v4

    .line 306
    .local v31, "fractionCurl":F
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeEnd:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBegin:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 310
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeCurl:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-direct {v0, v2, v1, v4, v8}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->interpolateCurlSize(Landroid/graphics/Point;FLandroid/graphics/Point;Landroid/graphics/Point;)V

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeEnd:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->interpolateFlatOpacity(FLandroid/graphics/Point;Landroid/graphics/Point;)F

    move-result v5

    .line 313
    .local v5, "popinOpacity":F
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v2, v31

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBegin:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v8}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->interpolateFlatOpacity(FLandroid/graphics/Point;Landroid/graphics/Point;)F

    move-result v11

    .line 315
    .local v11, "popoutOpacity":F
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->updatedMesh()Lcom/google/android/apps/books/view/pages/PageMesh;

    move-result-object v15

    .line 316
    .local v15, "mesh":Lcom/google/android/apps/books/view/pages/PageMesh;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeEnd:Landroid/graphics/Point;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawFlatPage(ILandroid/graphics/Point;FJ)Z

    move-result v2

    or-int v35, v35, v2

    .line 317
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBegin:Landroid/graphics/Point;

    move-object/from16 v8, p0

    move-wide v12, v6

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawFlatPage(ILandroid/graphics/Point;FJ)Z

    move-result v2

    or-int v35, v35, v2

    .line 318
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeCurl:Landroid/graphics/Point;

    move-object/from16 v12, p0

    move/from16 v13, v17

    move-wide/from16 v16, v6

    invoke-direct/range {v12 .. v17}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawCurlingPage(ILandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;J)Z

    move-result v2

    or-int v35, v35, v2

    .line 319
    if-eqz v37, :cond_3

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeCurl:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawShadow(ILandroid/graphics/Point;)V

    .line 322
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeCurl:Landroid/graphics/Point;

    move-object/from16 v12, p0

    move/from16 v13, v23

    move-wide/from16 v16, v6

    invoke-direct/range {v12 .. v17}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawCurlingPage(ILandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;J)Z

    move-result v2

    or-int v35, v35, v2

    .line 397
    .end local v3    # "end":I
    .end local v5    # "popinOpacity":F
    .end local v9    # "begin":I
    .end local v11    # "popoutOpacity":F
    .end local v15    # "mesh":Lcom/google/android/apps/books/view/pages/PageMesh;
    .end local v17    # "back":I
    .end local v23    # "front":I
    .end local v28    # "backSlot":I
    .end local v31    # "fractionCurl":F
    .end local v33    # "frontSlot":I
    .end local v41    # "slot":I
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->initializationCompletionSignaled:Z

    if-nez v2, :cond_4

    .line 398
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCallback:Lcom/google/android/apps/books/view/pages/PageTurnCallback;

    invoke-interface {v2}, Lcom/google/android/apps/books/view/pages/PageTurnCallback;->initializationComplete()V

    .line 399
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->initializationCompletionSignaled:Z

    .line 402
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingPageVisible:Z

    move/from16 v0, v35

    if-eq v0, v2, :cond_5

    .line 403
    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mLoadingPageVisible:Z

    .line 404
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    move/from16 v0, v35

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->onLoadingPageVisibilityChanged(Z)V

    .line 407
    :cond_5
    const/16 v42, 0x0

    .line 409
    .local v42, "updating":Z
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v2, :cond_e

    const/4 v2, 0x3

    :goto_5
    sub-int v30, v4, v2

    .line 410
    .local v30, "firstVisiblePage":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v2, :cond_f

    const/4 v2, 0x2

    :goto_6
    add-int v34, v4, v2

    .line 411
    .local v34, "lastVisiblePage":I
    move/from16 v39, v30

    .local v39, "page":I
    :goto_7
    move/from16 v0, v39

    move/from16 v1, v34

    if-gt v0, v1, :cond_10

    if-nez v42, :cond_10

    .line 412
    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Lcom/google/android/apps/books/view/pages/Page;->isUpdating(J)Z

    move-result v2

    or-int v42, v42, v2

    .line 411
    add-int/lit8 v39, v39, 0x1

    goto :goto_7

    .line 300
    .end local v30    # "firstVisiblePage":I
    .end local v34    # "lastVisiblePage":I
    .end local v39    # "page":I
    .end local v42    # "updating":Z
    .restart local v28    # "backSlot":I
    .restart local v33    # "frontSlot":I
    .restart local v41    # "slot":I
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v9, v2, -0x1

    .line 301
    .restart local v9    # "begin":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v23, v2, 0x0

    .line 302
    .restart local v23    # "front":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v17, v2, 0x1

    .line 303
    .restart local v17    # "back":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v3, v2, 0x2

    .line 304
    .restart local v3    # "end":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    move/from16 v31, v0

    .restart local v31    # "fractionCurl":F
    goto/16 :goto_3

    .line 324
    .end local v3    # "end":I
    .end local v9    # "begin":I
    .end local v17    # "back":I
    .end local v23    # "front":I
    .end local v31    # "fractionCurl":F
    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v17, v2, 0x5

    .line 325
    .restart local v17    # "back":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move/from16 v32, v0

    .line 326
    .local v32, "front":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v18, v0

    const/high16 v19, 0x3f800000    # 1.0f

    move-object/from16 v16, p0

    move-wide/from16 v20, v6

    invoke-direct/range {v16 .. v21}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawFlatPage(ILandroid/graphics/Point;FJ)Z

    move-result v2

    or-int v35, v35, v2

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v18, p0

    move/from16 v19, v32

    move-wide/from16 v22, v6

    invoke-direct/range {v18 .. v23}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawFlatPage(ILandroid/graphics/Point;FJ)Z

    move-result v2

    or-int v35, v35, v2

    move/from16 v23, v32

    .end local v32    # "front":I
    .restart local v23    # "front":I
    goto/16 :goto_4

    .line 333
    .end local v17    # "back":I
    .end local v23    # "front":I
    .end local v28    # "backSlot":I
    .end local v33    # "frontSlot":I
    .end local v41    # "slot":I
    :cond_8
    if-eqz v36, :cond_d

    .line 336
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRightToLeft()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

    sget-object v4, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne v2, v4, :cond_a

    .line 343
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v29, v2, 0x1

    .line 344
    .local v29, "curl":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move/from16 v19, v0

    .line 345
    .local v19, "flat":I
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    sub-float v31, v2, v4

    .line 378
    .restart local v31    # "fractionCurl":F
    :goto_8
    const v2, 0x3fe8ba2e

    mul-float v38, v2, v31

    .line 379
    .local v38, "normalizedCurlFraction":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->interpolateFlatOpacity(FLandroid/graphics/Point;Landroid/graphics/Point;)F

    move-result v21

    .line 383
    .local v21, "popOpacity":F
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->updatedMesh()Lcom/google/android/apps/books/view/pages/PageMesh;

    move-result-object v15

    .line 384
    .restart local v15    # "mesh":Lcom/google/android/apps/books/view/pages/PageMesh;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeBack:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v18, p0

    move-wide/from16 v22, v6

    invoke-direct/range {v18 .. v23}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawFlatPage(ILandroid/graphics/Point;FJ)Z

    move-result v2

    or-int v35, v35, v2

    .line 385
    if-eqz v37, :cond_9

    .line 386
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawShadow(ILandroid/graphics/Point;)V

    .line 388
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v24, v0

    move-object/from16 v22, p0

    move/from16 v23, v29

    move-object/from16 v25, v15

    move-wide/from16 v26, v6

    invoke-direct/range {v22 .. v27}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawCurlingPage(ILandroid/graphics/Point;Lcom/google/android/apps/books/view/pages/PageMesh;J)Z

    move-result v2

    or-int v35, v35, v2

    .line 389
    goto/16 :goto_4

    .line 352
    .end local v15    # "mesh":Lcom/google/android/apps/books/view/pages/PageMesh;
    .end local v19    # "flat":I
    .end local v21    # "popOpacity":F
    .end local v29    # "curl":I
    .end local v31    # "fractionCurl":F
    .end local v38    # "normalizedCurlFraction":F
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move/from16 v29, v0

    .line 353
    .restart local v29    # "curl":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v19, v2, -0x1

    .line 354
    .restart local v19    # "flat":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    move/from16 v31, v0

    .restart local v31    # "fractionCurl":F
    goto :goto_8

    .line 357
    .end local v19    # "flat":I
    .end local v29    # "curl":I
    .end local v31    # "fractionCurl":F
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

    sget-object v4, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne v2, v4, :cond_c

    .line 363
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v29, v2, -0x1

    .line 364
    .restart local v29    # "curl":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move/from16 v19, v0

    .line 365
    .restart local v19    # "flat":I
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    sub-float v31, v2, v4

    .restart local v31    # "fractionCurl":F
    goto/16 :goto_8

    .line 372
    .end local v19    # "flat":I
    .end local v29    # "curl":I
    .end local v31    # "fractionCurl":F
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move/from16 v29, v0

    .line 373
    .restart local v29    # "curl":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v19, v2, 0x1

    .line 374
    .restart local v19    # "flat":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    move/from16 v31, v0

    .restart local v31    # "fractionCurl":F
    goto/16 :goto_8

    .line 390
    .end local v19    # "flat":I
    .end local v29    # "curl":I
    .end local v31    # "fractionCurl":F
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    move/from16 v23, v0

    .line 391
    .restart local v23    # "front":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageSize(ILandroid/graphics/Point;)V

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSizeFront:Landroid/graphics/Point;

    move-object/from16 v24, v0

    const/high16 v25, 0x3f800000    # 1.0f

    move-object/from16 v22, p0

    move-wide/from16 v26, v6

    invoke-direct/range {v22 .. v27}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->drawFlatPage(ILandroid/graphics/Point;FJ)Z

    move-result v2

    or-int v35, v35, v2

    goto/16 :goto_4

    .line 409
    .end local v23    # "front":I
    .restart local v42    # "updating":Z
    :cond_e
    const/4 v2, 0x1

    goto/16 :goto_5

    .line 410
    .restart local v30    # "firstVisiblePage":I
    :cond_f
    const/4 v2, 0x1

    goto/16 :goto_6

    .line 415
    .restart local v34    # "lastVisiblePage":I
    .restart local v39    # "page":I
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animating:Z

    if-nez v2, :cond_11

    if-eqz v42, :cond_12

    :cond_11
    const/4 v2, 0x1

    :goto_9
    return v2

    :cond_12
    const/4 v2, 0x0

    goto :goto_9
.end method

.method public setBackgroundColor(I)V
    .locals 5
    .param p1, "backgroundColor"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x437f0000    # 255.0f

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundFloats:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundFloats:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundFloats:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundFloats:[F

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 735
    iput p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mBackgroundColor:I

    .line 736
    iput-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCachedBorderBitmap:Landroid/graphics/Bitmap;

    .line 737
    iput-object v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mEmptyTexturePlacement:Lcom/google/android/apps/books/view/pages/TexturePlacement;

    .line 738
    return-void
.end method

.method public setCallback(Lcom/google/android/apps/books/view/pages/PageTurnCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/google/android/apps/books/view/pages/PageTurnCallback;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mCallback:Lcom/google/android/apps/books/view/pages/PageTurnCallback;

    .line 151
    return-void
.end method

.method public setDisplayTwoPages(Z)V
    .locals 3
    .param p1, "displayTwoPages"    # Z

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    .line 182
    const/4 v0, 0x0

    .local v0, "slot":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/view/pages/Page;->setBackFacing(Z)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    return-void
.end method

.method public setExecutingInitialLoadTransition(Z)V
    .locals 3
    .param p1, "executing"    # Z

    .prologue
    .line 754
    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mExecutingInitialLoadTransition:Z

    if-eq v1, p1, :cond_1

    .line 755
    const/4 v0, 0x0

    .local v0, "slot":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 756
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    aget-object v2, v1, v0

    if-nez p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/view/pages/Page;->setShowSpinnerWhenLoading(Z)V

    .line 755
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 756
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 759
    .end local v0    # "slot":I
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mExecutingInitialLoadTransition:Z

    .line 760
    return-void
.end method

.method public setPage(ILandroid/graphics/Bitmap;ZLcom/google/android/apps/books/view/pages/PageRenderDetails;Z)V
    .locals 6
    .param p1, "slot"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "validData"    # Z
    .param p4, "pageRenderDetails"    # Lcom/google/android/apps/books/view/pages/PageRenderDetails;
    .param p5, "potentialPadding"    # Z

    .prologue
    const-wide/16 v4, 0x0

    .line 590
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    aget-object v0, v1, p1

    .line 591
    .local v0, "page":Lcom/google/android/apps/books/view/pages/Page;
    invoke-virtual {v0, p2, p5, p4}, Lcom/google/android/apps/books/view/pages/Page;->updateTexture(Landroid/graphics/Bitmap;ZLcom/google/android/apps/books/view/pages/PageRenderDetails;)V

    .line 593
    if-eqz p3, :cond_1

    .line 597
    iget-wide v2, v0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 598
    iget-wide v2, p4, Lcom/google/android/apps/books/view/pages/PageRenderDetails;->transitionStartTime:J

    iput-wide v2, v0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    .line 609
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iput-wide v4, v0, Lcom/google/android/apps/books/view/pages/Page;->timestampData:J

    .line 607
    iput-wide v4, v0, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    goto :goto_0
.end method

.method public setPageToEmpty(I)V
    .locals 2
    .param p1, "slot"    # I

    .prologue
    const/4 v1, 0x0

    .line 585
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->invalidateTexture(IZZLandroid/graphics/Point;)V

    .line 586
    return-void
.end method

.method public setPageToLoading(ILandroid/graphics/Point;)V
    .locals 2
    .param p1, "slot"    # I
    .param p2, "displaySize"    # Landroid/graphics/Point;

    .prologue
    const/4 v1, 0x1

    .line 581
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mExecutingInitialLoadTransition:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->invalidateTexture(IZZLandroid/graphics/Point;)V

    .line 582
    return-void

    .line 581
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 8
    .param p1, "currentPageIndex"    # I
    .param p2, "inFractionTurned"    # F
    .param p3, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p4, "movingToNewPosition"    # Z

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    .line 531
    iget v2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    .line 532
    .local v2, "previousPageIndex":I
    iput p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    .line 533
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p3, v3, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float p2, v3, p2

    .end local p2    # "inFractionTurned":F
    :cond_0
    iput p2, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->fractionTurned:F

    .line 535
    iput-object p3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 536
    iput-object p3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animationOrientation:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 537
    iput-boolean v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->animating:Z

    .line 541
    if-ne p1, v2, :cond_1

    if-eqz p4, :cond_2

    .line 542
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 543
    .local v0, "now":J
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    aget-object v3, v3, v4

    iput-wide v6, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 544
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    iput-wide v6, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 545
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    iput-wide v6, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 546
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    iput-wide v6, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 547
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    iput-wide v6, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 548
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    iput-wide v6, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 549
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    iget v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v4, v4, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMod(II)I

    move-result v4

    aget-object v3, v3, v4

    iput-wide v0, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 550
    iget-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    if-eqz v3, :cond_2

    .line 551
    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageWheel:[Lcom/google/android/apps/books/view/pages/Page;

    iget v4, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mainPageIndex:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageMod(II)I

    move-result v4

    aget-object v3, v3, v4

    iput-wide v0, v3, Lcom/google/android/apps/books/view/pages/Page;->timestampDisplaying:J

    .line 554
    .end local v0    # "now":J
    :cond_2
    return-void
.end method

.method public setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V
    .locals 0
    .param p1, "direction"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    .line 158
    return-void
.end method

.method public startBookmarkAnimate(IZ)V
    .locals 1
    .param p1, "slot"    # I
    .param p2, "isGainingBookmark"    # Z

    .prologue
    .line 577
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->getPageWheel(I)Lcom/google/android/apps/books/view/pages/Page;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/Page;->mBookmarkAnimator:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->startAnimatingBookmark(Z)V

    .line 578
    return-void
.end method

.method public zoom(FFF)V
    .locals 2
    .param p1, "scale"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mDisplayTwoPages:Z

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setZoom(FFFZ)V

    .line 574
    return-void
.end method
