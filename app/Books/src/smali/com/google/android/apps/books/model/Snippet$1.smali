.class final Lcom/google/android/apps/books/model/Snippet$1;
.super Ljava/lang/Object;
.source "Snippet.java"

# interfaces
.implements Lcom/google/android/apps/books/model/Snippet$Builder;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/Snippet;->builder(II)Lcom/google/android/apps/books/model/Snippet$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$color:I

.field final synthetic val$style:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lcom/google/android/apps/books/model/Snippet$1;->val$color:I

    iput p2, p0, Lcom/google/android/apps/books/model/Snippet$1;->val$style:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build(Ljava/lang/String;Ljava/util/regex/Matcher;)Lcom/google/android/apps/books/model/Snippet;
    .locals 7
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "matcher"    # Ljava/util/regex/Matcher;

    .prologue
    const/4 v6, 0x0

    .line 40
    invoke-static {p1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    .line 42
    .local v4, "string":Landroid/text/SpannableString;
    :cond_0
    new-instance v0, Landroid/text/style/BackgroundColorSpan;

    iget v5, p0, Lcom/google/android/apps/books/model/Snippet$1;->val$color:I

    invoke-direct {v0, v5}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    .line 43
    .local v0, "bcs":Landroid/text/style/BackgroundColorSpan;
    new-instance v2, Landroid/text/style/StyleSpan;

    iget v5, p0, Lcom/google/android/apps/books/model/Snippet$1;->val$style:I

    invoke-direct {v2, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 44
    .local v2, "ss":Landroid/text/style/StyleSpan;
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    .line 45
    .local v3, "start":I
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    .line 46
    .local v1, "end":I
    invoke-virtual {v4, v2, v3, v1, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 47
    invoke-virtual {v4, v0, v3, v1, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 49
    invoke-virtual {p2}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-nez v5, :cond_0

    .line 50
    new-instance v5, Lcom/google/android/apps/books/model/Snippet;

    invoke-direct {v5, v4}, Lcom/google/android/apps/books/model/Snippet;-><init>(Landroid/text/Spanned;)V

    return-object v5
.end method
