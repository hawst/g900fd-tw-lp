.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$7;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNoteAtCharOffset(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$7;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$7;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1200(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestLayout()V

    .line 569
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 558
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 561
    return-void
.end method
