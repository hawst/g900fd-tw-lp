.class Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;
.super Landroid/os/AsyncTask;
.source "FootnoteCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->loadLinkCards(Lcom/google/android/apps/books/render/TouchableItem;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$item:Lcom/google/android/apps/books/render/TouchableItem;

.field final synthetic val$position:Lcom/google/android/apps/books/common/Position;

.field final synthetic val$screenHeight:I

.field final synthetic val$segment:Lcom/google/android/apps/books/model/Segment;

.field final synthetic val$textZoom:F

.field final synthetic val$theme:Ljava/lang/String;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/render/TouchableItem;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;FILcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    iput-object p2, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iput-object p4, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    iput-object p5, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$theme:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$position:Lcom/google/android/apps/books/common/Position;

    iput p7, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$textZoom:F

    iput p8, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$screenHeight:I

    iput-object p9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 12
    .param p1, "voids"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    :try_start_0
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    # getter for: Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v9}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->access$000(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$volumeId:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-static {v9, v10, v11}, Lcom/google/android/apps/books/data/DataControllerUtils;->getSegmentContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;)Ljava/lang/String;

    move-result-object v8

    .line 114
    .local v8, "segmentContent":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    iget-object v9, v9, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    invoke-virtual {v9}, Lcom/google/android/apps/books/render/TouchableItem;->getUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v5

    .line 116
    .local v5, "footnoteId":Ljava/lang/String;
    :goto_0
    new-instance v9, Lcom/google/android/apps/books/model/FootnoteExtractor;

    const-string v10, "bookcontent"

    invoke-direct {v9, v8, v5, v10}, Lcom/google/android/apps/books/model/FootnoteExtractor;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/google/android/apps/books/model/FootnoteExtractor;->getFootnote()Ljava/lang/String;

    move-result-object v4

    .line 119
    .local v4, "footnoteContent":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 120
    const/4 v9, 0x0

    invoke-static {v9}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v9

    .line 149
    .end local v4    # "footnoteContent":Ljava/lang/String;
    .end local v5    # "footnoteId":Ljava/lang/String;
    .end local v8    # "segmentContent":Ljava/lang/String;
    :goto_1
    return-object v9

    .line 114
    .restart local v8    # "segmentContent":Ljava/lang/String;
    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    iget-object v5, v9, Lcom/google/android/apps/books/render/TouchableItem;->originalId:Ljava/lang/String;

    goto :goto_0

    .line 123
    .restart local v4    # "footnoteContent":Ljava/lang/String;
    .restart local v5    # "footnoteId":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v9, "<head><style>"

    invoke-direct {v3, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 126
    .local v3, "footnoteBuilder":Ljava/lang/StringBuilder;
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    # getter for: Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v9}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->access$100(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegmentIdToCssIndices()Ljava/util/Map;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v10}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 128
    .local v1, "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_2

    .line 129
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 130
    .local v6, "i":Ljava/lang/Integer;
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    # getter for: Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;
    invoke-static {v9}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->access$200(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/render/ReaderDataSource;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/google/android/apps/books/render/ReaderDataSource;->getCssContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "css":Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 148
    .end local v0    # "css":Ljava/lang/String;
    .end local v1    # "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .end local v3    # "footnoteBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "footnoteContent":Ljava/lang/String;
    .end local v5    # "footnoteId":Ljava/lang/String;
    .end local v6    # "i":Ljava/lang/Integer;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "segmentContent":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 149
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v9

    goto :goto_1

    .line 135
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .restart local v3    # "footnoteBuilder":Ljava/lang/StringBuilder;
    .restart local v4    # "footnoteContent":Ljava/lang/String;
    .restart local v5    # "footnoteId":Ljava/lang/String;
    .restart local v8    # "segmentContent":Ljava/lang/String;
    :cond_2
    :try_start_1
    const-string v9, "\nbody { font-family: serif; }</style>"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "<link href=\'override.css\' type=\'text/css\' rel=\'stylesheet\'/>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</head><body id=\'footnoteBody\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$theme:Ljava/lang/String;

    const-string v10, "2"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 139
    const-string v9, " class=\'sepia-mode\'"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_3
    :goto_3
    const-string v9, ">"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</body>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v9

    goto/16 :goto_1

    .line 140
    :cond_4
    iget-object v9, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$theme:Ljava/lang/String;

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 141
    const-string v9, " class=\'footnote-night-mode\'"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 108
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 156
    .local v2, "footnote":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    iget-object v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$position:Lcom/google/android/apps/books/common/Position;

    iget-object v3, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$item:Lcom/google/android/apps/books/render/TouchableItem;

    iget-object v3, v3, Lcom/google/android/apps/books/render/TouchableItem;->linkText:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$textZoom:F

    iget-object v5, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$theme:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$screenHeight:I

    iget-object v7, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    # invokes: Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->loadContent(Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->access$300(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V

    .line 161
    .end local v2    # "footnote":Ljava/lang/String;
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    sget-object v1, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 108
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;->onPostExecute(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
