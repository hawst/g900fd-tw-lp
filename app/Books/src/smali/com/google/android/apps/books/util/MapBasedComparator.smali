.class public Lcom/google/android/apps/books/util/MapBasedComparator;
.super Ljava/lang/Object;
.source "MapBasedComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U::",
        "Ljava/lang/Comparable",
        "<TU;>;>",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mDefaultValue:Ljava/lang/Comparable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TU;"
        }
    .end annotation
.end field

.field private final mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;TU;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/lang/Comparable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TT;TU;>;TU;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/apps/books/util/MapBasedComparator;, "Lcom/google/android/apps/books/util/MapBasedComparator<TT;TU;>;"
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<TT;TU;>;"
    .local p2, "defaultValue":Ljava/lang/Comparable;, "TU;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/util/MapBasedComparator;->mMap:Ljava/util/Map;

    .line 19
    iput-object p2, p0, Lcom/google/android/apps/books/util/MapBasedComparator;->mDefaultValue:Ljava/lang/Comparable;

    .line 20
    return-void
.end method

.method private get(Ljava/lang/Object;)Ljava/lang/Comparable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TU;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/apps/books/util/MapBasedComparator;, "Lcom/google/android/apps/books/util/MapBasedComparator<TT;TU;>;"
    .local p1, "key":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/MapBasedComparator;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 31
    .local v0, "fromMap":Ljava/lang/Comparable;, "TU;"
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/MapBasedComparator;->mDefaultValue:Ljava/lang/Comparable;

    .end local v0    # "fromMap":Ljava/lang/Comparable;, "TU;"
    :cond_0
    return-object v0
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/apps/books/util/MapBasedComparator;, "Lcom/google/android/apps/books/util/MapBasedComparator<TT;TU;>;"
    .local p1, "lhs":Ljava/lang/Object;, "TT;"
    .local p2, "rhs":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MapBasedComparator;->get(Ljava/lang/Object;)Ljava/lang/Comparable;

    move-result-object v0

    .line 25
    .local v0, "leftIndex":Ljava/lang/Comparable;, "TU;"
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/util/MapBasedComparator;->get(Ljava/lang/Object;)Ljava/lang/Comparable;

    move-result-object v1

    .line 26
    .local v1, "rightIndex":Ljava/lang/Comparable;, "TU;"
    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    return v2
.end method
