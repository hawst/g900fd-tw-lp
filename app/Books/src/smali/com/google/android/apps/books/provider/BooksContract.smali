.class public Lcom/google/android/apps/books/provider/BooksContract;
.super Ljava/lang/Object;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/BooksContract$Files;,
        Lcom/google/android/apps/books/provider/BooksContract$Configuration;,
        Lcom/google/android/apps/books/provider/BooksContract$AccountContent;,
        Lcom/google/android/apps/books/provider/BooksContract$Accounts;,
        Lcom/google/android/apps/books/provider/BooksContract$SessionKeys;,
        Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;,
        Lcom/google/android/apps/books/provider/BooksContract$Collections;,
        Lcom/google/android/apps/books/provider/BooksContract$VolumeStates;,
        Lcom/google/android/apps/books/provider/BooksContract$Pages;,
        Lcom/google/android/apps/books/provider/BooksContract$SharedResources;,
        Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;,
        Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;,
        Lcom/google/android/apps/books/provider/BooksContract$Resources;,
        Lcom/google/android/apps/books/provider/BooksContract$Chapters;,
        Lcom/google/android/apps/books/provider/BooksContract$Segments;,
        Lcom/google/android/apps/books/provider/BooksContract$Volumes;,
        Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumeColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumeBaseColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$VolumeStatesColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$PageColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$ResourceResourceColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$SegmentResourceColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$ResourceColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$ChapterColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$SegmentColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;,
        Lcom/google/android/apps/books/provider/BooksContract$VolumeColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$BaseVolumeColumns;,
        Lcom/google/android/apps/books/provider/BooksContract$BooksUri;,
        Lcom/google/android/apps/books/provider/BooksContract$PathParam;
    }
.end annotation


# static fields
.field public static final BASE_CONTENT_URI:Landroid/net/Uri;

.field public static final BOOKSWEB_CONTENT_URI:Landroid/net/Uri;

.field private static final sMatchCodeToUri:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/provider/BooksContract$BooksUri;",
            ">;"
        }
    .end annotation
.end field

.field private static sUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sMatchCodeToUri:Landroid/util/SparseArray;

    .line 296
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sUriMatcher:Landroid/content/UriMatcher;

    .line 445
    const-string v0, "content://com.google.android.apps.books"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract;->BASE_CONTENT_URI:Landroid/net/Uri;

    .line 448
    const-string v0, "books-content://com.google.android.apps.books"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract;->BOOKSWEB_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$000()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sMatchCodeToUri:Landroid/util/SparseArray;

    return-object v0
.end method

.method private static buildUriMatcher()Landroid/content/UriMatcher;
    .locals 9

    .prologue
    .line 373
    new-instance v4, Landroid/content/UriMatcher;

    const/4 v6, -0x1

    invoke-direct {v4, v6}, Landroid/content/UriMatcher;-><init>(I)V

    .line 374
    .local v4, "matcher":Landroid/content/UriMatcher;
    const-string v1, "com.google.android.apps.books"

    .line 376
    .local v1, "authority":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->values()[Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    .line 377
    .local v5, "uri":Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    const-string v6, "com.google.android.apps.books"

    iget-object v7, v5, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->matchPath:Ljava/lang/String;

    iget v8, v5, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->code:I

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 376
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 380
    .end local v5    # "uri":Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    :cond_0
    return-object v4
.end method

.method public static getUriType(Landroid/net/Uri;)Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 299
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sMatchCodeToUri:Landroid/util/SparseArray;

    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract;->match(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    return-object v0
.end method

.method public static markAsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 433
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "callerIsSyncAdapter"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static match(Landroid/net/Uri;)I
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 392
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sUriMatcher:Landroid/content/UriMatcher;

    if-nez v0, :cond_0

    .line 393
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract;->buildUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sUriMatcher:Landroid/content/UriMatcher;

    .line 395
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method
