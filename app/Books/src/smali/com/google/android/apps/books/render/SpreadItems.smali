.class public Lcom/google/android/apps/books/render/SpreadItems;
.super Ljava/lang/Object;
.source "SpreadItems.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mCapacity:I

.field private mFirst:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mSecond:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "twoPageSpreads"    # Z

    .prologue
    .line 21
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    .line 23
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private checkBounds(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 55
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    if-lt p1, v0, :cond_1

    .line 56
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 58
    :cond_1
    return-void
.end method


# virtual methods
.method public fill(Ljava/lang/Object;Z)V
    .locals 0
    .param p2, "twoPageSpreads"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-eqz p2, :cond_0

    .line 74
    invoke-virtual {p0, p1, p1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/SpreadItems;->checkBounds(I)V

    .line 43
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mFirst:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mSecond:Ljava/lang/Object;

    goto :goto_0
.end method

.method public getFirst()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mFirst:Ljava/lang/Object;

    return-object v0
.end method

.method public getLast()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    const/4 v0, 0x1

    .line 47
    iget v1, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public set(Lcom/google/android/apps/books/render/SpreadItems;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    .local p1, "other":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<+TT;>;"
    iget v0, p1, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    iput v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    .line 30
    iget-object v0, p1, Lcom/google/android/apps/books/render/SpreadItems;->mFirst:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mFirst:Ljava/lang/Object;

    .line 31
    iget-object v0, p1, Lcom/google/android/apps/books/render/SpreadItems;->mSecond:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mSecond:Ljava/lang/Object;

    .line 32
    return-void
.end method

.method public setItems(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    .local p1, "first":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/books/render/SpreadItems;->mFirst:Ljava/lang/Object;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mSecond:Ljava/lang/Object;

    .line 70
    return-void
.end method

.method public setItems(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    .local p1, "first":Ljava/lang/Object;, "TT;"
    .local p2, "second":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/books/render/SpreadItems;->mFirst:Ljava/lang/Object;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/books/render/SpreadItems;->mSecond:Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<TT;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/SpreadItems;->mCapacity:I

    return v0
.end method
