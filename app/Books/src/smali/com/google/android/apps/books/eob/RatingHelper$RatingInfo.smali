.class public Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
.super Ljava/lang/Object;
.source "RatingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/eob/RatingHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RatingInfo"
.end annotation


# instance fields
.field public final authorProfileImageUrl:Ljava/lang/String;

.field public final authorTitle:Ljava/lang/String;

.field public final docTitle:Ljava/lang/String;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field public final rating:I

.field public final requestCode:I

.field public final reviewComment:Ljava/lang/String;

.field public final reviewTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    const-string v0, "author_profile_image_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->authorProfileImageUrl:Ljava/lang/String;

    .line 186
    const-string v0, "author_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->authorTitle:Ljava/lang/String;

    .line 187
    const-string v0, "review_comment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->reviewComment:Ljava/lang/String;

    .line 188
    const-string v0, "review_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->reviewTitle:Ljava/lang/String;

    .line 189
    const-string v0, "rating"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    .line 190
    const-string v0, "rate_and_review_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->mPendingIntent:Landroid/app/PendingIntent;

    .line 191
    const-string v0, "rate_and_review_request_code"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->requestCode:I

    .line 192
    const-string v0, "doc_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->docTitle:Ljava/lang/String;

    .line 193
    return-void
.end method


# virtual methods
.method public maybeLaunchRating(Landroid/app/Activity;)Z
    .locals 10
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->mPendingIntent:Landroid/app/PendingIntent;

    if-nez v0, :cond_1

    .line 203
    const-string v0, "RatingHelper"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const-string v0, "RatingHelper"

    const-string v1, "maybeLaunchRating failed: no PI"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v8

    .line 220
    :goto_0
    return v0

    .line 209
    :cond_1
    :try_start_0
    const-string v0, "RatingHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    const-string v0, "RatingHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeLaunchRating startIntent, info.rating was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->requestCode:I

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    const/4 v0, 0x1

    goto :goto_0

    .line 215
    :catch_0
    move-exception v7

    .line 216
    .local v7, "e":Landroid/content/IntentSender$SendIntentException;
    const-string v0, "RatingHelper"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217
    const-string v0, "RatingHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeLaunchRating startIntent... failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/content/IntentSender$SendIntentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    move v0, v8

    .line 220
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 225
    invoke-static {p0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "docTitle"

    iget-object v2, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->docTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "rating"

    iget v2, p0, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
