.class public Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;
.super Ljava/lang/Object;
.source "ServerAnnotationAdditionReceipt.java"


# instance fields
.field public final annotationFromServer:Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

.field public final receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)V
    .locals 0
    .param p1, "receipt"    # Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    .param p2, "annotationFromServer"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    .line 18
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->annotationFromServer:Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .line 19
    return-void
.end method
