.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getManifestOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$volumeId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method

.method private ensureSharedResourcesOnControlThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p4, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 602
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;

    invoke-direct {v0, p0, p1, p4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$1;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 614
    .local v0, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;

    invoke-direct {v1, p0, p2, p4, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {p3, v1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 621
    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 586
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$volumeId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getVolumeManifest(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v1

    .line 587
    .local v1, "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->verifyManifest(Lcom/google/android/apps/books/model/VolumeManifest;)V
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1900(Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 589
    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->hasSharedResources(Lcom/google/android/apps/books/model/VolumeManifest;)Z
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2000(Lcom/google/android/apps/books/model/VolumeManifest;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 590
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0, v2, v3, p1, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->ensureSharedResourcesOnControlThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 597
    .end local v1    # "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    :goto_0
    return-void

    .line 592
    .restart local v1    # "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifestOnMainThread(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 594
    .end local v1    # "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishManifestExceptionOnMainThread(Ljava/lang/String;Ljava/lang/Exception;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
