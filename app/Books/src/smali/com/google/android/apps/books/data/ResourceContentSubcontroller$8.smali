.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$resource:Lcom/google/android/apps/books/model/Resource;

.field final synthetic val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;)V
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getResource(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 510
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, v2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 511
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->resourceRetriever:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;->get(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Resource;

    .line 512
    .local v1, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 517
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "resource":Lcom/google/android/apps/books/model/Resource;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 495
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v2}, Lcom/google/android/apps/books/data/DataControllerBlob;->save()V

    .line 496
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iget-object v2, v2, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->resourceRetriever:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;->get(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v1

    .line 497
    .local v1, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, v3, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->setResources(Ljava/lang/String;Ljava/util/Collection;)V

    .line 498
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$resource:Lcom/google/android/apps/books/model/Resource;

    iget-object v4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {v2, v3, v1, p1, v4}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->saveFontAssociations(Lcom/google/android/apps/books/model/Resource;Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)V

    .line 499
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1500(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    sget-object v4, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1600(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    .end local v1    # "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    :goto_0
    return-void

    .line 500
    :catch_0
    move-exception v0

    .line 501
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1500(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 503
    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1600(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1600(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v2
.end method
