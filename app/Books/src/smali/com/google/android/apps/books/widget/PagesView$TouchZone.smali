.class public final enum Lcom/google/android/apps/books/widget/PagesView$TouchZone;
.super Ljava/lang/Enum;
.source "PagesView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TouchZone"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/widget/PagesView$TouchZone;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/widget/PagesView$TouchZone;

.field public static final enum BOOKMARK:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

.field public static final enum CENTER:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

.field public static final enum LEFT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

.field public static final enum RIGHT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 127
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    const-string v1, "LEFT_EDGE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/widget/PagesView$TouchZone;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->LEFT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 128
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    const-string v1, "RIGHT_EDGE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/widget/PagesView$TouchZone;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->RIGHT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 129
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/widget/PagesView$TouchZone;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->CENTER:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 130
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    const-string v1, "BOOKMARK"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/widget/PagesView$TouchZone;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->BOOKMARK:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    .line 126
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->LEFT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->RIGHT_EDGE:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->CENTER:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->BOOKMARK:Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->$VALUES:[Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 126
    const-class v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/widget/PagesView$TouchZone;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/apps/books/widget/PagesView$TouchZone;->$VALUES:[Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/widget/PagesView$TouchZone;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/widget/PagesView$TouchZone;

    return-object v0
.end method
