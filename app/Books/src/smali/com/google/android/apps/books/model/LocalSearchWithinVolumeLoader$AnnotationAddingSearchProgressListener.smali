.class Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;
.super Ljava/lang/Object;
.source "LocalSearchWithinVolumeLoader.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnnotationAddingSearchProgressListener"
.end annotation


# instance fields
.field private final mAnnotationsInSegment:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentAnnotationIndex:I

.field private final mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

.field final synthetic this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p2, "annotationsInSegment":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mCurrentAnnotationIndex:I

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mAnnotationsInSegment:Lcom/google/common/collect/ImmutableList;

    .line 60
    # getter for: Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mAnnotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;
    invoke-static {p1}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->access$000(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Lcom/google/android/apps/books/geo/AnnotationSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/geo/AnnotationSet;->getPageOrdering()Lcom/google/android/apps/books/common/Position$PageOrdering;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    .line 61
    return-void
.end method

.method private getNextCandidateAnnotation()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mAnnotationsInSegment:Lcom/google/common/collect/ImmutableList;

    iget v1, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mCurrentAnnotationIndex:I

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    return-object v0
.end method

.method private hasMoreAnnotations()Z
    .locals 2

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mCurrentAnnotationIndex:I

    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mAnnotationsInSegment:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasMoreAnnotationsBefore(Lcom/google/android/apps/books/annotations/TextLocation;)Z
    .locals 3
    .param p1, "beforeThis"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->hasMoreAnnotations()Z

    move-result v2

    if-nez v2, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 84
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->getNextCandidateAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 86
    .local v0, "nextNoteStart":Lcom/google/android/apps/books/annotations/TextLocation;
    iget-object v2, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-static {v0, p1, v2}, Lcom/google/android/apps/books/annotations/TextLocation;->compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v2

    if-gez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onSearchedUpTo(Lcom/google/android/apps/books/annotations/TextLocation;Ljava/util/Collection;)V
    .locals 8
    .param p1, "beforeThis"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "queue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    const/4 v7, 0x0

    .line 65
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->hasMoreAnnotationsBefore(Lcom/google/android/apps/books/annotations/TextLocation;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->getNextCandidateAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 67
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "note":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 69
    iget-object v3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    # getter for: Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mPattern:Ljava/util/regex/Pattern;
    invoke-static {v3}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->access$100(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 70
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 71
    new-instance v3, Lcom/google/android/apps/books/model/SearchResult;

    iget-object v4, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->this$0:Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    # getter for: Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;
    invoke-static {v4}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->access$200(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Lcom/google/android/apps/books/model/Snippet$Builder;

    move-result-object v4

    invoke-interface {v4, v2, v1}, Lcom/google/android/apps/books/model/Snippet$Builder;->build(Ljava/lang/String;Ljava/util/regex/Matcher;)Lcom/google/android/apps/books/model/Snippet;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v5}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v6

    invoke-direct {v3, v4, v5, v7, v6}, Lcom/google/android/apps/books/model/SearchResult;-><init>(Lcom/google/android/apps/books/model/Snippet;Ljava/util/List;ZI)V

    invoke-interface {p2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 76
    .end local v1    # "m":Ljava/util/regex/Matcher;
    :cond_0
    iget v3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mCurrentAnnotationIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;->mCurrentAnnotationIndex:I

    goto :goto_0

    .line 78
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v2    # "note":Ljava/lang/String;
    :cond_1
    return-void
.end method
