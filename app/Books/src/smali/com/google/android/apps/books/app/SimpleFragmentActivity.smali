.class public abstract Lcom/google/android/apps/books/app/SimpleFragmentActivity;
.super Lcom/google/android/ublib/actionbar/UBLibActivity;
.source "SimpleFragmentActivity.java"


# instance fields
.field private mAddedFragments:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract addFragments()Z
.end method

.method protected maybeCreateFragments()V
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->mAddedFragments:Z

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->addFragments()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->mAddedFragments:Z

    .line 44
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    if-eqz p1, :cond_0

    .line 24
    const-string v0, "addedFragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->mAddedFragments:Z

    .line 26
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 37
    const-string v0, "addedFragments"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->mAddedFragments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 38
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStart()V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->maybeCreateFragments()V

    .line 32
    return-void
.end method
