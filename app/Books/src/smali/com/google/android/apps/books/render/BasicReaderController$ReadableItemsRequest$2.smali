.class Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionFailureHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mExternalRequestId:I

    const/4 v2, -0x1

    move-object v4, v3

    move-object v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/ReaderListener;->onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$2;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mTaskRequestId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    return-void
.end method
