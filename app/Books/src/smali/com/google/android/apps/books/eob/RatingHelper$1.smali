.class final Lcom/google/android/apps/books/eob/RatingHelper$1;
.super Ljava/lang/Object;
.source "RatingHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/RatingHelper;->requestRatingInfo(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private mServiceDisconnected:Z

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$readyListener:Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;)V
    .locals 1

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$accountName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$activity:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$readyListener:Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->mServiceDisconnected:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/eob/RatingHelper$1;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/RatingHelper$1;

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->mServiceDisconnected:Z

    return v0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 75
    const-string v2, "RatingHelper"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    const-string v2, "RatingHelper"

    const-string v3, "onServiceConnected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_0
    invoke-static {p2}, Lcom/android/vending/reviews/IReviewsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/vending/reviews/IReviewsService;

    move-result-object v0

    .line 79
    .local v0, "reviewsService":Lcom/android/vending/reviews/IReviewsService;
    move-object v1, p0

    .line 80
    .local v1, "thisServiceConnection":Landroid/content/ServiceConnection;
    new-instance v3, Lcom/google/android/apps/books/eob/RatingHelper$1$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/books/eob/RatingHelper$1$1;-><init>(Lcom/google/android/apps/books/eob/RatingHelper$1;Lcom/android/vending/reviews/IReviewsService;Landroid/content/ServiceConnection;)V

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/Void;

    const/4 v5, 0x0

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/Void;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 132
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 136
    const-string v0, "RatingHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "RatingHelper"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/eob/RatingHelper$1;->mServiceDisconnected:Z

    .line 141
    return-void
.end method
