.class Lcom/google/android/apps/books/render/WebViewRendererModel$2;
.super Ljava/lang/Object;
.source "WebViewRendererModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/WebViewRendererModel;->sendOnRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

.field final synthetic val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

.field final synthetic val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    .locals 0

    .prologue
    .line 1146
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iput-object p2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iput-object p3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1149
    new-instance v0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mViewLayerType:I
    invoke-static {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$2000(Lcom/google/android/apps/books/render/WebViewRendererModel;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mActivePages:Ljava/util/Set;
    invoke-static {v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$2100(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Set;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;Lcom/google/android/apps/books/widget/DevicePageRendering;ILjava/util/Set;)V

    .line 1151
    .local v0, "painter":Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->val$consumer:Lcom/google/android/apps/books/render/RenderResponseConsumer;

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/books/render/RenderResponseConsumer;->onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V

    .line 1152
    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->mRecycled:Z
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;->access$2200(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewPagePainter;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1153
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$2;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mActivePages:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$2100(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1155
    :cond_0
    return-void
.end method
