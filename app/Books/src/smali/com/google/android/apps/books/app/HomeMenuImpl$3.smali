.class Lcom/google/android/apps/books/app/HomeMenuImpl$3;
.super Ljava/lang/Object;
.source "HomeMenuImpl.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeMenuImpl;->configureSearchView(Landroid/content/Context;Landroid/support/v7/widget/SearchView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

.field final synthetic val$searchView:Landroid/support/v7/widget/SearchView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeMenuImpl;Landroid/support/v7/widget/SearchView;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->val$searchView:Landroid/support/v7/widget/SearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 202
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # setter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mLastQuery:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$102(Lcom/google/android/apps/books/app/HomeMenuImpl;Ljava/lang/String;)Ljava/lang/String;

    .line 205
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->val$searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/HomeMenuImpl;->setSearchViewExpanded(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$200(Lcom/google/android/apps/books/app/HomeMenuImpl;Z)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/HomeMenu$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$000(Lcom/google/android/apps/books/app/HomeMenuImpl;)Lcom/google/android/apps/books/app/HomeMenu$Callbacks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/HomeMenu$Callbacks;->startSearch(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/HomeMenuImpl;

    # setter for: Lcom/google/android/apps/books/app/HomeMenuImpl;->mLastQuery:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/apps/books/app/HomeMenuImpl;->access$102(Lcom/google/android/apps/books/app/HomeMenuImpl;Ljava/lang/String;)Ljava/lang/String;

    .line 219
    const/4 v0, 0x1

    return v0
.end method
