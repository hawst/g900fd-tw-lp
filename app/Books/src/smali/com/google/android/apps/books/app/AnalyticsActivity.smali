.class public abstract Lcom/google/android/apps/books/app/AnalyticsActivity;
.super Lcom/google/android/ublib/actionbar/UBLibActivity;
.source "AnalyticsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onStart()V
    .locals 0

    .prologue
    .line 12
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStart()V

    .line 13
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStart(Landroid/app/Activity;)V

    .line 14
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStop()V

    .line 19
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStop(Landroid/app/Activity;)V

    .line 20
    return-void
.end method
