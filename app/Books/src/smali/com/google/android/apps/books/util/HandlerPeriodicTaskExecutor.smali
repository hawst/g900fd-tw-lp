.class public Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;
.super Ljava/lang/Object;
.source "HandlerPeriodicTaskExecutor.java"

# interfaces
.implements Lcom/google/android/apps/books/util/PeriodicTaskExecutor;


# static fields
.field private static final sFactory:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;


# instance fields
.field private final mCallbacks:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

.field private final mDelayMillis:J

.field private final mHandler:Landroid/os/Handler;

.field private mLastCallbackTime:J

.field private mNextAllowedCallbackTime:J

.field private mQueued:Z

.field private mStopped:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->sFactory:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V
    .locals 2
    .param p1, "callbacks"    # Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;
    .param p2, "delayMillis"    # J

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mCallbacks:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

    .line 48
    iput-wide p2, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mDelayMillis:J

    .line 49
    invoke-static {p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->createHandler(Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;)Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mHandler:Landroid/os/Handler;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->onHandlerMessage()Z

    move-result v0

    return v0
.end method

.method private cancelMessages()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 112
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mQueued:Z

    .line 113
    return-void
.end method

.method private static createHandler(Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;)Landroid/os/Handler;
    .locals 2
    .param p0, "executor"    # Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    .prologue
    .line 69
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor$2;-><init>(Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    return-object v0
.end method

.method public static getFactory()Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->sFactory:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;

    return-object v0
.end method

.method private onHandlerMessage()Z
    .locals 4

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mCallbacks:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

    invoke-interface {v1}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;->run()Z

    move-result v0

    .line 79
    .local v0, "schedule":Z
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mLastCallbackTime:J

    .line 80
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mQueued:Z

    .line 81
    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->schedule()V

    .line 84
    :cond_0
    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method public delay(J)V
    .locals 7
    .param p1, "delayMillis"    # J

    .prologue
    .line 95
    iget-boolean v1, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mStopped:Z

    if-eqz v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mNextAllowedCallbackTime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    add-long/2addr v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mNextAllowedCallbackTime:J

    .line 102
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mQueued:Z

    .line 103
    .local v0, "reschedule":Z
    invoke-direct {p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->cancelMessages()V

    .line 105
    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->schedule()V

    goto :goto_0
.end method

.method public schedule()V
    .locals 12

    .prologue
    .line 54
    iget-boolean v8, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mQueued:Z

    if-nez v8, :cond_1

    iget-boolean v8, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mStopped:Z

    if-nez v8, :cond_1

    .line 55
    iget-wide v8, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mLastCallbackTime:J

    iget-wide v10, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mDelayMillis:J

    add-long v6, v8, v10

    .line 56
    .local v6, "timeAfterNormalCallbackInterval":J
    iget-wide v4, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mNextAllowedCallbackTime:J

    .line 57
    .local v4, "timeAfterExplicitDelay":J
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 59
    .local v0, "callbackTime":J
    const-wide/16 v8, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long v10, v0, v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 60
    .local v2, "delay":J
    const-string v8, "HandlerScheduler"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 61
    const-string v8, "HandlerScheduler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Scheduling task after "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 64
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mQueued:Z

    .line 66
    .end local v0    # "callbackTime":J
    .end local v2    # "delay":J
    .end local v4    # "timeAfterExplicitDelay":J
    .end local v6    # "timeAfterNormalCallbackInterval":J
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->cancelMessages()V

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->mStopped:Z

    .line 91
    return-void
.end method
