.class public Lcom/google/android/apps/books/view/pages/TexturePlacement;
.super Ljava/lang/Object;
.source "TexturePlacement.java"


# instance fields
.field private mBufferValid:Z

.field private final mDebugName:Ljava/lang/String;

.field private final mTextureId:I

.field private final mTransformFloatBuffer:Ljava/nio/FloatBuffer;

.field private final mTransformMatrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(ILandroid/graphics/Matrix;Ljava/lang/String;)V
    .locals 1
    .param p1, "textureId"    # I
    .param p2, "transformMatrix"    # Landroid/graphics/Matrix;
    .param p3, "debugName"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/16 v0, 0x9

    invoke-static {v0}, Ljava/nio/FloatBuffer;->allocate(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTransformFloatBuffer:Ljava/nio/FloatBuffer;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mBufferValid:Z

    .line 20
    iput p1, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTextureId:I

    .line 21
    iput-object p2, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTransformMatrix:Landroid/graphics/Matrix;

    .line 22
    iput-object p3, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mDebugName:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "textureId"    # I
    .param p2, "debugName"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/view/pages/TexturePlacement;-><init>(ILandroid/graphics/Matrix;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method public getDebugName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mDebugName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTextureId:I

    return v0
.end method

.method public matrixFloatBuffer()Ljava/nio/FloatBuffer;
    .locals 2

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mBufferValid:Z

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTransformMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTransformFloatBuffer:Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->array()[F

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mBufferValid:Z

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTransformFloatBuffer:Ljava/nio/FloatBuffer;

    return-object v0
.end method

.method public mutableTransformMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mBufferValid:Z

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTransformMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mDebugName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/view/pages/TexturePlacement;->mTextureId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
