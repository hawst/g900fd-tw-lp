.class Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;
.super Lcom/google/android/apps/books/data/BasePendingAction;
.source "MyEbooksSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

.field final synthetic val$licenseResponse:Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

.field final synthetic val$myEbooksVolumes:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;->val$licenseResponse:Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

    iput-object p3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;->val$myEbooksVolumes:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    invoke-direct {p0}, Lcom/google/android/apps/books/data/BasePendingAction;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 256
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;->val$licenseResponse:Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->setMyEbooksVolumes(Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)Z

    move-result v2

    .line 258
    .local v2, "myEbooksChanged":Z
    iget-object v3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    invoke-static {v3}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$900(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 260
    if-eqz v2, :cond_0

    .line 261
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->copyListeners()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 262
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;->val$myEbooksVolumes:Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    iget-object v3, v3, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    invoke-interface {v1, v3}, Lcom/google/android/apps/books/model/BooksDataListener;->onMyEbooksVolumes(Ljava/util/List;)V

    goto :goto_0

    .line 265
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method
