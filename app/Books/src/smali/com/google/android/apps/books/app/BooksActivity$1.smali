.class Lcom/google/android/apps/books/app/BooksActivity$1;
.super Landroid/os/AsyncTask;
.source "BooksActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/BooksActivity;->createShortcut(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BooksActivity;

.field final synthetic val$shortcutIconSize:I

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BooksActivity;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    iput-object p2, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$volumeId:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$shortcutIconSize:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private getErrorBitmap(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "shortcutIconSize"    # I

    .prologue
    .line 333
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BooksActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f030000

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 335
    .local v0, "cover":Landroid/graphics/Bitmap;
    const/4 v1, 0x1

    invoke-static {v0, p1, p1, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public varargs doInBackground([Ljava/lang/Void;)Landroid/content/Intent;
    .locals 12
    .param p1, "unused"    # [Ljava/lang/Void;

    .prologue
    .line 295
    iget-object v8, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v8}, Lcom/google/android/apps/books/app/BooksActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 297
    .local v0, "account":Landroid/accounts/Account;
    const-string v6, ""

    .line 301
    .local v6, "title":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-static {v8, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    .line 303
    .local v2, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    iget-object v8, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$volumeId:Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x1

    sget-object v11, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v2, v8, v9, v10, v11}, Lcom/google/android/apps/books/data/DataControllerUtils;->getVolumeData(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;ZZLcom/google/android/apps/books/data/BooksDataController$Priority;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v7

    .line 306
    .local v7, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    sget-object v8, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v2, v7, v8}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 308
    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 312
    iget-object v8, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v8}, Lcom/google/android/apps/books/app/BooksActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/BooksActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$volumeId:Ljava/lang/String;

    invoke-static {v9, v10}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildCoverThumbnailUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v8

    iget v9, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$shortcutIconSize:I

    invoke-static {v8, v9}, Lcom/google/android/apps/books/util/ImageUtils;->getCover(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 321
    .end local v2    # "dc":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v7    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    .local v1, "cover":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v8, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    iget-object v9, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$volumeId:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v10}, Lcom/google/android/apps/books/app/BooksActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v10

    iget-object v10, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v8, v9, v10}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 324
    .local v5, "shortcutIntent":Landroid/content/Intent;
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 325
    .local v4, "returnIntent":Landroid/content/Intent;
    const-string v8, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v4, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 326
    const-string v8, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v4, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    const-string v8, "android.intent.extra.shortcut.ICON"

    invoke-virtual {v4, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 329
    return-object v4

    .line 316
    .end local v1    # "cover":Landroid/graphics/Bitmap;
    .end local v4    # "returnIntent":Landroid/content/Intent;
    .end local v5    # "shortcutIntent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    .line 317
    .local v3, "e":Ljava/lang/Exception;
    iget v8, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->val$shortcutIconSize:I

    invoke-direct {p0, v8}, Lcom/google/android/apps/books/app/BooksActivity$1;->getErrorBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .restart local v1    # "cover":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 292
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksActivity$1;->doInBackground([Ljava/lang/Void;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Landroid/content/Intent;)V
    .locals 3
    .param p1, "result"    # Landroid/content/Intent;

    .prologue
    .line 341
    if-eqz p1, :cond_1

    .line 342
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/books/app/BooksActivity;->setResult(ILandroid/content/Intent;)V

    .line 348
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "ProgressDialogFragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 350
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_0

    .line 352
    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .end local v0    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BooksActivity;->finish()V

    .line 355
    return-void

    .line 344
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/BooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BooksActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/BooksActivity;->setResult(I)V

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 292
    check-cast p1, Landroid/content/Intent;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BooksActivity$1;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method
