.class public Lcom/google/android/apps/books/data/ByteArrayCache;
.super Ljava/lang/Object;
.source "ByteArrayCache.java"


# instance fields
.field private mBytes:[B

.field private final mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/InputStreamSource;)V
    .locals 0
    .param p1, "inputStreamFactory"    # Lcom/google/android/apps/books/data/InputStreamSource;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/books/data/ByteArrayCache;->mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;

    .line 22
    return-void
.end method


# virtual methods
.method public openInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/data/ByteArrayCache;->mBytes:[B

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ByteArrayCache;->mBytes:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/ByteArrayCache;->mInputStreamSource:Lcom/google/android/apps/books/data/InputStreamSource;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/InputStreamSource;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0
.end method
