.class Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
.super Ljava/lang/Object;
.source "HtmlSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/HtmlSpreadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PageData"
.end annotation


# instance fields
.field mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

.field final mContentRect:Landroid/graphics/Rect;

.field final mContentScale:Lcom/google/android/apps/books/util/Holder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Holder",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field final mContentSize:Landroid/graphics/Point;

.field private final mLetterboxMargins:Landroid/graphics/Point;

.field final mMasks:[Landroid/graphics/Rect;

.field private final mPageFit:Landroid/graphics/Matrix;

.field final mPositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field final mProgressBar:Landroid/widget/ProgressBar;

.field final mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "positionOnScreen"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/4 v3, 0x1

    .line 89
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    .line 76
    new-instance v0, Lcom/google/android/apps/books/util/Holder;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/util/Holder;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentScale:Lcom/google/android/apps/books/util/Holder;

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentRect:Landroid/graphics/Rect;

    .line 84
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Rect;

    const/4 v1, 0x0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v0, v1

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPageFit:Landroid/graphics/Matrix;

    .line 87
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    .line 90
    iput-object p3, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 95
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 97
    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;
    invoke-static {p1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$000(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newWrapContentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    return-void

    .line 92
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPageFit:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private onContentChanged(Z)V
    .locals 2
    .param p1, "haveContent"    # Z

    .prologue
    .line 140
    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->updatePageTransform()V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->invalidatePageContent()V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # invokes: Lcom/google/android/apps/books/widget/HtmlSpreadView;->updateSpreadContentRect()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$200(Lcom/google/android/apps/books/widget/HtmlSpreadView;)V

    .line 145
    return-void

    .line 140
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 2
    .param p1, "drawable"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p2, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mDecorationsView:Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$000(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView$MyDecorationsGroup;->attachBookmarkAnimator(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 103
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->onContentChanged(Z)V

    .line 104
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setLoading(Landroid/graphics/Point;)V
    .locals 3
    .param p1, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    .line 108
    if-eqz p1, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 113
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->onContentChanged(Z)V

    .line 114
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method

.method setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V
    .locals 7
    .param p1, "pageType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p2, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p3, "displaySize"    # Landroid/graphics/Point;

    .prologue
    const/4 v6, 0x0

    .line 119
    iget-object v4, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    invoke-interface {p2, v4}, Lcom/google/android/apps/books/util/SimpleDrawable;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 120
    iget-object v4, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    invoke-virtual {v4, p3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 122
    move-object v0, p2

    .line 135
    .local v0, "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    .line 136
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->onContentChanged(Z)V

    .line 137
    return-void

    .line 123
    .end local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    :cond_0
    sget-object v4, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-ne p1, v4, :cond_1

    .line 124
    iget v4, p3, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 125
    .local v1, "leftOffset":F
    iget v4, p3, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 126
    .local v3, "topOffset":F
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 127
    .local v2, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 128
    new-instance v0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;

    invoke-direct {v0, p2, p3, v2}, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;Landroid/graphics/Matrix;)V

    .line 129
    .restart local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    goto :goto_0

    .line 130
    .end local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    .end local v1    # "leftOffset":F
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v3    # "topOffset":F
    :cond_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 131
    .restart local v2    # "matrix":Landroid/graphics/Matrix;
    iget v4, p3, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    iget v5, p3, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 133
    new-instance v0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;

    invoke-direct {v0, p2, p3, v2}, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;Landroid/graphics/Matrix;)V

    .restart local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    goto :goto_0
.end method

.method updatePageTransform()V
    .locals 19

    .prologue
    .line 149
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    sget-object v15, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    if-ne v14, v15, :cond_0

    const/4 v7, 0x1

    .line 150
    .local v7, "isLeft":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    sget-object v15, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    if-ne v14, v15, :cond_1

    const/4 v8, 0x1

    .line 154
    .local v8, "isRight":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    .local v1, "arr$":[Landroid/graphics/Rect;
    array-length v9, v1

    .local v9, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_2
    if-ge v6, v9, :cond_2

    aget-object v11, v1, v6

    .line 155
    .local v11, "mask":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->setEmpty()V

    .line 154
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 149
    .end local v1    # "arr$":[Landroid/graphics/Rect;
    .end local v6    # "i$":I
    .end local v7    # "isLeft":Z
    .end local v8    # "isRight":Z
    .end local v9    # "len$":I
    .end local v11    # "mask":Landroid/graphics/Rect;
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 150
    .restart local v7    # "isLeft":Z
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 158
    .restart local v1    # "arr$":[Landroid/graphics/Rect;
    .restart local v6    # "i$":I
    .restart local v8    # "isRight":Z
    .restart local v9    # "len$":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    if-eqz v14, :cond_8

    .line 159
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    invoke-interface {v14, v15}, Lcom/google/android/apps/books/util/SimpleDrawable;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 161
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v15}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v15

    iget v15, v15, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v16, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentScale:Lcom/google/android/apps/books/util/Holder;

    move-object/from16 v17, v0

    invoke-static/range {v14 .. v17}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;IILcom/google/android/apps/books/util/Holder;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v14, v14, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->x:I

    if-le v14, v15, :cond_5

    .line 167
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v14, v14, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->x:I

    sub-int v10, v14, v15

    .line 168
    .local v10, "marginsX":I
    if-eqz v8, :cond_3

    .line 169
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v12, v14, Landroid/graphics/Point;->x:I

    .line 170
    .local v12, "pageTx":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    iput v10, v14, Landroid/graphics/Point;->x:I

    .line 171
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->x:I

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    add-int v17, v17, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    .line 189
    .end local v10    # "marginsX":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v14, v14, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->y:I

    if-le v14, v15, :cond_7

    .line 190
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v14, v14, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->y:I

    sub-int/2addr v14, v15

    div-int/lit8 v13, v14, 0x2

    .line 192
    .local v13, "pageTy":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    const/4 v15, 0x0

    neg-int v0, v13

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    .line 193
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    const/4 v15, 0x1

    aget-object v14, v14, v15

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v17

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    add-int v18, v18, v13

    add-int/lit8 v18, v18, 0x1

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    .line 198
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    iput v13, v14, Landroid/graphics/Point;->y:I

    .line 200
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPageFit:Landroid/graphics/Matrix;

    invoke-virtual {v14}, Landroid/graphics/Matrix;->reset()V

    .line 201
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPageFit:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentScale:Lcom/google/android/apps/books/util/Holder;

    invoke-virtual {v14}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v16

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentScale:Lcom/google/android/apps/books/util/Holder;

    invoke-virtual {v14}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v14

    move/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 202
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPageFit:Landroid/graphics/Matrix;

    int-to-float v15, v12

    int-to-float v0, v13

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 204
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->x:I

    add-int/2addr v15, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    add-int v16, v16, v13

    move/from16 v0, v16

    invoke-virtual {v14, v12, v13, v15, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 223
    .end local v12    # "pageTx":I
    .end local v13    # "pageTy":I
    :goto_5
    return-void

    .line 173
    .restart local v10    # "marginsX":I
    :cond_3
    if-eqz v7, :cond_4

    .line 174
    add-int/lit8 v12, v10, 0x1

    .line 175
    .restart local v12    # "pageTx":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    iput v12, v14, Landroid/graphics/Point;->x:I

    .line 176
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    neg-int v15, v12

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_3

    .line 178
    .end local v12    # "pageTx":I
    :cond_4
    div-int/lit8 v12, v10, 0x2

    .line 179
    .restart local v12    # "pageTx":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    iput v12, v14, Landroid/graphics/Point;->x:I

    .line 180
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    const/4 v15, 0x0

    aget-object v14, v14, v15

    neg-int v15, v12

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    .line 181
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    const/4 v15, 0x1

    aget-object v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->x:I

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    add-int v17, v17, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_3

    .line 184
    .end local v10    # "marginsX":I
    .end local v12    # "pageTx":I
    :cond_5
    if-eqz v8, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v12, v14, Landroid/graphics/Point;->x:I

    .line 185
    .restart local v12    # "pageTx":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    const/4 v15, 0x0

    iput v15, v14, Landroid/graphics/Point;->x:I

    goto/16 :goto_3

    .line 184
    .end local v12    # "pageTx":I
    :cond_6
    const/4 v12, 0x0

    goto :goto_6

    .line 196
    .restart local v12    # "pageTx":I
    :cond_7
    const/4 v13, 0x0

    .restart local v13    # "pageTy":I
    goto/16 :goto_4

    .line 207
    .end local v12    # "pageTx":I
    .end local v13    # "pageTy":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v14, v14, Landroid/graphics/Point;->x:I

    div-int/lit8 v5, v14, 0x2

    .line 208
    .local v5, "halfWidth":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mTopContainer:Landroid/widget/FrameLayout;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$300(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/widget/FrameLayout;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v14

    div-int/lit8 v2, v14, 0x2

    .line 209
    .local v2, "centerX":I
    if-eqz v7, :cond_9

    sub-int/2addr v2, v5

    .line 210
    :cond_9
    if-eqz v8, :cond_a

    add-int/2addr v2, v5

    .line 211
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v15}, Landroid/widget/ProgressBar;->getWidth()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    sub-int v15, v2, v15

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Landroid/widget/ProgressBar;->setX(F)V

    .line 213
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v14}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v14

    iget v14, v14, Landroid/graphics/Point;->y:I

    div-int/lit8 v3, v14, 0x2

    .line 214
    .local v3, "centerY":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v15}, Landroid/widget/ProgressBar;->getHeight()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    sub-int v15, v3, v15

    int-to-float v15, v15

    invoke-virtual {v14, v15}, Landroid/widget/ProgressBar;->setY(F)V

    .line 216
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mLetterboxMargins:Landroid/graphics/Point;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/Point;->set(II)V

    .line 218
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v14, v14, Landroid/graphics/Point;->y:I

    div-int/lit8 v4, v14, 0x2

    .line 220
    .local v4, "halfHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v15, v15, Landroid/graphics/Point;->x:I

    sub-int v15, v2, v15

    add-int/2addr v15, v5

    sub-int v16, v3, v4

    add-int v17, v2, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    add-int v18, v18, v3

    sub-int v18, v18, v4

    invoke-virtual/range {v14 .. v18}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_5
.end method
