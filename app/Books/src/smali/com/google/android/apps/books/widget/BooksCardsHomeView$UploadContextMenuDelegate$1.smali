.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;
.super Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;->onContextMenuPressed(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;

.field final synthetic val$doc:Lcom/google/android/apps/books/playcards/UploadDocument;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;Landroid/content/Context;Lcom/google/android/apps/books/playcards/UploadDocument;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;->this$1:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;

    iput-object p3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;->val$doc:Lcom/google/android/apps/books/playcards/UploadDocument;

    invoke-direct {p0, p2}, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public addMenuEntries(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;->this$1:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;

    iget-object v1, v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    sget-object v2, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->CANCEL:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate$1;->val$doc:Lcom/google/android/apps/books/playcards/UploadDocument;

    invoke-virtual {v3}, Lcom/google/android/apps/books/playcards/UploadDocument;->getUploadId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    return-void
.end method
