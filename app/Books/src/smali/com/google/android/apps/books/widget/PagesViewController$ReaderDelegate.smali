.class public interface abstract Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Navigator;
.implements Lcom/google/android/apps/books/widget/AnnotationIndex;
.implements Lcom/google/android/apps/books/widget/TtsLoadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReaderDelegate"
.end annotation


# virtual methods
.method public abstract editHighlight(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method

.method public abstract editNote(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method

.method public abstract enableNextSearch(Z)V
.end method

.method public abstract enablePreviousSearch(Z)V
.end method

.method public abstract getAnnotationsForPassage(I)Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEndOfBookView()Landroid/view/View;
.end method

.method public abstract getLocale()Ljava/util/Locale;
.end method

.method public abstract getPaintableAnnotations(ILjava/util/Set;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRenderingTheme(Lcom/google/android/apps/books/render/ReaderSettings;)Ljava/lang/String;
.end method

.method public abstract getSearchResultMap()Lcom/google/android/apps/books/app/SearchResultMap;
.end method

.method public abstract getSelectionPopup()Lcom/google/android/apps/books/widget/SelectionPopup;
.end method

.method public abstract getTextLocationComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
.end method

.method public abstract getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
.end method

.method public abstract isAnyAudioPlaying()Z
.end method

.method public abstract isScrubbing()Z
.end method

.method public abstract isValidPassageIndex(I)Z
.end method

.method public abstract isVertical()Z
.end method

.method public abstract maybeLoadEndOfBookPage()V
.end method

.method public abstract moveScrubberToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)V
.end method

.method public abstract moveToImageModeSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
.end method

.method public abstract moveToPosition(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
.end method

.method public abstract moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
.end method

.method public abstract onCanceledRendererRequests()V
.end method

.method public abstract onCompletedPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
.end method

.method public abstract onDismissedSelection()V
.end method

.method public abstract onEndOfBookPresenceChanged(Z)V
.end method

.method public abstract onError(Ljava/lang/Exception;)V
.end method

.method public abstract onFinishedTransitionToMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
.end method

.method public abstract onIsTurningChanged(ZZ)V
.end method

.method public abstract onMissingPosition(Lcom/google/android/apps/books/common/Position;)V
.end method

.method public abstract onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V
.end method

.method public abstract onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V
.end method

.method public abstract onPassageBecameVisible(I)V
.end method

.method public abstract onPositionChanged(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;)V
.end method

.method public abstract onSelectionChanged(ILcom/google/android/apps/books/app/SelectionState;)V
.end method

.method public abstract onSelectionUpdate()V
.end method

.method public abstract onStartedPageTurn()V
.end method

.method public abstract onStartedSelection()V
.end method

.method public abstract onUserSelectedNewPosition()V
.end method

.method public abstract onVisibleDevicePagesChanged(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract prepareBookView(Lcom/google/android/apps/books/widget/BookView$Callbacks;Z)Lcom/google/android/apps/books/widget/BookView;
.end method

.method public abstract resetZoom()V
.end method

.method public abstract searchNavigationEnabled()Z
.end method

.method public abstract setActionBarElevation(F)V
.end method

.method public abstract setSearchBarMatchText(IIZ)V
.end method

.method public abstract setSystemBarsVisible(Z)V
.end method

.method public abstract setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
.end method

.method public abstract showCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method

.method public abstract showTableOfContents(I)V
.end method
