.class Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreloadPassageRequest"
.end annotation


# instance fields
.field public final passageIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;II)V
    .locals 6
    .param p2, "requestId"    # I
    .param p3, "passageIndex"    # I

    .prologue
    .line 1334
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1335
    const/4 v4, 0x0

    const/4 v5, 0x6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1336
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;->passageIndex:I

    .line 1337
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1341
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342
    const-string v0, "ReaderController"

    const-string v1, "Issuing preloadPassage(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;->passageIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1347
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;->passageIndex:I

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$PreloadPassageRequest;->mTaskRequestId:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/render/TextReader;->preloadPassage(II)V

    .line 1348
    return-void
.end method
