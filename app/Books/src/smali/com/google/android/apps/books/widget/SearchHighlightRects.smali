.class Lcom/google/android/apps/books/widget/SearchHighlightRects;
.super Ljava/lang/Object;
.source "SearchHighlightRects.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/WalkableHighlightRects;


# instance fields
.field private final mSearchHighlightsColor:I

.field protected final rects:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;I)V
    .locals 0
    .param p2, "searchHighlightsColor"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "rects":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/render/LabeledRect;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SearchHighlightRects;->rects:Ljava/util/Collection;

    .line 20
    iput p2, p0, Lcom/google/android/apps/books/widget/SearchHighlightRects;->mSearchHighlightsColor:I

    .line 21
    return-void
.end method


# virtual methods
.method public getWalker()Lcom/google/android/apps/books/widget/Walker;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 25
    new-instance v2, Lcom/google/android/apps/books/widget/WalkerFromCollection;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/SearchHighlightRects;->rects:Ljava/util/Collection;

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/widget/WalkerFromCollection;-><init>(Ljava/util/Collection;)V

    .line 26
    .local v2, "rectsWalker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/LabeledRect;>;"
    new-instance v3, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    invoke-direct {v3}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;-><init>()V

    iget v4, p0, Lcom/google/android/apps/books/widget/SearchHighlightRects;->mSearchHighlightsColor:I

    invoke-virtual {v3, v4, v5, v2}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->set(IZLcom/google/android/apps/books/widget/Walker;)Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    move-result-object v0

    .line 28
    .local v0, "hsc":Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    aput-object v0, v3, v5

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 29
    .local v1, "hscList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    new-instance v3, Lcom/google/android/apps/books/widget/WalkerFromCollection;

    invoke-direct {v3, v1}, Lcom/google/android/apps/books/widget/WalkerFromCollection;-><init>(Ljava/util/Collection;)V

    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "rects"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SearchHighlightRects;->rects:Ljava/util/Collection;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
