.class final enum Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;
.super Ljava/lang/Enum;
.source "BaseBooksActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AccountState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

.field public static final enum IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

.field public static final enum RETRIEVING_AUTH_TOKEN:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

.field public static final enum SHOWING_PICKER:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 465
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 466
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    const-string v1, "SHOWING_PICKER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->SHOWING_PICKER:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 467
    new-instance v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    const-string v1, "RETRIEVING_AUTH_TOKEN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->RETRIEVING_AUTH_TOKEN:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    .line 464
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    sget-object v1, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->SHOWING_PICKER:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->RETRIEVING_AUTH_TOKEN:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->$VALUES:[Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 464
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 464
    const-class v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;
    .locals 1

    .prologue
    .line 464
    sget-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->$VALUES:[Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    return-object v0
.end method


# virtual methods
.method isInProgress()Z
    .locals 1

    .prologue
    .line 470
    sget-object v0, Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;->IDLE:Lcom/google/android/apps/books/app/BaseBooksActivity$AccountState;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
