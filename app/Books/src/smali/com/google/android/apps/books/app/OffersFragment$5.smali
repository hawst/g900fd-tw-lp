.class Lcom/google/android/apps/books/app/OffersFragment$5;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OffersFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment;

.field final synthetic val$skipThisOfferButton:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->val$skipThisOfferButton:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;"
    const/4 v5, 0x0

    .line 193
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 194
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;

    .line 195
    .local v1, "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    iget-object v2, v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 197
    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    iget-object v2, v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/OfferData;

    # setter for: Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;
    invoke-static {v3, v2}, Lcom/google/android/apps/books/app/OffersFragment;->access$702(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/model/OfferData;)Lcom/google/android/apps/books/model/OfferData;

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # getter for: Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;
    invoke-static {v3}, Lcom/google/android/apps/books/app/OffersFragment;->access$700(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/model/OfferData;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->createOfferCards(Lcom/google/android/apps/books/model/OfferData;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/OffersFragment;->access$800(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/model/OfferData;)V

    .line 209
    .end local v1    # "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->val$skipThisOfferButton:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 210
    return-void

    .line 200
    .restart local v1    # "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 201
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "dialogError"

    const v3, 0x7f0f01b9

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 202
    iget-object v2, p0, Lcom/google/android/apps/books/app/OffersFragment$5;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    invoke-static {v2}, Lcom/google/android/apps/books/app/OffersFragment;->access$900(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v4}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 205
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    :cond_2
    const-string v2, "OffersFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    const-string v2, "OffersFragment"

    const-string v3, "missing offers data"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 190
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment$5;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
