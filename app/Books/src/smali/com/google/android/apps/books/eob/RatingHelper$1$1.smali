.class Lcom/google/android/apps/books/eob/RatingHelper$1$1;
.super Landroid/os/AsyncTask;
.source "RatingHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/RatingHelper$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

.field final synthetic val$reviewsService:Lcom/android/vending/reviews/IReviewsService;

.field final synthetic val$thisServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/RatingHelper$1;Lcom/android/vending/reviews/IReviewsService;Landroid/content/ServiceConnection;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->val$reviewsService:Lcom/android/vending/reviews/IReviewsService;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->val$thisServiceConnection:Landroid/content/ServiceConnection;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/os/Bundle;
    .locals 5
    .param p1, "nada"    # [Ljava/lang/Void;

    .prologue
    .line 85
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->val$reviewsService:Lcom/android/vending/reviews/IReviewsService;

    iget-object v2, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    iget-object v2, v2, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$accountName:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "book-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    iget-object v4, v4, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/android/vending/reviews/IReviewsService;->getRateAndReviewIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 92
    :goto_0
    return-object v1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "RatingHelper"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    const-string v1, "RatingHelper"

    const-string v2, "requestRatingInfo: getRateAndReviewIntent failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 80
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->doInBackground([Ljava/lang/Void;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x5

    const/4 v3, 0x0

    .line 97
    if-eqz p1, :cond_2

    const/4 v1, 0x1

    .line 100
    .local v1, "gotBundle":Z
    :goto_0
    if-eqz v1, :cond_4

    .line 101
    new-instance v2, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;

    invoke-direct {v2, p1}, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;-><init>(Landroid/os/Bundle;)V

    .line 102
    .local v2, "ratingInfo":Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    const-string v4, "RatingHelper"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    iget v4, v2, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    if-lez v4, :cond_3

    .line 104
    const-string v4, "RatingHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v2, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->docTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rated "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->rating:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stars by "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->authorTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    # getter for: Lcom/google/android/apps/books/eob/RatingHelper$1;->mServiceDisconnected:Z
    invoke-static {v4}, Lcom/google/android/apps/books/eob/RatingHelper$1;->access$000(Lcom/google/android/apps/books/eob/RatingHelper$1;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 119
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    iget-object v4, v4, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->val$thisServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v4, v5}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    iget-object v4, v4, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$readyListener:Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;

    iput-boolean v3, v4, Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;->requestIsInProcess:Z

    .line 129
    iget-object v3, p0, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->this$0:Lcom/google/android/apps/books/eob/RatingHelper$1;

    iget-object v3, v3, Lcom/google/android/apps/books/eob/RatingHelper$1;->val$readyListener:Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/eob/RatingHelper$ReadyListener;->onRatingInfoAvailable(Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;)V

    .line 130
    return-void

    .end local v1    # "gotBundle":Z
    .end local v2    # "ratingInfo":Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    :cond_2
    move v1, v3

    .line 97
    goto :goto_0

    .line 107
    .restart local v1    # "gotBundle":Z
    .restart local v2    # "ratingInfo":Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    :cond_3
    const-string v4, "RatingHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v2, Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;->docTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " hasn\'t been rated yet"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 111
    .end local v2    # "ratingInfo":Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    :cond_4
    const/4 v2, 0x0

    .line 112
    .restart local v2    # "ratingInfo":Lcom/google/android/apps/books/eob/RatingHelper$RatingInfo;
    const-string v4, "RatingHelper"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    const-string v4, "RatingHelper"

    const-string v5, "requestRatingInfo got null bundle"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "RatingHelper"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 122
    const-string v4, "RatingHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IReviewsService unbindService failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 80
    check-cast p1, Landroid/os/Bundle;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/eob/RatingHelper$1$1;->onPostExecute(Landroid/os/Bundle;)V

    return-void
.end method
