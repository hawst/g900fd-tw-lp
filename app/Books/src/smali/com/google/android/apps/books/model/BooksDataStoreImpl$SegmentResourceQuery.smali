.class interface abstract Lcom/google/android/apps/books/model/BooksDataStoreImpl$SegmentResourceQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "SegmentResourceQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2042
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "segment_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resource_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "css_class"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SegmentResourceQuery;->PROJECTION:[Ljava/lang/String;

    .line 2048
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SegmentResourceQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SegmentResourceQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method
