.class Lcom/google/android/apps/books/app/OnboardingController$3;
.super Ljava/lang/Object;
.source "OnboardingController.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OnboardingController;->getImage(Ljava/lang/String;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OnboardingController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/apps/books/app/OnboardingController$3;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OnboardingController$3;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 192
    if-eqz p2, :cond_0

    .line 193
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 194
    .local v0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/OnboardingController$3;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 199
    :goto_0
    return-void

    .line 196
    .end local v0    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 197
    .restart local v0    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/OnboardingController$3;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method
