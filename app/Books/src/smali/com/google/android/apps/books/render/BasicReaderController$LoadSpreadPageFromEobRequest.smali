.class Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSpreadPageFromEobRequest"
.end annotation


# instance fields
.field final spreadOffset:I

.field final spreadPageIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;III)V
    .locals 6
    .param p2, "spreadOffset"    # I
    .param p3, "spreadPageIndex"    # I
    .param p4, "requestId"    # I

    .prologue
    .line 1409
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1410
    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1411
    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->spreadOffset:I

    .line 1412
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->spreadPageIndex:I

    .line 1413
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1417
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1418
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Issuing LoadSpreadPageFromEobRequest("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->spreadOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->spreadPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->mTaskRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 1422
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->spreadOffset:I

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->spreadPageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageFromEobRequest;->mTaskRequestId:I

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/render/TextReader;->loadSpreadPageFromEob(III)V

    .line 1423
    return-void
.end method
