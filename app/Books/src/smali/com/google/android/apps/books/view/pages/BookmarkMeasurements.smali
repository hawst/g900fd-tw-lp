.class public Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
.super Ljava/lang/Object;
.source "BookmarkMeasurements.java"


# static fields
.field private static final mTempPoint:Landroid/graphics/Point;


# instance fields
.field private final mBookmarkHeight:I

.field private final mBookmarkMargin:I

.field private final mBookmarkWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "bookmarkWidth"    # I
    .param p2, "bookmarkHeight"    # I
    .param p3, "marginWidth"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkWidth:I

    .line 38
    iput p2, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkHeight:I

    .line 39
    iput p3, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkMargin:I

    .line 40
    return-void
.end method

.method private bookmarkLeftCoordToPageWidthRatio(ZLandroid/graphics/Point;)F
    .locals 3
    .param p1, "isLeftPageOfTwo"    # Z
    .param p2, "pageDims"    # Landroid/graphics/Point;

    .prologue
    .line 117
    if-eqz p1, :cond_0

    .line 118
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkMargin:I

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 120
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkWidth:I

    iget v2, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkMargin:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method public static from(Landroid/content/res/Resources;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .locals 1
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->from(Landroid/content/res/Resources;F)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v0

    return-object v0
.end method

.method public static from(Landroid/content/res/Resources;F)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .locals 5
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "scale"    # F

    .prologue
    .line 24
    const v2, 0x7f09015d

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 26
    .local v1, "marginWidth":I
    const v2, 0x7f020044

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 27
    .local v0, "bookmark":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;-><init>(III)V

    return-object v2
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkHeight:I

    return v0
.end method

.method public getMargin()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkMargin:I

    return v0
.end method

.method public getTapTargetSize(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "letterboxSize"    # Landroid/graphics/Point;
    .param p2, "out"    # Landroid/graphics/Point;

    .prologue
    .line 76
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkMargin:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkWidth:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkMargin:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->x:I

    .line 77
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkHeight:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 78
    return-object p2
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkWidth:I

    return v0
.end method

.method public inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;III)Z
    .locals 4
    .param p1, "event"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;
    .param p2, "pageWidth"    # I
    .param p3, "horizontalLetterbox"    # I
    .param p4, "verticalLetterbox"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    sget-object v2, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {v2, p3, p4}, Landroid/graphics/Point;->set(II)V

    .line 54
    sget-object v2, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    sget-object v3, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->getTapTargetSize(Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 58
    iget-object v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sget-object v3, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_1

    move v0, v1

    .line 65
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/TouchedPageEvent;->isLeftPageOfTwo()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 63
    iget-object v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    sget-object v3, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 65
    :cond_2
    iget-object v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    sget-object v3, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mTempPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v3, p2, v3

    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public setBookmarkTransformationMatrix(FZLandroid/graphics/Matrix;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "bookmarkPercentVisible"    # F
    .param p2, "isLeftPageOfTwo"    # Z
    .param p3, "matrix"    # Landroid/graphics/Matrix;
    .param p4, "pageDims"    # Landroid/graphics/Point;

    .prologue
    const/4 v3, 0x0

    .line 95
    invoke-virtual {p3}, Landroid/graphics/Matrix;->reset()V

    .line 97
    invoke-direct {p0, p2, p4}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->bookmarkLeftCoordToPageWidthRatio(ZLandroid/graphics/Point;)F

    move-result v0

    neg-float v0, v0

    invoke-virtual {p3, v0, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 99
    iget v0, p4, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p4, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->mBookmarkHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 101
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    invoke-virtual {p3, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 102
    return-void
.end method
