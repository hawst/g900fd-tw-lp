.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1$1;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1$1;->this$2:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 398
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1$1;->this$2:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;

    iget-object v3, v3, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    iget-object v3, v3, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-virtual {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 399
    .local v1, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 401
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v2

    .line 403
    .local v2, "syncController":Lcom/google/android/apps/books/sync/SyncController;
    invoke-interface {v2}, Lcom/google/android/apps/books/sync/SyncController;->getSyncAutomatically()Z

    move-result v3

    if-nez v3, :cond_0

    .line 404
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/sync/SyncController;->requestManualSync(Z)V

    .line 408
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1$1;->this$2:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;

    iget-object v3, v3, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;->this$1:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mOnboardHostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->access$1700(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->exitOnboardFlow()V

    .line 410
    return-void
.end method
