.class public Lcom/google/android/apps/books/api/ApiaryTranslationServer;
.super Ljava/lang/Object;
.source "ApiaryTranslationServer.java"

# interfaces
.implements Lcom/google/android/apps/books/api/TranslationServer;


# instance fields
.field private final mDeveloperKeyFactory:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/common/base/Supplier;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/base/Supplier",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "developerKeyFactory":Lcom/google/common/base/Supplier;, "Lcom/google/common/base/Supplier<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->mDeveloperKeyFactory:Lcom/google/common/base/Supplier;

    .line 48
    return-void
.end method

.method private buildRequestForDiscoverLanguages()Lcom/google/api/client/http/HttpRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    new-instance v2, Lcom/google/api/client/http/GenericUrl;

    const-string v3, "https://www.googleapis.com/language/translate/v2"

    invoke-direct {v2, v3}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    .line 133
    .local v2, "url":Lcom/google/api/client/http/GenericUrl;
    invoke-virtual {v2}, Lcom/google/api/client/http/GenericUrl;->getPathParts()Ljava/util/List;

    move-result-object v3

    const-string v4, "languages"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v3, "key"

    invoke-direct {p0}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->getDeveloperKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string v3, "target"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-static {}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->createTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v1

    .line 139
    .local v1, "transport":Lcom/google/api/client/http/HttpTransport;
    invoke-virtual {v1}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/api/client/http/HttpRequestFactory;->buildGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 140
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    new-instance v3, Lcom/google/api/client/json/JsonObjectParser;

    new-instance v4, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v4}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-direct {v3, v4}, Lcom/google/api/client/json/JsonObjectParser;-><init>(Lcom/google/api/client/json/JsonFactory;)V

    invoke-virtual {v0, v3}, Lcom/google/api/client/http/HttpRequest;->setParser(Lcom/google/api/client/util/ObjectParser;)Lcom/google/api/client/http/HttpRequest;

    .line 141
    return-object v0
.end method

.method private buildRequestForTranslateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/google/api/client/http/HttpRequest;
    .locals 8
    .param p1, "sourceLanguage"    # Ljava/lang/String;
    .param p2, "targetLanguage"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x13ff

    .line 101
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-le v5, v6, :cond_0

    .line 102
    const/4 v5, 0x0

    invoke-interface {p3, v5, v6}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p3

    .line 107
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 108
    .local v1, "queryParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/CharSequence;>;"
    const-string v5, "key"

    invoke-direct {p0}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->getDeveloperKey()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 110
    const-string v5, "source"

    invoke-interface {v1, v5, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    :cond_1
    const-string v5, "target"

    invoke-interface {v1, v5, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    const-string v5, "q"

    invoke-interface {v1, v5, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    new-instance v0, Lcom/google/api/client/http/UrlEncodedContent;

    invoke-direct {v0, v1}, Lcom/google/api/client/http/UrlEncodedContent;-><init>(Ljava/lang/Object;)V

    .line 119
    .local v0, "body":Lcom/google/api/client/http/UrlEncodedContent;
    new-instance v4, Lcom/google/api/client/http/GenericUrl;

    const-string v5, "https://www.googleapis.com/language/translate/v2"

    invoke-direct {v4, v5}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    .line 120
    .local v4, "url":Lcom/google/api/client/http/GenericUrl;
    invoke-static {}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->createTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v3

    .line 121
    .local v3, "transport":Lcom/google/api/client/http/HttpTransport;
    invoke-virtual {v3}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v5

    invoke-virtual {v5, v4, v0}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v2

    .line 122
    .local v2, "request":Lcom/google/api/client/http/HttpRequest;
    new-instance v5, Lcom/google/api/client/json/JsonObjectParser;

    new-instance v6, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v6}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-direct {v5, v6}, Lcom/google/api/client/json/JsonObjectParser;-><init>(Lcom/google/api/client/json/JsonFactory;)V

    invoke-virtual {v2, v5}, Lcom/google/api/client/http/HttpRequest;->setParser(Lcom/google/api/client/util/ObjectParser;)Lcom/google/api/client/http/HttpRequest;

    .line 126
    invoke-virtual {v2}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v5

    const-string v6, "X-HTTP-Method-Override"

    const-string v7, "GET"

    invoke-virtual {v5, v6, v7}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 128
    return-object v2
.end method

.method private extractLanguages(Lcom/google/android/apps/books/api/data/JsonTranslations;)Lcom/google/common/collect/BiMap;
    .locals 5
    .param p1, "languagesInfo"    # Lcom/google/android/apps/books/api/data/JsonTranslations;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/api/data/JsonTranslations;",
            ")",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v3, p1, Lcom/google/android/apps/books/api/data/JsonTranslations;->data:Lcom/google/android/apps/books/api/data/JsonTranslations$Data;

    iget-object v3, v3, Lcom/google/android/apps/books/api/data/JsonTranslations$Data;->languages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/common/collect/HashBiMap;->create(I)Lcom/google/common/collect/HashBiMap;

    move-result-object v2

    .line 83
    .local v2, "result":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p1, Lcom/google/android/apps/books/api/data/JsonTranslations;->data:Lcom/google/android/apps/books/api/data/JsonTranslations$Data;

    iget-object v3, v3, Lcom/google/android/apps/books/api/data/JsonTranslations$Data;->languages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/api/data/JsonTranslations$Language;

    .line 84
    .local v1, "language":Lcom/google/android/apps/books/api/data/JsonTranslations$Language;
    iget-object v3, v1, Lcom/google/android/apps/books/api/data/JsonTranslations$Language;->name:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/apps/books/api/data/JsonTranslations$Language;->language:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 86
    .end local v1    # "language":Lcom/google/android/apps/books/api/data/JsonTranslations$Language;
    :cond_0
    return-object v2
.end method

.method private getDeveloperKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->mDeveloperKeyFactory:Lcom/google/common/base/Supplier;

    invoke-interface {v0}, Lcom/google/common/base/Supplier;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private getResponse(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;
    .locals 1
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-static {p1}, Lcom/google/android/apps/books/net/HttpUtils;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public discoverLanguages()Lcom/google/common/collect/BiMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->buildRequestForDiscoverLanguages()Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 57
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->getResponse(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v2

    .line 58
    .local v2, "response":Lcom/google/api/client/http/HttpResponse;
    const-class v3, Lcom/google/android/apps/books/api/data/JsonTranslations;

    invoke-virtual {v2, v3}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/JsonTranslations;

    .line 59
    .local v0, "languages":Lcom/google/android/apps/books/api/data/JsonTranslations;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->extractLanguages(Lcom/google/android/apps/books/api/data/JsonTranslations;)Lcom/google/common/collect/BiMap;

    move-result-object v3

    return-object v3
.end method

.method public translateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;
    .locals 7
    .param p1, "sourceLanguage"    # Ljava/lang/String;
    .param p2, "targetLanguage"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->buildRequestForTranslateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 71
    .local v1, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/api/ApiaryTranslationServer;->getResponse(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v2

    .line 72
    .local v2, "response":Lcom/google/api/client/http/HttpResponse;
    const-class v5, Lcom/google/android/apps/books/api/data/JsonTranslations;

    invoke-virtual {v2, v5}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/api/data/JsonTranslations;

    .line 73
    .local v3, "result":Lcom/google/android/apps/books/api/data/JsonTranslations;
    iget-object v5, v3, Lcom/google/android/apps/books/api/data/JsonTranslations;->data:Lcom/google/android/apps/books/api/data/JsonTranslations$Data;

    iget-object v5, v5, Lcom/google/android/apps/books/api/data/JsonTranslations$Data;->translations:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/api/data/JsonTranslations$Translation;

    iget-object v0, v5, Lcom/google/android/apps/books/api/data/JsonTranslations$Translation;->detectedSourceLanguage:Ljava/lang/String;

    .line 75
    .local v0, "detectedLanguage":Ljava/lang/String;
    iget-object v5, v3, Lcom/google/android/apps/books/api/data/JsonTranslations;->data:Lcom/google/android/apps/books/api/data/JsonTranslations$Data;

    iget-object v5, v5, Lcom/google/android/apps/books/api/data/JsonTranslations$Data;->translations:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/api/data/JsonTranslations$Translation;

    iget-object v4, v5, Lcom/google/android/apps/books/api/data/JsonTranslations$Translation;->translatedText:Ljava/lang/String;

    .line 77
    .local v4, "translatedText":Ljava/lang/CharSequence;
    new-instance v5, Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;

    invoke-direct {v5, v0, v4}, Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-object v5
.end method
