.class Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;
.super Ljava/lang/Object;
.source "MyEbooksSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iput-object p4, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailure(Ljava/lang/Exception;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$100(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Ljava/lang/Exception;)V

    .line 146
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iget-object v4, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$200(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 137
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
