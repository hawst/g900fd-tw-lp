.class Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;
.super Ljava/lang/Object;
.source "SharedResourceSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->downloadResources(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

.field final synthetic val$e:Ljava/io/IOException;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$tempsToSave:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->val$tempsToSave:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p4, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->val$e:Ljava/io/IOException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->val$tempsToSave:Ljava/util/List;

    # invokes: Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->abortAll(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->access$200(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Ljava/util/List;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;->val$e:Ljava/io/IOException;

    # invokes: Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->publishFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->access$300(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V

    .line 150
    return-void
.end method
