.class public Lcom/google/android/apps/books/util/ReaderUtils;
.super Ljava/lang/Object;
.source "ReaderUtils.java"


# static fields
.field private static sDefaultTextZoom:F

.field private static sDefaultTextZoomIncrement:F

.field private static sDeviceHasTooManyPixels:Z

.field private static sDeviceTextureLimit:I

.field private static sFullColorScreenBytes:I

.field private static sInitialized:Z

.field private static sIsTablet:Z

.field private static sLandscapeWidth:I

.field private static sLargestSize:I

.field private static sLinkTapThresholdInPixels:I

.field private static sMemoryClass:I

.field private static sPortraitWidth:I

.field private static sSmallestSize:I

.field private static sSupportsOpenGLES2:Z

.field private static sTwoFlowingPagesInLandscape:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x258

    const/4 v1, 0x0

    .line 50
    sput-boolean v1, Lcom/google/android/apps/books/util/ReaderUtils;->sSupportsOpenGLES2:Z

    .line 51
    sput-boolean v1, Lcom/google/android/apps/books/util/ReaderUtils;->sTwoFlowingPagesInLandscape:Z

    .line 52
    const/16 v0, 0x190

    sput v0, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    .line 53
    sput v2, Lcom/google/android/apps/books/util/ReaderUtils;->sLandscapeWidth:I

    .line 54
    sput v2, Lcom/google/android/apps/books/util/ReaderUtils;->sLargestSize:I

    .line 55
    sput v2, Lcom/google/android/apps/books/util/ReaderUtils;->sSmallestSize:I

    .line 56
    const v0, 0xea600

    sput v0, Lcom/google/android/apps/books/util/ReaderUtils;->sFullColorScreenBytes:I

    .line 57
    sput v1, Lcom/google/android/apps/books/util/ReaderUtils;->sLinkTapThresholdInPixels:I

    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoom:F

    .line 59
    const/high16 v0, 0x3e000000    # 0.125f

    sput v0, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoomIncrement:F

    .line 60
    const/16 v0, 0x10

    sput v0, Lcom/google/android/apps/books/util/ReaderUtils;->sMemoryClass:I

    .line 61
    sput-boolean v1, Lcom/google/android/apps/books/util/ReaderUtils;->sIsTablet:Z

    .line 64
    sput-boolean v1, Lcom/google/android/apps/books/util/ReaderUtils;->sDeviceHasTooManyPixels:Z

    .line 65
    const v0, 0x7fffffff

    sput v0, Lcom/google/android/apps/books/util/ReaderUtils;->sDeviceTextureLimit:I

    return-void
.end method

.method public static countWhiteSpaceChars(Ljava/lang/String;II)I
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "minIndex"    # I
    .param p2, "maxIndex"    # I

    .prologue
    .line 540
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 541
    const/4 v2, 0x0

    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 542
    const/4 v1, 0x0

    .line 543
    .local v1, "result":I
    move v0, p1

    .local v0, "ii":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 544
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/ReaderUtils;->isWhiteSpace(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 545
    add-int/lit8 v1, v1, 0x1

    .line 543
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 547
    :cond_1
    return v1
.end method

.method public static currentlyInLandscape(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 332
    const/4 v0, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static detectAberrantDevices()V
    .locals 5

    .prologue
    .line 239
    const/16 v0, 0xa

    .line 240
    .local v0, "HEALTHY_RATIO_MEMORY_OVER_SS":I
    const/16 v1, 0x800

    .line 242
    .local v1, "TYPICAL_TEXTURE_SIZE":I
    sget v3, Lcom/google/android/apps/books/util/ReaderUtils;->sMemoryClass:I

    shl-int/lit8 v3, v3, 0x14

    sget v4, Lcom/google/android/apps/books/util/ReaderUtils;->sFullColorScreenBytes:I

    div-int v2, v3, v4

    .line 243
    .local v2, "myRatio":I
    sget v3, Lcom/google/android/apps/books/util/ReaderUtils;->sMemoryClass:I

    const/16 v4, 0x30

    if-le v3, v4, :cond_0

    const/16 v3, 0xa

    if-ge v2, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    sput-boolean v3, Lcom/google/android/apps/books/util/ReaderUtils;->sDeviceHasTooManyPixels:Z

    .line 244
    sget-boolean v3, Lcom/google/android/apps/books/util/ReaderUtils;->sDeviceHasTooManyPixels:Z

    if-eqz v3, :cond_1

    const/16 v3, 0x800

    :goto_1
    sput v3, Lcom/google/android/apps/books/util/ReaderUtils;->sDeviceTextureLimit:I

    .line 245
    return-void

    .line 243
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 244
    :cond_1
    const v3, 0x7fffffff

    goto :goto_1
.end method

.method public static deviceHasTooManyPixels()Z
    .locals 1

    .prologue
    .line 251
    sget-boolean v0, Lcom/google/android/apps/books/util/ReaderUtils;->sDeviceHasTooManyPixels:Z

    return v0
.end method

.method public static displayTwoPagesForFlowingText(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 325
    sget-boolean v0, Lcom/google/android/apps/books/util/ReaderUtils;->sTwoFlowingPagesInLandscape:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/books/util/ReaderUtils;->currentlyInLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static formatPageTitle(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/Long;Z)Ljava/lang/CharSequence;
    .locals 8
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "pageTitle"    # Ljava/lang/String;
    .param p2, "maxPageNumber"    # Ljava/lang/Long;
    .param p3, "condensed"    # Z

    .prologue
    const v3, 0x7f0f0091

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 427
    invoke-static {p1}, Lcom/google/android/apps/books/util/ReaderUtils;->parsePageNumber(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 429
    .local v0, "pageLong":Ljava/lang/Long;
    if-eqz v0, :cond_2

    .line 430
    if-eqz p2, :cond_1

    .line 432
    if-eqz p3, :cond_0

    const v2, 0x7f0f0090

    .line 434
    .local v2, "patternId":I
    :goto_0
    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 435
    .local v1, "pattern":Ljava/lang/String;
    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "%1$s"

    aput-object v4, v3, v7

    const-string v4, "%2$s"

    aput-object v4, v3, v6

    new-array v4, v5, [Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1, v3, v4}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 448
    .end local v1    # "pattern":Ljava/lang/String;
    .end local v2    # "patternId":I
    :goto_1
    return-object v3

    .line 432
    :cond_0
    const v2, 0x7f0f008f

    goto :goto_0

    .line 441
    :cond_1
    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "%1$s"

    aput-object v5, v4, v7

    new-array v5, v6, [Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1

    .line 448
    :cond_2
    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "%1$s"

    aput-object v5, v4, v7

    new-array v5, v6, [Ljava/lang/CharSequence;

    aput-object p1, v5, v7

    invoke-static {v3, v4, v5}, Landroid/text/TextUtils;->replace(Ljava/lang/CharSequence;[Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_1
.end method

.method public static getBaselineReadingMemoryFootprint(Landroid/graphics/Point;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)I
    .locals 2
    .param p0, "screenSize"    # Landroid/graphics/Point;
    .param p1, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 584
    const/high16 v0, 0x800000

    .line 585
    .local v0, "result":I
    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    if-ne p1, v1, :cond_0

    .line 586
    invoke-static {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->getMainSpreadDrawingCacheBytes(Landroid/graphics/Point;)I

    move-result v1

    add-int/2addr v0, v1

    .line 588
    :cond_0
    return v0
.end method

.method public static getColorFromAttr(Landroid/content/Context;II)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attr"    # I
    .param p2, "defaultColor"    # I

    .prologue
    .line 664
    const/4 v5, 0x1

    new-array v3, v5, [I

    const/4 v5, 0x0

    aput p1, v3, v5

    .line 665
    .local v3, "queryColorAttr":[I
    const/4 v1, 0x0

    .line 666
    .local v1, "indexOfQueryColorAttr":I
    new-instance v4, Landroid/util/TypedValue;

    invoke-direct {v4}, Landroid/util/TypedValue;-><init>()V

    .line 667
    .local v4, "typedValue":Landroid/util/TypedValue;
    iget v5, v4, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0, v5, v3}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 668
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v1, p2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 669
    .local v2, "intColor":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 670
    return v2
.end method

.method public static getCurrentSizeRange(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 1
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "smallestSize"    # Landroid/graphics/Point;
    .param p2, "largestSize"    # Landroid/graphics/Point;

    .prologue
    .line 174
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnOrAfter(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/util/ReaderUtils;->getCurrentSizeRangeJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/util/ReaderUtils;->getCurrentSizeRangePreJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    goto :goto_0
.end method

.method private static getCurrentSizeRangeJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "smallestSize"    # Landroid/graphics/Point;
    .param p2, "largestSize"    # Landroid/graphics/Point;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0, p1, p2}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 185
    return-void
.end method

.method private static getCurrentSizeRangePreJb(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "smallestSize"    # Landroid/graphics/Point;
    .param p2, "largestSize"    # Landroid/graphics/Point;

    .prologue
    .line 189
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/ReaderUtils;->getSize(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 190
    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 191
    .local v1, "min":I
    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 192
    .local v0, "max":I
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Point;->set(II)V

    .line 193
    invoke-virtual {p2, v0, v0}, Landroid/graphics/Point;->set(II)V

    .line 194
    return-void
.end method

.method public static getDefaultTextZoom()F
    .locals 2

    .prologue
    .line 347
    sget-boolean v0, Lcom/google/android/apps/books/util/ReaderUtils;->sInitialized:Z

    if-nez v0, :cond_0

    .line 348
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ReaderUtils has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_0
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoom:F

    return v0
.end method

.method public static getDefaultTextZoomIncrement()F
    .locals 1

    .prologue
    .line 357
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoomIncrement:F

    return v0
.end method

.method public static getFullColorScreenBytes()I
    .locals 1

    .prologue
    .line 258
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sFullColorScreenBytes:I

    return v0
.end method

.method public static getMarginPercentSides(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 570
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 571
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0c001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    return v1
.end method

.method public static getMaxPageImageWidth(ILandroid/graphics/Point;Landroid/graphics/Point;)I
    .locals 12
    .param p0, "memoryClass"    # I
    .param p1, "shortEdgeScreenLengths"    # Landroid/graphics/Point;
    .param p2, "longEdgeScreenLengths"    # Landroid/graphics/Point;

    .prologue
    .line 612
    iget v7, p2, Landroid/graphics/Point;->x:I

    iget v8, p2, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 614
    .local v2, "longestLongEdgeLength":I
    iget v7, p1, Landroid/graphics/Point;->x:I

    iget v8, p1, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 621
    .local v3, "longestShortEdgeLength":I
    const/16 v6, 0xd

    .line 623
    .local v6, "worstCaseResidentPageImages":I
    shl-int/lit8 v0, p0, 0x14

    .line 624
    .local v0, "availableMemory":I
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7, v3, v2}, Landroid/graphics/Point;-><init>(II)V

    sget-object v8, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {v7, v8}, Lcom/google/android/apps/books/util/ReaderUtils;->getBaselineReadingMemoryFootprint(Landroid/graphics/Point;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)I

    move-result v1

    .line 626
    .local v1, "baselineMemory":I
    sub-int v4, v0, v1

    .line 632
    .local v4, "memoryForPageImages":I
    int-to-double v8, v4

    const-wide v10, 0x4054cccccccccccdL    # 83.2

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v5, v8

    .line 634
    .local v5, "width":I
    return v5
.end method

.method public static getMemoryClass()I
    .locals 1

    .prologue
    .line 365
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sMemoryClass:I

    return v0
.end method

.method public static getPortraitScreenHeight()I
    .locals 1

    .prologue
    .line 395
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sLargestSize:I

    return v0
.end method

.method public static getPortraitScreenWidth()I
    .locals 1

    .prologue
    .line 387
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    return v0
.end method

.method public static getSize(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 197
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/ReaderUtils;->getSizeHCMR2(Landroid/view/Display;Landroid/graphics/Point;)V

    .line 198
    return-void
.end method

.method private static getSizeHCMR2(Landroid/view/Display;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "display"    # Landroid/view/Display;
    .param p1, "size"    # Landroid/graphics/Point;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 203
    return-void
.end method

.method public static getThemedBackgroundColor(Ljava/lang/String;)I
    .locals 2
    .param p0, "readerTheme"    # Ljava/lang/String;

    .prologue
    const/4 v0, -0x1

    .line 489
    const-string v1, "0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 501
    :cond_0
    :goto_0
    return v0

    .line 492
    :cond_1
    const-string v1, "1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 493
    const/high16 v0, -0x1000000

    goto :goto_0

    .line 495
    :cond_2
    const-string v1, "2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 497
    const v0, -0x131e36

    goto :goto_0
.end method

.method public static getThemedBlueForegroundColorId(Ljava/lang/String;)I
    .locals 1
    .param p0, "renderTheme"    # Ljava/lang/String;

    .prologue
    .line 523
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    const v0, 0x7f0b00be

    .line 526
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0b00bf

    goto :goto_0
.end method

.method public static getThemedForegroundColor(Ljava/lang/String;)I
    .locals 2
    .param p0, "readerTheme"    # Ljava/lang/String;

    .prologue
    const/high16 v0, -0x1000000

    .line 459
    const-string v1, "0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    :cond_0
    :goto_0
    return v0

    .line 462
    :cond_1
    const-string v1, "1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 464
    const/4 v0, -0x1

    goto :goto_0

    .line 466
    :cond_2
    const-string v1, "2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 468
    const v0, -0x9bafce

    goto :goto_0
.end method

.method public static getThemedLetterboxColorId(Ljava/lang/String;)I
    .locals 1
    .param p0, "renderTheme"    # Ljava/lang/String;

    .prologue
    .line 505
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    const v0, 0x7f0b0109

    .line 510
    :goto_0
    return v0

    .line 507
    :cond_0
    const-string v0, "2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 508
    const v0, 0x7f0b010b

    goto :goto_0

    .line 510
    :cond_1
    const v0, 0x7f0b010a

    goto :goto_0
.end method

.method public static getThemedSelectionColorId(Ljava/lang/String;)I
    .locals 1
    .param p0, "renderTheme"    # Ljava/lang/String;

    .prologue
    .line 514
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    const v0, 0x7f0b00cb

    .line 519
    :goto_0
    return v0

    .line 516
    :cond_0
    const-string v0, "2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 517
    const v0, 0x7f0b00cc

    goto :goto_0

    .line 519
    :cond_1
    const v0, 0x7f0b00cd

    goto :goto_0
.end method

.method public static isTablet()Z
    .locals 1

    .prologue
    .line 372
    sget-boolean v0, Lcom/google/android/apps/books/util/ReaderUtils;->sIsTablet:Z

    return v0
.end method

.method public static isWhiteSpace(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 532
    invoke-static {p0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xa0

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static linkTapThresholdInPixels()I
    .locals 1

    .prologue
    .line 340
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sLinkTapThresholdInPixels:I

    return v0
.end method

.method public static maxPictureWidth(F)I
    .locals 2
    .param p0, "fudge"    # F

    .prologue
    .line 316
    sget-boolean v0, Lcom/google/android/apps/books/util/ReaderUtils;->sTwoFlowingPagesInLandscape:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, p0

    float-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, p0

    float-to-int v0, v0

    sget v1, Lcom/google/android/apps/books/util/ReaderUtils;->sLandscapeWidth:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public static declared-synchronized maybeInitialize(Landroid/content/Context;)V
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 81
    const-class v20, Lcom/google/android/apps/books/util/ReaderUtils;

    monitor-enter v20

    :try_start_0
    sget-boolean v19, Lcom/google/android/apps/books/util/ReaderUtils;->sInitialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v19, :cond_1

    .line 164
    :cond_0
    :goto_0
    monitor-exit v20

    return-void

    .line 84
    :cond_1
    const/16 v19, 0x1

    :try_start_1
    sput-boolean v19, Lcom/google/android/apps/books/util/ReaderUtils;->sInitialized:Z

    .line 86
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 87
    .local v14, "res":Landroid/content/res/Resources;
    const v19, 0x7f09014d

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sLinkTapThresholdInPixels:I

    .line 89
    const-string v19, "activity"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 91
    .local v2, "activityMgr":Landroid/app/ActivityManager;
    if-eqz v2, :cond_2

    .line 92
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    move/from16 v19, v0

    const/high16 v21, 0x20000

    move/from16 v0, v19

    move/from16 v1, v21

    if-lt v0, v1, :cond_4

    const/16 v19, 0x1

    :goto_1
    sput-boolean v19, Lcom/google/android/apps/books/util/ReaderUtils;->sSupportsOpenGLES2:Z

    .line 93
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v19

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sMemoryClass:I

    .line 96
    :cond_2
    const-string v19, "window"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/WindowManager;

    .line 97
    .local v11, "mgr":Landroid/view/WindowManager;
    if-eqz v11, :cond_0

    .line 101
    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 102
    .local v6, "display":Landroid/view/Display;
    new-instance v15, Landroid/graphics/Point;

    invoke-direct {v15}, Landroid/graphics/Point;-><init>()V

    .line 103
    .local v15, "smallestSize":Landroid/graphics/Point;
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 104
    .local v9, "largestSize":Landroid/graphics/Point;
    invoke-static {v6, v15, v9}, Lcom/google/android/apps/books/util/ReaderUtils;->getCurrentSizeRange(Landroid/view/Display;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 106
    iget v0, v15, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    .line 107
    iget v0, v9, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v9, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v19

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sLargestSize:I

    .line 108
    iget v0, v15, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v15, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sSmallestSize:I

    .line 109
    iget v0, v15, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    iget v0, v15, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v19

    mul-int/lit8 v19, v19, 0x4

    sget v21, Lcom/google/android/apps/books/util/ReaderUtils;->sLargestSize:I

    mul-int v19, v19, v21

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sFullColorScreenBytes:I

    .line 111
    iget v7, v15, Landroid/graphics/Point;->y:I

    .line 112
    .local v7, "landscapeScreenHeight":I
    iget v8, v9, Landroid/graphics/Point;->x:I

    .line 114
    .local v8, "landscapeScreenWidth":I
    const v19, 0x7f09012e

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 116
    .local v12, "minLandscapeHeightForTwoPages":I
    const v19, 0x7f09012f

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 118
    .local v13, "minLandscapeWidthForTwoPages":I
    const v19, 0x7f0900f0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 121
    .local v16, "smallestTabletPortraitWidth":I
    if-lt v7, v12, :cond_5

    if-lt v8, v13, :cond_5

    const/16 v19, 0x1

    :goto_2
    sput-boolean v19, Lcom/google/android/apps/books/util/ReaderUtils;->sTwoFlowingPagesInLandscape:Z

    .line 125
    sget-boolean v19, Lcom/google/android/apps/books/util/ReaderUtils;->sTwoFlowingPagesInLandscape:Z

    if-eqz v19, :cond_3

    div-int/lit8 v8, v8, 0x2

    .end local v8    # "landscapeScreenWidth":I
    :cond_3
    sput v8, Lcom/google/android/apps/books/util/ReaderUtils;->sLandscapeWidth:I

    .line 128
    sget v19, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    move/from16 v0, v16

    move/from16 v1, v19

    if-gt v0, v1, :cond_6

    const/16 v19, 0x1

    :goto_3
    sput-boolean v19, Lcom/google/android/apps/books/util/ReaderUtils;->sIsTablet:Z

    .line 133
    new-instance v10, Landroid/util/DisplayMetrics;

    invoke-direct {v10}, Landroid/util/DisplayMetrics;-><init>()V

    .line 134
    .local v10, "metrics":Landroid/util/DisplayMetrics;
    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 142
    const v18, 0x3faaaaab

    .line 143
    .local v18, "webkitPxPerCssPt":F
    const v19, 0x3faaaaab

    iget v0, v10, Landroid/util/DisplayMetrics;->densityDpi:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v17, v19, v21

    .line 145
    .local v17, "webkitInchesPerPt":F
    const/high16 v3, 0x41900000    # 18.0f

    .line 146
    .local v3, "desiredDpFor12pt":F
    const v4, 0x3de66666    # 0.1125f

    .line 147
    .local v4, "desiredInchesFor12pt":F
    const v5, 0x3c199999    # 0.009374999f

    .line 149
    .local v5, "desiredInchesPerPt":F
    const v19, 0x3c199999    # 0.009374999f

    div-float v19, v19, v17

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoom:F

    .line 151
    sget v19, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoom:F

    const/high16 v21, 0x41000000    # 8.0f

    div-float v19, v19, v21

    sput v19, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoomIncrement:F

    .line 153
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->detectAberrantDevices()V

    .line 155
    const-string v19, "ReaderUtils"

    const/16 v21, 0x4

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 156
    const-string v19, "ReaderUtils"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "PortraitW "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget v22, Lcom/google/android/apps/books/util/ReaderUtils;->sPortraitWidth:I

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " LandscapeW "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget v22, Lcom/google/android/apps/books/util/ReaderUtils;->sLandscapeWidth:I

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " SmallestSize "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget v22, Lcom/google/android/apps/books/util/ReaderUtils;->sSmallestSize:I

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " LargestSize "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget v22, Lcom/google/android/apps/books/util/ReaderUtils;->sLargestSize:I

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " textZoom "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget v22, Lcom/google/android/apps/books/util/ReaderUtils;->sDefaultTextZoom:F

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " isTablet "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-boolean v22, Lcom/google/android/apps/books/util/ReaderUtils;->sIsTablet:Z

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " Memory "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget v22, Lcom/google/android/apps/books/util/ReaderUtils;->sMemoryClass:I

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 81
    .end local v2    # "activityMgr":Landroid/app/ActivityManager;
    .end local v3    # "desiredDpFor12pt":F
    .end local v4    # "desiredInchesFor12pt":F
    .end local v5    # "desiredInchesPerPt":F
    .end local v6    # "display":Landroid/view/Display;
    .end local v7    # "landscapeScreenHeight":I
    .end local v9    # "largestSize":Landroid/graphics/Point;
    .end local v10    # "metrics":Landroid/util/DisplayMetrics;
    .end local v11    # "mgr":Landroid/view/WindowManager;
    .end local v12    # "minLandscapeHeightForTwoPages":I
    .end local v13    # "minLandscapeWidthForTwoPages":I
    .end local v14    # "res":Landroid/content/res/Resources;
    .end local v15    # "smallestSize":Landroid/graphics/Point;
    .end local v16    # "smallestTabletPortraitWidth":I
    .end local v17    # "webkitInchesPerPt":F
    .end local v18    # "webkitPxPerCssPt":F
    :catchall_0
    move-exception v19

    monitor-exit v20

    throw v19

    .line 92
    .restart local v2    # "activityMgr":Landroid/app/ActivityManager;
    .restart local v14    # "res":Landroid/content/res/Resources;
    :cond_4
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 121
    .restart local v6    # "display":Landroid/view/Display;
    .restart local v7    # "landscapeScreenHeight":I
    .restart local v8    # "landscapeScreenWidth":I
    .restart local v9    # "largestSize":Landroid/graphics/Point;
    .restart local v11    # "mgr":Landroid/view/WindowManager;
    .restart local v12    # "minLandscapeHeightForTwoPages":I
    .restart local v13    # "minLandscapeWidthForTwoPages":I
    .restart local v15    # "smallestSize":Landroid/graphics/Point;
    .restart local v16    # "smallestTabletPortraitWidth":I
    :cond_5
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 128
    .end local v8    # "landscapeScreenWidth":I
    :cond_6
    const/16 v19, 0x0

    goto/16 :goto_3
.end method

.method public static parsePageNumber(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4
    .param p0, "pageTitle"    # Ljava/lang/String;

    .prologue
    .line 405
    const-string v3, "\\d+"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 406
    .local v2, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 407
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 408
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 409
    .local v0, "match":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    .line 411
    .end local v0    # "match":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static searchWebSelectedText(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    .line 639
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 640
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 641
    const-string v5, "query"

    invoke-virtual {v1, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 643
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 644
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 645
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v2, 0x1

    .line 648
    .local v2, "isIntentSafe":Z
    :cond_0
    if-nez v2, :cond_1

    .line 649
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 650
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 651
    const-string v5, "http://www.google.com/search"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "q"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 653
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 656
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->markExternalIntent(Landroid/content/Intent;)V

    .line 657
    sget-object v5, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_SEARCH:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTextSelectionAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;)V

    .line 660
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 661
    return-void
.end method

.method public static shouldUseDarkTheme(Ljava/lang/String;)Z
    .locals 1
    .param p0, "readerTheme"    # Ljava/lang/String;

    .prologue
    .line 455
    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static supportsOpenGLES2()Z
    .locals 1

    .prologue
    .line 361
    sget-boolean v0, Lcom/google/android/apps/books/util/ReaderUtils;->sSupportsOpenGLES2:Z

    return v0
.end method
