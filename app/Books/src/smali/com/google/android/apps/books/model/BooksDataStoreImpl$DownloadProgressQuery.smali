.class interface abstract Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadProgressQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2070
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "segment_fraction"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resource_fraction"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "page_fraction"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "structure_fraction"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;->PROJECTION:[Ljava/lang/String;

    .line 2076
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method
