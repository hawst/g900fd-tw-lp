.class public interface abstract Lcom/google/android/apps/books/render/ImageModeSelectionState$Callbacks;
.super Ljava/lang/Object;
.source "ImageModeSelectionState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeSelectionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract onSelectionAppearanceChanged(Lcom/google/android/apps/books/render/PageStructureSelection;)V
.end method

.method public abstract onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
.end method
