.class public Lcom/google/android/apps/books/model/VolumesQuery;
.super Ljava/lang/Object;
.source "VolumesQuery.java"


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static sColumnToIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "volume_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "cover_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "creator"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "preferred_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "has_image_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "has_text_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "viewability"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "buy_url"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "open_access"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "page_count"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "canonical_url"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "language"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "spread"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "is_right_to_left"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "publisher"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "tts_permission"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "content_version"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "first_chapter_start_segment_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "media_overlay_active_class"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "has_media_overlays"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "is_uploaded"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "version"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "rental_state"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "rental_start"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "rental_expiration"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "explicit_offline_license"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "max_offline_devices"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "image_mode_first_book_body_page"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "image_mode_last_book_body_page"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "text_mode_first_book_body_page"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "text_mode_last_book_body_page"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "quote_sharing_allowed"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/model/VolumesQuery;->PROJECTION:[Ljava/lang/String;

    .line 35
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    sget-object v1, Lcom/google/android/apps/books/model/VolumesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/VolumesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/books/model/VolumesQuery;->sColumnToIndex:Ljava/util/Map;

    return-void
.end method
