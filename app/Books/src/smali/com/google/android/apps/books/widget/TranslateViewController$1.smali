.class Lcom/google/android/apps/books/widget/TranslateViewController$1;
.super Ljava/lang/Object;
.source "TranslateViewController.java"

# interfaces
.implements Landroid/support/v7/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TranslateViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/TranslateViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/TranslateViewController;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$1;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/support/v7/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/support/v7/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 102
    .local v0, "inflater":Landroid/view/MenuInflater;
    if-eqz v0, :cond_0

    .line 103
    const v1, 0x7f120002

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$1;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mActionModeTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$000(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/support/v7/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 106
    const/4 v1, 0x1

    return v1
.end method

.method public onDestroyActionMode(Landroid/support/v7/view/ActionMode;)V
    .locals 1
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$1;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->dismiss()V

    .line 97
    return-void
.end method

.method public onPrepareActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 91
    const/4 v0, 0x1

    return v0
.end method
