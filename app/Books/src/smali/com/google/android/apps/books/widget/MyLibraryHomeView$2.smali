.class Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;
.super Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;
.source "MyLibraryHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/MyLibraryHomeView;->setCardsAdapter(Lcom/google/android/apps/books/app/LibraryFilter;Lcom/google/android/apps/books/widget/HeaderGridViewController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

.field final synthetic val$extraCellPadding:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/MyLibraryHomeView;Landroid/widget/Adapter;I)V
    .locals 0
    .param p2, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    iput p3, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->val$extraCellPadding:I

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;-><init>(Landroid/widget/Adapter;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;
    .param p2, "x1"    # I

    .prologue
    .line 276
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;
    .locals 6
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 279
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/widget/ListToRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;

    move-result-object v0

    .line 280
    .local v0, "handle":Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;, "Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle<*>;"
    iget-object v1, v0, Lcom/google/android/apps/books/widget/TypeSafeRecyclerViewAdapter$ViewHandle;->itemView:Landroid/view/View;

    iget v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->val$extraCellPadding:I

    iget v3, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->val$extraCellPadding:I

    iget v4, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->val$extraCellPadding:I

    iget v5, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$2;->val$extraCellPadding:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 282
    return-object v0
.end method
