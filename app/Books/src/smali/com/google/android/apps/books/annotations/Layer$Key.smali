.class public Lcom/google/android/apps/books/annotations/Layer$Key;
.super Ljava/lang/Object;
.source "Layer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/Layer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Key"
.end annotation


# instance fields
.field public final layerId:Ljava/lang/String;

.field public final type:Lcom/google/android/apps/books/annotations/Layer$Type;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/android/apps/books/annotations/Layer$Type;
    .param p3, "layerId"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    .line 51
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$Type;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    .line 52
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 68
    instance-of v2, p1, Lcom/google/android/apps/books/annotations/Layer$Key;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 69
    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$Key;

    .line 70
    .local v0, "otherKey":Lcom/google/android/apps/books/annotations/Layer$Key;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 74
    .end local v0    # "otherKey":Lcom/google/android/apps/books/annotations/Layer$Key;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "volumeId"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "type"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "layerId"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
