.class public interface abstract Lcom/google/android/apps/books/common/VolumeUsageManager;
.super Ljava/lang/Object;
.source "VolumeUsageManager.java"


# virtual methods
.method public abstract acquireVolumeLock(Ljava/lang/String;Ljava/lang/Object;)V
.end method

.method public abstract isVolumeLocked(Ljava/lang/String;)Z
.end method

.method public abstract releaseVolumeLock(Ljava/lang/String;Ljava/lang/Object;)V
.end method
