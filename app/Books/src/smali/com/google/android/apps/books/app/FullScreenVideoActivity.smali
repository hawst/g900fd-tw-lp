.class public Lcom/google/android/apps/books/app/FullScreenVideoActivity;
.super Lcom/google/android/ublib/actionbar/UBLibActivity;
.source "FullScreenVideoActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field private mCurrentPosition:I

.field private mExpireRentalHandler:Landroid/os/Handler;

.field private mIncomingIntent:Landroid/content/Intent;

.field private mIsPaused:Z

.field private mIsPreparing:Z

.field private mMediaController:Landroid/widget/MediaController;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSystemUi:Lcom/google/android/ublib/view/SystemUi;

.field private mVideoUrl:Ljava/lang/String;

.field private mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;-><init>()V

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPreparing:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/FullScreenVideoActivity;)Landroid/widget/MediaController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/FullScreenVideoActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mMediaController:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/FullScreenVideoActivity;Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/FullScreenVideoActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/graphics/Rect;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->maybePushViewAboveSystemWindows(Landroid/view/View;Landroid/graphics/Rect;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/FullScreenVideoActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/FullScreenVideoActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setActionAndNavBarsVisible(Z)V

    return-void
.end method

.method private static createExpireRentalHandler(Lcom/google/android/apps/books/app/FullScreenVideoActivity;)Landroid/os/Handler;
    .locals 2
    .param p0, "videoActivity"    # Lcom/google/android/apps/books/app/FullScreenVideoActivity;

    .prologue
    .line 69
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/app/FullScreenVideoActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity$1;-><init>(Lcom/google/android/apps/books/app/FullScreenVideoActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    return-object v0
.end method

.method private maybePushViewAboveSystemWindows(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 223
    if-eqz p1, :cond_1

    iget v1, p2, Landroid/graphics/Rect;->right:I

    if-nez v1, :cond_0

    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    if-eqz v1, :cond_1

    .line 224
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 226
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget v3, p2, Landroid/graphics/Rect;->right:I

    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 228
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    .end local v0    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    return-void
.end method

.method private returnFromActivity(I)V
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 337
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 338
    .local v0, "resultIntent":Landroid/content/Intent;
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setResult(ILandroid/content/Intent;)V

    .line 339
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->finish()V

    .line 340
    return-void
.end method

.method private setActionAndNavBarsVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 318
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 319
    if-eqz p1, :cond_2

    .line 320
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 327
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    if-eqz v1, :cond_1

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    invoke-interface {v1, p1}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    .line 330
    :cond_1
    return-void

    .line 322
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 344
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->returnFromActivity(I)V

    .line 345
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v2, 0x0

    .line 306
    const-string v0, "FullScreenVideoActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const-string v0, "FullScreenVideoActivity"

    const-string v1, "video onCompletion"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_0
    iput v2, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mCurrentPosition:I

    .line 310
    iget v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mCurrentPosition:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    .line 312
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setActionAndNavBarsVisible(Z)V

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0, v2}, Landroid/widget/MediaController;->show(I)V

    .line 314
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-super/range {p0 .. p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIncomingIntent:Landroid/content/Intent;

    .line 84
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIncomingIntent:Landroid/content/Intent;

    invoke-virtual {v11}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "action":Ljava/lang/String;
    const-string v11, "android.intent.action.VIEW"

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 86
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIncomingIntent:Landroid/content/Intent;

    invoke-virtual {v11}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoUrl:Ljava/lang/String;

    .line 87
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoUrl:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 88
    const-string v11, "FullScreenVideoActivity"

    const/4 v14, 0x3

    invoke-static {v11, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 89
    const-string v11, "FullScreenVideoActivity"

    const-string v14, " empty URL"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->returnFromActivity(I)V

    .line 99
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 100
    const-string v11, "video_position"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mCurrentPosition:I

    .line 101
    const-string v11, "video_is_paused"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    .line 103
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setRequestedOrientation(I)V

    .line 106
    const v11, 0x7f040054

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setContentView(I)V

    .line 107
    const v11, 0x7f0e011c

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/ublib/view/SystemUiUtils;->makeSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    .line 108
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    const/4 v14, 0x1

    invoke-interface {v11, v14}, Lcom/google/android/ublib/view/SystemUi;->setViewFullscreen(Z)V

    .line 110
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setActionAndNavBarsVisible(Z)V

    .line 111
    const v11, 0x7f0e011d

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/widget/BooksVideoView;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    .line 112
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    new-instance v14, Lcom/google/android/apps/books/app/FullScreenVideoActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity$2;-><init>(Lcom/google/android/apps/books/app/FullScreenVideoActivity;)V

    invoke-virtual {v11, v14}, Lcom/google/android/apps/books/widget/BooksVideoView;->setCallbacks(Lcom/google/android/apps/books/widget/BooksVideoView$Callbacks;)V

    .line 119
    const v11, 0x7f0e011e

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 120
    new-instance v11, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v11, v0, v1}, Lcom/google/android/apps/books/app/FullScreenVideoActivity$3;-><init>(Lcom/google/android/apps/books/app/FullScreenVideoActivity;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mMediaController:Landroid/widget/MediaController;

    .line 148
    const/4 v11, 0x0

    sget-object v14, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v11, v14, v15, v1}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 149
    .local v5, "attributes":Landroid/content/res/TypedArray;
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 151
    .local v6, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 152
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIncomingIntent:Landroid/content/Intent;

    const-string v14, "title"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 154
    .local v7, "bookTitle":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIncomingIntent:Landroid/content/Intent;

    const-string v14, "expiration"

    const-wide/16 v16, -0x1

    move-wide/from16 v0, v16

    invoke-virtual {v11, v14, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    .line 156
    .local v12, "rentalExpiration":J
    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-lez v11, :cond_4

    .line 157
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    cmp-long v11, v14, v12

    if-lez v11, :cond_3

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->finish()V

    .line 161
    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->createExpireRentalHandler(Lcom/google/android/apps/books/app/FullScreenVideoActivity;)Landroid/os/Handler;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mExpireRentalHandler:Landroid/os/Handler;

    .line 163
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long v8, v12, v14

    .line 164
    .local v8, "delay":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mExpireRentalHandler:Landroid/os/Handler;

    const/4 v14, 0x1

    invoke-virtual {v11, v14, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 166
    .end local v8    # "delay":J
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    .line 167
    .local v3, "actionBar":Landroid/support/v7/app/ActionBar;
    invoke-virtual {v3, v6}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 168
    if-eqz v7, :cond_5

    .line 169
    invoke-virtual {v3, v7}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 171
    :cond_5
    const/16 v4, 0xc

    .line 173
    .local v4, "actionBarHomeOptions":I
    const/16 v10, 0xe

    .line 175
    .local v10, "mask":I
    const/16 v11, 0xc

    const/16 v14, 0xe

    invoke-virtual {v3, v11, v14}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 176
    return-void

    .line 94
    .end local v3    # "actionBar":Landroid/support/v7/app/ActionBar;
    .end local v4    # "actionBarHomeOptions":I
    .end local v5    # "attributes":Landroid/content/res/TypedArray;
    .end local v6    # "background":Landroid/graphics/drawable/Drawable;
    .end local v7    # "bookTitle":Ljava/lang/CharSequence;
    .end local v10    # "mask":I
    .end local v12    # "rentalExpiration":J
    :cond_6
    const-string v11, "FullScreenVideoActivity"

    const/4 v14, 0x3

    invoke-static {v11, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 95
    const-string v11, "FullScreenVideoActivity"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " unrecognized intent action: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_7
    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->returnFromActivity(I)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onDestroy()V

    .line 209
    iput-object v2, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mExpireRentalHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mExpireRentalHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 213
    iput-object v2, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mExpireRentalHandler:Landroid/os/Handler;

    .line 215
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v6, 0x1

    .line 258
    const-string v1, "FullScreenVideoActivity"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    const-string v1, "FullScreenVideoActivity"

    const-string v2, "onError, what=%d, extra=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_0
    iput-boolean v6, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    .line 262
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setActionAndNavBarsVisible(Z)V

    .line 264
    if-ne p2, v6, :cond_1

    const/16 v1, -0x3ec

    if-ne p3, v1, :cond_1

    .line 265
    const v0, 0x7f0f014a

    .line 269
    .local v0, "errorMessageId":I
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 271
    return v6

    .line 267
    .end local v0    # "errorMessageId":I
    :cond_1
    const v0, 0x7f0f014b

    .restart local v0    # "errorMessageId":I
    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 250
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 246
    :pswitch_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->returnFromActivity(I)V

    .line 247
    const/4 v0, 0x1

    goto :goto_0

    .line 244
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v2, 0x0

    .line 279
    const-string v0, "FullScreenVideoActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "FullScreenVideoActivity"

    const-string v1, "video onPrepared"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPreparing:Z

    .line 285
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    const-string v0, "FullScreenVideoActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    const-string v0, "FullScreenVideoActivity"

    const-string v1, "  onPrepared called after finish()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_1
    :goto_0
    return-void

    .line 292
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/BooksVideoView;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    .line 295
    iget v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mCurrentPosition:I

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 296
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPaused:Z

    if-nez v0, :cond_1

    .line 297
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 180
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onResume()V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/widget/BooksVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/widget/BooksVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/widget/BooksVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksVideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 187
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPreparing:Z

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/BooksVideoView;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksVideoView;->requestFocus()Z

    .line 190
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->setActionAndNavBarsVisible(Z)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 192
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 234
    invoke-super {p0, p1}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "video_position"

    iget-object v1, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BooksVideoView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 238
    const-string v1, "video_is_paused"

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mIsPreparing:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/FullScreenVideoActivity;->mVideoView:Lcom/google/android/apps/books/widget/BooksVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 240
    :cond_0
    return-void

    .line 238
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStart()V

    .line 197
    invoke-static {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->incrementNumResumedActivities(Landroid/app/Activity;)V

    .line 198
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 202
    invoke-static {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication;->decrementNumResumedActivities(Landroid/app/Activity;)V

    .line 203
    invoke-super {p0}, Lcom/google/android/ublib/actionbar/UBLibActivity;->onStop()V

    .line 204
    return-void
.end method
