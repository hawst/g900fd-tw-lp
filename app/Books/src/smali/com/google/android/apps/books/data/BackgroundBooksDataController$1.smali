.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$1;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->initializeDataStore()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$1;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$1;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore;->cleanTempDirectory()V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$1;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore;->ensureMyEbooksCollection()V

    .line 223
    return-void
.end method
