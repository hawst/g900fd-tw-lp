.class Lcom/google/android/apps/books/view/pages/Bezier;
.super Ljava/lang/Object;
.source "Bezier.java"


# static fields
.field private static final sPrevious:Landroid/graphics/PointF;

.field private static final sPt:Landroid/graphics/PointF;


# instance fields
.field private final mValues:[F

.field private nb:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/Bezier;->sPrevious:Landroid/graphics/PointF;

    .line 60
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/Bezier;->sPt:Landroid/graphics/PointF;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/view/pages/Bezier;->nb:I

    .line 16
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    .line 19
    return-void
.end method


# virtual methods
.method public addValue(I)V
    .locals 4
    .param p1, "v"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    iget v1, p0, Lcom/google/android/apps/books/view/pages/Bezier;->nb:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/books/view/pages/Bezier;->nb:I

    int-to-float v2, p1

    const v3, 0x3a83126f    # 0.001f

    mul-float/2addr v2, v3

    aput v2, v0, v1

    .line 29
    return-void
.end method

.method public getLength()F
    .locals 7

    .prologue
    .line 66
    const/16 v3, 0x32

    .line 67
    .local v3, "nbSamples":I
    const v0, 0x3ca3d70a    # 0.02f

    .line 69
    .local v0, "interval":F
    const/4 v2, 0x0

    .line 70
    .local v2, "length":F
    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/books/view/pages/Bezier;->sPrevious:Landroid/graphics/PointF;

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/books/view/pages/Bezier;->pointAt(FLandroid/graphics/PointF;)V

    .line 71
    const/4 v1, 0x1

    .local v1, "j":I
    :goto_0
    const/16 v4, 0x32

    if-gt v1, v4, :cond_0

    .line 72
    int-to-float v4, v1

    const v5, 0x3ca3d70a    # 0.02f

    mul-float/2addr v4, v5

    sget-object v5, Lcom/google/android/apps/books/view/pages/Bezier;->sPt:Landroid/graphics/PointF;

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/books/view/pages/Bezier;->pointAt(FLandroid/graphics/PointF;)V

    .line 73
    sget-object v4, Lcom/google/android/apps/books/view/pages/Bezier;->sPt:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sget-object v5, Lcom/google/android/apps/books/view/pages/Bezier;->sPrevious:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    sget-object v5, Lcom/google/android/apps/books/view/pages/Bezier;->sPt:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sget-object v6, Lcom/google/android/apps/books/view/pages/Bezier;->sPrevious:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    invoke-static {v4, v5}, Landroid/graphics/PointF;->length(FF)F

    move-result v4

    add-float/2addr v2, v4

    .line 74
    sget-object v4, Lcom/google/android/apps/books/view/pages/Bezier;->sPrevious:Landroid/graphics/PointF;

    sget-object v5, Lcom/google/android/apps/books/view/pages/Bezier;->sPt:Landroid/graphics/PointF;

    invoke-virtual {v4, v5}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    :cond_0
    return v2
.end method

.method pointAt(FLandroid/graphics/PointF;)V
    .locals 10
    .param p1, "t"    # F
    .param p2, "p"    # Landroid/graphics/PointF;

    .prologue
    .line 38
    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v1, v6, p1

    .line 39
    .local v1, "onet":F
    mul-float v6, p1, p1

    mul-float v3, v6, p1

    .line 40
    .local v3, "t3":F
    mul-float v6, v1, v1

    mul-float v2, v6, v1

    .line 41
    .local v2, "onet3":F
    const/high16 v6, 0x40400000    # 3.0f

    mul-float/2addr v6, p1

    mul-float v0, v6, v1

    .line 44
    .local v0, "cross":F
    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v7, 0x6

    aget v6, v6, v7

    mul-float/2addr v6, v2

    mul-float v7, v0, v1

    iget-object v8, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v9, 0x4

    aget v8, v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float v7, v0, p1

    iget-object v8, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v9, 0x2

    aget v8, v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    mul-float/2addr v7, v3

    add-float v4, v6, v7

    .line 49
    .local v4, "x":F
    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v7, 0x7

    aget v6, v6, v7

    mul-float/2addr v6, v2

    mul-float v7, v0, v1

    iget-object v8, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float v7, v0, p1

    iget-object v8, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v9, 0x3

    aget v8, v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/books/view/pages/Bezier;->mValues:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    mul-float/2addr v7, v3

    add-float v5, v6, v7

    .line 54
    .local v5, "y":F
    invoke-virtual {p2, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    .line 55
    return-void
.end method
