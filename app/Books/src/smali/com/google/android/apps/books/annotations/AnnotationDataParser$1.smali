.class final Lcom/google/android/apps/books/annotations/AnnotationDataParser$1;
.super Ljava/lang/Object;
.source "AnnotationDataParser.java"

# interfaces
.implements Lcom/google/android/apps/books/util/JsonUtils$NodeParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationDataParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
        "<",
        "Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    .locals 6
    .param p1, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 218
    const-string v4, "senses"

    invoke-virtual {p1, v4}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v4

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->SENSE_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$000()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v3

    .line 219
    .local v3, "senses":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;>;"
    const-string v4, "examples"

    invoke-virtual {p1, v4}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v4

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$100()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v2

    .line 221
    .local v2, "examples":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    const-string v4, "derivates"

    invoke-virtual {p1, v4}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v4

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$100()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v1

    .line 223
    .local v1, "derivatives":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    const-string v4, "source"

    invoke-virtual {p1, v4}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v4

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAttribution(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    invoke-static {v4}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$200(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    move-result-object v0

    .line 224
    .local v0, "attribution":Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    new-instance v4, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    return-object v4
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 215
    check-cast p1, Lorg/codehaus/jackson/JsonNode;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$1;->apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;

    move-result-object v0

    return-object v0
.end method
