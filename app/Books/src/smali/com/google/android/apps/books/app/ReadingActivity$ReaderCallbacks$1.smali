.class Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;
.super Ljava/lang/Object;
.source "ReadingActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->showNewPositionAvailableDialog(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$chapterTitle:Ljava/lang/String;

.field final synthetic val$pageTitle:Ljava/lang/String;

.field final synthetic val$position:Lcom/google/android/apps/books/common/Position;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$volumeId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$position:Lcom/google/android/apps/books/common/Position;

    iput-object p5, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$chapterTitle:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$pageTitle:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 145
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    iget-object v3, v3, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/ReadingActivity;->isActivityDestroyed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->this$1:Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;

    iget-object v3, v3, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks;->this$0:Lcom/google/android/apps/books/app/ReadingActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/ReadingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 149
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 150
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$account:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$volumeId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$position:Lcom/google/android/apps/books/common/Position;

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$chapterTitle:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReadingActivity$ReaderCallbacks$1;->val$pageTitle:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/apps/books/app/NewPositionAvailableFragment$Arguments;->create(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 152
    .local v0, "arguments":Landroid/os/Bundle;
    const-class v3, Lcom/google/android/apps/books/app/NewPositionAvailableFragment;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v2, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 154
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method
