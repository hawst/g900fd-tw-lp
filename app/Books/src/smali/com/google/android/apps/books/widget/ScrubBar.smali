.class public Lcom/google/android/apps/books/widget/ScrubBar;
.super Landroid/widget/FrameLayout;
.source "ScrubBar.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;,
        Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;,
        Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;,
        Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;,
        Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;
    }
.end annotation


# static fields
.field public static final UNDO_THUMBNAIL_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;


# instance fields
.field private final mAccessibilityEnabled:Z

.field private mAllPointsOfInterest:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimatingPosition:Z

.field private final mBookmarkClick:Landroid/view/View$OnClickListener;

.field private mBuyButton:Landroid/widget/TextView;

.field private final mDisplayDensity:F

.field private final mExtraBorderHeight:I

.field private mFirstEventX:F

.field private mFreezePositionInProgress:Z

.field private mIsTrackingEvent:Z

.field private mItemPosition:I

.field private mKnobView:Landroid/widget/ImageView;

.field private mLastEventTime:J

.field private mLastEventX:F

.field private mLastReadChapterLabel:Ljava/lang/CharSequence;

.field private mLastReadPageLabel:Ljava/lang/CharSequence;

.field private mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

.field private final mMovementThreshold:I

.field private mPositionUpdateSequenceId:I

.field private final mPostPositionUpdateHandler:Landroid/os/Handler;

.field private mQueuedPosition:Ljava/lang/Integer;

.field private mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

.field private mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
            "<*>;"
        }
    .end annotation
.end field

.field private mSosImageView:Landroid/widget/ImageView;

.field private mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

.field private final mStartOfSkimClick:Landroid/view/View$OnClickListener;

.field private mStartedMoving:Z

.field private mTempPoint:Landroid/graphics/Point;

.field private mThumbnailMaxSizeInPixels:Landroid/graphics/Point;

.field mThumbnailRenderConsumer:Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;

.field private mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

.field private final mVelocityThreshold:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 242
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/books/widget/ScrubBar;->UNDO_THUMBNAIL_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 329
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 330
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 333
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/ScrubBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 334
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 337
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 248
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    .line 254
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAnimatingPosition:Z

    .line 263
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z

    .line 266
    iput-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;

    .line 278
    iput v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    .line 294
    const-string v1, "BooksScrubberVelocity"

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ScrubBar"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mVelocityThreshold:F

    .line 323
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAllPointsOfInterest:Ljava/util/List;

    .line 325
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTempPoint:Landroid/graphics/Point;

    .line 326
    iput-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailMaxSizeInPixels:Landroid/graphics/Point;

    .line 593
    new-instance v1, Lcom/google/android/apps/books/widget/ScrubBar$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/ScrubBar$1;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPostPositionUpdateHandler:Landroid/os/Handler;

    .line 1042
    new-instance v1, Lcom/google/android/apps/books/widget/ScrubBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/ScrubBar$2;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkimClick:Landroid/view/View$OnClickListener;

    .line 1083
    new-instance v1, Lcom/google/android/apps/books/widget/ScrubBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/ScrubBar$3;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBookmarkClick:Landroid/view/View$OnClickListener;

    .line 1098
    iput-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailRenderConsumer:Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;

    .line 338
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAccessibilityEnabled:Z

    .line 339
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 340
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mDisplayDensity:F

    .line 341
    const v1, 0x7f090133

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mMovementThreshold:I

    .line 343
    const v1, 0x7f09013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mExtraBorderHeight:I

    .line 345
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->createBookmarkViews()V

    .line 346
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/ScrubBar;Landroid/view/View;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/ScrubBar;->layoutChild(Landroid/view/View;II)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTempPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getThumbnailMaxSizeInPixels()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/widget/ScrubBar;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mSosImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/widget/ScrubBar;)Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/ScrubBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/widget/ScrubBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/ScrubBar;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/ScrubBar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->setPositionAndPause(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/widget/ScrubBar;)Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    return-object v0
.end method

.method private attemptClaimDrag()V
    .locals 2

    .prologue
    .line 1039
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1040
    return-void
.end method

.method private computePosition(III)I
    .locals 3
    .param p1, "itemIndex"    # I
    .param p2, "trackLeft"    # I
    .param p3, "trackWidth"    # I

    .prologue
    .line 517
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->getItemCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 518
    .local v0, "intervals":I
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/ScrubBar;->maybeFlipItem(II)I

    move-result v1

    .line 519
    .local v1, "item":I
    if-gtz v0, :cond_1

    .end local p2    # "trackLeft":I
    :goto_1
    return p2

    .line 517
    .end local v0    # "intervals":I
    .end local v1    # "item":I
    .restart local p2    # "trackLeft":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 519
    .restart local v0    # "intervals":I
    .restart local v1    # "item":I
    :cond_1
    mul-int v2, v1, p3

    div-int/2addr v2, v0

    add-int/2addr p2, v2

    goto :goto_1
.end method

.method private computeScrubberIndex(F)I
    .locals 8
    .param p1, "xCoord"    # F

    .prologue
    const/4 v5, 0x0

    .line 524
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->findNearbyPointOfInterest(F)I

    move-result v1

    .line 525
    .local v1, "pointOfInterest":I
    const/4 v6, -0x1

    if-eq v1, v6, :cond_0

    .line 536
    .end local v1    # "pointOfInterest":I
    :goto_0
    return v1

    .line 529
    .restart local v1    # "pointOfInterest":I
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->getItemCount()I

    move-result v6

    add-int/lit8 v0, v6, -0x1

    .line 530
    .local v0, "intervals":I
    :goto_1
    if-lez v0, :cond_2

    .line 531
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getLeft()I

    move-result v3

    .line 532
    .local v3, "trackLeft":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getWidth()I

    move-result v4

    .line 533
    .local v4, "trackWidth":I
    int-to-float v6, v3

    sub-float v6, p1, v6

    int-to-float v7, v0

    mul-float/2addr v6, v7

    int-to-float v7, v4

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 534
    .local v2, "touchItem":I
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/books/widget/ScrubBar;->maybeFlipItem(II)I

    move-result v6

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    .end local v0    # "intervals":I
    .end local v2    # "touchItem":I
    .end local v3    # "trackLeft":I
    .end local v4    # "trackWidth":I
    :cond_1
    move v0, v5

    .line 529
    goto :goto_1

    .restart local v0    # "intervals":I
    :cond_2
    move v1, v5

    .line 536
    goto :goto_0
.end method

.method private createBookmarkViews()V
    .locals 9

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 375
    .local v0, "bookmarksCount":I
    new-array v6, v0, [Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iput-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .line 377
    move-object v2, p0

    .line 378
    .local v2, "group":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 379
    .local v5, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f0201

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "contentDescription":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 382
    const v6, 0x7f0400ac

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 383
    .local v4, "icon":Landroid/view/View;
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 384
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBookmarkClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 386
    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 387
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 389
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    new-instance v7, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    invoke-static {v2, v5}, Lcom/google/android/apps/books/widget/ScrubBar;->makePositionDot(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v8

    invoke-direct {v7, v4, v8, v1}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;-><init>(Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V

    aput-object v7, v6, v3

    .line 381
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 392
    .end local v4    # "icon":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private dispatchScrubContinues(I)V
    .locals 1
    .param p1, "itemPosition"    # I

    .prologue
    .line 666
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;->onScrubUpdated(I)V

    .line 669
    :cond_0
    return-void
.end method

.method private dispatchScrubFinished(I)V
    .locals 1
    .param p1, "itemPosition"    # I

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;->onScrubFinished(I)V

    .line 663
    :cond_0
    return-void
.end method

.method private dispatchScrubStarted()V
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;->onScrubStarted()V

    .line 657
    :cond_0
    return-void
.end method

.method private findNearbyPointOfInterest(F)I
    .locals 8
    .param p1, "eventX"    # F

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090192

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 575
    .local v3, "gravityWellSize":I
    const/4 v1, -0x1

    .line 576
    .local v1, "closestScrubIndex":I
    const/high16 v0, 0x4f000000

    .line 578
    .local v0, "closestDistance":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAllPointsOfInterest:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .line 579
    .local v5, "point":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    # invokes: Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->hasValidLocation()Z
    invoke-static {v5}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->access$100(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 580
    # invokes: Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionCenter()F
    invoke-static {v5}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->access$200(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)F

    move-result v6

    sub-float/2addr v6, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 581
    .local v2, "distance":F
    int-to-float v6, v3

    cmpg-float v6, v2, v6

    if-gtz v6, :cond_0

    cmpg-float v6, v2, v0

    if-gez v6, :cond_0

    .line 582
    move v0, v2

    .line 583
    iget v1, v5, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    goto :goto_0

    .line 588
    .end local v2    # "distance":F
    .end local v5    # "point":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    :cond_1
    return v1
.end method

.method private findXCoordOfPointOfInterest(I)F
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    .line 559
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAllPointsOfInterest:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .line 560
    .local v1, "point":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    iget v2, v1, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    if-ne v2, p1, :cond_0

    # invokes: Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->hasValidLocation()Z
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->access$100(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 561
    # invokes: Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionCenter()F
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->access$200(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)F

    move-result v2

    .line 564
    .end local v1    # "point":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    :goto_0
    return v2

    :cond_1
    const/high16 v2, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method private finishTrackingTouchEvent()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 972
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPostPositionUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 973
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mIsTrackingEvent:Z

    .line 974
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->setPressed(Z)V

    .line 975
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->invalidate()V

    .line 976
    return-void
.end method

.method private getBuyMargins()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/JavaUtils;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    return-object v0
.end method

.method private getKnobX(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 836
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v0, v4, 0x2

    .line 838
    .local v0, "halfKnobWidth":I
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->findXCoordOfPointOfInterest(I)F

    move-result v4

    float-to-int v1, v4

    .line 839
    .local v1, "interestingXCoord":I
    if-ltz v1, :cond_0

    .line 840
    sub-int v4, v1, v0

    .line 844
    :goto_0
    return v4

    .line 842
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getLeft()I

    move-result v2

    .line 843
    .local v2, "trackLeft":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getWidth()I

    move-result v3

    .line 844
    .local v3, "trackWidth":I
    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->computePosition(III)I

    move-result v4

    sub-int/2addr v4, v0

    goto :goto_0
.end method

.method private getThumbnailMaxSizeInPixels()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 687
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailMaxSizeInPixels:Landroid/graphics/Point;

    if-nez v3, :cond_0

    .line 688
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 690
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f090137

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 691
    .local v1, "maxWidth":I
    const v3, 0x7f090138

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 692
    .local v0, "maxHeight":I
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailMaxSizeInPixels:Landroid/graphics/Point;

    .line 694
    .end local v0    # "maxHeight":I
    .end local v1    # "maxWidth":I
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailMaxSizeInPixels:Landroid/graphics/Point;

    return-object v3
.end method

.method private incPositionUpdateSequenceId()V
    .locals 2

    .prologue
    .line 484
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 485
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    .line 489
    :goto_0
    return-void

    .line 487
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    goto :goto_0
.end method

.method private layoutChild(Landroid/view/View;II)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I

    .prologue
    .line 875
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 876
    .local v1, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 877
    .local v0, "height":I
    add-int v2, p2, v1

    add-int v3, p3, v0

    invoke-virtual {p1, p2, p3, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 878
    return-void
.end method

.method private layoutSegments(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 829
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    .line 830
    .local v1, "segment":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->layoutAndShow()V

    goto :goto_0

    .line 832
    .end local v1    # "segment":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;
    :cond_0
    return-void
.end method

.method private static makePositionDot(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3
    .param p0, "group"    # Landroid/view/ViewGroup;
    .param p1, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 232
    const v1, 0x7f0400ad

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 235
    .local v0, "position":Landroid/view/View;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 236
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 237
    return-object v0
.end method

.method private maybeFlipItem(II)I
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "intervals"    # I

    .prologue
    .line 511
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->isRightToLeft()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 512
    .local v0, "isRTL":Z
    :goto_0
    if-eqz v0, :cond_0

    sub-int p1, p2, p1

    .end local p1    # "itemIndex":I
    :cond_0
    return p1

    .line 511
    .end local v0    # "isRTL":Z
    .restart local p1    # "itemIndex":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeSetPositionDelayed(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    const/4 v1, 0x3

    .line 468
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z

    if-eqz v0, :cond_1

    .line 469
    const-string v0, "ScrubBar"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    const-string v0, "ScrubBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queuing up position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;

    .line 479
    :goto_0
    return-void

    .line 474
    :cond_1
    const-string v0, "ScrubBar"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 475
    const-string v0, "ScrubBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updating to position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " without queueing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->setPositionAndPause(I)V

    goto :goto_0
.end method

.method private positionKnob()V
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->getKnobX(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    .line 853
    return-void
.end method

.method private readAloudCurrentPositionIfAccessibilityEnabled()I
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x0

    return v0
.end method

.method private resolveAndLayout(Lcom/google/android/apps/books/widget/SegmentPositionResolver;Ljava/util/List;)V
    .locals 1
    .param p1, "segmentResolver"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/SegmentPositionResolver;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 823
    .local p2, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->resolvePositions(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 824
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/ScrubBar;->layoutSegments(Ljava/util/List;)V

    .line 826
    :cond_0
    return-void
.end method

.method private setPositionAndPause(I)V
    .locals 6
    .param p1, "itemIndex"    # I

    .prologue
    .line 492
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z

    .line 493
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;

    .line 494
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->setPosition(I)V

    .line 495
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->readAloudCurrentPositionIfAccessibilityEnabled()I

    move-result v1

    .line 497
    .local v1, "pauseLength":I
    if-nez v1, :cond_0

    .line 499
    const/16 v1, 0x3e8

    .line 502
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 503
    .local v0, "message":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v0, Landroid/os/Message;->what:I

    .line 504
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->incPositionUpdateSequenceId()V

    .line 505
    iget v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPositionUpdateSequenceId:I

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 506
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mPostPositionUpdateHandler:Landroid/os/Handler;

    int-to-long v4, v1

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 507
    return-void
.end method

.method private startTrackingTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 888
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 890
    .local v1, "eventY":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getTop()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v1, v6

    if-gez v6, :cond_1

    move v5, v4

    .line 930
    :cond_0
    :goto_0
    return v5

    .line 894
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 895
    .local v0, "eventX":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getWidth()I

    move-result v6

    int-to-float v3, v6

    .line 896
    .local v3, "knobWidth":F
    const/high16 v6, 0x40000000    # 2.0f

    div-float v2, v3, v6

    .line 898
    .local v2, "knobHalfWidth":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getLeft()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v6, v2

    cmpg-float v6, v0, v6

    if-ltz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getRight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v2

    cmpl-float v6, v0, v6

    if-gtz v6, :cond_0

    .line 903
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLeft()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v6, v0, v6

    if-ltz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getRight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v0, v6

    if-gtz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getBottom()I

    move-result v6

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getBuyMargins()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    cmpg-float v6, v1, v6

    if-lez v6, :cond_0

    .line 914
    :cond_2
    iput-boolean v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z

    .line 915
    iput-object v8, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastReadChapterLabel:Ljava/lang/CharSequence;

    .line 916
    iput-object v8, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastReadPageLabel:Ljava/lang/CharSequence;

    .line 917
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventTime:J

    .line 918
    iput-object v8, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;

    .line 919
    iput-boolean v5, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mIsTrackingEvent:Z

    .line 920
    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventX:F

    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFirstEventX:F

    .line 923
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getX()F

    move-result v6

    cmpg-float v6, v0, v6

    if-ltz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getX()F

    move-result v6

    add-float/2addr v6, v3

    cmpl-float v6, v0, v6

    if-lez v6, :cond_4

    :cond_3
    move v4, v5

    :cond_4
    iput-boolean v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartedMoving:Z

    .line 924
    iget-boolean v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartedMoving:Z

    if-eqz v4, :cond_5

    .line 925
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->incPositionUpdateSequenceId()V

    .line 926
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->dispatchScrubStarted()V

    .line 928
    :cond_5
    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/widget/ScrubBar;->setPressed(Z)V

    .line 929
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    goto/16 :goto_0
.end method

.method private trackTouchEvent(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 980
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 982
    .local v4, "touchX":F
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/ScrubBar;->computeScrubberIndex(F)I

    move-result v3

    .line 983
    .local v3, "itemPosition":I
    const-string v6, "ScrubBar"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 984
    const-string v6, "ScrubBar"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "itemPosition: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " started "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartedMoving:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    :cond_0
    iget-boolean v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartedMoving:Z

    if-nez v6, :cond_2

    .line 988
    iget v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFirstEventX:F

    sub-float v6, v4, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mMovementThreshold:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    .line 1036
    :goto_0
    return-void

    .line 991
    :cond_1
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartedMoving:Z

    .line 992
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->incPositionUpdateSequenceId()V

    .line 993
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->dispatchScrubStarted()V

    .line 999
    :cond_2
    iget-wide v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventTime:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    .line 1002
    const/4 v2, 0x0

    .line 1019
    .local v2, "isFastMovement":Z
    :goto_1
    iput v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventX:F

    .line 1020
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventTime:J

    .line 1022
    if-eqz v2, :cond_9

    .line 1023
    const-string v6, "ScrubBar"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1024
    const-string v6, "ScrubBar"

    const-string v7, "fast movement"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    :cond_3
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->setPosition(I)V

    .line 1027
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->dispatchScrubContinues(I)V

    .line 1028
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQueuedPosition:Ljava/lang/Integer;

    .line 1029
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mFreezePositionInProgress:Z

    goto :goto_0

    .line 1004
    .end local v2    # "isFastMovement":Z
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventTime:J

    sub-long v0, v6, v8

    .line 1005
    .local v0, "deltaTime":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-nez v6, :cond_6

    .line 1006
    const-string v6, "ScrubBar"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1007
    const-string v6, "ScrubBar"

    const-string v7, "maybe a duplicate event? count as slow..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "isFastMovement":Z
    goto :goto_1

    .line 1011
    .end local v2    # "isFastMovement":Z
    :cond_6
    iget v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mLastEventX:F

    sub-float v6, v4, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    long-to-float v7, v0

    div-float/2addr v6, v7

    iget v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mDisplayDensity:F

    div-float v5, v6, v7

    .line 1013
    .local v5, "xVelocity":F
    const-string v6, "ScrubBar"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1014
    const-string v6, "ScrubBar"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "x velocity is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    :cond_7
    iget v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mVelocityThreshold:F

    cmpl-float v6, v5, v6

    if-lez v6, :cond_8

    const/4 v2, 0x1

    .restart local v2    # "isFastMovement":Z
    :goto_2
    goto :goto_1

    .end local v2    # "isFastMovement":Z
    :cond_8
    const/4 v2, 0x0

    goto :goto_2

    .line 1031
    .end local v0    # "deltaTime":J
    .end local v5    # "xVelocity":F
    .restart local v2    # "isFastMovement":Z
    :cond_9
    const-string v6, "ScrubBar"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1032
    const-string v6, "ScrubBar"

    const-string v7, "slow movement"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    :cond_a
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->maybeSetPositionDelayed(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 1095
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailRenderConsumer:Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;

    .line 1096
    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 1054
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1058
    const/4 v0, 0x1

    .line 1060
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBuyButton()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    return-object v0
.end method

.method public getNewThumbnailRenderConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .locals 2

    .prologue
    .line 1158
    new-instance v0, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;Lcom/google/android/apps/books/widget/ScrubBar$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mThumbnailRenderConsumer:Lcom/google/android/apps/books/widget/ScrubBar$ThumbnailRenderConsumer;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 415
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    return v0
.end method

.method public isScrubbing()Z
    .locals 1

    .prologue
    .line 1150
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mIsTrackingEvent:Z

    return v0
.end method

.method public onFinishInflate()V
    .locals 9

    .prologue
    .line 349
    const v7, 0x7f0e01b1

    invoke-virtual {p0, v7}, Lcom/google/android/apps/books/widget/ScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/widget/ScrubTrackView;

    iput-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    .line 350
    const v7, 0x7f0e01b4

    invoke-virtual {p0, v7}, Lcom/google/android/apps/books/widget/ScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    .line 351
    const v7, 0x7f0e01b3

    invoke-virtual {p0, v7}, Lcom/google/android/apps/books/widget/ScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mSosImageView:Landroid/widget/ImageView;

    .line 352
    const v7, 0x7f0e01d6

    invoke-virtual {p0, v7}, Lcom/google/android/apps/books/widget/ScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    .line 354
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 355
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f0e01b2

    invoke-virtual {p0, v7}, Lcom/google/android/apps/books/widget/ScrubBar;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 356
    .local v6, "sosTargetView":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkimClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f0200

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "contentDescription":Ljava/lang/String;
    new-instance v7, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    invoke-static {p0, v4}, Lcom/google/android/apps/books/widget/ScrubBar;->makePositionDot(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v8

    invoke-direct {v7, v6, v8, v2}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;-><init>(Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .local v0, "arr$":[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v0, v3

    .line 363
    .local v1, "bookmark":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    iget-object v7, v1, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->bringToFront()V

    .line 364
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAllPointsOfInterest:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 366
    .end local v1    # "bookmark":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v7, v7, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->bringToFront()V

    .line 367
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAllPointsOfInterest:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 369
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->bringToFront()V

    .line 370
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 52
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 700
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getMeasuredWidth()I

    move-result v51

    .line 701
    .local v51, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getMeasuredHeight()I

    move-result v30

    .line 703
    .local v30, "height":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v45

    check-cast v45, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 706
    .local v45, "trackParams":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getMeasuredHeight()I

    move-result v43

    .line 709
    .local v43, "trackHeight":I
    move-object/from16 v0, v45

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v44, v0

    .line 710
    .local v44, "trackLeft":I
    move-object/from16 v0, v45

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v46, v51, v4

    .line 711
    .local v46, "trackRight":I
    sub-int v49, v46, v44

    .line 712
    .local v49, "trackWidth":I
    move/from16 v42, v30

    .line 713
    .local v42, "trackBottom":I
    sub-int v47, v42, v43

    .line 714
    .local v47, "trackTop":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    move/from16 v0, v44

    move/from16 v1, v47

    move/from16 v2, v46

    move/from16 v3, v42

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/ScrubTrackView;->layout(IIII)V

    .line 716
    add-int v4, v47, v30

    div-int/lit8 v48, v4, 0x2

    .line 718
    .local v48, "trackVerticalCenter":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    .line 721
    .local v37, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 722
    .local v7, "bookmarkIconWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    .line 724
    .local v22, "bookmarkIconHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v23

    .line 726
    .local v23, "bookmarkPositionHeight":I
    sub-int v8, v47, v22

    .line 727
    .local v8, "bookmarkIconTop":I
    div-int/lit8 v4, v23, 0x2

    sub-int v13, v48, v4

    .line 729
    .local v13, "bookmarkPositionTop":I
    new-instance v40, Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    move-object/from16 v0, v40

    move/from16 v1, v44

    move/from16 v2, v46

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;-><init>(II)V

    .line 732
    .local v40, "segmentResolver":Lcom/google/android/apps/books/widget/SegmentPositionResolver;
    const v4, 0x7f090134

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 735
    .local v12, "ringSpacing":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-eqz v4, :cond_2

    const/16 v41, 0x1

    .line 739
    .local v41, "showBuyButton":Z
    :goto_0
    if-eqz v41, :cond_0

    .line 740
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getBuyMargins()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v26

    .line 741
    .local v26, "buyMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v29

    .line 742
    .local v29, "buyWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v24

    .line 743
    .local v24, "buyHeight":I
    move-object/from16 v0, v26

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v5, v47, v24

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v28

    .line 745
    .local v28, "buyTop":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->isRightToLeft()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 746
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    add-int v5, v44, v29

    move/from16 v0, v44

    move/from16 v1, v28

    move/from16 v2, v47

    invoke-virtual {v4, v0, v1, v5, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 752
    .end local v24    # "buyHeight":I
    .end local v26    # "buyMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v28    # "buyTop":I
    .end local v29    # "buyWidth":I
    :cond_0
    :goto_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v32

    .line 753
    .local v32, "iconSegments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v36

    .line 755
    .local v36, "positionSegments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    move-object/from16 v20, v0

    .local v20, "arr$":[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v34, v0

    .local v34, "len$":I
    const/16 v31, 0x0

    .local v31, "i$":I
    :goto_2
    move/from16 v0, v31

    move/from16 v1, v34

    if-ge v0, v1, :cond_5

    aget-object v21, v20, v31

    .line 757
    .local v21, "bookmark":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    move-object/from16 v0, v21

    iget v0, v0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    move/from16 v38, v0

    .line 758
    .local v38, "scrubberIndex":I
    const/4 v4, -0x1

    move/from16 v0, v38

    if-eq v0, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    move/from16 v0, v38

    if-ne v0, v4, :cond_4

    .line 759
    :cond_1
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->hideViews()V

    .line 755
    :goto_3
    add-int/lit8 v31, v31, 0x1

    goto :goto_2

    .line 735
    .end local v20    # "arr$":[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    .end local v21    # "bookmark":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    .end local v31    # "i$":I
    .end local v32    # "iconSegments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    .end local v34    # "len$":I
    .end local v36    # "positionSegments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    .end local v38    # "scrubberIndex":I
    .end local v41    # "showBuyButton":Z
    :cond_2
    const/16 v41, 0x0

    goto :goto_0

    .line 748
    .restart local v24    # "buyHeight":I
    .restart local v26    # "buyMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v28    # "buyTop":I
    .restart local v29    # "buyWidth":I
    .restart local v41    # "showBuyButton":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    sub-int v5, v46, v29

    move/from16 v0, v28

    move/from16 v1, v46

    move/from16 v2, v47

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_1

    .line 761
    .end local v24    # "buyHeight":I
    .end local v26    # "buyMargins":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v28    # "buyTop":I
    .end local v29    # "buyWidth":I
    .restart local v20    # "arr$":[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    .restart local v21    # "bookmark":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    .restart local v31    # "i$":I
    .restart local v32    # "iconSegments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    .restart local v34    # "len$":I
    .restart local v36    # "positionSegments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;>;"
    .restart local v38    # "scrubberIndex":I
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, v38

    move/from16 v2, v44

    move/from16 v3, v49

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->computePosition(III)I

    move-result v6

    .line 762
    .local v6, "center":I
    new-instance v4, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    move-object/from16 v0, v21

    iget-object v9, v0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;IIILandroid/view/View;)V

    move-object/from16 v0, v32

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 764
    new-instance v9, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    move-object/from16 v0, v21

    iget-object v14, v0, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    move-object/from16 v10, p0

    move v11, v6

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;IIILandroid/view/View;)V

    move-object/from16 v0, v36

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 769
    .end local v6    # "center":I
    .end local v21    # "bookmark":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    .end local v38    # "scrubberIndex":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    # invokes: Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->hasValidLocation()Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->access$100(Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 770
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    move-object/from16 v0, p0

    move/from16 v1, v44

    move/from16 v2, v49

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/books/widget/ScrubBar;->computePosition(III)I

    move-result v16

    .line 773
    .local v16, "sosCenterX":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mSosImageView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-nez v4, :cond_8

    .line 774
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 783
    :goto_4
    new-instance v9, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v14, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->positionView:Landroid/view/View;

    move-object/from16 v10, p0

    move/from16 v11, v16

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;IIILandroid/view/View;)V

    move-object/from16 v0, v36

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 787
    .end local v16    # "sosCenterX":I
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/widget/ScrubBar;->resolveAndLayout(Lcom/google/android/apps/books/widget/SegmentPositionResolver;Ljava/util/List;)V

    .line 788
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/widget/ScrubBar;->resolveAndLayout(Lcom/google/android/apps/books/widget/SegmentPositionResolver;Ljava/util/List;)V

    .line 796
    if-eqz v41, :cond_9

    .line 797
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getBuyMargins()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v35

    .line 798
    .local v35, "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLeft()I

    move-result v4

    move-object/from16 v0, v35

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v25, v4, v5

    .line 799
    .local v25, "buyLeft":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mBuyButton:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getRight()I

    move-result v4

    move-object/from16 v0, v35

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v27, v4, v5

    .line 801
    .local v27, "buyRight":I
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v31

    .local v31, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_5
    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface/range {v31 .. v31}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    .line 802
    .local v39, "segment":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;
    # getter for: Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->mView:Landroid/view/View;
    invoke-static/range {v39 .. v39}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;->access$700(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;)Landroid/view/View;

    move-result-object v50

    .line 804
    .local v50, "view":Landroid/view/View;
    invoke-virtual/range {v50 .. v50}, Landroid/view/View;->getLeft()I

    move-result v4

    move/from16 v0, v27

    if-ge v4, v0, :cond_7

    invoke-virtual/range {v50 .. v50}, Landroid/view/View;->getRight()I

    move-result v4

    move/from16 v0, v25

    if-ge v0, v4, :cond_7

    .line 805
    const/4 v4, 0x4

    move-object/from16 v0, v50

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 777
    .end local v25    # "buyLeft":I
    .end local v27    # "buyRight":I
    .end local v35    # "margins":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v39    # "segment":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;
    .end local v50    # "view":Landroid/view/View;
    .restart local v16    # "sosCenterX":I
    .local v31, "i$":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v18, v47, v4

    .line 779
    .local v18, "sosTargetTop":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    .line 780
    .local v17, "measuredWidth":I
    new-instance v14, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v0, v4, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->iconView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v15, p0

    invoke-direct/range {v14 .. v19}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberSegment;-><init>(Lcom/google/android/apps/books/widget/ScrubBar;IIILandroid/view/View;)V

    move-object/from16 v0, v32

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 810
    .end local v16    # "sosCenterX":I
    .end local v17    # "measuredWidth":I
    .end local v18    # "sosTargetTop":I
    .end local v31    # "i$":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v33, v48, v4

    .line 811
    .local v33, "knobTop":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-direct {v0, v4, v5, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->layoutChild(Landroid/view/View;II)V

    .line 812
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-eqz v4, :cond_b

    const/4 v4, 0x0

    :goto_6
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 815
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/books/widget/ScrubBar;->mAnimatingPosition:Z

    if-nez v4, :cond_a

    .line 816
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ScrubBar;->positionKnob()V

    .line 819
    :cond_a
    return-void

    .line 812
    :cond_b
    const/4 v4, 0x4

    goto :goto_6
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 674
    .local v3, "parent":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 675
    .local v2, "idealWidth":I
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/ScrubBar;->measureChildren(II)V

    .line 678
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/ScrubTrackView;->getMeasuredHeight()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->getThumbnailMaxSizeInPixels()Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mExtraBorderHeight:I

    add-int v1, v5, v6

    .line 681
    .local v1, "idealHeight":I
    invoke-static {v2, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->resolveSize(II)I

    move-result v4

    .line 682
    .local v4, "width":I
    invoke-static {v1, p2}, Lcom/google/android/apps/books/widget/ScrubBar;->resolveSize(II)I

    move-result v0

    .line 683
    .local v0, "height":I
    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/books/widget/ScrubBar;->setMeasuredDimension(II)V

    .line 684
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 935
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-nez v2, :cond_0

    .line 965
    :goto_0
    return v0

    .line 940
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v1

    .line 965
    goto :goto_0

    .line 942
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->startTrackingTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 944
    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mIsTrackingEvent:Z

    if-nez v2, :cond_2

    .line 946
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->startTrackingTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 948
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    .line 950
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->attemptClaimDrag()V

    goto :goto_1

    .line 953
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartedMoving:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mIsTrackingEvent:Z

    if-eqz v2, :cond_3

    move v0, v1

    .line 954
    .local v0, "scrubActive":Z
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->finishTrackingTouchEvent()V

    .line 955
    if-eqz v0, :cond_1

    .line 956
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;->SCRUBBER_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logScrubberAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$ScrubberAction;)V

    .line 957
    iget v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/ScrubBar;->dispatchScrubFinished(I)V

    .line 958
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->readAloudCurrentPositionIfAccessibilityEnabled()I

    goto :goto_1

    .line 962
    .end local v0    # "scrubActive":Z
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->finishTrackingTouchEvent()V

    goto :goto_1

    .line 940
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setBookmarkItems([I)V
    .locals 5
    .param p1, "itemIndices"    # [I

    .prologue
    .line 1070
    const/4 v1, 0x0

    .line 1071
    .local v1, "itemsChanged":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1072
    array-length v3, p1

    if-ge v0, v3, :cond_1

    aget v2, p1, v0

    .line 1073
    .local v2, "newValue":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->scrubberIndex:I

    if-eq v3, v2, :cond_0

    .line 1074
    const/4 v1, 0x1

    .line 1075
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mQuickBookmarks:[Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->setScrubberIndex(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;I)V

    .line 1071
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1072
    .end local v2    # "newValue":I
    :cond_1
    const/4 v2, -0x1

    goto :goto_1

    .line 1078
    :cond_2
    if-eqz v1, :cond_3

    .line 1079
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->requestLayout()V

    .line 1081
    :cond_3
    return-void
.end method

.method public setOnScrubListener(Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    .line 412
    return-void
.end method

.method public setPosition(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    const/4 v2, 0x0

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    if-nez v0, :cond_1

    .line 438
    const-string v0, "ScrubBar"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    const-string v0, "ScrubBar"

    const-string v1, "position set before index"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 445
    :cond_1
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;->getMaxViewableIndex()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 447
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    if-eq v0, p1, :cond_0

    .line 452
    iput p1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    .line 453
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAnimatingPosition:Z

    .line 455
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/ScrubTrackView;->setPosition(I)V

    .line 457
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->positionKnob()V

    goto :goto_0
.end method

.method public setPositionAnimationProgress(IF)V
    .locals 5
    .param p1, "newPosition"    # I
    .param p2, "progress"    # F

    .prologue
    .line 862
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->getKnobX(I)I

    move-result v0

    .line 863
    .local v0, "endX":I
    iget v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mItemPosition:I

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/ScrubBar;->getKnobX(I)I

    move-result v1

    .line 865
    .local v1, "startX":I
    int-to-float v3, v1

    sub-int v4, v0, v1

    int-to-float v4, v4

    mul-float/2addr v4, p2

    add-float v2, v3, v4

    .line 868
    .local v2, "x":F
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    sub-float v3, v2, v3

    sub-int v4, v0, v1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 869
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mKnobView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setX(F)V

    .line 871
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAnimatingPosition:Z

    .line 872
    return-void
.end method

.method public setScrubIndex(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "metadata":Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;, "Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata<*>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    .line 397
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mAllPointsOfInterest:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    .line 399
    .local v1, "pointOfInterest":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->adjustContentDescription(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V

    goto :goto_0

    .line 402
    .end local v1    # "pointOfInterest":Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/widget/ScrubTrackView;->setMetadata(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V

    .line 403
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ScrubTrackView;->requestLayout()V

    .line 404
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mTrackView:Lcom/google/android/apps/books/widget/ScrubTrackView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ScrubTrackView;->invalidate()V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->requestLayout()V

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBar;->invalidate()V

    .line 408
    return-void
.end method

.method public setStartOfSkimPosition(I)V
    .locals 2
    .param p1, "itemIndex"    # I

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mStartOfSkim:Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBar;->mScrubberMetadata:Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/books/widget/ScrubBar$PointOfInterest;->setScrubberIndex(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;I)V

    .line 423
    return-void
.end method
