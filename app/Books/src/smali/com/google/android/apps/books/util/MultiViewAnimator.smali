.class public Lcom/google/android/apps/books/util/MultiViewAnimator;
.super Ljava/lang/Object;
.source "MultiViewAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/MultiViewAnimator$1;,
        Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;,
        Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    }
.end annotation


# instance fields
.field private final mInitialState:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator;->mInitialState:Ljava/util/Map;

    .line 63
    return-void
.end method

.method private addUpdateListeners(Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;
    .locals 7
    .param p1, "animator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 144
    iget-object v3, p0, Lcom/google/android/apps/books/util/MultiViewAnimator;->mInitialState:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 145
    .local v1, "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 146
    .local v2, "view":Landroid/view/View;
    new-instance v4, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    new-instance v5, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    const/4 v6, 0x0

    invoke-direct {v5, v2, v6}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;-><init>(Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$1;)V

    invoke-direct {v4, v2, v3, v5}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewPropertyInterpolator;-><init>(Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;)V

    invoke-virtual {p1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_0

    .line 149
    .end local v1    # "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;>;"
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-object p1
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator;->mInitialState:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 187
    return-void
.end method

.method public getSavedViewState(Landroid/view/View;)Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;->getSavedViewState(Landroid/view/View;Z)Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    move-result-object v0

    return-object v0
.end method

.method public getSavedViewState(Landroid/view/View;Z)Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "toUpdate"    # Z

    .prologue
    const/4 v2, 0x0

    .line 122
    if-eqz p2, :cond_1

    move-object v0, v2

    .line 123
    .local v0, "state":Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    :goto_0
    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .end local v0    # "state":Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    invoke-direct {v0, p1, v2}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;-><init>(Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$1;)V

    .line 125
    .restart local v0    # "state":Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    iget-object v1, p0, Lcom/google/android/apps/books/util/MultiViewAnimator;->mInitialState:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    :cond_0
    return-object v0

    .line 122
    .end local v0    # "state":Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/util/MultiViewAnimator;->mInitialState:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    move-object v0, v1

    goto :goto_0
.end method

.method public getSnapbackAnimator()Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;->addUpdateListeners(Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public updateInitialStates()V
    .locals 5

    .prologue
    .line 138
    iget-object v2, p0, Lcom/google/android/apps/books/util/MultiViewAnimator;->mInitialState:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 139
    .local v1, "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;>;"
    new-instance v3, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;-><init>(Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$1;)V

    invoke-interface {v1, v3}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 141
    .end local v1    # "mapEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;>;"
    :cond_0
    return-void
.end method
