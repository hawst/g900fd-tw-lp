.class public Lcom/google/android/apps/books/sync/JsonCcBox;
.super Ljava/lang/Object;
.source "JsonCcBox.java"

# interfaces
.implements Lcom/google/android/apps/books/model/CcBox;


# instance fields
.field private final mCcbox:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "ccbox"    # Lorg/json/JSONObject;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/sync/JsonCcBox;->mCcbox:Lorg/json/JSONObject;

    .line 17
    return-void
.end method

.method private getInt(Ljava/lang/String;)I
    .locals 4
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/sync/JsonCcBox;->mCcbox:Lorg/json/JSONObject;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 46
    :goto_0
    return v1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "JsonCcBox"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const-string v1, "JsonCcBox"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t retrieve "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getH()I
    .locals 1

    .prologue
    .line 36
    const-string v0, "H"

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/JsonCcBox;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getW()I
    .locals 1

    .prologue
    .line 31
    const-string v0, "W"

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/JsonCcBox;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 21
    const-string v0, "X"

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/JsonCcBox;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 26
    const-string v0, "Y"

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/JsonCcBox;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
