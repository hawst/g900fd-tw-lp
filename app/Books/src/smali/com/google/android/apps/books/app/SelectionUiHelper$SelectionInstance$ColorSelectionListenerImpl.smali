.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ColorSelectionListenerImpl"
.end annotation


# instance fields
.field private final mRemoveHighlightButton:Landroid/view/View;

.field final synthetic this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/view/View;)V
    .locals 0
    .param p2, "removeHighlightButton"    # Landroid/view/View;

    .prologue
    .line 1232
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233
    iput-object p2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;

    .line 1234
    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public colorSelected(I)V
    .locals 10
    .param p1, "colorIndex"    # I

    .prologue
    const/4 v8, 0x0

    .line 1238
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->highlightSelectionFromPrefColorId(IZ)V

    .line 1239
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1240
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 1302
    :goto_0
    return-void

    .line 1244
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$300(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->getShowRequestId()I

    move-result v3

    .line 1245
    .local v3, "reqId":I
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1248
    new-instance v6, Landroid/animation/LayoutTransition;

    invoke-direct {v6}, Landroid/animation/LayoutTransition;-><init>()V

    .line 1249
    .local v6, "lt":Landroid/animation/LayoutTransition;
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1252
    .local v2, "removeButtonParent":Landroid/view/ViewGroup;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1253
    .local v4, "startTime":J
    new-instance v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$1;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;Landroid/view/ViewGroup;IJ)V

    invoke-virtual {v6, v0}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 1283
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1286
    .end local v2    # "removeButtonParent":Landroid/view/ViewGroup;
    .end local v4    # "startTime":J
    .end local v6    # "lt":Landroid/animation/LayoutTransition;
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1287
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getPopup(Z)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v7

    .line 1288
    .local v7, "popup":Lcom/google/android/apps/books/widget/SelectionPopup;
    if-eqz v7, :cond_2

    .line 1289
    invoke-interface {v7}, Lcom/google/android/apps/books/widget/SelectionPopup;->layout()V

    .line 1292
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;->mRemoveHighlightButton:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$2;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl$2;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;I)V

    const-wide/16 v8, 0x5dc

    invoke-virtual {v0, v1, v8, v9}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
