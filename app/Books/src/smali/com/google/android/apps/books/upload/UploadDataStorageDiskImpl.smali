.class public Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;
.super Ljava/lang/Object;
.source "UploadDataStorageDiskImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/upload/UploadDataStorage;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mUploadsDataFile:Ljava/io/File;

.field private final mUploadsDirectory:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;Landroid/content/Context;)V
    .locals 1
    .param p1, "uploadsDataFile"    # Ljava/io/File;
    .param p2, "uploadsDirectory"    # Ljava/io/File;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDataFile:Ljava/io/File;

    .line 35
    iput-object p2, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDirectory:Ljava/io/File;

    .line 36
    iput-object p3, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mContext:Landroid/content/Context;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDataFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 40
    return-void
.end method


# virtual methods
.method public copyToTempDir(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p1, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "originalFileName"    # Ljava/lang/String;
    .param p3, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
        }
    .end annotation

    .prologue
    .line 92
    const/high16 v7, 0x100000

    iget-object v8, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->getMaxUploadSizeMB(Landroid/content/Context;)I

    move-result v8

    mul-int v4, v7, v8

    .line 94
    .local v4, "maxFileSizeBytes":I
    const/4 v5, 0x0

    .line 95
    .local v5, "outputStream":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 97
    .local v1, "fileToSave":Ljava/io/File;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 99
    .local v3, "inputStream":Ljava/io/FileInputStream;
    new-instance v2, Ljava/io/File;

    iget-object v7, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDirectory:Ljava/io/File;

    invoke-direct {v2, v7, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    .end local v1    # "fileToSave":Ljava/io/File;
    .local v2, "fileToSave":Ljava/io/File;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 102
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .local v6, "outputStream":Ljava/io/FileOutputStream;
    int-to-long v8, v4

    :try_start_2
    invoke-static {v3, v6, v8, v9}, Lcom/google/android/apps/books/util/IOUtils;->copyWithLimit(Ljava/io/InputStream;Ljava/io/OutputStream;J)J
    :try_end_2
    .catch Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    long-to-int v7, v8

    .line 109
    if-eqz v6, :cond_0

    .line 110
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 112
    :cond_0
    if-eqz p1, :cond_1

    .line 113
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_1
    return v7

    .line 103
    .end local v2    # "fileToSave":Ljava/io/File;
    .end local v3    # "inputStream":Ljava/io/FileInputStream;
    .end local v6    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v1    # "fileToSave":Ljava/io/File;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
    :goto_0
    if-eqz v1, :cond_2

    .line 105
    :try_start_3
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 107
    :cond_2
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 109
    .end local v0    # "e":Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
    :catchall_0
    move-exception v7

    :goto_1
    if-eqz v5, :cond_3

    .line 110
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 112
    :cond_3
    if-eqz p1, :cond_4

    .line 113
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_4
    throw v7

    .line 109
    .end local v1    # "fileToSave":Ljava/io/File;
    .restart local v2    # "fileToSave":Ljava/io/File;
    .restart local v3    # "inputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "fileToSave":Ljava/io/File;
    .restart local v1    # "fileToSave":Ljava/io/File;
    goto :goto_1

    .end local v1    # "fileToSave":Ljava/io/File;
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "fileToSave":Ljava/io/File;
    .restart local v6    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v7

    move-object v1, v2

    .end local v2    # "fileToSave":Ljava/io/File;
    .restart local v1    # "fileToSave":Ljava/io/File;
    move-object v5, v6

    .end local v6    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 103
    .end local v1    # "fileToSave":Ljava/io/File;
    .restart local v2    # "fileToSave":Ljava/io/File;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileToSave":Ljava/io/File;
    .restart local v1    # "fileToSave":Ljava/io/File;
    goto :goto_0

    .end local v1    # "fileToSave":Ljava/io/File;
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v2    # "fileToSave":Ljava/io/File;
    .restart local v6    # "outputStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileToSave":Ljava/io/File;
    .restart local v1    # "fileToSave":Ljava/io/File;
    move-object v5, v6

    .end local v6    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 120
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDirectory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 121
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 122
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 132
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDirectory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 133
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public get()Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 8

    .prologue
    .line 45
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v6, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDataFile:Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 46
    .local v2, "is":Ljava/io/InputStream;
    invoke-static {v2}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v5

    .line 47
    .local v5, "uploadsList":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    new-instance v4, Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-direct {v4}, Lcom/google/android/apps/books/upload/Upload$Uploads;-><init>()V

    .line 48
    .local v4, "uploads":Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-virtual {v5}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->getUploadList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 49
    .local v3, "uploadProto":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    new-instance v6, Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v6, v3}, Lcom/google/android/apps/books/upload/Upload;-><init>(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)V

    invoke-virtual {v4, v6}, Lcom/google/android/apps/books/upload/Upload$Uploads;->put(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 53
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "uploadProto":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    .end local v4    # "uploads":Lcom/google/android/apps/books/upload/Upload$Uploads;
    .end local v5    # "uploadsList":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v6, "UploadDataStore"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 55
    const-string v6, "UploadDataStore"

    const-string v7, "Uploads data file not found"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_1
    new-instance v4, Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-direct {v4}, Lcom/google/android/apps/books/upload/Upload$Uploads;-><init>()V

    :cond_1
    return-object v4

    .line 57
    :catch_1
    move-exception v0

    .line 59
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "UploadDataStore"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 60
    const-string v6, "UploadDataStore"

    const-string v7, "IO error reading uploads data from file"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getFileInputStream(Ljava/lang/String;)Ljava/io/FileInputStream;
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 126
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDirectory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 127
    .local v0, "file":Ljava/io/File;
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    return-object v1
.end method

.method public set(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
    .locals 7
    .param p1, "uploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    const/4 v6, 0x6

    .line 69
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/google/android/apps/books/upload/UploadDataStorageDiskImpl;->mUploadsDataFile:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 70
    .local v4, "os":Ljava/io/OutputStream;
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->newBuilder()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v3

    .line 71
    .local v3, "listBuilder":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    invoke-virtual {p1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 72
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v5}, Lcom/google/android/apps/books/upload/Upload;->toUploadProto()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->addUpload(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 77
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "listBuilder":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .end local v4    # "os":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v5, "UploadDataStore"

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 79
    const-string v5, "UploadDataStore"

    const-string v6, "File not found error saving uploads data"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_1
    return-void

    .line 74
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "listBuilder":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .restart local v4    # "os":Ljava/io/OutputStream;
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->build()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->writeTo(Ljava/io/OutputStream;)V

    .line 76
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 81
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "listBuilder":Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .end local v4    # "os":Ljava/io/OutputStream;
    :catch_1
    move-exception v0

    .line 83
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "UploadDataStore"

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 84
    const-string v5, "UploadDataStore"

    const-string v6, "IO error saving uploads data"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
