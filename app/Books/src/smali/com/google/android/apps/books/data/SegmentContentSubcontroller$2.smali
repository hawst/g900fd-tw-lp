.class Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;
.super Ljava/lang/Object;
.source "SegmentContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$ignoreResources:Z

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$segment:Lcom/google/android/apps/books/model/Segment;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p4, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iput-object p5, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iput-boolean p6, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$ignoreResources:Z

    iput-object p7, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$100(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 209
    :goto_0
    return-void

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v0}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$200(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v6

    .line 198
    .local v6, "haveContentConsumers":Z
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 206
    .local v7, "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$segment:Lcom/google/android/apps/books/model/Segment;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v5, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    iget-boolean v8, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$ignoreResources:Z

    iget-object v9, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$300(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_0

    .line 200
    .end local v7    # "saver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    :catch_0
    move-exception v10

    .line 201
    .local v10, "e":Ljava/io/IOException;
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v10}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->access$100(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 185
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
