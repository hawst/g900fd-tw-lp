.class Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;
.super Lcom/google/android/apps/books/provider/NotifyingProvidelet;
.source "LocalCollectionsProvidelet.java"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V
    .locals 0
    .param p1, "notifier"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
    .param p2, "database"    # Lcom/google/android/apps/books/provider/database/BooksDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;",
            "Lcom/google/android/apps/books/provider/database/BooksDatabase;",
            "Lcom/google/android/apps/books/util/pool/Pool",
            "<",
            "Lcom/google/android/apps/books/util/SelectionBuilder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p3, "builderPool":Lcom/google/android/apps/books/util/pool/Pool;, "Lcom/google/android/apps/books/util/pool/Pool<Lcom/google/android/apps/books/util/SelectionBuilder;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    .line 26
    return-void
.end method

.method private augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)Lcom/google/android/apps/books/util/SelectionBuilder;
    .locals 6
    .param p1, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 71
    packed-switch p2, :pswitch_data_0

    .line 84
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bad match "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for URI "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 75
    :pswitch_0
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->restrictAccount(Lcom/google/android/apps/books/util/SelectionBuilder;Landroid/net/Uri;)V

    .line 87
    :goto_0
    :pswitch_1
    return-object p1

    .line 78
    :pswitch_2
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->restrictAccount(Lcom/google/android/apps/books/util/SelectionBuilder;Landroid/net/Uri;)V

    .line 79
    invoke-static {p3}, Lcom/google/android/apps/books/provider/BooksContract$Collections;->getCollectionId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 80
    .local v0, "collectionId":J
    const-string v2, "collection_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x2bc
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private restrictAccount(Lcom/google/android/apps/books/util/SelectionBuilder;Landroid/net/Uri;)V
    .locals 4
    .param p1, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 91
    invoke-static {p2}, Lcom/google/android/apps/books/provider/BooksContract$Collections;->getAccountName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "account":Ljava/lang/String;
    const-string v1, "account_name=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 93
    return-void
.end method


# virtual methods
.method public deleteWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 58
    invoke-direct {p0, p4, p2, p3}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 59
    const-string v0, "collections"

    invoke-virtual {p4, v0}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/SelectionBuilder;->delete(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    return v0
.end method

.method public insertWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 37
    const/16 v1, 0x2bc

    if-eq p2, v1, :cond_0

    .line 38
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad match "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for URI "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 41
    :cond_0
    const-string v1, "account_name"

    invoke-virtual {p4, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "accountName":Ljava/lang/String;
    const-string v1, "collection_id"

    invoke-virtual {p4, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 44
    .local v2, "collectionId":J
    const-string v1, "collections"

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v4, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 45
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$Collections;->itemUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public queryWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/util/SelectionBuilder;)Landroid/database/Cursor;
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 31
    invoke-direct {p0, p6, p2, p3}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 32
    const-string v0, "collections"

    invoke-virtual {p6, v0}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p4, p5}, Lcom/google/android/apps/books/util/SelectionBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public updateWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "values"    # Landroid/content/ContentValues;
    .param p5, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 51
    invoke-direct {p0, p5, p2, p3}, Lcom/google/android/apps/books/provider/LocalCollectionsProvidelet;->augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 52
    const-string v0, "collections"

    invoke-virtual {p5, v0}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p4}, Lcom/google/android/apps/books/util/SelectionBuilder;->update(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method
