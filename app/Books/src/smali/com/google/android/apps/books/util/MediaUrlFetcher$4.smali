.class Lcom/google/android/apps/books/util/MediaUrlFetcher$4;
.super Ljava/lang/Object;
.source "MediaUrlFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchFailed(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

.field final synthetic val$messageId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/MediaUrlFetcher;I)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    iput p2, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;->val$messageId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 197
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # getter for: Lcom/google/android/apps/books/util/MediaUrlFetcher;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$100(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;->val$messageId:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaUrlFetcher$4;->this$0:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    # getter for: Lcom/google/android/apps/books/util/MediaUrlFetcher;->mMediaFoundListener:Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;
    invoke-static {v0}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->access$600(Lcom/google/android/apps/books/util/MediaUrlFetcher;)Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;->onUrlFetchedError()V

    .line 200
    return-void
.end method
