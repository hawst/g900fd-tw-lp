.class public Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;
.super Ljava/lang/Object;
.source "SimpleContentFileManager.java"

# interfaces
.implements Lcom/google/android/apps/books/provider/database/ContentFileManager;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method


# virtual methods
.method public deleteAllFiles()V
    .locals 2

    .prologue
    .line 32
    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/util/StorageUtils;->getInternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z

    .line 34
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/util/StorageUtils;->getExternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 36
    .local v0, "externalStorageDirectory":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 37
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDelete(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .end local v0    # "externalStorageDirectory":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public migrateResourceFiles()V
    .locals 2

    .prologue
    .line 46
    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/util/StorageUtils;->getInternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/provider/database/ResourceFileSha1RenameMigrator;->execute(Ljava/io/File;)V

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/provider/database/SimpleContentFileManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/util/StorageUtils;->getExternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 50
    .local v0, "externalStorageDirectory":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 51
    invoke-static {v0}, Lcom/google/android/apps/books/provider/database/ResourceFileSha1RenameMigrator;->execute(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    .end local v0    # "externalStorageDirectory":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v1

    goto :goto_0
.end method
