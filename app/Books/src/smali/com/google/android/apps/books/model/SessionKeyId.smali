.class public interface abstract Lcom/google/android/apps/books/model/SessionKeyId;
.super Ljava/lang/Object;
.source "SessionKeyId.java"


# virtual methods
.method public abstract addToContentValues(Landroid/content/ContentValues;)V
.end method

.method public abstract deleteKeyAndEncryptedContent(Lcom/google/android/apps/books/model/BooksDataStore;)V
.end method

.method public abstract load(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/SessionKey;
.end method

.method public abstract update(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/model/BooksDataStore;)V
.end method
