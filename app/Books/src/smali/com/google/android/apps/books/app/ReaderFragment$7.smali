.class Lcom/google/android/apps/books/app/ReaderFragment$7;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 1397
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$7;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActionBarBottom()I
    .locals 2

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$7;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SystemBarManager;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$7;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/SystemBarManager;->getActionBarCurrentBottom(Landroid/support/v7/app/ActionBarActivity;)I

    move-result v0

    return v0
.end method

.method public onCardsLayoutHidden()V
    .locals 1

    .prologue
    .line 1411
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$7;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/InfoCardsHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->onCardsLayoutHidden()V

    .line 1412
    return-void
.end method

.method public onSystemWindowInsetsChanged(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$7;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SystemBarManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/SystemBarManager;->onSystemWindowInsetsChanged(Landroid/graphics/Rect;)V

    .line 1417
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$7;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->updateMainTouchAreaInsets(Landroid/graphics/Rect;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3000(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/graphics/Rect;)V

    .line 1418
    return-void
.end method
