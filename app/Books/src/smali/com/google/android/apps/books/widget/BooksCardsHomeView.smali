.class public abstract Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.super Lcom/google/android/apps/books/widget/BaseBooksHomeView;
.source "BooksCardsHomeView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$4;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$PinBookMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;,
        Lcom/google/android/apps/books/widget/BooksCardsHomeView$BookDocumentMenuHandler;
    }
.end annotation


# instance fields
.field protected mCardData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation
.end field

.field protected mCardsContainer:Landroid/view/ViewGroup;

.field private mCardsShowing:Z

.field protected mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private final mContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

.field private mCoverImageLoaded:Z

.field private final mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

.field private mImageManager:Lcom/google/android/apps/books/common/ImageManager;

.field private final mMyBooksHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

.field protected final mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

.field private mProgressView:Landroid/view/View;

.field protected mProgressViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private final mShowingCardsVisibilityChangedListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

.field private final mUploadMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;Z)V
    .locals 2
    .param p1, "controller"    # Lcom/google/android/apps/books/widget/BooksHomeController;
    .param p2, "cardsContainer"    # Landroid/view/ViewGroup;
    .param p3, "isDeviceConnected"    # Z

    .prologue
    const/4 v1, 0x0

    .line 259
    invoke-interface {p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/books/widget/BaseBooksHomeView;-><init>(Landroid/content/Context;Z)V

    .line 85
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsShowing:Z

    .line 87
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCoverImageLoaded:Z

    .line 95
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$1;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mShowingCardsVisibilityChangedListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    .line 206
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$2;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mMyBooksHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    .line 214
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mMyBooksHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 217
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadContextMenuDelegate;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mUploadMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    .line 219
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    .line 261
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    .line 262
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    .line 263
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsShowing:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/util/List;ZLcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/google/android/apps/books/playcards/BookDocument;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->populateBookMenu(Ljava/util/List;ZLcom/google/android/apps/books/playcards/BookDocument;)V

    return-void
.end method

.method private canShowCards()Z
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCoverImageLoaded:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->alreadyFetchedMyEbooks()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cardsAreShowing()Z
    .locals 1

    .prologue
    .line 519
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsShowing:Z

    return v0
.end method

.method private getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "ensurer"    # Lcom/google/android/apps/books/common/ImageManager$Ensurer;
    .param p4, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    if-nez v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v6

    .line 356
    .local v6, "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v6}, Lcom/google/android/apps/books/app/BooksApplication;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    .line 358
    .end local v6    # "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v0

    return-object v0
.end method

.method private hideCards(Z)V
    .locals 2
    .param p1, "animated"    # Z

    .prologue
    const/4 v1, 0x0

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-eqz v0, :cond_0

    .line 499
    if-eqz p1, :cond_1

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 505
    :cond_0
    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsShowing:Z

    .line 506
    return-void

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    goto :goto_0
.end method

.method private maybeShowCards(Z)Z
    .locals 3
    .param p1, "animated"    # Z

    .prologue
    const/4 v1, 0x1

    .line 481
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->cardsAreShowing()Z

    move-result v0

    .line 482
    .local v0, "cardsAreShowing":Z
    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->canShowCards()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 483
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->setCardsShown(ZZ)V

    .line 486
    if-nez p1, :cond_0

    .line 487
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsShowing:Z

    :cond_0
    move v0, v1

    .line 491
    .end local v0    # "cardsAreShowing":Z
    :cond_1
    return v0
.end method

.method private populateBookMenu(Ljava/util/List;ZLcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 14
    .param p2, "isRecommendation"    # Z
    .param p3, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;Z",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ")V"
        }
    .end annotation

    .prologue
    .line 710
    .local p1, "menuEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    .line 711
    .local v3, "volumeId":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 713
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 714
    .local v4, "context":Landroid/content/Context;
    new-instance v1, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;

    const-string v2, "books_inapp_home_submenu_buy_from_recommendation"

    move-object/from16 v0, p3

    invoke-direct {v1, v4, v2, v0}, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/books/playcards/BookDocument;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 716
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->NOT_INTERESTED:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 718
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;->HAVE_IT:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 749
    .end local v4    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 721
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v1, v3}, Lcom/google/android/apps/books/widget/BooksHomeController;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v13

    .line 722
    .local v13, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 723
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 725
    :cond_2
    const-string v1, "http://schemas.google.com/books/2008#view_no_pages"

    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 726
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v1, v3}, Lcom/google/android/apps/books/widget/BooksHomeController;->isPinned(Ljava/lang/String;)Z

    move-result v12

    .line 727
    .local v12, "pinned":Z
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v1, v3}, Lcom/google/android/apps/books/widget/BooksHomeController;->isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v9, 0x1

    .line 729
    .local v9, "isNotDownloadedAtAll":Z
    :goto_1
    if-nez v12, :cond_6

    if-nez v9, :cond_3

    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_3
    const/4 v10, 0x1

    .line 731
    .local v10, "menuItemSaysDownload":Z
    :goto_2
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->isVolumeAboutToBeDeleted(Ljava/lang/String;)Z

    move-result v8

    .line 732
    .local v8, "isAboutToBeDeleted":Z
    if-eqz v10, :cond_7

    const v11, 0x7f0f00c7

    .line 739
    .local v11, "menuItemStringId":I
    :goto_3
    invoke-static {v13}, Lcom/google/android/apps/books/model/VolumeDataUtils;->isOnlineOnly(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v7, 0x1

    .line 740
    .local v7, "enableDownloadMenuItem":Z
    :goto_4
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$PinBookMenuEntry;

    invoke-direct {v1, p0, v3, v11, v7}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$PinBookMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZ)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 744
    .end local v7    # "enableDownloadMenuItem":Z
    .end local v8    # "isAboutToBeDeleted":Z
    .end local v9    # "isNotDownloadedAtAll":Z
    .end local v10    # "menuItemSaysDownload":Z
    .end local v11    # "menuItemStringId":I
    .end local v12    # "pinned":Z
    :cond_4
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;

    invoke-direct {v1, p0, v13}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$RemoveBookMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/model/VolumeData;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 745
    invoke-interface {v13}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 746
    new-instance v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 727
    .restart local v12    # "pinned":Z
    :cond_5
    const/4 v9, 0x0

    goto :goto_1

    .line 729
    .restart local v9    # "isNotDownloadedAtAll":Z
    :cond_6
    const/4 v10, 0x0

    goto :goto_2

    .line 732
    .restart local v8    # "isAboutToBeDeleted":Z
    .restart local v10    # "menuItemSaysDownload":Z
    :cond_7
    if-eqz v8, :cond_8

    const v11, 0x7f0f00c9

    goto :goto_3

    :cond_8
    const v11, 0x7f0f00c8

    goto :goto_3

    .line 739
    .restart local v11    # "menuItemStringId":I
    :cond_9
    const/4 v7, 0x0

    goto :goto_4
.end method

.method private setCardsShown(ZZ)V
    .locals 5
    .param p1, "shown"    # Z
    .param p2, "animate"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 440
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->ensureCardsViews()V

    .line 442
    if-nez p1, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->maybeShowOrHideEmptyView()V

    .line 446
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    const v4, 0x7f0e0130

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 448
    .local v0, "progressBar":Landroid/widget/ProgressBar;
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 449
    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 452
    .end local v0    # "progressBar":Landroid/widget/ProgressBar;
    :cond_0
    if-eqz p2, :cond_2

    .line 453
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-nez p1, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    .line 454
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mShowingCardsVisibilityChangedListener:Lcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;

    invoke-virtual {v1, p1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZLcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    .line 460
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 453
    goto :goto_0

    .line 457
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-nez p1, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v1, p1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 457
    goto :goto_2
.end method

.method private showProgress()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 512
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->setCardsShown(ZZ)V

    .line 513
    return-void
.end method

.method private sortCardData(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 342
    .local p1, "cardsData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    const/4 v0, 0x0

    .line 343
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    if-eqz p1, :cond_0

    .line 345
    invoke-static {p1}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getSortComparator()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 347
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 349
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected ensureCardsViews()V
    .locals 3

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getContentView()Landroid/view/View;

    move-result-object v0

    .line 524
    .local v0, "contentView":Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-void

    .line 528
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    const v2, 0x7f0e012f

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressView:Landroid/view/View;

    .line 529
    new-instance v1, Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 531
    new-instance v1, Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-direct {v1, v0}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 532
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisibleNoAnim(Z)V

    goto :goto_0
.end method

.method public getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;
    .param p4, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v0, p4}, Lcom/google/android/apps/books/widget/BooksHomeController;->getCoverLoadEnsurer(Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getBookCoverImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v0

    return-object v0
.end method

.method public abstract getContentView()Landroid/view/View;
.end method

.method protected getEmptyLibraryView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "maximizeHeight"    # Z

    .prologue
    .line 757
    const v4, 0x7f040069

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 758
    .local v1, "emptyView":Landroid/view/View;
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 759
    .local v2, "fullscreenMetrics":Landroid/util/DisplayMetrics;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContext:Landroid/content/Context;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 760
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 763
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    mul-int/lit8 v0, v4, 0x3

    .line 765
    .local v0, "bgHeight":I
    if-eqz p3, :cond_0

    .line 768
    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v4, v0

    invoke-virtual {v1, v4}, Landroid/view/View;->setMinimumHeight(I)V

    .line 771
    :cond_0
    return-object v1
.end method

.method protected getFilteredCardData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getLibraryFilter()Lcom/google/common/base/Predicate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/CollectionUtils;->filterToList(Ljava/lang/Iterable;Lcom/google/common/base/Predicate;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->postProcessVolumeData(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    return-object v0
.end method

.method protected getLibraryFilter()Lcom/google/common/base/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    return-object v0
.end method

.method abstract getSortComparator()Lcom/google/android/apps/books/app/LibraryComparator;
.end method

.method public getUploadContextMenuDelegate()Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mUploadMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    return-object v0
.end method

.method public getVolumeContextMenuDelegate()Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContextMenuDelegate:Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;

    return-object v0
.end method

.method public hide(Z)V
    .locals 0
    .param p1, "animated"    # Z

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->hideCards(Z)V

    .line 275
    return-void
.end method

.method public isAvailableForReading(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->isAvailableForReading(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected isLibraryEmpty()Z
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    if-nez v0, :cond_0

    .line 411
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public isPinned(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->isPinned(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected abstract isVolumeAboutToBeDeleted(Ljava/lang/String;)Z
.end method

.method protected abstract maybeShowOrHideEmptyView()V
.end method

.method public moveToHome()V
    .locals 0

    .prologue
    .line 390
    return-void
.end method

.method protected onCardsDataChanged()V
    .locals 0

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->resetCardsAdapterData()V

    .line 423
    return-void
.end method

.method public onCoverImageLoaded()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 778
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCoverImageLoaded:Z

    if-nez v0, :cond_0

    .line 779
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCoverImageLoaded:Z

    .line 780
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->maybeShowCards(Z)Z

    .line 782
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 386
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 378
    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    .line 379
    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 380
    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressViewAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 381
    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mProgressView:Landroid/view/View;

    .line 382
    return-void
.end method

.method public onLearnMoreAboutUploadFailure()V
    .locals 4

    .prologue
    .line 557
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 558
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 559
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {}, Lcom/google/android/apps/books/util/OceanUris;->getFileUploadLearnMoreUrl()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 561
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->markExternalIntent(Landroid/content/Intent;)V

    .line 562
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 564
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 370
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 374
    return-void
.end method

.method protected postProcessVolumeData(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 399
    .local p1, "volumeData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    return-object p1
.end method

.method protected abstract resetCardsAdapterData()V
.end method

.method public setCardsData(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    const/4 v9, 0x3

    const/4 v8, 0x1

    .line 283
    const-string v6, "BooksCardsHomeView"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 284
    const-string v6, "BooksCardsHomeView"

    const-string v7, "Reloading cards list"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    .line 297
    .local v0, "booksHomeController":Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->getDownloadedOnlyMode()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 298
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 299
    .local v2, "cardsFilteredByDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/CardData;

    .line 300
    .local v1, "cardData":Lcom/google/android/apps/books/widget/CardData;
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 301
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/CardData;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    .line 302
    .local v5, "volumeId":Ljava/lang/String;
    invoke-interface {v0, v5}, Lcom/google/android/apps/books/widget/BooksHomeController;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 303
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 308
    .end local v1    # "cardData":Lcom/google/android/apps/books/widget/CardData;
    .end local v2    # "cardsFilteredByDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "volumeId":Ljava/lang/String;
    :cond_2
    move-object v2, p1

    .line 311
    .restart local v2    # "cardsFilteredByDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->sortCardData(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 312
    .local v4, "sortedCardData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    iget-object v6, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    invoke-static {v4, v6}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 313
    const-string v6, "BooksCardsHomeView"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 314
    const-string v6, "BooksCardsHomeView"

    const-string v7, "Ignoring unchanged sorted card list"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->cardsAreShowing()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/BooksHomeController;->alreadyFetchedMyEbooks()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 318
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->maybeShowCards(Z)Z

    .line 335
    :cond_5
    :goto_1
    return-void

    .line 323
    :cond_6
    iput-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mCardData:Ljava/util/List;

    .line 330
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->cardsAreShowing()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->onCardsDataChanged()V

    goto :goto_1

    .line 333
    :cond_7
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->maybeShowCards(Z)Z

    goto :goto_1
.end method

.method public show(Z)V
    .locals 1
    .param p1, "animated"    # Z

    .prologue
    .line 267
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->maybeShowCards(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->showProgress()V

    .line 270
    :cond_0
    return-void
.end method
