.class public Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
.super Lcom/google/android/apps/books/app/ReadAlongController;
.source "MediaOverlaysController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;,
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;,
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;,
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;,
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;,
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;,
        Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;
    }
.end annotation


# instance fields
.field private final mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

.field private mClipBatchId:I

.field private mClipsEnsured:Z

.field private final mCurrentPassageClipList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentReadableItemRequestId:I

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

.field private final mEnsureClipsTaskFactory:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;

.field private mLastReadElement:I

.field private mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private mPassageCount:I

.field private mPassageIndex:I

.field private final mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

.field private final mShouldDisplayTwoPages:Z

.field private mValidElementIdsToPages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;ZLcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 2
    .param p1, "dataSource"    # Lcom/google/android/apps/books/app/ReadAlongController$DataSource;
    .param p2, "reader"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;
    .param p3, "shouldDisplayTwoPages"    # Z
    .param p4, "factory"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;
    .param p5, "audioClipPlayer"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;
    .param p6, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    const/4 v0, -0x1

    .line 209
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadAlongController;-><init>()V

    .line 50
    iput v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    .line 55
    iput v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageCount:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    .line 210
    iput-object p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    .line 211
    iput-object p2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    .line 212
    iput-boolean p3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mShouldDisplayTwoPages:Z

    .line 213
    iput-object p4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mEnsureClipsTaskFactory:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;

    .line 214
    iput-object p5, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

    .line 215
    iput-object p6, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

    new-instance v1, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;-><init>(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;->setListener(Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;)V

    .line 263
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->finishSpeaking(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybePlayNextClip(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipsEnsured:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybeQueueClips()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipBatchId:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->prepStateForNextPassage()V

    return-void
.end method

.method private finishSpeaking(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 5
    .param p1, "reason"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    const/4 v4, 0x3

    const/4 v3, -0x1

    .line 476
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishSpeaking at clipIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mSpeaking:Z

    .line 480
    iput v3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    .line 481
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   finishSpeaking changed lastRead from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :cond_1
    iput v3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    iget v1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->activateMediaElement(IILjava/lang/String;)V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->finishedPlayback(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;->close()V

    .line 489
    return-void
.end method

.method private maybePlayNextClip(Ljava/lang/String;)V
    .locals 25
    .param p1, "position"    # Ljava/lang/String;

    .prologue
    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->isSpeaking()Z

    move-result v3

    if-nez v3, :cond_1

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getMediaClips()Lcom/google/android/apps/books/util/MediaClips;

    move-result-object v15

    .line 345
    .local v15, "mediaClips":Lcom/google/android/apps/books/util/MediaClips;
    if-nez v15, :cond_2

    .line 346
    const-string v3, "MediaOverlaysController"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 347
    const-string v3, "MediaOverlaysController"

    const-string v4, "MediaClips empty "

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 352
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 353
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 355
    .local v17, "passageElementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v22

    .line 356
    .local v22, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->nextPassageWithClips()I

    move-result v16

    .line 357
    .local v16, "nextPassageWithClips":I
    const/4 v3, -0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_3

    .line 358
    sget-object v3, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->END_OF_BOOK:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->finishSpeaking(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    goto :goto_0

    .line 362
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    move/from16 v0, v16

    if-eq v0, v3, :cond_5

    .line 366
    const/16 p1, 0x0

    .line 369
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mShouldDisplayTwoPages:Z

    if-eqz v3, :cond_4

    const/16 v12, 0x3e8

    .line 370
    .local v12, "delayPerPage":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-static {v12}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->createSilentDelayClip(I)Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipsEnsured:Z

    .line 375
    new-instance v18, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;-><init>(ILjava/lang/String;Ljava/util/List;)V

    .line 377
    .local v18, "request":Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    move-object/from16 v0, v18

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    goto :goto_0

    .line 369
    .end local v12    # "delayPerPage":I
    .end local v18    # "request":Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    :cond_4
    const/16 v12, 0x7d0

    goto :goto_1

    .line 381
    :cond_5
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    .line 383
    const/16 v24, 0x0

    .line 385
    .local v24, "segmentsIndicesForPassage":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v24

    .line 387
    if-eqz v24, :cond_6

    invoke-interface/range {v24 .. v24}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 388
    :cond_6
    const-string v3, "MediaOverlaysController"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 389
    const-string v3, "MediaOverlaysController"

    const-string v4, "Passage with media clips contained no segments"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 394
    :cond_7
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v23

    .line 396
    .local v23, "segmentsForPassageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-interface/range {v24 .. v24}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    .line 397
    .local v21, "segmentIndex":Ljava/lang/Integer;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 401
    .end local v21    # "segmentIndex":Ljava/lang/Integer;
    :cond_8
    const/4 v9, 0x0

    .line 402
    .local v9, "addedAllClipsForPassage":Z
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_9
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/apps/books/model/Segment;

    .line 403
    .local v19, "segment":Lcom/google/android/apps/books/model/Segment;
    if-eqz v9, :cond_b

    .line 429
    .end local v19    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_a
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 430
    const-string v3, "MediaOverlaysController"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 431
    const-string v3, "MediaOverlaysController"

    const-string v4, "Next passage with media clips contained no clips"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 406
    .restart local v19    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_b
    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Lcom/google/android/apps/books/util/MediaClips;->getNextSegmentClipList(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v20

    .line 408
    .local v20, "segmentClipList":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;"
    if-eqz v20, :cond_9

    .line 411
    const/4 v13, 0x1

    .line 412
    .local v13, "firstClipInList":Z
    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 413
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    .line 414
    .local v10, "clip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    if-eqz v13, :cond_c

    .line 415
    const/4 v13, 0x0

    .line 416
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v10}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getSegmentId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForSegmentId(Ljava/lang/String;)I

    move-result v11

    .line 418
    .local v11, "currentMediaClipListPassage":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    if-eq v11, v3, :cond_c

    .line 419
    const/4 v9, 0x1

    .line 420
    goto :goto_3

    .line 423
    .end local v11    # "currentMediaClipListPassage":I
    :cond_c
    invoke-virtual {v10}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getElementId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 436
    .end local v10    # "clip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    .end local v13    # "firstClipInList":Z
    .end local v19    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v20    # "segmentClipList":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;"
    :cond_d
    new-instance v18, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;-><init>(ILjava/lang/String;Ljava/util/List;)V

    .line 438
    .restart local v18    # "request":Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    move-object/from16 v0, v18

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    .line 440
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mEnsureClipsTaskFactory:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    new-instance v7, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v8}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;-><init>(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface/range {v3 .. v8}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;->create(Ljava/lang/String;Ljava/util/List;ILcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;Lcom/google/android/apps/books/data/BooksDataController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;->execute()V

    goto/16 :goto_0
.end method

.method private maybeQueueClips()V
    .locals 1

    .prologue
    .line 517
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipsEnsured:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 518
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->queueMediaClips(I)V

    .line 520
    :cond_0
    return-void
.end method

.method private nextPassageWithClips()I
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 452
    iget v6, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    iget v7, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageCount:I

    if-lt v6, v7, :cond_1

    .line 472
    :cond_0
    :goto_0
    return v5

    .line 456
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getMediaClips()Lcom/google/android/apps/books/util/MediaClips;

    move-result-object v3

    .line 457
    .local v3, "mediaClips":Lcom/google/android/apps/books/util/MediaClips;
    iget-object v6, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget v7, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFirstSegmentForPassageIndex(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v4

    .line 460
    .local v4, "segmentToStartSearchFrom":Lcom/google/android/apps/books/model/Segment;
    if-eqz v4, :cond_0

    .line 464
    invoke-interface {v4}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/apps/books/util/MediaClips;->getNextSegmentClipList(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v0

    .line 466
    .local v0, "clipListForNextSegmentWithClips":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 467
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    .line 468
    .local v1, "firstClip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    invoke-virtual {v1}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getSegmentId()Ljava/lang/String;

    move-result-object v2

    .line 469
    .local v2, "idOfFirstSegmentWithClips":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForSegmentId(Ljava/lang/String;)I

    move-result v5

    goto :goto_0
.end method

.method private prepStateForNextPassage()V
    .locals 1

    .prologue
    .line 620
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    .line 621
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipsEnsured:Z

    .line 622
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    .line 623
    return-void
.end method

.method private queueMediaClips(I)V
    .locals 10
    .param p1, "firstClipToAddIndex"    # I

    .prologue
    const/4 v9, 0x3

    .line 564
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mSpeaking:Z

    if-eqz v0, :cond_7

    .line 565
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queueMediaClips("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "),passage="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 569
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    const-string v0, "MediaOverlaysController"

    const-string v1, "  queueMediaClips(): cliplist empty"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->prepStateForNextPassage()V

    .line 573
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybePlayNextClip(Ljava/lang/String;)V

    .line 613
    :cond_2
    :goto_0
    return-void

    .line 576
    :cond_3
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipBatchId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipBatchId:I

    .line 577
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 578
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   queueMediaClips changed lastRead from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :cond_4
    iput p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    .line 583
    move v3, p1

    .local v3, "clipIndex":I
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentPassageClipList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    .line 586
    .local v6, "clip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    invoke-virtual {v6}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getElementId()Ljava/lang/String;

    move-result-object v7

    .line 588
    .local v7, "elementId":Ljava/lang/String;
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    iput v0, v6, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->passageIndex:I

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v6, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->devicePage:I

    .line 591
    const/4 v0, 0x1

    iput-boolean v0, v6, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->foundInPassageContent:Z

    .line 604
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

    new-instance v0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    invoke-virtual {v6}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getClipBeginMs()I

    move-result v1

    invoke-virtual {v6}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getClipEndMs()I

    move-result v2

    iget v4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipBatchId:I

    invoke-virtual {v6}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->getAudioResource()Lcom/google/android/apps/books/model/Resource;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;-><init>(IIIILcom/google/android/apps/books/model/Resource;)V

    invoke-interface {v8, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;->addClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 584
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 593
    :cond_6
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 594
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queueMediaClips(): Clip id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not in content. Playing anyway"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 609
    .end local v3    # "clipIndex":I
    .end local v6    # "clip":Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    .end local v7    # "elementId":Ljava/lang/String;
    :cond_7
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 610
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "queueMediaClips("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") -- ignored: not speaking"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private stopSpeaking(Z)V
    .locals 6
    .param p1, "notifyReader"    # Z

    .prologue
    const/4 v2, 0x3

    const/4 v5, 0x0

    .line 534
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    const-string v0, "MediaOverlaysController"

    const-string v1, "stopSpeaking(%b) passage %d, clip=%d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;->reset()V

    .line 539
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mSpeaking:Z

    .line 540
    if-eqz p1, :cond_1

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->stoppedPlaying()V

    .line 543
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .line 544
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mAudioClipPlayer:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;->close()V

    .line 546
    return-void
.end method


# virtual methods
.method public canResume()Z
    .locals 2

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mSpeaking:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelResume()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 279
    const-string v0, "MediaOverlaysController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelResume() changed lastRead from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_0
    iput v3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    .line 284
    return-void
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v3, 0x3

    .line 496
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPassageMoListReady passage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", returned "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    if-ne p1, v0, :cond_2

    .line 507
    iput p2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    .line 508
    iput-object p3, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    .line 509
    invoke-direct {p0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybeQueueClips()V

    .line 514
    :cond_1
    :goto_0
    return-void

    .line 510
    :cond_2
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 511
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPassageMoListReady requestId too old: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", new "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 309
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    const-string v0, "MediaOverlaysController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resume at passage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", clipIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 323
    const-string v0, "MediaOverlaysController"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    const-string v0, "MediaOverlaysController"

    const-string v1, "mValidElementIdsToPages is null, forcing startMediaAtPassage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->startMediaAtPassage(ILcom/google/android/apps/books/common/Position;)V

    .line 332
    :goto_0
    return-void

    .line 328
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mSpeaking:Z

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->startedPlaying()V

    .line 330
    iget v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mLastReadElement:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->queueMediaClips(I)V

    goto :goto_0
.end method

.method public setMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 0
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 556
    iput-object p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 557
    return-void
.end method

.method public setPassageCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 526
    iput p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageCount:I

    .line 527
    return-void
.end method

.method public startMediaAtPassage(ILcom/google/android/apps/books/common/Position;)V
    .locals 5
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 290
    const-string v1, "MediaOverlaysController"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    const-string v1, "MediaOverlaysController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startMediaAtPassage passage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->stopSpeaking(Z)V

    .line 297
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mSpeaking:Z

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mReader:Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;->startedPlaying()V

    .line 299
    iput-boolean v4, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipsEnsured:Z

    .line 300
    iput-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mValidElementIdsToPages:Ljava/util/Map;

    .line 301
    iput p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mPassageIndex:I

    .line 302
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybePlayNextClip(Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method public stopSpeaking()V
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->stopSpeaking(Z)V

    .line 553
    return-void
.end method
