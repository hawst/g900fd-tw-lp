.class public Lcom/google/android/apps/books/util/LazyThreadPool;
.super Ljava/lang/Object;
.source "LazyThreadPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;
    }
.end annotation


# instance fields
.field private final mControlExecutor:Ljava/util/concurrent/Executor;

.field private mExecutingTaskCount:I

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mTaskSource:Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;

.field private final mWorkerExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/util/Logger;)V
    .locals 0
    .param p1, "taskSource"    # Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;
    .param p2, "controlExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "workerExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mControlExecutor:Ljava/util/concurrent/Executor;

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mTaskSource:Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;

    .line 48
    iput-object p3, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mWorkerExecutor:Ljava/util/concurrent/Executor;

    .line 49
    iput-object p4, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/LazyThreadPool;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/util/LazyThreadPool;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/apps/books/util/LazyThreadPool;->onTaskFinished()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/util/LazyThreadPool;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/LazyThreadPool;

    .prologue
    .line 12
    iget v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    return v0
.end method

.method static synthetic access$110(Lcom/google/android/apps/books/util/LazyThreadPool;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/util/LazyThreadPool;

    .prologue
    .line 12
    iget v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/util/LazyThreadPool;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/LazyThreadPool;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mLogger:Lcom/google/android/apps/books/util/Logger;

    return-object v0
.end method

.method private onTaskFinished()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mControlExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/util/LazyThreadPool$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/util/LazyThreadPool$3;-><init>(Lcom/google/android/apps/books/util/LazyThreadPool;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 104
    return-void
.end method

.method private runTask(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v1, "LazyThreadPool"

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v1, "LazyThreadPool"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting new task; # workers = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mWorkerExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/util/LazyThreadPool$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/util/LazyThreadPool$2;-><init>(Lcom/google/android/apps/books/util/LazyThreadPool;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 90
    return-void
.end method


# virtual methods
.method public tickleOnControlExecutor()V
    .locals 3

    .prologue
    .line 57
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mTaskSource:Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;

    iget v2, p0, Lcom/google/android/apps/books/util/LazyThreadPool;->mExecutingTaskCount:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/util/LazyThreadPool$TaskSource;->poll(I)Ljava/lang/Runnable;

    move-result-object v0

    .local v0, "task":Ljava/lang/Runnable;
    if-eqz v0, :cond_0

    .line 58
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/LazyThreadPool;->runTask(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method
