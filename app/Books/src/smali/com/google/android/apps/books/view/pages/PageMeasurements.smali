.class public Lcom/google/android/apps/books/view/pages/PageMeasurements;
.super Ljava/lang/Object;
.source "PageMeasurements.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    }
.end annotation


# instance fields
.field private final mPageDimensions:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "pageWidth"    # I
    .param p2, "pageHeight"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageMeasurements;->mPageDimensions:Landroid/graphics/Point;

    .line 31
    return-void
.end method

.method private getPageHeight()I
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageMeasurements;->mPageDimensions:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method


# virtual methods
.method public calculateCenteredTextureTransformationMatrix(Landroid/graphics/Point;Landroid/graphics/Point;ZILandroid/graphics/Matrix;)V
    .locals 14
    .param p1, "textureDims"    # Landroid/graphics/Point;
    .param p2, "pageDims"    # Landroid/graphics/Point;
    .param p3, "flipped"    # Z
    .param p4, "millisForFullSpin"    # I
    .param p5, "outMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 117
    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Matrix;->reset()V

    .line 120
    const/high16 v9, -0x41000000    # -0.5f

    const/high16 v10, -0x41000000    # -0.5f

    move-object/from16 v0, p5

    invoke-virtual {v0, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 123
    if-eqz p3, :cond_1

    const/high16 v3, -0x40800000    # -1.0f

    .line 126
    .local v3, "flipFactor":F
    :goto_0
    if-eqz p2, :cond_2

    const/4 v4, 0x1

    .line 127
    .local v4, "hasPageDims":Z
    :goto_1
    if-eqz v4, :cond_3

    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/Point;->x:I

    :goto_2
    int-to-float v9, v9

    iget v10, p1, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    div-float v5, v9, v10

    .line 128
    .local v5, "xScale":F
    if-eqz v4, :cond_4

    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/Point;->y:I

    :goto_3
    int-to-float v9, v9

    iget v10, p1, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    div-float v8, v9, v10

    .line 129
    .local v8, "yScale":F
    mul-float v9, v3, v5

    move-object/from16 v0, p5

    invoke-virtual {v0, v9, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 131
    if-eqz p4, :cond_0

    .line 133
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    move/from16 v0, p4

    int-to-long v12, v0

    rem-long v6, v10, v12

    .line 136
    .local v6, "millisSinceAtTopOfRotation":J
    const/high16 v9, 0x43b40000    # 360.0f

    mul-float/2addr v9, v3

    move/from16 v0, p4

    int-to-float v10, v0

    div-float v2, v9, v10

    .line 138
    .local v2, "degreesPerMilli":F
    long-to-float v9, v6

    mul-float/2addr v9, v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v9}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 142
    .end local v2    # "degreesPerMilli":F
    .end local v6    # "millisSinceAtTopOfRotation":J
    :cond_0
    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f000000    # 0.5f

    move-object/from16 v0, p5

    invoke-virtual {v0, v9, v10}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 143
    return-void

    .line 123
    .end local v3    # "flipFactor":F
    .end local v4    # "hasPageDims":Z
    .end local v5    # "xScale":F
    .end local v8    # "yScale":F
    :cond_1
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 126
    .restart local v3    # "flipFactor":F
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 127
    .restart local v4    # "hasPageDims":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageWidth()I

    move-result v9

    goto :goto_2

    .line 128
    .restart local v5    # "xScale":F
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageHeight()I

    move-result v9

    goto :goto_3
.end method

.method public calculateNeededBorders(IIZZ)Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    .locals 9
    .param p1, "bitmapWidth"    # I
    .param p2, "bitmapHeight"    # I
    .param p3, "twoPageLandscape"    # Z
    .param p4, "isLeftPageOfTwo"    # Z

    .prologue
    const/high16 v8, 0x3fc00000    # 1.5f

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v6, 0x1

    .line 163
    new-instance v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;

    invoke-direct {v1}, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;-><init>()V

    .line 164
    .local v1, "borders":Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;
    int-to-float v4, p1

    int-to-float v5, p2

    div-float v0, v4, v5

    .line 166
    .local v0, "aspectRatio":F
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    sub-float v2, v4, v5

    .line 167
    .local v2, "extraHorizontalPixels":F
    cmpl-float v4, v2, v7

    if-lez v4, :cond_3

    .line 168
    if-eqz p3, :cond_2

    .line 169
    if-eqz p4, :cond_1

    .line 170
    iput v6, v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->left:I

    .line 190
    :cond_0
    :goto_0
    return-object v1

    .line 172
    :cond_1
    iput v6, v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->right:I

    goto :goto_0

    .line 175
    :cond_2
    iput v6, v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->right:I

    .line 176
    cmpl-float v4, v2, v8

    if-lez v4, :cond_0

    .line 177
    iput v6, v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->left:I

    goto :goto_0

    .line 181
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v0

    sub-float v3, v4, v5

    .line 182
    .local v3, "extraVerticalPixels":F
    cmpl-float v4, v3, v7

    if-lez v4, :cond_0

    .line 183
    iput v6, v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->bottom:I

    .line 184
    cmpl-float v4, v3, v8

    if-lez v4, :cond_0

    .line 185
    iput v6, v1, Lcom/google/android/apps/books/view/pages/PageMeasurements$NeededBorders;->top:I

    goto :goto_0
.end method

.method public constructPageStretchContentTransformMatrix([FLandroid/graphics/Point;Landroid/graphics/Point;)Z
    .locals 10
    .param p1, "matrix"    # [F
    .param p2, "textureDimensions"    # Landroid/graphics/Point;
    .param p3, "pageDims"    # Landroid/graphics/Point;

    .prologue
    const/4 v6, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageWidth()I

    move-result v3

    .line 58
    .local v3, "pageWidth":I
    invoke-direct {p0}, Lcom/google/android/apps/books/view/pages/PageMeasurements;->getPageHeight()I

    move-result v1

    .line 59
    .local v1, "pageHeight":I
    if-eqz p3, :cond_0

    .line 60
    iput v3, p3, Landroid/graphics/Point;->x:I

    .line 61
    iput v1, p3, Landroid/graphics/Point;->y:I

    .line 64
    :cond_0
    invoke-static {p1, v6}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 66
    iget v7, p2, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    iget v8, p2, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    div-float v4, v7, v8

    .line 67
    .local v4, "textureVerticalSkinniness":F
    int-to-float v7, v1

    int-to-float v8, v3

    div-float v2, v7, v8

    .line 69
    .local v2, "pageVerticalSkinniness":F
    cmpl-float v7, v4, v2

    if-nez v7, :cond_2

    .line 75
    const/4 v6, 0x1

    .line 97
    :cond_1
    :goto_0
    return v6

    .line 79
    :cond_2
    cmpl-float v7, v4, v2

    if-lez v7, :cond_3

    .line 81
    div-float v5, v2, v4

    .line 83
    .local v5, "widthScale":F
    invoke-static {p1, v6, v5, v9, v9}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 84
    if-eqz p3, :cond_1

    .line 85
    iget v7, p3, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    mul-float/2addr v7, v5

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, p3, Landroid/graphics/Point;->x:I

    goto :goto_0

    .line 89
    .end local v5    # "widthScale":F
    :cond_3
    div-float v0, v4, v2

    .line 91
    .local v0, "heightScale":F
    invoke-static {p1, v6, v9, v0, v9}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 92
    if-eqz p3, :cond_1

    .line 93
    iget v7, p3, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    mul-float/2addr v7, v0

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, p3, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public getPageWidth()I
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageMeasurements;->mPageDimensions:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method
