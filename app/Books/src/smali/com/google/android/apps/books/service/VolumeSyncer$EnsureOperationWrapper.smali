.class Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;
.super Ljava/lang/Object;
.source "VolumeSyncer.java"

# interfaces
.implements Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/VolumeSyncer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EnsureOperationWrapper"
.end annotation


# instance fields
.field final mOperation:Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

.field final synthetic this$0:Lcom/google/android/apps/books/service/VolumeSyncer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/VolumeSyncer;Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;)V
    .locals 0
    .param p2, "operation"    # Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;->mOperation:Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    .line 402
    return-void
.end method


# virtual methods
.method public final ensure()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 406
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # getter for: Lcom/google/android/apps/books/service/VolumeSyncer;->mEncounteredIoException:Z
    invoke-static {v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$800(Lcom/google/android/apps/books/service/VolumeSyncer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    :goto_0
    return-void

    .line 411
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;->mOperation:Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;

    invoke-interface {v1}, Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;->ensure()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$EnsureOperationWrapper;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/service/VolumeSyncer;->mEncounteredIoException:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$802(Lcom/google/android/apps/books/service/VolumeSyncer;Z)Z

    .line 414
    throw v0
.end method
