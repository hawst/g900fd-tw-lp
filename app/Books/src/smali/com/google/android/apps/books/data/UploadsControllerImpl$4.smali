.class Lcom/google/android/apps/books/data/UploadsControllerImpl$4;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadProgress(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$id:Ljava/lang/String;

.field final synthetic val$uploadPercentage:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;->val$id:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;->val$uploadPercentage:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$4$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl$4$1;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$4;)V

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/data/UploadsControllerImpl;->queueUpdate(Lcom/google/android/ublib/utils/Updater;Z)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$500(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/ublib/utils/Updater;Z)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mDataController:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$300(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$4$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl$4$2;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$4;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->notifyListeners(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;)V

    .line 201
    return-void
.end method
