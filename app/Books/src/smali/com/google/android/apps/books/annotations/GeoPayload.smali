.class Lcom/google/android/apps/books/annotations/GeoPayload;
.super Ljava/lang/Object;
.source "GeoPayload.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationDataPayload;


# instance fields
.field private final mCommon:Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

.field private final mDictionary:Lcom/google/android/apps/books/annotations/DictionaryEntry;

.field private final mGeo:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;Lcom/google/android/apps/books/annotations/DictionaryEntry;)V
    .locals 0
    .param p1, "common"    # Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    .param p2, "geo"    # Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .param p3, "dict"    # Lcom/google/android/apps/books/annotations/DictionaryEntry;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/GeoPayload;->mCommon:Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    .line 20
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/GeoPayload;->mGeo:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;

    .line 21
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/GeoPayload;->mDictionary:Lcom/google/android/apps/books/annotations/DictionaryEntry;

    .line 22
    return-void
.end method


# virtual methods
.method public getCommon()Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/GeoPayload;->mCommon:Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    return-object v0
.end method

.method public getDictionary()Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/GeoPayload;->mDictionary:Lcom/google/android/apps/books/annotations/DictionaryEntry;

    return-object v0
.end method

.method public getGeo()Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/GeoPayload;->mGeo:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;

    return-object v0
.end method
