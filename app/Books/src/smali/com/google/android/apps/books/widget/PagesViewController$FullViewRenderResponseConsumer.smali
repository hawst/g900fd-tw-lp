.class Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/RenderResponseConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullViewRenderResponseConsumer"
.end annotation


# instance fields
.field private final mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

.field private final mPosition:Lcom/google/android/apps/books/common/Position;

.field private final mPvcRef:Ljava/lang/ref/Reference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/Reference",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController;",
            ">;"
        }
    .end annotation
.end field

.field private mRecycled:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/common/Position;)V
    .locals 1
    .param p1, "pvc"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 1083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mPvcRef:Ljava/lang/ref/Reference;

    .line 1085
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    .line 1086
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mPosition:Lcom/google/android/apps/books/common/Position;

    .line 1087
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p2, "x1"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "x2"    # Lcom/google/android/apps/books/common/Position;
    .param p4, "x3"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 1072
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/common/Position;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    .prologue
    .line 1072
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->recycle()V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)Lcom/google/android/apps/books/util/RenderRequestContext;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    return-object v0
.end method

.method private recycle()V
    .locals 1

    .prologue
    .line 1079
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mRecycled:Z

    .line 1080
    return-void
.end method


# virtual methods
.method public isPurgeable()Z
    .locals 1

    .prologue
    .line 1130
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mRecycled:Z

    return v0
.end method

.method public onMissingPosition()V
    .locals 2

    .prologue
    .line 1122
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mPvcRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController;

    .line 1123
    .local v0, "pvc":Lcom/google/android/apps/books/widget/PagesViewController;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->isCurrentRequest(Lcom/google/android/apps/books/util/RenderRequestContext;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2200(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1124
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mPosition:Lcom/google/android/apps/books/common/Position;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->onMissingPosition(Lcom/google/android/apps/books/common/Position;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2300(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/common/Position;)V

    .line 1126
    :cond_0
    return-void
.end method

.method public onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 1091
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mPvcRef:Ljava/lang/ref/Reference;

    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesViewController;

    .line 1092
    .local v1, "pvc":Lcom/google/android/apps/books/widget/PagesViewController;
    if-eqz v1, :cond_0

    .line 1093
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    .line 1094
    .local v0, "manager":Lcom/google/android/apps/books/util/UIThreadTaskManager;
    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->isBusy()Z
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1800(Lcom/google/android/apps/books/widget/PagesViewController;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1095
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForFullView(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V
    invoke-static {v1, p1, v2, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1900(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V

    .line 1110
    .end local v0    # "manager":Lcom/google/android/apps/books/util/UIThreadTaskManager;
    :cond_0
    :goto_0
    return-void

    .line 1097
    .restart local v0    # "manager":Lcom/google/android/apps/books/util/UIThreadTaskManager;
    :cond_1
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;

    invoke-direct {v2, p0, v1, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->post(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;I)V

    goto :goto_0
.end method

.method public onSpecialState(Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 2
    .param p1, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 1114
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mPvcRef:Ljava/lang/ref/Reference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController;

    .line 1115
    .local v0, "pvc":Lcom/google/android/apps/books/widget/PagesViewController;
    if-eqz v0, :cond_0

    .line 1116
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->onSpecialState(Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2100(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    .line 1118
    :cond_0
    return-void
.end method
