.class public Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
.super Ljava/lang/Object;
.source "TableSynchronizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/sync/TableSynchronizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncResult"
.end annotation


# instance fields
.field public final modifiedValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field public final originals:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field public final orphanRows:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Collection;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/content/ContentValues;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "orphanRows":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/content/ContentValues;>;"
    .local p2, "originals":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/content/ContentValues;>;"
    .local p3, "modifiedValues":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/content/ContentValues;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->orphanRows:Ljava/util/Collection;

    .line 74
    iput-object p2, p0, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->originals:Ljava/util/Map;

    .line 75
    iput-object p3, p0, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->modifiedValues:Ljava/util/Map;

    .line 76
    return-void
.end method
