.class public interface abstract Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;
.super Ljava/lang/Object;
.source "SearchResultsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SearchResultsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SearchResultCallbacks"
.end annotation


# virtual methods
.method public abstract onSearchResultSelected(Lcom/google/android/apps/books/annotations/TextLocation;)V
.end method

.method public abstract onSearchWebSelected(Ljava/lang/String;)V
.end method
