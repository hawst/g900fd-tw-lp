.class public Lcom/google/android/apps/books/preference/LocalPreferences;
.super Ljava/lang/Object;
.source "LocalPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/preference/LocalPreferences$VolumeSortOrder;,
        Lcom/google/android/apps/books/preference/LocalPreferences$AutoReadAloud;
    }
.end annotation


# instance fields
.field private final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    const-string v0, "context is null in LocalPreferences()"

    invoke-static {p1, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/SharedPreferences;)V

    .line 185
    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-object p1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 190
    return-void
.end method

.method public static isAccountKey(Ljava/lang/String;)Z
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 524
    const-string v0, "account"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public appVersionHasChanged(Ljava/lang/String;)Z
    .locals 4
    .param p1, "currentAppVersion"    # Ljava/lang/String;

    .prologue
    .line 623
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "lastBooksAppVersionOpened"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 625
    .local v0, "previouslyOpenedAppVersion":Ljava/lang/String;
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public applyMissingDefaults()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 229
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "brightness"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 230
    const-string v1, "brightness"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "themes"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 233
    const-string v1, "themes"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 235
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "infoCards"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 236
    const-string v1, "infoCards"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 238
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "typeface2"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 239
    const-string v1, "typeface2"

    const-string v2, "default"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 241
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "textZoom"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 242
    const-string v1, "textZoom"

    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoom()F

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 244
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "lineHeight2"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 245
    const-string v1, "lineHeight2"

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 247
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "justification2"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 248
    const-string v1, "justification2"

    const-string v2, "default"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 250
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "viewMode"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 251
    const-string v1, "viewMode"

    const-string v2, "readnow"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 253
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "downloadMode"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 254
    const-string v1, "downloadMode"

    const-string v2, "wifiOnly"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 256
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "autoReadAloud"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 257
    const-string v1, "autoReadAloud"

    sget v2, Lcom/google/android/apps/books/preference/LocalPreferences$AutoReadAloud;->_DEFAULT:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 259
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "rotationLockMode"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 260
    const-string v1, "rotationLockMode"

    const-string v2, "system"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 262
    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "volumeKeyPageTurn"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 263
    const-string v1, "volumeKeyPageTurn"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 265
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "highlightColor"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 266
    const-string v1, "highlightColor"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 268
    :cond_c
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 269
    return-void
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 199
    .local v0, "accountName":Ljava/lang/String;
    if-eqz v0, :cond_0

    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 194
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getAutoReadAloud()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 404
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "autoReadAloud"

    sget v3, Lcom/google/android/apps/books/preference/LocalPreferences$AutoReadAloud;->_DEFAULT:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBrightness()I
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "brightness"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getDismissedHatsSurveyTimestamp()J
    .locals 4

    .prologue
    .line 732
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "dismissedHatsSurveyTimestamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDownloadMode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "downloadMode"

    const-string v2, "wifiOnly"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadedOnlyMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 387
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "downloadedOnlyMode"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getFrontend()Ljava/lang/String;
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "db_frontend"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHasCheckedAutostartOfOnboardingQuiz()Z
    .locals 3

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "autostartedOnboardingQuiz"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getHasLoadedLibraryBefore()Z
    .locals 3

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "hasLoadedLibraryBefore"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getHasShownUnsupportedCountryWarning()Z
    .locals 3

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "shownUnsupportedCountry"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getHatsDeviceId()I
    .locals 3

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "hatsDeviceId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getHighlightColor()I
    .locals 3

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "highlightColor"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getLastDictionaryMetadataSync()J
    .locals 4

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "lastDictionaryMetadataSync"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getLastOpenedTocTab()I
    .locals 3

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "lastOpenedTocTab"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getLoggingId()Ljava/lang/String;
    .locals 5

    .prologue
    .line 645
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "stringNonNegativeLoggingId"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 646
    .local v1, "loggingId":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 647
    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->getRandom()Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    .line 649
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 650
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "stringNonNegativeLoggingId"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 651
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 653
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-object v1
.end method

.method public getPageLayout()Z
    .locals 4

    .prologue
    .line 281
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "page_layout"

    const-string v3, "2up"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "layout":Ljava/lang/String;
    const-string v1, "1up"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public getPageTurnMode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pageTurnMode"

    const-string v2, "turn3d"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlaylogStickyId()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 665
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "stringNonNegativePlaylogStickyId"

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 667
    .local v2, "playlogStickyId":J
    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    .line 668
    invoke-static {}, Lcom/google/android/apps/books/app/BooksApplication;->getRandom()Ljava/util/Random;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 669
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 670
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "stringNonNegativePlaylogStickyId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 671
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 673
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-wide v2
.end method

.method public getReaderTheme()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "themes"

    const-string v2, "0"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRotationLockMode()Ljava/lang/String;
    .locals 3

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "rotationLockMode"

    const-string v2, "system"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getShowUnpinDialog()Z
    .locals 3

    .prologue
    .line 588
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "showUnpinDialog"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowVolumeKeyPageTurnDialog()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "showVolumeKeyPageTurnDialog"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getSuppressOnboardingCard()Z
    .locals 3

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "dismissedOnboardingCard"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getTextAlign()Ljava/lang/String;
    .locals 4

    .prologue
    .line 328
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "justification2"

    const-string v3, "default"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330
    .local v0, "prefValue":Ljava/lang/String;
    const-string v1, "default"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "prefValue":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getTextZoom()F
    .locals 3

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "textZoom"

    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoom()F

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getTranslateLanguage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, ""

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTypeface()Ljava/lang/String;
    .locals 4

    .prologue
    .line 308
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "typeface2"

    const-string v3, "default"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "prefValue":Ljava/lang/String;
    const-string v1, "default"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "prefValue":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getUseNetworkTts()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 418
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "networkTts"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getVolumeKeyPageTurn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 449
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "volumeKeyPageTurn"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getVolumeSortOrder()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 4

    .prologue
    .line 334
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "volumeSortOrder"

    sget-object v3, Lcom/google/android/apps/books/preference/LocalPreferences$VolumeSortOrder;->_DEFAULT:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "comparatorString":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/app/LibraryComparator;->fromString(Ljava/lang/String;)Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v1

    return-object v1
.end method

.method public hasDismissedHatsSurveyTimestamp()Z
    .locals 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "dismissedHatsSurveyTimestamp"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasHatsDeviceId()Z
    .locals 2

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "hatsDeviceId"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasSeenCloudLoadingIntro()Z
    .locals 3

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "hasSeenCloudLoadingIntro"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setAccount(Landroid/accounts/Account;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 205
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    .line 206
    const-string v1, "account"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 210
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 211
    return-void

    .line 208
    :cond_0
    const-string v1, "account"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public setAutoReadAloud(Z)V
    .locals 3
    .param p1, "autoReadAloud"    # Z

    .prologue
    .line 410
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 411
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 413
    .local v1, "value":I
    :goto_0
    const-string v2, "autoReadAloud"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 414
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 415
    return-void

    .line 411
    .end local v1    # "value":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDismissedHatsSurveyTimestamp(J)V
    .locals 3
    .param p1, "timestamp"    # J

    .prologue
    .line 736
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dismissedHatsSurveyTimestamp"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 738
    return-void
.end method

.method public setDownloadMode(Ljava/lang/String;)V
    .locals 4
    .param p1, "downloadMode"    # Ljava/lang/String;

    .prologue
    .line 374
    const-string v1, "always"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "wifiOnly"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 377
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "downloadMode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 378
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 384
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    return-void

    .line 380
    :cond_2
    const-string v1, "LocalPreferences"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 381
    const-string v1, "LocalPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown downloadMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDownloadedOnlyMode(Z)V
    .locals 3
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 393
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 394
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 396
    .local v1, "value":I
    :goto_0
    const-string v2, "downloadedOnlyMode"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 397
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 398
    return-void

    .line 394
    .end local v1    # "value":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setFrontend(Ljava/lang/String;)V
    .locals 2
    .param p1, "frontend"    # Ljava/lang/String;

    .prologue
    .line 218
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 219
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "db_frontend"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 220
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 221
    return-void
.end method

.method public setHasCheckedAutostartOfOnboardingQuiz(Z)V
    .locals 2
    .param p1, "onboardingQuizStarted"    # Z

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "autostartedOnboardingQuiz"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 712
    return-void
.end method

.method public setHasLoadedLibraryBefore()V
    .locals 3

    .prologue
    .line 572
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 573
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "hasLoadedLibraryBefore"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 574
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 575
    return-void
.end method

.method public setHasSeenCloudLoadingIntro(Z)V
    .locals 2
    .param p1, "wasSeen"    # Z

    .prologue
    .line 613
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 614
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "hasSeenCloudLoadingIntro"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 615
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 616
    return-void
.end method

.method public setHasShownUnsupportedCountryWarning(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 533
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 534
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "shownUnsupportedCountry"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 535
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 536
    return-void
.end method

.method public setHatsDeviceId(I)V
    .locals 2
    .param p1, "deviceId"    # I

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hatsDeviceId"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 725
    return-void
.end method

.method public setHighlightColor(I)V
    .locals 4
    .param p1, "highlightColor"    # I

    .prologue
    .line 496
    const/4 v1, 0x1

    if-lt p1, v1, :cond_1

    const/4 v1, 0x4

    if-gt p1, v1, :cond_1

    .line 498
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 499
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "highlightColor"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 500
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 506
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    const-string v1, "LocalPreferences"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    const-string v1, "LocalPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown highlightColor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLastAppVersionOpened(Ljava/lang/String;)V
    .locals 2
    .param p1, "currentAppVersion"    # Ljava/lang/String;

    .prologue
    .line 632
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 633
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "lastBooksAppVersionOpened"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 634
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 635
    return-void
.end method

.method public setLastDictionaryMetadataSync(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastDictionaryMetadataSync"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 688
    return-void
.end method

.method public setLastOpenedTocTab(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 582
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 583
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "lastOpenedTocTab"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 584
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 585
    return-void
.end method

.method public setPageTurnMode(Ljava/lang/String;)V
    .locals 4
    .param p1, "pageTurnMode"    # Ljava/lang/String;

    .prologue
    .line 468
    const-string v1, "turn2d"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "turn3d"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 470
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "pageTurnMode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 471
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 477
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    return-void

    .line 473
    :cond_2
    const-string v1, "LocalPreferences"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 474
    const-string v1, "LocalPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown pageTurnMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setRotationLockMode(Ljava/lang/String;)V
    .locals 4
    .param p1, "rotationLockMode"    # Ljava/lang/String;

    .prologue
    .line 435
    const-string v1, "system"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "landscape"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "portrait"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 439
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "rotationLockMode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 440
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 446
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    return-void

    .line 442
    :cond_2
    const-string v1, "LocalPreferences"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 443
    const-string v1, "LocalPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown orientationMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSawVolumeKeyPageTurnDialog()V
    .locals 3

    .prologue
    .line 485
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 486
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "showVolumeKeyPageTurnDialog"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 487
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 488
    return-void
.end method

.method public setShouldShowOfflineDictionaryCard(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 544
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 545
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "showOfflineDictionaryCard"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 546
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 547
    return-void
.end method

.method public setShowUnpinDialog(Z)V
    .locals 2
    .param p1, "showUnpinDialog"    # Z

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "showUnpinDialog"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 594
    return-void
.end method

.method public setSidePanelWasClosedOnce(Z)V
    .locals 2
    .param p1, "wasClosed"    # Z

    .prologue
    .line 607
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 608
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "sidePanelClosedOnce"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 609
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 610
    return-void
.end method

.method public setSuppressOnboardingCard(Z)V
    .locals 2
    .param p1, "suppressCard"    # Z

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dismissedOnboardingCard"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 700
    return-void
.end method

.method public setTranslateLanguage(Ljava/lang/String;)V
    .locals 2
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 514
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 515
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, ""

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 516
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 517
    return-void
.end method

.method public setUseNetworkTts(Z)V
    .locals 3
    .param p1, "useNetworkTts"    # Z

    .prologue
    .line 423
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 424
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 425
    .local v1, "value":I
    :goto_0
    const-string v2, "networkTts"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 426
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 427
    return-void

    .line 424
    .end local v1    # "value":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setViewMode(Ljava/lang/String;)V
    .locals 4
    .param p1, "viewMode"    # Ljava/lang/String;

    .prologue
    .line 357
    const-string v1, "readnow"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mylibrary"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 358
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 359
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "viewMode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 360
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 366
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    return-void

    .line 362
    :cond_2
    const-string v1, "LocalPreferences"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 363
    const-string v1, "LocalPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown viewMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVolumeKeyPageTurn(Z)V
    .locals 3
    .param p1, "volumeKeyPageTurn"    # Z

    .prologue
    .line 455
    iget-object v2, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 456
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 458
    .local v1, "value":I
    :goto_0
    const-string v2, "volumeKeyPageTurn"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 459
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 460
    return-void

    .line 456
    .end local v1    # "value":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVolumeSortOrder(Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 3
    .param p1, "sortOrder"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 340
    iget-object v1, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 341
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "volumeSortOrder"

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/LibraryComparator;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 342
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 343
    return-void
.end method

.method public shouldShowOfflineDictionaryCard()Z
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "showOfflineDictionaryCard"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public wasSidePanelClosedOnce()Z
    .locals 3

    .prologue
    .line 597
    iget-object v0, p0, Lcom/google/android/apps/books/preference/LocalPreferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "sidePanelClosedOnce"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
