.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$resource:Lcom/google/android/apps/books/model/Resource;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Resource;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 469
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 477
    :goto_0
    return-void

    .line 473
    :cond_0
    new-instance v4, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localBlob(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/android/apps/books/data/DataControllerBlob;
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1300(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/android/apps/books/data/DataControllerBlob;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v3, v3, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v5, v5, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localResourceRetriever(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
    invoke-static {v2, v3, v5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1400(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v4, v1, v0, v2, v3}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$1;)V

    .line 476
    .local v4, "result":Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$resource:Lcom/google/android/apps/books/model/Resource;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    const/4 v5, 0x0

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 466
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$7;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
