.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeCover(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$imageConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volume:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 918
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$imageConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 921
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCoverSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$imageConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;->getImage(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 923
    return-void
.end method
