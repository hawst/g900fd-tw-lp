.class Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReadAlongController$DataSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RendererReadAlongDataSource"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 7938
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 7938
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 7959
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getLocale()Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16200(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public isPassageForbidden(I)Z
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 7954
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/Renderer;->isPassageForbidden(I)Z

    move-result v0

    return v0
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 7941
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 7942
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/Renderer;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v0

    .line 7944
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shutDown()V
    .locals 0

    .prologue
    .line 7966
    return-void
.end method
