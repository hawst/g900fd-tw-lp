.class public Lcom/google/android/apps/books/render/RenderPosition;
.super Ljava/lang/Object;
.source "RenderPosition.java"


# instance fields
.field public final bitmapConfig:Landroid/graphics/Bitmap$Config;

.field public final pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

.field public final sampleSize:I

.field public final spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/PageIdentifier;Landroid/graphics/Bitmap$Config;)V
    .locals 2
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 74
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V
    .locals 2
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "spreadPageIdentifier"    # Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .param p3, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;
    .param p4, "sampleSize"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-nez p1, :cond_0

    if-eqz p2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkArgument(Z)V

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 57
    iput-object p2, p0, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .line 58
    if-nez p3, :cond_3

    .line 59
    const-string v0, "RenderPosition"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    const-string v0, "RenderPosition"

    const-string v1, "Warning: using default bitmap config"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_1
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/google/android/apps/books/render/RenderPosition;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 66
    :goto_1
    iput p4, p0, Lcom/google/android/apps/books/render/RenderPosition;->sampleSize:I

    .line 67
    return-void

    .line 55
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_3
    iput-object p3, p0, Lcom/google/android/apps/books/render/RenderPosition;->bitmapConfig:Landroid/graphics/Bitmap$Config;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;)V
    .locals 2
    .param p1, "spreadPageIdentifier"    # Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .param p2, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 70
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V

    .line 71
    return-void
.end method


# virtual methods
.method public getIndices()Lcom/google/android/apps/books/render/PageIndices;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    goto :goto_0
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    iget-object v0, v0, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v0, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 136
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "pageIdentifier"

    iget-object v2, p0, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "spreadPageIdentifier"

    iget-object v2, p0, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
