.class Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;
.super Landroid/os/AsyncTask;
.source "BaseBooksActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResetStorageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/common/FileStorageManager;)V
    .locals 0
    .param p1, "fsm"    # Lcom/google/android/apps/books/common/FileStorageManager;

    .prologue
    .line 939
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 940
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    .line 941
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 935
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$ResetStorageTask;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    invoke-interface {v0}, Lcom/google/android/apps/books/common/FileStorageManager;->resetFileStorageLocation()V

    .line 946
    const/4 v0, 0x0

    return-object v0
.end method
