.class public interface abstract Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;
.super Ljava/lang/Object;
.source "MediaOverlaysController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AbstractEnsureClipsTaskFactory"
.end annotation


# virtual methods
.method public abstract create(Ljava/lang/String;Ljava/util/List;ILcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;Lcom/google/android/apps/books/data/BooksDataController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;I",
            "Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ")",
            "Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;"
        }
    .end annotation
.end method
