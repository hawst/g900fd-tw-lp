.class public interface abstract Lcom/google/android/apps/books/widget/PagesDisplay$Callbacks;
.super Ljava/lang/Object;
.source "PagesDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract getCurrentOffsetFromBasePosition()I
.end method

.method public abstract getCurrentRelevantOffsets()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method
