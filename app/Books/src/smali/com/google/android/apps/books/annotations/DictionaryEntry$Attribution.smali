.class public Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/DictionaryEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Attribution"
.end annotation


# instance fields
.field public final text:Ljava/lang/String;

.field public final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;->text:Ljava/lang/String;

    .line 105
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;->url:Ljava/lang/String;

    .line 106
    return-void
.end method
