.class public Lcom/google/android/apps/books/app/HelpUtils;
.super Ljava/lang/Object;
.source "HelpUtils.java"


# static fields
.field private static final BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

.field private static final BYTES_PER_PIXEL:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    sput-object v0, Lcom/google/android/apps/books/app/HelpUtils;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    .line 57
    sget-object v0, Lcom/google/android/apps/books/app/HelpUtils;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {v0}, Lcom/google/android/apps/books/util/BitmapUtils;->getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I

    move-result v0

    sput v0, Lcom/google/android/apps/books/app/HelpUtils;->BYTES_PER_PIXEL:I

    return-void
.end method

.method private static createAndDrawToBitmap(Landroid/app/Activity;IILandroid/view/View;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I
    .param p3, "view"    # Landroid/view/View;
    .param p4, "bitmapWidth"    # I
    .param p5, "bitmapHeight"    # I

    .prologue
    const/4 v9, 0x0

    .line 121
    sget-object v6, Lcom/google/android/apps/books/app/HelpUtils;->BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 122
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 124
    .local v2, "canvas":Landroid/graphics/Canvas;
    int-to-float v6, p4

    int-to-float v7, p1

    div-float/2addr v6, v7

    int-to-float v7, p5

    int-to-float v8, p2

    div-float/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Landroid/graphics/Canvas;->scale(FF)V

    .line 126
    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    .line 127
    .local v4, "theme":Landroid/content/res/Resources$Theme;
    const/4 v6, 0x1

    new-array v6, v6, [I

    const v7, 0x1010054

    aput v7, v6, v9

    invoke-virtual {v4, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 129
    .local v5, "typedArray":Landroid/content/res/TypedArray;
    invoke-virtual {v5, v9, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 130
    .local v3, "res":I
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 131
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 134
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 137
    invoke-virtual {p3, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 138
    return-object v1
.end method

.method public static getScreenshot(Landroid/view/View;Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 20
    .param p0, "view"    # Landroid/view/View;
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 67
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 68
    .local v3, "viewWidth":I
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 70
    .local v4, "viewHeight":I
    mul-int v17, v3, v4

    .line 72
    .local v17, "origNumPixels":I
    sget v2, Lcom/google/android/apps/books/app/HelpUtils;->BYTES_PER_PIXEL:I

    mul-int v16, v17, v2

    .line 74
    .local v16, "origBytes":I
    const/high16 v2, 0x49800000    # 1048576.0f

    move/from16 v0, v16

    int-to-float v5, v0

    div-float v19, v2, v5

    .line 76
    .local v19, "scaleFactorSquared":F
    move/from16 v0, v19

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v0, v8

    move/from16 v18, v0

    .line 81
    .local v18, "scaleFactor":F
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v18, v2

    if-lez v2, :cond_0

    .line 82
    move v6, v3

    .line 83
    .local v6, "adjustedWidth":I
    move v7, v4

    .local v7, "adjustedHeight":I
    :goto_0
    move-object/from16 v2, p1

    move-object/from16 v5, p0

    .line 90
    :try_start_0
    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/books/app/HelpUtils;->createAndDrawToBitmap(Landroid/app/Activity;IILandroid/view/View;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 106
    :goto_1
    return-object v2

    .line 85
    .end local v6    # "adjustedWidth":I
    .end local v7    # "adjustedHeight":I
    :cond_0
    int-to-float v2, v3

    mul-float v2, v2, v18

    float-to-int v6, v2

    .line 86
    .restart local v6    # "adjustedWidth":I
    int-to-float v2, v4

    mul-float v2, v2, v18

    float-to-int v7, v2

    .restart local v7    # "adjustedHeight":I
    goto :goto_0

    .line 92
    :catch_0
    move-exception v14

    .line 95
    .local v14, "e":Ljava/lang/OutOfMemoryError;
    :try_start_1
    const-string v2, "HelpUtils"

    const/4 v5, 0x6

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 96
    const-string v2, "HelpUtils"

    const-string v5, "Could not create feedback screenshot. Sending small screenshot"

    invoke-static {v2, v5, v14}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 99
    :cond_1
    div-int/lit8 v2, v6, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v12

    div-int/lit8 v2, v7, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v13

    move-object/from16 v8, p1

    move v9, v3

    move v10, v4

    move-object/from16 v11, p0

    invoke-static/range {v8 .. v13}, Lcom/google/android/apps/books/app/HelpUtils;->createAndDrawToBitmap(Landroid/app/Activity;IILandroid/view/View;II)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_1

    .line 102
    :catch_1
    move-exception v15

    .line 103
    .local v15, "eInner":Ljava/lang/OutOfMemoryError;
    const-string v2, "HelpUtils"

    const/4 v5, 0x6

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 104
    const-string v2, "HelpUtils"

    const-string v5, "Could not create small feedback screenshot"

    invoke-static {v2, v5, v15}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;Lcom/google/android/apps/books/util/Config;)V
    .locals 16
    .param p0, "helpContext"    # Ljava/lang/String;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "screenshot"    # Landroid/graphics/Bitmap;
    .param p4, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 144
    new-instance v8, Landroid/content/Intent;

    const-string v12, "android.intent.action.VIEW"

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/books/util/OceanUris;->getTermsOfServiceUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;

    move-result-object v13

    invoke-direct {v8, v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 148
    .local v8, "tosIntent":Landroid/content/Intent;
    new-instance v6, Landroid/content/Intent;

    const-string v12, "android.intent.action.VIEW"

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/books/util/OceanUris;->getPrivacyUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;

    move-result-object v13

    invoke-direct {v6, v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 152
    .local v6, "privacyIntent":Landroid/content/Intent;
    const v12, 0x7f0f00cd

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 153
    .local v5, "licensesTitle":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 154
    .local v1, "args":Landroid/os/Bundle;
    new-instance v4, Landroid/content/Intent;

    const-class v12, Lcom/google/android/apps/books/app/LicensesWebViewActivity;

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    .local v4, "licensesIntent":Landroid/content/Intent;
    const-string v12, "android.intent.extra.TITLE"

    invoke-virtual {v1, v12, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {v4, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 158
    new-instance v11, Landroid/content/Intent;

    const-class v12, Lcom/google/android/apps/books/app/CopyVersionActivity;

    move-object/from16 v0, p2

    invoke-direct {v11, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    .local v11, "versionIntent":Landroid/content/Intent;
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 162
    .local v10, "versionArgs":Landroid/os/Bundle;
    const v12, 0x7f0f00dd

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/books/util/Config;->getPronounceableVersionString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 164
    .local v9, "version":Ljava/lang/String;
    const-string v12, "android.intent.extra.books.VERSION"

    invoke-virtual {v10, v12, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {v11, v10}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 167
    invoke-virtual/range {p2 .. p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 169
    .local v7, "res":Landroid/content/res/Resources;
    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->newInstance(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    const/4 v13, 0x0

    const v14, 0x7f0f00cf

    invoke-virtual {v7, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14, v8}, Lcom/google/android/gms/googlehelp/GoogleHelp;->addAdditionalOverflowMenuItem(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    const/4 v13, 0x1

    const v14, 0x7f0f00d1

    invoke-virtual {v7, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14, v6}, Lcom/google/android/gms/googlehelp/GoogleHelp;->addAdditionalOverflowMenuItem(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    const/4 v13, 0x2

    const v14, 0x7f0f00cd

    invoke-virtual {v7, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14, v4}, Lcom/google/android/gms/googlehelp/GoogleHelp;->addAdditionalOverflowMenuItem(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    const/4 v13, 0x3

    invoke-virtual {v12, v13, v9, v11}, Lcom/google/android/gms/googlehelp/GoogleHelp;->addAdditionalOverflowMenuItem(ILjava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v12

    invoke-static {}, Lcom/google/android/apps/books/util/OceanUris;->getHelpCenterUrl()Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    .line 181
    .local v2, "helpInstance":Lcom/google/android/gms/googlehelp/GoogleHelp;
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 182
    .local v3, "helpIntent":Landroid/content/Intent;
    new-instance v12, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v12, v3}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;->launch(Landroid/content/Intent;)V

    .line 184
    const-string v12, "HelpUtils"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 185
    const-string v12, "HelpUtils"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Starting help for context "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :cond_0
    return-void
.end method
