.class public Lcom/google/android/apps/books/annotations/ServerAnnotation;
.super Ljava/lang/Object;
.source "ServerAnnotation.java"


# instance fields
.field public final annotation:Lcom/google/android/apps/books/annotations/Annotation;

.field public final receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V
    .locals 0
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "receipt"    # Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 17
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    .line 18
    return-void
.end method

.method public static locallyCreated(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/ServerAnnotation;
    .locals 2
    .param p0, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/annotations/ServerAnnotation;-><init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 22
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "annotation"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "receipt"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withOverriddenLocalId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/ServerAnnotation;
    .locals 3
    .param p1, "editedLocalId"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/annotations/Annotation;->withUpdatedId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ServerAnnotation;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/ServerAnnotation;-><init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    return-object v0
.end method
