.class public Lcom/google/android/apps/books/provider/DataConversions;
.super Ljava/lang/Object;
.source "DataConversions.java"


# direct methods
.method public static calculateContentStatus(Lcom/google/android/apps/books/model/Page;)I
    .locals 1
    .param p0, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 206
    invoke-interface {p0}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    .line 208
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/books/model/Page;->getRemoteUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    const/4 v0, 0x2

    goto :goto_0

    .line 211
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ccBoxToContentValues(Lcom/google/android/apps/books/model/CcBox;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "box"    # Lcom/google/android/apps/books/model/CcBox;

    .prologue
    .line 216
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 217
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "cc_box_x"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/CcBox;->getX()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 218
    const-string v1, "cc_box_y"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/CcBox;->getY()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    const-string v1, "cc_box_w"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/CcBox;->getW()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 220
    const-string v1, "cc_box_h"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/CcBox;->getH()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 221
    return-object v0
.end method

.method public static chapterToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Chapter;Ljava/util/List;Ljava/util/List;I)Landroid/content/ContentValues;
    .locals 5
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "chapter"    # Lcom/google/android/apps/books/model/Chapter;
    .param p5, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Chapter;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;I)",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .line 141
    .local p3, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .local p4, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 142
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "account_name"

    invoke-virtual {v2, v3, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v3, "volume_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v3, "chapter_order"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145
    const-string v3, "chapter_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Chapter;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v3, "title"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Chapter;->getStartSegmentIndex()I

    move-result v1

    .line 148
    .local v1, "startSegmentIndex":I
    const-string v4, "start_section_id"

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v0

    .line 150
    .local v0, "startPageIndex":I
    const-string v4, "start_page_id"

    invoke-interface {p4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v3, "depth"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Chapter;->getDepth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    const-string v3, "reading_position"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Chapter;->getReadingPosition()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-object v2
.end method

.method public static offlineEntryToDictionaryEntry(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .locals 24
    .param p0, "oe"    # Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    .prologue
    .line 262
    if-nez p0, :cond_0

    .line 263
    const/4 v2, 0x0

    .line 299
    :goto_0
    return-object v2

    .line 266
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v23

    .line 268
    .local v23, "words":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->getWordList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    .line 269
    .local v19, "ow":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v20

    .line 270
    .local v20, "senseList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;>;"
    invoke-virtual/range {v19 .. v19}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getSenseList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;

    .line 271
    .local v18, "os":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    invoke-virtual/range {v18 .. v18}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPartOfSpeech()Ljava/lang/String;

    move-result-object v5

    .line 272
    .local v5, "partOfSpeech":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getPronunciation()Ljava/lang/String;

    move-result-object v4

    .line 273
    .local v4, "pronunciation":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 274
    .local v7, "definitions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;>;"
    invoke-virtual/range {v18 .. v18}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getDefinitionList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 275
    .local v12, "def":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;

    const/4 v3, 0x0

    invoke-direct {v2, v12, v3}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 277
    .end local v12    # "def":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 278
    .local v9, "synonyms":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    invoke-virtual/range {v18 .. v18}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;->getSynonymList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    .line 279
    .local v21, "syn":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 281
    .end local v21    # "syn":Ljava/lang/String;
    :cond_2
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 285
    .end local v4    # "pronunciation":Ljava/lang/String;
    .end local v5    # "partOfSpeech":Ljava/lang/String;
    .end local v7    # "definitions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;>;"
    .end local v9    # "synonyms":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "os":Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineSense;
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v14

    .line 287
    .local v14, "derivatives":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    invoke-virtual/range {v19 .. v19}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getDerivativeList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 288
    .local v13, "derivative":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;

    const/4 v3, 0x0

    invoke-direct {v2, v13, v3}, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 291
    .end local v13    # "derivative":Ljava/lang/String;
    :cond_4
    new-instance v22, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    .line 293
    .local v22, "word":Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 296
    .end local v14    # "derivatives":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v19    # "ow":Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;
    .end local v20    # "senseList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;>;"
    .end local v22    # "word":Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;
    :cond_5
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_6

    .line 297
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 299
    :cond_6
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->getWord(I)Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ocean/offline/dict/OfflineDictEntry$Word;->getTitle()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/books/annotations/DictionaryEntry;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public static pageToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 184
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 185
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v1, "page_order"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getPageOrder()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 189
    const-string v1, "page_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "content_status"

    invoke-static {p2}, Lcom/google/android/apps/books/provider/DataConversions;->calculateContentStatus(Lcom/google/android/apps/books/model/Page;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 192
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 193
    const-string v1, "title"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getRemoteUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 196
    const-string v1, "remote_url"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getRemoteUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_1
    return-object v0
.end method

.method public static resourceToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Ljava/lang/String;II)Landroid/content/ContentValues;
    .locals 5
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p3, "relatedSegmentId"    # Ljava/lang/String;
    .param p4, "order"    # I
    .param p5, "contentStatus"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 230
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 231
    .local v0, "result":Landroid/content/ContentValues;
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v1, "resource_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v1, "resource_type"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v1, "related_section_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v1, "resource_order"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 237
    const-string v1, "remote_url"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v1, "content_status"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 239
    const-string v1, "language"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const-string v1, "md5_hash"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getMd5Hash()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v4, "is_shared"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 242
    const-string v1, "is_default"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getIsDefault()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 243
    const-string v1, "overlay"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Resource;->getOverlay()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return-object v0

    :cond_0
    move v1, v3

    .line 241
    goto :goto_0

    :cond_1
    move v2, v3

    .line 242
    goto :goto_1
.end method

.method public static segmentResourceToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SegmentResource;I)Landroid/content/ContentValues;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentResource"    # Lcom/google/android/apps/books/model/SegmentResource;
    .param p3, "resourceIndexInSegment"    # I

    .prologue
    .line 249
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 250
    .local v0, "result":Landroid/content/ContentValues;
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v1, "segment_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/SegmentResource;->getSegmentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v1, "resource_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/SegmentResource;->getResourceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v1, "css_class"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/SegmentResource;->getCssClass()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v1, "title"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/SegmentResource;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v1, "resource_order"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 257
    return-object v0
.end method

.method public static segmentToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;I)Landroid/content/ContentValues;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p3, "index"    # I

    .prologue
    .line 158
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 159
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "account_name"

    invoke-virtual {v1, v2, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v2, "volume_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v2, "segment_order"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 162
    const-string v2, "segment_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v2, "title"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v2, "start_position"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v2, "page_count"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getPageCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 167
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 174
    .local v0, "contentStatus":I
    :goto_0
    const-string v2, "content_status"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 175
    const-string v2, "fixed_layout_version"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getFixedLayoutVersion()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 176
    const-string v2, "fixed_viewport_width"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 177
    const-string v2, "fixed_viewport_height"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getFixedViewportHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 178
    const-string v2, "remote_url"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-object v1

    .line 169
    .end local v0    # "contentStatus":I
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 170
    const/4 v0, 0x2

    .restart local v0    # "contentStatus":I
    goto :goto_0

    .line 172
    .end local v0    # "contentStatus":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "contentStatus":I
    goto :goto_0
.end method

.method public static volumeDataToContentValues(Lcom/google/android/apps/books/model/VolumeData;)Landroid/content/ContentValues;
    .locals 10
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 48
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 49
    .local v4, "result":Landroid/content/ContentValues;
    const-string v5, "volume_id"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 53
    const-string v5, "version"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getEtag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_0
    const-string v8, "title"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_8

    const-string v5, ""

    :goto_0
    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "author":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 59
    const-string v5, "creator"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_1
    const-string v5, "publisher"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getPublisher()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v5, "date"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v5, "description"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 67
    const-string v5, "language"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_2
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getServerCoverUri()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "coverUri":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 72
    const-string v5, "cover_url"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_3
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 76
    const-string v5, "position"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_4
    const-string v5, "last_access"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 81
    const-string v8, "is_uploaded"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v5

    if-eqz v5, :cond_9

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 83
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 84
    const-string v5, "buy_url"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_5
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "rentalState":Ljava/lang/String;
    if-nez v3, :cond_a

    .line 89
    const-string v5, "rental_state"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 90
    const-string v5, "rental_start"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 91
    const-string v5, "rental_expiration"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 104
    :goto_2
    const-string v5, "open_access"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getOldStyleOpenAccessValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v5, "viewability"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 108
    const-string v5, "tts_permission"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_6
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 112
    const-string v5, "canonical_url"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_7
    const-string v8, "explicit_offline_license"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v5

    if-eqz v5, :cond_b

    move v5, v6

    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 118
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v2

    .line 119
    .local v2, "maxDownloadDevices":I
    const v5, 0x7fffffff

    if-ne v2, v5, :cond_c

    .line 120
    const-string v5, "max_offline_devices"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 125
    :goto_4
    const-string v5, "quote_sharing_allowed"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isQuoteSharingAllowed()Z

    move-result v8

    if-eqz v8, :cond_d

    :goto_5
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 128
    return-object v4

    .line 55
    .end local v0    # "author":Ljava/lang/String;
    .end local v1    # "coverUri":Ljava/lang/String;
    .end local v2    # "maxDownloadDevices":I
    .end local v3    # "rentalState":Ljava/lang/String;
    :cond_8
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .restart local v0    # "author":Ljava/lang/String;
    .restart local v1    # "coverUri":Ljava/lang/String;
    :cond_9
    move v5, v7

    .line 81
    goto/16 :goto_1

    .line 93
    .restart local v3    # "rentalState":Ljava/lang/String;
    :cond_a
    const-string v5, "rental_state"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v5, "rental_start"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalStart()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 95
    const-string v5, "rental_expiration"

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_2

    :cond_b
    move v5, v7

    .line 115
    goto :goto_3

    .line 122
    .restart local v2    # "maxDownloadDevices":I
    :cond_c
    const-string v5, "max_offline_devices"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_4

    :cond_d
    move v6, v7

    .line 125
    goto :goto_5
.end method
