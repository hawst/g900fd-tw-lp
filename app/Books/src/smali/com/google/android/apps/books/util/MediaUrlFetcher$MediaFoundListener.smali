.class public interface abstract Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;
.super Ljava/lang/Object;
.source "MediaUrlFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/MediaUrlFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaFoundListener"
.end annotation


# virtual methods
.method public abstract onFileFound(Ljava/io/FileDescriptor;)V
.end method

.method public abstract onUrlFetchSucceeded(Ljava/lang/String;)V
.end method

.method public abstract onUrlFetchedError()V
.end method
