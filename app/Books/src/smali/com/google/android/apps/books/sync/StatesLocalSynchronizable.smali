.class public Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;
.super Lcom/google/android/apps/books/sync/BaseStatesSynchronizable;
.source "StatesLocalSynchronizable.java"


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "callerIsSyncAdapter"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/sync/BaseStatesSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 39
    return-void
.end method


# virtual methods
.method public extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "oldValues"    # Landroid/content/ContentValues;
    .param p2, "newValues"    # Landroid/content/ContentValues;

    .prologue
    .line 51
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;->checkValues(Landroid/content/ContentValues;)V

    .line 52
    invoke-static {p1, p2}, Lcom/google/android/apps/books/sync/SyncUtil;->extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    .line 53
    .local v0, "result":Landroid/content/ContentValues;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "StatesLocalSync"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    const-string v1, "StatesLocalSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updates: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    return-object v0
.end method

.method public update(Landroid/content/ContentValues;Landroid/content/ContentValues;)I
    .locals 5
    .param p1, "oldValues"    # Landroid/content/ContentValues;
    .param p2, "updateValues"    # Landroid/content/ContentValues;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;->getItemUri(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 44
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, "last_access"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "oldTime":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v1, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    .line 46
    .local v1, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;->getResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "last_access=?"

    invoke-virtual {v3, v2, p2, v4, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method
