.class public Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
.super Ljava/lang/Object;
.source "SyncVolumeLicensesResponse.java"


# instance fields
.field private final contentVersions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final volumesInMyEbooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->volumesInMyEbooks:Ljava/util/List;

    .line 16
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->contentVersions:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addVolume(Lcom/google/android/apps/books/model/VolumeData;Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "contentVersion"    # Ljava/lang/String;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->volumesInMyEbooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    if-eqz p2, :cond_0

    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->contentVersions:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    :cond_0
    return-void
.end method

.method public getContentVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->contentVersions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getVolumes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->volumesInMyEbooks:Ljava/util/List;

    return-object v0
.end method
