.class Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;
.super Ljava/lang/Object;
.source "LocalDictionarySubController.java"

# interfaces
.implements Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/LocalDictionarySubController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressListener"
.end annotation


# instance fields
.field mLastProgress:I

.field final mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

.field final mNetworkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

.field final synthetic this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p2, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    .param p3, "networkTaskServices"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mLastProgress:I

    .line 410
    iput-object p2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 411
    iput-object p3, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mNetworkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    .line 412
    return-void
.end method


# virtual methods
.method public bytesTransferred(J)V
    .locals 7
    .param p1, "byteCount"    # J

    .prologue
    .line 416
    const-wide/16 v2, 0x64

    mul-long/2addr v2, p1

    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    invoke-virtual {v1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getDictionarySizeInBytes()J

    move-result-wide v4

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 419
    .local v0, "currentProgress":I
    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 420
    const/16 v0, 0x64

    .line 422
    :cond_0
    iget v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mLastProgress:I

    if-le v0, v1, :cond_1

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mMetadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    iget-object v3, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mNetworkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    # invokes: Lcom/google/android/apps/books/data/LocalDictionarySubController;->updateDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;ILcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$700(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;ILcom/google/android/apps/books/data/NetworkTaskServices;)V

    .line 424
    iput v0, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$ProgressListener;->mLastProgress:I

    .line 426
    :cond_1
    return-void
.end method
