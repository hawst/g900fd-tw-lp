.class Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;
.super Ljava/lang/Object;
.source "GridRowInvalidationManager.java"

# interfaces
.implements Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->invalidateLists()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 109
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mInvalidatePending:Z
    invoke-static {v2, v3}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$402(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;Z)Z

    .line 110
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mDestroyed:Z
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$500(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    :cond_0
    return-void

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->mManagedViews:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->access$600(Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 114
    .local v1, "listView":Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-virtual {v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->invalidateViews()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    const-string v0, "GridRowInvalidationManager.invalidateViews"

    return-object v0
.end method
