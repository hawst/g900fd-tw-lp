.class Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

.field final synthetic val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iput-object p2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    .line 266
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$requestId:I

    iget-object v4, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v4, v4, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    iget-object v4, v4, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mRequestId:I
    invoke-static {v4}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$000(Lcom/google/android/apps/books/app/AudioClipPlayer;)I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 267
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 268
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/ParcelFileDescriptor;

    .line 270
    .local v2, "pfd":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    .end local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    :cond_0
    :goto_0
    return-void

    .line 271
    .restart local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "AudioClipPlayer"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 273
    const-string v3, "AudioClipPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error closing unused MO file descriptor "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v5, v5, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$clip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;
    invoke-static {v5}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->access$100(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 281
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    :cond_1
    const/4 v1, 0x0

    .line 282
    .local v1, "exception":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 284
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/ParcelFileDescriptor;

    .line 285
    .restart local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    # invokes: Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->access$200(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 286
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 294
    .end local v2    # "pfd":Landroid/os/ParcelFileDescriptor;
    :goto_1
    if-nez v1, :cond_3

    .line 295
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    # invokes: Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->getPlayer()Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->access$200(Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepareAsync()V

    goto :goto_0

    .line 287
    :catch_1
    move-exception v0

    .line 288
    .local v0, "e":Ljava/lang/Exception;
    move-object v1, v0

    .line 289
    goto :goto_1

    .line 291
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->val$finalResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    goto :goto_1

    .line 299
    :cond_3
    const-string v3, "AudioClipPlayer"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 300
    const-string v3, "AudioClipPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error retrieving "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v5, v5, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$clip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->mResource:Lcom/google/android/apps/books/model/Resource;
    invoke-static {v5}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->access$100(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 303
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z
    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$302(Lcom/google/android/apps/books/app/AudioClipPlayer;Z)Z

    .line 304
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$clip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->makeClipSilent()V

    .line 305
    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->this$1:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    iget-object v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    iget-object v4, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1$1;->this$2:Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;

    iget-object v4, v4, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer$1;->val$clip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    # invokes: Lcom/google/android/apps/books/app/AudioClipPlayer;->playSilentClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$400(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    goto/16 :goto_0
.end method
