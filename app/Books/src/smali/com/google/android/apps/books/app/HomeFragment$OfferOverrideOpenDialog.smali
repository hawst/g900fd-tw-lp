.class public Lcom/google/android/apps/books/app/HomeFragment$OfferOverrideOpenDialog;
.super Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OfferOverrideOpenDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2407
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 2425
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$OfferOverrideOpenDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->consumeTransitionView()Landroid/view/View;

    .line 2426
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 2427
    return-void
.end method

.method protected onOkClicked(Z)V
    .locals 6
    .param p1, "checkboxChecked"    # Z

    .prologue
    .line 2410
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$OfferOverrideOpenDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 2411
    .local v2, "args":Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 2412
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 2413
    .local v4, "volumeId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$OfferOverrideOpenDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2414
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_0

    .line 2415
    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->consumeTransitionView()Landroid/view/View;

    move-result-object v3

    .line 2418
    .local v3, "pressedView":Landroid/view/View;
    sget-object v5, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_LIBRARY_VIEW:Lcom/google/android/apps/books/app/BookOpeningFlags;

    invoke-static {v1, v0, v4, v5, v3}, Lcom/google/android/apps/books/app/HomeFragment;->openBook(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    .line 2421
    .end local v3    # "pressedView":Landroid/view/View;
    :cond_0
    return-void
.end method
