.class Lcom/google/android/apps/books/app/ReaderMenuImpl$3;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 370
    const-string v0, "ReaderMenu"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    const-string v0, "ReaderMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFocusChange() with hasFocus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_0
    if-nez p2, :cond_1

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/ReaderMenuImpl;->dismissRecentSearchesPopup()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$400(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$3;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$200(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;->onSearchFieldFocusChanged(Z)V

    .line 377
    return-void
.end method
