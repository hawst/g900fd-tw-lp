.class public Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
.super Ljava/lang/Object;
.source "VolumeWithMaybeVersion.java"


# instance fields
.field public final contentVersion:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "contentVersion"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->volumeId:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public asVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 31
    instance-of v2, p1, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    if-nez v2, :cond_1

    .line 35
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 34
    check-cast v0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    .line 35
    .local v0, "vv":Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->volumeId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->volumeId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->volumeId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "volumeId"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "contentVersion"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;->contentVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
