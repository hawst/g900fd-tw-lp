.class public Lcom/google/android/apps/books/api/TranslationServerController;
.super Ljava/lang/Object;
.source "TranslationServerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;,
        Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private mCachedLanguages:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCachedLanguagesCode:Ljava/lang/String;

.field private final mServer:Lcom/google/android/apps/books/api/TranslationServer;

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/api/TranslationServer;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "server"    # Lcom/google/android/apps/books/api/TranslationServer;
    .param p2, "uiThreadExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "backgroundExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mServer:Lcom/google/android/apps/books/api/TranslationServer;

    .line 45
    iput-object p2, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    .line 46
    iput-object p3, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/api/TranslationServerController;)Lcom/google/android/apps/books/api/TranslationServer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/api/TranslationServerController;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mServer:Lcom/google/android/apps/books/api/TranslationServer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/api/TranslationServerController;Lcom/google/common/collect/BiMap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/api/TranslationServerController;
    .param p1, "x1"    # Lcom/google/common/collect/BiMap;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/api/TranslationServerController;->setCachedLanguages(Lcom/google/common/collect/BiMap;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/api/TranslationServerController;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/api/TranslationServerController;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method private getCachedLanguages()Lcom/google/common/collect/BiMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mCachedLanguagesCode:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mCachedLanguages:Lcom/google/common/collect/BiMap;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mCachedLanguages:Lcom/google/common/collect/BiMap;

    return-object v0
.end method

.method private setCachedLanguages(Lcom/google/common/collect/BiMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "cache":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mCachedLanguages:Lcom/google/common/collect/BiMap;

    .line 149
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mCachedLanguagesCode:Ljava/lang/String;

    .line 150
    return-void
.end method


# virtual methods
.method public discoverLanguages(Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/apps/books/api/TranslationServerController;->getCachedLanguages()Lcom/google/common/collect/BiMap;

    move-result-object v0

    .line 55
    .local v0, "cache":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    .line 58
    if-eqz p1, :cond_0

    .line 59
    invoke-interface {p1, v0}, Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;->onCompleted(Lcom/google/common/collect/BiMap;)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/books/api/TranslationServerController$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/api/TranslationServerController$1;-><init>(Lcom/google/android/apps/books/api/TranslationServerController;Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public translateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;)V
    .locals 7
    .param p1, "sourceLanguage"    # Ljava/lang/String;
    .param p2, "targetLanguage"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/CharSequence;
    .param p4, "callback"    # Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;

    .prologue
    .line 105
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const/4 v0, 0x0

    invoke-interface {p4, v0, p3}, Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;->onCompleted(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 140
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/api/TranslationServerController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/books/api/TranslationServerController$2;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/api/TranslationServerController$2;-><init>(Lcom/google/android/apps/books/api/TranslationServerController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/books/api/TranslationServerController$TranslateTextCallback;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
