.class Lcom/google/android/apps/books/data/UploadsControllerImpl$5$2;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$5;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$5;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$5$2;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$5$2;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$5;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$5;->val$id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$5$2;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$5;

    iget-object v1, v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$5;->val$status:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/books/model/BooksDataListener;->onUploadStatusUpdate(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 226
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 222
    check-cast p1, Lcom/google/android/apps/books/model/BooksDataListener;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$5$2;->take(Lcom/google/android/apps/books/model/BooksDataListener;)V

    return-void
.end method
