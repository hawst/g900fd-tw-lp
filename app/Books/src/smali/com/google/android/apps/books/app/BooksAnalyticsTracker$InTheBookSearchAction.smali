.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InTheBookSearchAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum ACTION_BAR_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum DEVICE_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SCRUBBER_EXIT_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SCRUBBER_MATCH_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SCRUBBER_NEXT_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SCRUBBER_PREVIOUS_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SEARCH_COMPLETE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SELECT_RESULT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

.field public static final enum SUBMIT_QUERY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 103
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "ACTION_BAR_SEARCH_BUTTON"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->ACTION_BAR_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 107
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "DEVICE_SEARCH_BUTTON"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->DEVICE_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 111
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SUBMIT_QUERY"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SUBMIT_QUERY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 115
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SEARCH_COMPLETE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SEARCH_COMPLETE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 119
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SELECT_RESULT"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SELECT_RESULT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 123
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SCRUBBER_PREVIOUS_ARROW_CLICKED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_PREVIOUS_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 127
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SCRUBBER_NEXT_ARROW_CLICKED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_NEXT_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 131
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SCRUBBER_EXIT_CLICKED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_EXIT_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 135
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const-string v1, "SCRUBBER_MATCH_CLICKED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_MATCH_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    .line 99
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->ACTION_BAR_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->DEVICE_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SUBMIT_QUERY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SEARCH_COMPLETE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SELECT_RESULT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_PREVIOUS_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_NEXT_ARROW_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_EXIT_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_MATCH_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 99
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    return-object v0
.end method
