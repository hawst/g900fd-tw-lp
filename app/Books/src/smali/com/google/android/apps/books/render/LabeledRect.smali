.class public Lcom/google/android/apps/books/render/LabeledRect;
.super Ljava/lang/Object;
.source "LabeledRect.java"

# interfaces
.implements Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;
.implements Lcom/google/android/apps/books/widget/Assignable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;",
        "Lcom/google/android/apps/books/widget/Assignable",
        "<",
        "Lcom/google/android/apps/books/render/LabeledRect;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public rect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;Ljava/lang/String;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    .line 23
    iput-object p2, p0, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public assign(Lcom/google/android/apps/books/render/LabeledRect;)Lcom/google/android/apps/books/render/LabeledRect;
    .locals 2
    .param p1, "newLabeledRect"    # Lcom/google/android/apps/books/render/LabeledRect;

    .prologue
    .line 40
    iget-object v0, p1, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    iget-object v1, p1, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/render/LabeledRect;->set(Landroid/graphics/Rect;Ljava/lang/String;)Lcom/google/android/apps/books/render/LabeledRect;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic assign(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/apps/books/render/LabeledRect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/LabeledRect;->assign(Lcom/google/android/apps/books/render/LabeledRect;)Lcom/google/android/apps/books/render/LabeledRect;

    move-result-object v0

    return-object v0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public set(Landroid/graphics/Rect;Ljava/lang/String;)Lcom/google/android/apps/books/render/LabeledRect;
    .locals 0
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    .line 30
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "rect"

    iget-object v3, p0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    const-string v2, "label"

    iget-object v3, p0, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    .line 47
    .local v0, "helper":Lcom/google/common/base/Objects$ToStringHelper;
    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
