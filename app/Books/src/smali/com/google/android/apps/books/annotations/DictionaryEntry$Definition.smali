.class public Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/DictionaryEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Definition"
.end annotation


# instance fields
.field public final examples:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;"
        }
    .end annotation
.end field

.field public final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p2, "examples":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;->text:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;->examples:Ljava/util/List;

    .line 88
    return-void
.end method
