.class public final Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DismissedRecommendation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;",
        "Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 403
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->buildParsed()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .locals 1

    .prologue
    .line 397
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 448
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->buildPartial()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .locals 3

    .prologue
    .line 406
    new-instance v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;-><init>()V

    .line 407
    .local v0, "builder":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    new-instance v1, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;-><init>(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    .line 408
    return-object v0
.end method


# virtual methods
.method public addDismissedRecs(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .locals 2
    .param p1, "value"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .prologue
    .line 524
    if-nez p1, :cond_0

    .line 525
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 527
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 528
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$802(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;Ljava/util/List;)Ljava/util/List;

    .line 530
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    return-object p0
.end method

.method public build()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 439
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->buildPartial()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    .locals 3

    .prologue
    .line 452
    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    if-nez v1, :cond_0

    .line 453
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 456
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v1, v2, :cond_1

    .line 457
    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    iget-object v2, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    # setter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$802(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;Ljava/util/List;)Ljava/util/List;

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    .line 461
    .local v0, "returnMe":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    .line 462
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .locals 2

    .prologue
    .line 425
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->mergeFrom(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    .prologue
    .line 466
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->getDefaultInstance()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-object p0

    .line 467
    :cond_1
    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$802(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;Ljava/util/List;)Ljava/util/List;

    .line 471
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->dismissedRecs_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->access$800(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 481
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 482
    .local v1, "tag":I
    sparse-switch v1, :sswitch_data_0

    .line 486
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 487
    :sswitch_0
    return-object p0

    .line 492
    :sswitch_1
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->newBuilder()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    .line 493
    .local v0, "subBuilder":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 494
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->buildPartial()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->addDismissedRecs(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    goto :goto_0

    .line 482
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 397
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    return-object v0
.end method
