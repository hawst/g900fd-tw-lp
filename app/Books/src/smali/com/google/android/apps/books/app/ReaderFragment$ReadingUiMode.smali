.class public final enum Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
.super Ljava/lang/Enum;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReadingUiMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

.field public static final enum FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

.field public static final enum GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

.field public static final enum SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;


# instance fields
.field final showActionBarShadow:Z

.field final showScrubber:Z

.field final showSystemUi:Z


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 361
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const-string v1, "FULL"

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 362
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const-string v4, "SKIM"

    move v6, v5

    move v7, v5

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v3, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 363
    new-instance v6, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const-string v7, "GRID"

    move v8, v12

    move v9, v5

    move v10, v2

    move v11, v2

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v6, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 360
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->GRID:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    aput-object v1, v0, v12

    sput-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->$VALUES:[Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 0
    .param p3, "showSystemUi"    # Z
    .param p4, "showScrubber"    # Z
    .param p5, "showActionBarShadow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)V"
        }
    .end annotation

    .prologue
    .line 369
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 370
    iput-boolean p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showSystemUi:Z

    .line 371
    iput-boolean p4, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showScrubber:Z

    .line 372
    iput-boolean p5, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showActionBarShadow:Z

    .line 373
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 360
    const-class v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1

    .prologue
    .line 360
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->$VALUES:[Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    return-object v0
.end method
