.class public Lcom/google/android/apps/books/data/VolumeCoverSubcontroller;
.super Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
.source "VolumeCoverSubcontroller.java"


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "coverHeight"    # I

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;-><init>(I)V

    .line 9
    return-void
.end method


# virtual methods
.method protected getCoverFile(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 1
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-interface {p1, p2}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolumeCoverFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method
