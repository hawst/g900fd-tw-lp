.class public Lcom/google/android/apps/books/app/data/JsonSaleInfo$Offer;
.super Ljava/lang/Object;
.source "JsonSaleInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/data/JsonSaleInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Offer"
.end annotation


# instance fields
.field public finskyOfferType:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "finskyOfferType"
    .end annotation
.end field

.field public listPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "listPrice"
    .end annotation
.end field

.field public rentalDuration:Lcom/google/android/apps/books/app/data/JsonSaleInfo$Duration;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "rentalDuration"
    .end annotation
.end field

.field public retailPrice:Lcom/google/android/apps/books/app/data/JsonSaleInfo$OfferPrice;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "retailPrice"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
