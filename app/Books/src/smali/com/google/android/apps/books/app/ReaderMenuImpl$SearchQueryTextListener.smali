.class Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchQueryTextListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Lcom/google/android/apps/books/app/ReaderMenuImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderMenuImpl$1;

    .prologue
    .line 393
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 2
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mRecentSearches:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$500(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Ljava/util/List;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/app/ReaderMenuImpl;->showFilteredRecentSearchesPopup(Ljava/util/List;Ljava/lang/String;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$600(Lcom/google/android/apps/books/app/ReaderMenuImpl;Ljava/util/List;Ljava/lang/String;)V

    .line 407
    const/4 v0, 0x1

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 396
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SUBMIT_QUERY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->clearSearchViewFocus()V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$200(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;->onSearchQuerySubmitted(Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchQueryTextListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/ReaderMenuImpl;->dismissRecentSearchesPopup()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$400(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    .line 401
    const/4 v0, 0x1

    return v0
.end method
