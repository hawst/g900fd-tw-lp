.class public Lcom/google/android/apps/books/util/MathUtils;
.super Ljava/lang/Object;
.source "MathUtils.java"


# direct methods
.method public static compare(II)I
    .locals 1
    .param p0, "v1"    # I
    .param p1, "v2"    # I

    .prologue
    .line 195
    if-ne p0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-le p0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static constrain(FFF)F
    .locals 1
    .param p0, "amount"    # F
    .param p1, "low"    # F
    .param p2, "high"    # F

    .prologue
    .line 37
    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    .end local p1    # "low":F
    :goto_0
    return p1

    .restart local p1    # "low":F
    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method public static constrain(III)I
    .locals 0
    .param p0, "amount"    # I
    .param p1, "low"    # I
    .param p2, "high"    # I

    .prologue
    .line 33
    if-ge p0, p1, :cond_0

    .end local p1    # "low":I
    :goto_0
    return p1

    .restart local p1    # "low":I
    :cond_0
    if-le p0, p2, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method public static divideRoundingDown(II)I
    .locals 1
    .param p0, "n"    # I
    .param p1, "d"    # I

    .prologue
    .line 204
    if-ltz p0, :cond_0

    .line 205
    div-int v0, p0, p1

    .line 207
    :goto_0
    return v0

    :cond_0
    xor-int/lit8 v0, p0, -0x1

    div-int/2addr v0, p1

    xor-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public static fitIntoEvenHorizontal(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p0, "ioSize"    # Landroid/graphics/Point;
    .param p1, "targetSize"    # Landroid/graphics/Point;

    .prologue
    .line 177
    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 178
    .local v1, "targetAspectRatio":F
    iget v2, p0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, p0, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 180
    .local v0, "originalAspectRatio":F
    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    .line 182
    iget v2, p1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Lcom/google/android/apps/books/util/MathUtils;->roundToEven(F)I

    move-result v2

    iput v2, p0, Landroid/graphics/Point;->x:I

    .line 183
    iget v2, p1, Landroid/graphics/Point;->y:I

    iput v2, p0, Landroid/graphics/Point;->y:I

    .line 189
    :goto_0
    return-void

    .line 186
    :cond_0
    iget v2, p1, Landroid/graphics/Point;->x:I

    iput v2, p0, Landroid/graphics/Point;->x:I

    .line 187
    iget v2, p1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    invoke-static {v2}, Lcom/google/android/apps/books/util/MathUtils;->round(F)I

    move-result v2

    iput v2, p0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public static getBoundingRect(Ljava/util/List;)Landroid/graphics/Rect;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;)",
            "Landroid/graphics/Rect;"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "rectangles":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 69
    :cond_0
    const/4 v2, 0x0

    .line 80
    :cond_1
    return-object v2

    .line 71
    :cond_2
    const/4 v2, 0x0

    .line 72
    .local v2, "result":Landroid/graphics/Rect;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 73
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 74
    if-nez v2, :cond_4

    .line 75
    new-instance v2, Landroid/graphics/Rect;

    .end local v2    # "result":Landroid/graphics/Rect;
    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 77
    .restart local v2    # "result":Landroid/graphics/Rect;
    :cond_4
    invoke-virtual {v2, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public static manhattanDistanceFromPointToRect(IILandroid/graphics/Rect;)I
    .locals 2
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 98
    const/4 v0, 0x0

    .line 99
    .local v0, "distance":I
    iget v1, p2, Landroid/graphics/Rect;->left:I

    if-ge p0, v1, :cond_2

    .line 100
    iget v1, p2, Landroid/graphics/Rect;->left:I

    sub-int v0, v1, p0

    .line 104
    :cond_0
    :goto_0
    iget v1, p2, Landroid/graphics/Rect;->top:I

    if-ge p1, v1, :cond_3

    .line 105
    iget v1, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p1

    add-int/2addr v0, v1

    .line 109
    :cond_1
    :goto_1
    return v0

    .line 101
    :cond_2
    iget v1, p2, Landroid/graphics/Rect;->right:I

    if-le p0, v1, :cond_0

    .line 102
    iget v1, p2, Landroid/graphics/Rect;->right:I

    sub-int v0, p0, v1

    goto :goto_0

    .line 106
    :cond_3
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    if-le p1, v1, :cond_1

    .line 107
    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    sub-int v1, p1, v1

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method public static round(F)I
    .locals 4
    .param p0, "f"    # F

    .prologue
    .line 153
    float-to-double v0, p0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public static roundToEven(F)I
    .locals 2
    .param p0, "f"    # F

    .prologue
    .line 160
    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v1, p0

    float-to-int v0, v1

    .line 161
    .local v0, "roundUp":I
    and-int/lit8 v1, v0, 0x1

    xor-int/2addr v1, v0

    return v1
.end method

.method public static setScaleAndScrollInverseMatrix(Landroid/graphics/Matrix;FFF)V
    .locals 2
    .param p0, "matrix"    # Landroid/graphics/Matrix;
    .param p1, "scale"    # F
    .param p2, "scrollX"    # F
    .param p3, "scrollY"    # F

    .prologue
    .line 55
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    .line 56
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 57
    const/high16 v1, 0x3f800000    # 1.0f

    div-float v0, v1, p1

    .line 58
    .local v0, "scaleInv":F
    invoke-virtual {p0, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 59
    invoke-virtual {p0, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 61
    .end local v0    # "scaleInv":F
    :cond_0
    return-void
.end method

.method public static setScaleAndScrollMatrix(Landroid/graphics/Matrix;FFF)V
    .locals 2
    .param p0, "matrix"    # Landroid/graphics/Matrix;
    .param p1, "scale"    # F
    .param p2, "scrollX"    # F
    .param p3, "scrollY"    # F

    .prologue
    .line 45
    invoke-virtual {p0}, Landroid/graphics/Matrix;->reset()V

    .line 46
    invoke-virtual {p0, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 47
    neg-float v0, p2

    neg-float v1, p3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 48
    return-void
.end method

.method public static squaredDistanceBetweenPoints(IIII)J
    .locals 8
    .param p0, "x1"    # I
    .param p1, "y1"    # I
    .param p2, "x2"    # I
    .param p3, "y2"    # I

    .prologue
    .line 142
    sub-int v4, p2, p0

    int-to-long v0, v4

    .line 143
    .local v0, "dX":J
    sub-int v4, p3, p1

    int-to-long v2, v4

    .line 144
    .local v2, "dY":J
    mul-long v4, v0, v0

    mul-long v6, v2, v2

    add-long/2addr v4, v6

    return-wide v4
.end method

.method public static squaredDistanceFromPointToRect(IILandroid/graphics/Rect;)J
    .locals 8
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 118
    iget v4, p2, Landroid/graphics/Rect;->left:I

    if-ge p0, v4, :cond_0

    .line 119
    iget v4, p2, Landroid/graphics/Rect;->left:I

    sub-int v4, p0, v4

    int-to-long v0, v4

    .line 127
    .local v0, "dX":J
    :goto_0
    iget v4, p2, Landroid/graphics/Rect;->top:I

    if-ge p1, v4, :cond_2

    .line 128
    iget v4, p2, Landroid/graphics/Rect;->top:I

    sub-int v4, p1, v4

    int-to-long v2, v4

    .line 135
    .local v2, "dY":J
    :goto_1
    mul-long v4, v0, v0

    mul-long v6, v2, v2

    add-long/2addr v4, v6

    return-wide v4

    .line 120
    .end local v0    # "dX":J
    .end local v2    # "dY":J
    :cond_0
    iget v4, p2, Landroid/graphics/Rect;->right:I

    if-le p0, v4, :cond_1

    .line 121
    iget v4, p2, Landroid/graphics/Rect;->right:I

    sub-int v4, p0, v4

    int-to-long v0, v4

    .restart local v0    # "dX":J
    goto :goto_0

    .line 123
    .end local v0    # "dX":J
    :cond_1
    const-wide/16 v0, 0x0

    .restart local v0    # "dX":J
    goto :goto_0

    .line 129
    :cond_2
    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    if-le p1, v4, :cond_3

    .line 130
    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    sub-int v4, p1, v4

    int-to-long v2, v4

    .restart local v2    # "dY":J
    goto :goto_1

    .line 132
    .end local v2    # "dY":J
    :cond_3
    const-wide/16 v2, 0x0

    .restart local v2    # "dY":J
    goto :goto_1
.end method
