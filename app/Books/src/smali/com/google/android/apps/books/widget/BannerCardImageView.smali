.class public Lcom/google/android/apps/books/widget/BannerCardImageView;
.super Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;
.source "BannerCardImageView.java"


# instance fields
.field private mBanner:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->onFinishInflate()V

    .line 32
    const v0, 0x7f0e00e0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BannerCardImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    .line 33
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 55
    invoke-super/range {p0 .. p5}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->onLayout(ZIIII)V

    .line 56
    sub-int v1, p5, p3

    .line 57
    .local v1, "height":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BannerCardImageView;->getPaddingBottom()I

    move-result v5

    sub-int v0, v1, v5

    .line 59
    .local v0, "contentBottom":I
    iget-object v5, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-static {v5}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 60
    iget-object v5, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 61
    .local v2, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BannerCardImageView;->getPaddingLeft()I

    move-result v5

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v4, v5, v6

    .line 62
    .local v4, "thisChildLeft":I
    iget v5, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v0, v5

    .line 63
    .local v3, "thisChildBottom":I
    iget-object v5, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int v6, v3, v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v5, v4, v6, v7, v3}, Landroid/view/View;->layout(IIII)V

    .line 66
    .end local v2    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v3    # "thisChildBottom":I
    .end local v4    # "thisChildLeft":I
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 37
    invoke-super {p0, p1, p2}, Lcom/google/android/ublib/cardlib/layout/PlayCardThumbnail;->onMeasure(II)V

    .line 39
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 40
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BannerCardImageView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BannerCardImageView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v0, v2, v3

    .line 46
    .local v0, "contentWidth":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-static {v3}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->includeInLayout(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 48
    .local v1, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/BannerCardImageView;->mBanner:Landroid/view/View;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 51
    .end local v1    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    return-void
.end method
