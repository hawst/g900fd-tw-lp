.class Lcom/google/android/apps/books/app/ReaderFragment$18;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 3791
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accessDenied(I)V
    .locals 5
    .param p1, "maxDevices"    # I

    .prologue
    const/4 v4, 0x0

    .line 3796
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError()Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3809
    :cond_0
    :goto_0
    return-void

    .line 3799
    :cond_1
    const-string v1, "ReaderFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3800
    const-string v1, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing book because exceeded offline limit of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3802
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowErrorDialog()Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6700(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3805
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getConfig()Lcom/google/android/apps/books/util/Config;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4, v4}, Lcom/google/android/apps/books/util/OceanUris;->getOfflineLimitUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3807
    .local v0, "infoUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    new-instance v2, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;

    invoke-direct {v2, p1, v0}, Lcom/google/android/apps/books/app/ConcurrentAccessLimitExceededFragment;-><init>(ILandroid/net/Uri;)V

    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE_LIMIT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->showFatalErrorDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6900(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public licenseError()V
    .locals 3

    .prologue
    .line 3819
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v2, "Error while acquiring license"

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7000(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V

    .line 3820
    return-void
.end method

.method public offlineAccessDenied()V
    .locals 3

    .prologue
    .line 3813
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$18;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v2, "Device offline while reading online-only"

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7000(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V

    .line 3815
    return-void
.end method
