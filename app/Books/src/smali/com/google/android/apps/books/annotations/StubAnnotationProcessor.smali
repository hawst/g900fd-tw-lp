.class public Lcom/google/android/apps/books/annotations/StubAnnotationProcessor;
.super Ljava/lang/Object;
.source "StubAnnotationProcessor.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationProcessor;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 0
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 10
    return-void
.end method

.method public delete(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 14
    return-void
.end method

.method public done()V
    .locals 0

    .prologue
    .line 26
    return-void
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public updateCharacterQuota(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    .locals 0
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "characterQuota"    # Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .prologue
    .line 18
    return-void
.end method
