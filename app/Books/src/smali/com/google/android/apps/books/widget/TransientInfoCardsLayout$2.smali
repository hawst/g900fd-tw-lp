.class Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;
.super Ljava/lang/Object;
.source "TransientInfoCardsLayout.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)V
    .locals 0

    .prologue
    .line 388
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Landroid/view/View;)V
    .locals 2
    .param p1, "t"    # Landroid/view/View;

    .prologue
    .line 391
    const v1, 0x7f0e00fc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 392
    .local v0, "backgroundView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 393
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;->this$0:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    # getter for: Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->mCardBackgroundAlpha:F
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->access$100(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 395
    :cond_0
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 388
    check-cast p1, Landroid/view/View;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$2;->take(Landroid/view/View;)V

    return-void
.end method
