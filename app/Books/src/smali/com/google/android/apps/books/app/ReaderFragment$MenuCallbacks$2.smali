.class Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->onItemSelected(Lcom/google/android/apps/books/app/ReaderMenu$Item;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;)V
    .locals 0

    .prologue
    .line 4217
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4220
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10702(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 4221
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTtsEnabled(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7600(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 4222
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->START_READ_ALOUD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStart(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V

    .line 4223
    return-void
.end method
