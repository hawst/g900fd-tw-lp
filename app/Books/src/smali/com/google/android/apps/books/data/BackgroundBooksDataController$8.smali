.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeData(Ljava/lang/String;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$fromServer:Z

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;ZLjava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-boolean p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$fromServer:Z

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$volumeId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 400
    iget-boolean v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$fromServer:Z

    if-nez v3, :cond_1

    .line 402
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$volumeId:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolume(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    .line 403
    .local v1, "fromDataStore":Lcom/google/android/apps/books/model/VolumeData;
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeData(Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    invoke-static {v3, v1, v4, v5, v6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 422
    .end local v1    # "fromDataStore":Lcom/google/android/apps/books/model/VolumeData;
    :cond_0
    :goto_0
    return-void

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "BgDataController"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 408
    const-string v3, "BgDataController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error loading local volume data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$volumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/apps/books/data/ConsumerMaps;->addConsumers(Ljava/lang/Object;Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v2

    .line 417
    .local v2, "startNewTask":Z
    if-eqz v2, :cond_0

    .line 421
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$volumeId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeDataOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_0
.end method
