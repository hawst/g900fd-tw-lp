.class Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;
.super Ljava/lang/Object;
.source "VolumeManifestParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/VolumeManifestParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ParsedResources"
.end annotation


# instance fields
.field private final mResourceIdToIndex:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mResources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResources:Ljava/util/List;

    .line 76
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResourceIdToIndex:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/net/VolumeManifestParser$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/net/VolumeManifestParser$1;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;-><init>()V

    return-void
.end method


# virtual methods
.method public addResource(Lcom/google/android/apps/books/model/Resource;)V
    .locals 3
    .param p1, "resource"    # Lcom/google/android/apps/books/model/Resource;

    .prologue
    .line 83
    iget-object v1, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResourceIdToIndex:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 84
    .local v0, "resourceIndex":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResources:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResources:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResourceIdToIndex:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_0
    return-void
.end method

.method public getResources()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->mResources:Ljava/util/List;

    return-object v0
.end method
