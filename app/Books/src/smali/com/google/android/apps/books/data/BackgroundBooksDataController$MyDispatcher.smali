.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;
.super Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDispatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/net/BooksServer;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/DataControllerStore;)V
    .locals 8
    .param p2, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p3, "server"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p4, "controlTaskExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p6, "sessionKeySubcontroller"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p7, "localDictionarySubController"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p8, "dataControllerStore"    # Lcom/google/android/apps/books/model/DataControllerStore;

    .prologue
    .line 1157
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    .line 1158
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;-><init>(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/net/BooksServer;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/DataControllerStore;)V

    .line 1160
    return-void
.end method


# virtual methods
.method public executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V
    .locals 1
    .param p1, "task"    # Lcom/google/android/apps/books/data/NetworkTask;

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkTasks:Lcom/google/android/apps/books/data/NetworkTaskQueue;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/NetworkTaskQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/NetworkTaskQueue;->addTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 1175
    return-void
.end method

.method public removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1164
    .local p1, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-super {p0, p1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 1166
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/LocalDictionarySubController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->clearCaches()V

    .line 1169
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onDictionaryMetadataInvalidated()V
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V

    .line 1170
    return-void
.end method

.method public scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V
    .locals 1
    .param p1, "task"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1180
    invoke-super {p0, p1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 1184
    :goto_0
    return-void

    .line 1182
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPendingActions:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
