.class public Lcom/google/android/apps/books/annotations/data/JsonAnnotationsSummaryResponse;
.super Ljava/lang/Object;
.source "JsonAnnotationsSummaryResponse.java"


# instance fields
.field public kind:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "kind"
    .end annotation
.end field

.field public layers:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "layers"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonLayer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
