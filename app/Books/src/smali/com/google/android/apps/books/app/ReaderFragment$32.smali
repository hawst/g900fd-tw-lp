.class Lcom/google/android/apps/books/app/ReaderFragment$32;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 6261
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private scale(IIIII)I
    .locals 2
    .param p1, "value"    # I
    .param p2, "min"    # I
    .param p3, "max"    # I
    .param p4, "newMin"    # I
    .param p5, "newMax"    # I

    .prologue
    .line 6285
    sub-int v0, p1, p2

    sub-int v1, p5, p4

    mul-int/2addr v0, v1

    sub-int v1, p3, p2

    div-int/2addr v0, v1

    add-int/2addr v0, p4

    return v0
.end method


# virtual methods
.method public done()V
    .locals 2

    .prologue
    .line 6432
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSettings:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13202(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 6434
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 6435
    return-void
.end method

.method public onBrightnessChanged(IZ)V
    .locals 16
    .param p1, "brightness"    # I
    .param p2, "byUser"    # Z

    .prologue
    .line 6297
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v15

    .line 6298
    .local v15, "window":Landroid/view/Window;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0e00ac

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 6300
    .local v9, "colorFilter":Landroid/view/View;
    const/4 v1, -0x1

    move/from16 v0, p1

    if-eq v0, v1, :cond_0

    if-nez v9, :cond_2

    .line 6301
    :cond_0
    move/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/android/apps/books/util/WindowUtils;->setBrightness(ILandroid/view/Window;)V

    .line 6302
    if-eqz v9, :cond_1

    .line 6303
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6335
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_BRIGHTNESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    move/from16 v0, p2

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6336
    return-void

    .line 6307
    :cond_2
    const/4 v14, 0x7

    .line 6308
    .local v14, "minBrightness":I
    const/16 v12, 0x64

    .line 6309
    .local v12, "maxBrightness":I
    const/16 v10, 0x35

    .line 6312
    .local v10, "filterThreshold":I
    const/4 v13, 0x0

    .line 6313
    .local v13, "minAlpha":I
    const/16 v11, 0xc8

    .line 6315
    .local v11, "maxAlpha":I
    const/16 v1, 0x35

    move/from16 v0, p1

    if-ge v0, v1, :cond_3

    .line 6319
    const/4 v1, 0x7

    invoke-static {v1, v15}, Lcom/google/android/apps/books/util/WindowUtils;->setBrightness(ILandroid/view/Window;)V

    .line 6321
    const/4 v3, 0x7

    const/16 v4, 0x35

    const/16 v5, 0xc8

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/app/ReaderFragment$32;->scale(IIIII)I

    move-result v8

    .line 6323
    .local v8, "alpha":I
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v8, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 6324
    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 6327
    .end local v8    # "alpha":I
    :cond_3
    const/16 v3, 0x35

    const/16 v4, 0x64

    const/4 v5, 0x7

    const/16 v6, 0x64

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/app/ReaderFragment$32;->scale(IIIII)I

    move-result v7

    .line 6329
    .local v7, "adjustedBrightness":I
    invoke-static {v7, v15}, Lcom/google/android/apps/books/util/WindowUtils;->setBrightness(ILandroid/view/Window;)V

    .line 6331
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onJustificationChanged(Z)V
    .locals 3
    .param p1, "byUser"    # Z

    .prologue
    .line 6351
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6352
    const-string v1, "ReaderFragment"

    const-string v2, "onJustificationChanged()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6355
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6356
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->applySettingsIfChanged(Landroid/content/Context;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13600(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V

    .line 6357
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_JUSTIFICATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6358
    return-void
.end method

.method public onLineHeightSettingChanged(FZ)V
    .locals 4
    .param p1, "newLineHeight"    # F
    .param p2, "byUser"    # Z

    .prologue
    const/4 v3, 0x3

    .line 6395
    const-string v1, "ReaderFragment"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6396
    const-string v1, "ReaderFragment"

    const-string v2, "onLineHeightSettingChanged()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6399
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 6400
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->setLineHeight(F)V

    .line 6403
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/google/android/apps/books/data/BooksDataController;->setLineHeight(Ljava/lang/String;F)V

    .line 6405
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6406
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->applySettingsIfChanged(Landroid/content/Context;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13600(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V

    .line 6410
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_LINE_HEIGHT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v1, v2, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6411
    return-void

    .line 6407
    :cond_2
    const-string v1, "ReaderFragment"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6408
    const-string v1, "ReaderFragment"

    const-string v2, "onLineHeightSettingChanged: no metadata"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPageLayoutChanged(ZZ)V
    .locals 3
    .param p1, "isFitWidth"    # Z
    .param p2, "byUser"    # Z

    .prologue
    .line 6415
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6416
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageLayoutChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6419
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isFitWidth()Z

    move-result v0

    if-eq p1, v0, :cond_1

    .line 6420
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_PAGE_LAYOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v0, v1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6423
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/data/BooksDataController;->setFitWidth(Ljava/lang/String;Z)V

    .line 6426
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->restartCurrentActivity(Z)V

    .line 6428
    :cond_1
    return-void
.end method

.method public onReadingModeChanged(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Z)V
    .locals 2
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .param p2, "byUser"    # Z

    .prologue
    .line 6272
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 6273
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 6275
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->saveReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 6277
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_READING_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v0, v1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6279
    :cond_0
    return-void
.end method

.method public onShowingSettings()V
    .locals 2

    .prologue
    .line 6265
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSettings:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13202(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 6267
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 6268
    return-void
.end method

.method public onTextZoomSettingChanged(FZ)V
    .locals 4
    .param p1, "newTextZoom"    # F
    .param p2, "byUser"    # Z

    .prologue
    const/4 v3, 0x3

    .line 6373
    const-string v1, "ReaderFragment"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6374
    const-string v1, "ReaderFragment"

    const-string v2, "onTextZoomSettingChanged()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6379
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 6380
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->setTextZoom(F)V

    .line 6383
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/google/android/apps/books/data/BooksDataController;->setTextZoom(Ljava/lang/String;F)V

    .line 6385
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6386
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->applySettingsIfChanged(Landroid/content/Context;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13600(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V

    .line 6390
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_TEXT_ZOOM:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v1, v2, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6391
    return-void

    .line 6387
    :cond_2
    const-string v1, "ReaderFragment"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6388
    const-string v1, "ReaderFragment"

    const-string v2, "onTextZoomSettingChanged: no metadata or no mValidTextZoomInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onThemeChanged(Z)V
    .locals 3
    .param p1, "byUser"    # Z

    .prologue
    .line 6340
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6341
    const-string v1, "ReaderFragment"

    const-string v2, "onThemeChanged()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6344
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6345
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->applySettingsIfChanged(Landroid/content/Context;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13600(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V

    .line 6346
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_THEME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6347
    return-void
.end method

.method public onTypefaceChanged(Z)V
    .locals 3
    .param p1, "byUser"    # Z

    .prologue
    .line 6362
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6363
    const-string v1, "ReaderFragment"

    const-string v2, "onTypefaceChanged()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6366
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6367
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->applySettingsIfChanged(Landroid/content/Context;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13600(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V

    .line 6368
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$32;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_TYPEFACE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    invoke-static {v1, v2, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 6369
    return-void
.end method
