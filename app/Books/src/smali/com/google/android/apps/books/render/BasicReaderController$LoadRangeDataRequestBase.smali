.class abstract Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "LoadRangeDataRequestBase"
.end annotation


# instance fields
.field final mNeedSelectionData:Z

.field final mPassageIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;IZIIZ)V
    .locals 6
    .param p2, "passageIndex"    # I
    .param p3, "needSelectionData"    # Z
    .param p4, "externalRequestId"    # I
    .param p5, "taskRequestId"    # I
    .param p6, "reposted"    # Z

    .prologue
    .line 1097
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1098
    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move v2, p5

    move v3, p4

    move v4, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1099
    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mPassageIndex:I

    .line 1100
    iput-boolean p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mNeedSelectionData:Z

    .line 1101
    return-void
.end method


# virtual methods
.method protected abstract createRepostRequest()Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;
.end method

.method protected abstract executeReaderCall()V
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x0

    .line 1115
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 1116
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mPassageIndex:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/ReaderDataModel;->isPassageReady(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1117
    const-string v1, "ReaderController"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1118
    const-string v1, "ReaderController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LoadRangeData: passage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mPassageIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " loaded, executing"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->executeReaderCall()V

    .line 1123
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$1;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;)V

    .line 1131
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnLoadedRangeDataHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$1700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mTaskRequestId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1148
    .end local v0    # "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataHandler;
    :goto_0
    return-void

    .line 1133
    :cond_1
    const-string v1, "ReaderController"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1134
    const-string v1, "ReaderController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LoadRangeData: passage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mPassageIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not loaded, loading"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    :cond_2
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase$2;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;)V

    .line 1144
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mTaskRequestId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1146
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mPassageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataRequestBase;->mTaskRequestId:I

    invoke-interface {v1, v2, v4, v4, v3}, Lcom/google/android/apps/books/render/TextReader;->loadPage(IIII)V

    goto :goto_0
.end method
