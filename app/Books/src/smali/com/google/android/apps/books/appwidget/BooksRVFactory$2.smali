.class Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;
.super Landroid/database/ContentObserver;
.source "BooksAppWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/appwidget/BooksRVFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/appwidget/BooksRVFactory;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method private different(Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "a":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    .local p2, "b":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 111
    if-nez p1, :cond_2

    .line 112
    if-eqz p2, :cond_1

    move v1, v2

    :goto_0
    move v2, v1

    .line 125
    :cond_0
    :goto_1
    return v2

    :cond_1
    move v1, v3

    .line 112
    goto :goto_0

    .line 114
    :cond_2
    if-eqz p2, :cond_0

    .line 117
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ne v1, v4, :cond_0

    .line 120
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 121
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/VolumeData;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move v2, v3

    .line 122
    goto :goto_1

    .line 120
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfUpdate"    # Z

    .prologue
    .line 89
    const-string v2, "BooksAppWidget"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    const-string v2, "BooksAppWidget"

    const-string v3, "onChange() triggered from provider"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    # invokes: Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadVolumeList()Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->access$100(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)Ljava/util/List;

    move-result-object v1

    .line 98
    .local v1, "volumeList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    # getter for: Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->access$200(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;->different(Ljava/util/List;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    # invokes: Lcom/google/android/apps/books/appwidget/BooksRVFactory;->update()V
    invoke-static {v2}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->access$000(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .end local v1    # "volumeList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    :cond_1
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "BooksAppWidget"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    const-string v2, "BooksAppWidget"

    const-string v3, "Exception while loading volume list"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
