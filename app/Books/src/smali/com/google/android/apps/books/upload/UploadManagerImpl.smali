.class public Lcom/google/android/apps/books/upload/UploadManagerImpl;
.super Ljava/lang/Object;
.source "UploadManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/upload/UploadManager;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mContext:Landroid/content/Context;

.field private final mScheduler:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p2, p0, Lcom/google/android/apps/books/upload/UploadManagerImpl;->mAccount:Landroid/accounts/Account;

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/upload/UploadManagerImpl;->mContext:Landroid/content/Context;

    .line 21
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/UploadManagerImpl;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 22
    return-void
.end method


# virtual methods
.method public getUploader(Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;Lcom/google/android/apps/books/upload/Upload;Lcom/google/android/apps/books/upload/UploadDataStorage;)Lcom/google/android/apps/books/upload/SingleBookUploader;
    .locals 7
    .param p1, "controller"    # Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    .param p2, "upload"    # Lcom/google/android/apps/books/upload/Upload;
    .param p3, "storage"    # Lcom/google/android/apps/books/upload/UploadDataStorage;

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/UploadManagerImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/books/upload/UploadManagerImpl;->mAccount:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/apps/books/upload/UploadManagerImpl;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;Lcom/google/android/apps/books/upload/Upload;Lcom/google/android/apps/books/upload/UploadDataStorage;Ljava/util/concurrent/ScheduledExecutorService;)V

    return-object v0
.end method
