.class public interface abstract Lcom/google/android/apps/books/annotations/BlobStore;
.super Ljava/lang/Object;
.source "BlobStore.java"


# virtual methods
.method public abstract get(Ljava/lang/String;)Ljava/io/InputStream;
.end method

.method public abstract save()V
.end method

.method public abstract set(Ljava/lang/String;)Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
