.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Z
    .locals 5

    .prologue
    .line 436
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;
    invoke-static {v2}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$2000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/libraries/bind/card/CardListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardListView;->getLastVisiblePosition()I

    move-result v0

    .line 437
    .local v0, "lastVisibleRow":I
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mCardListView:Lcom/google/android/libraries/bind/card/CardListView;
    invoke-static {v2}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$2100(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/libraries/bind/card/CardListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/card/CardListView;->getCount()I

    move-result v1

    .line 438
    .local v1, "totalRowCount":I
    const-string v2, "OnboardSampleQuizPage"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 439
    const-string v2, "OnboardSampleQuizPage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Maybe bulk display samples. lastVisibleRow="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", totalRowCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :cond_0
    add-int/lit8 v2, v0, 0x2

    if-lt v2, v1, :cond_1

    .line 451
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # invokes: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->bulkDisplayNewSamples()V
    invoke-static {v2}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$2200(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V

    .line 454
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSubsequentRowsToFetch:I
    invoke-static {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$2300(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$4;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-virtual {v4}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getNumColumns()I

    move-result v4

    mul-int/2addr v3, v4

    # invokes: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->maybeLoadMoreSamples(I)V
    invoke-static {v2, v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$2400(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;I)V

    .line 455
    const/4 v2, 0x1

    return v2
.end method
