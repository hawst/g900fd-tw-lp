.class public Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
.super Ljava/lang/Object;
.source "SubdividedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SubdividedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Subdivision"
.end annotation


# instance fields
.field public final adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

.field public final isTitleEnabled:Z

.field public final showTitleWhenOtherSubdivisionsAreEmpty:Z

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "adapter"    # Lcom/google/android/apps/books/widget/DescribingListAdapter;

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;Z)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;Z)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "adapter"    # Lcom/google/android/apps/books/widget/DescribingListAdapter;
    .param p3, "isTitleEnabled"    # Z

    .prologue
    .line 43
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;ZZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;ZZ)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "adapter"    # Lcom/google/android/apps/books/widget/DescribingListAdapter;
    .param p3, "isTitleEnabled"    # Z
    .param p4, "showTitleWhenAlone"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->title:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    .line 38
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->isTitleEnabled:Z

    .line 39
    iput-boolean p4, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->showTitleWhenOtherSubdivisionsAreEmpty:Z

    .line 40
    return-void
.end method
