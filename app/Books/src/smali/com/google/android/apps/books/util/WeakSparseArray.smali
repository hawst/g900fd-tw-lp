.class public Lcom/google/android/apps/books/util/WeakSparseArray;
.super Ljava/lang/Object;
.source "WeakSparseArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mRefs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/Reference",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/apps/books/util/WeakSparseArray;, "Lcom/google/android/apps/books/util/WeakSparseArray<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/apps/books/util/WeakSparseArray;, "Lcom/google/android/apps/books/util/WeakSparseArray<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 20
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/apps/books/util/WeakSparseArray;, "Lcom/google/android/apps/books/util/WeakSparseArray<TT;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/Reference;

    .line 28
    .local v0, "ref":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<TT;>;"
    if-nez v0, :cond_1

    .line 29
    const/4 v1, 0x0

    .line 38
    :cond_0
    :goto_0
    return-object v1

    .line 32
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 34
    .local v1, "value":Ljava/lang/Object;, "TT;"
    if-nez v1, :cond_0

    .line 35
    iget-object v2, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0
.end method

.method public put(ILjava/lang/Object;)V
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/apps/books/util/WeakSparseArray;, "Lcom/google/android/apps/books/util/WeakSparseArray<TT;>;"
    .local p2, "value":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 24
    return-void
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/apps/books/util/WeakSparseArray;, "Lcom/google/android/apps/books/util/WeakSparseArray<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/Reference;

    .line 43
    .local v0, "ref":Ljava/lang/ref/Reference;, "Ljava/lang/ref/Reference<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/util/WeakSparseArray;->mRefs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 44
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method
