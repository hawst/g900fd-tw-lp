.class Lcom/google/android/apps/books/view/pages/BezierPage;
.super Ljava/lang/Object;
.source "BezierPage.java"


# static fields
.field private static final sD:Landroid/graphics/PointF;

.field private static final sPrec:Landroid/graphics/PointF;

.field private static final sPt:Landroid/graphics/PointF;


# instance fields
.field private final bezierIndex:[I

.field private final bezierX:[F

.field private final beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

.field private final lengths:[F

.field private nb:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/BezierPage;->sPrec:Landroid/graphics/PointF;

    .line 43
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/BezierPage;->sPt:Landroid/graphics/PointF;

    .line 44
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/view/pages/BezierPage;->sD:Landroid/graphics/PointF;

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x65

    const/4 v1, 0x3

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-array v0, v1, [Lcom/google/android/apps/books/view/pages/Bezier;

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

    .line 20
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->lengths:[F

    .line 21
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->nb:I

    .line 25
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierX:[F

    .line 27
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierIndex:[I

    return-void
.end method


# virtual methods
.method addBezier(Lcom/google/android/apps/books/view/pages/Bezier;)V
    .locals 3
    .param p1, "b"    # Lcom/google/android/apps/books/view/pages/Bezier;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->nb:I

    aput-object p1, v0, v1

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->lengths:[F

    iget v1, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->nb:I

    invoke-virtual {p1}, Lcom/google/android/apps/books/view/pages/Bezier;->getLength()F

    move-result v2

    aput v2, v0, v1

    .line 32
    iget v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->nb:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->nb:I

    .line 33
    return-void
.end method

.method pointAt(FLandroid/graphics/PointF;)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "p"    # Landroid/graphics/PointF;

    .prologue
    .line 100
    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v2, p1

    float-to-double v2, v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    double-to-int v1, v2

    .line 103
    .local v1, "index":I
    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 104
    const/16 v1, 0x64

    .line 107
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierIndex:[I

    aget v0, v2, v1

    .line 108
    .local v0, "bzIndex":I
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierX:[F

    aget v3, v3, v1

    invoke-virtual {v2, v3, p2}, Lcom/google/android/apps/books/view/pages/Bezier;->pointAt(FLandroid/graphics/PointF;)V

    .line 109
    return-void
.end method

.method sampleDistances()V
    .locals 18

    .prologue
    .line 50
    const v3, 0x3c23d70a    # 0.01f

    .line 51
    .local v3, "delta":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    const/4 v14, 0x0

    sget-object v15, Lcom/google/android/apps/books/view/pages/BezierPage;->sPrec:Landroid/graphics/PointF;

    invoke-virtual {v13, v14, v15}, Lcom/google/android/apps/books/view/pages/Bezier;->pointAt(FLandroid/graphics/PointF;)V

    .line 52
    const/4 v2, 0x0

    .line 53
    .local v2, "currentIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierX:[F

    const/4 v14, 0x0

    const/4 v15, 0x0

    aput v15, v13, v14

    .line 54
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierIndex:[I

    const/4 v14, 0x0

    aput v2, v13, v14

    .line 55
    const/4 v7, 0x0

    .line 57
    .local v7, "prevLength":F
    const/4 v6, 0x1

    .local v6, "j":I
    :goto_0
    const/16 v13, 0x64

    if-gt v6, v13, :cond_3

    .line 58
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierX:[F

    add-int/lit8 v14, v6, -0x1

    aget v8, v13, v14

    .line 59
    .local v8, "prevT":F
    const v4, 0x3c23d70a    # 0.01f

    .line 60
    .local v4, "deltaT":F
    add-float v11, v8, v4

    .line 61
    .local v11, "t":F
    const v12, 0x3c23d70a    # 0.01f

    .line 63
    .local v12, "targetDist":F
    const/4 v13, 0x2

    if-ge v2, v13, :cond_0

    int-to-float v13, v6

    const v14, 0x3c23d70a    # 0.01f

    mul-float/2addr v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->lengths:[F

    aget v14, v14, v2

    add-float/2addr v14, v7

    cmpl-float v13, v13, v14

    if-lez v13, :cond_0

    .line 65
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->lengths:[F

    aget v13, v13, v2

    add-float/2addr v7, v13

    .line 66
    add-int/lit8 v13, v6, -0x1

    int-to-float v13, v13

    const v14, 0x3c23d70a    # 0.01f

    mul-float/2addr v13, v14

    sub-float v13, v7, v13

    sub-float/2addr v12, v13

    .line 67
    add-int/lit8 v2, v2, 0x1

    .line 68
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

    aget-object v13, v13, v2

    const/4 v14, 0x0

    sget-object v15, Lcom/google/android/apps/books/view/pages/BezierPage;->sPrec:Landroid/graphics/PointF;

    invoke-virtual {v13, v14, v15}, Lcom/google/android/apps/books/view/pages/Bezier;->pointAt(FLandroid/graphics/PointF;)V

    .line 69
    move v11, v4

    .line 71
    :cond_0
    const/4 v9, 0x0

    .line 73
    .local v9, "step":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->beziers:[Lcom/google/android/apps/books/view/pages/Bezier;

    aget-object v13, v13, v2

    sget-object v14, Lcom/google/android/apps/books/view/pages/BezierPage;->sPt:Landroid/graphics/PointF;

    invoke-virtual {v13, v11, v14}, Lcom/google/android/apps/books/view/pages/Bezier;->pointAt(FLandroid/graphics/PointF;)V

    .line 74
    sget-object v13, Lcom/google/android/apps/books/view/pages/BezierPage;->sD:Landroid/graphics/PointF;

    sget-object v14, Lcom/google/android/apps/books/view/pages/BezierPage;->sPt:Landroid/graphics/PointF;

    iget v14, v14, Landroid/graphics/PointF;->x:F

    sget-object v15, Lcom/google/android/apps/books/view/pages/BezierPage;->sPrec:Landroid/graphics/PointF;

    iget v15, v15, Landroid/graphics/PointF;->x:F

    sub-float/2addr v14, v15

    sget-object v15, Lcom/google/android/apps/books/view/pages/BezierPage;->sPt:Landroid/graphics/PointF;

    iget v15, v15, Landroid/graphics/PointF;->y:F

    sget-object v16, Lcom/google/android/apps/books/view/pages/BezierPage;->sPrec:Landroid/graphics/PointF;

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/graphics/PointF;->set(FF)V

    .line 75
    sget-object v13, Lcom/google/android/apps/books/view/pages/BezierPage;->sD:Landroid/graphics/PointF;

    invoke-virtual {v13}, Landroid/graphics/PointF;->length()F

    move-result v5

    .line 79
    .local v5, "dist":F
    sub-float v13, v5, v12

    div-float/2addr v13, v12

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    float-to-double v14, v13

    const-wide v16, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v13, v14, v16

    if-gez v13, :cond_1

    .line 88
    :goto_2
    sget-object v13, Lcom/google/android/apps/books/view/pages/BezierPage;->sPrec:Landroid/graphics/PointF;

    sget-object v14, Lcom/google/android/apps/books/view/pages/BezierPage;->sPt:Landroid/graphics/PointF;

    invoke-virtual {v13, v14}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierX:[F

    aput v11, v13, v6

    .line 90
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/view/pages/BezierPage;->bezierIndex:[I

    aput v2, v13, v6

    .line 57
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 80
    :cond_1
    cmpl-float v13, v5, v12

    if-lez v13, :cond_2

    .line 81
    float-to-double v14, v4

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    div-double v14, v14, v16

    double-to-float v4, v14

    .line 82
    sub-float/2addr v11, v4

    .line 86
    :goto_3
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "step":I
    .local v10, "step":I
    const/16 v13, 0x14

    if-lt v9, v13, :cond_4

    move v9, v10

    .end local v10    # "step":I
    .restart local v9    # "step":I
    goto :goto_2

    .line 84
    :cond_2
    add-float/2addr v11, v4

    goto :goto_3

    .line 92
    .end local v4    # "deltaT":F
    .end local v5    # "dist":F
    .end local v8    # "prevT":F
    .end local v9    # "step":I
    .end local v11    # "t":F
    .end local v12    # "targetDist":F
    :cond_3
    return-void

    .restart local v4    # "deltaT":F
    .restart local v5    # "dist":F
    .restart local v8    # "prevT":F
    .restart local v10    # "step":I
    .restart local v11    # "t":F
    .restart local v12    # "targetDist":F
    :cond_4
    move v9, v10

    .end local v10    # "step":I
    .restart local v9    # "step":I
    goto :goto_1
.end method
