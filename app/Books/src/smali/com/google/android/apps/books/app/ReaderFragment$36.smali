.class Lcom/google/android/apps/books/app/ReaderFragment$36;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 7715
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$36;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public make(Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;)Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;
    .locals 4
    .param p1, "callbacks"    # Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

    .prologue
    .line 7718
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$36;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getUseNetworkTts()Z

    move-result v0

    .line 7719
    .local v0, "useNetworkTts":Z
    new-instance v1, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$36;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$36;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getLocale()Ljava/util/Locale;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16200(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, p1, v0, v3}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;ZLjava/util/Locale;)V

    return-object v1
.end method
