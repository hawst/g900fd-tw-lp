.class public final Lcom/google/android/apps/books/model/ImmutableVolumeData;
.super Lcom/google/android/apps/books/model/BaseVolumeData;
.source "ImmutableVolumeData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutableVolumeData$1;,
        Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    }
.end annotation


# instance fields
.field private final mAuthor:Ljava/lang/String;

.field private final mBuyUrl:Ljava/lang/String;

.field private final mCanonicalUrl:Ljava/lang/String;

.field private final mDate:Ljava/lang/String;

.field private final mDescription:Ljava/lang/String;

.field private final mEtag:Ljava/lang/String;

.field private final mIsLimitedPreview:Z

.field private final mIsPublicDomain:Z

.field private final mIsQuoteSharingAllowed:Z

.field private final mIsUploaded:Z

.field private final mLanguage:Ljava/lang/String;

.field private final mLastAccess:J

.field private final mLocalCoverUri:Landroid/net/Uri;

.field private final mMaxDownloadDevices:I

.field private final mPageCount:I

.field private final mPublisher:Ljava/lang/String;

.field private final mReadingPosition:Ljava/lang/String;

.field private final mRentalExpiration:J

.field private final mRentalStart:J

.field private final mRentalState:Ljava/lang/String;

.field private final mServerCoverUri:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

.field private final mUsesExplicitOfflineLicenseManagement:Z

.field private final mViewability:Ljava/lang/String;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/google/android/apps/books/model/BaseVolumeData;-><init>()V

    .line 350
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mVolumeId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$100(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mVolumeId:Ljava/lang/String;

    .line 351
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$200(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mTitle:Ljava/lang/String;

    .line 352
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mAuthor:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$300(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mAuthor:Ljava/lang/String;

    .line 353
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPageCount:I
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$400(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mPageCount:I

    .line 354
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsLimitedPreview:Z
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$500(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsLimitedPreview:Z

    .line 355
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsUploaded:Z
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$600(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsUploaded:Z

    .line 356
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mViewability:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$700(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mViewability:Ljava/lang/String;

    .line 357
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mCanonicalUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$800(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mCanonicalUrl:Ljava/lang/String;

    .line 358
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mBuyUrl:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$900(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mBuyUrl:Ljava/lang/String;

    .line 359
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLanguage:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1000(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mLanguage:Ljava/lang/String;

    .line 360
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mReadingPosition:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1100(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mReadingPosition:Ljava/lang/String;

    .line 361
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLastAccess:J
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1200(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mLastAccess:J

    .line 362
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mEtag:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1300(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mEtag:Ljava/lang/String;

    .line 363
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPublisher:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1400(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mPublisher:Ljava/lang/String;

    .line 364
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDate:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1500(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mDate:Ljava/lang/String;

    .line 365
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDescription:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1600(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mDescription:Ljava/lang/String;

    .line 366
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mServerCoverUri:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1700(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mServerCoverUri:Ljava/lang/String;

    .line 367
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLocalCoverUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1800(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mLocalCoverUri:Landroid/net/Uri;

    .line 368
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$1900(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    .line 369
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsPublicDomain:Z
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2000(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsPublicDomain:Z

    .line 370
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalState:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2100(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mRentalState:Ljava/lang/String;

    .line 371
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalStart:J
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2200(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mRentalStart:J

    .line 372
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalExpiration:J
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2300(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mRentalExpiration:J

    .line 373
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mUsesExplicitOfflineLicenseManagement:Z
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2400(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mUsesExplicitOfflineLicenseManagement:Z

    .line 374
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mMaxDownloadDevices:I
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2500(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mMaxDownloadDevices:I

    .line 375
    # getter for: Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsQuoteSharingAllowed:Z
    invoke-static {p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->access$2600(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsQuoteSharingAllowed:Z

    .line 376
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;Lcom/google/android/apps/books/model/ImmutableVolumeData$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/ImmutableVolumeData;-><init>(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)V

    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mAuthor:Ljava/lang/String;

    return-object v0
.end method

.method public getBuyUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mBuyUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getCanonicalUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mCanonicalUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mEtag:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public getLastAccess()J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mLastAccess:J

    return-wide v0
.end method

.method public getLocalCoverUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mLocalCoverUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getMaxOfflineDevices()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mMaxDownloadDevices:I

    return v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mPageCount:I

    return v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mPublisher:Ljava/lang/String;

    return-object v0
.end method

.method public getReadingPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mReadingPosition:Ljava/lang/String;

    return-object v0
.end method

.method public getRentalExpiration()J
    .locals 2

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mRentalExpiration:J

    return-wide v0
.end method

.method public getRentalStart()J
    .locals 2

    .prologue
    .line 153
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mRentalStart:J

    return-wide v0
.end method

.method public getRentalState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mRentalState:Ljava/lang/String;

    return-object v0
.end method

.method public getServerCoverUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mServerCoverUri:Ljava/lang/String;

    return-object v0
.end method

.method public getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getViewability()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mViewability:Ljava/lang/String;

    return-object v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public isLimitedPreview()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsLimitedPreview:Z

    return v0
.end method

.method public isPublicDomain()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsPublicDomain:Z

    return v0
.end method

.method public isQuoteSharingAllowed()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsQuoteSharingAllowed:Z

    return v0
.end method

.method public isUploaded()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mIsUploaded:Z

    return v0
.end method

.method public usesExplicitOfflineLicenseManagement()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData;->mUsesExplicitOfflineLicenseManagement:Z

    return v0
.end method
