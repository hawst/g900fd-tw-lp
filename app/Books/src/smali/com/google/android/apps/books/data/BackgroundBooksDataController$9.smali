.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeDataOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 431
    :try_start_0
    new-instance v1, Lcom/google/android/apps/books/model/JsonVolumeData;

    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->val$volumeId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getVolumeOverview(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/ApiaryVolume;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Landroid/accounts/Account;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/model/JsonVolumeData;-><init>(Lcom/google/android/apps/books/api/data/ApiaryVolume;Landroid/accounts/Account;)V

    .line 434
    .local v1, "fromServer":Lcom/google/android/apps/books/model/VolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->saveVolumeDataOnMainThread(Lcom/google/android/apps/books/model/VolumeData;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    .end local v1    # "fromServer":Lcom/google/android/apps/books/model/VolumeData;
    :goto_0
    return-void

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeDataExceptionOnMainThread(Ljava/lang/String;Ljava/lang/Exception;)V
    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$1600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
