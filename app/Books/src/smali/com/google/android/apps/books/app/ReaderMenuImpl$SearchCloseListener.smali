.class Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;
.super Ljava/lang/Object;
.source "ReaderMenuImpl.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnCloseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderMenuImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchCloseListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;Lcom/google/android/apps/books/app/ReaderMenuImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderMenuImpl;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderMenuImpl$1;

    .prologue
    .line 355
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;-><init>(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    return-void
.end method


# virtual methods
.method public onClose()Z
    .locals 2

    .prologue
    .line 358
    const-string v0, "ReaderMenu"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const-string v0, "ReaderMenu"

    const-string v1, "mOnSearchCloseListener.onClose() being called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # invokes: Lcom/google/android/apps/books/app/ReaderMenuImpl;->dismissRecentSearchesPopup()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$400(Lcom/google/android/apps/books/app/ReaderMenuImpl;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderMenuImpl$SearchCloseListener;->this$0:Lcom/google/android/apps/books/app/ReaderMenuImpl;

    # getter for: Lcom/google/android/apps/books/app/ReaderMenuImpl;->mCallbacks:Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderMenuImpl;->access$200(Lcom/google/android/apps/books/app/ReaderMenuImpl;)Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;->onSearchClosed()Z

    .line 363
    const/4 v0, 0x0

    return v0
.end method
