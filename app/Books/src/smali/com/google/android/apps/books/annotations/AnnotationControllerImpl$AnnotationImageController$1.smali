.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->fetchImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

.field final synthetic val$cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->val$url:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->val$cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 574
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->val$url:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/books/annotations/BlobKeys;->forUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 576
    .local v1, "blobCacheKey":Ljava/lang/String;
    const/4 v3, 0x0

    .line 577
    .local v3, "error":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 579
    .local v4, "imageBytes":[B
    :try_start_0
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBlobStore:Lcom/google/android/apps/books/annotations/BlobStore;
    invoke-static {v10}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->access$1300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Lcom/google/android/apps/books/annotations/BlobStore;

    move-result-object v10

    invoke-interface {v10, v1}, Lcom/google/android/apps/books/annotations/BlobStore;->get(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 580
    .local v5, "input":Ljava/io/InputStream;
    if-nez v5, :cond_3

    .line 581
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;
    invoke-static {v10}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->access$1400(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->val$url:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-array v13, v13, [I

    invoke-interface {v10, v11, v12, v13}, Lcom/google/android/apps/books/net/ResponseGetter;->get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 582
    .local v8, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    .line 585
    .local v6, "networkInput":Ljava/io/InputStream;
    invoke-static {v6}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v4

    .line 586
    new-instance v5, Ljava/io/ByteArrayInputStream;

    .end local v5    # "input":Ljava/io/InputStream;
    invoke-direct {v5, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 592
    .end local v6    # "networkInput":Ljava/io/InputStream;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .restart local v5    # "input":Ljava/io/InputStream;
    :cond_0
    :goto_0
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 593
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 594
    new-instance v3, Ljava/lang/Exception;

    .end local v3    # "error":Ljava/lang/Exception;
    const-string v10, "Could not decode bitmap"

    invoke-direct {v3, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    .end local v5    # "input":Ljava/io/InputStream;
    .restart local v3    # "error":Ljava/lang/Exception;
    :cond_1
    :goto_1
    if-eqz v0, :cond_4

    .line 603
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v9

    .line 608
    .local v9, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    :goto_2
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v10}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->access$1500(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Ljava/util/concurrent/Executor;

    move-result-object v10

    new-instance v11, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1$1;

    invoke-direct {v11, p0, v9}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-interface {v10, v11}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 617
    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    :try_start_1
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->val$cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    sget-object v11, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNRESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    if-ne v10, v11, :cond_2

    .line 619
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBlobStore:Lcom/google/android/apps/books/annotations/BlobStore;
    invoke-static {v10}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->access$1300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Lcom/google/android/apps/books/annotations/BlobStore;

    move-result-object v10

    invoke-interface {v10, v1}, Lcom/google/android/apps/books/annotations/BlobStore;->set(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v7

    .line 620
    .local v7, "output":Ljava/io/OutputStream;
    invoke-virtual {v7, v4}, Ljava/io/OutputStream;->write([B)V

    .line 621
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 622
    iget-object v10, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBlobStore:Lcom/google/android/apps/books/annotations/BlobStore;
    invoke-static {v10}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->access$1300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Lcom/google/android/apps/books/annotations/BlobStore;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/android/apps/books/annotations/BlobStore;->save()V

    .line 623
    const-string v10, "AnnotationC"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 624
    const-string v10, "AnnotationC"

    const-string v11, "Saved image to cache"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 632
    .end local v7    # "output":Ljava/io/OutputStream;
    :cond_2
    :goto_3
    return-void

    .line 588
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v9    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    .restart local v5    # "input":Ljava/io/InputStream;
    :cond_3
    :try_start_2
    const-string v10, "AnnotationC"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 589
    const-string v10, "AnnotationC"

    const-string v11, "Loaded image from cache"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 596
    .end local v3    # "error":Ljava/lang/Exception;
    .end local v5    # "input":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 597
    .local v2, "e":Ljava/io/IOException;
    const/4 v0, 0x0

    .line 598
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    move-object v3, v2

    .restart local v3    # "error":Ljava/lang/Exception;
    goto :goto_1

    .line 605
    .end local v2    # "e":Ljava/io/IOException;
    :cond_4
    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v9

    .restart local v9    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    goto :goto_2

    .line 627
    :catch_1
    move-exception v2

    .line 628
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v10, "AnnotationC"

    const/4 v11, 0x6

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 629
    const-string v10, "AnnotationC"

    const-string v11, "Exception while saving bitmap in cache"

    invoke-static {v10, v11, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method
