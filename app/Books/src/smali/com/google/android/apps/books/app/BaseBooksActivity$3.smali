.class Lcom/google/android/apps/books/app/BaseBooksActivity$3;
.super Ljava/lang/Object;
.source "BaseBooksActivity.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 1013
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$3;, "Lcom/google/android/apps/books/app/BaseBooksActivity.3;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$3;, "Lcom/google/android/apps/books/app/BaseBooksActivity.3;"
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;"
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1018
    iget-object v7, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1041
    :cond_0
    :goto_0
    return-void

    .line 1021
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1022
    const-string v6, "BooksActivity"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1023
    const-string v6, "BooksActivity"

    const-string v7, "calling for manual sync"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    iget-object v7, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;
    invoke-static {v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$300(Lcom/google/android/apps/books/app/BaseBooksActivity;)Landroid/accounts/Account;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/google/android/apps/books/sync/SyncController;->requestManualSync(Z)V

    goto :goto_0

    .line 1029
    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;
    invoke-static {v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$1200(Lcom/google/android/apps/books/app/BaseBooksActivity;)Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;
    invoke-static {v8}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$300(Lcom/google/android/apps/books/app/BaseBooksActivity;)Landroid/accounts/Account;

    move-result-object v8

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastMyEbooksFetchTime(Ljava/lang/String;)J

    move-result-wide v2

    .line 1030
    .local v2, "fetchTime":J
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-nez v7, :cond_4

    .line 1032
    .local v1, "firstFetch":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 1033
    iget-object v7, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    .line 1034
    .local v4, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    .line 1035
    .local v5, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v7, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;
    invoke-static {v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$300(Lcom/google/android/apps/books/app/BaseBooksActivity;)Landroid/accounts/Account;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/books/util/LoaderParams;->buildFrom(Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v0

    .line 1036
    .local v0, "args":Landroid/os/Bundle;
    const-class v7, Lcom/google/android/apps/books/app/SyncErrorFragment;

    const/4 v8, 0x0

    invoke-static {v7, v8, v0, v5, v6}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 1038
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "firstFetch":Z
    .end local v4    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v5    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_4
    move v1, v6

    .line 1030
    goto :goto_1
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1013
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$3;, "Lcom/google/android/apps/books/app/BaseBooksActivity.3;"
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$3;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
