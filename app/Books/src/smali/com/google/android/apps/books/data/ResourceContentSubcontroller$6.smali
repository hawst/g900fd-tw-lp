.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$resource:Lcom/google/android/apps/books/model/Resource;

.field final synthetic val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Resource;)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$resource:Lcom/google/android/apps/books/model/Resource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    iget-object v0, v0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;->resourceRetriever:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;->get(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v8

    .line 453
    .local v8, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->copyListeners()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 454
    .local v7, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v0, v0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v1, v1, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-interface {v7, v0, v1, v8}, Lcom/google/android/apps/books/model/BooksDataListener;->onNewResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 458
    .end local v7    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$resource:Lcom/google/android/apps/books/model/Resource;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v4, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$6;->val$result:Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;

    const/4 v5, 0x1

    move-object v2, p1

    # invokes: Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->publishResult(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->access$1200(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ResourceContentSubcontroller$Result;Z)V

    .line 459
    return-void
.end method
