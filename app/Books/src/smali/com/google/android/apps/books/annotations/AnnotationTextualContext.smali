.class public Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
.super Ljava/lang/Object;
.source "AnnotationTextualContext.java"


# instance fields
.field public final afterSelectedText:Ljava/lang/String;

.field public final beforeSelectedText:Ljava/lang/String;

.field public final selectedText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "beforeSelectedText"    # Ljava/lang/String;
    .param p2, "selectedText"    # Ljava/lang/String;
    .param p3, "afterSelectedText"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    .line 26
    return-void
.end method

.method private isEmpty()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEmpty(Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)Z
    .locals 1
    .param p0, "context"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 113
    if-eqz p0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    if-ne p0, p1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 81
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 82
    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 84
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 85
    check-cast v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .line 86
    .local v0, "other":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 87
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 88
    goto :goto_0

    .line 89
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 90
    goto :goto_0

    .line 91
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 92
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 93
    goto :goto_0

    .line 94
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 95
    goto :goto_0

    .line 96
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 97
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 98
    goto :goto_0

    .line 99
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 100
    goto :goto_0
.end method

.method public getNormalizedSelectedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->collapseWhitespace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 67
    const/16 v0, 0x1f

    .line 68
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 69
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 70
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 72
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 73
    return v1

    .line 69
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 70
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 72
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnnotationTextualContext [beforeSelectedText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->beforeSelectedText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selectedText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->selectedText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", afterSelectedText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;->afterSelectedText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
