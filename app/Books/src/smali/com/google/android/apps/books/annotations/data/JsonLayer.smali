.class public Lcom/google/android/apps/books/annotations/data/JsonLayer;
.super Ljava/lang/Object;
.source "JsonLayer.java"


# instance fields
.field public allowedCharacterCount:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "allowedCharacterCount"
    .end annotation
.end field

.field public layerId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "layerId"
    .end annotation
.end field

.field public limitType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "limitType"
    .end annotation
.end field

.field public remainingCharacterCount:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "remainingCharacterCount"
    .end annotation
.end field

.field public updated:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "updated"
    .end annotation
.end field

.field public version:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "volumeAnnotationsVersion"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
