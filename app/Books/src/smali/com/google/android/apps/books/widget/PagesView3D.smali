.class public Lcom/google/android/apps/books/widget/PagesView3D;
.super Lcom/google/android/opengl/common/GLSurfaceView;
.source "PagesView3D.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesView;


# instance fields
.field private final mBitmapConfig:Landroid/graphics/Bitmap$Config;

.field private final mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

.field private mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

.field private mDestroyed:Z

.field private mDisplayFitWidth:Z

.field private mDisplayTwoPages:Z

.field private mFullPageSize:Landroid/graphics/Point;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mPageOffsetToLetterbox:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

.field private mPendingLoadingBitmap:Landroid/graphics/Bitmap;

.field private mSurfaceIsValid:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/PagesView3D;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/android/opengl/common/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDisplayTwoPages:Z

    .line 42
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDisplayFitWidth:Z

    .line 59
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageOffsetToLetterbox:Landroid/util/SparseArray;

    .line 80
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setFocusable(Z)V

    .line 81
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setFocusableInTouchMode(Z)V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->from(Landroid/content/res/Resources;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .line 83
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageTurnJava;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/widget/PagesView3D;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->chooseBitmapConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mBitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 85
    new-instance v0, Lcom/google/android/apps/books/util/ProductionLogger;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/util/ProductionLogger;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/PagesView3D;)Lcom/google/android/apps/books/widget/PagesView$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesView3D;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/PagesView3D;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesView3D;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDestroyed:Z

    return v0
.end method

.method private chooseBitmapConfig()Landroid/graphics/Bitmap$Config;
    .locals 2

    .prologue
    .line 89
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getMemoryClass()I

    move-result v0

    .line 90
    .local v0, "memClass":I
    const/16 v1, 0x40

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->deviceHasTooManyPixels()Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 94
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_0
.end method

.method private deliverLoadingBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 119
    const-string v0, "PagesView3D"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "PagesView3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uploading loading bitmap with dimensions ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setLoadingBitmap(Landroid/graphics/Bitmap;)V

    .line 124
    return-void
.end method

.method private displayTwoPages()Z
    .locals 1

    .prologue
    .line 559
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDisplayTwoPages:Z

    return v0
.end method

.method private firstPartOfAnimationIsOffscreen(Lcom/google/android/apps/books/util/ScreenDirection;)Z
    .locals 3
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 167
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v1

    .line 168
    .local v1, "writingDirection":Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/ReadingDirection;->fromScreenDirection(Lcom/google/android/apps/books/util/ScreenDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ReadingDirection;

    move-result-object v0

    .line 170
    .local v0, "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    sget-object v2, Lcom/google/android/apps/books/util/ReadingDirection;->BACKWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    if-ne v0, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private flipPage(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 4
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    .line 271
    .local v0, "increment":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v2

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/ScreenDirection;->dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;I)I

    move-result v3

    add-int v1, v2, v3

    .line 273
    .local v1, "pageOffset":I
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->flipPage(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 274
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/widget/PagesView3D;->moveToPosition(ILcom/google/android/apps/books/util/ScreenDirection;)V

    .line 275
    return-void

    .line 270
    .end local v0    # "increment":I
    .end local v1    # "pageOffset":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getFullPageSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mFullPageSize:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 349
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mFullPageSize:Landroid/graphics/Point;

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mFullPageSize:Landroid/graphics/Point;

    return-object v0
.end method

.method private loadPages(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 6
    .param p1, "turnDirection"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v0

    .line 315
    .local v0, "displayTwoPages":Z
    if-nez p1, :cond_0

    .line 316
    invoke-static {v0}, Lcom/google/android/apps/books/util/PagesViewUtils;->getRelativePageOffsets(Z)[I

    move-result-object v1

    .line 323
    .local v1, "pageOffsets":[I
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v2, v1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->loadPages([I)V

    .line 324
    return-void

    .line 317
    .end local v1    # "pageOffsets":[I
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v2, :cond_2

    .line 319
    if-eqz v0, :cond_1

    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .restart local v1    # "pageOffsets":[I
    :goto_1
    goto :goto_0

    .end local v1    # "pageOffsets":[I
    :cond_1
    new-array v1, v3, [I

    const/4 v2, -0x1

    aput v2, v1, v4

    goto :goto_1

    .line 321
    :cond_2
    if-eqz v0, :cond_3

    new-array v1, v5, [I

    fill-array-data v1, :array_1

    .restart local v1    # "pageOffsets":[I
    :goto_2
    goto :goto_0

    .end local v1    # "pageOffsets":[I
    :cond_3
    new-array v1, v3, [I

    aput v3, v1, v4

    goto :goto_2

    .line 319
    :array_0
    .array-data 4
        -0x2
        -0x3
    .end array-data

    .line 321
    :array_1
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method private moveToPosition(ILcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 4
    .param p1, "targetOffset"    # I
    .param p2, "turnDirection"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setAnchorIndex(I)V

    .line 295
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/PagesView3D;->loadPages(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 300
    :cond_1
    return-void
.end method

.method private savePageLetterbox(ILcom/google/android/apps/books/render/PagePainter;)V
    .locals 5
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 459
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    invoke-interface {p2, v3}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 460
    .local v0, "letterbox":Landroid/graphics/Point;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageHeight()I

    move-result v4

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 461
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageWidth()I

    move-result v3

    iget v4, v0, Landroid/graphics/Point;->x:I

    sub-int v1, v3, v4

    .line 462
    .local v1, "xDelta":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageHeight()I

    move-result v3

    iget v4, v0, Landroid/graphics/Point;->y:I

    sub-int v2, v3, v4

    .line 463
    .local v2, "yDelta":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v3

    if-eqz v3, :cond_0

    .end local v1    # "xDelta":I
    :goto_0
    div-int/lit8 v3, v2, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Point;->set(II)V

    .line 464
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageOffsetToLetterbox:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 465
    return-void

    .line 463
    .restart local v1    # "xDelta":I
    :cond_0
    div-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method

.method private setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V
    .locals 1
    .param p1, "offset"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "pageRenderDetails"    # Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V

    .line 423
    :cond_0
    return-void
.end method

.method private setPageToBitmap(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V
    .locals 3
    .param p1, "offset"    # I
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "pageRenderDetails"    # Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    .prologue
    .line 406
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v1, :cond_1

    .line 407
    const-string v1, "PagesView3D"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 408
    const-string v1, "PagesView3D"

    const-string v2, "PagesView3D creating bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_0
    const/4 v1, 0x1

    invoke-interface {p2, v1}, Lcom/google/android/apps/books/render/PagePainter;->getSharedBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 412
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/apps/books/widget/PagesView3D;->setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V

    .line 414
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-void
.end method

.method private updatePageMeasurements()V
    .locals 4

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    new-instance v1, Lcom/google/android/apps/books/view/pages/PageMeasurements;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/view/pages/PageMeasurements;-><init>(II)V

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setMeasurements(Lcom/google/android/apps/books/view/pages/PageMeasurements;Z)V

    .line 528
    return-void
.end method


# virtual methods
.method public dxToFraction(FLcom/google/android/apps/books/util/ScreenDirection;)F
    .locals 6
    .param p1, "dx"    # F
    .param p2, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getWidth()I

    move-result v2

    .line 479
    .local v2, "width":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v3

    if-eqz v3, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v4, v2

    div-float v1, v3, v4

    .line 480
    .local v1, "sensitivity":F
    :goto_0
    neg-float v3, p1

    mul-float v0, v3, v1

    .line 484
    .local v0, "fraction":F
    cmpl-float v3, v0, v5

    if-lez v3, :cond_0

    sub-float v3, v0, v5

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    add-float v0, v5, v3

    .line 485
    :cond_0
    sget-object v3, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p2, v3, :cond_1

    .line 486
    neg-float v0, v0

    .line 488
    :cond_1
    return v0

    .line 479
    .end local v0    # "fraction":F
    .end local v1    # "sensitivity":F
    :cond_2
    const v3, 0x3f19999a    # 0.6f

    int-to-float v4, v2

    div-float v1, v3, v4

    goto :goto_0
.end method

.method public finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V
    .locals 1
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "flinging"    # Z
    .param p3, "velocity"    # F
    .param p4, "isCompletion"    # Z

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->finishOpening(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 177
    return-void
.end method

.method public getBitmapConfig()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mBitmapConfig:Landroid/graphics/Bitmap$Config;

    return-object v0
.end method

.method public getOnePageHeight()I
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getHeight()I

    move-result v0

    return v0
.end method

.method public getOnePageWidth()I
    .locals 2

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getWidth()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/PagesViewUtils;->getPageWidth(IZ)I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 432
    return-object p0
.end method

.method public inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
    .locals 5
    .param p1, "event"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageOffsetToLetterbox:Landroid/util/SparseArray;

    iget v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->offset:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    .line 187
    .local v0, "letterbox":Landroid/graphics/Point;
    if-nez v0, :cond_0

    .line 188
    const/4 v1, 0x0

    .line 190
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mBookmarkMeasurements:Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getOnePageWidth()I

    move-result v2

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v4, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;III)Z

    move-result v1

    goto :goto_0
.end method

.method public isCached(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 427
    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 246
    invoke-super {p0}, Lcom/google/android/opengl/common/GLSurfaceView;->onAttachedToWindow()V

    .line 247
    const-string v0, "PagesView3D"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    const-string v0, "PagesView3D"

    const-string v1, "onAttachedToWindow()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/PagesView3D;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPendingLoadingBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPendingLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->deliverLoadingBitmap(Landroid/graphics/Bitmap;)V

    .line 255
    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPendingLoadingBitmap:Landroid/graphics/Bitmap;

    .line 257
    :cond_1
    return-void
.end method

.method public onBasePositionChanged()V
    .locals 0

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->refreshPages()V

    .line 137
    return-void
.end method

.method public onBookmarkChanged(IZ)V
    .locals 1
    .param p1, "targetOffset"    # I
    .param p2, "isGainingBookmark"    # Z

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->startBookmarkAnimate(IZ)V

    .line 182
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDestroyed:Z

    .line 237
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0}, Lcom/google/android/opengl/common/GLSurfaceView;->onDetachedFromWindow()V

    .line 262
    const-string v0, "PagesView3D"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const-string v0, "PagesView3D"

    const-string v1, "onDetachedFromWindow()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 266
    return-void
.end method

.method public onEndedTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZI)V
    .locals 0
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "completed"    # Z
    .param p3, "offsetFromBasePosition"    # I

    .prologue
    .line 285
    if-eqz p2, :cond_0

    .line 286
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesView3D;->flipPage(Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 288
    :cond_0
    return-void
.end method

.method public onLoadingPageVisibilityChanged(Z)V
    .locals 1
    .param p1, "loadingPageVisible"    # Z

    .prologue
    .line 594
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView3D$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/PagesView3D$3;-><init>(Lcom/google/android/apps/books/widget/PagesView3D;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->post(Ljava/lang/Runnable;)Z

    .line 602
    return-void
.end method

.method public onPageOffsetChanged(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 132
    return-void
.end method

.method public onPagesInvalidated()V
    .locals 1

    .prologue
    .line 579
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView3D$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PagesView3D$2;-><init>(Lcom/google/android/apps/books/widget/PagesView3D;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->post(Ljava/lang/Runnable;)Z

    .line 587
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 231
    invoke-super {p0}, Lcom/google/android/opengl/common/GLSurfaceView;->onPause()V

    .line 232
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 241
    invoke-super {p0}, Lcom/google/android/opengl/common/GLSurfaceView;->onResume()V

    .line 242
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 518
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/opengl/common/GLSurfaceView;->onSizeChanged(IIII)V

    .line 519
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->updatePageMeasurements()V

    .line 520
    return-void
.end method

.method public onStartedTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 0
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 279
    return-void
.end method

.method public onTurnAnimationFinished()V
    .locals 2

    .prologue
    .line 140
    const-string v0, "PagesView3D"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "PagesView3D"

    const-string v1, "Animation complete"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/widget/PagesView3D$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PagesView3D$1;-><init>(Lcom/google/android/apps/books/widget/PagesView3D;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->post(Ljava/lang/Runnable;)Z

    .line 151
    return-void
.end method

.method public refreshPages()V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageOffsetToLetterbox:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->getCurrentOffsetFromBasePosition()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->moveToPosition(ILcom/google/android/apps/books/util/ScreenDirection;)V

    .line 441
    :cond_0
    return-void
.end method

.method public resetZoom()V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->resetZoom()V

    .line 503
    :cond_0
    return-void
.end method

.method public setCallbacks(Lcom/google/android/apps/books/widget/PagesView$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    .line 100
    return-void
.end method

.method public setCoverView(Landroid/view/View;)V
    .locals 1
    .param p1, "coverView"    # Landroid/view/View;

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setCoverView(Landroid/view/View;)V

    .line 514
    return-void
.end method

.method public setDisplayTwoPages(ZZ)V
    .locals 0
    .param p1, "displayTwoPages"    # Z
    .param p2, "displayFitWidth"    # Z

    .prologue
    .line 570
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDisplayTwoPages:Z

    .line 571
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mDisplayFitWidth:Z

    .line 572
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->updatePageMeasurements()V

    .line 573
    return-void
.end method

.method public setExecutingInitialLoadTransition(Z)V
    .locals 1
    .param p1, "executing"    # Z

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setExecutingInitialLoadTransition(Z)V

    .line 607
    return-void
.end method

.method public setGestureFraction(Lcom/google/android/apps/books/util/ScreenDirection;FI)V
    .locals 3
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "fraction"    # F
    .param p3, "offsetFromBasePosition"    # I

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesView3D;->firstPartOfAnimationIsOffscreen(Lcom/google/android/apps/books/util/ScreenDirection;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v1

    if-nez v1, :cond_0

    .line 158
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3ecccccd    # 0.4f

    add-float/2addr v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 162
    .local v0, "animationFraction":F
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    const/4 v2, 0x0

    invoke-virtual {v1, p3, v0, p1, v2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 163
    return-void

    .line 160
    .end local v0    # "animationFraction":F
    :cond_0
    move v0, p2

    .restart local v0    # "animationFraction":F
    goto :goto_0
.end method

.method public setLoadingBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mSurfaceIsValid:Z

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesView3D;->deliverLoadingBitmap(Landroid/graphics/Bitmap;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPendingLoadingBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setPageBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setBackgroundColor(I)V

    .line 105
    return-void
.end method

.method public setPageLoading(ILandroid/graphics/Point;)V
    .locals 1
    .param p1, "offset"    # I
    .param p2, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setPageLoading(ILandroid/graphics/Point;)V

    .line 357
    return-void
.end method

.method public setPageRendering(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V
    .locals 8
    .param p1, "offsetFromBasePosition"    # I
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p4, "transitionStartTime"    # J

    .prologue
    .line 446
    const-string v0, "painter can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesView3D;->savePageLetterbox(ILcom/google/android/apps/books/render/PagePainter;)V

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mCallbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->atLeastOnePositionHasBookmark(I)Z

    move-result v1

    .line 452
    .local v1, "hasBookmark":Z
    const/4 v6, 0x0

    .line 453
    .local v6, "centerBitmapOnPage":Z
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageRenderDetails;-><init>(ZZLandroid/graphics/Point;J)V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->setPageToBitmap(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V

    .line 456
    return-void
.end method

.method public setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;J)V
    .locals 7
    .param p1, "offset"    # I
    .param p2, "page"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "displaySize"    # Landroid/graphics/Point;
    .param p5, "transitionStartTime"    # J

    .prologue
    const/4 v1, 0x0

    .line 332
    if-eqz p3, :cond_1

    .line 337
    sget-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-ne p2, v0, :cond_0

    const/4 v2, 0x1

    .line 340
    .local v2, "centerOnPage":Z
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/view/pages/PageRenderDetails;

    move-object v3, p4

    move-wide v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageRenderDetails;-><init>(ZZLandroid/graphics/Point;J)V

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/apps/books/widget/PagesView3D;->setPageToBitmap(ILandroid/graphics/Bitmap;Lcom/google/android/apps/books/view/pages/PageRenderDetails;)V

    .line 345
    .end local v2    # "centerOnPage":Z
    :goto_1
    return-void

    :cond_0
    move v2, v1

    .line 337
    goto :goto_0

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1, p4}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setPageLoading(ILandroid/graphics/Point;)V

    goto :goto_1
.end method

.method public setPagesLoading()V
    .locals 6

    .prologue
    .line 361
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->getFullPageSize()Landroid/graphics/Point;

    move-result-object v1

    .line 362
    .local v1, "fullPageSize":Landroid/graphics/Point;
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesView3D;->displayTwoPages()Z

    move-result v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/PagesViewUtils;->getRelativePageOffsets(Z)[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v4, v0, v2

    .line 363
    .local v4, "offset":I
    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/books/widget/PagesView3D;->setPageLoading(ILandroid/graphics/Point;)V

    .line 362
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 365
    .end local v4    # "offset":I
    :cond_0
    return-void
.end method

.method public setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V
    .locals 1
    .param p1, "direction"    # Lcom/google/android/apps/books/util/WritingDirection;

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V

    .line 551
    :cond_0
    return-void
.end method

.method public supportsPageTurnMode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pageTurnMode"    # Ljava/lang/String;

    .prologue
    .line 532
    const-string v0, "turn3d"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 214
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/opengl/common/GLSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 215
    const-string v0, "PagesView3D"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "PagesView3D"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceChanged() w="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", h="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 196
    invoke-super {p0, p1}, Lcom/google/android/opengl/common/GLSurfaceView;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 197
    const-string v0, "PagesView3D"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const-string v0, "PagesView3D"

    const-string v1, "surfaceCreated()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mSurfaceIsValid:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setAnchorIndex(I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-virtual {v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setPagesEmpty()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mPageTurn:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->setTurnState(IFLcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 210
    :cond_1
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 222
    invoke-super {p0, p1}, Lcom/google/android/opengl/common/GLSurfaceView;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 223
    const-string v0, "PagesView3D"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const-string v0, "PagesView3D"

    const-string v1, "surfaceDestroyed()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesView3D;->mSurfaceIsValid:Z

    .line 227
    return-void
.end method
