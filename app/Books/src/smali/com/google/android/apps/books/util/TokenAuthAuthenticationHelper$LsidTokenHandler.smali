.class Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;
.super Ljava/lang/Object;
.source "TokenAuthAuthenticationHelper.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LsidTokenHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private final mSid:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;)V
    .locals 0
    .param p2, "sid"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->mSid:Ljava/lang/String;

    .line 142
    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "value":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    const-string v3, "authtoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "lsid":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    iget-object v3, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->mSid:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->getUberToken(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$100(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 160
    .end local v1    # "lsid":Ljava/lang/String;
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v2, "TokenAuthHelper"

    const-string v3, "cancelled while acquiring token"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 150
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V
    invoke-static {v2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V

    goto :goto_0

    .line 151
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 152
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    const-string v2, "TokenAuthHelper"

    const-string v3, "authentication error while acquiring token"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V
    invoke-static {v2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V

    goto :goto_0

    .line 154
    .end local v0    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v0

    .line 155
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "TokenAuthHelper"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    const-string v2, "TokenAuthHelper"

    const-string v3, "IO error while acquiring token"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 158
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V
    invoke-static {v2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V

    goto :goto_0
.end method
