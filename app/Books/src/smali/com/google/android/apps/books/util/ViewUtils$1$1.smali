.class Lcom/google/android/apps/books/util/ViewUtils$1$1;
.super Ljava/lang/Object;
.source "ViewUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/util/ViewUtils$1;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/ViewUtils$1;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/ViewUtils$1;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/google/android/apps/books/util/ViewUtils$1$1;->this$0:Lcom/google/android/apps/books/util/ViewUtils$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/apps/books/util/ViewUtils$1$1;->this$0:Lcom/google/android/apps/books/util/ViewUtils$1;

    iget-object v0, v0, Lcom/google/android/apps/books/util/ViewUtils$1;->val$tag:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/books/util/ViewUtils$1$1;->this$0:Lcom/google/android/apps/books/util/ViewUtils$1;

    iget-object v0, v0, Lcom/google/android/apps/books/util/ViewUtils$1;->val$tag:Ljava/lang/String;

    const-string v1, "Page turn animation complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/ViewUtils$1$1;->this$0:Lcom/google/android/apps/books/util/ViewUtils$1;

    iget-object v0, v0, Lcom/google/android/apps/books/util/ViewUtils$1;->val$callbacks:Lcom/google/android/apps/books/widget/PagesView$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView$Callbacks;->onPageTurnAnimationFinished()V

    .line 324
    return-void
.end method
