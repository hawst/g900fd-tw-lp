.class public interface abstract Lcom/google/android/apps/books/annotations/AnnotationProcessor;
.super Ljava/lang/Object;
.source "AnnotationProcessor.java"


# virtual methods
.method public abstract add(Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
.end method

.method public abstract delete(Ljava/lang/String;)V
.end method

.method public abstract done()V
.end method

.method public abstract reset()V
.end method

.method public abstract updateCharacterQuota(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
.end method
