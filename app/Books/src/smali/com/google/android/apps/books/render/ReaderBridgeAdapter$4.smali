.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onChapterReady(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$chapterIndex:I

.field final synthetic val$metricsJSON:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput-object p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;->val$metricsJSON:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;->val$chapterIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;->val$metricsJSON:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, "metrics":Lcom/google/android/apps/books/util/JsPerformanceMetrics;
    iget-object v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;->val$chapterIndex:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/books/render/ReaderListener;->onChapterReady(ILcom/google/android/apps/books/util/JsPerformanceMetrics;)V

    .line 115
    return-void
.end method
