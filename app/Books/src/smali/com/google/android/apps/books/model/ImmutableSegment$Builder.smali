.class public Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
.super Ljava/lang/Object;
.source "ImmutableSegment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutableSegment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mFixedLayoutVersion:I

.field private mFixedViewportHeight:I

.field private mFixedViewportWidth:I

.field private mId:Ljava/lang/String;

.field private mPageCount:I

.field private mStartPosition:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mUrl:Ljava/lang/String;

.field private mViewable:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/Segment;
    .locals 10

    .prologue
    .line 100
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableSegment;

    iget-object v1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mStartPosition:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mPageCount:I

    iget-boolean v5, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mViewable:Z

    iget v6, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedLayoutVersion:I

    iget v7, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedViewportWidth:I

    iget v8, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedViewportHeight:I

    iget-object v9, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mUrl:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/model/ImmutableSegment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/lang/String;)V

    return-object v0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 108
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mId:Ljava/lang/String;

    .line 109
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mTitle:Ljava/lang/String;

    .line 110
    iput-object v1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mStartPosition:Ljava/lang/String;

    .line 111
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mPageCount:I

    .line 112
    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mViewable:Z

    .line 113
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedLayoutVersion:I

    .line 114
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedViewportWidth:I

    .line 115
    iput v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedViewportHeight:I

    .line 116
    return-void
.end method

.method public setFixedLayoutVersion(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "fixedLayoutVersion"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedLayoutVersion:I

    .line 81
    return-object p0
.end method

.method public setFixedLayoutViewportHeight(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "vpHeight"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedViewportHeight:I

    .line 91
    return-object p0
.end method

.method public setFixedLayoutViewportWidth(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "vpWidth"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mFixedViewportWidth:I

    .line 86
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mId:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public setPageCount(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "pageCount"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mPageCount:I

    .line 71
    return-object p0
.end method

.method public setStartPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "startPosition"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mStartPosition:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mTitle:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mUrl:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public setViewable(Z)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 0
    .param p1, "viewable"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->mViewable:Z

    .line 76
    return-object p0
.end method
