.class public Lcom/google/android/apps/books/app/TouchInterceptingLinearLayout;
.super Landroid/widget/LinearLayout;
.source "TouchInterceptingLinearLayout.java"


# instance fields
.field private mOnTouchListener:Landroid/view/View$OnTouchListener;


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/app/TouchInterceptingLinearLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 32
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/TouchInterceptingLinearLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
