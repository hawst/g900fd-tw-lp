.class public Lcom/google/android/apps/books/provider/database/ResourceResourcesTable;
.super Ljava/lang/Object;
.source "ResourceResourcesTable.java"


# direct methods
.method public static getCreationSql()Ljava/lang/String;
    .locals 8

    .prologue
    .line 28
    const-string v1, "REFERENCES volumes(volume_id)"

    .line 31
    .local v1, "refVolumeId":Ljava/lang/String;
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "FOREIGN KEY(%s, %s, %s) REFERENCES %s(%s, %s, %s), "

    const/4 v5, 0x7

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "account_name"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "volume_id"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "referenced_res_id"

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-string v7, "resources"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "account_name"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "volume_id"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "resource_id"

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "foreignKey":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CREATE TABLE resource_resources (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL REFERENCES volumes(volume_id), compound_res_id TEXT NOT NULL, referenced_res_id TEXT NOT NULL, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "UNIQUE (account_name, volume_id, compound_res_id, referenced_res_id) ON CONFLICT IGNORE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "result":Ljava/lang/String;
    return-object v2
.end method

.method public static getCreationSqlVersion134()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    const-string v0, "CREATE TABLE resource_resources (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL REFERENCES volumes(volume_id), compound_res_id TEXT NOT NULL, referenced_res_id TEXT NOT NULL, FOREIGN KEY(account_name, volume_id, referenced_res_id) REFERENCES resources(account_name, volume_id, resource_id), UNIQUE (account_name, volume_id, compound_res_id, referenced_res_id) ON CONFLICT IGNORE)"

    .line 65
    .local v0, "result":Ljava/lang/String;
    const-string v1, "CREATE TABLE resource_resources (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL REFERENCES volumes(volume_id), compound_res_id TEXT NOT NULL, referenced_res_id TEXT NOT NULL, FOREIGN KEY(account_name, volume_id, referenced_res_id) REFERENCES resources(account_name, volume_id, resource_id), UNIQUE (account_name, volume_id, compound_res_id, referenced_res_id) ON CONFLICT IGNORE)"

    return-object v1
.end method
