.class Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
.super Lcom/google/android/apps/books/data/ConsumerMap;
.source "ExceptionOrConsumerMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/books/data/ConsumerMap",
        "<TK;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<TV;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TK;TV;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ConsumerMap;-><init>()V

    return-void
.end method

.method public static createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;-><init>()V

    return-object v0
.end method


# virtual methods
.method public publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V

    .line 45
    return-void
.end method

.method public publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p2, "e"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Exception;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p3, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TV;>;>;"
    invoke-static {p2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 40
    .local v0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TV;>;"
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 41
    return-void
.end method

.method public publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 30
    return-void
.end method

.method public publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;, "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    .local p3, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TV;>;>;"
    invoke-static {p2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 25
    .local v0, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<TV;>;"
    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 26
    return-void
.end method
