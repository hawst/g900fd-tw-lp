.class Lcom/google/android/apps/books/app/HomeFragment$14$1;
.super Landroid/support/v4/view/PagerAdapter;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment$14;->addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mOldFilterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/LibraryFilter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/android/apps/books/app/HomeFragment$14;

.field final synthetic val$filters:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment$14;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1040
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$14;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->val$filters:Ljava/util/List;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 1073
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1074
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->val$filters:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v3, -0x1

    const/4 v4, -0x2

    .line 1078
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$14;

    iget-object v5, v5, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    invoke-static {v5}, Lcom/google/android/apps/books/app/HomeFragment;->access$1000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    move-result-object v5

    check-cast p1, Landroid/view/View;

    .end local p1    # "object":Ljava/lang/Object;
    invoke-virtual {v5, p1}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getFilter(Landroid/view/View;)Lcom/google/android/apps/books/app/LibraryFilter;

    move-result-object v0

    .line 1080
    .local v0, "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    if-nez v0, :cond_1

    move v1, v4

    .line 1095
    :cond_0
    :goto_0
    return v1

    .line 1085
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->mOldFilterList:Ljava/util/List;

    if-nez v5, :cond_2

    move v1, v4

    .line 1086
    goto :goto_0

    .line 1089
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->val$filters:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1090
    .local v1, "index":I
    if-ne v1, v3, :cond_3

    move v1, v4

    .line 1091
    goto :goto_0

    .line 1094
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->mOldFilterList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1095
    .local v2, "oldIndex":I
    if-ne v2, v1, :cond_0

    move v1, v3

    goto :goto_0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->val$filters:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/LibraryFilter;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$14;

    iget-object v1, v1, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter;->getDisplayName(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 1047
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$14;

    iget-object v2, v2, Lcom/google/android/apps/books/app/HomeFragment$14;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$1000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->val$filters:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getContentView(Lcom/google/android/apps/books/app/LibraryFilter;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    .line 1051
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1052
    .local v1, "parent":Landroid/view/ViewParent;
    if-eqz v1, :cond_0

    .line 1053
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "parent":Landroid/view/ViewParent;
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1056
    :cond_0
    if-eqz v0, :cond_1

    .line 1057
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1059
    :cond_1
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 1110
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 1064
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 1067
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->val$filters:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$14$1;->mOldFilterList:Ljava/util/List;

    .line 1068
    return-void
.end method
