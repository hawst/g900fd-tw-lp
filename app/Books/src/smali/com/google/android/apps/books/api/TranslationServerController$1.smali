.class Lcom/google/android/apps/books/api/TranslationServerController$1;
.super Ljava/lang/Object;
.source "TranslationServerController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/api/TranslationServerController;->discoverLanguages(Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/api/TranslationServerController;

.field final synthetic val$callback:Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/api/TranslationServerController;Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServerController$1;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    iput-object p2, p0, Lcom/google/android/apps/books/api/TranslationServerController$1;->val$callback:Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 68
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$1;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    # getter for: Lcom/google/android/apps/books/api/TranslationServerController;->mServer:Lcom/google/android/apps/books/api/TranslationServer;
    invoke-static {v2}, Lcom/google/android/apps/books/api/TranslationServerController;->access$000(Lcom/google/android/apps/books/api/TranslationServerController;)Lcom/google/android/apps/books/api/TranslationServer;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/api/TranslationServer;->discoverLanguages()Lcom/google/common/collect/BiMap;

    move-result-object v1

    .line 69
    .local v1, "results":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$1;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    # getter for: Lcom/google/android/apps/books/api/TranslationServerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/api/TranslationServerController;->access$200(Lcom/google/android/apps/books/api/TranslationServerController;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/api/TranslationServerController$1$1;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/books/api/TranslationServerController$1$1;-><init>(Lcom/google/android/apps/books/api/TranslationServerController$1;Lcom/google/common/collect/BiMap;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .end local v1    # "results":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/google/android/apps/books/api/TranslationServerController$1;->this$0:Lcom/google/android/apps/books/api/TranslationServerController;

    # getter for: Lcom/google/android/apps/books/api/TranslationServerController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/api/TranslationServerController;->access$200(Lcom/google/android/apps/books/api/TranslationServerController;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/api/TranslationServerController$1$2;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/books/api/TranslationServerController$1$2;-><init>(Lcom/google/android/apps/books/api/TranslationServerController$1;Ljava/lang/Exception;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
