.class public Lcom/google/android/apps/books/annotations/TextLocationRange;
.super Lcom/google/android/apps/books/annotations/LocationRange;
.source "TextLocationRange.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/annotations/LocationRange",
        "<",
        "Lcom/google/android/apps/books/annotations/TextLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)V
    .locals 2
    .param p1, "start"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .param p2, "end"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 24
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/apps/books/annotations/LocationRange;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 2
    .param p1, "startPos"    # Ljava/lang/String;
    .param p2, "startOffset"    # I
    .param p3, "endPos"    # Ljava/lang/String;
    .param p4, "endOffset"    # I

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    invoke-static {p3, p4}, Lcom/google/android/apps/books/annotations/TextLocation;->createOrNull(Ljava/lang/String;I)Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected getCorrespondingRangeFromAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/LocationRange;
    .locals 1
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ")",
            "Lcom/google/android/apps/books/annotations/LocationRange",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v0

    return-object v0
.end method
