.class Lcom/google/android/apps/books/app/AbstractSetting$1;
.super Ljava/lang/Object;
.source "AbstractSetting.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AbstractSetting;->createView(Landroid/app/Activity;ZLcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/AbstractSetting;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;

.field final synthetic val$overridesViaGservices:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AbstractSetting;Landroid/app/Activity;ZLcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->this$0:Lcom/google/android/apps/books/app/AbstractSetting;

    iput-object p2, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->val$activity:Landroid/app/Activity;

    iput-boolean p3, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->val$overridesViaGservices:Z

    iput-object p4, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOptionValuesChanged([Ljava/lang/String;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/String;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->this$0:Lcom/google/android/apps/books/app/AbstractSetting;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->val$activity:Landroid/app/Activity;

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->val$overridesViaGservices:Z

    # invokes: Lcom/google/android/apps/books/app/AbstractSetting;->applyValues([Ljava/lang/String;Landroid/content/Context;Z)Z
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/books/app/AbstractSetting;->access$000(Lcom/google/android/apps/books/app/AbstractSetting;[Ljava/lang/String;Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/app/AbstractSetting$1;->val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;->onOptionChanged()V

    .line 44
    :cond_0
    return-void
.end method
