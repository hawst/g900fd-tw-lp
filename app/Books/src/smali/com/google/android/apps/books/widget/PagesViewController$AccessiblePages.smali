.class public interface abstract Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AccessiblePages"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;
    }
.end annotation


# virtual methods
.method public abstract setImportantForAccessibility(Z)V
.end method

.method public abstract setPageRendering(ILcom/google/android/apps/books/widget/DevicePageRendering;)V
.end method

.method public abstract setSelectionChangedListener(Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;)V
.end method

.method public abstract wantsPageRenderings()Z
.end method
