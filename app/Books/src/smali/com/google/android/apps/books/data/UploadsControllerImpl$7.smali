.class Lcom/google/android/apps/books/data/UploadsControllerImpl$7;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Updater;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadFilesize(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Updater",
        "<",
        "Lcom/google/android/apps/books/upload/Upload;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$fileSize:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;I)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$7;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$7;->val$fileSize:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;
    .locals 1
    .param p1, "upload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    .line 263
    iget v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$7;->val$fileSize:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/upload/Upload;->setFileSize(I)V

    .line 264
    return-object p1
.end method

.method public bridge synthetic update(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 260
    check-cast p1, Lcom/google/android/apps/books/upload/Upload;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$7;->update(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v0

    return-object v0
.end method
