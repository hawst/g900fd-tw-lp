.class Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;
.super Ljava/lang/Object;
.source "BooksImageManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

.field final synthetic val$resultBitmap:Landroid/graphics/Bitmap;

.field final synthetic val$resultException:Ljava/lang/Throwable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    iput-object p2, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->val$resultBitmap:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->val$resultException:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 363
    const-string v4, "BooksImageManager"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    .line 364
    .local v3, "isDebuggable":Z
    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    iget-object v4, v4, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksImageManager;->access$000(Lcom/google/android/apps/books/app/BooksImageManager;)Ljava/util/HashMap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;
    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->access$400(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;)Lcom/google/android/ublib/util/ImageSpecifier;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 365
    .local v1, "callbacks":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/common/ImageCallback;>;"
    if-eqz v1, :cond_1

    .line 367
    # invokes: Lcom/google/android/apps/books/app/BooksImageManager;->asList(Ljava/util/Set;)Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksImageManager;->access$500(Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/ImageCallback;

    .line 368
    .local v0, "callback":Lcom/google/android/apps/books/common/ImageCallback;
    if-eqz v3, :cond_0

    .line 369
    const-string v4, "BooksImageManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invoking image callback "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->val$resultBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->val$resultException:Ljava/lang/Throwable;

    invoke-interface {v0, v4, v5}, Lcom/google/android/apps/books/common/ImageCallback;->onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 373
    .end local v0    # "callback":Lcom/google/android/apps/books/common/ImageCallback;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    if-eqz v3, :cond_2

    .line 374
    const-string v4, "BooksImageManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Image cancelled during scale: adding to cache anyways: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;
    invoke-static {v6}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->access$400(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;)Lcom/google/android/ublib/util/ImageSpecifier;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->val$resultBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_3

    .line 380
    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    iget-object v4, v4, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    iget-object v5, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;
    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->access$400(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;)Lcom/google/android/ublib/util/ImageSpecifier;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->val$resultBitmap:Landroid/graphics/Bitmap;

    # invokes: Lcom/google/android/apps/books/app/BooksImageManager;->cacheBitmap(Lcom/google/android/ublib/util/ImageSpecifier;Landroid/graphics/Bitmap;)V
    invoke-static {v4, v5, v6}, Lcom/google/android/apps/books/app/BooksImageManager;->access$600(Lcom/google/android/apps/books/app/BooksImageManager;Lcom/google/android/ublib/util/ImageSpecifier;Landroid/graphics/Bitmap;)V

    .line 383
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    iget-object v4, v4, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksImageManager;->access$000(Lcom/google/android/apps/books/app/BooksImageManager;)Ljava/util/HashMap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;
    invoke-static {v5}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->access$400(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;)Lcom/google/android/ublib/util/ImageSpecifier;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 385
    if-eqz v3, :cond_4

    .line 386
    const-string v4, "BooksImageManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Image task for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;
    invoke-static {v6}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->access$400(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;)Lcom/google/android/ublib/util/ImageSpecifier;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " exiting; "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;->this$1:Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    iget-object v6, v6, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/google/android/apps/books/app/BooksImageManager;->access$000(Lcom/google/android/apps/books/app/BooksImageManager;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " remain"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_4
    return-void
.end method
