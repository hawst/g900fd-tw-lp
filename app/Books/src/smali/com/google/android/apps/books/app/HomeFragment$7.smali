.class Lcom/google/android/apps/books/app/HomeFragment$7;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->maybeStartRecommendationsLoad(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$app:Lcom/google/android/apps/books/app/BooksApplication;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/BooksApplication;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$7;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$7;->val$app:Lcom/google/android/apps/books/app/BooksApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "recommendationsEnabled"    # Ljava/lang/Boolean;

    .prologue
    .line 575
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 583
    :goto_0
    return-void

    .line 578
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$7;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$1900(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/Eventual;->loadingConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    .line 580
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;>;"
    new-instance v1, Lcom/google/android/apps/books/app/LoadRecommendationsTask;

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$7;->val$app:Lcom/google/android/apps/books/app/BooksApplication;

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment$7;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->forHomePage()Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;-><init>(Lcom/google/android/apps/books/app/BooksApplication;Landroid/accounts/Account;Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;Lcom/google/android/ublib/utils/Consumer;)V

    .line 582
    .local v1, "task":Lcom/google/android/apps/books/app/LoadRecommendationsTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v1, v2}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 572
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$7;->take(Ljava/lang/Boolean;)V

    return-void
.end method
