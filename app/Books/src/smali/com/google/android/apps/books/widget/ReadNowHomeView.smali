.class public Lcom/google/android/apps/books/widget/ReadNowHomeView;
.super Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.source "ReadNowHomeView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

.field private mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

.field protected mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

.field protected mCardsAdapterHasBeenSet:Z

.field private mCurrentlyShowingOfferId:Ljava/lang/String;

.field private final mDismissOffersClickHandler:Landroid/view/View$OnClickListener;

.field private mEmptyLibraryView:Landroid/view/View;

.field private mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

.field private mHasFetchedEbooksOnce:Z

.field private mHasReceivedRecommendations:Z

.field private mIsNewUserForOnboarding:Z

.field private mIsSchoolOwnedConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mLibraryFilter:Lcom/google/common/base/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation
.end field

.field private final mOffers:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mOnboardBooksClickHandler:Landroid/view/View$OnClickListener;

.field private final mRecentSeeAllClickHandler:Landroid/view/View$OnClickListener;

.field private final mRecommendationClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecommendationImageProviderFactory:Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;

.field private mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/PlayCardClusterView",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field protected mRecommendationsFooter:Landroid/widget/FrameLayout;

.field private final mRecommendationsHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

.field private final mRecommendationsSeeMoreClickHandler:Landroid/view/View$OnClickListener;

.field private final mRecommendedBookDocuments:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field private mRecommendedBooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end field

.field private final mRedeemOffersClickHandler:Landroid/view/View$OnClickListener;

.field private mShouldLayoutRecsInRows:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;ZLandroid/view/View$OnClickListener;)V
    .locals 6
    .param p1, "controller"    # Lcom/google/android/apps/books/widget/BooksHomeController;
    .param p2, "cardsContainer"    # Landroid/view/ViewGroup;
    .param p3, "isDeviceConnected"    # Z
    .param p4, "seeAllHandler"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v1, 0x0

    .line 221
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;-><init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;Z)V

    .line 82
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBookDocuments:Ljava/util/Map;

    .line 84
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    .line 87
    iput-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapterHasBeenSet:Z

    .line 113
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$1;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    .line 121
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$2;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mLibraryFilter:Lcom/google/common/base/Predicate;

    .line 158
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;Lcom/google/android/apps/books/widget/ReadNowHomeView$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationImageProviderFactory:Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;

    .line 161
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$3;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsSeeMoreClickHandler:Landroid/view/View$OnClickListener;

    .line 169
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$4;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOnboardBooksClickHandler:Landroid/view/View$OnClickListener;

    .line 182
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$5;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRedeemOffersClickHandler:Landroid/view/View$OnClickListener;

    .line 189
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$6;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mDismissOffersClickHandler:Landroid/view/View$OnClickListener;

    .line 202
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$7;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    .line 222
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-interface {p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getFilteredCardData()Ljava/util/List;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/books/widget/BooksCardsAdapter$Callbacks;Lcom/google/android/apps/books/widget/BooksHomeController;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 224
    iput-object p4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecentSeeAllClickHandler:Landroid/view/View$OnClickListener;

    .line 225
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->inflateCardsView(Landroid/view/ViewGroup;)V

    .line 226
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/ReadNowHomeView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ReadNowHomeView;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCurrentlyShowingOfferId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/ReadNowHomeView;)Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ReadNowHomeView;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    return-object v0
.end method

.method static synthetic access$301(Lcom/google/android/apps/books/widget/ReadNowHomeView;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ReadNowHomeView;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->setCardsData(Ljava/util/List;)V

    return-void
.end method

.method private buildEmptyCard(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 437
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 438
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mEmptyLibraryView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 439
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getEmptyLibraryView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mEmptyLibraryView:Landroid/view/View;

    .line 442
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mEmptyLibraryView:Landroid/view/View;

    return-object v1
.end method

.method private buildOffersCard(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 466
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 467
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0400af

    const/4 v5, 0x0

    invoke-virtual {v2, v4, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 468
    .local v0, "cardView":Landroid/view/View;
    const v4, 0x7f0e00d3

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 469
    .local v3, "redeemButton":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRedeemOffersClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    const v4, 0x7f0e01ab

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 471
    .local v1, "dismissButton":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mDismissOffersClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 472
    return-object v0
.end method

.method private buildOnboardCard(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 446
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 447
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0400b0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 448
    .local v0, "cardView":Landroid/view/View;
    const v4, 0x7f0e0151

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 449
    .local v3, "onboardButton":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOnboardBooksClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451
    new-instance v1, Lcom/google/android/apps/books/widget/ReadNowHomeView$12;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$12;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    .line 461
    .local v1, "emptyReadNowCardGotItClickHandler":Landroid/view/View$OnClickListener;
    const v4, 0x7f0e0152

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 462
    return-object v0
.end method

.method private buildRecentHeaderView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 424
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 425
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f040037

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;

    .line 428
    .local v0, "header":Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 429
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0f01a3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const v5, 0x7f0f01a4

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    const v3, 0x7f0f01a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 432
    iget-object v3, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecentSeeAllClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewHeader;->setMoreButtonClickHandler(Landroid/view/View$OnClickListener;)V

    .line 433
    return-object v0
.end method

.method private buildSpacer(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 476
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 477
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0400b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method private convertToTruncatedBookDocumentList(Ljava/util/List;Landroid/content/Context;I)Ljava/util/List;
    .locals 8
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "columnCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation

    .prologue
    .line 546
    .local p1, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mShouldLayoutRecsInRows:Z

    if-eqz v7, :cond_0

    const/4 p3, 0x5

    .end local p3    # "columnCount":I
    :cond_0
    invoke-static {v6, p3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 550
    .local v4, "numToShow":I
    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 551
    .local v2, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 552
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    .line 553
    .local v5, "recBook":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBookDocuments:Ljava/util/Map;

    iget-object v7, v5, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 554
    .local v0, "cachedDoc":Lcom/google/android/apps/books/playcards/BookDocument;
    if-eqz v0, :cond_1

    .line 555
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 557
    :cond_1
    new-instance v1, Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-direct {v1, v5, p2}, Lcom/google/android/apps/books/playcards/BookDocument;-><init>(Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;Landroid/content/Context;)V

    .line 558
    .local v1, "convertedBook":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559
    iget-object v6, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBookDocuments:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 562
    .end local v0    # "cachedDoc":Lcom/google/android/apps/books/playcards/BookDocument;
    .end local v1    # "convertedBook":Lcom/google/android/apps/books/playcards/BookDocument;
    .end local v5    # "recBook":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    :cond_2
    return-object v2
.end method

.method private getCardHeap()Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    if-nez v0, :cond_0

    .line 592
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    return-object v0
.end method

.method private getRecommendationsFooter()Landroid/widget/FrameLayout;
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setupRecommendationsFooter(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private inflateCardsView(Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 625
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 626
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0400c3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iput-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .line 628
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getColumnCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setColumnCount(I)V

    .line 630
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v1, :cond_0

    .line 631
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setAdapter(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    .line 634
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeAddRecommendationsToCardsView()V

    .line 635
    return-void
.end method

.method private maybeAddRecommendationsToCardsView()V
    .locals 3

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    if-eqz v0, :cond_1

    .line 278
    const-string v0, "ReadNowHomeView"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const-string v0, "ReadNowHomeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "maybeAddRecommendationsToCardsView has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recommendations"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setRecommendations(Ljava/util/List;)V

    .line 283
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    .line 285
    :cond_1
    return-void
.end method

.method private maybeShowOrHideOffersCard()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 372
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/Eventual;->hasValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/Eventual;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 376
    .local v1, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->isDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 377
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->showInfoView(Z)V

    .line 378
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCurrentlyShowingOfferId:Ljava/lang/String;

    goto :goto_0

    .line 381
    :cond_3
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/OfferData;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/OfferData;->getOfferId()Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "offerIdToShow":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCurrentlyShowingOfferId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 383
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_SAW_READ_NOW_CARD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 385
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCurrentlyShowingOfferId:Ljava/lang/String;

    .line 390
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/google/android/apps/books/widget/ReadNowHomeView$10;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$10;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/RecommendationsUtil;->loadShouldShowRecommendations(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private maybeShowOrHideRecommendationsFooter()V
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mHasFetchedEbooksOnce:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mHasReceivedRecommendations:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->isDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->showRecommendationsFooter(Z)V

    .line 312
    return-void

    .line 308
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setHasFetchedEbooksOnce(Z)V
    .locals 1
    .param p1, "hasFetched"    # Z

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setHasFetchedEbooksOnce(Z)V

    .line 687
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mHasFetchedEbooksOnce:Z

    if-eq v0, p1, :cond_1

    .line 688
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mHasFetchedEbooksOnce:Z

    .line 689
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeShowOrHideRecommendationsFooter()V

    .line 691
    :cond_1
    return-void
.end method

.method private setupRecommendationsFooter(Landroid/view/ViewGroup;Ljava/util/List;)V
    .locals 19
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 481
    .local p2, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 484
    .local v2, "context":Landroid/content/Context;
    if-nez v2, :cond_1

    .line 538
    :cond_0
    :goto_0
    return-void

    .line 488
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    if-nez v3, :cond_2

    .line 489
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    .line 491
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 494
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 499
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->isTablet()Z

    move-result v3

    if-nez v3, :cond_3

    .line 500
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mShouldLayoutRecsInRows:Z

    .line 501
    const/4 v11, 0x1

    .line 521
    .local v11, "columns":I
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v11}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->convertToTruncatedBookDocumentList(Ljava/util/List;Landroid/content/Context;I)Ljava/util/List;

    move-result-object v4

    .line 524
    .local v4, "bookDocuments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getCardHeap()Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v3}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsSeeMoreClickHandler:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationImageProviderFactory:Lcom/google/android/apps/books/widget/ReadNowHomeView$RecommendationImageProviderFactory;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOptionalActionCallback:Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mShouldLayoutRecsInRows:Z

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->buildCluster(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Landroid/view/View$OnClickListener;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;IZ)Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    .line 531
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 533
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->setVisibility(I)V

    .line 534
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 503
    .end local v4    # "bookDocuments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .end local v11    # "columns":I
    :cond_3
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 504
    .local v18, "res":Landroid/content/res/Resources;
    const v3, 0x7f0900fc

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    .line 508
    .local v15, "minCellWidth":I
    const v3, 0x7f0900f7

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 510
    .local v14, "extraRowPadding":I
    const v3, 0x7f0900f6

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 512
    .local v13, "extraCellPadding":I
    const v3, 0x7f0c000e

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v16

    .line 514
    .local v16, "minColumns":I
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-static {v0, v15, v14, v13, v1}, Lcom/google/android/ublib/cardlib/utils/PlayCardsUtils;->getCardsGridColumnCount(Landroid/content/res/Resources;IIII)I

    move-result v17

    .line 517
    .local v17, "potentialColumns":I
    const/4 v3, 0x3

    move/from16 v0, v17

    if-ge v0, v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mShouldLayoutRecsInRows:Z

    .line 518
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mShouldLayoutRecsInRows:Z

    if-eqz v3, :cond_5

    const/4 v11, 0x1

    .restart local v11    # "columns":I
    :goto_3
    goto/16 :goto_1

    .line 517
    .end local v11    # "columns":I
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    move/from16 v11, v17

    .line 518
    goto :goto_3
.end method

.method private showRecommendationsFooter(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/4 v1, 0x0

    .line 295
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    if-nez v2, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 300
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 304
    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private withShouldShowOnboardingCard(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "showOnboardingCardConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    const/4 v2, 0x0

    .line 332
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 333
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 362
    :goto_0
    return-void

    .line 337
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_ONBOARDING_CARD:Lcom/google/android/apps/books/util/ConfigValue;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 342
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mIsNewUserForOnboarding:Z

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARD_EXISTING:Lcom/google/android/apps/books/util/ConfigValue;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 343
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 347
    :cond_2
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;Lcom/google/android/ublib/utils/Consumer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mIsSchoolOwnedConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mIsSchoolOwnedConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v1}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->withIsSchoolOwned(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method


# virtual methods
.method public acknowledgeUploadFailure(Ljava/lang/String;)V
    .locals 1
    .param p1, "uploadId"    # Ljava/lang/String;

    .prologue
    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/UploadsController;->cancelUpload(Ljava/lang/String;)V

    .line 741
    return-void
.end method

.method public getColumnCount()I
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    return-object v0
.end method

.method public getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v0

    return-object v0
.end method

.method public getHelpContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 750
    const-string v0, "mobile_read_now"

    return-object v0
.end method

.method protected getLibraryFilter()Lcom/google/common/base/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/base/Predicate",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mLibraryFilter:Lcom/google/common/base/Predicate;

    return-object v0
.end method

.method public getSnapshottableView()Landroid/view/View;
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected getSortComparator()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 1

    .prologue
    .line 680
    sget-object v0, Lcom/google/android/apps/books/app/LibraryComparator;->BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

    return-object v0
.end method

.method protected isVolumeAboutToBeDeleted(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->volumeDownloadAnimationInProgress(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected maybeShowOrHideEmptyView()V
    .locals 0

    .prologue
    .line 410
    return-void
.end method

.method public maybeUpdateOnboardCardVisibility()V
    .locals 1

    .prologue
    .line 318
    new-instance v0, Lcom/google/android/apps/books/widget/ReadNowHomeView$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView$8;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->withShouldShowOnboardingCard(Lcom/google/android/ublib/utils/Consumer;)V

    .line 327
    return-void
.end method

.method protected onCardsDataChanged()V
    .locals 1

    .prologue
    .line 705
    invoke-super {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->onCardsDataChanged()V

    .line 706
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->alreadyFetchedMyEbooks()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setHasFetchedEbooksOnce(Z)V

    .line 709
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 230
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .line 231
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    .line 232
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    .line 233
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    .line 234
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    .line 235
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    .line 236
    return-void
.end method

.method public onDeviceConnectionChanged(Z)V
    .locals 1
    .param p1, "connected"    # Z

    .prologue
    .line 718
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->onDeviceConnectionChanged(Z)V

    .line 719
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->notifyDataSetChanged()V

    .line 725
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v0, :cond_1

    .line 726
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    .line 728
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeShowOrHideRecommendationsFooter()V

    .line 729
    return-void
.end method

.method public onRecommendationDismissed(Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .param p1, "dismissedVolumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 570
    .local p2, "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/BooksHomeController;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 572
    .local v2, "context":Landroid/content/Context;
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationsFooter:Landroid/widget/FrameLayout;

    if-eqz v4, :cond_0

    if-nez p2, :cond_1

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    if-eqz v4, :cond_2

    .line 577
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->getColumnCount()I

    move-result v1

    .line 578
    .local v1, "columns":I
    invoke-direct {p0, p2, v2, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->convertToTruncatedBookDocumentList(Ljava/util/List;Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    .line 580
    .local v0, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mShouldLayoutRecsInRows:Z

    invoke-static {v4, v1, v5}, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;->getCardMetadata(IIZ)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move-result-object v3

    .line 582
    .local v3, "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendationPlayCardCluster:Lcom/google/android/ublib/cardlib/PlayCardClusterView;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getCardHeap()Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    move-result-object v5

    invoke-virtual {v4, v0, v5, v3}, Lcom/google/android/ublib/cardlib/PlayCardClusterView;->onDocumentsChanged(Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;)V

    .line 584
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBookDocuments:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 586
    .end local v0    # "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    .end local v1    # "columns":I
    .end local v3    # "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    :cond_2
    iput-object p2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    goto :goto_0
.end method

.method protected postProcessVolumeData(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 656
    .local p1, "cardDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/CardData;

    .line 658
    .local v0, "cardData":Lcom/google/android/apps/books/widget/CardData;
    const-string v3, "Null cardData in cardDataList"

    invoke-static {v0, v3}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 659
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 660
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    .line 662
    .local v2, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    const-string v3, "cardData contains null volumeData"

    invoke-static {v0, v3}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 671
    .end local v0    # "cardData":Lcom/google/android/apps/books/widget/CardData;
    .end local v2    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    .end local p1    # "cardDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    :cond_1
    :goto_0
    return-object p1

    .restart local p1    # "cardDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    :cond_2
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0
.end method

.method protected resetCardsAdapterData()V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getFilteredCardData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->resetCardData(Ljava/util/List;)V

    .line 240
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapterHasBeenSet:Z

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setCardsAdapter(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V

    .line 242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapterHasBeenSet:Z

    .line 244
    :cond_0
    return-void
.end method

.method public setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fraction"    # F

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setAnimatedVolumeDownloadFraction(Ljava/lang/String;F)V

    .line 696
    return-void
.end method

.method protected setCardsAdapter(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V
    .locals 2
    .param p1, "cardsAdapter"    # Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .prologue
    const/4 v0, 0x1

    .line 601
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    .line 602
    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setUseCoverOnlyCards(Z)V

    .line 603
    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setPinsAreTouchable(Z)V

    .line 604
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->setMaximumViewsToDisplay(I)V

    .line 606
    new-instance v0, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;-><init>(Lcom/google/android/apps/books/widget/BaseCardsAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->buildSpacer(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setSpacerView(Landroid/view/View;)V

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->buildOffersCard(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setInfoView(Landroid/view/View;)V

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->buildOnboardCard(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setOnboardView(Landroid/view/View;)V

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->buildRecentHeaderView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setHeader(Landroid/view/View;)V

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getRecommendationsFooter()Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setFooter(Landroid/view/View;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->buildEmptyCard(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setEmptyView(Landroid/view/View;)V

    .line 613
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mHasFetchedEbooksOnce:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->setHasFetchedEbooksOnce(Z)V

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/StaggeredCoverView;->setAdapter(Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;)V

    .line 619
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeShowOrHideRecommendationsFooter()V

    .line 620
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeUpdateOnboardCardVisibility()V

    .line 621
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeShowOrHideOffersCard()V

    .line 622
    return-void
.end method

.method public setCardsData(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 415
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/widget/ReadNowHomeView$11;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$11;-><init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 421
    return-void
.end method

.method public setIsNewUser(Z)V
    .locals 0
    .param p1, "isNewUser"    # Z

    .prologue
    .line 367
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mIsNewUserForOnboarding:Z

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeUpdateOnboardCardVisibility()V

    .line 369
    return-void
.end method

.method public setNoOffers()V
    .locals 2

    .prologue
    .line 273
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setOffers(Ljava/util/List;Z)V

    .line 274
    return-void
.end method

.method public setOffers(Ljava/util/List;Z)V
    .locals 2
    .param p2, "fromServer"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p1, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    if-eqz p1, :cond_2

    move-object v0, p1

    .line 263
    .local v0, "safeOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    :goto_0
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/Eventual;->hasValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeShowOrHideOffersCard()V

    .line 269
    :cond_1
    return-void

    .line 261
    .end local v0    # "safeOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public setRecommendations(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mGridView:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setupRecommendationsFooter(Landroid/view/ViewGroup;Ljava/util/List;)V

    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mHasReceivedRecommendations:Z

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeShowOrHideRecommendationsFooter()V

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mRecommendedBooks:Ljava/util/List;

    goto :goto_0
.end method

.method public updateRentalBadges()V
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mAdapter:Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BooksStaggeredGridAdapter;->notifyDataSetChanged()V

    .line 736
    :cond_0
    return-void
.end method

.method public volumeDownloadFractionAnimationDone(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCardsAdapter:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->volumeDownloadFractionAnimationDone(Ljava/lang/String;)V

    .line 701
    return-void
.end method
