.class public Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
.super Ljava/lang/Object;
.source "ImmutableVolumeData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutableVolumeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAuthor:Ljava/lang/String;

.field private mBuyUrl:Ljava/lang/String;

.field private mCanonicalUrl:Ljava/lang/String;

.field private mDate:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mEtag:Ljava/lang/String;

.field private mIsLimitedPreview:Z

.field private mIsPublicDomain:Z

.field private mIsQuoteSharingAllowed:Z

.field private mIsUploaded:Z

.field private mLanguage:Ljava/lang/String;

.field private mLastAccess:J

.field private mLocalCoverUri:Landroid/net/Uri;

.field private mMaxDownloadDevices:I

.field private mPageCount:I

.field private mPublisher:Ljava/lang/String;

.field private mReadingPosition:Ljava/lang/String;

.field private mRentalExpiration:J

.field private mRentalStart:J

.field private mRentalState:Ljava/lang/String;

.field private mServerCoverUri:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

.field private mUsesExplicitOfflineLicenseManagement:Z

.field private mViewability:Ljava/lang/String;

.field private mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mReadingPosition:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLastAccess:J

    return-wide v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mEtag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPublisher:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mServerCoverUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLocalCoverUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsPublicDomain:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalState:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalStart:J

    return-wide v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-wide v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalExpiration:J

    return-wide v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mUsesExplicitOfflineLicenseManagement:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mMaxDownloadDevices:I

    return v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsQuoteSharingAllowed:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mAuthor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPageCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsLimitedPreview:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsUploaded:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mViewability:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mCanonicalUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mBuyUrl:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/VolumeData;
    .locals 2

    .prologue
    .line 316
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableVolumeData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/model/ImmutableVolumeData;-><init>(Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;Lcom/google/android/apps/books/model/ImmutableVolumeData$1;)V

    return-object v0
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 320
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mVolumeId:Ljava/lang/String;

    .line 321
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTitle:Ljava/lang/String;

    .line 322
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mAuthor:Ljava/lang/String;

    .line 323
    iput v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPageCount:I

    .line 324
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsLimitedPreview:Z

    .line 325
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsUploaded:Z

    .line 326
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mViewability:Ljava/lang/String;

    .line 327
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mCanonicalUrl:Ljava/lang/String;

    .line 328
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mBuyUrl:Ljava/lang/String;

    .line 329
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLanguage:Ljava/lang/String;

    .line 330
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mReadingPosition:Ljava/lang/String;

    .line 331
    iput-wide v2, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLastAccess:J

    .line 332
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mEtag:Ljava/lang/String;

    .line 333
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPublisher:Ljava/lang/String;

    .line 334
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDate:Ljava/lang/String;

    .line 335
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDescription:Ljava/lang/String;

    .line 336
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mServerCoverUri:Ljava/lang/String;

    .line 337
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLocalCoverUri:Landroid/net/Uri;

    .line 338
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    .line 339
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsPublicDomain:Z

    .line 340
    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalState:Ljava/lang/String;

    .line 341
    iput-wide v2, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalStart:J

    .line 342
    iput-wide v2, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalExpiration:J

    .line 343
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mUsesExplicitOfflineLicenseManagement:Z

    .line 344
    iput v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mMaxDownloadDevices:I

    .line 345
    iput-boolean v1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsQuoteSharingAllowed:Z

    .line 346
    return-void
.end method

.method public setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mAuthor:Ljava/lang/String;

    .line 220
    return-object p0
.end method

.method public setBuyUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "buyUrl"    # Ljava/lang/String;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mBuyUrl:Ljava/lang/String;

    .line 244
    return-object p0
.end method

.method public setCanonicalUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "canonicalUrl"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mCanonicalUrl:Ljava/lang/String;

    .line 240
    return-object p0
.end method

.method public setDate(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDate:Ljava/lang/String;

    .line 268
    return-object p0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mDescription:Ljava/lang/String;

    .line 272
    return-object p0
.end method

.method public setEtag(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mEtag:Ljava/lang/String;

    .line 260
    return-object p0
.end method

.method public setIsLimitedPreview(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "isLimitedPreview"    # Z

    .prologue
    .line 227
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsLimitedPreview:Z

    .line 228
    return-object p0
.end method

.method public setIsPublicDomain(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "isPublicDomain"    # Z

    .prologue
    .line 287
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsPublicDomain:Z

    .line 288
    return-object p0
.end method

.method public setIsQuoteSharingAllowed(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "isQuoteSharingAllowed"    # Z

    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsQuoteSharingAllowed:Z

    .line 313
    return-object p0
.end method

.method public setIsUploaded(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "isUploaded"    # Z

    .prologue
    .line 231
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mIsUploaded:Z

    .line 232
    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLanguage:Ljava/lang/String;

    .line 248
    return-object p0
.end method

.method public setLastAccess(J)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 1
    .param p1, "lastAccess"    # J

    .prologue
    .line 255
    iput-wide p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLastAccess:J

    .line 256
    return-object p0
.end method

.method public setLocalCoverUri(Landroid/net/Uri;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "localCoverUri"    # Landroid/net/Uri;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mLocalCoverUri:Landroid/net/Uri;

    .line 280
    return-object p0
.end method

.method public setMaxDownloadDevices(I)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "maxDownloadDevices"    # I

    .prologue
    .line 308
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mMaxDownloadDevices:I

    .line 309
    return-object p0
.end method

.method public setPageCount(I)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "pageCount"    # I

    .prologue
    .line 223
    iput p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPageCount:I

    .line 224
    return-object p0
.end method

.method public setPublisher(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "publisher"    # Ljava/lang/String;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mPublisher:Ljava/lang/String;

    .line 264
    return-object p0
.end method

.method public setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mReadingPosition:Ljava/lang/String;

    .line 252
    return-object p0
.end method

.method public setRentalExpiration(J)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 1
    .param p1, "rentalExpiration"    # J

    .prologue
    .line 299
    iput-wide p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalExpiration:J

    .line 300
    return-object p0
.end method

.method public setRentalStart(J)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 1
    .param p1, "rentalStart"    # J

    .prologue
    .line 295
    iput-wide p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalStart:J

    .line 296
    return-object p0
.end method

.method public setRentalState(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "rentalState"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mRentalState:Ljava/lang/String;

    .line 292
    return-object p0
.end method

.method public setServerCoverUri(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "serverCoverUri"    # Ljava/lang/String;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mServerCoverUri:Ljava/lang/String;

    .line 276
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTitle:Ljava/lang/String;

    .line 216
    return-object p0
.end method

.method public setTtsPermission(Lcom/google/android/apps/books/model/VolumeData$TtsPermission;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "ttsPermission"    # Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mTtsPermission:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    .line 284
    return-object p0
.end method

.method public setUsesExplicitOfflineLicenseManagement(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "usesExplicitOfflineLicenseManagement"    # Z

    .prologue
    .line 304
    iput-boolean p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mUsesExplicitOfflineLicenseManagement:Z

    .line 305
    return-object p0
.end method

.method public setViewability(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "viewability"    # Ljava/lang/String;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mViewability:Ljava/lang/String;

    .line 236
    return-object p0
.end method

.method public setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->mVolumeId:Ljava/lang/String;

    .line 212
    return-object p0
.end method
