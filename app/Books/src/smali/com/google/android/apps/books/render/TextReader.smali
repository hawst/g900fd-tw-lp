.class public interface abstract Lcom/google/android/apps/books/render/TextReader;
.super Ljava/lang/Object;
.source "TextReader.java"


# virtual methods
.method public abstract activateMediaElement(IILjava/lang/String;I)V
.end method

.method public abstract applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
.end method

.method public abstract initializeJavascript(Lcom/google/android/apps/books/util/JsConfiguration;)V
.end method

.method public abstract loadNearbyText(IIFFI)V
.end method

.method public abstract loadNotNormalizedRangeData(IIIII)V
.end method

.method public abstract loadPage(IIII)V
.end method

.method public abstract loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;I)V
.end method

.method public abstract loadRangeData(IILcom/google/android/apps/books/annotations/TextLocationRange;ZIII)V
.end method

.method public abstract loadRangeDataBulk(IILjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract loadSpreadPage(ILjava/lang/String;III)V
.end method

.method public abstract loadSpreadPageFromEob(III)V
.end method

.method public abstract maybePurgePassages(Ljava/util/Collection;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract preloadPassage(II)V
.end method

.method public abstract requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;I)V
.end method

.method public abstract setOverlayActiveClass(Ljava/lang/String;)V
.end method
