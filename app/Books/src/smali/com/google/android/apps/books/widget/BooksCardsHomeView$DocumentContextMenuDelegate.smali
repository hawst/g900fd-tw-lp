.class public Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;
.super Ljava/lang/Object;
.source "BooksCardsHomeView.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DocumentContextMenuDelegate"
.end annotation


# instance fields
.field private final mHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;)V
    .locals 0
    .param p2, "handlerFactory"    # Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;->mHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    .line 132
    return-void
.end method


# virtual methods
.method public onContextMenuPressed(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    .locals 3
    .param p1, "cardView"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->onCardOverflowMenuShown()V

    .line 138
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getDocument()Lcom/google/android/ublib/cardlib/model/Document;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 139
    .local v0, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DocumentContextMenuDelegate;->mHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;->getHandler(Lcom/google/android/apps/books/playcards/BookDocument;)Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p2, v2}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;->showPopupMenu(Landroid/view/View;Lcom/google/android/ublib/view/SystemUi;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    move-result-object v1

    return-object v1
.end method
