.class final Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;
.super Landroid/os/AsyncTask;
.source "BaseBooksActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CheckForUnsupportedCountry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mShouldShowWarningDialog:Z

.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 1

    .prologue
    .line 955
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.CheckForUnsupportedCountry;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->mShouldShowWarningDialog:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/BaseBooksActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/BaseBooksActivity$1;

    .prologue
    .line 955
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.CheckForUnsupportedCountry;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 955
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.CheckForUnsupportedCountry;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.CheckForUnsupportedCountry;"
    const/4 v2, 0x0

    .line 960
    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-static {v1}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->isInSupportedCountry(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 968
    :cond_0
    :goto_0
    return-object v2

    .line 964
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 965
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHasShownUnsupportedCountryWarning()Z

    move-result v1

    if-nez v1, :cond_0

    .line 966
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->mShouldShowWarningDialog:Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 955
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.CheckForUnsupportedCountry;"
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;, "Lcom/google/android/apps/books/app/BaseBooksActivity<TT;>.CheckForUnsupportedCountry;"
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 973
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->mShouldShowWarningDialog:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->isActivityDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mHasShownUnsupportedCountryDialog:Z
    invoke-static {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$1000(Lcom/google/android/apps/books/app/BaseBooksActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 975
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 976
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 977
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-class v2, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {v2, v5, v3, v1, v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 979
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 983
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mHasShownUnsupportedCountryDialog:Z
    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$1002(Lcom/google/android/apps/books/app/BaseBooksActivity;Z)Z

    .line 985
    new-instance v2, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;

    iget-object v3, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$CheckForUnsupportedCountry;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {v2, v3, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;-><init>(Lcom/google/android/apps/books/app/BaseBooksActivity;Lcom/google/android/apps/books/app/BaseBooksActivity$1;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/BaseBooksActivity$SaveHasShownUnsupportedCountry;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 987
    .end local v0    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method
