.class public Lcom/google/android/apps/books/app/InfoCardsHelper;
.super Ljava/lang/Object;
.source "InfoCardsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;,
        Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;
    }
.end annotation


# instance fields
.field private mAllCardsRequested:Z

.field private final mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

.field private final mCardProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/InfoCardProvider;",
            ">;"
        }
    .end annotation
.end field

.field private mCardsCount:I

.field private final mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

.field private mIfNoCards:Ljava/lang/Runnable;

.field private mInitialStateForNextCards:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

.field private mNextDispatch:I

.field private mNumCardRequests:I

.field private final mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

.field private final mProviderCallbackResults:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;",
            ">;"
        }
    .end annotation
.end field

.field private final mProviderOrderToPendingCards:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSelectionSequenceNumber:I

.field private mWaitingForHideAnimation:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;Lcom/google/android/ublib/infocards/SuggestionGridLayout;)V
    .locals 2
    .param p2, "callbacks"    # Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;
    .param p3, "cardsLayout"    # Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/InfoCardProvider;",
            ">;",
            "Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;",
            "Lcom/google/android/ublib/infocards/SuggestionGridLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "cardProviders":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/InfoCardProvider;>;"
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderOrderToPendingCards:Ljava/util/Map;

    .line 86
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;

    .line 89
    iput v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNextDispatch:I

    .line 92
    iput v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNumCardRequests:I

    .line 95
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mAllCardsRequested:Z

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mIfNoCards:Ljava/lang/Runnable;

    .line 112
    new-instance v0, Lcom/google/android/apps/books/app/InfoCardsHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/InfoCardsHelper$1;-><init>(Lcom/google/android/apps/books/app/InfoCardsHelper;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    .line 106
    iput-object p1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardProviders:Ljava/util/List;

    .line 107
    iput-object p2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    .line 108
    iput-object p3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mOnDismissListener:Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->setOnDismissListener(Lcom/google/android/ublib/infocards/SuggestionGridLayout$OnDismissListener;)V

    .line 110
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/InfoCardsHelper;)Lcom/google/android/ublib/infocards/SuggestionGridLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/InfoCardsHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    return v0
.end method

.method static synthetic access$120(Lcom/google/android/apps/books/app/InfoCardsHelper;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/InfoCardsHelper;)Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/InfoCardsHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/InfoCardsHelper;)Ljava/util/PriorityQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/InfoCardsHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/InfoCardsHelper;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->maybeLoadCards()V

    return-void
.end method

.method private addCards(ILjava/util/List;)V
    .locals 7
    .param p1, "providerOrder"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v3, 0x0

    .line 264
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p2

    move-object v4, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->addStackToColumn(Ljava/util/List;ILcom/google/android/ublib/infocards/DismissTrailFactory;Landroid/view/View;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mInitialStateForNextCards:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;->onCardsAdded(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V

    .line 267
    return-void
.end method

.method private createViewConsumer()Lcom/google/android/ublib/utils/Consumer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 198
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNumCardRequests:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNumCardRequests:I

    .line 199
    .local v0, "cardProviderOrder":I
    iget v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I

    .line 200
    .local v1, "selectionSequenceNumber":I
    new-instance v2, Lcom/google/android/apps/books/app/InfoCardsHelper$3;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/apps/books/app/InfoCardsHelper$3;-><init>(Lcom/google/android/apps/books/app/InfoCardsHelper;II)V

    return-object v2
.end method

.method private maybeLoadCards()V
    .locals 4

    .prologue
    .line 211
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;

    iget v1, v1, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->providerOrder:I

    iget v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNextDispatch:I

    if-ne v1, v2, :cond_0

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;

    .line 214
    .local v0, "result":Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;
    iget v1, v0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->sequenceNumber:I

    iget v2, v0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->providerOrder:I

    iget-object v3, v0, Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/books/app/InfoCardsHelper;->onLoadedCards(IILcom/google/android/apps/books/util/ExceptionOr;)V

    .line 215
    iget v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNextDispatch:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNextDispatch:I

    goto :goto_0

    .line 217
    .end local v0    # "result":Lcom/google/android/apps/books/app/InfoCardsHelper$ProviderCallbackResult;
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mAllCardsRequested:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNextDispatch:I

    iget v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNumCardRequests:I

    if-lt v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mIfNoCards:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mIfNoCards:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 222
    :cond_1
    return-void
.end method

.method private onAllCardsRequested()V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mAllCardsRequested:Z

    .line 194
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->maybeLoadCards()V

    .line 195
    return-void
.end method

.method private onLoadedCards(IILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .param p1, "sequenceNumber"    # I
    .param p2, "providerOrder"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p3, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;"
    const/4 v1, 0x3

    .line 272
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I

    if-eq p1, v0, :cond_1

    .line 274
    const-string v0, "InfoCardsHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "InfoCardsHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring cards for provider "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " due to wrong sequence number"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mWaitingForHideAnimation:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    if-eqz v0, :cond_3

    .line 283
    const-string v0, "InfoCardsHelper"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    const-string v1, "InfoCardsHelper"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " cards for provider "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_2
    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->addCards(ILjava/util/List;)V

    goto :goto_0

    .line 289
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderOrderToPendingCards:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public onCardsLayoutHidden()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 245
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mWaitingForHideAnimation:Z

    .line 246
    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    if-eqz v2, :cond_2

    .line 247
    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsLayout:Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/ublib/infocards/SuggestionGridLayout;->removeAllViews()V

    .line 249
    iput v3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardsCount:I

    .line 252
    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderOrderToPendingCards:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 253
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Landroid/view/View;>;>;"
    const-string v2, "InfoCardsHelper"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 254
    const-string v3, "InfoCardsHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " cards for provider "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " after hiding previous cards"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-direct {p0, v3, v2}, Lcom/google/android/apps/books/app/InfoCardsHelper;->addCards(ILjava/util/List;)V

    goto :goto_0

    .line 259
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Landroid/view/View;>;>;"
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderOrderToPendingCards:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 261
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mIfNoCards:Ljava/lang/Runnable;

    .line 230
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mAllCardsRequested:Z

    .line 231
    iput v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNumCardRequests:I

    .line 232
    iput v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mNextDispatch:I

    .line 233
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderOrderToPendingCards:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mProviderCallbackResults:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->clear()V

    .line 236
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mWaitingForHideAnimation:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;->hideCardsLayout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mWaitingForHideAnimation:Z

    .line 239
    :cond_0
    return-void
.end method

.method public setTextSelection(Lcom/google/android/apps/books/app/SelectionState;)V
    .locals 7
    .param p1, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    .line 129
    iget v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I

    .line 130
    .local v2, "newSequenceNumber":I
    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v5, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mInitialStateForNextCards:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 132
    iget-object v5, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardProviders:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/app/InfoCardProvider;

    .line 133
    .local v3, "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    invoke-interface {p1}, Lcom/google/android/apps/books/app/SelectionState;->getSelectedText()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->createViewConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/google/android/apps/books/app/InfoCardProvider;->loadCards(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 134
    invoke-virtual {v3}, Lcom/google/android/apps/books/app/InfoCardProvider;->getSelectableLayerId()Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "selectableLayerId":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->createViewConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    .line 137
    .local v0, "annotationConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    invoke-interface {v5, v4}, Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;->getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/books/app/InfoCardsHelper$2;

    invoke-direct {v6, p0, v0, v2, v3}, Lcom/google/android/apps/books/app/InfoCardsHelper$2;-><init>(Lcom/google/android/apps/books/app/InfoCardsHelper;Lcom/google/android/ublib/utils/Consumer;ILcom/google/android/apps/books/app/InfoCardProvider;)V

    invoke-interface {p1, v5, v6}, Lcom/google/android/apps/books/app/SelectionState;->loadMatchingAnnotation(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 153
    .end local v0    # "annotationConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    .end local v3    # "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    .end local v4    # "selectableLayerId":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->onAllCardsRequested()V

    .line 154
    return-void
.end method

.method public showCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 3
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    .line 158
    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->TAPPED:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mInitialStateForNextCards:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 160
    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardProviders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/InfoCardProvider;

    .line 161
    .local v1, "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->createViewConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/books/app/InfoCardProvider;->loadCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 163
    .end local v1    # "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->onAllCardsRequested()V

    .line 164
    return-void
.end method

.method public showEndOfBookCards(Z)V
    .locals 3
    .param p1, "fromEobb"    # Z

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    .line 169
    sget-object v2, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mInitialStateForNextCards:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 171
    iget-object v2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardProviders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/InfoCardProvider;

    .line 172
    .local v1, "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->createViewConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/books/app/InfoCardProvider;->loadEndOfBookCards(ZLcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 174
    .end local v1    # "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->onAllCardsRequested()V

    .line 175
    return-void
.end method

.method public showLinkCards(Lcom/google/android/apps/books/render/TouchableItem;FLjava/lang/String;ILjava/lang/Runnable;)V
    .locals 7
    .param p1, "item"    # Lcom/google/android/apps/books/render/TouchableItem;
    .param p2, "textZoom"    # F
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "screenHeight"    # I
    .param p5, "ifNoCards"    # Ljava/lang/Runnable;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    .line 180
    iput-object p5, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mIfNoCards:Ljava/lang/Runnable;

    .line 182
    sget-object v1, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    iput-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mInitialStateForNextCards:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper;->mCardProviders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/InfoCardProvider;

    .line 185
    .local v0, "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->createViewConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v5

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/app/InfoCardProvider;->loadLinkCards(Lcom/google/android/apps/books/render/TouchableItem;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 187
    .end local v0    # "provider":Lcom/google/android/apps/books/app/InfoCardProvider;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->onAllCardsRequested()V

    .line 188
    return-void
.end method
