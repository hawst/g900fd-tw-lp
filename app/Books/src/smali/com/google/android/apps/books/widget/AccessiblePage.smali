.class public Lcom/google/android/apps/books/widget/AccessiblePage;
.super Landroid/widget/EditText;
.source "AccessiblePage.java"


# instance fields
.field private mInSaveInstanceState:Z

.field private mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field private mRenderedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field private mSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public getPageText(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/lang/String;
    .locals 1
    .param p1, "renderedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 71
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering;->pageText:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 72
    :cond_0
    const-string v0, ""

    .line 74
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering;->pageText:Ljava/lang/String;

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 107
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mInSaveInstanceState:Z

    .line 108
    invoke-super {p0}, Landroid/widget/EditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 109
    .local v0, "state":Landroid/os/Parcelable;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mInSaveInstanceState:Z

    .line 110
    return-object v0
.end method

.method protected onSelectionChanged(II)V
    .locals 5
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 83
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 84
    .local v0, "left":I
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 86
    .local v1, "right":I
    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mInSaveInstanceState:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    if-eqz v2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mRenderedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-interface {v2, v3, v4, v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;->onAccessibleSelectionChanged(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;II)V

    .line 90
    :cond_0
    return-void
.end method

.method public setPageRendering(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 3
    .param p1, "renderedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 48
    iget-object v2, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mRenderedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-eq v2, p1, :cond_0

    .line 49
    iput-object p1, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mRenderedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 50
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/AccessiblePage;->getPageText(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "newText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AccessiblePage;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "oldText":Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AccessiblePage;->setText(Ljava/lang/CharSequence;)V

    .line 67
    .end local v0    # "newText":Ljava/lang/String;
    .end local v1    # "oldText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setup(Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AccessiblePage;->setTextIsSelectable(Z)V

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/books/widget/AccessiblePage;->mPagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 45
    return-void
.end method
