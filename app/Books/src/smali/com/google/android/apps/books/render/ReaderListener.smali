.class public interface abstract Lcom/google/android/apps/books/render/ReaderListener;
.super Ljava/lang/Object;
.source "ReaderListener.java"


# virtual methods
.method public abstract d(Ljava/lang/String;)V
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract onActivatedMoElement(IILjava/lang/String;)V
.end method

.method public abstract onChapterReady(ILcom/google/android/apps/books/util/JsPerformanceMetrics;)V
.end method

.method public abstract onChapterUnloaded(I)V
.end method

.method public abstract onDocumentChanged()V
.end method

.method public abstract onError(Ljava/lang/Throwable;)V
.end method

.method public abstract onInvalidPosition(II)V
.end method

.method public abstract onJsApiReady()V
.end method

.method public abstract onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
.end method

.method public abstract onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onMissingPosition(I)V
.end method

.method public abstract onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
.end method

.method public abstract onPageData(IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onPageLoaded(III)V
.end method

.method public abstract onPageRangeReady(II)V
.end method

.method public abstract onPassageMoListReady(IILjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
.end method

.method public abstract onPassagesPurged(ILjava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onPreferencesApplied()V
.end method

.method public abstract onReaderInitialized()V
.end method

.method public abstract onReaderReady()V
.end method
