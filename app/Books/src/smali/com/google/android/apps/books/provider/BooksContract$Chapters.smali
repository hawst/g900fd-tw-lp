.class public final Lcom/google/android/apps/books/provider/BooksContract$Chapters;
.super Ljava/lang/Object;
.source "BooksContract.java"

# interfaces
.implements Lcom/google/android/apps/books/provider/BooksContract$ChapterColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Chapters"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161
    return-void
.end method

.method public static buildChapterDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2207
    const-string v0, "Missing Account"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208
    iget-object v0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->buildChapterDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildChapterDirUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2173
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->builder([Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildChapterUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "chapterId"    # Ljava/lang/String;

    .prologue
    .line 2219
    const-string v0, "Missing Account"

    invoke-static {p0, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->builder([Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildChapterUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "baseUri"    # Landroid/net/Uri;
    .param p1, "chapterId"    # Ljava/lang/String;

    .prologue
    .line 2193
    const-string v0, "Valid chapter ID required"

    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2194
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->getAccountName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->getVolumeId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->buildChapterUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildChapterUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "chapterId"    # Ljava/lang/String;

    .prologue
    .line 2184
    const-string v0, "Valid chapter ID required"

    invoke-static {p2, v0}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2185
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->VOLUMES_CHAPTERS_ID:Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->builder([Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getAccountName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2224
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->ACCOUNT_NAME:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->valueFrom(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static getInfo(Landroid/net/Uri;Z)Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "getChapterId"    # Z

    .prologue
    .line 2260
    invoke-static {p0}, Lcom/google/android/apps/books/provider/BooksContract;->getUriType(Landroid/net/Uri;)Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    move-result-object v2

    .line 2261
    .local v2, "type":Lcom/google/android/apps/books/provider/BooksContract$BooksUri;
    sget-object v4, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->ACCOUNT_NAME:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-virtual {v2, p0, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getWildcard(Landroid/net/Uri;Lcom/google/android/apps/books/provider/BooksContract$PathParam;)Ljava/lang/String;

    move-result-object v0

    .line 2262
    .local v0, "accountName":Ljava/lang/String;
    sget-object v4, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-virtual {v2, p0, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getWildcard(Landroid/net/Uri;Lcom/google/android/apps/books/provider/BooksContract$PathParam;)Ljava/lang/String;

    move-result-object v3

    .line 2263
    .local v3, "volumeId":Ljava/lang/String;
    sget-object v4, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->CHAPTER_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-virtual {v2, p0, v4}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getWildcard(Landroid/net/Uri;Lcom/google/android/apps/books/provider/BooksContract$PathParam;)Ljava/lang/String;

    move-result-object v1

    .line 2264
    .local v1, "chapterId":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 2265
    invoke-static {v1}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267
    :cond_0
    new-instance v4, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;

    invoke-direct {v4, v0, v3, v1}, Lcom/google/android/apps/books/provider/BooksContract$Chapters$Info;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method public static getVolumeId(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2240
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->valueFrom(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
