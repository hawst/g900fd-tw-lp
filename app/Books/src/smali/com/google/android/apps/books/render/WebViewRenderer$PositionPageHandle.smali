.class Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;
.super Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;
.source "WebViewRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositionPageHandle"
.end annotation


# instance fields
.field private final mPosition:Lcom/google/android/apps/books/common/Position;

.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRenderer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/common/Position;)V
    .locals 1
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 596
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    .line 597
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 598
    iput-object p2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    .line 599
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/WebViewRenderer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;
    .param p2, "x1"    # Lcom/google/android/apps/books/common/Position;
    .param p3, "x2"    # Lcom/google/android/apps/books/render/WebViewRenderer$1;

    .prologue
    .line 592
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/common/Position;)V

    return-void
.end method


# virtual methods
.method public createSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .locals 1

    .prologue
    .line 626
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFirstBookPageIndex()I
    .locals 2

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    # invokes: Lcom/google/android/apps/books/render/WebViewRenderer;->getFirstBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$600(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method

.method public getFirstChapterIndex()I
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method

.method public getGridRowIndex()I
    .locals 3

    .prologue
    .line 613
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    .line 614
    .local v0, "segmentIndex":I
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$700(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->rightIndexToLeftIndex(I)I

    move-result v1

    return v1
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$PositionPageHandle;->mPosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method
