.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

.field final synthetic val$hasOfflineLicense:Z

.field final synthetic val$pinned:Z

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->val$volumeId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->val$pinned:Z

    iput-boolean p4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->val$hasOfflineLicense:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$000(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->val$volumeId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->val$pinned:Z

    iget-boolean v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;->val$hasOfflineLicense:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V

    .line 238
    return-void
.end method
