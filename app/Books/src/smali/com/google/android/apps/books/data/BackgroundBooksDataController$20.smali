.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getSegmentContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$contentConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$ignoreResources:Z

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$resourcesConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$segment:Lcom/google/android/apps/books/model/Segment;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$contentConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$resourcesConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-boolean p7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$ignoreResources:Z

    iput-object p8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSegmentContentSubcontroller:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$segment:Lcom/google/android/apps/books/model/Segment;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$contentConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$resourcesConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-boolean v7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$ignoreResources:Z

    iget-object v8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getSegmentContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 827
    return-void
.end method
