.class public interface abstract Lcom/google/android/apps/books/model/Clock;
.super Ljava/lang/Object;
.source "Clock.java"


# static fields
.field public static final MONOTONIC_CLOCK:Lcom/google/android/apps/books/model/Clock;

.field public static final SYSTEM_CLOCK:Lcom/google/android/apps/books/model/Clock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    new-instance v0, Lcom/google/android/apps/books/model/Clock$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/Clock$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/model/Clock;->SYSTEM_CLOCK:Lcom/google/android/apps/books/model/Clock;

    .line 32
    new-instance v0, Lcom/google/android/apps/books/model/Clock$2;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/Clock$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/model/Clock;->MONOTONIC_CLOCK:Lcom/google/android/apps/books/model/Clock;

    return-void
.end method


# virtual methods
.method public abstract currentTime()J
.end method

.method public abstract timeUnitsPerSecond()J
.end method
