.class public Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;
.super Ljava/lang/Object;
.source "JsonAnnotation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Annotations"
.end annotation


# instance fields
.field public error:Lcom/google/android/apps/books/api/data/JsonError;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "error"
    .end annotation
.end field

.field public items:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "items"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public nextPageToken:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "nextPageToken"
    .end annotation
.end field

.field public totalItems:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "totalItems"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
