.class Lcom/google/android/apps/books/data/OffersSubcontroller$3;
.super Ljava/lang/Object;
.source "OffersSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/OffersSubcontroller;->saveDismissedOfferOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

.field final synthetic val$offerId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->val$offerId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mDismissedOffers:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$200(Lcom/google/android/apps/books/data/OffersSubcontroller;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->val$offerId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mDismissedOffers:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$200(Lcom/google/android/apps/books/data/OffersSubcontroller;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->setDismissedOffers(Ljava/util/Set;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->getFilteredCachedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$300(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/high16 v4, -0x8000000000000000L

    move-object v1, p1

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->notifyListeners(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$400(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v6

    .line 208
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "OffersSubcontroller"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    const-string v0, "OffersSubcontroller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error saving dismissed offers: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
