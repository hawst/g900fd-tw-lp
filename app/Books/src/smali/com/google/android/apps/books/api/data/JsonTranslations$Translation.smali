.class public Lcom/google/android/apps/books/api/data/JsonTranslations$Translation;
.super Ljava/lang/Object;
.source "JsonTranslations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/data/JsonTranslations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Translation"
.end annotation


# instance fields
.field public detectedSourceLanguage:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "detectedSourceLanguage"
    .end annotation
.end field

.field public translatedText:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "translatedText"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
