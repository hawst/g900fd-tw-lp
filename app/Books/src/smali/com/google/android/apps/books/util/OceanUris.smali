.class public Lcom/google/android/apps/books/util/OceanUris;
.super Ljava/lang/Object;
.source "OceanUris.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/OceanUris$BookStore;
    }
.end annotation


# static fields
.field private static final sFinsky:Lcom/google/android/apps/books/util/OceanUris$BookStore;

.field private static final sOceanWeb:Lcom/google/android/apps/books/util/OceanUris$BookStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/apps/books/util/OceanUris$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/OceanUris$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/OceanUris;->sOceanWeb:Lcom/google/android/apps/books/util/OceanUris$BookStore;

    .line 123
    new-instance v0, Lcom/google/android/apps/books/util/OceanUris$2;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/OceanUris$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/OceanUris;->sFinsky:Lcom/google/android/apps/books/util/OceanUris$BookStore;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris;->fallbackStoreCollectionUri(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/OceanUris;->finskyWithCampaign(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static appendAuthorAndTitle(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "builder"    # Landroid/net/Uri$Builder;
    .param p1, "author"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 446
    if-eqz p1, :cond_0

    .line 447
    const-string v0, "a"

    invoke-virtual {p0, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 449
    :cond_0
    if-eqz p2, :cond_1

    .line 450
    const-string v0, "t"

    invoke-virtual {p0, v0, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 452
    :cond_1
    return-void
.end method

.method public static appendCampaignId(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 507
    if-nez p0, :cond_0

    .line 508
    const/4 v0, 0x0

    .line 510
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/OceanUris;->buildUponWithCampaign(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static appendCampaignId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 500
    if-nez p0, :cond_0

    .line 501
    const/4 v0, 0x0

    .line 503
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/OceanUris;->appendCampaignId(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static appendFifeHeight(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "height"    # I

    .prologue
    .line 551
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/OceanUris;->buildUponWithFifeSize(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static appendFifeSizeOption(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;

    .prologue
    .line 531
    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 532
    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/books/util/OceanUris;->appendFifeWidthHeight(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object p0

    .line 543
    .end local p0    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-object p0

    .line 536
    .restart local p0    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 537
    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/OceanUris;->appendFifeWidth(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    .line 540
    :cond_2
    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/OceanUris;->appendFifeHeight(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0
.end method

.method private static appendFifeWidth(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "width"    # I

    .prologue
    .line 547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/OceanUris;->buildUponWithFifeSize(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static appendFifeWidthHeight(Landroid/net/Uri;II)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/OceanUris;->buildUponWithFifeSize(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static appendSourceAndVersion(Landroid/net/Uri$Builder;Lcom/google/android/apps/books/util/Config;)V
    .locals 3
    .param p0, "builder"    # Landroid/net/Uri$Builder;
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 431
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/Config;->getSourceParam()Ljava/lang/String;

    move-result-object v0

    .line 432
    .local v0, "sourceParam":Ljava/lang/String;
    const-string v2, "is"

    invoke-virtual {p0, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 433
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/Config;->getVersionString()Ljava/lang/String;

    move-result-object v1

    .line 434
    .local v1, "versionString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 435
    const-string v2, "av"

    invoke-virtual {p0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 437
    :cond_0
    return-void
.end method

.method private static appendVolumeId(Landroid/net/Uri$Builder;Ljava/lang/String;)V
    .locals 1
    .param p0, "builder"    # Landroid/net/Uri$Builder;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 440
    if-eqz p1, :cond_0

    .line 441
    const-string v0, "v"

    invoke-virtual {p0, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 443
    :cond_0
    return-void
.end method

.method public static buildPageUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "books"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "volumes"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "image"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "start"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "num"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "oos"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildSearchIntent(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-static {p0}, Lcom/google/android/apps/books/util/OceanUris;->getBookStore(Lcom/google/android/apps/books/util/Config;)Lcom/google/android/apps/books/util/OceanUris$BookStore;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getSearchUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static buildUponWithCampaign(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 514
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 515
    .local v0, "builder":Landroid/net/Uri$Builder;
    if-eqz p1, :cond_0

    .line 516
    const-string v1, "pcampaignid"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 518
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    :cond_0
    return-object v0
.end method

.method private static buildUponWithFifeSize(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "sizeOption"    # Ljava/lang/String;

    .prologue
    .line 559
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 560
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "fife"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private static fallbackStoreCollectionUri(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "collectionId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 493
    const-string v0, "OceanUris"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    const-string v0, "OceanUris"

    const-string v1, "Shouldn\'t be asked to open a store collection on a non-Phonesky device."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris;->localFinskyCollectionUri(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static finskyWithCampaign(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getPlayStoreBaseUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/OceanUris;->buildUponWithCampaign(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static getAboutTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-static {p0}, Lcom/google/android/apps/books/util/OceanUris;->getBookStore(Lcom/google/android/apps/books/util/Config;)Lcom/google/android/apps/books/util/OceanUris$BookStore;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getAboutTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getBookStore(Lcom/google/android/apps/books/util/Config;)Lcom/google/android/apps/books/util/OceanUris$BookStore;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->isLargeDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/OceanUris;->sFinsky:Lcom/google/android/apps/books/util/OceanUris$BookStore;

    .line 182
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/books/util/OceanUris;->sOceanWeb:Lcom/google/android/apps/books/util/OceanUris$BookStore;

    goto :goto_0
.end method

.method public static getBuyTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 203
    invoke-static {p0}, Lcom/google/android/apps/books/util/OceanUris;->getBookStore(Lcom/google/android/apps/books/util/Config;)Lcom/google/android/apps/books/util/OceanUris$BookStore;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getBuyTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getCollectionVolumesUrl(Lcom/google/android/apps/books/util/Config;J)Landroid/net/Uri;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "collectionId"    # J

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getBaseGDataUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "books"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "me"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "collections"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "volumes"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getContentBlockedUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 378
    const-string v0, "http://support.google.com/mobile/?p=books_multipledevices"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getFileUploadLearnMoreUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 386
    const-string v0, "https://support.google.com/googleplay/?p=books_upload"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static getFinskyShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 241
    sget-object v0, Lcom/google/android/apps/books/util/OceanUris;->sFinsky:Lcom/google/android/apps/books/util/OceanUris$BookStore;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getHelpCenterUrl()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 366
    const-string v0, "http://support.google.com/mobile/?p=books_androidapp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getInternationalAvailabilityUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 382
    const-string v0, "http://support.google.com/mobile/?p=books_international"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getManifestUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "books"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "volumes"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "content"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "manifest"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "alt"

    const-string v3, "xml"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hk"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ev"

    const-string v3, "3a"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ef"

    const-string v3, "3"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ftc"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "oos"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "multi_toc"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 301
    .local v0, "uriBuilder":Landroid/net/Uri$Builder;
    const-string v1, "av"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 304
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    .line 306
    const-string v1, "svg"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 319
    :goto_0
    const-string v1, "math"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 322
    const-string v1, "flvers"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 325
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 326
    const-string v1, "vert"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 329
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/util/Config;->prepareForOcean(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 310
    :cond_1
    const-string v1, "svg"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 313
    const-string v1, "dfo"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public static getOfflineLimitUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "author"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 474
    const-string v2, "http://support.google.com/mobile/?p=books_offlinelimit"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 475
    .local v0, "bare":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 476
    .local v1, "builder":Landroid/net/Uri$Builder;
    invoke-static {v1, p0}, Lcom/google/android/apps/books/util/OceanUris;->appendSourceAndVersion(Landroid/net/Uri$Builder;Lcom/google/android/apps/books/util/Config;)V

    .line 477
    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/OceanUris;->appendVolumeId(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    .line 478
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/books/util/OceanUris;->appendAuthorAndTitle(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public static getPrivacyUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 362
    const-string v0, "http://support.google.com/mobile/?p=market_privacy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getShareUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 252
    sget-object v0, Lcom/google/android/apps/books/util/OceanUris;->sFinsky:Lcom/google/android/apps/books/util/OceanUris$BookStore;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getAboutTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 195
    invoke-static {p0}, Lcom/google/android/apps/books/util/OceanUris;->getBookStore(Lcom/google/android/apps/books/util/Config;)Lcom/google/android/apps/books/util/OceanUris$BookStore;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getStoreCollectionUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "collectionId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 207
    invoke-static {p0}, Lcom/google/android/apps/books/util/OceanUris;->getBookStore(Lcom/google/android/apps/books/util/Config;)Lcom/google/android/apps/books/util/OceanUris$BookStore;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/apps/books/util/OceanUris$BookStore;->getStoreCollectionUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getTermsOfServiceUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://play.google.com/intl/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/about/play-terms.html"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static localFinskyCollectionUri(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p1, "collectionId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getPlayStoreBaseUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/books/util/OceanUris;->buildUponWithCampaign(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "books"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "collection"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
