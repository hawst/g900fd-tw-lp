.class public interface abstract Lcom/google/android/apps/books/net/ResponseGetter;
.super Ljava/lang/Object;
.source "ResponseGetter.java"


# virtual methods
.method public varargs abstract execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public varargs abstract get(Ljava/lang/String;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
