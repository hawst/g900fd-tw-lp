.class public Lcom/google/android/apps/books/app/HomeFragment$UnPinDialog;
.super Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnPinDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2250
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected onOkClicked(Z)V
    .locals 5
    .param p1, "checkboxChecked"    # Z

    .prologue
    .line 2254
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$UnPinDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 2255
    .local v1, "args":Landroid/os/Bundle;
    invoke-static {v1}, Lcom/google/android/apps/books/util/VolumeArguments;->getId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 2256
    .local v2, "volumeId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$UnPinDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2258
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 2267
    :goto_0
    return-void

    .line 2262
    :cond_0
    if-eqz p1, :cond_1

    .line 2263
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_SUPPRESS_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2264
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/preference/LocalPreferences;->setShowUnpinDialog(Z)V

    .line 2266
    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startUnpinProcess(Ljava/lang/String;)V

    goto :goto_0
.end method
