.class Lcom/google/android/apps/books/app/HomeFragment$15;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->setupMyLibraryHeader()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 1163
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$15;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 1180
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "v"    # F
    .param p3, "i2"    # I

    .prologue
    .line 1166
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 1172
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$15;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/google/android/apps/books/app/HomeFragment;->access$2400(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0208

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment$15;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mPagerAdapter:Landroid/support/v4/view/PagerAdapter;
    invoke-static {v5}, Lcom/google/android/apps/books/app/HomeFragment;->access$2500(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/PagerAdapter;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1175
    .local v0, "tabString":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$15;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$15;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2400(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/ublib/utils/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 1176
    return-void
.end method
