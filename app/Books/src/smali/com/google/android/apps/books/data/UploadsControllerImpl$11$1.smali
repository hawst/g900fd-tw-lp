.class Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

.field final synthetic val$uploads:Lcom/google/android/apps/books/upload/Upload$Uploads;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$11;Lcom/google/android/apps/books/upload/Upload$Uploads;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->val$uploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->val$uploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

    # setter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$702(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    .line 407
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # invokes: Lcom/google/android/apps/books/data/UploadsControllerImpl;->applyUpdates()V
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$800(Lcom/google/android/apps/books/data/UploadsControllerImpl;)V

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v1, v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-static {v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$700(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    iget-object v0, v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mDataController:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$300(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1$1;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->notifyListeners(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;)V

    .line 418
    return-void
.end method
