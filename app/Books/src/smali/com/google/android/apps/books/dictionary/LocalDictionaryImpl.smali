.class public Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;
.super Ljava/lang/Object;
.source "LocalDictionaryImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/dictionary/LocalDictionary;


# instance fields
.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDecryptCipher:Ljavax/crypto/Cipher;

.field private mEncryptCipher:Ljavax/crypto/Cipher;

.field private final mLocale:Ljava/util/Locale;

.field private mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method public constructor <init>(Ljava/io/File;[BLjava/lang/String;)V
    .locals 6
    .param p1, "file"    # Ljava/io/File;
    .param p2, "key"    # [B
    .param p3, "languageCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 47
    .local v0, "iv":[B
    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 49
    const-string v1, "AES/CBC/PKCS5Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mEncryptCipher:Ljavax/crypto/Cipher;

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mEncryptCipher:Ljavax/crypto/Cipher;

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v2, p2, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v3, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v1, v5, v2, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 52
    const-string v1, "AES/CBC/PKCS5Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDecryptCipher:Ljavax/crypto/Cipher;

    .line 53
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDecryptCipher:Ljavax/crypto/Cipher;

    const/4 v2, 0x2

    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, p2, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v1, v2, v3, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 55
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "select ENTRY from DICT where TERM =?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;

    .line 59
    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mLocale:Ljava/util/Locale;

    .line 60
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 98
    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 102
    iput-object v1, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 104
    :cond_1
    return-void
.end method

.method public lookup(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .locals 13
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 65
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mLocale:Ljava/util/Locale;

    invoke-virtual {p1, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 66
    const/4 v4, 0x0

    .line 67
    .local v4, "pfd":Landroid/os/ParcelFileDescriptor;
    const/4 v2, 0x0

    .line 69
    .local v2, "fileStream":Ljava/io/InputStream;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mEncryptCipher:Ljavax/crypto/Cipher;

    const-string v9, "UTF-8"

    invoke-virtual {p1, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v9

    invoke-virtual {v8, v9}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    .line 70
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mTermQueryStmt:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForBlobFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    .line 71
    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .end local v2    # "fileStream":Ljava/io/InputStream;
    .local v3, "fileStream":Ljava/io/InputStream;
    :try_start_1
    iget-object v6, p0, Lcom/google/android/apps/books/dictionary/LocalDictionaryImpl;->mDecryptCipher:Ljavax/crypto/Cipher;

    invoke-static {v3}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 74
    .local v0, "decryptedEntry":[B
    invoke-static {v0}, Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;->parseFrom([B)Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/provider/DataConversions;->offlineEntryToDictionaryEntry(Lcom/google/ocean/offline/dict/OfflineDictEntry$OfflineEntry;)Lcom/google/android/apps/books/annotations/DictionaryEntry;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 88
    new-array v6, v12, [Ljava/io/Closeable;

    aput-object v3, v6, v11

    aput-object v4, v6, v10

    invoke-static {v6}, Lcom/google/android/apps/books/util/IOUtils;->close([Ljava/io/Closeable;)V

    move-object v2, v3

    .end local v0    # "decryptedEntry":[B
    .end local v3    # "fileStream":Ljava/io/InputStream;
    .restart local v2    # "fileStream":Ljava/io/InputStream;
    :goto_0
    return-object v5

    .line 77
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Landroid/database/sqlite/SQLiteDoneException;
    :goto_1
    :try_start_2
    const-string v6, "LocalDictionaryImpl"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 79
    const-string v6, "LocalDictionaryImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Couldn\'t find a match for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 88
    :cond_0
    new-array v6, v12, [Ljava/io/Closeable;

    aput-object v2, v6, v11

    aput-object v4, v6, v10

    invoke-static {v6}, Lcom/google/android/apps/books/util/IOUtils;->close([Ljava/io/Closeable;)V

    goto :goto_0

    .line 82
    .end local v1    # "e":Landroid/database/sqlite/SQLiteDoneException;
    :catch_1
    move-exception v1

    .line 83
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v6, "LocalDictionaryImpl"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 84
    const-string v6, "LocalDictionaryImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "An exception occured during lookup for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 88
    :cond_1
    new-array v6, v12, [Ljava/io/Closeable;

    aput-object v2, v6, v11

    aput-object v4, v6, v10

    invoke-static {v6}, Lcom/google/android/apps/books/util/IOUtils;->close([Ljava/io/Closeable;)V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    :goto_3
    new-array v6, v12, [Ljava/io/Closeable;

    aput-object v2, v6, v11

    aput-object v4, v6, v10

    invoke-static {v6}, Lcom/google/android/apps/books/util/IOUtils;->close([Ljava/io/Closeable;)V

    throw v5

    .end local v2    # "fileStream":Ljava/io/InputStream;
    .restart local v3    # "fileStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fileStream":Ljava/io/InputStream;
    .restart local v2    # "fileStream":Ljava/io/InputStream;
    goto :goto_3

    .line 82
    .end local v2    # "fileStream":Ljava/io/InputStream;
    .restart local v3    # "fileStream":Ljava/io/InputStream;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "fileStream":Ljava/io/InputStream;
    .restart local v2    # "fileStream":Ljava/io/InputStream;
    goto :goto_2

    .line 77
    .end local v2    # "fileStream":Ljava/io/InputStream;
    .restart local v3    # "fileStream":Ljava/io/InputStream;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "fileStream":Ljava/io/InputStream;
    .restart local v2    # "fileStream":Ljava/io/InputStream;
    goto :goto_1
.end method
