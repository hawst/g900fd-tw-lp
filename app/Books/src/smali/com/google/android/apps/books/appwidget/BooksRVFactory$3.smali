.class Lcom/google/android/apps/books/appwidget/BooksRVFactory$3;
.super Ljava/lang/Object;
.source "BooksAppWidgetService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/appwidget/BooksRVFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$3;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-static {p2}, Lcom/google/android/apps/books/preference/LocalPreferences;->isAccountKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    const-string v0, "BooksAppWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const-string v0, "BooksAppWidget"

    const-string v1, "onSharedPreferenceChanged() triggered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$3;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    # invokes: Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadAccountFromPreferences()V
    invoke-static {v0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->access$300(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$3;->this$0:Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    # invokes: Lcom/google/android/apps/books/appwidget/BooksRVFactory;->update()V
    invoke-static {v0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->access$000(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V

    .line 147
    :cond_1
    return-void
.end method
