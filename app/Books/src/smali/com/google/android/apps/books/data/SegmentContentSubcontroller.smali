.class public Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
.super Ljava/lang/Object;
.source "SegmentContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    }
.end annotation


# instance fields
.field private final mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContentIdToResult:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;",
            ">;"
        }
    .end annotation
.end field

.field private final mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

.field private final mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field

.field private final mWidthString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/EncryptionScheme;)V
    .locals 3
    .param p1, "encryptionScheme"    # Lcom/google/android/apps/books/data/EncryptionScheme;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 54
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 56
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 75
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    .line 78
    iput-object p1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    .line 93
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->deviceHasTooManyPixels()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 94
    .local v0, "factor":F
    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/books/util/ReaderUtils;->maxPictureWidth(F)I

    move-result v1

    .line 95
    .local v1, "width":I
    const/16 v2, 0x640

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 96
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mWidthString:Ljava/lang/String;

    .line 97
    return-void

    .line 93
    .end local v0    # "factor":F
    .end local v1    # "width":I
    :cond_0
    const v0, 0x3faccccc    # 1.3499999f

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Segment;
    .param p4, "x4"    # Ljava/lang/Runnable;
    .param p5, "x5"    # Z
    .param p6, "x6"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 45
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/Segment;
    .param p4, "x4"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p5, "x5"    # Ljava/lang/Runnable;
    .param p6, "x6"    # Z
    .param p7, "x7"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p8, "x8"    # Z
    .param p9, "x9"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 45
    invoke-direct/range {p0 .. p9}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mWidthString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p4, "x4"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p5, "x5"    # Ljava/lang/Runnable;

    .prologue
    .line 45
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    .param p4, "x4"    # Z

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V

    return-void
.end method

.method private getContentOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;ZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 11
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p5, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p6, "haveContentConsumers"    # Z
    .param p7, "saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p8, "ignoreResources"    # Z
    .param p9, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/Segment;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/Runnable;",
            "Z",
            "Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;",
            "Z",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 227
    .local p4, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;

    iget-object v3, p2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    move-object v1, p0

    move-object/from16 v2, p9

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move/from16 v7, p8

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalSessionKey;ZZLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 279
    return-void
.end method

.method private getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 322
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$6;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    invoke-interface {p1, p3, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 338
    return-void
.end method

.method private getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 8
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p4, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "ignoreResources"    # Z
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 211
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V

    .line 212
    return-void
.end method

.method private handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p4, "e"    # Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;
    .param p5, "continuation"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 284
    .local p3, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$4;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 295
    return-void
.end method

.method private publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 441
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 429
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$8;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$8;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 435
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/sync/VolumeContentId;",
            "Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p4, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    .local p5, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    iget-object v7, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    iget-object v8, p3, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->resources:Ljava/util/List;

    invoke-virtual {v7, p2, v8}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 388
    iget-object v7, p3, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->resources:Ljava/util/List;

    invoke-static {p5, v7}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverSuccess(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Object;)V

    .line 390
    iget-object v7, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v7, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->hasConsumersForKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz p4, :cond_3

    .line 391
    :cond_0
    const/4 v2, 0x0

    .line 392
    .local v2, "encrypted":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 393
    .local v0, "cleartext":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 395
    .local v3, "inflated":Ljava/io/InputStream;
    :try_start_0
    iget-object v7, p3, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v7}, Lcom/google/android/apps/books/data/DataControllerBlob;->openInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 396
    iget-object v7, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mEncryptionScheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    iget-object v8, p3, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v8}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Lcom/google/android/apps/books/data/EncryptionScheme;->decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;

    move-result-object v0

    .line 397
    new-instance v4, Ljava/util/zip/InflaterInputStream;

    invoke-direct {v4, v0}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    .end local v3    # "inflated":Ljava/io/InputStream;
    .local v4, "inflated":Ljava/io/InputStream;
    :try_start_1
    new-instance v5, Lcom/google/android/apps/books/util/Buffer;

    invoke-direct {v5, v4}, Lcom/google/android/apps/books/util/Buffer;-><init>(Ljava/io/InputStream;)V

    .line 399
    .local v5, "payload":Lcom/google/android/apps/books/util/Buffer;
    iget-object v7, v5, Lcom/google/android/apps/books/util/Buffer;->data:[B

    iget v8, v5, Lcom/google/android/apps/books/util/Buffer;->bytes:I

    invoke-static {v7, v8}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([BI)Ljava/lang/String;

    move-result-object v6

    .line 400
    .local v6, "segmentText":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v7, p2, v6, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 412
    if-eqz v2, :cond_1

    .line 413
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 415
    :cond_1
    if-eqz v0, :cond_2

    .line 416
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 418
    :cond_2
    if-eqz v4, :cond_3

    .line 419
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a

    .line 425
    .end local v0    # "cleartext":Ljava/io/InputStream;
    .end local v2    # "encrypted":Ljava/io/InputStream;
    .end local v4    # "inflated":Ljava/io/InputStream;
    .end local v5    # "payload":Lcom/google/android/apps/books/util/Buffer;
    .end local v6    # "segmentText":Ljava/lang/String;
    :cond_3
    :goto_0
    return-void

    .line 401
    .restart local v0    # "cleartext":Ljava/io/InputStream;
    .restart local v2    # "encrypted":Ljava/io/InputStream;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 402
    .local v1, "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    :goto_1
    :try_start_3
    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 412
    if-eqz v2, :cond_4

    .line 413
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 415
    :cond_4
    if-eqz v0, :cond_5

    .line 416
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 418
    :cond_5
    if-eqz v3, :cond_3

    .line 419
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 421
    :catch_1
    move-exception v7

    goto :goto_0

    .line 404
    .end local v1    # "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    :catch_2
    move-exception v1

    .line 405
    .local v1, "e":Ljava/security/GeneralSecurityException;
    :goto_2
    :try_start_5
    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 412
    if-eqz v2, :cond_6

    .line 413
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 415
    :cond_6
    if-eqz v0, :cond_7

    .line 416
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 418
    :cond_7
    if-eqz v3, :cond_3

    .line 419
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 421
    :catch_3
    move-exception v7

    goto :goto_0

    .line 407
    .end local v1    # "e":Ljava/security/GeneralSecurityException;
    :catch_4
    move-exception v1

    .line 408
    .local v1, "e":Ljava/io/IOException;
    :goto_3
    :try_start_7
    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 412
    if-eqz v2, :cond_8

    .line 413
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 415
    :cond_8
    if-eqz v0, :cond_9

    .line 416
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 418
    :cond_9
    if-eqz v3, :cond_3

    .line 419
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_0

    .line 421
    :catch_5
    move-exception v7

    goto :goto_0

    .line 411
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 412
    :goto_4
    if-eqz v2, :cond_a

    .line 413
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 415
    :cond_a
    if-eqz v0, :cond_b

    .line 416
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 418
    :cond_b
    if-eqz v3, :cond_c

    .line 419
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 422
    :cond_c
    :goto_5
    throw v7

    .line 421
    :catch_6
    move-exception v8

    goto :goto_5

    .line 411
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "inflated":Ljava/io/InputStream;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    goto :goto_4

    .line 407
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    :catch_7
    move-exception v1

    move-object v3, v4

    .end local v4    # "inflated":Ljava/io/InputStream;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    goto :goto_3

    .line 404
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    :catch_8
    move-exception v1

    move-object v3, v4

    .end local v4    # "inflated":Ljava/io/InputStream;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    goto :goto_2

    .line 401
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    :catch_9
    move-exception v1

    move-object v3, v4

    .end local v4    # "inflated":Ljava/io/InputStream;
    .restart local v3    # "inflated":Ljava/io/InputStream;
    goto :goto_1

    .line 421
    .end local v3    # "inflated":Ljava/io/InputStream;
    .restart local v4    # "inflated":Ljava/io/InputStream;
    .restart local v5    # "payload":Lcom/google/android/apps/books/util/Buffer;
    .restart local v6    # "segmentText":Ljava/lang/String;
    :catch_a
    move-exception v7

    goto :goto_0
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Z)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    .param p4, "save"    # Z

    .prologue
    const/4 v4, 0x0

    .line 347
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 349
    if-eqz p4, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$7;

    invoke-direct {v0, p0, p3, p2, p4}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$7;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 382
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0, p2, v1, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "contentId"    # Lcom/google/android/apps/books/sync/VolumeContentId;
    .param p3, "result"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    .prologue
    .line 299
    new-instance v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$5;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 318
    return-void
.end method


# virtual methods
.method public getSegmentContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 17
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p7, "ignoreResources"    # Z
    .param p8, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Segment;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;Z",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    .local p4, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    .local p5, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p6, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v4, Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v2}, Lcom/google/android/apps/books/sync/VolumeContentId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .local v4, "contentId":Lcom/google/android/apps/books/sync/VolumeContentId;
    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 107
    new-instance v14, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;

    const-string v2, "Segment is forbidden"

    invoke-direct {v14, v2}, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;-><init>(Ljava/lang/String;)V

    .line 108
    .local v14, "e":Ljava/lang/Exception;
    move-object/from16 v0, p4

    invoke-static {v0, v14}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 109
    move-object/from16 v0, p5

    invoke-static {v0, v14}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 110
    move-object/from16 v0, p6

    invoke-static {v0, v14}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    .line 171
    .end local v14    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentIdToResult:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;

    .line 115
    .local v5, "cachedResult":Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
    if-eqz v5, :cond_2

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    .line 116
    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 117
    const/16 p4, 0x0

    .line 118
    const/16 p5, 0x0

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    goto :goto_0

    .line 125
    :cond_2
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-interface/range {p3 .. p3}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getLocalSegmentState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 132
    .local v16, "localState":Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    if-nez p4, :cond_3

    .line 134
    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;->getStatus()Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    if-ne v2, v3, :cond_3

    .line 139
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverSuccess(Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Object;)V

    .line 141
    invoke-static/range {p6 .. p6}, Lcom/google/android/apps/books/util/ExceptionOr;->maybeDeliverOpaqueSuccess(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 127
    .end local v16    # "localState":Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    :catch_0
    move-exception v14

    .line 128
    .local v14, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0

    .line 146
    .end local v14    # "e":Ljava/io/IOException;
    .restart local v16    # "localState":Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    :cond_3
    const/4 v15, 0x0

    .line 147
    .local v15, "hadExistingConsumers":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mContentConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p4

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    or-int/2addr v15, v2

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mResourcesConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p5

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    or-int/2addr v15, v2

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p6

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_3
    or-int/2addr v15, v2

    .line 150
    if-nez v15, :cond_0

    .line 154
    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;->getStatus()Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    if-ne v2, v3, :cond_7

    .line 157
    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;)V

    goto/16 :goto_0

    .line 147
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 148
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 149
    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 161
    :cond_7
    new-instance v6, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$1;

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object v9, v4

    move-object/from16 v10, p3

    move/from16 v11, p7

    move-object/from16 v12, p8

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/SegmentContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .local v6, "expiredKeyContinuation":Ljava/lang/Runnable;
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object v9, v4

    move-object/from16 v10, p3

    move-object v11, v6

    move/from16 v12, p7

    move-object/from16 v13, p8

    .line 169
    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;->getKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/Segment;Ljava/lang/Runnable;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto/16 :goto_0
.end method
