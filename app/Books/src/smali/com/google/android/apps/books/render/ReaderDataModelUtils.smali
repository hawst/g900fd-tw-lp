.class public Lcom/google/android/apps/books/render/ReaderDataModelUtils;
.super Ljava/lang/Object;
.source "ReaderDataModelUtils.java"


# direct methods
.method public static getBestEffortPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;
    .locals 2
    .param p0, "model"    # Lcom/google/android/apps/books/render/ReaderDataModel;
    .param p1, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 35
    const/4 v1, 0x1

    invoke-interface {p0, p1, v1}, Lcom/google/android/apps/books/render/ReaderDataModel;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 36
    .local v0, "result":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v0, :cond_0

    .line 37
    const/4 v1, 0x0

    .line 39
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v1

    goto :goto_0
.end method

.method public static getBookPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;
    .locals 2
    .param p0, "model"    # Lcom/google/android/apps/books/render/ReaderDataModel;
    .param p1, "page"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 10
    const/4 v1, 0x0

    invoke-interface {p0, p1, v1}, Lcom/google/android/apps/books/render/ReaderDataModel;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 11
    .local v0, "normalized":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v1

    if-nez v1, :cond_0

    .line 12
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v1

    .line 14
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
