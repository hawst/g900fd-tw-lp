.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onPassageMoListReady(IILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$moElementsJson:Ljava/lang/String;

.field final synthetic val$passageIndex:I

.field final synthetic val$requestId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->val$requestId:I

    iput p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->val$passageIndex:I

    iput-object p4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->val$moElementsJson:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->val$requestId:I

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->val$passageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iget-object v4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;->val$moElementsJson:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->parseMoElements(Ljava/lang/String;)Ljava/util/Map;
    invoke-static {v3, v4}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$100(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/render/ReaderListener;->onPassageMoListReady(IILjava/util/Map;)V

    .line 278
    return-void
.end method
