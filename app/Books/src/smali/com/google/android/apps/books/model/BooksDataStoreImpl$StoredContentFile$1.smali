.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

.field final synthetic val$fileSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;)V
    .locals 0

    .prologue
    .line 978
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;->this$1:Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;->val$fileSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .locals 2
    .param p1, "content"    # Lcom/google/android/apps/books/model/EncryptedContent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 981
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;->this$1:Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    iget-object v1, v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    # getter for: Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;
    invoke-static {v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->access$000(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Lcom/google/android/apps/books/common/FileStorageManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/common/FileStorageManager;->getLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    .line 982
    .local v0, "lock":Ljava/util/concurrent/locks/Lock;
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 984
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;->val$fileSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 986
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method
