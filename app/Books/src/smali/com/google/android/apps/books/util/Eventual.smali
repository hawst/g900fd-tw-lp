.class public Lcom/google/android/apps/books/util/Eventual;
.super Ljava/lang/Object;
.source "Eventual.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mConsumers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private mLoadingConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v1, p0, Lcom/google/android/apps/books/util/Eventual;->mValue:Ljava/lang/Object;

    .line 20
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mConsumers:Ljava/util/List;

    .line 83
    iput-object v1, p0, Lcom/google/android/apps/books/util/Eventual;->mLoadingConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 40
    return-void
.end method

.method public static create()Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/apps/books/util/Eventual",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/books/util/Eventual;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/Eventual;-><init>()V

    return-object v0
.end method

.method public static create(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)",
            "Lcom/google/android/apps/books/util/Eventual",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    new-instance v0, Lcom/google/android/apps/books/util/Eventual;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/Eventual;-><init>()V

    .line 34
    .local v0, "e":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 35
    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mValue:Ljava/lang/Object;

    return-object v0
.end method

.method public hasValue()Z
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mValue:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadingConsumer()Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mLoadingConsumer:Lcom/google/android/ublib/utils/Consumer;

    if-nez v0, :cond_0

    .line 93
    new-instance v0, Lcom/google/android/apps/books/util/Eventual$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/Eventual$1;-><init>(Lcom/google/android/apps/books/util/Eventual;)V

    iput-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mLoadingConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mLoadingConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v0}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    return-object v0
.end method

.method public onLoad(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    iput-object p1, p0, Lcom/google/android/apps/books/util/Eventual;->mValue:Ljava/lang/Object;

    .line 53
    iget-object v3, p0, Lcom/google/android/apps/books/util/Eventual;->mConsumers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 54
    iget-object v1, p0, Lcom/google/android/apps/books/util/Eventual;->mConsumers:Ljava/util/List;

    .line 55
    .local v1, "consumers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/utils/Consumer<TT;>;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/util/Eventual;->mConsumers:Ljava/util/List;

    .line 56
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/utils/Consumer;

    .line 57
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    .end local v0    # "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    .end local v1    # "consumers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/utils/Consumer<TT;>;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method protected unload()V
    .locals 1

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mValue:Ljava/lang/Object;

    .line 108
    return-void
.end method

.method public whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Eventual;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mValue:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/Eventual;->mConsumers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
