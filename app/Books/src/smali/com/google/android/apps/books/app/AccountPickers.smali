.class public Lcom/google/android/apps/books/app/AccountPickers;
.super Ljava/lang/Object;
.source "AccountPickers.java"


# direct methods
.method public static getPicker()Lcom/google/android/apps/books/app/AccountPicker;
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnIcsOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/books/app/IcsAccountPicker;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/IcsAccountPicker;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/app/PreIcsAccountPicker;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/PreIcsAccountPicker;-><init>()V

    goto :goto_0
.end method
