.class final Lcom/google/android/apps/books/app/RecommendationsUtil$1;
.super Ljava/lang/Object;
.source "RecommendationsUtil.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/RecommendationsUtil;->loadShouldShowRecommendations(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isSchoolOwned"    # Ljava/lang/Boolean;

    .prologue
    .line 94
    # getter for: Lcom/google/android/apps/books/app/RecommendationsUtil;->sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {}, Lcom/google/android/apps/books/app/RecommendationsUtil;->access$200()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 95
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 91
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/RecommendationsUtil$1;->take(Ljava/lang/Boolean;)V

    return-void
.end method
