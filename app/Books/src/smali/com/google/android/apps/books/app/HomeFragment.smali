.class public Lcom/google/android/apps/books/app/HomeFragment;
.super Lcom/google/android/apps/books/app/BooksFragment;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/BooksHomeController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;,
        Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;,
        Lcom/google/android/apps/books/app/HomeFragment$DeleteFromLibraryDialog;,
        Lcom/google/android/apps/books/app/HomeFragment$OfferOverrideOpenDialog;,
        Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;,
        Lcom/google/android/apps/books/app/HomeFragment$UnPinDialog;,
        Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;,
        Lcom/google/android/apps/books/app/HomeFragment$OfferOverridePinDialog;,
        Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;,
        Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;,
        Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final LOGD:Z

.field private static final MY_LIBRARY_INTENT_PATHS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static sClosedBookInSecondHalf:Z

.field private static sShowUploads:Z

.field private static sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

.field private static sUploadFileName:Ljava/lang/String;


# instance fields
.field private final mBackgroundDataController:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ">;"
        }
    .end annotation
.end field

.field private final mBackgroundDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private mBooksApplication:Lcom/google/android/apps/books/app/BooksApplication;

.field private mCardsData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckedForAutoStartOnboarding:Z

.field private final mConnReceiver:Landroid/content/BroadcastReceiver;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mCreatingShortcut:Z

.field private mCurrentViewMode:Ljava/lang/String;

.field private final mDataController:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ">;"
        }
    .end annotation
.end field

.field private final mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private mDeviceConnected:Z

.field private mDismissedSurvey:Z

.field private mEbooksFetchTimeBeforeFragmentCreation:J

.field private mFragmentCreateTime:J

.field private mGoToReadNowOnBackPressed:Z

.field private mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

.field private mHomeDisplayTimeLogged:Z

.field private mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

.field private mIsNewUserForOnboarding:Z

.field private mIsSchoolOwnedConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mLastAlreadyFetchedMyEbooks:Z

.field private mLoadVolumesScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

.field private mLoadZwiebackCookieResult:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadedDismissedRecs:Z

.field final mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

.field private mMenu:Lcom/google/android/apps/books/app/HomeMenu;

.field private final mMenuCallbacks:Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;

.field private final mMyEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;"
        }
    .end annotation
.end field

.field private mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

.field private mMyLibraryFilterSource:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

.field private mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

.field private final mOffers:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mOffersProxy:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOverrideUploadsComparator:Lcom/google/android/apps/books/app/LibraryComparator;

.field private mPagerAdapter:Landroid/support/v4/view/PagerAdapter;

.field private mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

.field private mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final mRecentSeeAllClickHandler:Landroid/view/View$OnClickListener;

.field private mRecommendationsEnabledConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRentalBadgesHandler:Landroid/os/Handler;

.field private mResolver:Landroid/content/ContentResolver;

.field private mSelectedLibraryFilter:I

.field private mShouldOfferHatsSurvey:Ljava/lang/Boolean;

.field private mShowingHatsSurvey:Z

.field private mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

.field private final mSideDrawerCallbacks:Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;

.field private mStartedLoadingZwiebackCookie:Z

.field private mStartedOffersLoad:Z

.field private mStartedRecommendedBooksLoad:Z

.field private mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

.field private final mUnpinCountdownAnimations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/animation/ObjectAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

.field private mView:Landroid/view/ViewGroup;

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private mVolumesData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 180
    const-string v0, "HomeFragment"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/books/app/HomeFragment;->LOGD:Z

    .line 199
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/books"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/books/"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/HomeFragment;->MY_LIBRARY_INTENT_PATHS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 537
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksFragment;-><init>()V

    .line 212
    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilterSource:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    .line 214
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedRecommendedBooksLoad:Z

    .line 215
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    .line 217
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedOffersLoad:Z

    .line 218
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    invoke-static {v0}, Lcom/google/android/apps/books/util/UiThreadConsumer;->proxyTo(Lcom/google/android/apps/books/util/Eventual;)Lcom/google/android/apps/books/util/UiThreadConsumer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOffersProxy:Lcom/google/android/ublib/utils/Consumer;

    .line 233
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 234
    iput v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSelectedLibraryFilter:I

    .line 245
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mEbooksFetchTimeBeforeFragmentCreation:J

    .line 353
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$1;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecentSeeAllClickHandler:Landroid/view/View$OnClickListener;

    .line 377
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawerCallbacks:Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;

    .line 380
    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    .line 381
    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    .line 382
    new-instance v0, Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-direct {v0}, Lcom/google/android/apps/books/upload/Upload$Uploads;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

    .line 393
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z

    .line 399
    new-instance v0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    .line 449
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenuCallbacks:Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;

    .line 521
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    .line 525
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataController:Lcom/google/android/apps/books/util/Eventual;

    .line 531
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;

    .line 663
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$9;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$9;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mConnReceiver:Landroid/content/BroadcastReceiver;

    .line 1443
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$18;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$18;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 1888
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$23;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$23;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 1969
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$24;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$24;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/HomeFragment$4;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/HomeFragment$5;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 551
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->updateActionBarTitle()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/app/HomeFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->moveHomeViewToTop()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/model/BooksDataListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->onUserSelectedViewMode(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOffersProxy:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/app/HomeFragment;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->updateDeviceConnected(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->maybeSetupRecommendationFooter(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/support/v4/view/PagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/google/android/apps/books/app/HomeFragment;Landroid/support/v4/view/PagerAdapter;)Landroid/support/v4/view/PagerAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mPagerAdapter:Landroid/support/v4/view/PagerAdapter;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadVolumesScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->setVolumes(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/books/app/HomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mIsNewUserForOnboarding:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->togglePinned(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/HomeFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->showCurrentViewMode(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/util/Map;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendLocalVolumeDataToViews(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/util/Map;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendDownloadFractionsToViews(Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$3200(Lcom/google/android/apps/books/app/HomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadedDismissedRecs:Z

    return v0
.end method

.method static synthetic access$3202(Lcom/google/android/apps/books/app/HomeFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadedDismissedRecs:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->setUploadsDataInCardsData()V

    return-void
.end method

.method static synthetic access$3500(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V

    return-void
.end method

.method static synthetic access$3600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$3900(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateVolumeDownloadProgress()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/HomeFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getStartingViewMode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/HomeFragment;->updatePinInDataController(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$4100(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->onUnpinAnimationEnd(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/google/android/apps/books/app/HomeFragment;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isExpiredNonSampleRental(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4400(Lcom/google/android/apps/books/app/HomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    return v0
.end method

.method static synthetic access$4500(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->hasOfflineLicense(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4600(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "x3"    # Landroid/view/View;

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/HomeFragment;->openBook(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$4700(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeQueueRentalBadgesUpdate()V

    return-void
.end method

.method static synthetic access$4902(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/util/ExceptionOr;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadZwiebackCookieResult:Lcom/google/android/apps/books/util/ExceptionOr;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5000(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeAdvanceHats()V

    return-void
.end method

.method static synthetic access$5100(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeShowHatsPrompt()V

    return-void
.end method

.method static synthetic access$5200(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->dismissHatsSurvey()V

    return-void
.end method

.method static synthetic access$5300(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->showHatsSurvey()V

    return-void
.end method

.method static synthetic access$5400(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->persistHatsDismissal()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/BooksHomeView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/app/LibraryFilter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/LibraryFilter;)Lcom/google/android/apps/books/app/LibraryFilter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/LibraryFilter;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/books/app/HomeFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSelectedLibraryFilter:I

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->setOverrideUploadsComparator(Lcom/google/android/apps/books/app/LibraryComparator;)V

    return-void
.end method

.method private applyStylesToHeader(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)V
    .locals 4
    .param p1, "header"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p2, "titleResId"    # I

    .prologue
    .line 927
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 928
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b00d0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 929
    .local v0, "floatingColor":I
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->findToolbar(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v7/widget/Toolbar;

    move-result-object v2

    .line 930
    .local v2, "toolbar":Landroid/support/v7/widget/Toolbar;
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 931
    const v3, 0x7f0b00d1

    invoke-virtual {p1, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBackgroundResource(I)V

    .line 932
    invoke-virtual {v2, p2}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 936
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/HomeSideDrawer;->syncActionBarIconState()V

    .line 937
    return-void
.end method

.method private clearHatsState()V
    .locals 1

    .prologue
    .line 2910
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    .line 2911
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mShowingHatsSurvey:Z

    .line 2912
    return-void
.end method

.method private dismissHatsSurvey()V
    .locals 1

    .prologue
    .line 2858
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    if-eqz v0, :cond_0

    .line 2859
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-virtual {v0}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->dismiss()V

    .line 2861
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V

    .line 2862
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetActivityOrientation()V

    .line 2863
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->persistHatsDismissal()V

    .line 2864
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDismissedSurvey:Z

    .line 2865
    return-void
.end method

.method private enableHeaderListLayout(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZI)V
    .locals 2
    .param p1, "headerView"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p2, "enabled"    # Z
    .param p3, "titleResId"    # I

    .prologue
    .line 1191
    if-eqz p2, :cond_1

    .line 1192
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setVisibility(I)V

    .line 1193
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->findToolbar(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    .line 1194
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    if-lez p3, :cond_0

    .line 1195
    invoke-virtual {v0, p3}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 1197
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 1201
    .end local v0    # "toolbar":Landroid/support/v7/widget/Toolbar;
    :goto_0
    return-void

    .line 1199
    :cond_1
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private filterRecommendations(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "recommendations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    const/4 v8, 0x3

    .line 794
    const-string v5, "HomeFragment"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 795
    const-string v5, "HomeFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateRecommendationsList has "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " recommendations"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    if-eqz v5, :cond_3

    .line 799
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/common/collect/Sets;->newHashSetWithExpectedSize(I)Ljava/util/HashSet;

    move-result-object v4

    .line 801
    .local v4, "volumeIdsInLibrary":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/VolumeData;

    .line 802
    .local v3, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 805
    .end local v3    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_1
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 806
    .local v2, "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    .line 812
    .local v1, "recommendedBook":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    iget-object v5, v1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v5, v5, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    iget-object v6, v1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 814
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 818
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "recommendedBook":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    .end local v2    # "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    .end local v4    # "volumeIdsInLibrary":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_3
    move-object v2, p1

    .line 821
    .restart local v2    # "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    :cond_4
    const-string v5, "HomeFragment"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 822
    const-string v5, "HomeFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateRecommendationsList has "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " recommendations after filtering"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    :cond_5
    return-object v2
.end method

.method private findToolbar(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v7/widget/Toolbar;
    .locals 1
    .param p1, "headerView"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 1204
    const v0, 0x7f0e0176

    invoke-virtual {p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method private getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBooksApplication:Lcom/google/android/apps/books/app/BooksApplication;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 658
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBooksApplication:Lcom/google/android/apps/books/app/BooksApplication;

    .line 660
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBooksApplication:Lcom/google/android/apps/books/app/BooksApplication;

    return-object v0
.end method

.method private getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
    .locals 1

    .prologue
    .line 2592
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private getContentContainerView()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 2916
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 2917
    const/4 v0, 0x0

    .line 2919
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    const v1, 0x7f0e0105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method private getStartingViewMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1317
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    .line 1325
    :goto_0
    return-object v0

    .line 1320
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->shouldStartInMyLibrary()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1321
    const-string v0, "mylibrary"

    goto :goto_0

    .line 1325
    :cond_1
    const-string v0, "readnow"

    goto :goto_0
.end method

.method private hasOfflineLicense(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1839
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->hasOfflineLicense(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isExpiredNonSampleRental(Ljava/lang/String;)Z
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1772
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    .line 1773
    .local v0, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    if-nez v0, :cond_1

    .line 1774
    const-string v1, "HomeFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1775
    const-string v1, "HomeFragment"

    const-string v2, "missing metadata for volume"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    :cond_0
    const/4 v1, 0x1

    .line 1779
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/books/util/RentalUtils;->isExpiredNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v1

    goto :goto_0
.end method

.method private isReadableOffline(Ljava/lang/String;)Z
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1843
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    .line 1844
    .local v1, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    .line 1847
    .local v0, "usesEolm":Z
    :goto_0
    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->hasOfflineLicense(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    return v2

    .end local v0    # "usesEolm":Z
    :cond_1
    move v0, v3

    .line 1844
    goto :goto_0

    .restart local v0    # "usesEolm":Z
    :cond_2
    move v2, v3

    .line 1847
    goto :goto_1
.end method

.method private lockOrientation(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 1293
    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1294
    return-void
.end method

.method private static makeRentalBadgesHandler(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/os/Handler;
    .locals 2
    .param p0, "fragment"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 2736
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$30;

    invoke-direct {v1, p0, p0}, Lcom/google/android/apps/books/app/HomeFragment$30;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    return-object v0
.end method

.method private maybeAdvanceHats()V
    .locals 8

    .prologue
    .line 2792
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2793
    .local v0, "activity":Landroid/app/Activity;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->shouldWorkOnHats()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->shouldOfferHatsSurvey(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2812
    :cond_0
    :goto_0
    return-void

    .line 2796
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadZwiebackCookieResult:Lcom/google/android/apps/books/util/ExceptionOr;

    if-nez v4, :cond_2

    .line 2797
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedLoadingZwiebackCookie:Z

    if-nez v4, :cond_0

    .line 2798
    new-instance v4, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/books/app/HomeFragment$LoadZwiebackTask;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/content/Context;)V

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/apps/books/util/Nothing;

    invoke-static {v4, v5}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 2799
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedLoadingZwiebackCookie:Z

    goto :goto_0

    .line 2801
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    if-nez v4, :cond_0

    .line 2802
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadZwiebackCookieResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2803
    .local v3, "zwiebackCookie":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 2806
    const-string v2, "jzoqldt6hy2ts3ibzxezjtucc4"

    .line 2807
    .local v2, "siteId":Ljava/lang/String;
    new-instance v1, Lcom/google/android/libraries/happiness/HatsSurveyParams;

    const-string v4, "jzoqldt6hy2ts3ibzxezjtucc4"

    invoke-direct {v1, v3, v4}, Lcom/google/android/libraries/happiness/HatsSurveyParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2808
    .local v1, "params":Lcom/google/android/libraries/happiness/HatsSurveyParams;
    new-instance v4, Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/google/android/apps/books/app/HomeFragment$HatsCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment$1;)V

    invoke-direct {v4, v5, v6, v1}, Lcom/google/android/libraries/happiness/HatsSurveyManager;-><init>(Landroid/content/Context;Lcom/google/android/libraries/happiness/HatsSurveyClient;Lcom/google/android/libraries/happiness/HatsSurveyParams;)V

    iput-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    .line 2810
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-virtual {v4}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->requestSurvey()V

    goto :goto_0
.end method

.method private maybeAutoStartOnboarding()V
    .locals 1

    .prologue
    .line 1554
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1573
    :cond_0
    :goto_0
    return-void

    .line 1559
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCheckedForAutoStartOnboarding:Z

    if-nez v0, :cond_0

    .line 1563
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCheckedForAutoStartOnboarding:Z

    .line 1565
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$20;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$20;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->withShouldAutoStartOnboarding(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private maybeGetDataController(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 646
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 647
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 648
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v1

    .line 649
    .local v1, "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    if-eqz v1, :cond_0

    .line 650
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 651
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataController:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 654
    .end local v1    # "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    :cond_0
    return-void
.end method

.method private maybeLogHomeDisplayTime()V
    .locals 6

    .prologue
    .line 1464
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeDisplayTimeLogged:Z

    if-nez v2, :cond_0

    .line 1465
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mFragmentCreateTime:J

    sub-long v0, v2, v4

    .line 1466
    .local v0, "loadTime":J
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->myEbooksNeverFetched()Z

    move-result v3

    invoke-static {v2, v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->maybeLogHomeDisplayTime(Landroid/content/Context;JZ)V

    .line 1468
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeDisplayTimeLogged:Z

    .line 1470
    .end local v0    # "loadTime":J
    :cond_0
    return-void
.end method

.method private maybeQueueRentalBadgesUpdate()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 2713
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->isDestroyed()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2733
    :cond_0
    :goto_0
    return-void

    .line 2717
    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    .line 2718
    .local v2, "earliestRentalBadgeUpdateTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 2719
    .local v8, "now":J
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/model/VolumeData;

    .line 2720
    .local v5, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-static {v5}, Lcom/google/android/apps/books/util/RentalUtils;->isRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2721
    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lcom/google/android/apps/books/util/RentalUtils;->getNextTimeLeftBadgeTransition(JJ)J

    move-result-wide v6

    .line 2723
    .local v6, "nextTransition":J
    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    goto :goto_1

    .line 2727
    .end local v5    # "volume":Lcom/google/android/apps/books/model/VolumeData;
    .end local v6    # "nextTransition":J
    :cond_3
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRentalBadgesHandler:Landroid/os/Handler;

    invoke-virtual {v10, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 2729
    const-wide v10, 0x7fffffffffffffffL

    cmp-long v10, v2, v10

    if-eqz v10, :cond_0

    .line 2730
    const-wide/16 v10, 0x64

    add-long/2addr v10, v2

    sub-long v0, v10, v8

    .line 2731
    .local v0, "delay":J
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRentalBadgesHandler:Landroid/os/Handler;

    invoke-virtual {v10, v12, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private maybeResetCards()V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 741
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V

    .line 743
    :cond_0
    return-void
.end method

.method private maybeSetupRecommendationFooter(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 830
    .local p1, "recommendations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadedDismissedRecs:Z

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->filterRecommendations(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setRecommendations(Ljava/util/List;)V

    .line 833
    :cond_0
    return-void
.end method

.method private maybeShowHatsPrompt()V
    .locals 8

    .prologue
    .line 2958
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->shouldWorkOnHats()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    if-nez v4, :cond_1

    .line 2993
    :cond_0
    :goto_0
    return-void

    .line 2962
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContentContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 2963
    .local v1, "contentContainer":Landroid/view/ViewGroup;
    if-eqz v1, :cond_0

    .line 2967
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2968
    .local v2, "context":Landroid/content/Context;
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0400c8

    invoke-virtual {v4, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2970
    const v4, 0x7f0e01de

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2971
    .local v3, "promptView":Landroid/view/View;
    const v4, 0x7f0e01df

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/app/HomeFragment$33;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/app/HomeFragment$33;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2977
    const v4, 0x7f0e01e0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/app/HomeFragment$34;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/app/HomeFragment$34;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2986
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    const/high16 v6, -0x80000000

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 2990
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationY(F)V

    .line 2991
    const-string v4, "translationY"

    const/4 v5, 0x1

    new-array v5, v5, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2992
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0
.end method

.method private maybeStartOffersLoad(Landroid/app/Activity;Z)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "localLibraryAvailable"    # Z

    .prologue
    .line 597
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedOffersLoad:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$8;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/books/app/HomeFragment$8;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 607
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedOffersLoad:Z

    .line 609
    :cond_0
    return-void
.end method

.method private maybeStartRecommendationsLoad(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 562
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedRecommendedBooksLoad:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 565
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v2, Lcom/google/android/apps/books/app/HomeFragment$6;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/HomeFragment$6;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 571
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 572
    .local v0, "app":Lcom/google/android/apps/books/app/BooksApplication;
    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$7;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/books/app/HomeFragment$7;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/BooksApplication;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendationsEnabledConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 585
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendationsEnabledConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v1}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/app/RecommendationsUtil;->loadShouldShowRecommendations(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    .line 587
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mStartedRecommendedBooksLoad:Z

    .line 589
    .end local v0    # "app":Lcom/google/android/apps/books/app/BooksApplication;
    :cond_0
    return-void
.end method

.method private maybeUpdateVolumeDownloadProgress()V
    .locals 2

    .prologue
    .line 2244
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    if-eqz v0, :cond_0

    .line 2245
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeView;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v1, v1, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeDownloadProgress(Ljava/util/Map;)V

    .line 2248
    :cond_0
    return-void
.end method

.method private moveHomeViewToTop()V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeView;->moveToHome()V

    .line 749
    :cond_0
    return-void
.end method

.method private myEbooksNeverFetched()Z
    .locals 4

    .prologue
    .line 1473
    iget-wide v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mEbooksFetchTimeBeforeFragmentCreation:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onUnpinAnimationEnd(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2332
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncUi()Lcom/google/android/apps/books/service/SyncService$SyncUi;

    move-result-object v0

    .line 2333
    .local v0, "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    invoke-interface {v0, p1}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->cancelDownloadNotification(Ljava/lang/String;)V

    .line 2335
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_ANIMATION_COMPLETED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2336
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v2, Lcom/google/android/apps/books/app/HomeFragment$26;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$26;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 2342
    return-void
.end method

.method private onUserSelectedViewMode(Ljava/lang/String;)V
    .locals 2
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 688
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/app/HomeFragment;->setViewMode(Ljava/lang/String;Z)V

    .line 689
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 690
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setViewMode(Ljava/lang/String;)V

    .line 692
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V

    .line 693
    return-void
.end method

.method public static openBook(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p4, "coverView"    # Landroid/view/View;

    .prologue
    .line 2506
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    .line 2507
    return-void
.end method

.method private openBook(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "coverView"    # Landroid/view/View;

    .prologue
    .line 2609
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/apps/books/app/HomeFragment;->openBook(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    .line 2610
    return-void
.end method

.method private persistHatsDismissal()V
    .locals 1

    .prologue
    .line 2999
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 3000
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 3001
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksUserSurveys;->setDismissedSurvey(Landroid/content/Context;)V

    .line 3003
    :cond_0
    return-void
.end method

.method private removeHatsPromptView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2872
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->clearHatsState()V

    .line 2874
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContentContainerView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 2875
    .local v1, "container":Landroid/view/ViewGroup;
    if-nez v1, :cond_1

    .line 2904
    :cond_0
    :goto_0
    return-void

    .line 2878
    :cond_1
    const v3, 0x7f0e01de

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2879
    .local v2, "surveyPrompt":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 2882
    const-string v3, "translationY"

    new-array v4, v7, [F

    const/4 v5, 0x0

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    aput v6, v4, v5

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2884
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$32;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/apps/books/app/HomeFragment$32;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2900
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2903
    iput-boolean v7, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDismissedSurvey:Z

    goto :goto_0
.end method

.method private resetActivityOrientation()V
    .locals 2

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1280
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 1289
    :goto_0
    return-void

    .line 1283
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mShowingHatsSurvey:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanMR2OrLater()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1285
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->lockOrientation(Landroid/app/Activity;)V

    goto :goto_0

    .line 1287
    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method private resetHomeViewCards()V
    .locals 2

    .prologue
    .line 1663
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getMyLibraryFilterSource()Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->onCardDataLoaded(Ljava/util/List;)V

    .line 1665
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    if-eqz v0, :cond_0

    .line 1666
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BooksHomeView;->setCardsData(Ljava/util/List;)V

    .line 1668
    :cond_0
    return-void
.end method

.method private sendAccount(Lcom/google/android/apps/books/widget/BaseBooksHomeView;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "view"    # Lcom/google/android/apps/books/widget/BaseBooksHomeView;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 640
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 641
    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/widget/BaseBooksHomeView;->setAccount(Landroid/accounts/Account;)V

    .line 643
    :cond_0
    return-void
.end method

.method private sendAccountToViews(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendAccount(Lcom/google/android/apps/books/widget/BaseBooksHomeView;Landroid/accounts/Account;)V

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendAccount(Lcom/google/android/apps/books/widget/BaseBooksHomeView;Landroid/accounts/Account;)V

    .line 637
    return-void
.end method

.method private sendDownloadFractionsChanged(Lcom/google/android/apps/books/widget/BooksHomeView;Ljava/util/Map;)V
    .locals 1
    .param p1, "homeView"    # Lcom/google/android/apps/books/widget/BooksHomeView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/BooksHomeView;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2010
    .local p2, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    if-eqz p1, :cond_0

    .line 2011
    invoke-interface {p1}, Lcom/google/android/apps/books/widget/BooksHomeView;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeDownloadProgress(Ljava/util/Map;)V

    .line 2013
    :cond_0
    return-void
.end method

.method private sendDownloadFractionsToViews(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1879
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendDownloadFractionsChanged(Lcom/google/android/apps/books/widget/BooksHomeView;Ljava/util/Map;)V

    .line 1880
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendDownloadFractionsChanged(Lcom/google/android/apps/books/widget/BooksHomeView;Ljava/util/Map;)V

    .line 1881
    return-void
.end method

.method private sendLocalVolumeDataChanged(Lcom/google/android/apps/books/widget/BooksHomeView;Ljava/util/Map;)V
    .locals 1
    .param p1, "homeView"    # Lcom/google/android/apps/books/widget/BooksHomeView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/BooksHomeView;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2017
    .local p2, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    if-eqz p1, :cond_0

    .line 2018
    invoke-interface {p1}, Lcom/google/android/apps/books/widget/BooksHomeView;->getDataListener()Lcom/google/android/apps/books/model/BooksDataListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/model/BooksDataListener;->onLocalVolumeData(Ljava/util/Map;)V

    .line 2020
    :cond_0
    return-void
.end method

.method private sendLocalVolumeDataToViews(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1884
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendLocalVolumeDataChanged(Lcom/google/android/apps/books/widget/BooksHomeView;Ljava/util/Map;)V

    .line 1885
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->sendLocalVolumeDataChanged(Lcom/google/android/apps/books/widget/BooksHomeView;Ljava/util/Map;)V

    .line 1886
    return-void
.end method

.method private setOverrideUploadsComparator(Lcom/google/android/apps/books/app/LibraryComparator;)V
    .locals 0
    .param p1, "override"    # Lcom/google/android/apps/books/app/LibraryComparator;

    .prologue
    .line 763
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOverrideUploadsComparator:Lcom/google/android/apps/books/app/LibraryComparator;

    .line 764
    return-void
.end method

.method static setShowUploads(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)V
    .locals 1
    .param p0, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1335
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/books/app/HomeFragment;->sShowUploads:Z

    .line 1336
    sput-object p0, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    .line 1337
    sput-object p1, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileName:Ljava/lang/String;

    .line 1338
    return-void
.end method

.method private setUploadsDataInCardsData()V
    .locals 6

    .prologue
    .line 1644
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    .line 1645
    .local v2, "oldCardsData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    .line 1646
    if-eqz v2, :cond_1

    .line 1647
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/CardData;

    .line 1648
    .local v0, "data":Lcom/google/android/apps/books/widget/CardData;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->hasUploadData()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1649
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1654
    .end local v0    # "data":Lcom/google/android/apps/books/widget/CardData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-virtual {v4}, Lcom/google/android/apps/books/upload/Upload$Uploads;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/upload/Upload;

    .line 1655
    .local v3, "upload":Lcom/google/android/apps/books/upload/Upload;
    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/books/widget/CardData;

    invoke-direct {v5, v3}, Lcom/google/android/apps/books/widget/CardData;-><init>(Lcom/google/android/apps/books/upload/Upload;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1657
    .end local v3    # "upload":Lcom/google/android/apps/books/upload/Upload;
    :cond_2
    return-void
.end method

.method private setViewMode(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "mode"    # Ljava/lang/String;
    .param p2, "animateTransition"    # Z

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    :goto_0
    return-void

    .line 700
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    .line 702
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateMenu()V

    .line 703
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateDrawerContent()V

    .line 705
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/app/HomeFragment;->showCurrentViewMode(Z)V

    goto :goto_0
.end method

.method private setVolumes(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 14
    .param p1, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    .prologue
    const/4 v13, 0x3

    .line 1477
    iget-object v7, p1, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    .line 1478
    .local v7, "myEbooksVolumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeLogHomeDisplayTime()V

    .line 1480
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v11, Lcom/google/android/apps/books/app/HomeFragment$19;

    invoke-direct {v11, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$19;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 1491
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    .line 1493
    .local v2, "count":I
    const-string v10, "HomeFragment"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1494
    const-string v10, "HomeFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onLoadFinished() with "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " volumes"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->alreadyFetchedMyEbooks()Z

    move-result v0

    .line 1499
    .local v0, "alreadyFetchedMyEbooks":Z
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-static {v7, v10}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-boolean v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLastAlreadyFetchedMyEbooks:Z

    if-ne v10, v0, :cond_2

    .line 1501
    const-string v10, "HomeFragment"

    invoke-static {v10, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1502
    const-string v10, "HomeFragment"

    const-string v11, "Ignoring unchanged volumes list and library fetched state"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1544
    :cond_1
    :goto_0
    return-void

    .line 1506
    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLastAlreadyFetchedMyEbooks:Z

    .line 1508
    iput-object v7, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    .line 1509
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->setVolumesDataInCardData()V

    .line 1511
    invoke-static {}, Lcom/google/android/apps/books/app/LibraryFilter;->values()[Lcom/google/android/apps/books/app/LibraryFilter;

    move-result-object v1

    .local v1, "arr$":[Lcom/google/android/apps/books/app/LibraryFilter;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v3, v1, v4

    .line 1512
    .local v3, "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    invoke-virtual {v3, p0}, Lcom/google/android/apps/books/app/LibraryFilter;->setBooksHomeController(Lcom/google/android/apps/books/widget/BooksHomeController;)V

    .line 1511
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1515
    .end local v3    # "filter":Lcom/google/android/apps/books/app/LibraryFilter;
    :cond_3
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v10}, Lcom/google/android/apps/books/util/Eventual;->hasValue()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1516
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v10}, Lcom/google/android/apps/books/util/Eventual;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    invoke-direct {p0, v10}, Lcom/google/android/apps/books/app/HomeFragment;->maybeSetupRecommendationFooter(Ljava/util/List;)V

    .line 1519
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V

    .line 1520
    iget-boolean v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLastAlreadyFetchedMyEbooks:Z

    if-eqz v10, :cond_5

    .line 1521
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/apps/books/app/HomeFragment;->maybeStartRecommendationsLoad(Landroid/app/Activity;)V

    .line 1524
    :cond_5
    const/4 v5, 0x1

    .line 1525
    .local v5, "isNewUser":Z
    const/4 v8, 0x0

    .line 1526
    .local v8, "numberOfPublicDomainBooks":I
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/model/VolumeData;

    .line 1527
    .local v9, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v10

    if-eqz v10, :cond_7

    add-int/lit8 v8, v8, 0x1

    if-le v8, v13, :cond_6

    .line 1528
    :cond_7
    const/4 v5, 0x0

    .line 1534
    .end local v9    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    :cond_8
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mIsNewUserForOnboarding:Z

    .line 1535
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    if-eqz v10, :cond_9

    .line 1536
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iget-boolean v11, p0, Lcom/google/android/apps/books/app/HomeFragment;->mIsNewUserForOnboarding:Z

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setIsNewUser(Z)V

    .line 1539
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeQueueRentalBadgesUpdate()V

    .line 1541
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeAutoStartOnboarding()V

    .line 1543
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeAdvanceHats()V

    goto :goto_0
.end method

.method private setVolumesDataInCardData()V
    .locals 7

    .prologue
    .line 1619
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    .line 1620
    .local v3, "oldCardsData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    .line 1621
    if-eqz v3, :cond_2

    .line 1622
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/CardData;

    .line 1623
    .local v0, "data":Lcom/google/android/apps/books/widget/CardData;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1625
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->getUploadData()Lcom/google/android/apps/books/upload/Upload;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/upload/Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v5

    if-nez v5, :cond_1

    .line 1626
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1628
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUploads:Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/CardData;->getUploadData()Lcom/google/android/apps/books/upload/Upload;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/upload/Upload$Uploads;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1635
    .end local v0    # "data":Lcom/google/android/apps/books/widget/CardData;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/VolumeData;

    .line 1636
    .local v4, "volume":Lcom/google/android/apps/books/model/VolumeData;
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v5, v5, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 1638
    .local v2, "localData":Lcom/google/android/apps/books/model/LocalVolumeData;
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    new-instance v6, Lcom/google/android/apps/books/widget/CardData;

    invoke-direct {v6, v4, v2}, Lcom/google/android/apps/books/widget/CardData;-><init>(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1640
    .end local v2    # "localData":Lcom/google/android/apps/books/model/LocalVolumeData;
    .end local v4    # "volume":Lcom/google/android/apps/books/model/VolumeData;
    :cond_3
    return-void
.end method

.method private setupMyLibraryHeader()V
    .locals 4

    .prologue
    .line 1002
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040046

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 1005
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$14;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/app/HomeFragment$14;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 1163
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$15;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/HomeFragment$15;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 1183
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0f019e

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->applyStylesToHeader(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)V

    .line 1184
    return-void
.end method

.method private setupReadNowHeader()V
    .locals 2

    .prologue
    .line 940
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$13;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/HomeFragment$13;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0f019f

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->applyStylesToHeader(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)V

    .line 999
    return-void
.end method

.method private shouldOfferHatsSurvey(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2781
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mShouldOfferHatsSurvey:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 2782
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksUserSurveys;->shouldOfferHatsSurvey(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mShouldOfferHatsSurvey:Ljava/lang/Boolean;

    .line 2784
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mShouldOfferHatsSurvey:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private shouldStartInMyLibrary()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1297
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1298
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 1310
    :cond_0
    :goto_0
    return v3

    .line 1302
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1303
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 1306
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1307
    .local v1, "data":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 1310
    sget-object v4, Lcom/google/android/apps/books/app/HomeFragment;->MY_LIBRARY_INTENT_PATHS:Ljava/util/Set;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "ReadNow"

    invoke-virtual {v1}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method

.method private shouldWorkOnHats()Z
    .locals 1

    .prologue
    .line 2773
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/google/android/apps/books/app/HomeFragment;->sClosedBookInSecondHalf:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDismissedSurvey:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showCurrentViewMode(Z)V
    .locals 4
    .param p1, "animateTransition"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    const-string v1, "readnow"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->showIncomingView(Lcom/google/android/apps/books/widget/BooksHomeView;Z)V

    .line 711
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0f019f

    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/apps/books/app/HomeFragment;->enableHeaderListLayout(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZI)V

    .line 712
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/apps/books/app/HomeFragment;->enableHeaderListLayout(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZI)V

    .line 720
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->syncActionBarIconState()V

    .line 722
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeResetCards()V

    .line 723
    return-void

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    const-string v1, "mylibrary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->showIncomingView(Lcom/google/android/apps/books/widget/BooksHomeView;Z)V

    .line 715
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const v1, 0x7f0f019e

    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/apps/books/app/HomeFragment;->enableHeaderListLayout(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZI)V

    .line 716
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/apps/books/app/HomeFragment;->enableHeaderListLayout(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZI)V

    goto :goto_0
.end method

.method private showHatsSurvey()V
    .locals 3

    .prologue
    .line 2846
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2847
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 2855
    :goto_0
    return-void

    .line 2851
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHatsSurveyController:Lcom/google/android/libraries/happiness/HatsSurveyManager;

    invoke-virtual {v2}, Lcom/google/android/libraries/happiness/HatsSurveyManager;->getSurveyDialog()Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Landroid/support/v4/app/Fragment;)V

    .line 2853
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mShowingHatsSurvey:Z

    .line 2854
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetActivityOrientation()V

    goto :goto_0
.end method

.method private showIncomingView(Lcom/google/android/apps/books/widget/BooksHomeView;Z)V
    .locals 1
    .param p1, "incomingView"    # Lcom/google/android/apps/books/widget/BooksHomeView;
    .param p2, "animationTransition"    # Z

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    if-eqz v0, :cond_0

    .line 785
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/widget/BooksHomeView;->hide(Z)V

    .line 787
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    .line 788
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    invoke-interface {v0, p2}, Lcom/google/android/apps/books/widget/BooksHomeView;->show(Z)V

    .line 789
    return-void
.end method

.method private showStartingHomeView()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1341
    sget-boolean v4, Lcom/google/android/apps/books/app/HomeFragment;->sShowUploads:Z

    if-eqz v4, :cond_3

    .line 1342
    sput-boolean v5, Lcom/google/android/apps/books/app/HomeFragment;->sShowUploads:Z

    .line 1344
    sget-object v4, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileName:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 1345
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 1346
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 1347
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/data/ForegroundBooksDataController;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v3

    .line 1350
    .local v3, "uploader":Lcom/google/android/apps/books/data/UploadsController;
    if-eqz v3, :cond_0

    .line 1351
    sget-object v4, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    sget-object v5, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileName:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/books/data/UploadsController;->addUpload(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/lang/String;

    .line 1352
    sput-object v6, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    .line 1353
    sput-object v6, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileName:Ljava/lang/String;

    .line 1357
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v3    # "uploader":Lcom/google/android/apps/books/data/UploadsController;
    :cond_0
    sget-object v4, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    if-eqz v4, :cond_2

    .line 1359
    :try_start_0
    sget-object v4, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1365
    :cond_1
    :goto_0
    sput-object v6, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileDesc:Landroid/os/ParcelFileDescriptor;

    .line 1367
    :cond_2
    sput-object v6, Lcom/google/android/apps/books/app/HomeFragment;->sUploadFileName:Ljava/lang/String;

    .line 1369
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->goToUploadsView()V

    .line 1379
    :goto_1
    return-void

    .line 1360
    :catch_0
    move-exception v1

    .line 1361
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "HomeFragment"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1362
    const-string v4, "HomeFragment"

    const-string v5, "Could not close sUploadFileDesc"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1372
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getStartingViewMode()Ljava/lang/String;

    move-result-object v2

    .line 1375
    .local v2, "startingViewMode":Ljava/lang/String;
    iput-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    .line 1376
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeStart(Ljava/lang/String;Landroid/content/Context;)V

    .line 1377
    invoke-direct {p0, v2, v5}, Lcom/google/android/apps/books/app/HomeFragment;->setViewMode(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method private togglePinned(Ljava/lang/String;)V
    .locals 14
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2051
    const-string v10, "HomeFragment"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2052
    const-string v10, "HomeFragment"

    const-string v11, "togglePinned"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2055
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 2056
    .local v4, "context":Landroid/content/Context;
    if-nez v4, :cond_2

    .line 2058
    const-string v10, "HomeFragment"

    const/4 v11, 0x6

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2059
    const-string v10, "HomeFragment"

    const-string v11, "togglePinned while not attached to activity"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2164
    :cond_1
    :goto_0
    return-void

    .line 2064
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v9

    .line 2065
    .local v9, "volume":Lcom/google/android/apps/books/model/VolumeData;
    if-nez v9, :cond_3

    .line 2066
    const-string v10, "HomeFragment"

    const/4 v11, 0x6

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2067
    const-string v10, "HomeFragment"

    const-string v11, "togglePinned couldn\'t find volume data"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2072
    :cond_3
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;

    invoke-interface {v10, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/ObjectAnimator;

    .line 2073
    .local v1, "animation":Landroid/animation/ObjectAnimator;
    if-eqz v1, :cond_5

    .line 2075
    const-string v10, "HomeFragment"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2076
    const-string v10, "HomeFragment"

    const-string v11, "canceling unpin animation"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2078
    :cond_4
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 2079
    iget-object v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;

    invoke-interface {v10, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2080
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateVolumeDownloadProgress()V

    .line 2081
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->CANCEL_UNPIN_ANIMATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    goto :goto_0

    .line 2085
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isPinned(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    :cond_6
    const/4 v5, 0x1

    .line 2086
    .local v5, "currentPinned":Z
    :goto_1
    if-nez v5, :cond_9

    const/4 v7, 0x1

    .line 2089
    .local v7, "newPinned":Z
    :goto_2
    if-eqz v7, :cond_7

    iget-boolean v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-eqz v10, :cond_7

    .line 2090
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 2091
    .local v0, "account":Landroid/accounts/Account;
    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    .line 2092
    .local v3, "callbacks":Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;
    invoke-static {v4, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v10

    invoke-static {v3, p1, v10}, Lcom/google/android/apps/books/net/OfflineLicenseManager;->requestOfflineLicense(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController;)V

    .line 2096
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v3    # "callbacks":Lcom/google/android/apps/books/app/HomeFragment$PinningCallbacks;
    :cond_7
    if-eqz v7, :cond_c

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_c

    .line 2097
    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v8

    .line 2098
    .local v8, "title":Ljava/lang/String;
    iget-boolean v10, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-nez v10, :cond_a

    .line 2100
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2101
    .local v2, "args":Landroid/os/Bundle;
    new-instance v10, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v10, v2}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    const v11, 0x7f0f0169

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v8, v12, v13

    invoke-virtual {p0, v11, v12}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x104000a

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x7f0f00f8

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 2106
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v10

    const-class v11, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;

    const/4 v12, 0x0

    invoke-interface {v10, v11, v2, v12}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2108
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_WHILE_OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2110
    invoke-direct {p0, p1, v7}, Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2085
    .end local v2    # "args":Landroid/os/Bundle;
    .end local v5    # "currentPinned":Z
    .end local v7    # "newPinned":Z
    .end local v8    # "title":Ljava/lang/String;
    :cond_8
    const/4 v5, 0x0

    goto :goto_1

    .line 2086
    .restart local v5    # "currentPinned":Z
    :cond_9
    const/4 v7, 0x0

    goto :goto_2

    .line 2111
    .restart local v7    # "newPinned":Z
    .restart local v8    # "title":Ljava/lang/String;
    :cond_a
    invoke-static {v4}, Lcom/google/android/apps/books/util/NetUtils;->downloadContentSilently(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 2113
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2114
    .restart local v2    # "args":Landroid/os/Bundle;
    const v10, 0x7f0f0165

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2117
    .local v6, "dialogBody":Ljava/lang/String;
    new-instance v10, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v10, v2}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    const v11, 0x7f0f0168

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setCheckboxLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x104000a

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x7f0f00f9

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 2122
    new-instance v10, Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    invoke-direct {v10, v2}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v10, p1}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    .line 2125
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v10

    const-class v11, Lcom/google/android/apps/books/app/HomeFragment$OfferOverridePinDialog;

    const/4 v12, 0x0

    invoke-interface {v10, v11, v2, v12}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 2128
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_WHILE_ON_COSTLY_CONNECTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2129
    invoke-direct {p0, p1, v7}, Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2131
    .end local v2    # "args":Landroid/os/Bundle;
    .end local v6    # "dialogBody":Ljava/lang/String;
    :cond_b
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2132
    invoke-direct {p0, p1, v7}, Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2134
    .end local v8    # "title":Ljava/lang/String;
    :cond_c
    if-nez v7, :cond_f

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 2135
    const-string v10, "HomeFragment"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2136
    const-string v10, "HomeFragment"

    const-string v11, "starting animation"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2138
    :cond_d
    new-instance v10, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v10, v4}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10}, Lcom/google/android/apps/books/preference/LocalPreferences;->getShowUnpinDialog()Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2139
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2140
    .restart local v2    # "args":Landroid/os/Bundle;
    new-instance v10, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v10, v2}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    const v11, 0x7f0f019b

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x7f0f019c

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x104000a

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const/high16 v11, 0x1040000

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setCancelLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v10

    const v11, 0x7f0f019d

    invoke-virtual {p0, v11}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setCheckboxLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 2146
    invoke-virtual {p0, v9, v2}, Lcom/google/android/apps/books/app/HomeFragment;->addVolumeArgs(Lcom/google/android/apps/books/model/VolumeData;Landroid/os/Bundle;)V

    .line 2148
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_SAW_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2149
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v10

    const-class v11, Lcom/google/android/apps/books/app/HomeFragment$UnPinDialog;

    const/4 v12, 0x0

    invoke-interface {v10, v11, v2, v12}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2153
    .end local v2    # "args":Landroid/os/Bundle;
    :cond_e
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->startUnpinProcess(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2157
    :cond_f
    invoke-direct {p0, p1, v7}, Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V

    .line 2158
    if-eqz v7, :cond_10

    .line 2159
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->PIN_LOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    goto/16 :goto_0

    .line 2161
    :cond_10
    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->UNPIN_UNLOADED_VOLUME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v10}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    goto/16 :goto_0
.end method

.method private updateActionBarTitle()V
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->updateActionBarTitle()V

    .line 727
    return-void
.end method

.method private updateDeviceConnected(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 674
    invoke-static {p1}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    .line 675
    .local v0, "connected":Z
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-eq v0, v1, :cond_1

    .line 676
    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    .line 677
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->maybeStartRecommendationsLoad(Landroid/app/Activity;)V

    .line 678
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    if-eqz v1, :cond_0

    .line 679
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->onDeviceConnectionChanged(Z)V

    .line 681
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    if-eqz v1, :cond_1

    .line 682
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onDeviceConnectionChanged(Z)V

    .line 685
    :cond_1
    return-void
.end method

.method private updatePin(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z

    .prologue
    .line 2304
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$25;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/books/app/HomeFragment$25;-><init>(Lcom/google/android/apps/books/app/HomeFragment;ZLjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 2317
    return-void
.end method

.method private updatePinInDataController(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "pinned"    # Z

    .prologue
    .line 2321
    invoke-interface {p1, p2, p3}, Lcom/google/android/apps/books/data/BooksDataController;->setPinned(Ljava/lang/String;Z)V

    .line 2326
    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .line 2327
    .local v0, "action":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    :goto_0
    invoke-interface {p1, p2, v0}, Lcom/google/android/apps/books/data/BooksDataController;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    .line 2328
    return-void

    .line 2326
    .end local v0    # "action":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    goto :goto_0
.end method

.method private withShouldAutoStartOnboarding(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "shouldAutoStartOnboardingConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1580
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1582
    .local v0, "context":Landroid/content/Context;
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1583
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1615
    :goto_0
    return-void

    .line 1587
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_AUTO_START_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1588
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 1592
    :cond_1
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 1593
    .local v1, "localPrefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHasCheckedAutostartOfOnboardingQuiz()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1594
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 1599
    :cond_2
    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/preference/LocalPreferences;->setHasCheckedAutostartOfOnboardingQuiz(Z)V

    .line 1601
    new-instance v2, Lcom/google/android/apps/books/app/HomeFragment$21;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$21;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/ublib/utils/Consumer;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mIsSchoolOwnedConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 1613
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mIsSchoolOwnedConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v3}, Lcom/google/android/ublib/utils/Consumers;->weaklyWrapped(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/BooksApplication;->withIsSchoolOwned(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method


# virtual methods
.method protected addVolumeArgs(Lcom/google/android/apps/books/model/VolumeData;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 2293
    new-instance v0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;-><init>(Landroid/os/Bundle;)V

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    .line 2298
    return-void
.end method

.method public alreadyFetchedMyEbooks()Z
    .locals 4

    .prologue
    .line 1800
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastMyEbooksFetchTime(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dismissOffer(Ljava/lang/String;)V
    .locals 2
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 2761
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mBackgroundDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$31;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$31;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 2767
    return-void
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2383
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getCoverLoadEnsurer(Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageManager$Ensurer;
    .locals 3
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 2387
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/HomeFragment;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 2388
    .local v0, "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    if-nez v0, :cond_0

    .line 2389
    const/4 v2, 0x0

    .line 2392
    :goto_0
    return-object v2

    .line 2391
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    .line 2392
    .local v1, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    new-instance v2, Lcom/google/android/apps/books/app/HomeFragment$27;

    invoke-direct {v2, p0, v1, p1}, Lcom/google/android/apps/books/app/HomeFragment$27;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V

    goto :goto_0
.end method

.method public getDataCache()Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    .locals 1

    .prologue
    .line 2701
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    return-object v0
.end method

.method public getDownloadedOnlyMode()Z
    .locals 2

    .prologue
    .line 1860
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1862
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 1863
    const/4 v1, 0x0

    .line 1866
    :goto_0
    return v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getDownloadedOnlyMode()Z

    move-result v1

    goto :goto_0
.end method

.method public getLibrarySortOrder()Lcom/google/android/apps/books/app/LibraryComparator;
    .locals 2

    .prologue
    .line 2706
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOverrideUploadsComparator:Lcom/google/android/apps/books/app/LibraryComparator;

    if-eqz v0, :cond_0

    .line 2707
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOverrideUploadsComparator:Lcom/google/android/apps/books/app/LibraryComparator;

    .line 2709
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->getLibrarySortOrderFromPrefs()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v0

    goto :goto_0
.end method

.method public getMyLibraryFilter()Lcom/google/android/apps/books/app/LibraryFilter;
    .locals 1

    .prologue
    .line 1805
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    return-object v0
.end method

.method public getMyLibraryFilterSource()Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;
    .locals 2

    .prologue
    .line 1810
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilterSource:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    if-nez v0, :cond_0

    .line 1811
    new-instance v0, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilterSource:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    .line 1813
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilterSource:Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    return-object v0
.end method

.method public getUploadsController()Lcom/google/android/apps/books/data/UploadsController;
    .locals 2

    .prologue
    .line 2755
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataController;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v0

    return-object v0
.end method

.method public getViewMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCurrentViewMode:Ljava/lang/String;

    return-object v0
.end method

.method public getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1672
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    if-nez v3, :cond_0

    move-object v0, v2

    .line 1680
    :goto_0
    return-object v0

    .line 1674
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mVolumesData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeData;

    .line 1675
    .local v0, "data":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .end local v0    # "data":Lcom/google/android/apps/books/model/VolumeData;
    :cond_2
    move-object v0, v2

    .line 1680
    goto :goto_0
.end method

.method public goToUploadsView()V
    .locals 4

    .prologue
    .line 767
    const-string v2, "mylibrary"

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/app/HomeFragment;->setViewMode(Ljava/lang/String;Z)V

    .line 768
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v3, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    if-eq v2, v3, :cond_0

    .line 769
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getMyLibraryFilterSource()Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;->getPossibleFilters()Ljava/util/List;

    move-result-object v0

    .line 770
    .local v0, "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    sget-object v2, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 771
    .local v1, "uploadsPos":I
    if-ltz v1, :cond_1

    .line 772
    iput v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSelectedLibraryFilter:I

    .line 773
    sget-object v2, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryFilter:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 774
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onHomeFragmentLibraryFilterChanged()V

    .line 775
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 780
    .end local v0    # "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    .end local v1    # "uploadsPos":I
    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/apps/books/app/LibraryComparator;->BY_RECENCY:Lcom/google/android/apps/books/app/LibraryComparator;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/HomeFragment;->setOverrideUploadsComparator(Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 781
    return-void

    .line 776
    .restart local v0    # "filters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LibraryFilter;>;"
    .restart local v1    # "uploadsPos":I
    :cond_1
    const-string v2, "HomeFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 777
    const-string v2, "HomeFragment"

    const-string v3, "Could not find the uploads filter !?"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isAvailableForReading(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1784
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isReadableOffline(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->isExpiredNonSampleRental(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFullyDownloaded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1852
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getLicenseAction(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-ne v0, v1, :cond_0

    .line 1853
    const/4 v0, 0x0

    .line 1855
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1762
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getLicenseAction(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-ne v0, v1, :cond_0

    .line 1763
    const/4 v0, 0x0

    .line 1765
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isPinned(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1758
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getPinned(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isReadNowVolume(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 10
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 1790
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    .line 1791
    .local v4, "volumeId":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v5, v5, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 1792
    .local v1, "localData":Lcom/google/android/apps/books/model/LocalVolumeData;
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v5, v5, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 1794
    .local v0, "downloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-wide v8, 0x7fffffffffffffffL

    invoke-interface {v5, v6, v8, v9}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastSyncTime(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1795
    .local v2, "lastSync":J
    invoke-static {p1, v1, v0, v2, v3}, Lcom/google/android/apps/books/model/VolumeDataUtils;->isReadNowVolume(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;J)Z

    move-result v5

    return v5
.end method

.method public isSideDrawerOpen()Z
    .locals 1

    .prologue
    .line 732
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->isSideDrawerOpen()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeUpdateMenu()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1702
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    if-nez v3, :cond_0

    .line 1725
    :goto_0
    return-void

    .line 1709
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->isSideDrawerOpen()Z

    move-result v0

    .line 1711
    .local v0, "isSideDrawerOpen":Z
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    sget-object v7, Lcom/google/android/apps/books/app/HomeMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    if-nez v0, :cond_1

    move v3, v4

    :goto_1
    invoke-interface {v6, v7, v3}, Lcom/google/android/apps/books/app/HomeMenu;->setItemVisible(Lcom/google/android/apps/books/app/HomeMenu$Item;Z)V

    .line 1713
    iget-object v6, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    sget-object v7, Lcom/google/android/apps/books/app/HomeMenu$Item;->REFRESH:Lcom/google/android/apps/books/app/HomeMenu$Item;

    if-nez v0, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCreatingShortcut:Z

    if-nez v3, :cond_2

    move v3, v4

    :goto_2
    invoke-interface {v6, v7, v3}, Lcom/google/android/apps/books/app/HomeMenu;->setItemVisible(Lcom/google/android/apps/books/app/HomeMenu$Item;Z)V

    .line 1716
    if-nez v0, :cond_3

    const-string v3, "mylibrary"

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getViewMode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v4

    .line 1718
    .local v1, "showSort":Z
    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    sget-object v6, Lcom/google/android/apps/books/app/HomeMenu$Item;->SORT:Lcom/google/android/apps/books/app/HomeMenu$Item;

    invoke-interface {v3, v6, v1}, Lcom/google/android/apps/books/app/HomeMenu;->setItemVisible(Lcom/google/android/apps/books/app/HomeMenu$Item;Z)V

    .line 1721
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v6, "books:show_testing_ui"

    invoke-static {v3, v6, v5}, Lcom/google/android/apps/books/util/GservicesHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    move v2, v4

    .line 1724
    .local v2, "showTestingOptions":Z
    :goto_4
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    sget-object v4, Lcom/google/android/apps/books/app/HomeMenu$Item;->TESTING_OPTIONS:Lcom/google/android/apps/books/app/HomeMenu$Item;

    invoke-interface {v3, v4, v2}, Lcom/google/android/apps/books/app/HomeMenu;->setItemVisible(Lcom/google/android/apps/books/app/HomeMenu$Item;Z)V

    goto :goto_0

    .end local v1    # "showSort":Z
    .end local v2    # "showTestingOptions":Z
    :cond_1
    move v3, v5

    .line 1711
    goto :goto_1

    :cond_2
    move v3, v5

    .line 1713
    goto :goto_2

    :cond_3
    move v1, v5

    .line 1716
    goto :goto_3

    .restart local v1    # "showSort":Z
    :cond_4
    move v2, v5

    .line 1721
    goto :goto_4
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 613
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 615
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeGetDataController(Landroid/content/Context;)V

    .line 616
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getCreatingShortcut(Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCreatingShortcut:Z

    .line 617
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 2621
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/BooksFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2622
    packed-switch p1, :pswitch_data_0

    .line 2633
    :cond_0
    :goto_0
    return-void

    .line 2624
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    if-eqz v0, :cond_0

    .line 2628
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->maybeUpdateOnboardCardVisibility()V

    goto :goto_0

    .line 2622
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 555
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onAttach(Landroid/app/Activity;)V

    .line 556
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->maybeGetDataController(Landroid/content/Context;)V

    .line 557
    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2583
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z

    if-eqz v1, :cond_0

    .line 2584
    iput-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z

    .line 2585
    const-string v0, "readnow"

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->onUserSelectedViewMode(Ljava/lang/String;)V

    .line 2586
    const/4 v0, 0x1

    .line 2588
    :cond_0
    return v0
.end method

.method public onBookSelected(Ljava/lang/String;Landroid/view/View;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pressedView"    # Landroid/view/View;

    .prologue
    .line 2432
    sget-boolean v1, Lcom/google/android/apps/books/app/HomeFragment;->LOGD:Z

    if-eqz v1, :cond_0

    const-string v1, "HomeFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "openBookInReader("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2434
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 2435
    .local v0, "view":Landroid/view/View;
    if-nez v0, :cond_1

    .line 2499
    :goto_0
    return-void

    .line 2437
    :cond_1
    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$28;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/app/HomeFragment$28;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onCardOverflowMenuShown()V
    .locals 0

    .prologue
    .line 1871
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V

    .line 1872
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 471
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onCreate(Landroid/os/Bundle;)V

    .line 472
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/HomeFragment;->setHasOptionsMenu(Z)V

    .line 474
    if-eqz p1, :cond_0

    .line 475
    const-string v2, "HomeFragment.mGoToReadNow"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z

    .line 478
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mFragmentCreateTime:J

    .line 480
    invoke-static {}, Lcom/google/android/ublib/utils/Consumers;->getNoopConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    .line 481
    .local v1, "noop":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$2;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/books/app/HomeFragment$2;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 497
    new-instance v0, Lcom/google/android/apps/books/app/HomeFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/HomeFragment$3;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 516
    .local v0, "callbacks":Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;
    new-instance v2, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    const-wide/16 v4, 0x1f4

    invoke-direct {v2, v0, v4, v5}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadVolumesScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    .line 518
    invoke-static {p0}, Lcom/google/android/apps/books/app/HomeFragment;->makeRentalBadgesHandler(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/os/Handler;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRentalBadgesHandler:Landroid/os/Handler;

    .line 519
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 1685
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/app/BooksFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1687
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1688
    new-instance v0, Lcom/google/android/apps/books/app/HomeMenuImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenuCallbacks:Lcom/google/android/apps/books/app/HomeFragment$MenuCallbacks;

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/books/app/HomeMenuImpl;-><init>(Landroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/apps/books/app/HomeMenu$Callbacks;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    .line 1690
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 839
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 841
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mResolver:Landroid/content/ContentResolver;

    .line 843
    const v1, 0x7f040051

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    .line 845
    new-instance v1, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 847
    iget-wide v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mEbooksFetchTimeBeforeFragmentCreation:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 848
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastMyEbooksFetchTime(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mEbooksFetchTimeBeforeFragmentCreation:J

    .line 852
    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    .line 854
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    const v3, 0x7f0e0108

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 855
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    const v3, 0x7f0e0107

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 856
    new-instance v1, Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-boolean v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecentSeeAllClickHandler:Landroid/view/View$OnClickListener;

    invoke-direct {v1, p0, v3, v4, v5}, Lcom/google/android/apps/books/widget/ReadNowHomeView;-><init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;ZLandroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    .line 858
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iget-boolean v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mIsNewUserForOnboarding:Z

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setIsNewUser(Z)V

    .line 859
    new-instance v1, Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibPlayHeader:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-boolean v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    invoke-direct {v1, p0, v3, v4}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;-><init>(Lcom/google/android/apps/books/widget/BooksHomeController;Landroid/view/ViewGroup;Z)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    .line 861
    new-instance v1, Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawerCallbacks:Lcom/google/android/apps/books/app/HomeFragment$SideDrawerCallbacks;

    iget-boolean v6, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCreatingShortcut:Z

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/google/android/apps/books/app/HomeSideDrawer;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;Z)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    .line 864
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$10;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/HomeFragment$10;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 877
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$11;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/HomeFragment$11;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 901
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v3, Lcom/google/android/apps/books/app/HomeFragment$12;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/HomeFragment$12;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 908
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->sendAccountToViews(Landroid/accounts/Account;)V

    .line 909
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v1, v1, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->sendDownloadFractionsToViews(Ljava/util/Map;)V

    .line 910
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v1, v1, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->sendLocalVolumeDataToViews(Ljava/util/Map;)V

    .line 912
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mConnReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 915
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->myEbooksNeverFetched()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/books/app/HomeFragment;->maybeStartOffersLoad(Landroid/app/Activity;Z)V

    .line 917
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->setupReadNowHeader()V

    .line 918
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->setupMyLibraryHeader()V

    .line 920
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mView:Landroid/view/ViewGroup;

    return-object v1

    :cond_1
    move v1, v2

    .line 915
    goto :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    .line 1228
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->onDestroy()V

    .line 1229
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onDestroy()V

    .line 1234
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;

    invoke-static {v3}, Lcom/google/common/collect/Maps;->newHashMap(Ljava/util/Map;)Ljava/util/HashMap;

    move-result-object v2

    .line 1238
    .local v2, "localUnpinCountdownAnimations":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/animation/ObjectAnimator;>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1239
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/animation/ObjectAnimator;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1240
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/HomeFragment;->onUnpinAnimationEnd(Ljava/lang/String;)V

    goto :goto_0

    .line 1245
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/animation/ObjectAnimator;>;"
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v4, Lcom/google/android/apps/books/app/HomeFragment$16;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/app/HomeFragment$16;-><init>(Lcom/google/android/apps/books/app/HomeFragment;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 1252
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRentalBadgesHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1253
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRentalBadgesHandler:Landroid/os/Handler;

    .line 1255
    const/4 v3, 0x0

    sput-boolean v3, Lcom/google/android/apps/books/app/HomeFragment;->sClosedBookInSecondHalf:Z

    .line 1257
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDestroy()V

    .line 1258
    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1210
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDestroyView()V

    .line 1212
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1213
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mConnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1215
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->onDestroyView()V

    .line 1216
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onDestroyView()V

    .line 1218
    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mHomeView:Lcom/google/android/apps/books/widget/BooksHomeView;

    .line 1220
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    if-eqz v1, :cond_0

    .line 1221
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/HomeMenu;->tearDown()V

    .line 1222
    iput-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    .line 1224
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->clearHatsState()V

    .line 626
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDetach()V

    .line 627
    return-void
.end method

.method public onLibrarySortOrderChanged()V
    .locals 1

    .prologue
    .line 752
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->setOverrideUploadsComparator(Lcom/google/android/apps/books/app/LibraryComparator;)V

    .line 753
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeResetCards()V

    .line 754
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->moveHomeViewToTop()V

    .line 755
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->removeHatsPromptView()V

    .line 756
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 1733
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/app/HomeSideDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1742
    :goto_0
    return v0

    .line 1735
    :cond_0
    const v1, 0x7f0e0210

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1736
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;->HOME_MENU_SORT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeMenuAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeMenuOptionAction;)V

    .line 1737
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->getSortComparator()Lcom/google/android/apps/books/app/LibraryComparator;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->showLibrarySortMenu(Lcom/google/android/apps/books/app/LibraryComparator;)V

    goto :goto_0

    .line 1739
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    if-eqz v0, :cond_2

    .line 1740
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/HomeMenu;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 1742
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1431
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onPause()V

    .line 1433
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    if-eqz v0, :cond_0

    .line 1434
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onPause()V

    .line 1436
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    if-eqz v0, :cond_1

    .line 1437
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->onPause()V

    .line 1440
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1441
    return-void
.end method

.method public onPinSelected(Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1748
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$22;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$22;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1754
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1694
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1695
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeUpdateMenu()V

    .line 1696
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 1383
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onResume()V

    .line 1385
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetActivityOrientation()V

    .line 1387
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mContentObserver:Landroid/database/ContentObserver;

    if-nez v1, :cond_0

    .line 1388
    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$17;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/app/HomeFragment$17;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mContentObserver:Landroid/database/ContentObserver;

    .line 1396
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildAccountVolumesDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v0

    .line 1397
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1403
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLoadVolumesScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    invoke-interface {v1}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    .line 1407
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mSideDrawer:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/HomeSideDrawer;->syncActionBarIconState()V

    .line 1411
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->updateActionBarTitle()V

    .line 1415
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    if-eqz v1, :cond_1

    .line 1416
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->onResume()V

    .line 1418
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    if-eqz v1, :cond_2

    .line 1419
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->onResume()V

    .line 1422
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->maybeShowDogfoodDialog(Landroid/content/Context;)V

    .line 1424
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeAdvanceHats()V

    .line 1426
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->maybeAutoStartOnboarding()V

    .line 1427
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 2557
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2558
    const-string v0, "HomeFragment.mGoToReadNow"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mGoToReadNowOnBackPressed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2559
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 1728
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mMenu:Lcom/google/android/apps/books/app/HomeMenu;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeMenu;->onSearchRequested()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1262
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onStart()V

    .line 1264
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetActivityOrientation()V

    .line 1267
    const/4 v0, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/WindowUtils;->setBrightness(ILandroid/view/Window;)V

    .line 1269
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->showStartingHomeView()V

    .line 1273
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment;->mCardsData:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1274
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->resetHomeViewCards()V

    .line 1276
    :cond_0
    return-void
.end method

.method public removeFromLibrary(Ljava/lang/String;)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2528
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    .line 2529
    .local v3, "volume":Lcom/google/android/apps/books/model/VolumeData;
    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;->HOME_OVERFLOW_DELETE_FROM_LIBRARY_PROMPTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v3, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeOverflowAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/Context;)V

    .line 2533
    if-nez v3, :cond_1

    .line 2534
    const-string v4, "HomeFragment"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2535
    const-string v4, "HomeFragment"

    const-string v5, "missing metadata for volume"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2553
    :cond_0
    :goto_0
    return-void

    .line 2539
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2540
    .local v0, "args":Landroid/os/Bundle;
    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v4

    if-eqz v4, :cond_2

    const v2, 0x7f0f0171

    .line 2543
    .local v2, "titleTextId":I
    :goto_1
    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->isUploaded()Z

    move-result v4

    if-eqz v4, :cond_3

    const v1, 0x7f0f0173

    .line 2546
    .local v1, "bodyTextId":I
    :goto_2
    new-instance v4, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v4, v0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v4

    const v5, 0x7f0f0174

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v4

    const/high16 v5, 0x1040000

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setCancelLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v4

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 2551
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/books/app/HomeFragment;->addVolumeArgs(Lcom/google/android/apps/books/model/VolumeData;Landroid/os/Bundle;)V

    .line 2552
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/books/app/HomeFragment$DeleteFromLibraryDialog;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v0, v6}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 2540
    .end local v1    # "bodyTextId":I
    .end local v2    # "titleTextId":I
    :cond_2
    const v2, 0x7f0f0170

    goto :goto_1

    .line 2543
    .restart local v2    # "titleTextId":I
    :cond_3
    const v1, 0x7f0f0172

    goto :goto_2
.end method

.method public removeFromRecommendations(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2563
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/Eventual;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2564
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v2, Lcom/google/android/apps/books/app/HomeFragment$29;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$29;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 2570
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mRecommendedBooks:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/Eventual;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->filterRecommendations(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2572
    .local v0, "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2573
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->onRecommendationDismissed(Ljava/lang/String;Ljava/util/List;)V

    .line 2579
    .end local v0    # "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    :cond_0
    :goto_0
    return-void

    .line 2576
    .restart local v0    # "validRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setRecommendations(Ljava/util/List;)V

    goto :goto_0
.end method

.method public startBuyBook(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;
    .param p3, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    const/4 v5, 0x0

    .line 2669
    iget-boolean v6, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-nez v6, :cond_0

    .line 2670
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const v7, 0x7f0f00b7

    invoke-static {v6, v7, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 2697
    :goto_0
    return-void

    .line 2673
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v6

    invoke-static {v6, p1, p2}, Lcom/google/android/apps/books/util/OceanUris;->getBuyTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2675
    .local v2, "fallbackBuyUrl":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v4

    .line 2677
    .local v4, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    const/4 v3, 0x1

    .line 2678
    .local v3, "useDirectPurchaseApi":Z
    if-nez v4, :cond_2

    .line 2680
    move-object v0, v2

    .line 2696
    .local v0, "buyUrl":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v5

    invoke-interface {v5, p1, v0, v3, p3}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V

    goto :goto_0

    .line 2683
    .end local v0    # "buyUrl":Ljava/lang/String;
    :cond_2
    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v0

    .line 2684
    .restart local v0    # "buyUrl":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 2686
    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v1

    .line 2688
    .local v1, "canonicalUrl":Ljava/lang/String;
    if-eqz p3, :cond_3

    iget-boolean v3, p3, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    .line 2689
    :goto_2
    if-eqz v1, :cond_4

    .line 2690
    move-object v0, v1

    goto :goto_1

    :cond_3
    move v3, v5

    .line 2688
    goto :goto_2

    .line 2692
    :cond_4
    move-object v0, v2

    goto :goto_1
.end method

.method public startOnboarding(I)V
    .locals 3
    .param p1, "sequenceType"    # I

    .prologue
    .line 2637
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2638
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/books/onboard/BooksOnboardHostActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2639
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "OnboardIntentBuilder_sequenceType"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2640
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/books/app/HomeFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2642
    return-void
.end method

.method public startUnpinProcess(Ljava/lang/String;)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2275
    new-instance v2, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;-><init>(Lcom/google/android/apps/books/app/HomeFragment;Ljava/lang/String;)V

    .line 2276
    .local v2, "handler":Lcom/google/android/apps/books/app/HomeFragment$UnpinProgressHandler;
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mLocalVolumeDataCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->getDownloadFraction(Ljava/lang/String;)F

    move-result v1

    .line 2277
    .local v1, "downloadFraction":F
    const-string v3, "progress"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    aput v1, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v6, v7

    invoke-static {v2, v3, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2279
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/google/android/apps/books/app/HomeFragment;->mUnpinCountdownAnimations:Ljava/util/Map;

    invoke-interface {v3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2280
    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2281
    const v3, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v1, v3

    if-lez v3, :cond_0

    const-wide/16 v4, 0x1388

    .line 2283
    .local v4, "undownloadDelay":J
    :goto_0
    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2284
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2286
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;->START_UNPIN_COUNTDOWN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPinAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PinAction;)V

    .line 2287
    return-void

    .line 2281
    .end local v4    # "undownloadDelay":J
    :cond_0
    const-wide/16 v4, 0x1f4

    goto :goto_0
.end method

.method public viewAboutThisBook(Ljava/lang/String;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2657
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/HomeFragment;->mDeviceConnected:Z

    if-nez v1, :cond_0

    .line 2658
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0f00b9

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2665
    :goto_0
    return-void

    .line 2662
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    .line 2663
    .local v0, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v2

    const-string v3, "books_inapp_home_longpress_about"

    invoke-interface {v1, p1, v2, v3}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public viewBookStore(Ljava/lang/String;)V
    .locals 1
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 2646
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksActivity;->getHomeCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/HomeFragment$Callbacks;->startShop(Ljava/lang/String;)V

    .line 2647
    return-void
.end method

.method public viewOffers()V
    .locals 3

    .prologue
    .line 2614
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2615
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/books/app/OffersActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2616
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 2617
    return-void
.end method

.method public viewRecommendedSample(Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2601
    sget-object v0, Lcom/google/android/apps/books/app/BookOpeningFlags;->FROM_RECOMMENDED_SAMPLE:Lcom/google/android/apps/books/app/BookOpeningFlags;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/app/HomeFragment;->openBook(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V

    .line 2602
    return-void
.end method

.method public viewStoreLink(Lcom/google/android/apps/books/app/StoreLink;)V
    .locals 3
    .param p1, "storeLink"    # Lcom/google/android/apps/books/app/StoreLink;

    .prologue
    .line 2596
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/HomeFragment;->getCallbacks()Lcom/google/android/apps/books/app/HomeFragment$Callbacks;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/books/app/StoreLink;->viewStorePage(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Z)V

    .line 2597
    return-void
.end method

.method public viewStoreLinkForRecommendedBook(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2651
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->HOME_VIEW_RECOMMENDATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 2652
    const-string v0, "books_inapp_home_recommended_book"

    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/StoreLink;->forBook(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/app/StoreLink;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/HomeFragment;->viewStoreLink(Lcom/google/android/apps/books/app/StoreLink;)V

    .line 2653
    return-void
.end method
