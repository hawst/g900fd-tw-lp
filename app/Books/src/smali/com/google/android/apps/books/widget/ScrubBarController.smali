.class public Lcom/google/android/apps/books/widget/ScrubBarController;
.super Ljava/lang/Object;
.source "ScrubBarController.java"


# instance fields
.field private mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mQuickBookmarkPositions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end field

.field private mSavedScrubberPosition:I

.field private mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

.field private mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

.field private final mScrubListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;)V
    .locals 1
    .param p1, "scrubListener"    # Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    .line 30
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    .line 31
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mSavedScrubberPosition:I

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    .line 48
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    .line 49
    return-void
.end method

.method private nullRenderConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/google/android/apps/books/widget/ScrubBarController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ScrubBarController$1;-><init>(Lcom/google/android/apps/books/widget/ScrubBarController;)V

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    .line 76
    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrubBarController;->onDestroyScrubBar()V

    .line 78
    return-void
.end method

.method public getCurrentSpread()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 5

    .prologue
    .line 195
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ScrubBar;->getPosition()I

    move-result v1

    .line 196
    .local v1, "position":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 197
    const/4 v2, 0x0

    .line 200
    :goto_0
    return-object v2

    .line 199
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "pageId":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    goto :goto_0
.end method

.method public getNewThumbnailRenderConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-nez v0, :cond_0

    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/ScrubBarController;->nullRenderConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;

    move-result-object v0

    .line 146
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->getNewThumbnailRenderConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;

    move-result-object v0

    goto :goto_0
.end method

.method public getQuickBookmarkPosition(I)Lcom/google/android/apps/books/common/Position;
    .locals 1
    .param p1, "itemIndex"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public isScrubbing()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->isScrubbing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeInitializeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v0, :cond_0

    .line 82
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 83
    new-instance v0, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->setScrubIndex(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V

    .line 89
    :cond_0
    return-void
.end method

.method public moveToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 8
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v7, 0x6

    .line 103
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-nez v5, :cond_1

    .line 104
    :cond_0
    const-string v5, "SBController"

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 105
    const-string v5, "SBController"

    const-string v6, "Invalid state to moveToSpread"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    .end local p1    # "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_0
    return-object p1

    .line 113
    .restart local p1    # "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v6, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v6}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 115
    .local v4, "scrubberPosition":I
    move-object v3, p1

    .line 132
    .local v3, "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/books/widget/ScrubBar;->setPosition(I)V

    move-object p1, v3

    .line 133
    goto :goto_0

    .line 116
    .end local v3    # "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v4    # "scrubberPosition":I
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v5, "SBController"

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 118
    const-string v5, "SBController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to find requested position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " in volume "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getDefaultPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 122
    .local v0, "defaultPosition":Lcom/google/android/apps/books/common/Position;
    new-instance v3, Lcom/google/android/apps/books/render/SpreadIdentifier;

    const/4 v5, 0x0

    invoke-direct {v3, v0, v5}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 124
    .restart local v3    # "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :try_start_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    .restart local v4    # "scrubberPosition":I
    goto :goto_1

    .line 125
    .end local v4    # "scrubberPosition":I
    :catch_1
    move-exception v2

    .line 127
    .local v2, "e1":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v5, "SBController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to find default position in volume "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v4, 0x0

    .restart local v4    # "scrubberPosition":I
    goto :goto_1
.end method

.method public onDestroyScrubBar()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->getPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mSavedScrubberPosition:I

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ScrubBar;->destroy()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    .line 72
    :cond_0
    return-void
.end method

.method public onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V
    .locals 2
    .param p1, "handle"    # Lcom/google/android/apps/books/render/PageHandle;

    .prologue
    .line 171
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-nez v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v0

    .line 175
    .local v0, "pageIndex":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->isScrubbing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/ScrubBar;->setPosition(I)V

    goto :goto_0
.end method

.method public onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V
    .locals 2
    .param p1, "endHandle"    # Lcom/google/android/apps/books/render/PageHandle;
    .param p2, "progress"    # F

    .prologue
    .line 181
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-nez v1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v0

    .line 187
    .local v0, "endPosition":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->isScrubbing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/books/widget/ScrubBar;->setPositionAnimationProgress(IF)V

    goto :goto_0
.end method

.method public onScrubBarCreated(Lcom/google/android/apps/books/widget/ScrubBar;)V
    .locals 3
    .param p1, "scrubBar"    # Lcom/google/android/apps/books/widget/ScrubBar;

    .prologue
    const/4 v2, -0x1

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubListener:Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->setOnScrubListener(Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->setScrubIndex(Lcom/google/android/apps/books/widget/ScrubBar$ScrubberMetadata;)V

    .line 60
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mSavedScrubberPosition:I

    if-eq v0, v2, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    iget v1, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mSavedScrubberPosition:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ScrubBar;->setPosition(I)V

    .line 62
    iput v2, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mSavedScrubberPosition:I

    .line 64
    :cond_1
    return-void
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubIndex:Lcom/google/android/apps/books/util/VolumeScrubberMetadata;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/VolumeScrubberMetadata;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 95
    :cond_0
    return-void
.end method

.method public setStartOfSkimPosition(I)V
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/ScrubBar;->setStartOfSkimPosition(I)V

    .line 140
    :cond_0
    return-void
.end method

.method public updateQuickBookmarks(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "recentBookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v7, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 213
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 214
    .local v0, "an":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 217
    .end local v0    # "an":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v8}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionComparator()Ljava/util/Comparator;

    move-result-object v8

    invoke-static {v7, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 220
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    new-array v6, v7, [I

    .line 221
    .local v6, "pageIndices":[I
    const/4 v2, 0x0

    .line 222
    .local v2, "i":I
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mQuickBookmarkPositions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/common/Position;

    .line 224
    .local v5, "p":Lcom/google/android/apps/books/common/Position;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v5}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v7

    aput v7, v6, v2
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 228
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 225
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :catch_0
    move-exception v1

    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    move v2, v3

    .line 229
    .end local v1    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .end local v3    # "i":I
    .end local v5    # "p":Lcom/google/android/apps/books/common/Position;
    .restart local v2    # "i":I
    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/books/widget/ScrubBarController;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v7, v6}, Lcom/google/android/apps/books/widget/ScrubBar;->setBookmarkItems([I)V

    goto :goto_0
.end method
