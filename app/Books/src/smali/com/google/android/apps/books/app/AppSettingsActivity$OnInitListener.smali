.class Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnInitListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/AppSettingsActivity$1;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .locals 5
    .param p1, "status"    # I

    .prologue
    .line 90
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # getter for: Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$000(Lcom/google/android/apps/books/app/AppSettingsActivity;)Landroid/speech/tts/TextToSpeech;

    move-result-object v1

    if-nez v1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 94
    :cond_0
    if-nez p1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # getter for: Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$000(Lcom/google/android/apps/books/app/AppSettingsActivity;)Landroid/speech/tts/TextToSpeech;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/speech/tts/TextToSpeech;->getFeatures(Ljava/util/Locale;)Ljava/util/Set;

    move-result-object v0

    .line 96
    .local v0, "features":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    const-string v1, "networkTts"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/app/AppSettingsActivity;->mIsNetworkTtsSupported:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$102(Lcom/google/android/apps/books/app/AppSettingsActivity;Z)Z

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # getter for: Lcom/google/android/apps/books/app/AppSettingsActivity;->mAppSettingsAdapter:Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$200(Lcom/google/android/apps/books/app/AppSettingsActivity;)Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/AppSettingsActivity$NetworkTts;

    iget-object v3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/app/AppSettingsActivity$NetworkTts;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;->add(Ljava/lang/Object;)V

    .line 102
    .end local v0    # "features":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # getter for: Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$000(Lcom/google/android/apps/books/app/AppSettingsActivity;)Landroid/speech/tts/TextToSpeech;

    move-result-object v1

    invoke-virtual {v1}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OnInitListener;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/books/app/AppSettingsActivity;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$002(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/speech/tts/TextToSpeech;)Landroid/speech/tts/TextToSpeech;

    goto :goto_0
.end method
