.class public Lcom/google/android/apps/books/util/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field private static final sSystemOverrides:Lorg/json/JSONObject;


# instance fields
.field private final mBaseApiaryUri:Ljava/lang/String;

.field private final mBaseContentApiUri:Landroid/net/Uri;

.field private final mLargeDevice:Z

.field private final mLoggingID:Ljava/lang/String;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPackageName:Ljava/lang/String;

.field private final mPlayStoreBaseUri:Landroid/net/Uri;

.field private final mPronouceableVersionString:Ljava/lang/String;

.field private final mSourceParam:Ljava/lang/String;

.field private mUsingCustomApiaryFrontend:Z

.field private mUsingCustomContentApiFrontend:Z

.field private final mVersionString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 105
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    sput-object v1, Lcom/google/android/apps/books/util/Config;->sSystemOverrides:Lorg/json/JSONObject;

    .line 107
    :try_start_0
    sget-object v1, Lcom/google/android/apps/books/util/Config;->sSystemOverrides:Lorg/json/JSONObject;

    const-string v2, "BooksDrawElementRects"

    const-string v3, "BooksDrawElementRects"

    const/4 v4, 0x0

    const-string v5, "BooksConfig"

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 109
    sget-object v1, Lcom/google/android/apps/books/util/Config;->sSystemOverrides:Lorg/json/JSONObject;

    const-string v2, "BooksDrawReadingPos"

    const-string v3, "BooksDrawReadingPos"

    const/4 v4, 0x0

    const-string v5, "BooksConfig"

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    .local v0, "e":Lorg/json/JSONException;
    :cond_0
    :goto_0
    return-void

    .line 111
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_0
    move-exception v0

    .line 114
    .restart local v0    # "e":Lorg/json/JSONException;
    const-string v1, "BooksConfig"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    const-string v1, "BooksConfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to set JS system overrides: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v12, 0x7f0f00f6

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-boolean v9, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomContentApiFrontend:Z

    .line 145
    iput-boolean v9, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomApiaryFrontend:Z

    .line 165
    const-string v6, "context is null in Config()"

    invoke-static {p1, v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 167
    .local v0, "appContext":Landroid/content/Context;
    const-string v6, "context.getApplicationContext() is null in Config()"

    invoke-static {v0, v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    invoke-static {p1}, Lcom/google/android/apps/books/util/ReaderUtils;->maybeInitialize(Landroid/content/Context;)V

    .line 175
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 176
    iget-object v6, p0, Lcom/google/android/apps/books/util/Config;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v7, "mPackageManager is null in Config()"

    invoke-static {v6, v7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mPackageName:Ljava/lang/String;

    .line 179
    const-string v6, "https://play.google.com/store/"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mPlayStoreBaseUri:Landroid/net/Uri;

    .line 181
    new-instance v4, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v4, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 182
    .local v4, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v4}, Lcom/google/android/apps/books/preference/LocalPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mLoggingID:Ljava/lang/String;

    .line 184
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 185
    .local v5, "res":Landroid/content/res/Resources;
    const-string v6, "res is null in Config()"

    invoke-static {v5, v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/util/Config;->isLargeDevice(Landroid/content/res/Resources;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/books/util/Config;->mLargeDevice:Z

    .line 189
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 190
    const-string v6, "ge-tablet-app"

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mSourceParam:Ljava/lang/String;

    .line 195
    :goto_0
    const/4 v3, 0x0

    .line 197
    .local v3, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/util/Config;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v7, p0, Lcom/google/android/apps/books/util/Config;->mPackageName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 198
    const-string v6, "mPackageManager.getPackageInfo(...) is null in Config()"

    invoke-static {v3, v6}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_1
    if-eqz v3, :cond_1

    .line 205
    new-array v6, v11, [Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "v"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget v7, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v5, v12, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mPronouceableVersionString:Ljava/lang/String;

    .line 208
    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v7, v6, v9

    iget v7, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v5, v12, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mVersionString:Ljava/lang/String;

    .line 216
    :goto_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/Config;->computeBaseContentApiUri(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mBaseContentApiUri:Landroid/net/Uri;

    .line 217
    invoke-static {p1}, Lcom/google/android/apps/books/util/Config;->getBaseApiaryUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mBaseApiaryUri:Ljava/lang/String;

    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/books/util/Config;->checkCustomApiary()V

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/books/util/Config;->checkApiMatch()V

    .line 220
    return-void

    .line 192
    .end local v3    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    const-string v6, "ge-android-app"

    iput-object v6, p0, Lcom/google/android/apps/books/util/Config;->mSourceParam:Ljava/lang/String;

    goto :goto_0

    .line 200
    .restart local v3    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    .line 201
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v6, "BooksConfig"

    const-string v7, "Exception in mPackageManager.getPackageInfo(...)"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 211
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    const v6, 0x7f0f00f7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 212
    .local v1, "defaultVersionString":Ljava/lang/String;
    iput-object v1, p0, Lcom/google/android/apps/books/util/Config;->mPronouceableVersionString:Ljava/lang/String;

    .line 213
    iput-object v1, p0, Lcom/google/android/apps/books/util/Config;->mVersionString:Ljava/lang/String;

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "baseFinskyUri"    # Landroid/net/Uri;
    .param p3, "largeDevice"    # Z
    .param p4, "sourceParam"    # Ljava/lang/String;
    .param p5, "versionString"    # Ljava/lang/String;
    .param p6, "playStoreBaseUri"    # Landroid/net/Uri;

    .prologue
    .line 304
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ConfigValue;->getDefaultValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ConfigValue;->getDefaultValue()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/util/Config;-><init>(Landroid/content/Context;Landroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V

    .line 307
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "baseFinskyUri"    # Landroid/net/Uri;
    .param p3, "largeDevice"    # Z
    .param p4, "sourceParam"    # Ljava/lang/String;
    .param p5, "versionString"    # Ljava/lang/String;
    .param p6, "baseContentApiUri"    # Landroid/net/Uri;
    .param p7, "baseApiaryUri"    # Ljava/lang/String;
    .param p8, "playStoreBaseUri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomContentApiFrontend:Z

    .line 145
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomApiaryFrontend:Z

    .line 282
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/util/Config;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 283
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/util/Config;->mPackageName:Ljava/lang/String;

    .line 285
    iput-boolean p3, p0, Lcom/google/android/apps/books/util/Config;->mLargeDevice:Z

    .line 286
    iput-object p4, p0, Lcom/google/android/apps/books/util/Config;->mSourceParam:Ljava/lang/String;

    .line 287
    iput-object p5, p0, Lcom/google/android/apps/books/util/Config;->mVersionString:Ljava/lang/String;

    .line 288
    iput-object p5, p0, Lcom/google/android/apps/books/util/Config;->mPronouceableVersionString:Ljava/lang/String;

    .line 290
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 291
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getLoggingId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/util/Config;->mLoggingID:Ljava/lang/String;

    .line 293
    iput-object p6, p0, Lcom/google/android/apps/books/util/Config;->mBaseContentApiUri:Landroid/net/Uri;

    .line 294
    iput-object p7, p0, Lcom/google/android/apps/books/util/Config;->mBaseApiaryUri:Ljava/lang/String;

    .line 295
    iput-object p8, p0, Lcom/google/android/apps/books/util/Config;->mPlayStoreBaseUri:Landroid/net/Uri;

    .line 296
    return-void
.end method

.method public static buildFrom(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 310
    new-instance v0, Lcom/google/android/apps/books/util/Config;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/Config;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private checkApiMatch()V
    .locals 6

    .prologue
    .line 256
    iget-boolean v1, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomContentApiFrontend:Z

    iget-boolean v2, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomApiaryFrontend:Z

    if-eq v1, v2, :cond_0

    const-string v1, "BooksConfig"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 258
    const-string v0, "Using %s content API frontend but %s Apiary frontend; did you really mean to do that?"

    .line 260
    .local v0, "format":Ljava/lang/String;
    const-string v1, "BooksConfig"

    const-string v2, "Using %s content API frontend but %s Apiary frontend; did you really mean to do that?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomContentApiFrontend:Z

    invoke-static {v5}, Lcom/google/android/apps/books/util/Config;->frontendTypeDescription(Z)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomApiaryFrontend:Z

    invoke-static {v5}, Lcom/google/android/apps/books/util/Config;->frontendTypeDescription(Z)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    .end local v0    # "format":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private checkCustomApiary()V
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mBaseApiaryUri:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ConfigValue;->getDefaultValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BooksConfig"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const-string v0, "BooksConfig"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Using custom Books Apiary: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/util/Config;->mBaseApiaryUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomApiaryFrontend:Z

    .line 253
    :cond_0
    return-void
.end method

.method private computeBaseContentApiUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    invoke-static {p1}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "baseContentApiUri":Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/ConfigValue;->getDefaultValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "BooksConfig"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    const-string v1, "BooksConfig"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Using custom Books Content API: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomContentApiFrontend:Z

    .line 234
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private static frontendTypeDescription(Z)Ljava/lang/String;
    .locals 1
    .param p0, "isCustom"    # Z

    .prologue
    .line 148
    if-eqz p0, :cond_0

    const-string v0, "custom"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "default"

    goto :goto_0
.end method

.method private static getBaseApiaryUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 267
    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getRawString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "gservicesOverride":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 271
    .end local v0    # "gservicesOverride":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "gservicesOverride":Ljava/lang/String;
    :cond_0
    const-string v1, "BooksApiary"

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/ConfigValue;->getDefaultValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BooksConfig"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getBaseContentApiUri(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 238
    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getRawString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "override":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 243
    .end local v0    # "override":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "override":Ljava/lang/String;
    :cond_0
    const-string v1, "BooksFrontend"

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/ConfigValue;->getDefaultValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BooksConfig"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getEnableBlackManga()Z
    .locals 3

    .prologue
    .line 508
    const-string v0, "BooksEnableBlackManga"

    const/4 v1, 0x1

    const-string v2, "BooksConfig"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getJavascriptSystemOverrides()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 501
    sget-object v0, Lcom/google/android/apps/books/util/Config;->sSystemOverrides:Lorg/json/JSONObject;

    return-object v0
.end method

.method public static getUseOnMediaOverlays()Z
    .locals 3

    .prologue
    .line 516
    const-string v0, "BooksEnableMO"

    const/4 v1, 0x1

    const-string v2, "BooksConfig"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/util/LogUtil;->getLogTagProperty(Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private hasActivities(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 363
    iget-object v1, p0, Lcom/google/android/apps/books/util/Config;->mPackageManager:Landroid/content/pm/PackageManager;

    const/high16 v2, 0x10000

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 365
    .local v0, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isBooksUri(Landroid/net/Uri;)Z
    .locals 6
    .param p0, "originalUri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 481
    invoke-virtual {p0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 482
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "host":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v5, ".google.com"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, ".googleusercontent.com"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 493
    .end local v1    # "host":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 488
    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 489
    .local v2, "path":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lt v5, v4, :cond_0

    .line 492
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 493
    .local v0, "element":Ljava/lang/String;
    const-string v5, "books"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "ebooks"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method private isLargeDevice(Landroid/content/res/Resources;)Z
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 223
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static makeRelative(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 436
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public encryptedOceanUriBuilder(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)Landroid/net/Uri$Builder;
    .locals 3
    .param p1, "remoteUrl"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;

    .prologue
    .line 470
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/Config;->prepareForOcean(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 471
    .local v0, "remoteBuilder":Landroid/net/Uri$Builder;
    if-eqz p2, :cond_0

    .line 472
    const-string v1, "cp_ksver"

    iget-object v2, p2, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 474
    :cond_0
    return-object v0
.end method

.method public getBaseApiaryUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mBaseApiaryUri:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseContentApiUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mBaseContentApiUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getBaseGDataUri()Landroid/net/Uri;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getInstalledNativeFinskyPackageForUrl(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 344
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 346
    .local v0, "shopIntent":Landroid/content/Intent;
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/Config;->hasActivities(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 348
    const-string v1, "com.android.vending"

    .line 356
    :goto_0
    return-object v1

    .line 351
    :cond_0
    const-string v1, "com.google.android.finsky"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/util/Config;->hasActivities(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 353
    const-string v1, "com.google.android.finsky"

    goto :goto_0

    .line 356
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInstalledNativeFinskyPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/OceanUris;->getFinskyShopUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/util/Config;->getInstalledNativeFinskyPackageForUrl(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLoggingId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mLoggingID:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayStoreBaseUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mPlayStoreBaseUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getPronounceableVersionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mPronouceableVersionString:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mSourceParam:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/apps/books/util/Config;->mVersionString:Ljava/lang/String;

    return-object v0
.end method

.method public isLargeDevice()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/Config;->mLargeDevice:Z

    return v0
.end method

.method public prepareForOcean(Ljava/lang/String;)Landroid/net/Uri;
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 445
    if-nez p1, :cond_0

    .line 446
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 448
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 449
    .local v2, "originalUri":Landroid/net/Uri;
    invoke-static {v2}, Lcom/google/android/apps/books/util/Config;->isBooksUri(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 450
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not a Google Books uri"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 452
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getBaseContentApiUri()Landroid/net/Uri;

    move-result-object v0

    .line 453
    .local v0, "baseUri":Landroid/net/Uri;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "source"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/UriUtils;->dropQueryParam(Landroid/net/Uri;[Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "source"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getSourceParam()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 458
    .local v1, "newUriBuilder":Landroid/net/Uri$Builder;
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method public usingCustomFrontend()Z
    .locals 1

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomContentApiFrontend:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/util/Config;->mUsingCustomApiaryFrontend:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
