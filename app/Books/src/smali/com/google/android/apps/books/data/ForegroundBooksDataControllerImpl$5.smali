.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->loadDownloadProgress(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 321
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$000(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->val$volumeId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getDownloadProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    move-result-object v0

    .line 322
    .local v0, "progress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v1

    .line 324
    .local v1, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->val$volumeId:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$300(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5$1;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;Lcom/google/android/apps/books/model/VolumeDownloadProgress;Ljava/util/Map;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 334
    return-void
.end method
