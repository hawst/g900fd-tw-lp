.class public abstract Lcom/google/android/apps/books/render/TextModeReaderRenderer;
.super Lcom/google/android/apps/books/render/ReaderRenderer;
.source "TextModeReaderRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/TextModeReaderRenderer$FirstNonViewablePageHandle;
    }
.end annotation


# instance fields
.field protected final mMarginNoteIconTapSize:I


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;I)V
    .locals 0
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "marginNoteIconTapSize"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ReaderRenderer;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 22
    iput p2, p0, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->mMarginNoteIconTapSize:I

    .line 23
    return-void
.end method


# virtual methods
.method public createSearchMatchRectsCache(Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ILcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
    .locals 7
    .param p1, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p2, "searchHighlightColor"    # I
    .param p3, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "I",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            ")",
            "Lcom/google/android/apps/books/widget/SearchMatchRectsCache",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->displayTwoPages()Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;ZZLcom/google/android/apps/books/widget/TextModeSearchMatchRectsCache$RenderCallbacks;)V

    return-object v0
.end method

.method public isPositionEnabled(Lcom/google/android/apps/books/common/Position;)Z
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/TextModeReaderRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegmentForPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Segment;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v0

    return v0
.end method

.method public normalizedPageCoordinatesToContentCoordinates(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "in"    # Landroid/graphics/PointF;
    .param p3, "out"    # Landroid/graphics/PointF;

    .prologue
    .line 54
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 55
    .local v0, "pageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    iget v1, p2, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->denormalizeX(F)I

    move-result v1

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->denormalizeY(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p3, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 56
    const/4 v1, 0x1

    return v1
.end method
