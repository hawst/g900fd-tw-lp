.class Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;
.super Lcom/google/android/apps/books/annotations/StubAnnotationListener;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeAnnotationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 3304
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/StubAnnotationListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 3304
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public volumeAnnotationsLoaded(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "segment"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 3309
    .local p3, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3316
    const-string v2, "ReaderFragment"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3317
    const-string v3, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Loaded "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " volume annotations in layer "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for segment "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3321
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 3322
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->addAnnotations(Ljava/util/List;)V

    .line 3324
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3329
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2, p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForSegmentIndex(I)I

    move-result v1

    .line 3330
    .local v1, "passageIndex":I
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->volumeAnnotationsLoaded(ILjava/util/List;)V

    .line 3333
    .end local v0    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .end local v1    # "passageIndex":I
    :cond_1
    return-void
.end method
