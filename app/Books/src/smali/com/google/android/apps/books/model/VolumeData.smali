.class public interface abstract Lcom/google/android/apps/books/model/VolumeData;
.super Ljava/lang/Object;
.source "VolumeData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/VolumeData$1;,
        Lcom/google/android/apps/books/model/VolumeData$Access;,
        Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    }
.end annotation


# virtual methods
.method public abstract getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;
.end method

.method public abstract getAuthor()Ljava/lang/String;
.end method

.method public abstract getBuyUrl()Ljava/lang/String;
.end method

.method public abstract getCanonicalUrl()Ljava/lang/String;
.end method

.method public abstract getDate()Ljava/lang/String;
.end method

.method public abstract getDescription()Ljava/lang/String;
.end method

.method public abstract getEtag()Ljava/lang/String;
.end method

.method public abstract getLanguage()Ljava/lang/String;
.end method

.method public abstract getLastAccess()J
.end method

.method public abstract getLocalCoverUri()Landroid/net/Uri;
.end method

.method public abstract getMaxOfflineDevices()I
.end method

.method public abstract getOldStyleOpenAccessValue()Ljava/lang/String;
.end method

.method public abstract getPageCount()I
.end method

.method public abstract getPublisher()Ljava/lang/String;
.end method

.method public abstract getReadingPosition()Ljava/lang/String;
.end method

.method public abstract getRentalExpiration()J
.end method

.method public abstract getRentalStart()J
.end method

.method public abstract getRentalState()Ljava/lang/String;
.end method

.method public abstract getServerCoverUri()Ljava/lang/String;
.end method

.method public abstract getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getViewability()Ljava/lang/String;
.end method

.method public abstract getVolumeId()Ljava/lang/String;
.end method

.method public abstract isLimitedPreview()Z
.end method

.method public abstract isPublicDomain()Z
.end method

.method public abstract isQuoteSharingAllowed()Z
.end method

.method public abstract isUploaded()Z
.end method

.method public abstract usesExplicitOfflineLicenseManagement()Z
.end method
