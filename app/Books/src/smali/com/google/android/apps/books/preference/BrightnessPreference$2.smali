.class Lcom/google/android/apps/books/preference/BrightnessPreference$2;
.super Ljava/lang/Object;
.source "BrightnessPreference.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/preference/BrightnessPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/preference/BrightnessPreference;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z
    invoke-static {v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$100(Lcom/google/android/apps/books/preference/BrightnessPreference;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$200(Lcom/google/android/apps/books/preference/BrightnessPreference;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    # invokes: Lcom/google/android/apps/books/preference/BrightnessPreference;->setBrightnessAutoChecked(Z)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$300(Lcom/google/android/apps/books/preference/BrightnessPreference;Z)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # invokes: Lcom/google/android/apps/books/preference/BrightnessPreference;->getSummaryText()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$400(Lcom/google/android/apps/books/preference/BrightnessPreference;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/google/android/ublib/utils/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # invokes: Lcom/google/android/apps/books/preference/BrightnessPreference;->updateEnabled()V
    invoke-static {v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$500(Lcom/google/android/apps/books/preference/BrightnessPreference;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$2;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->persistPreference()V

    goto :goto_0

    .line 145
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
