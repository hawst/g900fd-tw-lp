.class public Lcom/google/android/apps/books/widget/SimpleDrawableView;
.super Landroid/view/View;
.source "SimpleDrawableView.java"


# instance fields
.field private mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

.field private final mContentSize:Landroid/graphics/Point;

.field private final mPageFit:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    .line 52
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mPageFit:Landroid/graphics/Matrix;

    .line 25
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 64
    const-string v3, "SimpleDrawableView"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    const-string v3, "SimpleDrawableView"

    const-string v4, "SimpleDrawableView#onDraw"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    if-eqz v3, :cond_2

    .line 69
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/SimpleDrawable;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->getMeasuredWidth()I

    move-result v2

    .line 71
    .local v2, "viewWidth":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 72
    .local v0, "checkpoint":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    if-eq v3, v2, :cond_1

    .line 75
    int-to-float v3, v2

    iget-object v4, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 76
    .local v1, "scaleRatio":F
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mPageFit:Landroid/graphics/Matrix;

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 77
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mPageFit:Landroid/graphics/Matrix;

    invoke-virtual {v3, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 78
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mPageFit:Landroid/graphics/Matrix;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 80
    .end local v1    # "scaleRatio":F
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    invoke-interface {v3, p1}, Lcom/google/android/apps/books/util/SimpleDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 82
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 87
    .end local v0    # "checkpoint":I
    .end local v2    # "viewWidth":I
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 40
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 41
    .local v1, "availableWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 42
    .local v0, "availableHeight":I
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    if-eqz v2, :cond_0

    .line 43
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/util/SimpleDrawable;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 44
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 45
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContentSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setMeasuredDimension(II)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setContent(Lcom/google/android/apps/books/util/SimpleDrawable;)V
    .locals 2
    .param p1, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SimpleDrawableView;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    .line 29
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 31
    .local v0, "layerType":I
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->invalidate()V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->requestLayout()V

    .line 34
    return-void

    .line 29
    .end local v0    # "layerType":I
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/util/SimpleDrawable;->getViewLayerType()I

    move-result v0

    goto :goto_0
.end method
