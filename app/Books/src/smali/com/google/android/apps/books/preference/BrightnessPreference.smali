.class public Lcom/google/android/apps/books/preference/BrightnessPreference;
.super Landroid/widget/LinearLayout;
.source "BrightnessPreference.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference;


# instance fields
.field private final mAutoListener:Landroid/view/View$OnClickListener;

.field private mBinding:Z

.field private mBrightnessAuto:Landroid/widget/ImageButton;

.field private mBrightnessScrub:Landroid/widget/SeekBar;

.field private final mKey:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

.field private final mMaxValue:I

.field private final mMinValue:I

.field private mPrefs:Landroid/content/SharedPreferences;

.field private final mScrubListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mStep:I

.field private mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    iput-boolean v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z

    .line 35
    const/4 v1, 0x7

    iput v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mMinValue:I

    .line 36
    const/16 v1, 0x64

    iput v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mMaxValue:I

    .line 37
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mStep:I

    .line 140
    new-instance v1, Lcom/google/android/apps/books/preference/BrightnessPreference$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/BrightnessPreference$2;-><init>(Lcom/google/android/apps/books/preference/BrightnessPreference;)V

    iput-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mAutoListener:Landroid/view/View$OnClickListener;

    .line 153
    new-instance v1, Lcom/google/android/apps/books/preference/BrightnessPreference$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/BrightnessPreference$3;-><init>(Lcom/google/android/apps/books/preference/BrightnessPreference;)V

    iput-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mScrubListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 53
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->Preference:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 55
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mKey:Ljava/lang/String;

    .line 56
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/preference/BrightnessPreference;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getScrubCurrentValueAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/preference/BrightnessPreference;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/preference/BrightnessPreference;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/preference/BrightnessPreference;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->setBrightnessAutoChecked(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/preference/BrightnessPreference;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getSummaryText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/preference/BrightnessPreference;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->updateEnabled()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/preference/BrightnessPreference;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/preference/BrightnessPreference;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getBrightnessValue()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/preference/BrightnessPreference;)Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/BrightnessPreference;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    return-object v0
.end method

.method private adjustSeekBarRange()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getScrubMaxValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 224
    return-void
.end method

.method private getBrightnessValue()I
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x64

    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x7

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private getScrubCurrentValueAsString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getScrubMaxValue()I
    .locals 1

    .prologue
    .line 190
    const/16 v0, 0x2f

    return v0
.end method

.method private getSummaryText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f009d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f009e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getScrubCurrentValueAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setBrightnessAutoChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 138
    return-void
.end method

.method private setBrightnessValue(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    const/4 v1, 0x0

    .line 207
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 208
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->setBrightnessAutoChecked(Z)V

    .line 216
    :goto_0
    return-void

    .line 210
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->setBrightnessAutoChecked(Z)V

    .line 212
    const/16 v2, 0x64

    if-lt p1, v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getScrubMaxValue()I

    move-result v0

    .line 214
    .local v0, "progress":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    if-gez v0, :cond_1

    move v0, v1

    .end local v0    # "progress":I
    :cond_1
    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 212
    :cond_2
    add-int/lit8 v2, p1, -0x7

    div-int/lit8 v0, v2, 0x2

    goto :goto_1
.end method

.method private updateEnabled()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 128
    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 129
    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 134
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 129
    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto :goto_1
.end method


# virtual methods
.method public bindPreference()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 107
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z

    .line 109
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mKey:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 110
    .local v0, "value":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->setBrightnessValue(I)V

    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->updateEnabled()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    iput-boolean v4, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z

    .line 115
    return-void

    .line 113
    .end local v0    # "value":I
    :catchall_0
    move-exception v1

    iput-boolean v4, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z

    throw v1
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroid/widget/LinearLayout;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getSummaryText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 63
    const v1, 0x7f0e0197

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessAuto:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mAutoListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v1, 0x7f0e0198

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mScrubListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0132

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "contentDescription":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mBrightnessScrub:Landroid/widget/SeekBar;

    new-instance v2, Lcom/google/android/apps/books/preference/BrightnessPreference$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/preference/BrightnessPreference$1;-><init>(Lcom/google/android/apps/books/preference/BrightnessPreference;)V

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/books/widget/SeekBarAccessibilityDelegate;->setAccessibilityDelegate(Landroid/widget/SeekBar;Ljava/lang/String;Lcom/google/common/base/Supplier;)V

    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->adjustSeekBarRange()V

    .line 80
    return-void
.end method

.method public persistPreference()V
    .locals 4

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->getBrightnessValue()I

    move-result v0

    .line 120
    .local v0, "value":I
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;->onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->updateEnabled()V

    .line 92
    return-void
.end method

.method public setWindow(Landroid/view/Window;)V
    .locals 0
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mWindow:Landroid/view/Window;

    .line 96
    return-void
.end method

.method public setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V
    .locals 0
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "listener"    # Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mPrefs:Landroid/content/SharedPreferences;

    .line 101
    check-cast p2, Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    .end local p2    # "listener":Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;
    iput-object p2, p0, Lcom/google/android/apps/books/preference/BrightnessPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->bindPreference()V

    .line 103
    return-void
.end method
