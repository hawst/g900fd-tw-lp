.class public interface abstract Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
.super Ljava/lang/Object;
.source "BooksFragmentCallbacks.java"


# virtual methods
.method public abstract addFragment(Landroid/support/v4/app/Fragment;)V
.end method

.method public abstract addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract addVolumeToMyEBooks(Landroid/accounts/Account;Ljava/lang/String;Z)V
.end method

.method public abstract authenticationFinished(Landroid/content/Intent;Ljava/lang/Exception;I)V
.end method

.method public abstract canStartAboutVolume(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract getActionBar()Landroid/support/v7/app/ActionBar;
.end method

.method public abstract getSystemUi()Lcom/google/android/ublib/view/SystemUi;
.end method

.method public abstract isActive()Z
.end method

.method public abstract moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
.end method

.method public abstract onExternalStorageException()V
.end method

.method public abstract startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V
.end method

.method public abstract startForcedSync()V
.end method

.method public abstract startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V
.end method

.method public abstract startSearch(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract startShare(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/net/Uri;)V
.end method

.method public abstract startShop(Ljava/lang/String;)V
.end method

.method public abstract startViewCollection(Ljava/lang/String;)V
.end method
