.class Lcom/google/android/apps/books/widget/PagesViewController$4;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 2051
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$4;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;)I
    .locals 3
    .param p1, "left"    # Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;
    .param p2, "right"    # Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;

    .prologue
    .line 2054
    iget v1, p1, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->mainDistance:I

    iget v2, p2, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->mainDistance:I

    sub-int v0, v1, v2

    .line 2055
    .local v0, "main":I
    if-eqz v0, :cond_0

    .end local v0    # "main":I
    :goto_0
    return v0

    .restart local v0    # "main":I
    :cond_0
    iget v1, p1, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->secondaryDistance:I

    iget v2, p2, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->secondaryDistance:I

    sub-int v0, v1, v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 2051
    check-cast p1, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$4;->compare(Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;)I

    move-result v0

    return v0
.end method
