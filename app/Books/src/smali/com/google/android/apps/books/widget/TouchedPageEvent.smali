.class public Lcom/google/android/apps/books/widget/TouchedPageEvent;
.super Ljava/lang/Object;
.source "TouchedPageEvent.java"


# instance fields
.field public final action:I

.field public final offset:I

.field public final pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field public final touchLocation:Landroid/graphics/Point;

.field public final touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;


# direct methods
.method public constructor <init>(Landroid/graphics/Point;ILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/view/MotionEvent;Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 1
    .param p1, "xy"    # Landroid/graphics/Point;
    .param p2, "slot"    # I
    .param p3, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p4, "originalEvent"    # Landroid/view/MotionEvent;
    .param p5, "touchedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    .line 32
    iput p2, p0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->offset:I

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 34
    invoke-virtual {p4}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->action:I

    .line 35
    iput-object p5, p0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 36
    return-void
.end method


# virtual methods
.method public isLeftPageOfTwo()Z
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
