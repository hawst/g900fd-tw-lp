.class interface abstract Lcom/google/android/apps/books/model/BooksDataStoreImpl$ResourceResourcesQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "ResourceResourcesQuery"
.end annotation


# static fields
.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2093
    # getter for: Lcom/google/android/apps/books/model/BooksDataStoreImpl;->GENERIC_RESOURCE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;
    invoke-static {}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->access$200()Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "referenced_res_id"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith([Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$ResourceResourcesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method
