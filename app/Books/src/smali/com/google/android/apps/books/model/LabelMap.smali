.class public Lcom/google/android/apps/books/model/LabelMap;
.super Ljava/lang/Object;
.source "LabelMap.java"


# static fields
.field private static final EMPTY_LABELS:[Ljava/lang/String;

.field private static final EMPTY_OFFSETS:[I


# instance fields
.field private final mLabels:[Ljava/lang/String;

.field private final mOffsets:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/apps/books/model/LabelMap;->EMPTY_OFFSETS:[I

    .line 27
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/books/model/LabelMap;->EMPTY_LABELS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 4
    .param p1, "offset"    # I
    .param p2, "label"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-array v1, v3, [I

    aput p1, v1, v2

    .line 43
    .local v1, "offsets":[I
    new-array v0, v3, [Ljava/lang/String;

    aput-object p2, v0, v2

    .line 44
    .local v0, "labels":[Ljava/lang/String;
    iput-object v1, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    .line 45
    iput-object v0, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>([I[Ljava/lang/String;)V
    .locals 0
    .param p1, "offsets"    # [I
    .param p2, "labels"    # [Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static empty()Lcom/google/android/apps/books/model/LabelMap;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/apps/books/model/LabelMap;

    sget-object v1, Lcom/google/android/apps/books/model/LabelMap;->EMPTY_OFFSETS:[I

    sget-object v2, Lcom/google/android/apps/books/model/LabelMap;->EMPTY_LABELS:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/LabelMap;-><init>([I[Ljava/lang/String;)V

    return-object v0
.end method

.method public static from(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/LabelMap;
    .locals 9
    .param p0, "labelsJson"    # Ljava/lang/String;
    .param p1, "offsetsJson"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 61
    .local v2, "labelsArray":Lorg/json/JSONArray;
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 62
    .local v5, "offsetsArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 63
    .local v3, "labelsLength":I
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 64
    .local v6, "offsetsLength":I
    if-eq v3, v6, :cond_0

    .line 65
    new-instance v7, Lorg/json/JSONException;

    const-string v8, "Array lengths did not match"

    invoke-direct {v7, v8}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 67
    :cond_0
    new-array v4, v3, [I

    .line 68
    .local v4, "offsets":[I
    new-array v1, v3, [Ljava/lang/String;

    .line 69
    .local v1, "labels":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 70
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    aput v7, v4, v0

    .line 71
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v0

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    new-instance v7, Lcom/google/android/apps/books/model/LabelMap;

    invoke-direct {v7, v4, v1}, Lcom/google/android/apps/books/model/LabelMap;-><init>([I[Ljava/lang/String;)V

    return-object v7
.end method


# virtual methods
.method public cloneLabels()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public cloneOffsets()[I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method public debugString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 116
    const-string v3, "["

    .line 117
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "ii":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 118
    iget-object v4, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    aget v2, v4, v0

    .line 119
    .local v2, "offset":I
    iget-object v4, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    aget-object v1, v4, v0

    .line 120
    .local v1, "label":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    .end local v1    # "label":Ljava/lang/String;
    .end local v2    # "offset":I
    :cond_0
    return-object v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    if-ne p0, p1, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v1

    .line 132
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 133
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 136
    check-cast v0, Lcom/google/android/apps/books/model/LabelMap;

    .line 138
    .local v0, "labelMap":Lcom/google/android/apps/books/model/LabelMap;
    iget-object v3, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 139
    goto :goto_0

    .line 141
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    iget-object v4, v0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 142
    goto :goto_0
.end method

.method public getCharacterOffsetAt(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    aget v0, v0, p1

    return v0
.end method

.method public getCharacterOffsetForLabel(Ljava/lang/String;)I
    .locals 3
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/LabelMap;->getNumberOfLabels()I

    move-result v1

    .line 104
    .local v1, "labelsCount":I
    const/4 v0, 0x0

    .local v0, "ii":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 105
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/LabelMap;->getLabelAt(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/LabelMap;->getCharacterOffsetAt(I)I

    move-result v2

    .line 109
    :goto_1
    return v2

    .line 104
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public getLabelAt(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getNumberOfLabels()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    array-length v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 150
    iget-object v1, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    .line 151
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 152
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LabelMap{mOffsets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/LabelMap;->mOffsets:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLabels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/LabelMap;->mLabels:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
