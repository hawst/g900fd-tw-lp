.class Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;
.super Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadMode"
.end annotation


# instance fields
.field private final mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 222
    const v0, 0x7f0f0152

    const v1, 0x7f0f0153

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(II)V

    .line 223
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 224
    return-void
.end method


# virtual methods
.method protected getValue()Z
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getDownloadMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wifiOnly"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected setValue(Z)V
    .locals 2
    .param p1, "isWifiOnly"    # Z

    .prologue
    .line 233
    if-eqz p1, :cond_0

    const-string v0, "wifiOnly"

    .line 236
    .local v0, "downloadMode":Ljava/lang/String;
    :goto_0
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;->DOWNLOAD_WIFI_ONLY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSettingChoice(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;Ljava/lang/String;)V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$DownloadMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->setDownloadMode(Ljava/lang/String;)V

    .line 239
    return-void

    .line 233
    .end local v0    # "downloadMode":Ljava/lang/String;
    :cond_0
    const-string v0, "always"

    goto :goto_0
.end method
