.class Lcom/google/android/apps/books/widget/PassageHighlightRects;
.super Ljava/lang/Object;
.source "PassageHighlightRects.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/WalkableHighlightRects;


# instance fields
.field protected final allColors:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected final colorToRects:Lcom/google/common/collect/SetMultimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/SetMultimap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field protected final colorToSelectedRects:Lcom/google/common/collect/SetMultimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/SetMultimap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field

.field protected final rects:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 1
    .param p3, "selectedAnnotationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    .local p2, "rects":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/render/LabeledRect;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToRects:Lcom/google/common/collect/SetMultimap;

    .line 31
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToSelectedRects:Lcom/google/common/collect/SetMultimap;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->allColors:Ljava/util/Set;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->rects:Ljava/util/Collection;

    .line 36
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/books/widget/PassageHighlightRects;->updateHighlightsAndSelections(Ljava/util/List;Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method private getHighlightColor(Lcom/google/android/apps/books/model/PaintableTextRange;)I
    .locals 1
    .param p1, "paintable"    # Lcom/google/android/apps/books/model/PaintableTextRange;

    .prologue
    .line 78
    invoke-interface {p1}, Lcom/google/android/apps/books/model/PaintableTextRange;->getColor()I

    move-result v0

    return v0
.end method

.method private static getPaintableWithId(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/books/model/PaintableTextRange;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/model/PaintableTextRange;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/PaintableTextRange;

    .line 70
    .local v1, "paintable":Lcom/google/android/apps/books/model/PaintableTextRange;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/PaintableTextRange;->getPaintableRangeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    .end local v1    # "paintable":Lcom/google/android/apps/books/model/PaintableTextRange;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getWalker()Lcom/google/android/apps/books/widget/Walker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/PassageHighlightRects$1;-><init>(Lcom/google/android/apps/books/widget/PassageHighlightRects;)V

    return-object v0
.end method

.method isSelected(Lcom/google/android/apps/books/model/PaintableTextRange;Ljava/lang/String;)Z
    .locals 1
    .param p1, "paintable"    # Lcom/google/android/apps/books/model/PaintableTextRange;
    .param p2, "selectedAnnotationId"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-interface {p1}, Lcom/google/android/apps/books/model/PaintableTextRange;->getPaintableRangeId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 146
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "colorToRects"

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToRects:Lcom/google/common/collect/SetMultimap;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateHighlightsAndSelections(Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p2, "selectedAnnotationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToSelectedRects:Lcom/google/common/collect/SetMultimap;

    invoke-interface {v4}, Lcom/google/common/collect/SetMultimap;->clear()V

    .line 45
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToRects:Lcom/google/common/collect/SetMultimap;

    invoke-interface {v4}, Lcom/google/common/collect/SetMultimap;->clear()V

    .line 46
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->allColors:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 48
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->rects:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/render/LabeledRect;

    .line 49
    .local v3, "paintableRect":Lcom/google/android/apps/books/render/LabeledRect;
    iget-object v4, v3, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/google/android/apps/books/widget/PassageHighlightRects;->getPaintableWithId(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/books/model/PaintableTextRange;

    move-result-object v2

    .line 51
    .local v2, "paintable":Lcom/google/android/apps/books/model/PaintableTextRange;
    if-eqz v2, :cond_0

    .line 52
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PassageHighlightRects;->getHighlightColor(Lcom/google/android/apps/books/model/PaintableTextRange;)I

    move-result v0

    .line 53
    .local v0, "highlightColor":I
    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/books/widget/PassageHighlightRects;->isSelected(Lcom/google/android/apps/books/model/PaintableTextRange;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 54
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToSelectedRects:Lcom/google/common/collect/SetMultimap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Lcom/google/common/collect/SetMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 58
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->allColors:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PassageHighlightRects;->colorToRects:Lcom/google/common/collect/SetMultimap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Lcom/google/common/collect/SetMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 61
    .end local v0    # "highlightColor":I
    .end local v2    # "paintable":Lcom/google/android/apps/books/model/PaintableTextRange;
    .end local v3    # "paintableRect":Lcom/google/android/apps/books/render/LabeledRect;
    :cond_2
    return-void
.end method
