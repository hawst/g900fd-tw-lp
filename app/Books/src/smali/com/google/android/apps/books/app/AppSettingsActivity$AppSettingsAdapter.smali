.class Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppSettingsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/books/app/AppSettingsItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/AppSettingsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/AppSettingsItem;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 204
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 205
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 209
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/AppSettingsItem;

    .line 210
    .local v0, "item":Lcom/google/android/apps/books/app/AppSettingsItem;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$AppSettingsAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/AppSettingsItem;->getView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
