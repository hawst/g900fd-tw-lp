.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;
.super Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;
.source "ReadAlongDataSourceFromStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReadableItemRetrieval"
.end annotation


# instance fields
.field private final mAltTextOffsets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mAltTexts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mOffsets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

.field private final mRequestId:I

.field private final mTextContent:Ljava/lang/StringBuilder;

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;I)V
    .locals 1
    .param p2, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    .param p3, "requestId"    # I

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    .line 190
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)V

    .line 178
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mLabels:Ljava/util/ArrayList;

    .line 180
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mOffsets:Ljava/util/ArrayList;

    .line 182
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTexts:Ljava/util/ArrayList;

    .line 184
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTextOffsets:Ljava/util/ArrayList;

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mTextContent:Ljava/lang/StringBuilder;

    .line 191
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .line 192
    iput p3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mRequestId:I

    .line 193
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTexts:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTextOffsets:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mLabels:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mOffsets:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mRequestId:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mTextContent:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method private getSearchTextProcessor()Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;
    .locals 1

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$3;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)V

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic getXmlHandler()Lcom/google/android/apps/books/model/ContentXmlHandler;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->getXmlHandler()Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;

    move-result-object v0

    return-object v0
.end method

.method protected getXmlHandler()Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
    .locals 4

    .prologue
    .line 213
    new-instance v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->getSearchTextProcessor()Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/Collection;)V

    return-object v0
.end method

.method makeLabelMap(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/model/LabelMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/apps/books/model/LabelMap;"
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "labels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "offsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    new-array v3, v5, [I

    .line 265
    .local v3, "intArray":[I
    const/4 v0, 0x0

    .line 266
    .local v0, "i":I
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 267
    .local v4, "offset":Ljava/lang/Integer;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v3, v0

    move v0, v1

    .line 268
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 269
    .end local v4    # "offset":Ljava/lang/Integer;
    :cond_0
    new-instance v6, Lcom/google/android/apps/books/model/LabelMap;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {p1, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    invoke-direct {v6, v3, v5}, Lcom/google/android/apps/books/model/LabelMap;-><init>([I[Ljava/lang/String;)V

    return-object v6
.end method

.method protected onFailedParse()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mUiExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$900(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$4;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 261
    return-void
.end method

.method protected onSuccessfulParse()V
    .locals 4

    .prologue
    .line 197
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mLabels:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mOffsets:Ljava/util/ArrayList;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->makeLabelMap(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v1

    .line 198
    .local v1, "positionMap":Lcom/google/android/apps/books/model/LabelMap;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTexts:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTextOffsets:Ljava/util/ArrayList;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->makeLabelMap(Ljava/util/List;Ljava/util/List;)Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v0

    .line 200
    .local v0, "altTextMap":Lcom/google/android/apps/books/model/LabelMap;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mUiExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$900(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$1;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 209
    return-void
.end method
