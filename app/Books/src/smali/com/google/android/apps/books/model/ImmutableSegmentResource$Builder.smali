.class public Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
.super Ljava/lang/Object;
.source "ImmutableSegmentResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/ImmutableSegmentResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCssClass:Ljava/lang/String;

.field private mResourceId:Ljava/lang/String;

.field private mSegmentId:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/model/SegmentResource;
    .locals 5

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableSegmentResource;

    iget-object v1, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mSegmentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mResourceId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mCssClass:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mTitle:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/model/ImmutableSegmentResource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public setCssClass(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .locals 0
    .param p1, "cssClass"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mCssClass:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public setResourceId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .locals 0
    .param p1, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mResourceId:Ljava/lang/String;

    .line 61
    return-object p0
.end method

.method public setSegmentId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .locals 0
    .param p1, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mSegmentId:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->mTitle:Ljava/lang/String;

    .line 71
    return-object p0
.end method
