.class public Lcom/google/android/apps/books/navigation/PageRangeLoader;
.super Ljava/lang/Object;
.source "PageRangeLoader.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;,
        Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;
    }
.end annotation


# instance fields
.field private mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private final mBasePositionIsCover:Z

.field private final mBaseSpreadPage:I

.field private final mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;"
        }
    .end annotation
.end field

.field private final mBitmapConfig:Landroid/graphics/Bitmap$Config;

.field private final mBitmapSampleSize:I

.field private final mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

.field private mCenterPageOffset:I

.field private final mEndIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

.field private mFirstPageOffset:I

.field private mFirstPlaceholderSpread:I

.field private final mGapPages:Landroid/util/SparseBooleanArray;

.field private final mHandler:Landroid/os/Handler;

.field private final mImportedPages:Landroid/util/SparseBooleanArray;

.field private mIsDestroyed:Z

.field private mIsPaused:Z

.field private mLastPageOffset:I

.field private mLastPlaceholderSpread:I

.field private mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

.field private final mPages:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/navigation/SnapshottingPage;",
            ">;"
        }
    .end annotation
.end field

.field private mPlaceholderPage:Lcom/google/android/apps/books/navigation/PageViewContent;

.field private mReachedEnd:Z

.field private mReachedStart:Z

.field private mRenderSeq:I

.field private final mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private mRequestCount:I

.field private mRequestPagesPending:Z

.field private mRequestProximity:I

.field private final mRequestedPages:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;",
            ">;"
        }
    .end annotation
.end field

.field private mSnapshotRequested:Z

.field private final mStartIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

.field private final mTempSpreadContents:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final mTmpFullSize:Landroid/graphics/Point;

.field private final mTmpThumbnailSize:Landroid/graphics/Point;

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/SpreadIdentifier;Landroid/graphics/Bitmap$Config;I)V
    .locals 5
    .param p2, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p5, "startPosition"    # Lcom/google/android/apps/books/common/Position;
    .param p6, "endPosition"    # Lcom/google/android/apps/books/common/Position;
    .param p7, "basePosition"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p8, "bitmapConfig"    # Landroid/graphics/Bitmap$Config;
    .param p9, "bitmapSampleSize"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            "Lcom/google/android/apps/books/render/Renderer;",
            "Lcom/google/android/apps/books/widget/BookView$Callbacks;",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Landroid/graphics/Bitmap$Config;",
            "I)V"
        }
    .end annotation

    .prologue
    .local p1, "bitmapCache":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<Lcom/google/android/apps/books/navigation/NavPageKey;>;"
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    .line 95
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadContents:Lcom/google/android/apps/books/render/SpreadItems;

    .line 97
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mHandler:Landroid/os/Handler;

    .line 103
    iput v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    .line 104
    iput v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    .line 106
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    .line 107
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    .line 109
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I

    .line 112
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestCount:I

    .line 114
    iput v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPlaceholderSpread:I

    .line 115
    iput v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPlaceholderSpread:I

    .line 120
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    .line 126
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mGapPages:Landroid/util/SparseBooleanArray;

    .line 168
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    .line 169
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    .line 442
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPlaceholderPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 713
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestPagesPending:Z

    .line 807
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mImportedPages:Landroid/util/SparseBooleanArray;

    .line 262
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 263
    iput-object p3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 264
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {p3}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v3

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 265
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 266
    iput-object p4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 267
    invoke-direct {p0, p5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->alignedStartPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mStartIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 269
    if-nez p6, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->getFirstPageAfterLastViewablePage()Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/render/PageIdentifier;->offsetBy(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mEndIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 276
    :goto_0
    if-nez p7, :cond_1

    .line 277
    iput v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPlaceholderSpread:I

    .line 278
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {v0, p5, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 283
    :goto_1
    iput-object p8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapConfig:Landroid/graphics/Bitmap$Config;

    .line 284
    iput p9, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapSampleSize:I

    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isPartialSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBaseSpreadPage:I

    .line 288
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->updatePageRange(I)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget v0, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v0, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/render/Renderer;->getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePositionIsCover:Z

    .line 293
    return-void

    .line 273
    :cond_0
    invoke-direct {p0, p6}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->alignedEndPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mEndIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    goto :goto_0

    .line 280
    :cond_1
    iput-object p7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 286
    goto :goto_2

    :cond_3
    move v1, v2

    .line 291
    goto :goto_3
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mSnapshotRequested:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/navigation/PageRangeLoader;Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->reinit(Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    return v0
.end method

.method static synthetic access$1402(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/android/apps/books/navigation/PageRangeLoader;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestPagesPending:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->doRequestPages()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseBooleanArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mImportedPages:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/navigation/PageRangeLoader;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderSeq:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/navigation/PageRangeLoader;ILcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "x3"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 40
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->addPage(ILcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/navigation/PageRangeLoader;)Landroid/util/SparseBooleanArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mGapPages:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/PageRangeLoader;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V

    return-void
.end method

.method private addPage(ILcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "info"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "isShared"    # Z
    .param p5, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 536
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 538
    invoke-virtual {p2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSnapshottingPage(ILcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/navigation/SnapshottingPage;

    move-result-object v0

    .line 539
    .local v0, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->setPainter(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 540
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->updatePageRange(I)V

    .line 542
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V

    .line 543
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V

    .line 544
    return-void
.end method

.method private alignedEndPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 242
    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 243
    .local v0, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v1

    .line 245
    .local v1, "spreadPageId":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v1, :cond_0

    iget v2, v1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .end local v0    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :goto_0
    return-object v0

    .restart local v0    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/PageIdentifier;->offsetBy(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    goto :goto_0
.end method

.method private alignedStartPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 5
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 221
    const/4 v3, 0x0

    invoke-static {p1, v3}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 222
    .local v0, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v2

    .line 224
    .local v2, "spreadPageId":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v2, :cond_0

    iget v3, v2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 226
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/render/PageIdentifier;->offsetBy(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 227
    .local v1, "priorPageId":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3, v1}, Lcom/google/android/apps/books/render/Renderer;->pageExists(Lcom/google/android/apps/books/render/PageIdentifier;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 231
    .end local v1    # "priorPageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method private doRequestPages()V
    .locals 5

    .prologue
    .line 748
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z

    move-result v2

    if-nez v2, :cond_3

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 757
    .local v1, "pos":I
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;-><init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;I)V

    .line 759
    .local v0, "consumer":Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 761
    const-string v2, "PageRangeLoader"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 762
    const-string v2, "PageRangeLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Request page "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getRenderPosition(I)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v4

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;)V

    .line 752
    .end local v0    # "consumer":Lcom/google/android/apps/books/navigation/PageRangeLoader$MyResponseConsumer;
    .end local v1    # "pos":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestCount:I

    if-ge v2, v3, :cond_0

    .line 753
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->findNeededPage()I

    move-result v1

    .line 754
    .restart local v1    # "pos":I
    const v2, 0x7fffffff

    if-ne v1, v2, :cond_1

    goto :goto_0
.end method

.method private findNeededPage()I
    .locals 10

    .prologue
    .line 659
    iget-boolean v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    if-eqz v8, :cond_0

    iget v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    .line 660
    .local v5, "minPage":I
    :goto_0
    iget-boolean v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    if-eqz v8, :cond_1

    iget v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    .line 662
    .local v4, "maxPage":I
    :goto_1
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    iget v9, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I

    sub-int/2addr v8, v9

    invoke-static {v5, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 663
    .local v7, "startPosition":I
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    iget v9, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I

    add-int/2addr v8, v9

    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 664
    .local v2, "endPosition":I
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    sub-int v8, v2, v8

    iget v9, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    sub-int/2addr v9, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 667
    .local v3, "maxOff":I
    const/4 v6, 0x0

    .local v6, "off":I
    :goto_2
    if-gt v6, v3, :cond_4

    .line 668
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    sub-int v1, v8, v6

    .line 669
    .local v1, "beforeCenter":I
    if-lt v1, v7, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isPageNeeded(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 679
    .end local v1    # "beforeCenter":I
    :goto_3
    return v1

    .line 659
    .end local v2    # "endPosition":I
    .end local v3    # "maxOff":I
    .end local v4    # "maxPage":I
    .end local v5    # "minPage":I
    .end local v6    # "off":I
    .end local v7    # "startPosition":I
    :cond_0
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    add-int/lit8 v5, v8, -0x1

    goto :goto_0

    .line 660
    .restart local v5    # "minPage":I
    :cond_1
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    add-int/lit8 v4, v8, 0x1

    goto :goto_1

    .line 673
    .restart local v1    # "beforeCenter":I
    .restart local v2    # "endPosition":I
    .restart local v3    # "maxOff":I
    .restart local v4    # "maxPage":I
    .restart local v6    # "off":I
    .restart local v7    # "startPosition":I
    :cond_2
    iget v8, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    add-int v0, v8, v6

    .line 674
    .local v0, "afterCenter":I
    if-eqz v6, :cond_3

    if-gt v0, v2, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isPageNeeded(I)Z

    move-result v8

    if-eqz v8, :cond_3

    move v1, v0

    .line 675
    goto :goto_3

    .line 667
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 679
    .end local v0    # "afterCenter":I
    .end local v1    # "beforeCenter":I
    :cond_4
    const v1, 0x7fffffff

    goto :goto_3
.end method

.method private findNeededSnapshot()I
    .locals 8

    .prologue
    .line 300
    const v1, 0x7fffffff

    .line 301
    .local v1, "bestOffset":I
    const v0, 0x7fffffff

    .line 303
    .local v0, "bestDistance":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 304
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 305
    .local v5, "off":I
    iget v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    sub-int v7, v5, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 306
    .local v2, "distance":I
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    .line 307
    .local v6, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    iget v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestProximity:I

    if-le v2, v7, :cond_1

    .line 308
    invoke-virtual {v6}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->isInUse()Z

    move-result v7

    if-nez v7, :cond_0

    .line 309
    invoke-virtual {v6}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->destroy()V

    .line 310
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    add-int/lit8 v4, v3, -0x1

    .end local v3    # "i":I
    .local v4, "i":I
    invoke-virtual {v7, v3}, Landroid/util/SparseArray;->removeAt(I)V

    move v3, v4

    .line 303
    .end local v4    # "i":I
    .restart local v3    # "i":I
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 312
    :cond_1
    if-ge v2, v0, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->needsSnapshot()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 313
    move v1, v5

    .line 314
    move v0, v2

    goto :goto_1

    .line 317
    .end local v2    # "distance":I
    .end local v5    # "off":I
    .end local v6    # "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    :cond_2
    return v1
.end method

.method private getLastPagePosition(I)I
    .locals 2
    .param p1, "spreadPosition"    # I

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    .end local p1    # "spreadPosition":I
    :goto_0
    return p1

    .restart local p1    # "spreadPosition":I
    :cond_0
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBaseSpreadPage:I

    sub-int p1, v0, v1

    goto :goto_0
.end method

.method private getPageDistance(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 1
    .param p1, "page1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "page2"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 783
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 784
    :cond_0
    const v0, 0x7fffffff

    .line 786
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/render/Renderer;->getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    goto :goto_0
.end method

.method private getPageHandle(I)Lcom/google/android/apps/books/render/PageHandle;
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 790
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 791
    .local v0, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/books/render/EmptyPageHandle;->INSTANCE:Lcom/google/android/apps/books/render/EmptyPageHandle;

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v1

    goto :goto_0
.end method

.method private getPageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 8
    .param p1, "pageOffset"    # I

    .prologue
    const/4 v4, 0x0

    .line 770
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPosition(I)I

    move-result v3

    .line 771
    .local v3, "spreadOffset":I
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v5, v6, v7}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 773
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_0

    .line 774
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/PageHandle;

    .line 775
    .local v0, "handle":Lcom/google/android/apps/books/render/PageHandle;
    if-nez v0, :cond_1

    move-object v2, v4

    .line 776
    .local v2, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :goto_1
    if-eqz v2, :cond_2

    .line 777
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPageIndex(I)I

    move-result v4

    sub-int/2addr v4, v1

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/render/PageIdentifier;->offsetBy(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    .line 779
    .end local v0    # "handle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v2    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_0
    return-object v4

    .line 775
    .restart local v0    # "handle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/books/render/PageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v2

    goto :goto_1

    .line 773
    .restart local v2    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private getPagePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 4
    .param p1, "pageId"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const v2, 0x7fffffff

    .line 795
    iget v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 796
    .local v0, "currentPageId":Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPageDistance(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v1

    .line 797
    .local v1, "distance":I
    if-ne v1, v2, :cond_0

    :goto_0
    return v2

    :cond_0
    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    add-int/2addr v2, v1

    goto :goto_0
.end method

.method private getPlaceholderPage()Lcom/google/android/apps/books/navigation/PageViewContent;
    .locals 12

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPlaceholderPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPlaceholderPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 478
    :goto_0
    return-object v0

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mStartIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v10

    .line 455
    .local v10, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0, v10}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v9

    .line 457
    .local v9, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v9}, Lcom/google/android/apps/books/render/PageHandle;->getWidth()I

    move-result v11

    .local v11, "width":I
    invoke-interface {v9}, Lcom/google/android/apps/books/render/PageHandle;->getHeight()I

    move-result v8

    .line 460
    .local v8, "height":I
    new-instance v2, Lcom/google/android/apps/books/navigation/PageRangeLoader$2;

    invoke-direct {v2, p0, v11, v8}, Lcom/google/android/apps/books/navigation/PageRangeLoader$2;-><init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;II)V

    .line 472
    .local v2, "sizeOnlyHandle":Lcom/google/android/apps/books/render/PageHandle;
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;->getPageSizes(Lcom/google/android/apps/books/render/PageHandle;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 474
    new-instance v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    sget-object v7, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/navigation/SnapshottingPage;-><init>(Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/render/PageHandle;IIIILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPlaceholderPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPlaceholderPage:Lcom/google/android/apps/books/navigation/PageViewContent;

    goto :goto_0
.end method

.method private getRenderPosition(I)Lcom/google/android/apps/books/render/RenderPosition;
    .locals 7
    .param p1, "pageOffset"    # I

    .prologue
    .line 740
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPosition(I)I

    move-result v1

    .line 741
    .local v1, "spreadOffset":I
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 742
    .local v0, "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    new-instance v2, Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPageIndex(I)I

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/books/render/SpreadPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;I)V

    .line 744
    .local v2, "spreadPageId":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    new-instance v3, Lcom/google/android/apps/books/render/RenderPosition;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapConfig:Landroid/graphics/Bitmap$Config;

    iget v6, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapSampleSize:I

    invoke-direct {v3, v4, v2, v5, v6}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V

    return-object v3
.end method

.method private getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .locals 2
    .param p1, "pageOffset"    # I

    .prologue
    .line 700
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v1}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v1

    if-nez v1, :cond_0

    .line 701
    sget-object v1, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 710
    :goto_0
    return-object v1

    .line 704
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPageIndex(I)I

    move-result v0

    .line 706
    .local v0, "offset":I
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 707
    rsub-int/lit8 v0, v0, 0x1

    .line 710
    :cond_1
    if-nez v0, :cond_2

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0
.end method

.method private getSnapshottingPage(I)Lcom/google/android/apps/books/navigation/SnapshottingPage;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    return-object v0
.end method

.method private getSnapshottingPage(ILcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/navigation/SnapshottingPage;
    .locals 8
    .param p1, "offset"    # I
    .param p2, "pageId"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSnapshottingPage(I)Lcom/google/android/apps/books/navigation/SnapshottingPage;

    move-result-object v0

    .line 177
    .local v0, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    if-nez v0, :cond_0

    .line 178
    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPageHandle(I)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v2

    .line 181
    .local v2, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;->getPageSizes(Lcom/google/android/apps/books/render/PageHandle;Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v1

    if-nez v1, :cond_2

    .line 185
    sget-object v7, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 192
    .local v7, "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_1
    new-instance v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    .end local v0    # "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpFullSize:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTmpThumbnailSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/navigation/SnapshottingPage;-><init>(Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/render/PageHandle;IIIILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 195
    .restart local v0    # "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 197
    .end local v2    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v7    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_0
    return-object v0

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v1, p2}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v2

    goto :goto_0

    .line 186
    .restart local v2    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    if-ne v1, v3, :cond_3

    .line 187
    sget-object v7, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .restart local v7    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    goto :goto_1

    .line 189
    .end local v7    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_3
    sget-object v7, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .restart local v7    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    goto :goto_1
.end method

.method private getSpreadPageIndex(I)I
    .locals 1
    .param p1, "pageOffset"    # I

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v0

    if-nez v0, :cond_0

    .line 689
    const/4 v0, 0x0

    .line 691
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBaseSpreadPage:I

    add-int/2addr v0, p1

    and-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getSpreadPosition(I)I
    .locals 2
    .param p1, "pagePosition"    # I

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    .end local p1    # "pagePosition":I
    :goto_0
    return p1

    .restart local p1    # "pagePosition":I
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBaseSpreadPage:I

    add-int/2addr v0, p1

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/MathUtils;->divideRoundingDown(II)I

    move-result p1

    goto :goto_0
.end method

.method private isActive()Z
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mIsPaused:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPageNeeded(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 648
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    .line 650
    .local v0, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->needsPainter()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mImportedPages:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mGapPages:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isPlaceholderSpread(I)Z
    .locals 1
    .param p1, "spreadOffset"    # I

    .prologue
    .line 490
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstSpreadOffset()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastSpreadOffset()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reinit(Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    .locals 2
    .param p1, "basePosition"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v1, 0x0

    .line 549
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 551
    iput v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    .line 552
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    .line 553
    iget v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderSeq:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderSeq:I

    .line 555
    iput-boolean v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    .line 556
    iput-boolean v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestedPages:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 559
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mGapPages:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 562
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->updatePageRange(I)V

    .line 563
    return-void
.end method

.method private requestPages()V
    .locals 2

    .prologue
    .line 719
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestPagesPending:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 732
    :cond_0
    :goto_0
    return-void

    .line 723
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRequestPagesPending:Z

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/navigation/PageRangeLoader$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$3;-><init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private requestSnapshot()V
    .locals 7

    .prologue
    .line 332
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mSnapshotRequested:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->findNeededSnapshot()I

    move-result v0

    .line 337
    .local v0, "off":I
    const v2, 0x7fffffff

    if-eq v0, v2, :cond_0

    .line 341
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mSnapshotRequested:Z

    .line 343
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    .line 345
    .local v1, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader$1;-><init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;ILcom/google/android/apps/books/navigation/SnapshottingPage;)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getSnapshotWaitMillis()J

    move-result-wide v4

    const v6, -0x7fffffff

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->postDelayed(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;JI)V

    goto :goto_0
.end method

.method private updatePageRange(I)V
    .locals 5
    .param p1, "newOffset"    # I

    .prologue
    const/4 v4, 0x1

    const v3, 0x7fffffff

    .line 139
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    if-eqz v2, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    if-nez v2, :cond_2

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mStartIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPagePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v1

    .line 145
    .local v1, "offsetToStart":I
    if-eq v1, v3, :cond_4

    .line 146
    iput-boolean v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    .line 147
    iput v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    .line 153
    .end local v1    # "offsetToStart":I
    :cond_2
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    if-nez v2, :cond_3

    .line 154
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mEndIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPagePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    .line 155
    .local v0, "offsetToEnd":I
    if-eq v0, v3, :cond_5

    .line 156
    iput-boolean v4, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    .line 157
    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    .line 163
    .end local v0    # "offsetToEnd":I
    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    invoke-interface {v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;->spreadRangeChanged()V

    goto :goto_0

    .line 148
    .restart local v1    # "offsetToStart":I
    :cond_4
    if-eq p1, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    if-ge p1, v2, :cond_2

    .line 149
    iput p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    goto :goto_1

    .line 158
    .end local v1    # "offsetToStart":I
    .restart local v0    # "offsetToEnd":I
    :cond_5
    if-eq p1, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    if-le p1, v2, :cond_3

    .line 159
    iput p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    goto :goto_2
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 893
    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mIsDestroyed:Z

    if-eqz v2, :cond_0

    .line 903
    :goto_0
    return-void

    .line 896
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mIsDestroyed:Z

    .line 897
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->setListener(Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;)V

    .line 898
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 899
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    .line 900
    .local v1, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->destroy()V

    .line 898
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 902
    .end local v1    # "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    goto :goto_0
.end method

.method public displayTwoPages()Z
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v0

    return v0
.end method

.method public exportPages(I)V
    .locals 6
    .param p1, "centerSpreadPosition"    # I

    .prologue
    .line 848
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastPagePosition(I)I

    move-result v0

    .line 849
    .local v0, "centerPage":I
    add-int/lit8 v4, v0, -0x3

    .local v4, "pos":I
    :goto_0
    add-int/lit8 v5, v0, 0x3

    if-gt v4, v5, :cond_2

    .line 850
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSnapshottingPage(I)Lcom/google/android/apps/books/navigation/SnapshottingPage;

    move-result-object v1

    .line 852
    .local v1, "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    if-nez v1, :cond_1

    .line 849
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 856
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getPageRendering()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v3

    .line 857
    .local v3, "pageRendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->getPagePainter()Lcom/google/android/apps/books/render/PagePainter;

    move-result-object v2

    .line 859
    .local v2, "pagePainter":Lcom/google/android/apps/books/render/PagePainter;
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 860
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    invoke-interface {v5, v3, v2}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V

    goto :goto_1

    .line 863
    .end local v1    # "page":Lcom/google/android/apps/books/navigation/SnapshottingPage;
    .end local v2    # "pagePainter":Lcom/google/android/apps/books/render/PagePainter;
    .end local v3    # "pageRendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_2
    return-void
.end method

.method public getFirstDisplaySpreadOffset()I
    .locals 2

    .prologue
    .line 432
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstSpreadOffset()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstSpreadOffset()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPlaceholderSpread:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public getFirstSpreadOffset()I
    .locals 2

    .prologue
    .line 414
    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPosition(I)I

    move-result v0

    .line 415
    .local v0, "firstSpread":I
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedStart:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPosition(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 416
    add-int/lit8 v0, v0, 0x1

    .line 418
    :cond_0
    return v0
.end method

.method public getLastDisplaySpreadOffset()I
    .locals 2

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastSpreadOffset()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastSpreadOffset()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPlaceholderSpread:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public getLastSpreadOffset()I
    .locals 2

    .prologue
    .line 423
    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPosition(I)I

    move-result v0

    .line 424
    .local v0, "lastSpread":I
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mReachedEnd:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPosition(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 425
    add-int/lit8 v0, v0, -0x1

    .line 427
    :cond_0
    return v0
.end method

.method public getPage(I)Lcom/google/android/apps/books/navigation/PageViewContent;
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSnapshottingPage(ILcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/navigation/SnapshottingPage;

    move-result-object v0

    return-object v0
.end method

.method public getSpreadId(I)J
    .locals 2
    .param p1, "spreadOffset"    # I

    .prologue
    .line 486
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isPlaceholderSpread(I)Z

    move-result v0

    if-eqz v0, :cond_0

    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    mul-int/lit8 v0, p1, 0x2

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 3
    .param p1, "spreadOffset"    # I

    .prologue
    .line 735
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget v2, v2, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    add-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    return-object v0
.end method

.method public getSpreadPages(ILcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;
    .locals 4
    .param p1, "spreadPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;)",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    .local p2, "spreadPages":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/navigation/PageViewContent;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->isPlaceholderSpread(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 497
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPlaceholderPage()Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v1

    .line 498
    .local v1, "placeholderPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mBasePositionIsCover:Z

    if-eqz v2, :cond_1

    .line 499
    :cond_0
    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V

    .line 519
    .end local v1    # "placeholderPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    :goto_0
    return-object p2

    .line 501
    .restart local v1    # "placeholderPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    :cond_1
    invoke-virtual {p2, v1, v1}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 506
    .end local v1    # "placeholderPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->displayTwoPages()Z

    move-result v2

    if-nez v2, :cond_3

    .line 507
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPage(I)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V

    goto :goto_0

    .line 509
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastPagePosition(I)I

    move-result v0

    .line 510
    .local v0, "lastPage":I
    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    if-le v0, v2, :cond_4

    .line 511
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPage(I)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V

    goto :goto_0

    .line 512
    :cond_4
    add-int/lit8 v2, v0, -0x1

    iget v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    if-ge v2, v3, :cond_5

    .line 513
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPage(I)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;)V

    goto :goto_0

    .line 515
    :cond_5
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPage(I)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPage(I)Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, Lcom/google/android/apps/books/render/SpreadItems;->setItems(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 10
    .param p1, "pageRendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    const/4 v4, 0x1

    .line 817
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    .line 818
    .local v6, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getPagePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v1

    .line 820
    .local v1, "position":I
    const v0, 0x7fffffff

    if-ne v1, v0, :cond_0

    .line 842
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    .line 824
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->addPage(ILcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mImportedPages:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 827
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/books/navigation/PageRangeLoader$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader$4;-><init>(Lcom/google/android/apps/books/navigation/PageRangeLoader;)V

    const-wide/16 v8, 0xfa0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v8, v9, v3}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->postDelayed(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;JI)V

    .line 841
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mImportedPages:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v1, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    goto :goto_0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 907
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mIsDestroyed:Z

    return v0
.end method

.method public isPartialSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Z
    .locals 4
    .param p1, "spreadId"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v0, p1, v3}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public onBookmarksChanged()V
    .locals 6

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastSpreadOffset()I

    move-result v0

    .line 867
    .local v0, "lastSpreadOffset":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getFirstSpreadOffset()I

    move-result v4

    .line 868
    .local v4, "spreadOffset":I
    :goto_0
    if-gt v4, v0, :cond_2

    .line 869
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadContents:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadPages(ILcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 870
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadContents:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v2

    .line 871
    .local v2, "pageCount":I
    const/4 v3, 0x0

    .local v3, "pageIndex":I
    :goto_1
    if-ge v3, v2, :cond_1

    .line 872
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mTempSpreadContents:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 873
    .local v1, "page":Lcom/google/android/apps/books/navigation/PageViewContent;
    if-eqz v1, :cond_0

    .line 874
    invoke-interface {v1}, Lcom/google/android/apps/books/navigation/PageViewContent;->onBookmarksChanged()V

    .line 871
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 868
    .end local v1    # "page":Lcom/google/android/apps/books/navigation/PageViewContent;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 878
    .end local v2    # "pageCount":I
    .end local v3    # "pageIndex":I
    :cond_2
    return-void
.end method

.method public onDecorationsChanged()V
    .locals 2

    .prologue
    .line 885
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 886
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mPages:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/navigation/SnapshottingPage;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/SnapshottingPage;->onDecorationsChanged()V

    .line 885
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 888
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V

    .line 889
    return-void
.end method

.method public setCenterSpreadOffset(I)V
    .locals 4
    .param p1, "centerSpread"    # I

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getLastPagePosition(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mFirstPageOffset:I

    iget v3, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mLastPageOffset:I

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/MathUtils;->constrain(III)I

    move-result v0

    .line 373
    .local v0, "centerPage":I
    iget v1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    if-eq v0, v1, :cond_0

    .line 374
    iput v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mCenterPageOffset:I

    .line 375
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V

    .line 376
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V

    .line 378
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    if-eq p1, v0, :cond_0

    .line 528
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mListener:Lcom/google/android/apps/books/navigation/PageRangeLoader$Listener;

    .line 529
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V

    .line 530
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V

    .line 532
    :cond_0
    return-void
.end method

.method public setPaused(Z)V
    .locals 0
    .param p1, "isPaused"    # Z

    .prologue
    .line 802
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/PageRangeLoader;->mIsPaused:Z

    .line 803
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestPages()V

    .line 804
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->requestSnapshot()V

    .line 805
    return-void
.end method
