.class public Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
.super Ljava/lang/Object;
.source "BookmarkAnimator.java"


# static fields
.field private static final FORWARD_INTERPOLATOR:Landroid/animation/TimeInterpolator;

.field private static final REVERSE_INTERPOLATOR:Landroid/animation/TimeInterpolator;


# instance fields
.field private mBookmarkShouldEventuallyBePresent:Z

.field private mCurrentAnimationStartFraction:F

.field private mDuration:J

.field private mEndValue:F

.field private mInterpolator:Landroid/animation/TimeInterpolator;

.field private mStartValue:F

.field private mTimestampStarted:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->FORWARD_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    .line 44
    new-instance v0, Lcom/google/android/apps/books/util/MirrorInterpolator;

    sget-object v1, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->FORWARD_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/util/MirrorInterpolator;-><init>(Landroid/animation/TimeInterpolator;)V

    sput-object v0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->REVERSE_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "hasBookmark"    # Z

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mTimestampStarted:J

    .line 72
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    .line 73
    return-void
.end method

.method private createInterpolator(J)Landroid/animation/TimeInterpolator;
    .locals 5
    .param p1, "now"    # J

    .prologue
    .line 184
    iget-boolean v3, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    if-eqz v3, :cond_1

    sget-object v1, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->FORWARD_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    .line 186
    .local v1, "result":Landroid/animation/TimeInterpolator;
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getElapsedAnimationFraction(J)F

    move-result v0

    .line 187
    .local v0, "elapsedFraction":F
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    .line 188
    new-instance v2, Lcom/google/android/apps/books/util/SubInterpolator;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/books/util/SubInterpolator;-><init>(Landroid/animation/TimeInterpolator;F)V

    .end local v1    # "result":Landroid/animation/TimeInterpolator;
    .local v2, "result":Landroid/animation/TimeInterpolator;
    move-object v1, v2

    .line 190
    .end local v2    # "result":Landroid/animation/TimeInterpolator;
    :cond_0
    return-object v1

    .line 184
    .end local v0    # "elapsedFraction":F
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->REVERSE_INTERPOLATOR:Landroid/animation/TimeInterpolator;

    goto :goto_0
.end method

.method private getElapsedAnimationFraction(J)F
    .locals 7
    .param p1, "now"    # J

    .prologue
    .line 205
    iget v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mCurrentAnimationStartFraction:F

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mCurrentAnimationStartFraction:F

    sub-float/2addr v1, v2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getElapsedAnimationMillis(J)J

    move-result-wide v2

    long-to-float v2, v2

    iget-wide v4, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mDuration:J

    long-to-float v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private getElapsedAnimationMillis(J)J
    .locals 3
    .param p1, "now"    # J

    .prologue
    .line 197
    iget-wide v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mTimestampStarted:J

    sub-long v0, p1, v0

    return-wide v0
.end method


# virtual methods
.method public calculateBookmarkRatioOnPage(J)F
    .locals 13
    .param p1, "now"    # J

    .prologue
    const/4 v11, 0x3

    .line 131
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->stillAnimating(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-wide v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mTimestampStarted:J

    iget-wide v2, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mDuration:J

    iget v6, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mStartValue:F

    iget v7, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mEndValue:F

    iget-object v8, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    move-wide v4, p1

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/books/util/AnimatorUtils;->getCurrentValue(JJJFFLandroid/animation/TimeInterpolator;)F

    move-result v9

    .line 134
    .local v9, "result":F
    const-string v0, "BookmarkAnimationState"

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const-string v0, "BookmarkAnimationState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Animating: fraction="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v10, v9

    .line 144
    .end local v9    # "result":F
    .local v10, "result":F
    :goto_0
    return v10

    .line 139
    .end local v10    # "result":F
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    if-eqz v0, :cond_3

    const v9, 0x3e9eb852    # 0.31f

    .line 141
    .restart local v9    # "result":F
    :goto_1
    const-string v0, "BookmarkAnimationState"

    invoke-static {v0, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    const-string v0, "BookmarkAnimationState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not animating: fraction="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v10, v9

    .line 144
    .end local v9    # "result":F
    .restart local v10    # "result":F
    goto :goto_0

    .line 139
    .end local v10    # "result":F
    :cond_3
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public createAnimator(Ljava/lang/Object;Ljava/lang/String;IJ)Landroid/animation/ObjectAnimator;
    .locals 10
    .param p1, "target"    # Ljava/lang/Object;
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "bookmarkHeight"    # I
    .param p4, "now"    # J

    .prologue
    const/4 v6, 0x0

    .line 161
    const v7, 0x3e9eb852    # 0.31f

    int-to-float v8, p3

    mul-float v5, v7, v8

    .line 162
    .local v5, "steadyStateVisibleTranslation":F
    iget-boolean v7, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    if-eqz v7, :cond_1

    move v0, v6

    .line 164
    .local v0, "currentTranslation":F
    :goto_0
    iget-boolean v7, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    if-eqz v7, :cond_2

    move v1, v5

    .line 166
    .local v1, "finalTranslation":F
    :goto_1
    const/high16 v6, 0x447a0000    # 1000.0f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {p0, p4, p5}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getElapsedAnimationFraction(J)F

    move-result v8

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-long v2, v6

    .line 168
    .local v2, "durationMillis":J
    const-string v6, "BookmarkAnimationState"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 169
    const-string v6, "BookmarkAnimationState"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Returning animator between "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_0
    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    aput v0, v6, v7

    const/4 v7, 0x1

    aput v1, v6, v7

    invoke-static {p1, p2, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 174
    .local v4, "result":Landroid/animation/ObjectAnimator;
    invoke-direct {p0, p4, p5}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->createInterpolator(J)Landroid/animation/TimeInterpolator;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 175
    invoke-virtual {v4, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 176
    return-object v4

    .end local v0    # "currentTranslation":F
    .end local v1    # "finalTranslation":F
    .end local v2    # "durationMillis":J
    .end local v4    # "result":Landroid/animation/ObjectAnimator;
    :cond_1
    move v0, v5

    .line 162
    goto :goto_0

    .restart local v0    # "currentTranslation":F
    :cond_2
    move v1, v6

    .line 164
    goto :goto_1
.end method

.method public getBookmarkWillBePresent()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    return v0
.end method

.method public isVisibleOnPage(J)Z
    .locals 1
    .param p1, "now"    # J

    .prologue
    .line 180
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->stillAnimating(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getBookmarkWillBePresent()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBookmarkInstantlyPresent(Z)V
    .locals 2
    .param p1, "hasBookmark"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    .line 118
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mTimestampStarted:J

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 120
    return-void
.end method

.method public startAnimatingBookmark(Z)V
    .locals 7
    .param p1, "isGainingBookmark"    # Z

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const v5, 0x3e9eb852    # 0.31f

    const/4 v4, 0x0

    .line 80
    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    if-ne p1, v1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 83
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mBookmarkShouldEventuallyBePresent:Z

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 86
    .local v2, "now":J
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->stillAnimating(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 87
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->getElapsedAnimationFraction(J)F

    move-result v0

    .line 92
    .local v0, "elapsedFraction":F
    sub-float v1, v6, v0

    iput v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mCurrentAnimationStartFraction:F

    .line 97
    .end local v0    # "elapsedFraction":F
    :goto_1
    if-eqz p1, :cond_3

    move v1, v4

    :goto_2
    iput v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mStartValue:F

    .line 98
    if-eqz p1, :cond_4

    :goto_3
    iput v5, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mEndValue:F

    .line 100
    const/high16 v1, 0x447a0000    # 1000.0f

    iget v4, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mCurrentAnimationStartFraction:F

    sub-float v4, v6, v4

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-long v4, v1

    iput-wide v4, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mDuration:J

    .line 102
    const-string v1, "BookmarkAnimationState"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    const-string v1, "BookmarkAnimationState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New bookmark presence="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; starting at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mCurrentAnimationStartFraction:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " progress"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_1
    iput-wide v2, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mTimestampStarted:J

    .line 109
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->createInterpolator(J)Landroid/animation/TimeInterpolator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mInterpolator:Landroid/animation/TimeInterpolator;

    goto :goto_0

    .line 94
    :cond_2
    iput v4, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mCurrentAnimationStartFraction:F

    goto :goto_1

    :cond_3
    move v1, v5

    .line 97
    goto :goto_2

    :cond_4
    move v5, v4

    .line 98
    goto :goto_3
.end method

.method public stillAnimating(J)Z
    .locals 5
    .param p1, "now"    # J

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mTimestampStarted:J

    iget-wide v2, p0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;->mDuration:J

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
