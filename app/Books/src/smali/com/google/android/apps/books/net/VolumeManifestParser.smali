.class public Lcom/google/android/apps/books/net/VolumeManifestParser;
.super Ljava/lang/Object;
.source "VolumeManifestParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/net/VolumeManifestParser$1;,
        Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;
    }
.end annotation


# direct methods
.method public static isForbidden(Ljava/lang/String;)Z
    .locals 3
    .param p0, "flagsValue"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 624
    invoke-static {p0}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 628
    :cond_0
    :goto_0
    return v1

    .line 627
    :cond_1
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 628
    .local v0, "value":I
    and-int/lit8 v2, v0, 0x10

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static parseChapter(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/Chapter;
    .locals 6
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 636
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;-><init>()V

    .line 639
    .local v0, "builder":Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 641
    .local v1, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .local v3, "type":I
    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v4

    if-le v4, v1, :cond_7

    :cond_1
    const/4 v4, 0x1

    if-eq v3, v4, :cond_7

    .line 642
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 643
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 644
    .local v2, "tag":Ljava/lang/String;
    const-string v4, "label"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 645
    const-string v4, "data"

    invoke-interface {p0, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    goto :goto_0

    .line 646
    :cond_2
    const-string v4, "segment_index"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 647
    const-string v4, "int"

    invoke-interface {p0, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setStartSegmentIndex(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    goto :goto_0

    .line 649
    :cond_3
    const-string v4, "page_index"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 650
    const-string v4, "int"

    invoke-interface {p0, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setStartPageIndex(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    goto :goto_0

    .line 652
    :cond_4
    const-string v4, "order"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 653
    const-string v4, "int"

    invoke-interface {p0, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    goto :goto_0

    .line 654
    :cond_5
    const-string v4, "depth"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 655
    const-string v4, "int"

    invoke-interface {p0, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setDepth(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    goto :goto_0

    .line 656
    :cond_6
    const-string v4, "reading_position"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 657
    const-string v4, "data"

    invoke-interface {p0, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    goto/16 :goto_0

    .line 662
    .end local v2    # "tag":Ljava/lang/String;
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->build()Lcom/google/android/apps/books/model/Chapter;

    move-result-object v4

    return-object v4
.end method

.method private static parseContentPositions(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .locals 7
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "contentStart":Ljava/lang/String;
    const/4 v0, 0x0

    .line 105
    .local v0, "contentEnd":Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    .line 107
    .local v2, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .local v4, "type":I
    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v5

    if-le v5, v2, :cond_3

    :cond_1
    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    .line 108
    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 109
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "tag":Ljava/lang/String;
    const-string v5, "content_start"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 111
    const-string v5, "data"

    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 112
    :cond_2
    const-string v5, "content_end"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 113
    const-string v5, "data"

    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 118
    .end local v3    # "tag":Ljava/lang/String;
    :cond_3
    new-instance v5, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    invoke-direct {v5, v1, v0}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5
.end method

.method private static parseFixedLayoutInfo(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;)V
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 415
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 417
    .local v0, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .local v2, "type":I
    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v3

    if-le v3, v0, :cond_2

    :cond_1
    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    .line 418
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 419
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 420
    .local v1, "tag":Ljava/lang/String;
    const-string v3, "viewport"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 421
    invoke-static {p0, p1}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseFixedLayoutViewport(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;)V

    goto :goto_0

    .line 425
    .end local v1    # "tag":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private static parseFixedLayoutViewport(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;)V
    .locals 11
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 439
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 440
    .local v0, "depth":I
    const/4 v7, 0x0

    .local v7, "widthSet":Z
    const/4 v2, 0x0

    .line 442
    .local v2, "heightSet":Z
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    .local v5, "type":I
    const/4 v8, 0x3

    if-ne v5, v8, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v8

    if-le v8, v0, :cond_7

    :cond_1
    const/4 v8, 0x1

    if-eq v5, v8, :cond_7

    .line 443
    const/4 v8, 0x2

    if-ne v5, v8, :cond_0

    .line 444
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 445
    .local v4, "tag":Ljava/lang/String;
    const-string v8, "width"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "height"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 447
    :cond_2
    const/4 v8, 0x0

    const-string v9, "data"

    invoke-interface {p0, v8, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 449
    .local v6, "value":Ljava/lang/String;
    const-string v8, "device-width"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 450
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getPortraitScreenWidth()I

    move-result v3

    .line 463
    .local v3, "pixels":I
    :cond_3
    :goto_1
    const-string v8, "width"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 464
    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutViewportWidth(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 465
    const/4 v7, 0x1

    goto :goto_0

    .line 451
    .end local v3    # "pixels":I
    :cond_4
    const-string v8, "device-height"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 452
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getPortraitScreenHeight()I

    move-result v3

    .restart local v3    # "pixels":I
    goto :goto_1

    .line 455
    .end local v3    # "pixels":I
    :cond_5
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .restart local v3    # "pixels":I
    goto :goto_1

    .line 456
    .end local v3    # "pixels":I
    :catch_0
    move-exception v1

    .line 457
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getPortraitScreenWidth()I

    move-result v3

    .line 458
    .restart local v3    # "pixels":I
    const-string v8, "VolumeManifestParser"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 459
    const-string v8, "VolumeManifestParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad viewport width/height value: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 467
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_6
    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutViewportHeight(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 468
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 475
    .end local v3    # "pixels":I
    .end local v4    # "tag":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_7
    if-nez v7, :cond_8

    .line 476
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getPortraitScreenWidth()I

    move-result v8

    invoke-virtual {p1, v8}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutViewportWidth(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 478
    :cond_8
    if-nez v2, :cond_9

    .line 479
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getPortraitScreenHeight()I

    move-result v8

    invoke-virtual {p1, v8}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutViewportHeight(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 481
    :cond_9
    return-void
.end method

.method private static parseFontInfo(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 9
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 549
    const-string v1, ""

    .local v1, "family":Ljava/lang/String;
    const-string v2, ""

    .local v2, "genericFamily":Ljava/lang/String;
    const-string v3, "normal"

    .local v3, "style":Ljava/lang/String;
    const-string v6, "normal"

    .line 552
    .local v6, "weight":Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 554
    .local v0, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    .local v5, "type":I
    const/4 v7, 0x3

    if-ne v5, v7, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-le v7, v0, :cond_5

    :cond_1
    const/4 v7, 0x1

    if-eq v5, v7, :cond_5

    .line 555
    const/4 v7, 0x2

    if-ne v5, v7, :cond_0

    .line 556
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 557
    .local v4, "tag":Ljava/lang/String;
    const-string v7, "family"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 558
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 559
    :cond_2
    const-string v7, "generic_family"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 560
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 561
    :cond_3
    const-string v7, "style"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 562
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 563
    :cond_4
    const-string v7, "weight"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 564
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 568
    .end local v4    # "tag":Ljava/lang/String;
    :cond_5
    invoke-static {v1, v2, v3, v6}, Lcom/google/android/apps/books/model/ResourceUtils;->makeFontOverlay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static parseManifest(Ljava/io/InputStream;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 23
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    const-string v20, "VolumeManifestParser"

    const/16 v21, 0x3

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    .line 131
    .local v8, "loggable":Z
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 132
    .local v3, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v15

    .line 133
    .local v15, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 136
    .local v10, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    new-instance v11, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-direct {v11, v0}, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;-><init>(Lcom/google/android/apps/books/net/VolumeManifestParser$1;)V

    .line 139
    .local v11, "parsedResources":Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v14

    .line 141
    .local v14, "segmentResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SegmentResource;>;"
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->builder()Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v2

    .line 143
    .local v2, "builder":Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    const/4 v7, 0x0

    .line 144
    .local v7, "hasTextMode":Z
    const/4 v5, 0x0

    .line 146
    .local v5, "hasImageMode":Z
    sget-object v20, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/books/util/Holder;->make(Ljava/lang/Object;)Lcom/google/android/apps/books/util/Holder;

    move-result-object v6

    .line 149
    .local v6, "hasMediaOverlays":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Boolean;>;"
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;->newPullParserLocked()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v12

    .line 150
    .local v12, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 153
    :cond_0
    :goto_0
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v18

    .local v18, "type":I
    const/16 v20, 0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 154
    const/16 v20, 0x2

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 155
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 156
    .local v16, "tag":Ljava/lang/String;
    const-string v20, "segment"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 157
    invoke-static {v12, v11, v14, v6}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseSegment(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;Ljava/util/List;Lcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/model/Segment;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 203
    .end local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v16    # "tag":Ljava/lang/String;
    .end local v18    # "type":I
    :catch_0
    move-exception v4

    .line 204
    .local v4, "e":Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v17, Ljava/net/ProtocolException;

    const-string v20, "Error parsing volume manifest"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    .line 205
    .local v17, "thrown":Ljava/net/ProtocolException;
    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/net/ProtocolException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 206
    throw v17

    .line 159
    .end local v4    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v17    # "thrown":Ljava/net/ProtocolException;
    .restart local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v16    # "tag":Ljava/lang/String;
    .restart local v18    # "type":I
    :cond_1
    :try_start_1
    const-string v20, "toc_entry"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 160
    invoke-static {v12}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseChapter(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/Chapter;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    :cond_2
    const-string v20, "page"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 162
    invoke-static {v12}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parsePage(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/Page;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_3
    const-string v20, "preferred_mode"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 164
    const/16 v20, 0x0

    const-string v21, "int"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromInteger(I)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setPreferredMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 166
    :cond_4
    const-string v20, "available_mode"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 167
    const/16 v20, 0x0

    const-string v21, "int"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 169
    .local v19, "value":I
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromInteger(I)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v9

    .line 170
    .local v9, "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v9, :cond_7

    .line 171
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v20

    sget-object v21, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_5

    const/16 v20, 0x1

    :goto_1
    or-int v7, v7, v20

    .line 172
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v20

    sget-object v21, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_6

    const/16 v20, 0x1

    :goto_2
    or-int v5, v5, v20

    goto/16 :goto_0

    .line 171
    :cond_5
    const/16 v20, 0x0

    goto :goto_1

    .line 172
    :cond_6
    const/16 v20, 0x0

    goto :goto_2

    .line 173
    :cond_7
    const-string v20, "VolumeManifestParser"

    const/16 v21, 0x5

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 174
    const-string v20, "VolumeManifestParser"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "unrecognized mode integer "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 176
    .end local v9    # "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .end local v19    # "value":I
    :cond_8
    const-string v20, "volume_version"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 177
    const/16 v20, 0x0

    const-string v21, "data"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setContentVersion(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 178
    :cond_9
    const-string v20, "first_chapter_start_segment"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 179
    const/16 v20, 0x0

    const-string v21, "int"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setFirstChapterStartSegmentIndex(I)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 181
    :cond_a
    const-string v20, "is_right_to_left"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 182
    const/16 v20, 0x0

    const-string v21, "bool"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setIsRightToLeft(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 184
    :cond_b
    const-string v20, "media_overlay_active_class"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 185
    const/16 v20, 0x0

    const-string v21, "data"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setMediaOverlayActiveClass(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 187
    :cond_c
    const-string v20, "language"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 188
    const/16 v20, 0x0

    const-string v21, "data"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v12, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 189
    :cond_d
    const-string v20, "image_mode_positions"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 190
    invoke-static {v12}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseContentPositions(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setImageModePositions(Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 191
    :cond_e
    const-string v20, "text_mode_positions"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 192
    invoke-static {v12}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseContentPositions(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setTextModePositions(Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto/16 :goto_0

    .line 193
    :cond_f
    const-string v20, "resource"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 194
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-static {v12, v0}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseResource(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v13

    .line 195
    .local v13, "resource":Lcom/google/android/apps/books/model/Resource;
    if-eqz v13, :cond_0

    invoke-interface {v13}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 196
    invoke-virtual {v11, v13}, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->addResource(Lcom/google/android/apps/books/model/Resource;)V

    goto/16 :goto_0

    .line 198
    .end local v13    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_10
    const-string v20, "meta"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 199
    invoke-static {v12, v2}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseRendition(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 209
    .end local v16    # "tag":Ljava/lang/String;
    :cond_11
    invoke-virtual {v2, v5}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setHasImageMode(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setHasTextMode(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v21

    invoke-virtual {v6}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setHasMediaOverlays(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 212
    invoke-virtual {v2, v14}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setSegmentResources(Ljava/util/Collection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 214
    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setChapters(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setSegments(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setPages(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v20

    invoke-virtual {v11}, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->getResources()Ljava/util/List;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setResources(Ljava/util/Collection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 217
    if-eqz v8, :cond_12

    .line 218
    const-string v20, "VolumeManifestParser"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "finished parsing manifest: found "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " chapters, "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " segments, "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " pages; MO="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v6}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_12
    invoke-virtual {v2}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->build()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v20

    return-object v20
.end method

.method private static parsePage(Lorg/xmlpull/v1/XmlPullParser;)Lcom/google/android/apps/books/model/Page;
    .locals 12
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v11, 0x0

    .line 577
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutablePage;->builder()Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    move-result-object v0

    .line 579
    .local v0, "builder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    const/4 v3, 0x0

    .line 582
    .local v3, "pageId":Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v1

    .line 583
    .local v1, "depth":I
    const/4 v2, 0x0

    .line 585
    .local v2, "isForbidden":Z
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .local v6, "type":I
    const/4 v10, 0x3

    if-ne v6, v10, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v10

    if-le v10, v1, :cond_6

    :cond_1
    if-eq v6, v9, :cond_6

    .line 586
    const/4 v10, 0x2

    if-ne v6, v10, :cond_0

    .line 587
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 588
    .local v5, "tag":Ljava/lang/String;
    const-string v10, "pid"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 589
    const-string v10, "data"

    invoke-interface {p0, v11, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 590
    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->pageId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    goto :goto_0

    .line 591
    :cond_2
    const-string v10, "src"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 592
    const-string v10, "data"

    invoke-interface {p0, v11, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 593
    .local v7, "url":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 594
    invoke-static {v7}, Lcom/google/android/apps/books/util/Config;->makeRelative(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 595
    .local v4, "relative":Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->remoteUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    goto :goto_0

    .line 597
    .end local v4    # "relative":Landroid/net/Uri;
    .end local v7    # "url":Ljava/lang/String;
    :cond_3
    const-string v10, "order"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 598
    const-string v10, "int"

    invoke-interface {p0, v11, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 599
    .local v8, "value":I
    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->pageOrder(I)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 600
    const-string v10, "missing pageId"

    invoke-static {v3, v10}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 601
    .end local v8    # "value":I
    :cond_4
    const-string v10, "title"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 602
    const-string v10, "data"

    invoke-interface {p0, v11, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 603
    .local v8, "value":Ljava/lang/String;
    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    goto :goto_0

    .line 604
    .end local v8    # "value":Ljava/lang/String;
    :cond_5
    const-string v10, "flags"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 605
    const-string v10, "int"

    invoke-interface {p0, v11, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 606
    .restart local v8    # "value":Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/apps/books/net/VolumeManifestParser;->isForbidden(Ljava/lang/String;)Z

    move-result v10

    or-int/2addr v2, v10

    goto/16 :goto_0

    .line 611
    .end local v5    # "tag":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/String;
    :cond_6
    const-string v10, "missing pageId"

    invoke-static {v3, v10}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 613
    if-nez v2, :cond_7

    :goto_1
    invoke-virtual {v0, v9}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->isViewable(Z)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 615
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->build()Lcom/google/android/apps/books/model/Page;

    move-result-object v9

    return-object v9

    .line 613
    :cond_7
    const/4 v9, 0x0

    goto :goto_1
.end method

.method private static parseRendition(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;)V
    .locals 7
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 236
    const/4 v1, 0x0

    .line 237
    .local v1, "metaName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 238
    .local v2, "metaValue":Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 240
    .local v0, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .local v4, "type":I
    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v5

    if-le v5, v0, :cond_3

    :cond_1
    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    .line 241
    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 242
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 243
    .local v3, "tag":Ljava/lang/String;
    const-string v5, "property"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 244
    const-string v5, "data"

    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 246
    :cond_2
    const-string v5, "cdata"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 247
    const-string v5, "data"

    invoke-interface {p0, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 251
    .end local v3    # "tag":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    .line 252
    const-string v5, "rendition:layout"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 264
    :cond_4
    :goto_1
    return-void

    .line 256
    :cond_5
    const-string v5, "rendition:orientation"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 258
    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setRenditionOrientation(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto :goto_1

    .line 259
    :cond_6
    const-string v5, "rendition:spread"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 261
    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setRenditionSpread(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    goto :goto_1
.end method

.method private static parseResource(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;)Lcom/google/android/apps/books/model/Resource;
    .locals 10
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "segmentResourceBuilder"    # Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 490
    new-instance v3, Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    invoke-direct {v3}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;-><init>()V

    .line 491
    .local v3, "result":Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    const/4 v6, 0x0

    .line 494
    .local v6, "url":Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 496
    .local v0, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    .local v5, "type":I
    const/4 v7, 0x3

    if-ne v5, v7, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v7

    if-le v7, v0, :cond_a

    :cond_1
    const/4 v7, 0x1

    if-eq v5, v7, :cond_a

    .line 497
    const/4 v7, 0x2

    if-ne v5, v7, :cond_0

    .line 498
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 499
    .local v4, "tag":Ljava/lang/String;
    const-string v7, "url"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 500
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 501
    invoke-virtual {v3, v6}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto :goto_0

    .line 502
    :cond_2
    const-string v7, "mime_type"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 503
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 504
    .local v1, "mimeType":Ljava/lang/String;
    invoke-virtual {v3, v1}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto :goto_0

    .line 505
    .end local v1    # "mimeType":Ljava/lang/String;
    :cond_3
    const-string v7, "css_class"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 506
    if-eqz p1, :cond_0

    .line 507
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setCssClass(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    goto :goto_0

    .line 510
    :cond_4
    const-string v7, "title"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 511
    if-eqz p1, :cond_0

    .line 512
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    goto :goto_0

    .line 514
    :cond_5
    const-string v7, "language"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 515
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto :goto_0

    .line 516
    :cond_6
    const-string v7, "md5_hash"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 517
    const-string v7, "data"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setMd5Hash(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto/16 :goto_0

    .line 518
    :cond_7
    const-string v7, "is_shared"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 519
    const-string v7, "bool"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setShared(Z)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto/16 :goto_0

    .line 521
    :cond_8
    const-string v7, "is_default"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 522
    const-string v7, "bool"

    invoke-interface {p0, v8, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setIsDefault(Z)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto/16 :goto_0

    .line 524
    :cond_9
    const-string v7, "font_info"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 525
    invoke-static {p0}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseFontInfo(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setOverlay(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    goto/16 :goto_0

    .line 530
    .end local v4    # "tag":Ljava/lang/String;
    :cond_a
    const-string v7, "missing url for resource"

    invoke-static {v6, v7}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    invoke-virtual {v3}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->getIsShared()Z

    move-result v8

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->getOverlay()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/books/model/ResourceUtils;->makeId(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 534
    .local v2, "resourceId":Ljava/lang/String;
    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 535
    if-eqz p1, :cond_b

    .line 536
    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setResourceId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    .line 539
    :cond_b
    invoke-virtual {v3}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->build()Lcom/google/android/apps/books/model/Resource;

    move-result-object v7

    return-object v7
.end method

.method private static parseSegment(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;Ljava/util/List;Lcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/model/Segment;
    .locals 27
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "parsedResources"    # Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlPullParser;",
            "Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;",
            "Lcom/google/android/apps/books/util/Holder",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/apps/books/model/Segment;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    .local p2, "segmentResources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SegmentResource;>;"
    .local p3, "foundSmilResource":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Boolean;>;"
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableSegment;->builder()Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    move-result-object v3

    .line 274
    .local v3, "builder":Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v14

    .line 275
    .local v14, "resources":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v17

    .line 277
    .local v17, "segmentResourceBuilders":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;>;"
    const/4 v15, 0x0

    .line 278
    .local v15, "segmentId":Ljava/lang/String;
    const/16 v18, 0x0

    .line 281
    .local v18, "startPosition":Ljava/lang/String;
    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v4

    .line 282
    .local v4, "depth":I
    const/4 v7, 0x0

    .line 283
    .local v7, "isForbidden":Z
    const/4 v11, 0x0

    .line 287
    .local v11, "relativeUri":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v20

    .local v20, "type":I
    const/16 v22, 0x3

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v22

    move/from16 v0, v22

    if-le v0, v4, :cond_d

    :cond_1
    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-eq v0, v1, :cond_d

    .line 288
    const/16 v22, 0x2

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 289
    invoke-interface/range {p0 .. p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v19

    .line 290
    .local v19, "tag":Ljava/lang/String;
    const-string v22, "label"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 291
    const/16 v22, 0x0

    const-string v23, "data"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 292
    invoke-virtual {v3, v15}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    goto :goto_0

    .line 293
    :cond_2
    const-string v22, "num_pages"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 294
    const/16 v22, 0x0

    const-string v23, "int"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setPageCount(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    goto :goto_0

    .line 296
    :cond_3
    const-string v22, "start_position"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 297
    const/16 v22, 0x0

    const-string v23, "data"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 298
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setStartPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    goto/16 :goto_0

    .line 299
    :cond_4
    const-string v22, "link"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 300
    const/16 v22, 0x0

    const-string v23, "data"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 301
    .local v10, "rawUrl":Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_0

    .line 302
    invoke-static {v10}, Lcom/google/android/apps/books/util/Config;->makeRelative(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    .line 303
    invoke-virtual {v3, v11}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    goto/16 :goto_0

    .line 305
    .end local v10    # "rawUrl":Ljava/lang/String;
    :cond_5
    const-string v22, "title"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 306
    const/16 v22, 0x0

    const-string v23, "data"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    goto/16 :goto_0

    .line 307
    :cond_6
    const-string v22, "fixed_layout_version"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 308
    const/16 v22, 0x0

    const-string v23, "int"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutVersion(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    goto/16 :goto_0

    .line 310
    :cond_7
    const-string v22, "text_properties"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 311
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseTextProperties(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;)V

    goto/16 :goto_0

    .line 312
    :cond_8
    const-string v22, "not_viewable"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 313
    const/16 v22, 0x0

    const-string v23, "bool"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 314
    .local v21, "value":Ljava/lang/String;
    const-string v22, "true"

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    or-int v7, v7, v22

    .line 315
    goto/16 :goto_0

    .end local v21    # "value":Ljava/lang/String;
    :cond_9
    const-string v22, "resource"

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 316
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->builder()Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    move-result-object v16

    .line 318
    .local v16, "segmentResourceBuilder":Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseResource(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v12

    .line 319
    .local v12, "resource":Lcom/google/android/apps/books/model/Resource;
    if-eqz v12, :cond_0

    .line 320
    invoke-interface {v12}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v9

    .line 321
    .local v9, "mimeType":Ljava/lang/String;
    const-string v22, "smil"

    move-object/from16 v0, v22

    invoke-static {v9, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    .line 322
    .local v8, "isSmilResource":Z
    const-string v22, "text/css"

    move-object/from16 v0, v22

    invoke-static {v9, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    .line 323
    .local v6, "isCss":Z
    if-eqz v8, :cond_a

    .line 324
    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Holder;->setValue(Ljava/lang/Object;)V

    .line 326
    :cond_a
    if-nez v6, :cond_b

    if-eqz v8, :cond_c

    .line 327
    :cond_b
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    :cond_c
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 347
    .end local v6    # "isCss":Z
    .end local v8    # "isSmilResource":Z
    .end local v9    # "mimeType":Ljava/lang/String;
    .end local v12    # "resource":Lcom/google/android/apps/books/model/Resource;
    .end local v16    # "segmentResourceBuilder":Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    .end local v19    # "tag":Ljava/lang/String;
    :cond_d
    if-nez v11, :cond_e

    .line 348
    const/4 v7, 0x1

    .line 349
    const-string v22, ""

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 351
    :cond_e
    if-nez v7, :cond_f

    const/16 v22, 0x1

    :goto_1
    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setViewable(Z)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 354
    if-eqz v7, :cond_10

    .line 355
    const-string v22, "VolumeManifestParser"

    const/16 v23, 0x3

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v22

    if-eqz v22, :cond_12

    .line 356
    const-string v22, "VolumeManifestParser"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Skipping resources for non-viewable segment "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ":"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_12

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/model/Resource;

    .line 358
    .local v13, "resourceInfo":Lcom/google/android/apps/books/model/Resource;
    const-string v22, "VolumeManifestParser"

    const-string v23, ">> type=%s,  url=%s"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-interface {v13}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    invoke-interface {v13}, Lcom/google/android/apps/books/model/Resource;->getUrl()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 351
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v13    # "resourceInfo":Lcom/google/android/apps/books/model/Resource;
    :cond_f
    const/16 v22, 0x0

    goto :goto_1

    .line 364
    :cond_10
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/books/model/Resource;

    .line 365
    .restart local v12    # "resource":Lcom/google/android/apps/books/model/Resource;
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/books/net/VolumeManifestParser$ParsedResources;->addResource(Lcom/google/android/apps/books/model/Resource;)V

    goto :goto_3

    .line 369
    .end local v12    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_11
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_12

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    .line 370
    .restart local v16    # "segmentResourceBuilder":Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setSegmentId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    .line 371
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->build()Lcom/google/android/apps/books/model/SegmentResource;

    move-result-object v22

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 375
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v16    # "segmentResourceBuilder":Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;
    :cond_12
    invoke-virtual {v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->build()Lcom/google/android/apps/books/model/Segment;

    move-result-object v22

    return-object v22
.end method

.method private static parseTextProperties(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;)V
    .locals 5
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 389
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 391
    .local v0, "depth":I
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .local v2, "type":I
    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v3

    if-le v3, v0, :cond_3

    :cond_1
    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    .line 392
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 393
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 394
    .local v1, "tag":Ljava/lang/String;
    const-string v3, "fixed_layout_version"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 395
    const/4 v3, 0x0

    const-string v4, "int"

    invoke-interface {p0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutVersion(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 398
    :cond_2
    const-string v3, "fixed_layout_info"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 399
    invoke-static {p0, p1}, Lcom/google/android/apps/books/net/VolumeManifestParser;->parseFixedLayoutInfo(Lorg/xmlpull/v1/XmlPullParser;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;)V

    goto :goto_0

    .line 403
    .end local v1    # "tag":Ljava/lang/String;
    :cond_3
    return-void
.end method
