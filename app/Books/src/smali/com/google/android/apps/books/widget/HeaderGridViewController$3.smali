.class Lcom/google/android/apps/books/widget/HeaderGridViewController$3;
.super Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
.source "HeaderGridViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/HeaderGridViewController;-><init>(Landroid/support/v7/widget/RecyclerView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$3;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpanSize(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$3;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$3;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mGridLayoutManager:Landroid/support/v7/widget/GridLayoutManager;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$600(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Landroid/support/v7/widget/GridLayoutManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->getSpanCount()I

    move-result v0

    goto :goto_0
.end method
