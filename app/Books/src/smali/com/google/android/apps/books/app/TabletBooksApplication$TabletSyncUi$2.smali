.class Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;
.super Ljava/lang/Object;
.source "TabletBooksApplication.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->addedVolumes(Landroid/accounts/Account;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

.field final synthetic val$addedVolumesBooksIntent:Landroid/content/Intent;

.field final synthetic val$firstVolumeId:Ljava/lang/String;

.field final synthetic val$notificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;Ljava/lang/String;Landroid/support/v4/app/NotificationCompat$Builder;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    iput-object p2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$firstVolumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$notificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    iput-object p4, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$addedVolumesBooksIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addedVolumesNotificationVisible()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    iget-object v1, v1, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    iget-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$addedVolumesBooksIntent:Landroid/content/Intent;

    const/high16 v3, 0x20000000

    invoke-static {v1, v0, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    const/4 v3, 0x3

    .line 468
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->addedVolumesNotificationVisible()Z

    move-result v1

    if-nez v1, :cond_1

    .line 469
    const-string v1, "SyncUI"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 470
    const-string v1, "SyncUI"

    const-string v2, "atl thumbnail result is too late."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 474
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 475
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # getter for: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mFirstAddedVolumeId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$500(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$firstVolumeId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 476
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 477
    .local v0, "thumbnail":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 478
    const-string v1, "SyncUI"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 479
    const-string v1, "SyncUI"

    const-string v2, "atl thumbnail is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 483
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # invokes: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getNotificationManager()Landroid/app/NotificationManager;
    invoke-static {v1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$600(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Landroid/app/NotificationManager;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$notificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 488
    .end local v0    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_3
    const-string v1, "SyncUI"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    const-string v1, "SyncUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "got atl thumbnail for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$firstVolumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but we\'re now waiting for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->this$1:Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    # getter for: Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mFirstAddedVolumeId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->access$500(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 495
    :cond_4
    const-string v1, "SyncUI"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496
    const-string v1, "SyncUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to get atl thumbnail for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->val$firstVolumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 464
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
