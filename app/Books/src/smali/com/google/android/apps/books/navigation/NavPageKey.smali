.class public Lcom/google/android/apps/books/navigation/NavPageKey;
.super Ljava/lang/Object;
.source "NavPageKey.java"


# instance fields
.field private final mFullRes:Z

.field private final mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/PageHandle;Z)V
    .locals 2
    .param p1, "pageHandle"    # Lcom/google/android/apps/books/render/PageHandle;
    .param p2, "fullRes"    # Z

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    if-nez p1, :cond_0

    move-object v0, v1

    .line 20
    .local v0, "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :goto_0
    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageIdentifier;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    .line 25
    :goto_1
    iput-boolean p2, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mFullRes:Z

    .line 26
    return-void

    .line 18
    .end local v0    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    goto :goto_0

    .line 23
    .restart local v0    # "pageId":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    if-ne p0, p1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v1

    .line 34
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 35
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 38
    check-cast v0, Lcom/google/android/apps/books/navigation/NavPageKey;

    .line 40
    .local v0, "that":Lcom/google/android/apps/books/navigation/NavPageKey;
    iget-boolean v3, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mFullRes:Z

    iget-boolean v4, v0, Lcom/google/android/apps/books/navigation/NavPageKey;->mFullRes:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 41
    goto :goto_0

    .line 43
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    iget-object v4, v0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    .line 45
    goto :goto_0

    .line 43
    :cond_5
    iget-object v3, v0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->hashCode()I

    move-result v0

    .line 55
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mFullRes:Z

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int v0, v2, v1

    .line 56
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 54
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "page"

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mPositionPageIdentifier:Lcom/google/android/apps/books/render/PositionPageIdentifier;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "fullRes"

    iget-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavPageKey;->mFullRes:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
