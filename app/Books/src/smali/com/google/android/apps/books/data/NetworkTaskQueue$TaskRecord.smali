.class Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;
.super Ljava/lang/Object;
.source "NetworkTaskQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/NetworkTaskQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TaskRecord"
.end annotation


# instance fields
.field final sequenceNumber:I

.field final task:Lcom/google/android/apps/books/data/NetworkTask;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/NetworkTask;I)V
    .locals 0
    .param p1, "task"    # Lcom/google/android/apps/books/data/NetworkTask;
    .param p2, "sequenceNumber"    # I

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->task:Lcom/google/android/apps/books/data/NetworkTask;

    .line 62
    iput p2, p0, Lcom/google/android/apps/books/data/NetworkTaskQueue$TaskRecord;->sequenceNumber:I

    .line 63
    return-void
.end method
