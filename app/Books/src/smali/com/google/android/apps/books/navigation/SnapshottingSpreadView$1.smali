.class Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;
.super Ljava/lang/Object;
.source "SnapshottingSpreadView.java"

# interfaces
.implements Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrewPage()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 270
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->access$000(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 271
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->access$100(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->drewCurrentPage()Z

    move-result v3

    if-nez v3, :cond_0

    .local v1, "rightLoading":Z
    :goto_0
    move v0, v1

    .line 276
    .local v0, "leftLoading":Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->access$200(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->setLoadingSides(ZZ)V

    .line 277
    return-void

    .end local v0    # "leftLoading":Z
    .end local v1    # "rightLoading":Z
    :cond_0
    move v1, v2

    .line 271
    goto :goto_0

    .line 273
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->access$000(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->drewCurrentPage()Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 274
    .restart local v0    # "leftLoading":Z
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;->this$0:Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    # getter for: Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->access$100(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->drewCurrentPage()Z

    move-result v3

    if-nez v3, :cond_3

    .restart local v1    # "rightLoading":Z
    :goto_3
    goto :goto_1

    .end local v0    # "leftLoading":Z
    .end local v1    # "rightLoading":Z
    :cond_2
    move v0, v2

    .line 273
    goto :goto_2

    .restart local v0    # "leftLoading":Z
    :cond_3
    move v1, v2

    .line 274
    goto :goto_3
.end method
