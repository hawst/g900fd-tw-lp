.class Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;
.super Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OrientationMode"
.end annotation


# static fields
.field private static mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mLocalPreferenceKeyToRString:Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;

    .prologue
    .line 306
    const v0, 0x7f0f0154

    invoke-static {p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->getOptions(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem;-><init>(ILjava/util/List;Lcom/google/android/apps/books/app/AppSettingsMultipleOptionsItem$AppSettingsListener;)V

    .line 307
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 308
    sget-object v0, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;

    invoke-interface {v0}, Lcom/google/common/collect/BiMap;->inverse()Lcom/google/common/collect/BiMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mLocalPreferenceKeyToRString:Lcom/google/common/collect/BiMap;

    .line 309
    return-void
.end method

.method private static getOptions(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/common/collect/HashBiMap;->create(I)Lcom/google/common/collect/HashBiMap;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;

    .line 326
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 328
    .local v1, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    const v4, 0x7f0f0155

    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 329
    .local v3, "system":Ljava/lang/CharSequence;
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    sget-object v4, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;

    const-string v5, "system"

    invoke-interface {v4, v3, v5}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    const v4, 0x7f0f0157

    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 333
    .local v2, "portrait":Ljava/lang/CharSequence;
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    sget-object v4, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;

    const-string v5, "portrait"

    invoke-interface {v4, v2, v5}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    const v4, 0x7f0f0156

    invoke-virtual {p0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 337
    .local v0, "landscape":Ljava/lang/CharSequence;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    sget-object v4, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;

    const-string v5, "landscape"

    invoke-interface {v4, v0, v5}, Lcom/google/common/collect/BiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    return-object v1
.end method


# virtual methods
.method protected getValue()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mLocalPreferenceKeyToRString:Lcom/google/common/collect/BiMap;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getRotationLockMode()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected setValue(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/CharSequence;

    .prologue
    .line 318
    sget-object v1, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mRStringToLocalPreferenceKey:Lcom/google/common/collect/BiMap;

    invoke-interface {v1, p1}, Lcom/google/common/collect/BiMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 319
    .local v0, "rotationLockMode":Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;->AUTO_ROTATE_SCREEN:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSettingChoice(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;Ljava/lang/String;)V

    .line 321
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$OrientationMode;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->setRotationLockMode(Ljava/lang/String;)V

    .line 322
    return-void
.end method
