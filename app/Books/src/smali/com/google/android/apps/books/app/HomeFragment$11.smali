.class Lcom/google/android/apps/books/app/HomeFragment$11;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$11;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 880
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;"
    if-nez p1, :cond_0

    .line 881
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$11;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setNoOffers()V

    .line 898
    :goto_0
    return-void

    .line 884
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 885
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;

    .line 886
    .local v1, "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    if-nez v1, :cond_1

    .line 887
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$11;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setNoOffers()V

    goto :goto_0

    .line 890
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$11;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    iget-boolean v4, v1, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->fromServer:Z

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setOffers(Ljava/util/List;Z)V

    goto :goto_0

    .line 892
    .end local v1    # "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v0

    .line 893
    .local v0, "e":Ljava/lang/Exception;
    if-eqz v0, :cond_3

    const-string v2, "HomeFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 894
    const-string v2, "HomeFragment"

    const-string v3, "Error loading offers"

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 896
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/app/HomeFragment$11;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->setNoOffers()V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 877
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$11;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
