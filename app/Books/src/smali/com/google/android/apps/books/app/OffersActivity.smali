.class public Lcom/google/android/apps/books/app/OffersActivity;
.super Lcom/google/android/apps/books/app/BaseBooksActivity;
.source "OffersActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/OffersActivity$1;,
        Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;,
        Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/BaseBooksActivity",
        "<",
        "Lcom/google/android/apps/books/app/OffersFragment$Callbacks;",
        ">;"
    }
.end annotation


# static fields
.field private static final sStubOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;


# instance fields
.field private mAddedFragments:Z

.field private mIsFromWidget:Z

.field private final mOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/OffersActivity$StubOffersCallbacks;-><init>(Lcom/google/android/apps/books/app/OffersActivity$1;)V

    sput-object v0, Lcom/google/android/apps/books/app/OffersActivity;->sStubOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;-><init>()V

    .line 132
    new-instance v0, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/OffersActivity$OffersCallbacks;-><init>(Lcom/google/android/apps/books/app/OffersActivity;Lcom/google/android/apps/books/app/OffersActivity$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersActivity;->mOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/OffersActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersActivity;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/OffersActivity;->mIsFromWidget:Z

    return v0
.end method

.method private addFragments()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    .line 240
    .local v6, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 242
    .local v4, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-string v0, "OffersActivity.offers"

    invoke-static {v6, v4, v0}, Lcom/google/android/apps/books/app/OffersActivity;->removeFragment(Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)V

    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->buildFrom(Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v3

    .line 245
    .local v3, "args":Landroid/os/Bundle;
    const v0, 0x7f0e00a9

    const-class v1, Lcom/google/android/apps/books/app/OffersFragment;

    const-string v2, "OffersActivity.offers"

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/OffersActivity;->createAndAddFragment(ILjava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 247
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 249
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/OffersActivity;->mAddedFragments:Z

    .line 250
    return-void
.end method

.method public static getOffersCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    instance-of v0, p0, Lcom/google/android/apps/books/app/OffersActivity;

    if-eqz v0, :cond_0

    .line 144
    check-cast p0, Lcom/google/android/apps/books/app/OffersActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    .line 146
    .restart local p0    # "context":Landroid/content/Context;
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/app/OffersActivity;->sStubOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    goto :goto_0
.end method

.method public static getOffersCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 139
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/OffersActivity;->getOffersCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private static removeFragment(Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)V
    .locals 1
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "ft"    # Landroid/support/v4/app/FragmentTransaction;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 232
    invoke-virtual {p0, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 233
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 236
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method public getFragmentCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->isActivityDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    sget-object v0, Lcom/google/android/apps/books/app/OffersActivity;->sStubOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    .line 155
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersActivity;->mOffersCallbacks:Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    goto :goto_0
.end method

.method protected getThemeId(Z)I
    .locals 1
    .param p1, "dark"    # Z

    .prologue
    .line 206
    const v0, 0x7f0a01ec

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "fromWidget"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/OffersActivity;->mIsFromWidget:Z

    .line 161
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/OffersActivity;->mIsFromWidget:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_VIEWED_FROM_WIDGET:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    :goto_0
    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 164
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onCreate(Landroid/os/Bundle;)V

    .line 166
    if-eqz p1, :cond_0

    .line 167
    const-string v2, "OffersActivity.addedFragments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/OffersActivity;->mAddedFragments:Z

    .line 170
    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 171
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040022

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 172
    .local v1, "view":Landroid/view/ViewGroup;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/OffersActivity;->setContentView(Landroid/view/View;)V

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/apps/books/util/StyleUtils;->configureFlatBlueActionBar(Landroid/content/Context;Landroid/support/v7/app/ActionBar;)V

    .line 178
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 179
    return-void

    .line 161
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "view":Landroid/view/ViewGroup;
    :cond_1
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_VIEWED_FROM_HOME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->finish()V

    .line 227
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/OffersActivity;->startActivity(Landroid/content/Intent;)V

    .line 229
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 189
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 194
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 191
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersActivity;->finish()V

    .line 192
    const/4 v0, 0x1

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 184
    const-string v0, "OffersActivity.addedFragments"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/OffersActivity;->mAddedFragments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    return-void
.end method

.method protected onSelectedAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/OffersActivity;->mAddedFragments:Z

    if-nez v0, :cond_0

    .line 200
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersActivity;->addFragments()V

    .line 202
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onStart()V

    .line 212
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStart(Landroid/app/Activity;)V

    .line 213
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 217
    invoke-super {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->onStop()V

    .line 218
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStop(Landroid/app/Activity;)V

    .line 219
    return-void
.end method
