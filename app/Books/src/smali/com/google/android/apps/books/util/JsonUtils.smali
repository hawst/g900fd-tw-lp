.class public Lcom/google/android/apps/books/util/JsonUtils;
.super Ljava/lang/Object;
.source "JsonUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    }
.end annotation


# direct methods
.method private static addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "collection":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-eqz p1, :cond_0

    .line 49
    invoke-interface {p0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_0
    return-void
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonNode;
    .locals 4
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/codehaus/jackson/JsonParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v1, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v1}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    .line 59
    .local v1, "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    invoke-virtual {v1}, Lorg/codehaus/jackson/map/ObjectMapper;->getJsonFactory()Lorg/codehaus/jackson/JsonFactory;

    move-result-object v0

    .line 60
    .local v0, "factory":Lorg/codehaus/jackson/JsonFactory;
    invoke-virtual {v0, p0}, Lorg/codehaus/jackson/JsonFactory;->createJsonParser(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    .line 61
    .local v2, "parser":Lorg/codehaus/jackson/JsonParser;
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonParser;->readValueAsTree()Lorg/codehaus/jackson/JsonNode;

    move-result-object v3

    .line 62
    .local v3, "result":Lorg/codehaus/jackson/JsonNode;
    return-object v3
.end method

.method public static parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;
    .locals 3
    .param p0, "json"    # Lorg/codehaus/jackson/JsonNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/codehaus/jackson/JsonNode;",
            "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "parser":Lcom/google/android/apps/books/util/JsonUtils$NodeParser;, "Lcom/google/android/apps/books/util/JsonUtils$NodeParser<TT;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/jackson/JsonNode;->isArray()Z

    move-result v2

    if-nez v2, :cond_2

    .line 37
    :cond_0
    const/4 v1, 0x0

    .line 44
    :cond_1
    return-object v1

    .line 39
    :cond_2
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 40
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-virtual {p0}, Lorg/codehaus/jackson/JsonNode;->getElements()Ljava/util/Iterator;

    move-result-object v0

    .line 41
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/codehaus/jackson/JsonNode;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 42
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/apps/books/util/JsonUtils$NodeParser;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JsonUtils;->addIfNotNull(Ljava/util/Collection;Ljava/lang/Object;)V

    goto :goto_0
.end method
