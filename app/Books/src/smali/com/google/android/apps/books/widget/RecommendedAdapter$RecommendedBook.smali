.class public Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
.super Ljava/lang/Object;
.source "RecommendedAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/RecommendedAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RecommendedBook"
.end annotation


# instance fields
.field public final author:Ljava/lang/String;

.field public final canonicalVolumeLink:Ljava/lang/String;

.field public final coverUri:Landroid/net/Uri;

.field public final purchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

.field public final rating:F

.field public final reason:Ljava/lang/String;

.field public final saleability:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "author"    # Ljava/lang/String;
    .param p3, "rating"    # F
    .param p4, "volumeId"    # Ljava/lang/String;
    .param p5, "coverUri"    # Ljava/lang/String;
    .param p6, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;
    .param p7, "reason"    # Ljava/lang/String;
    .param p8, "saleability"    # Ljava/lang/String;
    .param p9, "canonicalVolumeLink"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->title:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->author:Ljava/lang/String;

    .line 47
    iput p3, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->rating:F

    .line 48
    iput-object p4, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    .line 49
    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->coverUri:Landroid/net/Uri;

    .line 50
    iput-object p6, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->purchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    .line 51
    iput-object p7, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->reason:Ljava/lang/String;

    .line 52
    iput-object p8, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->saleability:Ljava/lang/String;

    .line 53
    iput-object p9, p0, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->canonicalVolumeLink:Ljava/lang/String;

    .line 54
    return-void
.end method
