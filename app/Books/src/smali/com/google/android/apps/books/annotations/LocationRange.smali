.class public abstract Lcom/google/android/apps/books/annotations/LocationRange;
.super Ljava/lang/Object;
.source "LocationRange.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mEnd:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mInclusive:Z

.field private final mStart:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0
    .param p3, "isInclusive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;Z)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    .local p1, "start":Ljava/lang/Object;, "TT;"
    .local p2, "end":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    .line 26
    iput-boolean p3, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mInclusive:Z

    .line 27
    return-void
.end method

.method private compareResultIsIntersecting(I)Z
    .locals 3
    .param p1, "compare"    # I

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    iget-boolean v2, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mInclusive:Z

    if-eqz v2, :cond_2

    if-gtz p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-ltz p1, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public containsIncludingEnd(Ljava/lang/Object;Ljava/util/Comparator;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/Comparator",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    .local p1, "location":Ljava/lang/Object;, "TT;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/LocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/LocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    if-ne p0, p1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v1

    .line 119
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 120
    goto :goto_0

    .line 122
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 123
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 125
    check-cast v0, Lcom/google/android/apps/books/annotations/LocationRange;

    .line 126
    .local v0, "other":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<*>;"
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    if-nez v3, :cond_4

    .line 127
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    if-eqz v3, :cond_5

    move v1, v2

    .line 128
    goto :goto_0

    .line 130
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 131
    goto :goto_0

    .line 133
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    if-nez v3, :cond_6

    .line 134
    iget-object v3, v0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    if-eqz v3, :cond_0

    move v1, v2

    .line 135
    goto :goto_0

    .line 137
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 138
    goto :goto_0
.end method

.method public firstContainingAnnotation(Ljava/util/Collection;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/Comparator",
            "<TT;>;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    .local p1, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    if-eqz p1, :cond_1

    .line 87
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 88
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/annotations/LocationRange;->getCorrespondingRangeFromAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/LocationRange;

    move-result-object v2

    .line 89
    .local v2, "range":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/LocationRange;->getStart()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/google/android/apps/books/annotations/LocationRange;->containsIncludingEnd(Ljava/lang/Object;Ljava/util/Comparator;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/LocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/google/android/apps/books/annotations/LocationRange;->containsIncludingEnd(Ljava/lang/Object;Ljava/util/Comparator;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 95
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "range":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public firstOverlappingAnnotation(Ljava/util/Collection;Ljava/util/Comparator;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/Comparator",
            "<TT;>;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    .local p1, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    if-eqz p1, :cond_1

    .line 68
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 69
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/annotations/LocationRange;->getCorrespondingRangeFromAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/LocationRange;

    move-result-object v2

    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/books/annotations/LocationRange;->intersects(Lcom/google/android/apps/books/annotations/LocationRange;Ljava/util/Comparator;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getCorrespondingRangeFromAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/LocationRange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ")",
            "Lcom/google/android/apps/books/annotations/LocationRange",
            "<TT;>;"
        }
    .end annotation
.end method

.method public getEnd()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    return-object v0
.end method

.method public getStart()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    const/4 v3, 0x0

    .line 107
    const/16 v0, 0x1f

    .line 108
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 109
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 110
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 111
    return v1

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 110
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public intersects(Lcom/google/android/apps/books/annotations/LocationRange;Ljava/util/Comparator;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/LocationRange",
            "<TT;>;",
            "Ljava/util/Comparator",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    .local p1, "that":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    .local p2, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<TT;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/LocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/LocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/LocationRange;->compareResultIsIntersecting(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/LocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/LocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/LocationRange;->compareResultIsIntersecting(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 100
    .local p0, "this":Lcom/google/android/apps/books/annotations/LocationRange;, "Lcom/google/android/apps/books/annotations/LocationRange<TT;>;"
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "start"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mStart:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "end"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/LocationRange;->mEnd:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
