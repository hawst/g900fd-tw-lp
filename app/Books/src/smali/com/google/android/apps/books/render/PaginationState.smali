.class public Lcom/google/android/apps/books/render/PaginationState;
.super Ljava/lang/Object;
.source "PaginationState.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ReaderDataModel;


# instance fields
.field private mFirstForbiddenPageIndices:Lcom/google/android/apps/books/render/PageIndices;

.field private final mFirstForbiddenPassageIndex:I

.field private final mPassageIndexToPassage:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/util/PassagePages;",
            ">;"
        }
    .end annotation
.end field

.field private final mPositionToIndices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/render/PageIndices;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 3
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mPassageIndexToPassage:Landroid/util/SparseArray;

    .line 30
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mPositionToIndices:Ljava/util/Map;

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/render/PaginationState;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFirstForbiddenPassageIndex()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPassageIndex:I

    .line 40
    new-instance v0, Lcom/google/android/apps/books/render/PageIndices;

    iget v1, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPassageIndex:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPageIndices:Lcom/google/android/apps/books/render/PageIndices;

    .line 41
    return-void
.end method

.method private maybeCreateBestEffortPageIdentifier(IIIZ)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 2
    .param p1, "currentPassageIndex"    # I
    .param p2, "currentPageIndex"    # I
    .param p3, "passageIndexOffset"    # I
    .param p4, "bestEffort"    # Z

    .prologue
    .line 202
    if-eqz p4, :cond_0

    .line 203
    add-int v0, p1, p3

    const/4 v1, 0x0

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mPassageIndexToPassage:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mPositionToIndices:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 285
    return-void
.end method

.method public getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I

    .prologue
    .line 275
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 276
    .local v0, "passageInfo":Lcom/google/android/apps/books/util/PassagePages;
    if-nez v0, :cond_0

    .line 277
    const/4 v1, 0x0

    .line 279
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/util/PassagePages;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v1

    goto :goto_0
.end method

.method public getFirstForbiddenPassageIndex()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPassageIndex:I

    return v0
.end method

.method public getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 50
    .local v0, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PaginationState;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/apps/books/util/PassagePages;

    .end local v0    # "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    invoke-direct {v0, p1}, Lcom/google/android/apps/books/util/PassagePages;-><init>(I)V

    .line 53
    .restart local v0    # "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    iget-object v1, p0, Lcom/google/android/apps/books/render/PaginationState;->mPassageIndexToPassage:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 56
    :cond_0
    return-object v0
.end method

.method public getPageIndices(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIndices;
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/PaginationState;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-interface {v1, p1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isPositionEnabled(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPageIndices:Lcom/google/android/apps/books/render/PageIndices;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_0
    return-object v1

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, "PaginationState"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    const-string v1, "PaginationState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to determine if position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is enabled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/PaginationState;->mPositionToIndices:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/PageIndices;

    goto :goto_0
.end method

.method public getPassageIndex(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 2
    .param p1, "position"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-static {p1}, Lcom/google/android/apps/books/common/Position;->createPositionOrNull(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/PaginationState;->getPageIndices(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 223
    .local v0, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v0, :cond_0

    .line 224
    iget v1, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 226
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mPassageIndexToPassage:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/PassagePages;

    return-object v0
.end method

.method public getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 13
    .param p1, "p1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "p2"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const v9, 0x7fffffff

    const/4 v12, 0x0

    .line 236
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v10

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 237
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v9

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v10

    sub-int v8, v9, v10

    .line 271
    :goto_0
    return v8

    .line 240
    :cond_0
    invoke-virtual {p0, p1, v12}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    .line 241
    .local v6, "normalizedLeft":Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-virtual {p0, p2, v12}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v7

    .line 242
    .local v7, "normalizedRight":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v6, :cond_1

    if-nez v7, :cond_2

    :cond_1
    move v8, v9

    .line 243
    goto :goto_0

    .line 248
    :cond_2
    const/4 v8, 0x0

    .line 249
    .local v8, "result":I
    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/Comparables;->min(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/PageIdentifier;

    .line 250
    .local v2, "earlierPage":Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/Comparables;->max(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/render/PageIdentifier;

    .line 251
    .local v4, "laterPage":Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v5

    .line 252
    .local v5, "lesserPassageIndex":I
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v3

    .line 253
    .local v3, "greaterPassageIndex":I
    move v0, v5

    .local v0, "currentPassageIndex":I
    :goto_1
    if-ge v0, v3, :cond_5

    .line 255
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v1

    .line 256
    .local v1, "currentPassagePages":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/PassagePages;->isPassageReady()Z

    move-result v10

    if-nez v10, :cond_4

    :cond_3
    move v8, v9

    .line 257
    goto :goto_0

    .line 259
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v10

    add-int/2addr v8, v10

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 261
    .end local v1    # "currentPassagePages":Lcom/google/android/apps/books/util/PassagePages;
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v9

    sub-int/2addr v8, v9

    .line 262
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v9

    add-int/2addr v8, v9

    .line 264
    if-ne v2, v6, :cond_6

    .line 265
    mul-int/lit8 v8, v8, -0x1

    .line 269
    :cond_6
    invoke-virtual {v6}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v9

    invoke-virtual {v7}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v10

    sub-int/2addr v9, v10

    add-int/2addr v8, v9

    .line 271
    goto :goto_0
.end method

.method public isForbidden(Lcom/google/android/apps/books/render/PageIndices;)Z
    .locals 1
    .param p1, "indices"    # Lcom/google/android/apps/books/render/PageIndices;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPageIndices:Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/PageIndices;->compareTo(Lcom/google/android/apps/books/render/PageIndices;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoaded(Lcom/google/android/apps/books/render/PageIndices;)Z
    .locals 4
    .param p1, "indices"    # Lcom/google/android/apps/books/render/PageIndices;

    .prologue
    const/4 v1, 0x0

    .line 211
    iget v2, p1, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 213
    .local v0, "passage":Lcom/google/android/apps/books/util/PassagePages;
    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, p1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-ltz v2, :cond_0

    iget v2, p1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->getLastReadyPageIndex()I

    move-result v3

    if-gt v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPassageReady(I)Z
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 232
    .local v0, "passage":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->isPassageReady()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 4
    .param p1, "identifier"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "bestEffort"    # Z

    .prologue
    .line 90
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0, v1, v2, p2}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIndices(IIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 99
    :goto_0
    return-object v1

    .line 94
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/PaginationState;->getPageIndices(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 95
    .local v0, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v0, :cond_1

    .line 96
    iget v1, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v2, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0, v1, v2, p2}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIndices(IIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    goto :goto_0

    .line 99
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public normalizePageIndices(IIZ)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 9
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "bestEffort"    # Z

    .prologue
    const/4 v7, 0x0

    .line 131
    move v1, p1

    .line 132
    .local v1, "currentPassageIndex":I
    move v0, p2

    .line 133
    .local v0, "currentPageIndex":I
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v5

    .line 134
    .local v5, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    iget-object v6, p0, Lcom/google/android/apps/books/render/PaginationState;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v3

    .line 137
    .local v3, "passageCount":I
    if-nez p3, :cond_0

    iget v6, p0, Lcom/google/android/apps/books/render/PaginationState;->mFirstForbiddenPassageIndex:I

    if-ne p1, v6, :cond_0

    if-ltz p2, :cond_0

    .line 138
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v2

    .line 139
    .local v2, "finalViewablePassage":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/PassagePages;->hasKnownSize()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 140
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v8, p2, 0x1

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    .line 197
    .end local v2    # "finalViewablePassage":Lcom/google/android/apps/books/util/PassagePages;
    :goto_0
    return-object v6

    .line 146
    :cond_0
    :goto_1
    if-ltz v1, :cond_4

    if-ge v1, v3, :cond_4

    .line 147
    if-gez v0, :cond_3

    .line 149
    add-int/lit8 v1, v1, -0x1

    .line 150
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v5

    .line 151
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/books/util/PassagePages;->hasKnownSize()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 152
    invoke-virtual {v5}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v6

    add-int/2addr v0, v6

    .line 153
    goto :goto_1

    .line 154
    :cond_1
    if-nez p3, :cond_2

    if-gez v1, :cond_2

    .line 155
    invoke-static {v7, v7, v0}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 157
    :cond_2
    const/4 v6, 0x1

    invoke-direct {p0, v1, v0, v6, p3}, Lcom/google/android/apps/books/render/PaginationState;->maybeCreateBestEffortPageIdentifier(IIIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 162
    :cond_3
    if-nez v5, :cond_5

    .line 193
    :cond_4
    if-eqz p3, :cond_b

    .line 195
    invoke-static {p1, p2, v7}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 166
    :cond_5
    invoke-virtual {v5}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v4

    .line 167
    .local v4, "passageNumPages":I
    if-ge v0, v4, :cond_6

    .line 169
    invoke-static {v1, v0, v7}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 172
    :cond_6
    invoke-virtual {v5}, Lcom/google/android/apps/books/util/PassagePages;->hasKnownSize()Z

    move-result v6

    if-eqz v6, :cond_a

    if-lt v0, v4, :cond_a

    .line 174
    add-int/lit8 v1, v1, 0x1

    .line 175
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/render/PaginationState;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v5

    .line 176
    if-eqz v5, :cond_7

    .line 177
    sub-int/2addr v0, v4

    goto :goto_1

    .line 178
    :cond_7
    if-nez p3, :cond_9

    if-ge v1, v3, :cond_8

    iget-object v6, p0, Lcom/google/android/apps/books/render/PaginationState;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isPassageForbidden(I)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 181
    :cond_8
    add-int/lit8 v6, v1, -0x1

    add-int/lit8 v7, v4, -0x1

    sub-int v8, v0, v4

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 184
    :cond_9
    const/4 v6, -0x1

    invoke-direct {p0, v1, v0, v6, p3}, Lcom/google/android/apps/books/render/PaginationState;->maybeCreateBestEffortPageIdentifier(IIIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 188
    :cond_a
    invoke-direct {p0, v1, v0, v7, p3}, Lcom/google/android/apps/books/render/PaginationState;->maybeCreateBestEffortPageIdentifier(IIIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    goto :goto_0

    .line 197
    .end local v4    # "passageNumPages":I
    :cond_b
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public normalizePosition(Lcom/google/android/apps/books/common/Position;IZ)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 3
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "offset"    # I
    .param p3, "bestEffort"    # Z

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PaginationState;->getPageIndices(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 105
    .local v0, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v0, :cond_0

    .line 106
    iget v1, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v2, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    add-int/2addr v2, p2

    invoke-virtual {p0, v1, v2, p3}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIndices(IIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 109
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPageIndices(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/PageIndices;)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "indices"    # Lcom/google/android/apps/books/render/PageIndices;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/render/PaginationState;->mPositionToIndices:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method
