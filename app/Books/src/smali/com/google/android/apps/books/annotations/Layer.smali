.class public Lcom/google/android/apps/books/annotations/Layer;
.super Ljava/lang/Object;
.source "Layer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;,
        Lcom/google/android/apps/books/annotations/Layer$Key;,
        Lcom/google/android/apps/books/annotations/Layer$LimitType;,
        Lcom/google/android/apps/books/annotations/Layer$Type;
    }
.end annotation


# instance fields
.field public final characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

.field public final key:Lcom/google/android/apps/books/annotations/Layer$Key;

.field public final layerVersion:Ljava/lang/String;

.field public final volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "contentVersion"    # Ljava/lang/String;
    .param p3, "layerVersion"    # Ljava/lang/String;

    .prologue
    .line 198
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->defaultFromLayerId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v1

    invoke-direct {p0, p1, v0, p3, v1}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    .line 200
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    .locals 3
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "contentVersion"    # Ljava/lang/String;
    .param p3, "layerVersion"    # Ljava/lang/String;
    .param p4, "quota"    # Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$Key;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    .line 185
    new-instance v1, Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/books/annotations/Layer;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 187
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    .line 188
    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .line 189
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/android/apps/books/annotations/Layer$Type;
    .param p3, "layerId"    # Ljava/lang/String;
    .param p4, "contentVersion"    # Ljava/lang/String;
    .param p5, "layerVersion"    # Ljava/lang/String;

    .prologue
    .line 213
    new-instance v1, Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/books/annotations/Layer$Key;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;)V

    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0, p5}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method public static fromJson(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 4
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "contentVersion"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/google/android/apps/books/annotations/Layer$Type;
    .param p3, "json"    # Lcom/google/android/apps/books/annotations/data/JsonLayer;

    .prologue
    .line 264
    iget-object v3, p3, Lcom/google/android/apps/books/annotations/data/JsonLayer;->version:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 265
    iget-object v2, p3, Lcom/google/android/apps/books/annotations/data/JsonLayer;->version:Ljava/lang/String;

    .line 271
    .local v2, "version":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v3, p3, Lcom/google/android/apps/books/annotations/data/JsonLayer;->layerId:Ljava/lang/String;

    invoke-direct {v0, p0, p2, v3}, Lcom/google/android/apps/books/annotations/Layer$Key;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Ljava/lang/String;)V

    .line 272
    .local v0, "key":Lcom/google/android/apps/books/annotations/Layer$Key;
    invoke-static {p3}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->fromJson(Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v1

    .line 273
    .local v1, "quota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    new-instance v3, Lcom/google/android/apps/books/annotations/Layer;

    invoke-direct {v3, v0, p1, v2, v1}, Lcom/google/android/apps/books/annotations/Layer;-><init>(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    return-object v3

    .line 269
    .end local v0    # "key":Lcom/google/android/apps/books/annotations/Layer$Key;
    .end local v1    # "quota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .end local v2    # "version":Ljava/lang/String;
    :cond_0
    iget-object v2, p3, Lcom/google/android/apps/books/annotations/data/JsonLayer;->updated:Ljava/lang/String;

    .restart local v2    # "version":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 291
    instance-of v2, p1, Lcom/google/android/apps/books/annotations/Layer;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 292
    check-cast v0, Lcom/google/android/apps/books/annotations/Layer;

    .line 293
    .local v0, "otherLayer":Lcom/google/android/apps/books/annotations/Layer;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 298
    .end local v0    # "otherLayer":Lcom/google/android/apps/books/annotations/Layer;
    :cond_0
    return v1
.end method

.method public getAllowedCharacterCount()I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->getAllowedCharacterCount()I

    move-result v0

    return v0
.end method

.method public getCharacterLimitType()Lcom/google/android/apps/books/annotations/Layer$LimitType;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    return-object v0
.end method

.method public getContentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getLayerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/Layer$Key;->layerId:Ljava/lang/String;

    return-object v0
.end method

.method public getRemainingCharacterCount()I
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->getRemainingCharacterCount()I

    move-result v0

    return v0
.end method

.method public getType()Lcom/google/android/apps/books/annotations/Layer$Type;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/Layer$Key;->type:Lcom/google/android/apps/books/annotations/Layer$Type;

    return-object v0
.end method

.method public getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 286
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isVolumeLayer()Z
    .locals 2

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getType()Lcom/google/android/apps/books/annotations/Layer$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$Type;->VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 278
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "key"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "contentVersion"

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "layerVersion"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "characterQuota"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
