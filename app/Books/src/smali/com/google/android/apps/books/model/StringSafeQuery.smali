.class public Lcom/google/android/apps/books/model/StringSafeQuery;
.super Ljava/lang/Object;
.source "StringSafeQuery.java"


# instance fields
.field private final mProjection:[Ljava/lang/String;

.field private final mProjectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    .line 21
    invoke-static {p1}, Lcom/google/android/apps/books/provider/database/QueryUtils;->buildColumnToIndex([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjectionMap:Ljava/util/Map;

    .line 22
    return-void
.end method

.method private safeCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 40
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeCursor;

    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjectionMap:Ljava/util/Map;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/books/model/StringSafeCursor;-><init>(Ljava/util/Map;Landroid/database/Cursor;)V

    goto :goto_0
.end method


# virtual methods
.method public augmentedWith(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeQuery;
    .locals 1
    .param p1, "added"    # Lcom/google/android/apps/books/model/StringSafeQuery;

    .prologue
    .line 48
    iget-object v0, p1, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith([Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    return-object v0
.end method

.method public varargs augmentedWith([Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeQuery;
    .locals 4
    .param p1, "added"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 52
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    array-length v1, v1

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v0, v1, [Ljava/lang/String;

    .line 53
    .local v0, "projection":[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 54
    iget-object v1, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    array-length v1, v1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    new-instance v1, Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    return-object v1
.end method

.method public query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 7
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortingOrder"    # Ljava/lang/String;

    .prologue
    .line 26
    iget-object v2, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 28
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/StringSafeQuery;->safeCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method

.method public query(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "groupBy"    # Ljava/lang/String;
    .param p6, "having"    # Ljava/lang/String;
    .param p7, "orderBy"    # Ljava/lang/String;

    .prologue
    .line 33
    iget-object v2, p0, Lcom/google/android/apps/books/model/StringSafeQuery;->mProjection:[Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 36
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/model/StringSafeQuery;->safeCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method
