.class public Lcom/google/android/apps/books/app/BitmapLruCache;
.super Landroid/support/v4/util/LruCache;
.source "BitmapLruCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/util/LruCache",
        "<TK;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "maxSize"    # I

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/apps/books/app/BitmapLruCache;, "Lcom/google/android/apps/books/app/BitmapLruCache<TK;>;"
    invoke-direct {p0, p1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    .line 14
    return-void
.end method

.method private static getByteCount(Landroid/graphics/Bitmap;)I
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected sizeOf(Ljava/lang/Object;Landroid/graphics/Bitmap;)I
    .locals 1
    .param p2, "value"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Landroid/graphics/Bitmap;",
            ")I"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/google/android/apps/books/app/BitmapLruCache;, "Lcom/google/android/apps/books/app/BitmapLruCache<TK;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    invoke-static {p2}, Lcom/google/android/apps/books/app/BitmapLruCache;->getByteCount(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 10
    .local p0, "this":Lcom/google/android/apps/books/app/BitmapLruCache;, "Lcom/google/android/apps/books/app/BitmapLruCache<TK;>;"
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/BitmapLruCache;->sizeOf(Ljava/lang/Object;Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method
