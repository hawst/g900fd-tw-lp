.class public Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
.super Ljava/lang/Object;
.source "VolumeAnnotationController.java"


# instance fields
.field private final mController:Lcom/google/android/apps/books/annotations/AnnotationController;

.field private final mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 0
    .param p1, "controller"    # Lcom/google/android/apps/books/annotations/AnnotationController;
    .param p2, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 26
    return-void
.end method


# virtual methods
.method public fetchAnnotationData(Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V
    .locals 6
    .param p1, "id"    # Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .param p2, "imageW"    # I
    .param p3, "imageH"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "II",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/AnnotationController;->fetchAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V

    .line 43
    return-void
.end method

.method public fetchCopyQuota()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationController;->fetchCopyQuota(Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    .line 52
    return-void
.end method

.method public fetchLayers(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p1, "layers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/annotations/AnnotationController;->fetchLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)V

    .line 34
    return-void
.end method

.method public fetchMapPreviewImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "cachePolicy"    # Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationController;->fetchImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V

    .line 48
    return-void
.end method

.method public getForegroundAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationController;->getForegroundAnnotationEditor(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v0

    return-object v0
.end method

.method public getRecentAnnotationsForLayer(Ljava/lang/String;I)Ljava/util/List;
    .locals 2
    .param p1, "layer"    # Ljava/lang/String;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationController;->getRecentAnnotationsForLayer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public varargs weaklyAddListeners([Lcom/google/android/apps/books/annotations/AnnotationListener;)V
    .locals 2
    .param p1, "listeners"    # [Lcom/google/android/apps/books/annotations/AnnotationListener;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mController:Lcom/google/android/apps/books/annotations/AnnotationController;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/annotations/AnnotationController;->weaklyAddListeners(Lcom/google/android/apps/books/annotations/VolumeVersion;[Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 30
    return-void
.end method
