.class public final Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;
.super Lcom/google/android/ublib/utils/WrappedIoException;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/net/HttpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OfflineIoException"
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 256
    invoke-direct {p0, p1, p2}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 257
    return-void
.end method
