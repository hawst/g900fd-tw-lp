.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeSearches(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1283
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1288
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$volumeId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getRecentVolumeSearches(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1289
    .local v1, "searches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    if-eqz v2, :cond_0

    .line 1290
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1303
    .end local v1    # "searches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 1292
    :catch_0
    move-exception v0

    .line 1293
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "BgDataController"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1294
    const-string v2, "BgDataController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception writing recent searches for volume "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    if-eqz v2, :cond_0

    .line 1299
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;->val$searchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method
