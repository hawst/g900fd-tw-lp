.class Lcom/google/android/apps/books/data/UploadsControllerImpl$6$1;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Updater;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Updater",
        "<",
        "Lcom/google/android/apps/books/upload/Upload$Uploads;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$6;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$6;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 2
    .param p1, "uploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 241
    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$6;

    iget-object v1, v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$6;->val$id:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/Upload;

    .line 242
    .local v0, "upload":Lcom/google/android/apps/books/upload/Upload;
    if-eqz v0, :cond_0

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$6;

    iget-object v1, v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$6;->val$volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/Upload;->setVolumeId(Ljava/lang/String;)V

    .line 245
    :cond_0
    return-object p1
.end method

.method public bridge synthetic update(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 238
    check-cast p1, Lcom/google/android/apps/books/upload/Upload$Uploads;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$6$1;->update(Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    return-object v0
.end method
