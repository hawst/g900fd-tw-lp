.class public Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;
.super Ljava/lang/Object;
.source "FixedPaginationPageMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenPageContent"
.end annotation


# instance fields
.field public bookPageIndex:I

.field public specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public set(I)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;
    .locals 1
    .param p1, "bookPageIndex"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->bookPageIndex:I

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 121
    return-object p0
.end method

.method public set(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;
    .locals 1
    .param p1, "specialPageDisplayType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->specialPageDisplayType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->bookPageIndex:I

    .line 116
    return-object p0
.end method
