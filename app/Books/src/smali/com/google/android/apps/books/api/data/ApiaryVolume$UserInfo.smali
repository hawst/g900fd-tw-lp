.class public Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;
.super Ljava/lang/Object;
.source "ApiaryVolume.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/data/ApiaryVolume;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserInfo"
.end annotation


# instance fields
.field public isUploaded:Z
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public readingPosition:Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public rentalPeriod:Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public rentalState:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public updated:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
