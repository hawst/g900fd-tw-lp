.class Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;
.super Ljava/lang/Object;
.source "HighlightsByColorWalkerFromList.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Lcom/google/android/apps/books/render/LabeledRect;",
        ">;"
    }
.end annotation


# instance fields
.field private mAnnotationIndex:I

.field private mCurrentAnnotationRects:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)V
    .locals 1

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    .line 75
    invoke-static {}, Lcom/google/android/apps/books/render/Walkers;->empty()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mCurrentAnnotationRects:Lcom/google/android/apps/books/widget/Walker;

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mTempRect:Landroid/graphics/Rect;

    return-void
.end method

.method private annotationIndexValid(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canUseCurrentAnnotation()Z
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentAnnotationHasRightColor()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentAnnotationHasRightSelectionStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private currentAnnotation()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mAnnotations:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$000(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    return-object v0
.end method

.method private currentAnnotationHasRightColor()Z
    .locals 2

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentColor()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private currentAnnotationHasRightSelectionStatus()Z
    .locals 2

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->isSelected(Lcom/google/android/apps/books/annotations/Annotation;)Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentlyServingSelected()Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private currentColor()I
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$300(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$400(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$400(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I
    invoke-static {v1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$300(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationColor:I
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$600(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)I

    move-result v0

    goto :goto_0
.end method

.method private currentlyServingSelected()Z
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mColorIndex:I
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$300(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mUnselectedAnnotationColors:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$400(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSelected(Lcom/google/android/apps/books/annotations/Annotation;)Z
    .locals 2
    .param p1, "currentAnnotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 131
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelectedAnnotationId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$500(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/render/LabeledRect;)Z
    .locals 3
    .param p1, "hrect"    # Lcom/google/android/apps/books/render/LabeledRect;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mCurrentAnnotationRects:Lcom/google/android/apps/books/widget/Walker;

    iget-object v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mTempRect:Landroid/graphics/Rect;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mTempRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/render/LabeledRect;->set(Landroid/graphics/Rect;Ljava/lang/String;)Lcom/google/android/apps/books/render/LabeledRect;

    .line 93
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    .line 97
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    .line 98
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mAnnotations:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$000(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->annotationIndexValid(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->canUseCurrentAnnotation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    iget v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    goto :goto_1

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mAnnotations:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$000(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->annotationIndexValid(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 104
    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$200(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Lcom/google/android/apps/books/render/PageStructureSelection;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mPageStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    invoke-static {v1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$100(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->currentAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/render/PageStructureSelection;->setTo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Lcom/google/android/apps/books/render/PageStructureLocationRange;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->this$0:Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;

    # getter for: Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->mSelection:Lcom/google/android/apps/books/render/PageStructureSelection;
    invoke-static {v0}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;->access$200(Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList;)Lcom/google/android/apps/books/render/PageStructureSelection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageStructureSelection;->textRects()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mCurrentAnnotationRects:Lcom/google/android/apps/books/widget/Walker;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mCurrentAnnotationRects:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 110
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->next(Lcom/google/android/apps/books/render/LabeledRect;)Z

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 66
    check-cast p1, Lcom/google/android/apps/books/render/LabeledRect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->next(Lcom/google/android/apps/books/render/LabeledRect;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mAnnotationIndex:I

    .line 85
    invoke-static {}, Lcom/google/android/apps/books/render/Walkers;->empty()Lcom/google/android/apps/books/widget/Walker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/HighlightsByColorWalkerFromList$1;->mCurrentAnnotationRects:Lcom/google/android/apps/books/widget/Walker;

    .line 86
    return-void
.end method
