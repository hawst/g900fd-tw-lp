.class public Lcom/google/android/apps/books/render/LineRectWalker;
.super Ljava/lang/Object;
.source "LineRectWalker.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field private mHasWordRectInWaiting:Z

.field private final mLineRect:Landroid/graphics/Rect;

.field private final mMaxDiffRatioY:F

.field private final mMaxGapRatioX:F

.field private final mWordRect:Landroid/graphics/Rect;

.field private final mWordRects:Lcom/google/android/apps/books/widget/Walker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/Walker;FF)V
    .locals 1
    .param p2, "maxGapRatioX"    # F
    .param p3, "maxDiffRatioY"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;FF)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "wordRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mHasWordRectInWaiting:Z

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRect:Landroid/graphics/Rect;

    .line 31
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mLineRect:Landroid/graphics/Rect;

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRects:Lcom/google/android/apps/books/widget/Walker;

    .line 57
    iput p2, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mMaxGapRatioX:F

    .line 58
    iput p3, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mMaxDiffRatioY:F

    .line 59
    return-void
.end method

.method private addWordToLine(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 8
    .param p1, "word"    # Landroid/graphics/Rect;
    .param p2, "line"    # Landroid/graphics/Rect;

    .prologue
    const/4 v4, 0x0

    .line 102
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v6

    add-int/2addr v5, v6

    div-int/lit8 v0, v5, 0x2

    .line 103
    .local v0, "averageHeight":I
    iget v5, p1, Landroid/graphics/Rect;->left:I

    iget v6, p2, Landroid/graphics/Rect;->right:I

    sub-int v3, v5, v6

    .line 109
    .local v3, "xDistance":I
    if-ltz v3, :cond_0

    int-to-float v5, v3

    int-to-float v6, v0

    iget v7, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mMaxGapRatioX:F

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v4

    .line 113
    :cond_1
    iget v5, p2, Landroid/graphics/Rect;->top:I

    iget v6, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 114
    .local v2, "topDiff":I
    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    iget v6, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 118
    .local v1, "bottomDiff":I
    add-int v5, v2, v1

    int-to-float v5, v5

    int-to-float v6, v0

    iget v7, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mMaxDiffRatioY:F

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_0

    .line 123
    iget v4, p1, Landroid/graphics/Rect;->right:I

    iput v4, p2, Landroid/graphics/Rect;->right:I

    .line 124
    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v5, p1, Landroid/graphics/Rect;->top:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p2, Landroid/graphics/Rect;->top:I

    .line 125
    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p2, Landroid/graphics/Rect;->bottom:I

    .line 126
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private getWordRectInWaiting()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 90
    iget-boolean v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mHasWordRectInWaiting:Z

    if-eqz v1, :cond_0

    .line 91
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mHasWordRectInWaiting:Z

    .line 92
    const/4 v0, 0x1

    .line 94
    :cond_0
    return v0
.end method


# virtual methods
.method public next(Landroid/graphics/Rect;)Z
    .locals 3
    .param p1, "item"    # Landroid/graphics/Rect;

    .prologue
    const/4 v0, 0x1

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/books/render/LineRectWalker;->getWordRectInWaiting()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRects:Lcom/google/android/apps/books/widget/Walker;

    iget-object v2, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRect:Landroid/graphics/Rect;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mLineRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRects:Lcom/google/android/apps/books/widget/Walker;

    iget-object v2, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRect:Landroid/graphics/Rect;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mLineRect:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/books/render/LineRectWalker;->addWordToLine(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 75
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mHasWordRectInWaiting:Z

    .line 76
    iget-object v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mLineRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mLineRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 11
    check-cast p1, Landroid/graphics/Rect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/LineRectWalker;->next(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mWordRects:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/LineRectWalker;->mHasWordRectInWaiting:Z

    .line 133
    return-void
.end method
