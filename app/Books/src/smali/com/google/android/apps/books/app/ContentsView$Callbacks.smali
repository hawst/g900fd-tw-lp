.class public interface abstract Lcom/google/android/apps/books/app/ContentsView$Callbacks;
.super Ljava/lang/Object;
.source "ContentsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ContentsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract navigateTo(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
.end method

.method public abstract onAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method
