.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->uiRemove(Lcom/google/android/apps/books/annotations/Annotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

.field final synthetic val$layer:Ljava/lang/String;

.field final synthetic val$localId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->val$localId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->val$layer:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->val$localId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->markForDeletion(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    new-instance v2, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 267
    return-void
.end method
