.class public Lcom/google/android/apps/books/util/EncryptionUtils;
.super Ljava/lang/Object;
.source "EncryptionUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;,
        Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;,
        Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    }
.end annotation


# static fields
.field public static final SEPARATOR:[B

.field public static final UPGRADE_SEPARATOR:[B

.field private static sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

.field private static final sRANDOM:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, ":"

    invoke-static {v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    .line 46
    const-string v0, ","

    invoke-static {v0}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/EncryptionUtils;->UPGRADE_SEPARATOR:[B

    .line 51
    new-instance v0, Lcom/google/android/apps/books/util/PlatformK_rStorage;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/PlatformK_rStorage;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    .line 55
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/EncryptionUtils;->sRANDOM:Ljava/util/Random;

    .line 56
    return-void
.end method

.method private static D_r([B)[B
    .locals 8
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x0

    .line 155
    sget-object v5, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    invoke-interface {v5}, Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;->getK_r()Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 156
    .local v0, "K_r":Ljavax/crypto/SecretKey;
    sget-object v5, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    invoke-interface {v5}, Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;->getK_rVersion()I

    move-result v1

    .line 160
    .local v1, "K_rVersion":I
    sget-object v5, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    invoke-static {p0, v6, v5}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v3

    .line 161
    .local v3, "offset":I
    invoke-static {p0, v6, v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readIntString([B)I

    move-result v4

    .line 162
    .local v4, "version":I
    sget-object v5, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    array-length v5, v5

    add-int/2addr v5, v3

    invoke-static {p0, v5}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object p0

    .line 164
    if-eq v4, v1, :cond_0

    .line 165
    new-instance v5, Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected root key \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'; expected \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 170
    :cond_0
    const/16 v5, 0xb

    invoke-static {p0, v5}, Lcom/google/android/apps/books/util/Base64;->decode([BI)[B

    move-result-object p0

    .line 171
    invoke-static {p0, v6, v7}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v2

    .line 172
    .local v2, "iv":[B
    invoke-static {p0, v7}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object p0

    .line 175
    invoke-static {v0, v2, p0}, Lcom/google/android/apps/books/util/EncryptionUtils;->decrypt(Ljavax/crypto/SecretKey;[B[B)[B

    move-result-object v5

    return-object v5
.end method

.method public static D_s(Ljava/io/InputStream;[BLjava/lang/String;)Ljava/io/InputStream;
    .locals 10
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "encryptedK_sClause"    # [B
    .param p2, "expectedK_sVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {p1}, Lcom/google/android/apps/books/util/EncryptionUtils;->getK_sVersionAndK_s([B)Landroid/util/Pair;

    move-result-object v2

    .line 211
    .local v2, "K_sVersion_and_K_s":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 212
    .local v1, "K_sVersion":Ljava/lang/String;
    iget-object v7, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/books/util/Hex;->decodeHex(Ljava/lang/String;)[B

    move-result-object v0

    .line 214
    .local v0, "K_s":[B
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unexpected session key \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' (expecting \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\')."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 218
    new-instance v6, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "AES"

    invoke-direct {v6, v0, v7}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 221
    .local v6, "sessionKey":Ljavax/crypto/SecretKey;
    const/16 v7, 0x10

    new-array v4, v7, [B

    .line 222
    .local v4, "iv":[B
    invoke-virtual {p0, v4}, Ljava/io/InputStream;->read([B)I

    .line 225
    const/4 v7, 0x4

    new-array v5, v7, [B

    .line 226
    .local v5, "lengthBuffer":[B
    invoke-virtual {p0, v5}, Ljava/io/InputStream;->read([B)I

    .line 227
    invoke-static {v5}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readIntLittle([B)I

    .line 229
    const-string v7, "AES/CBC/NoPadding"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 230
    .local v3, "cipher":Ljavax/crypto/Cipher;
    const/4 v7, 0x2

    new-instance v8, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v8, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v3, v7, v6, v8}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 232
    new-instance v7, Lcom/google/android/apps/books/util/FastCipherInputStream;

    invoke-direct {v7, p0, v3}, Lcom/google/android/apps/books/util/FastCipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    return-object v7
.end method

.method public static E_r([B)[B
    .locals 8
    .param p0, "payload"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 130
    sget-object v3, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    invoke-interface {v3}, Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;->getK_r()Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 131
    .local v0, "K_r":Ljavax/crypto/SecretKey;
    sget-object v3, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    invoke-interface {v3}, Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;->getK_rVersion()I

    move-result v1

    .line 132
    .local v1, "K_rVersion":I
    invoke-static {}, Lcom/google/android/apps/books/util/EncryptionUtils;->generateRandomIv()[B

    move-result-object v2

    .line 135
    .local v2, "iv":[B
    invoke-static {v0, v2, p0}, Lcom/google/android/apps/books/util/EncryptionUtils;->encrypt(Ljavax/crypto/SecretKey;[B[B)[B

    move-result-object p0

    .line 136
    new-array v3, v7, [[B

    aput-object v2, v3, v5

    aput-object p0, v3, v6

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->concatBuffers([[B)[B

    move-result-object p0

    .line 137
    const/16 v3, 0xb

    invoke-static {p0, v3}, Lcom/google/android/apps/books/util/Base64;->encode([BI)[B

    move-result-object p0

    .line 138
    const/4 v3, 0x3

    new-array v3, v3, [[B

    invoke-static {v1}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeIntString(I)[B

    move-result-object v4

    aput-object v4, v3, v5

    sget-object v4, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    aput-object v4, v3, v6

    aput-object p0, v3, v7

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->concatBuffers([[B)[B

    move-result-object v3

    return-object v3
.end method

.method public static createSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 314
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    const-string v6, "HmacSHA1"

    invoke-direct {v4, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 317
    .local v4, "signingKey":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_0
    const-string v5, "HmacSHA1"

    invoke-static {v5}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    .line 318
    .local v1, "mac":Ljavax/crypto/Mac;
    invoke-virtual {v1, v4}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    .line 326
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v1, v5}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v2

    .line 327
    .local v2, "rawHmac":[B
    new-instance v5, Ljava/lang/String;

    const/16 v6, 0x8

    invoke-static {v2, v6}, Lcom/google/android/apps/books/util/Base64;->encode([BI)[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 328
    .local v3, "signature":Ljava/lang/String;
    return-object v3

    .line 319
    .end local v1    # "mac":Ljavax/crypto/Mac;
    .end local v2    # "rawHmac":[B
    .end local v3    # "signature":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 321
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 323
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 324
    .local v0, "e":Ljava/security/InvalidKeyException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method private static decrypt(Ljavax/crypto/SecretKey;[B[B)[B
    .locals 6
    .param p0, "key"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "input"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 108
    invoke-static {p2, v5, v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readIntLittle([B)I

    move-result v2

    .line 109
    .local v2, "length":I
    invoke-static {p2, v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object p2

    .line 111
    const-string v3, "AES/CBC/NoPadding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 112
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v3, 0x2

    new-instance v4, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v4, p1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, v3, p0, v4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 113
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 117
    .local v1, "decrypted":[B
    array-length v3, v1

    if-gt v2, v3, :cond_0

    array-length v3, v1

    sub-int/2addr v3, v2

    const/16 v4, 0x10

    if-le v3, v4, :cond_1

    .line 118
    :cond_0
    new-instance v3, Ljava/security/GeneralSecurityException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid expected mesg length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " decrypted length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/GeneralSecurityException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 122
    :cond_1
    invoke-static {v1, v5, v2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v3

    return-object v3
.end method

.method private static encrypt(Ljavax/crypto/SecretKey;[B[B)[B
    .locals 6
    .param p0, "key"    # Ljavax/crypto/SecretKey;
    .param p1, "iv"    # [B
    .param p2, "input"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 93
    const-string v3, "AES/CBC/ZeroBytePadding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 94
    .local v0, "cipher":Ljavax/crypto/Cipher;
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v3, p1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, v5, p0, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 95
    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 98
    .local v1, "encrypted":[B
    array-length v3, p2

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeIntLittle(I)[B

    move-result-object v2

    .line 99
    .local v2, "lengthRaw":[B
    const/4 v3, 0x2

    new-array v3, v3, [[B

    const/4 v4, 0x0

    aput-object v2, v3, v4

    aput-object v1, v3, v5

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->concatBuffers([[B)[B

    move-result-object v3

    return-object v3
.end method

.method public static extractK_sMetadata([B)Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    .locals 5
    .param p0, "encryptedK_sClause"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 253
    new-instance v1, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;

    invoke-direct {v1}, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;-><init>()V

    .line 256
    .local v1, "meta":Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;
    invoke-static {p0}, Lcom/google/android/apps/books/util/EncryptionUtils;->D_r([B)[B

    move-result-object v0

    .line 260
    .local v0, "K_sClause":[B
    sget-object v3, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    invoke-static {v0, v4, v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v2

    .line 261
    .local v2, "offset":I
    invoke-static {v0, v4, v2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->nonce:[B

    .line 262
    sget-object v3, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    array-length v3, v3

    add-int/2addr v3, v2

    invoke-static {v0, v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object v0

    .line 265
    sget-object v3, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    invoke-static {v0, v4, v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v2

    .line 266
    invoke-static {v0, v4, v2}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([B)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_sVersion:Ljava/lang/String;

    .line 267
    sget-object v3, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    invoke-interface {v3}, Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;->getK_rVersion()I

    move-result v3

    iput v3, v1, Lcom/google/android/apps/books/util/EncryptionUtils$K_sMetadata;->K_rVersion:I

    .line 269
    return-object v1
.end method

.method public static extractSessionKeyInHexFormat([B)Ljava/lang/String;
    .locals 2
    .param p0, "encryptedK_sClause"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-static {p0}, Lcom/google/android/apps/books/util/EncryptionUtils;->getK_sVersionAndK_s([B)Landroid/util/Pair;

    move-result-object v0

    .line 244
    .local v0, "K_sVersion_and_K_s":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method public static generateAppInfo(Landroid/content/Context;)[B
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    .line 74
    .local v1, "config":Lcom/google/android/apps/books/util/Config;
    invoke-virtual {v1}, Lcom/google/android/apps/books/util/Config;->getSourceParam()Ljava/lang/String;

    move-result-object v3

    .line 76
    .local v3, "source":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 77
    .local v2, "resolver":Landroid/content/ContentResolver;
    const-string v4, "android_id"

    invoke-static {v2, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "androidId":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->writeString(Ljava/lang/String;)[B

    move-result-object v4

    return-object v4
.end method

.method public static generateRandomIv()[B
    .locals 2

    .prologue
    .line 83
    const/16 v1, 0x10

    new-array v0, v1, [B

    .line 84
    .local v0, "iv":[B
    invoke-static {}, Lcom/google/android/apps/books/util/EncryptionUtils;->getSecureRandom()Ljava/util/Random;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 85
    return-object v0
.end method

.method private static getK_sVersionAndK_s([B)Landroid/util/Pair;
    .locals 6
    .param p0, "encryptedK_sClause"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;,
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 181
    invoke-static {p0}, Lcom/google/android/apps/books/util/EncryptionUtils;->D_r([B)[B

    move-result-object v1

    .line 185
    .local v1, "K_sClause":[B
    sget-object v4, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    invoke-static {v1, v5, v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v3

    .line 186
    .local v3, "offset":I
    sget-object v4, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    array-length v4, v4

    add-int/2addr v4, v3

    invoke-static {v1, v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object v1

    .line 189
    sget-object v4, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    invoke-static {v1, v5, v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->bufferIndexOf([BI[B)I

    move-result v3

    .line 190
    invoke-static {v1, v5, v3}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BII)[B

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([B)Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "K_sVersion":Ljava/lang/String;
    sget-object v4, Lcom/google/android/apps/books/util/EncryptionUtils;->SEPARATOR:[B

    array-length v4, v4

    add-int/2addr v4, v3

    invoke-static {v1, v4}, Lcom/google/android/apps/books/util/ByteArrayUtils;->subBuffer([BI)[B

    move-result-object v1

    .line 194
    invoke-static {v1}, Lcom/google/android/apps/books/util/ByteArrayUtils;->readString([B)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "K_s":Ljava/lang/String;
    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method

.method private static getSecureRandom()Ljava/util/Random;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/apps/books/util/EncryptionUtils;->sRANDOM:Ljava/util/Random;

    return-object v0
.end method

.method public static setK_rStorage(Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;)V
    .locals 0
    .param p0, "value"    # Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    .prologue
    .line 60
    sput-object p0, Lcom/google/android/apps/books/util/EncryptionUtils;->sK_rStorage:Lcom/google/android/apps/books/util/EncryptionUtils$K_rStorage;

    .line 61
    return-void
.end method
