.class public interface abstract Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.super Ljava/lang/Object;
.source "ReadOnlyVolumeContentFile.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InputStreamSource;


# virtual methods
.method public abstract exists()Z
.end method

.method public abstract getLength()J
.end method

.method public abstract openParcelFileDescriptor(I)Landroid/os/ParcelFileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method
