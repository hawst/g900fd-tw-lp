.class Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "MyEbooksSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->getEbooksOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Ljava/util/List;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

.field final synthetic val$expiredKeyContinuation:Ljava/lang/Runnable;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

.field final synthetic val$volumeIdsToRenew:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/util/List;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iput-object p3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$volumeIdsToRenew:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p5, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 13
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 173
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v6

    .line 175
    .local v6, "server":Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v9

    .line 177
    .local v9, "volumesWithLicense":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 178
    .local v4, "layers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    new-instance v5, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

    invoke-direct {v5}, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;-><init>()V

    .line 181
    .local v5, "licenseResponse":Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$volumeIdsToRenew:Ljava/util/List;

    iget-object v11, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v11}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v11

    invoke-interface {v6, v10, v9, v11}, Lcom/google/android/apps/books/net/BooksServer;->syncVolumeLicenses(Ljava/util/Collection;Ljava/util/Collection;Lcom/google/android/apps/books/model/SessionKey;)Ljava/util/List;

    move-result-object v8

    .line 184
    .local v8, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    if-eqz v8, :cond_2

    .line 185
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .line 186
    .local v7, "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    new-instance v10, Lcom/google/android/apps/books/model/JsonVolumeData;

    iget-object v11, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAccount:Landroid/accounts/Account;
    invoke-static {v11}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$300(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;)Landroid/accounts/Account;

    move-result-object v11

    invoke-direct {v10, v7, v11}, Lcom/google/android/apps/books/model/JsonVolumeData;-><init>(Lcom/google/android/apps/books/api/data/ApiaryVolume;Landroid/accounts/Account;)V

    iget-object v11, v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v11, v11, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->contentVersion:Ljava/lang/String;

    invoke-virtual {v5, v10, v11}, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->addVolume(Lcom/google/android/apps/books/model/VolumeData;Ljava/lang/String;)V

    .line 189
    iget-object v10, v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;->layerInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;

    if-eqz v10, :cond_0

    iget-object v10, v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;->layerInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;

    iget-object v10, v10, Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;->layers:Ljava/util/List;

    if-eqz v10, :cond_0

    .line 190
    iget-object v10, v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;->layerInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;

    iget-object v10, v10, Lcom/google/android/apps/books/api/data/ApiaryVolume$LayerInfo;->layers:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/data/JsonLayer;

    .line 191
    .local v3, "layer":Lcom/google/android/apps/books/annotations/data/JsonLayer;
    iget-object v10, v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    iget-object v11, v7, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v11, v11, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->contentVersion:Ljava/lang/String;

    sget-object v12, Lcom/google/android/apps/books/annotations/Layer$Type;->VOLUME:Lcom/google/android/apps/books/annotations/Layer$Type;

    invoke-static {v10, v11, v12, v3}, Lcom/google/android/apps/books/annotations/Layer;->fromJson(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$Type;Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 204
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "layer":Lcom/google/android/apps/books/annotations/data/JsonLayer;
    .end local v4    # "layers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    .end local v5    # "licenseResponse":Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    .end local v6    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v7    # "volume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    .end local v8    # "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    .end local v9    # "volumesWithLicense":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iget-object v11, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v12, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->val$expiredKeyContinuation:Ljava/lang/Runnable;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Exception;Ljava/lang/Runnable;)V
    invoke-static {v10, p1, v11, v0, v12}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$600(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Exception;Ljava/lang/Runnable;)V

    .line 213
    .end local v0    # "e":Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException;
    :cond_1
    :goto_1
    return-void

    .line 200
    .restart local v4    # "layers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    .restart local v5    # "licenseResponse":Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    .restart local v6    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .restart local v8    # "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    .restart local v9    # "volumesWithLicense":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)V
    invoke-static {v10, p1, v5}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$400(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)V

    .line 201
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    .line 202
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;
    invoke-static {v10}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$500(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;)Lcom/google/android/apps/books/annotations/AnnotationController;

    move-result-object v10

    invoke-interface {v10, v4}, Lcom/google/android/apps/books/annotations/AnnotationController;->updateLayers(Ljava/util/List;)V
    :try_end_1
    .catch Lcom/google/android/apps/books/net/HttpHelper$KeyExpiredException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    .line 206
    .end local v4    # "layers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    .end local v5    # "licenseResponse":Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    .end local v6    # "server":Lcom/google/android/apps/books/net/BooksServer;
    .end local v8    # "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/api/data/ApiaryVolume;>;"
    .end local v9    # "volumesWithLicense":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_1
    move-exception v0

    .line 207
    .local v0, "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    invoke-static {v10, p1, v0}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$700(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V

    goto :goto_1

    .line 208
    .end local v0    # "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    :catch_2
    move-exception v0

    .line 209
    .local v0, "e":Ljava/security/GeneralSecurityException;
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    invoke-static {v10, p1, v0}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$700(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V

    goto :goto_1

    .line 210
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    :catch_3
    move-exception v0

    .line 211
    .local v0, "e":Ljava/io/IOException;
    iget-object v10, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    invoke-static {v10, p1, v0}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$700(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V

    goto :goto_1
.end method
