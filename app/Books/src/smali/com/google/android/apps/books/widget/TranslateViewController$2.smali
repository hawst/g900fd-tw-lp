.class Lcom/google/android/apps/books/widget/TranslateViewController$2;
.super Ljava/lang/Object;
.source "TranslateViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/api/TranslationServerController$DiscoverLanguagesCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/TranslateViewController;->discoverSupportedLanguages()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/TranslateViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/TranslateViewController;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/google/common/collect/BiMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "languages":Lcom/google/common/collect/BiMap;, "Lcom/google/common/collect/BiMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x1

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->getVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # setter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguages:Lcom/google/common/collect/BiMap;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$102(Lcom/google/android/apps/books/widget/TranslateViewController;Lcom/google/common/collect/BiMap;)Lcom/google/common/collect/BiMap;

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguages:Lcom/google/common/collect/BiMap;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$100(Lcom/google/android/apps/books/widget/TranslateViewController;)Lcom/google/common/collect/BiMap;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->sortedListOfLangaugeNames(Lcom/google/common/collect/BiMap;)Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$300(Lcom/google/android/apps/books/widget/TranslateViewController;Lcom/google/common/collect/BiMap;)Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$202(Lcom/google/android/apps/books/widget/TranslateViewController;Ljava/util/List;)Ljava/util/List;

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$400(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$200(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->addLanguagesToAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$500(Lcom/google/android/apps/books/widget/TranslateViewController;Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$600(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->lastUsedTargetLanguageCode()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$700(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->setSpinnerToLanguageCode(Landroid/widget/Spinner;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$800(Lcom/google/android/apps/books/widget/TranslateViewController;Landroid/widget/Spinner;Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$900(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mLanguageNames:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$200(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mDetectLanguageString:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1000(Lcom/google/android/apps/books/widget/TranslateViewController;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->addLanguagesToAdapter(Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$500(Lcom/google/android/apps/books/widget/TranslateViewController;Landroid/widget/ArrayAdapter;Ljava/util/List;Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$600(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1100(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mTargetLanguageSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$600(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # getter for: Lcom/google/android/apps/books/widget/TranslateViewController;->mSourceLanguageSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1100(Lcom/google/android/apps/books/widget/TranslateViewController;)Landroid/widget/Spinner;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/books/widget/TranslateViewController$2;->this$0:Lcom/google/android/apps/books/widget/TranslateViewController;

    # invokes: Lcom/google/android/apps/books/widget/TranslateViewController;->askServerToTranslateText()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->access$1200(Lcom/google/android/apps/books/widget/TranslateViewController;)V

    goto :goto_0
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 374
    const-string v0, "TranslateViewController"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    const-string v0, "TranslateViewController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error discovering languages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    return-void
.end method
