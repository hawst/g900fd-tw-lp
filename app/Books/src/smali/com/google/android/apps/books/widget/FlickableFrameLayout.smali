.class public Lcom/google/android/apps/books/widget/FlickableFrameLayout;
.super Landroid/widget/FrameLayout;
.source "FlickableFrameLayout.java"


# instance fields
.field private final mBackground:Landroid/graphics/drawable/Drawable;

.field private mBottom:I

.field private mChild:Landroid/view/View;

.field private final mDensity:F

.field private mDown:Z

.field private mDragging:Z

.field private mExtraHeight:I

.field private mIsFlickable:Z

.field private mLastY:F

.field private mLeft:I

.field private mOldAlpha:I

.field private final mPagingSlop:I

.field private mPosition:F

.field private mRight:I

.field private mStartPosition:F

.field private mTop:I

.field private mTouchX:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDensity:F

    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPagingSlop:I

    .line 31
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 36
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mOldAlpha:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDensity:F

    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPagingSlop:I

    .line 31
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 36
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mOldAlpha:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDensity:F

    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPagingSlop:I

    .line 31
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 36
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mOldAlpha:I

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    .line 58
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/widget/FlickableFrameLayout;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/FlickableFrameLayout;
    .param p1, "x1"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/FlickableFrameLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/FlickableFrameLayout;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->positionChild()V

    return-void
.end method

.method private positionChild()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 80
    iget-object v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mChild:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 81
    .local v3, "childWidth":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mChild:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 83
    .local v0, "childHeight":I
    iget-boolean v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mIsFlickable:Z

    if-nez v4, :cond_0

    .line 84
    iput v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    .line 87
    :cond_0
    iget v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLeft:I

    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mRight:I

    iget v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLeft:I

    sub-int/2addr v5, v6

    sub-int/2addr v5, v3

    div-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    .line 88
    .local v1, "childLeft":I
    iget v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mTop:I

    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mExtraHeight:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    add-int v2, v4, v5

    .line 90
    .local v2, "childTop":I
    iget v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    sub-float v4, v7, v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->setBackgroundAlpha(F)V

    .line 91
    iget-object v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mChild:Landroid/view/View;

    add-int v5, v1, v3

    add-int v6, v2, v0

    invoke-virtual {v4, v1, v2, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 92
    return-void
.end method

.method private setBackgroundAlpha(F)V
    .locals 2
    .param p1, "fraction"    # F

    .prologue
    .line 61
    iget-object v1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 62
    const/high16 v1, 0x43660000    # 230.0f

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 63
    .local v0, "alpha":I
    iget v1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mOldAlpha:I

    if-eq v0, v1, :cond_0

    .line 64
    if-nez v0, :cond_2

    .line 66
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/ublib/view/ViewCompat;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 75
    :cond_0
    :goto_0
    iput v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mOldAlpha:I

    .line 77
    .end local v0    # "alpha":I
    :cond_1
    return-void

    .line 68
    .restart local v0    # "alpha":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 70
    iget v1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mOldAlpha:I

    if-eqz v1, :cond_3

    const/16 v1, 0xe6

    if-ne v0, v1, :cond_0

    .line 71
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-static {p0, v1}, Lcom/google/android/ublib/view/ViewCompat;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private snapView(F)V
    .locals 8
    .param p1, "velocity"    # F

    .prologue
    .line 167
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    iget v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mStartPosition:F

    sub-float v2, v5, v6

    .line 168
    .local v2, "delta":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 171
    .local v0, "absDelta":F
    const v5, 0x3e4ccccd    # 0.2f

    cmpl-float v5, v0, v5

    if-gtz v5, :cond_0

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v5

    mul-float/2addr v5, p1

    const/high16 v6, 0x42c80000    # 100.0f

    iget v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDensity:F

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    .line 173
    :cond_0
    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mStartPosition:F

    sub-float v4, v5, v6

    .line 178
    .local v4, "target":F
    :goto_0
    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    aput v7, v5, v6

    const/4 v6, 0x1

    aput v4, v5, v6

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 180
    .local v1, "animator":Landroid/animation/ValueAnimator;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x10e0000

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 182
    .local v3, "duration":I
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mExtraHeight:I

    int-to-float v5, v5

    mul-float/2addr v5, v0

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v6

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    div-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 184
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 185
    int-to-long v6, v3

    invoke-virtual {v1, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 186
    new-instance v5, Lcom/google/android/apps/books/widget/FlickableFrameLayout$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout$1;-><init>(Lcom/google/android/apps/books/widget/FlickableFrameLayout;)V

    invoke-virtual {v1, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 194
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 195
    return-void

    .line 175
    .end local v1    # "animator":Landroid/animation/ValueAnimator;
    .end local v3    # "duration":I
    .end local v4    # "target":F
    :cond_1
    iget v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mStartPosition:F

    .restart local v4    # "target":F
    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 240
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 242
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 248
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 118
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 119
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 121
    .local v4, "y":F
    packed-switch v0, :pswitch_data_0

    .line 162
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    return v5

    .line 123
    :pswitch_0
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mIsFlickable:Z

    if-eqz v5, :cond_0

    .line 127
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    .line 128
    iget-object v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5}, Landroid/view/VelocityTracker;->clear()V

    .line 129
    iget-object v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 130
    iput v3, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mTouchX:F

    .line 131
    iput v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLastY:F

    .line 132
    iput-boolean v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDown:Z

    .line 134
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    const/high16 v6, 0x3f000000    # 0.5f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_1

    const/4 v5, 0x0

    :goto_1
    iput v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mStartPosition:F

    goto :goto_0

    :cond_1
    const/high16 v5, 0x3f800000    # 1.0f

    goto :goto_1

    .line 138
    :pswitch_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 140
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    if-nez v5, :cond_2

    .line 141
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mTouchX:F

    sub-float/2addr v5, v3

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 142
    .local v2, "drift":F
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPagingSlop:I

    int-to-float v5, v5

    cmpl-float v5, v2, v5

    if-lez v5, :cond_2

    .line 143
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDown:Z

    goto :goto_0

    .line 148
    .end local v2    # "drift":F
    :cond_2
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLastY:F

    sub-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 150
    .local v1, "delta":F
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDown:Z

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPagingSlop:I

    int-to-float v5, v5

    cmpl-float v5, v1, v5

    if-lez v5, :cond_0

    .line 151
    iput-boolean v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    .line 152
    iput v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLastY:F

    goto :goto_0

    .line 158
    .end local v1    # "delta":F
    :pswitch_2
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    .line 159
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDown:Z

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mChild:Landroid/view/View;

    .line 102
    iget-object v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mChild:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 105
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, p2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLeft:I

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, p3

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mTop:I

    .line 107
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getPaddingRight()I

    move-result v2

    sub-int v2, p4, v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mRight:I

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->getPaddingBottom()I

    move-result v2

    sub-int v2, p5, v2

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBottom:I

    .line 109
    iget v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mBottom:I

    iget v3, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mTop:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mChild:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mExtraHeight:I

    .line 110
    iget v2, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mExtraHeight:I

    iget v3, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPagingSlop:I

    mul-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mIsFlickable:Z

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->positionChild()V

    .line 113
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 199
    iget-boolean v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDown:Z

    if-nez v6, :cond_0

    .line 234
    :goto_0
    return v5

    .line 203
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 204
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 205
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 207
    .local v4, "y":F
    packed-switch v0, :pswitch_data_0

    .line 234
    :goto_1
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    goto :goto_0

    .line 210
    :pswitch_0
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLastY:F

    sub-float v1, v4, v5

    .line 211
    .local v1, "delta":F
    iput v4, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mLastY:F

    .line 213
    iget-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mIsFlickable:Z

    if-nez v5, :cond_1

    .line 214
    iput v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    goto :goto_1

    .line 216
    :cond_1
    iget v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    iget v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mExtraHeight:I

    int-to-float v6, v6

    div-float v6, v1, v6

    add-float/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    .line 217
    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iput v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mPosition:F

    .line 218
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->positionChild()V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->invalidate()V

    goto :goto_1

    .line 225
    .end local v1    # "delta":F
    :pswitch_1
    const/high16 v6, 0x44fa0000    # 2000.0f

    iget v7, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDensity:F

    mul-float v2, v6, v7

    .line 226
    .local v2, "maxVelocity":F
    iget-object v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v7, 0x3e8

    invoke-virtual {v6, v7, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 227
    iget-object v6, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v3

    .line 228
    .local v3, "velocity":F
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->snapView(F)V

    .line 229
    iput-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDown:Z

    .line 230
    iput-boolean v5, p0, Lcom/google/android/apps/books/widget/FlickableFrameLayout;->mDragging:Z

    goto :goto_1

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
