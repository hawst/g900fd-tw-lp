.class public Lcom/google/android/apps/books/util/LoggerWrapper;
.super Ljava/lang/Object;
.source "LoggerWrapper.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Logger;


# instance fields
.field private final mAvgPerfTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mPrefix:Ljava/lang/String;

.field private final mStartTime:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/AveragePerformanceTracker;)V
    .locals 2
    .param p1, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "avgPerfTracker"    # Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 21
    iput-object p2, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mPrefix:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mAvgPerfTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    .line 23
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mStartTime:J

    .line 24
    return-void
.end method


# virtual methods
.method public log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V
    .locals 8
    .param p1, "category"    # Lcom/google/android/apps/books/util/Logger$Category;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mStartTime:J

    sub-long v2, v4, v6

    .line 34
    .local v2, "elapsed":J
    iget-object v1, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mAvgPerfTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    invoke-virtual {v1, p2, v2, v3}, Lcom/google/android/apps/books/util/AveragePerformanceTracker;->buildAverageMessage(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "averageMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mLogger:Lcom/google/android/apps/books/util/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mPrefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " +"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, p1, v4}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mStartTime:J

    sub-long v2, v4, v6

    .line 46
    .local v2, "elapsed":J
    iget-object v1, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mAvgPerfTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    invoke-virtual {v1, p2, v2, v3}, Lcom/google/android/apps/books/util/AveragePerformanceTracker;->buildAverageMessage(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "averageMsg":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mLogger:Lcom/google/android/apps/books/util/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mPrefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " +"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, p1, v4}, Lcom/google/android/apps/books/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z
    .locals 1
    .param p1, "t"    # Lcom/google/android/apps/books/util/Logger$Category;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v0

    return v0
.end method

.method public shouldLog(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/util/LoggerWrapper;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
