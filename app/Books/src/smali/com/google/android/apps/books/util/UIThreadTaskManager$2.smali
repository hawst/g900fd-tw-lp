.class Lcom/google/android/apps/books/util/UIThreadTaskManager$2;
.super Ljava/lang/Object;
.source "UIThreadTaskManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/UIThreadTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->mQueueTaskPending:Z
    invoke-static {v1, v4}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$302(Lcom/google/android/apps/books/util/UIThreadTaskManager;Z)Z

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    const-wide/16 v4, 0x3e8

    # invokes: Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks(J)V
    invoke-static {v1, v4, v5}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$400(Lcom/google/android/apps/books/util/UIThreadTaskManager;J)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # invokes: Lcom/google/android/apps/books/util/UIThreadTaskManager;->getWaitMillis()J
    invoke-static {v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$500(Lcom/google/android/apps/books/util/UIThreadTaskManager;)J

    move-result-wide v2

    .line 126
    .local v2, "waitMillis":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # invokes: Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks(J)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$400(Lcom/google/android/apps/books/util/UIThreadTaskManager;J)V

    .line 129
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # invokes: Lcom/google/android/apps/books/util/UIThreadTaskManager;->pollQueue()Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    invoke-static {v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$600(Lcom/google/android/apps/books/util/UIThreadTaskManager;)Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    move-result-object v0

    .line 130
    .local v0, "task":Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->runTask()V

    .line 132
    iget-object v1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$2;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # invokes: Lcom/google/android/apps/books/util/UIThreadTaskManager;->checkQueuedTasks()V
    invoke-static {v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$700(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V

    goto :goto_0
.end method
