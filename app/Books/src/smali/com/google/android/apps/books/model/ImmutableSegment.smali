.class public Lcom/google/android/apps/books/model/ImmutableSegment;
.super Ljava/lang/Object;
.source "ImmutableSegment.java"

# interfaces
.implements Lcom/google/android/apps/books/model/Segment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    }
.end annotation


# instance fields
.field private final mFixedLayoutVersion:I

.field private final mFixedViewportHeight:I

.field private final mFixedViewportWidth:I

.field private final mId:Ljava/lang/String;

.field private final mPageCount:I

.field private final mStartPosition:Ljava/lang/String;

.field private mStartPositionObject:Lcom/google/android/apps/books/common/Position;

.field private final mTitle:Ljava/lang/String;

.field private final mUrl:Ljava/lang/String;

.field private final mViewable:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "startPosition"    # Ljava/lang/String;
    .param p4, "pageCount"    # I
    .param p5, "viewable"    # Z
    .param p6, "fixedLayoutVersion"    # I
    .param p7, "vpWidth"    # I
    .param p8, "vpHeight"    # I
    .param p9, "url"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mId:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mTitle:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mStartPosition:Ljava/lang/String;

    .line 29
    iput p4, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mPageCount:I

    .line 30
    iput-boolean p5, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mViewable:Z

    .line 31
    iput p6, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mFixedLayoutVersion:I

    .line 32
    iput p7, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mFixedViewportWidth:I

    .line 33
    iput p8, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mFixedViewportHeight:I

    .line 34
    iput-object p9, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mUrl:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 180
    instance-of v0, p1, Lcom/google/android/apps/books/model/Segment;

    if-eqz v0, :cond_0

    .line 181
    check-cast p1, Lcom/google/android/apps/books/model/Segment;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/SegmentUtils;->equals(Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/Segment;)Z

    move-result v0

    .line 183
    :goto_0
    return v0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFixedLayoutVersion()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mFixedLayoutVersion:I

    return v0
.end method

.method public getFixedViewportHeight()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mFixedViewportHeight:I

    return v0
.end method

.method public getFixedViewportWidth()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mFixedViewportWidth:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mPageCount:I

    return v0
.end method

.method public getStartPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mStartPosition:Ljava/lang/String;

    return-object v0
.end method

.method public getStartPositionObject()Lcom/google/android/apps/books/common/Position;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mStartPositionObject:Lcom/google/android/apps/books/common/Position;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/ImmutableSegment;->getStartPosition()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mStartPositionObject:Lcom/google/android/apps/books/common/Position;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mStartPositionObject:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 175
    invoke-static {p0}, Lcom/google/android/apps/books/model/SegmentUtils;->hashCode(Lcom/google/android/apps/books/model/Segment;)I

    move-result v0

    return v0
.end method

.method public isViewable()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableSegment;->mViewable:Z

    return v0
.end method
