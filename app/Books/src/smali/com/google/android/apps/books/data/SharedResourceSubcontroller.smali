.class public Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
.super Ljava/lang/Object;
.source "SharedResourceSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method private abortAll(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataStore$Committer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "tempsToSave":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/BooksDataStore$Committer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    .line 170
    .local v0, "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->bestEffortAbort()V

    goto :goto_0

    .line 172
    .end local v0    # "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->downloadResources(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->commitDownloadedResources(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->abortAll(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
    .param p1, "x1"    # Lcom/google/android/ublib/utils/Consumer;
    .param p2, "x2"    # Ljava/io/IOException;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->publishFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V

    return-void
.end method

.method private commitDownloadedResources(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataStore$Committer;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 158
    .local p2, "tempsToSave":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/BooksDataStore$Committer;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    .line 159
    .local v0, "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->commit()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 162
    .end local v0    # "committer":Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v1

    .line 163
    .local v1, "e":Ljava/io/IOException;
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->abortAll(Ljava/util/List;)V

    .line 164
    invoke-direct {p0, p3, v1}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->publishFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V

    .line 166
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 161
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->publishSuccess(Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private downloadResources(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p2, "needDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v4

    .line 128
    .local v4, "server":Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 130
    .local v6, "tempsToSave":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/BooksDataStore$Committer;>;"
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;

    .line 132
    .local v5, "task":Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;
    iget-object v7, v5, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Resource;->getUrl()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/google/android/apps/books/net/BooksServer;->getSharedFontContent(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 133
    .local v0, "content":Ljava/io/InputStream;
    iget-object v7, v5, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->resSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    new-instance v8, Lcom/google/android/apps/books/model/EncryptedContentImpl;

    invoke-direct {v8, v0}, Lcom/google/android/apps/books/model/EncryptedContentImpl;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v7, v5, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->resource:Lcom/google/android/apps/books/model/Resource;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Resource;->getMd5Hash()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 136
    .local v3, "md5Input":Ljava/io/ByteArrayInputStream;
    iget-object v7, v5, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;->md5Saver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    new-instance v8, Lcom/google/android/apps/books/model/EncryptedContentImpl;

    invoke-direct {v8, v3}, Lcom/google/android/apps/books/model/EncryptedContentImpl;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 144
    .end local v0    # "content":Ljava/io/InputStream;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "md5Input":Ljava/io/ByteArrayInputStream;
    .end local v5    # "task":Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;

    invoke-direct {v7, p0, v6, p3, v1}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V

    invoke-interface {p1, v7}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 153
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 138
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    new-instance v7, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$2;

    invoke-direct {v7, p0, v6, p3}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {p1, v7}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method private publishFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V
    .locals 1
    .param p2, "e"    # Ljava/io/IOException;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Ljava/io/IOException;",
            ")V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    invoke-static {p2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 177
    return-void
.end method

.method private publishSuccess(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    sget-object v0, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 181
    return-void
.end method


# virtual methods
.method public ensureSharedResources(Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 20
    .param p1, "networkPriority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p2, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p3, "resources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/Resource;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v11

    .line 69
    .local v11, "needDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/books/model/Resource;

    .line 70
    .local v12, "res":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v12}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 74
    invoke-interface {v12}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v14

    .line 75
    .local v14, "resId":Ljava/lang/String;
    invoke-interface {v12}, Lcom/google/android/apps/books/model/Resource;->getMd5Hash()Ljava/lang/String;

    move-result-object v7

    .line 76
    .local v7, "md5":Ljava/lang/String;
    const-string v15, "SharedResSub"

    const/16 v16, 0x3

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 77
    const-string v15, "SharedResSub"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Ensure Shared Res "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " md5 "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_1
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v15

    invoke-interface {v15, v14}, Lcom/google/android/apps/books/model/BooksDataStore;->getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v13

    .line 82
    .local v13, "resFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v15

    invoke-interface {v15, v14}, Lcom/google/android/apps/books/model/BooksDataStore;->getSharedResourceMD5File(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v8

    .line 84
    .local v8, "md5File":Lcom/google/android/apps/books/data/VolumeContentFile;
    invoke-interface {v13}, Lcom/google/android/apps/books/data/VolumeContentFile;->exists()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v8}, Lcom/google/android/apps/books/data/VolumeContentFile;->exists()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 85
    invoke-interface {v8}, Lcom/google/android/apps/books/data/VolumeContentFile;->getLength()J

    move-result-wide v16

    const-wide/16 v18, 0x20

    cmp-long v15, v16, v18

    if-nez v15, :cond_2

    .line 86
    invoke-interface {v8}, Lcom/google/android/apps/books/data/VolumeContentFile;->openInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 87
    .local v9, "md5Input":Ljava/io/InputStream;
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 88
    .local v10, "md5Output":Ljava/io/ByteArrayOutputStream;
    invoke-static {v9, v10}, Lcom/google/android/apps/books/util/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 89
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v15}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    .line 90
    .local v6, "match":I
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 91
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 92
    if-eqz v6, :cond_0

    .line 96
    .end local v6    # "match":I
    .end local v9    # "md5Input":Ljava/io/InputStream;
    .end local v10    # "md5Output":Ljava/io/ByteArrayOutputStream;
    :cond_2
    const-string v15, "SharedResSub"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 97
    const-string v15, "SharedResSub"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Replacing Shared Resource: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_3
    :goto_1
    new-instance v15, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;

    invoke-interface {v13}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v16

    invoke-interface {v8}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v15, v12, v0, v1}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;-><init>(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;)V

    invoke-interface {v11, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 119
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "md5":Ljava/lang/String;
    .end local v8    # "md5File":Lcom/google/android/apps/books/data/VolumeContentFile;
    .end local v11    # "needDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;>;"
    .end local v12    # "res":Lcom/google/android/apps/books/model/Resource;
    .end local v13    # "resFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    .end local v14    # "resId":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 120
    .local v4, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->publishFailure(Lcom/google/android/ublib/utils/Consumer;Ljava/io/IOException;)V

    .line 122
    .end local v4    # "e":Ljava/io/IOException;
    :goto_2
    return-void

    .line 100
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v7    # "md5":Ljava/lang/String;
    .restart local v8    # "md5File":Lcom/google/android/apps/books/data/VolumeContentFile;
    .restart local v11    # "needDownload":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/data/SharedResourceSubcontroller$SharedResourceDownloadTask;>;"
    .restart local v12    # "res":Lcom/google/android/apps/books/model/Resource;
    .restart local v13    # "resFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    .restart local v14    # "resId":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v15, "SharedResSub"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 101
    const-string v15, "SharedResSub"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Downloading new Shared Resource: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 109
    .end local v7    # "md5":Ljava/lang/String;
    .end local v8    # "md5File":Lcom/google/android/apps/books/data/VolumeContentFile;
    .end local v12    # "res":Lcom/google/android/apps/books/model/Resource;
    .end local v13    # "resFile":Lcom/google/android/apps/books/data/VolumeContentFile;
    .end local v14    # "resId":Ljava/lang/String;
    :cond_5
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_6

    .line 110
    new-instance v15, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v15, v0, v1, v11, v2}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/SharedResourceSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    goto :goto_2

    .line 117
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->publishSuccess(Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
