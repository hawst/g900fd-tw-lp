.class Lcom/google/android/apps/books/app/HighlightColorSelector$1;
.super Ljava/lang/Object;
.source "HighlightColorSelector.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatch(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HighlightColorSelector;

.field final synthetic val$prefColorIndex:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HighlightColorSelector;I)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->this$0:Lcom/google/android/apps/books/app/HighlightColorSelector;

    iput p2, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->val$prefColorIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->this$0:Lcom/google/android/apps/books/app/HighlightColorSelector;

    iget v1, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->val$prefColorIndex:I

    # setter for: Lcom/google/android/apps/books/app/HighlightColorSelector;->mCurrentPrefColorIndex:I
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->access$002(Lcom/google/android/apps/books/app/HighlightColorSelector;I)I

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->this$0:Lcom/google/android/apps/books/app/HighlightColorSelector;

    # getter for: Lcom/google/android/apps/books/app/HighlightColorSelector;->mShowSelection:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/HighlightColorSelector;->access$100(Lcom/google/android/apps/books/app/HighlightColorSelector;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->this$0:Lcom/google/android/apps/books/app/HighlightColorSelector;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/app/HighlightColorSelector;->setupSwatches(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/HighlightColorSelector;->access$200(Lcom/google/android/apps/books/app/HighlightColorSelector;Z)V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->this$0:Lcom/google/android/apps/books/app/HighlightColorSelector;

    # getter for: Lcom/google/android/apps/books/app/HighlightColorSelector;->mListener:Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HighlightColorSelector;->access$300(Lcom/google/android/apps/books/app/HighlightColorSelector;)Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/app/HighlightColorSelector$1;->val$prefColorIndex:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;->colorSelected(I)V

    .line 81
    return-void
.end method
