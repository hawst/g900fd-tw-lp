.class Lcom/google/android/apps/books/data/UploadsControllerImpl$13;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->resumeUpload(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$id:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 468
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$13;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$13;->val$id:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 471
    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$13;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$13;->val$id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/SingleBookUploader;

    .line 472
    .local v0, "uploader":Lcom/google/android/apps/books/upload/SingleBookUploader;
    if-eqz v0, :cond_0

    .line 473
    invoke-interface {v0}, Lcom/google/android/apps/books/upload/SingleBookUploader;->resumeUpload()V

    .line 475
    :cond_0
    return-void
.end method
