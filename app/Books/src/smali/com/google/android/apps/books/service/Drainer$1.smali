.class Lcom/google/android/apps/books/service/Drainer$1;
.super Ljava/lang/Object;
.source "Drainer.java"

# interfaces
.implements Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/service/Drainer;->managed(Ljava/util/concurrent/ExecutorService;)Lcom/google/android/apps/books/service/Drainer$DrainableExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/service/Drainer;

.field final synthetic val$delegate:Ljava/util/concurrent/ExecutorService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/Drainer;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/service/Drainer$1;->this$0:Lcom/google/android/apps/books/service/Drainer;

    iput-object p2, p0, Lcom/google/android/apps/books/service/Drainer$1;->val$delegate:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v1, p0, Lcom/google/android/apps/books/service/Drainer$1;->val$delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 51
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    iget-object v1, p0, Lcom/google/android/apps/books/service/Drainer$1;->this$0:Lcom/google/android/apps/books/service/Drainer;

    # getter for: Lcom/google/android/apps/books/service/Drainer;->mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v1}, Lcom/google/android/apps/books/service/Drainer;->access$000(Lcom/google/android/apps/books/service/Drainer;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 52
    return-object v0
.end method

.method public submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "task":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/service/Drainer$1;->val$delegate:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 44
    .local v0, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/service/Drainer$1;->this$0:Lcom/google/android/apps/books/service/Drainer;

    # getter for: Lcom/google/android/apps/books/service/Drainer;->mFutures:Ljava/util/concurrent/ConcurrentLinkedQueue;
    invoke-static {v1}, Lcom/google/android/apps/books/service/Drainer;->access$000(Lcom/google/android/apps/books/service/Drainer;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 45
    return-object v0
.end method
