.class public interface abstract Lcom/google/android/apps/books/app/AccountPicker$Callbacks;
.super Ljava/lang/Object;
.source "AccountPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AccountPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract finishedPickingAccount(Landroid/accounts/Account;)Z
.end method

.method public abstract showPreIcsAccountPicker()V
.end method
