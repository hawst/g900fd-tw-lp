.class public Lcom/google/android/apps/books/render/ReaderBridgeAdapter;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"


# static fields
.field private static final LOGD:Z


# instance fields
.field private final mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mListener:Lcom/google/android/apps/books/render/ReaderListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "ReaderBridgeAdapter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->LOGD:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/render/ReaderListener;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/render/ReaderDataSource;)V
    .locals 1
    .param p1, "target"    # Lcom/google/android/apps/books/render/ReaderListener;
    .param p2, "executor"    # Ljava/util/concurrent/Executor;
    .param p3, "dataSource"    # Lcom/google/android/apps/books/render/ReaderDataSource;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const-string v0, "missing target"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/ReaderListener;

    iput-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;

    .line 70
    const-string v0, "missing executor"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    .line 71
    const-string v0, "missing data source"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/ReaderDataSource;

    iput-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    .line 72
    return-void
.end method

.method private abbreviateContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 484
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 485
    .local v1, "length":I
    const-string v3, "ReaderBridgeAdapter"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0x32

    if-gt v1, v3, :cond_1

    .line 486
    :cond_0
    move-object v2, p1

    .line 493
    .local v2, "returnValue":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 488
    .end local v2    # "returnValue":Ljava/lang/String;
    :cond_1
    const/16 v0, 0x19

    .line 489
    .local v0, "half":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const/16 v5, 0x19

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "...(excised)..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v1, -0x19

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "returnValue":Ljava/lang/String;
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ReaderBridgeAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->parseMoElements(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private parseBounds(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .locals 6
    .param p1, "pageBoundsJson"    # Ljava/lang/String;

    .prologue
    .line 502
    const-string v5, ","

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 503
    .local v2, "numbers":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 504
    .local v1, "lef":I
    const/4 v5, 0x1

    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 505
    .local v4, "top":I
    const/4 v5, 0x2

    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 506
    .local v3, "rig":I
    const/4 v5, 0x3

    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 507
    .local v0, "bot":I
    new-instance v5, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    invoke-direct {v5, v1, v4, v3, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;-><init>(IIII)V

    return-object v5
.end method

.method private parseMoElements(Ljava/lang/String;)Ljava/util/Map;
    .locals 9
    .param p1, "jsonString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 564
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 565
    .local v2, "items":Lorg/json/JSONArray;
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v6

    .line 566
    .local v6, "readableItemsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_1

    .line 567
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 568
    .local v4, "jsonItem":Lorg/json/JSONObject;
    const-string v7, "elementId"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569
    .local v0, "elementId":Ljava/lang/String;
    const-string v7, "pageIndex"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 570
    .local v5, "pageIndex":I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 573
    .end local v0    # "elementId":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "items":Lorg/json/JSONArray;
    .end local v4    # "jsonItem":Lorg/json/JSONObject;
    .end local v5    # "pageIndex":I
    .end local v6    # "readableItemsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :catch_0
    move-exception v3

    .line 574
    .local v3, "je":Lorg/json/JSONException;
    const-string v7, "ReaderBridgeAdapter"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 575
    const-string v7, "ReaderBridgeAdapter"

    const-string v8, "Error parsing MO elements"

    invoke-static {v7, v8, v3}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 577
    :cond_0
    const/4 v6, 0x0

    .end local v3    # "je":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method

.method private parsePositions(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "positionIds"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation

    .prologue
    .line 511
    const/16 v5, 0x2c

    invoke-static {v5}, Lcom/google/common/base/Splitter;->on(C)Lcom/google/common/base/Splitter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/common/base/Splitter;->omitEmptyStrings()Lcom/google/common/base/Splitter;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/common/base/Splitter;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v2

    .line 513
    .local v2, "idList":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 514
    .local v4, "positions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 515
    .local v1, "id":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/common/Position;->createPositionOrNull(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    .line 516
    .local v3, "position":Lcom/google/android/apps/books/common/Position;
    if-eqz v3, :cond_0

    .line 517
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 520
    .end local v1    # "id":Ljava/lang/String;
    .end local v3    # "position":Lcom/google/android/apps/books/common/Position;
    :cond_1
    return-object v4
.end method

.method private parseTextRange(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 10
    .param p1, "contentRangeJson"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 582
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-object v6

    .line 584
    :cond_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 585
    .local v3, "jsonItem":Lorg/json/JSONObject;
    const-string v7, "startPosition"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 586
    .local v5, "startPosition":Ljava/lang/String;
    const-string v7, "startOffset"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 587
    .local v4, "startOffset":I
    const-string v7, "endPosition"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 588
    .local v1, "endPosition":Ljava/lang/String;
    const-string v7, "endOffset"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 590
    .local v0, "endOffset":I
    new-instance v7, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v7, v5, v4, v1, v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v7

    goto :goto_0

    .line 591
    .end local v0    # "endOffset":I
    .end local v1    # "endPosition":Ljava/lang/String;
    .end local v3    # "jsonItem":Lorg/json/JSONObject;
    .end local v4    # "startOffset":I
    .end local v5    # "startPosition":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 592
    .local v2, "je":Lorg/json/JSONException;
    const-string v7, "ReaderBridgeAdapter"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 593
    const-string v7, "ReaderBridgeAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error parseTextRange: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private parseTouchableItems(Ljava/lang/String;)Ljava/util/List;
    .locals 21
    .param p1, "TouchableItemsJson"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 525
    :try_start_0
    new-instance v12, Lorg/json/JSONArray;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 526
    .local v12, "items":Lorg/json/JSONArray;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v16

    .line 527
    .local v16, "touchableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v11, v3, :cond_4

    .line 528
    invoke-virtual {v12, v11}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 529
    .local v15, "jsonItem":Lorg/json/JSONObject;
    const-string v3, "bounds"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 530
    .local v14, "jsonBounds":Lorg/json/JSONObject;
    new-instance v5, Landroid/graphics/Rect;

    const-string v3, "left"

    invoke-virtual {v14, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v18, "top"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v18

    const-string v19, "right"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    const-string v20, "bottom"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v5, v3, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 533
    .local v5, "bounds":Landroid/graphics/Rect;
    const-string v3, "source"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 534
    .local v7, "source":Ljava/lang/String;
    const-string v3, "type"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 535
    .local v17, "typeString":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/books/render/TouchableItem;->typeFromString(Ljava/lang/String;)I

    move-result v4

    .line 536
    .local v4, "type":I
    if-nez v4, :cond_0

    const-string v3, "ReaderBridgeAdapter"

    const/16 v18, 0x6

    move/from16 v0, v18

    invoke-static {v3, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 537
    const-string v3, "ReaderBridgeAdapter"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "unknown media type: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    :cond_0
    const-string v3, "hasControls"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 540
    .local v6, "hasControls":Ljava/lang/Boolean;
    const-string v3, "linkText"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "linkText"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 542
    .local v8, "linkText":Ljava/lang/String;
    :goto_1
    const-string v3, "isFootnote"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 543
    .local v9, "isFootnote":I
    const-string v3, "originalId"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "originalId"

    invoke-virtual {v15, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 545
    .local v10, "originalId":Ljava/lang/String;
    :goto_2
    new-instance v3, Lcom/google/android/apps/books/render/TouchableItem;

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/books/render/TouchableItem;-><init>(ILandroid/graphics/Rect;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 540
    .end local v8    # "linkText":Ljava/lang/String;
    .end local v9    # "isFootnote":I
    .end local v10    # "originalId":Ljava/lang/String;
    :cond_1
    const-string v8, ""
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 543
    .restart local v8    # "linkText":Ljava/lang/String;
    .restart local v9    # "isFootnote":I
    :cond_2
    const/4 v10, 0x0

    goto :goto_2

    .line 549
    .end local v4    # "type":I
    .end local v5    # "bounds":Landroid/graphics/Rect;
    .end local v6    # "hasControls":Ljava/lang/Boolean;
    .end local v7    # "source":Ljava/lang/String;
    .end local v8    # "linkText":Ljava/lang/String;
    .end local v9    # "isFootnote":I
    .end local v11    # "i":I
    .end local v12    # "items":Lorg/json/JSONArray;
    .end local v14    # "jsonBounds":Lorg/json/JSONObject;
    .end local v15    # "jsonItem":Lorg/json/JSONObject;
    .end local v16    # "touchableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    .end local v17    # "typeString":Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 550
    .local v13, "je":Lorg/json/JSONException;
    const-string v3, "ReaderBridgeAdapter"

    const/16 v18, 0x6

    move/from16 v0, v18

    invoke-static {v3, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 551
    const-string v3, "ReaderBridgeAdapter"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error loading touchable: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    :cond_3
    const/16 v16, 0x0

    .end local v13    # "je":Lorg/json/JSONException;
    :cond_4
    return-object v16
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$19;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$19;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 375
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$20;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$20;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 386
    return-void
.end method

.method public getCssContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "cssIndex"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 434
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    invoke-interface {v3, p1, p2}, Lcom/google/android/apps/books/render/ReaderDataSource;->getCssContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 435
    .local v0, "content":Ljava/lang/String;
    sget-boolean v3, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->LOGD:Z

    if-eqz v3, :cond_0

    .line 436
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCssContent(volumeId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", cssIndex="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 438
    .local v2, "signature":Ljava/lang/String;
    const-string v3, "ReaderBridgeAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Returning "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " characters;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->abbreviateContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    .end local v0    # "content":Ljava/lang/String;
    .end local v2    # "signature":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 442
    :catch_0
    move-exception v1

    .line 443
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCssContent(volumeId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", cssIndex="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 445
    .restart local v2    # "signature":Ljava/lang/String;
    const-string v3, "ReaderBridgeAdapter"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/LogUtil;->logAsErrorOrDebug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$22;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$22;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/Throwable;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 455
    const-string v0, ""

    goto :goto_0
.end method

.method public getResourceContentUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 469
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/render/ReaderDataSource;->getResourceContentUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, "content":Ljava/lang/String;
    const-string v2, "ReaderBridgeAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "buildResourceContentUri() success, length="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 475
    .end local v0    # "content":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 472
    :catch_0
    move-exception v1

    .line 474
    .local v1, "e":Ljava/lang/Throwable;
    const-string v2, "error during buildResourceContentUri()"

    const-string v3, "ReaderBridgeAdapter"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/LogUtil;->logAsErrorOrDebug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const-string v0, "data:"

    goto :goto_0
.end method

.method public getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 397
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    invoke-interface {v3, p1, p2}, Lcom/google/android/apps/books/render/ReaderDataSource;->getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "content":Ljava/lang/String;
    sget-boolean v3, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->LOGD:Z

    if-eqz v3, :cond_0

    .line 399
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSectionContent(volumeId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", segmentId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 401
    .local v2, "signature":Ljava/lang/String;
    const-string v3, "ReaderBridgeAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Returning "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " characters;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->abbreviateContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    .end local v0    # "content":Ljava/lang/String;
    .end local v2    # "signature":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 405
    :catch_0
    move-exception v1

    .line 406
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSectionContent(volumeId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", segmentId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 408
    .restart local v2    # "signature":Ljava/lang/String;
    const-string v3, "ReaderBridgeAdapter"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/LogUtil;->logAsErrorOrDebug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$21;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$21;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/Throwable;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 418
    const-string v0, ""

    goto :goto_0
.end method

.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$13;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$13;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 266
    return-void
.end method

.method public onChapterReady(ILjava/lang/String;)V
    .locals 2
    .param p1, "chapterIndex"    # I
    .param p2, "metricsJSON"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$4;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 117
    return-void
.end method

.method public onChapterUnloaded(I)V
    .locals 2
    .param p1, "chapterIndex"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$5;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 129
    return-void
.end method

.method public onDocumentChanged()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$17;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$17;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 351
    return-void
.end method

.method public onInvalidPosition(II)V
    .locals 2
    .param p1, "margin"    # I
    .param p2, "requestId"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$10;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;II)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 208
    return-void
.end method

.method public onJsApiReady()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$1;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 83
    return-void
.end method

.method public onLoadedRangeData(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "requestId"    # I
    .param p2, "textPointsJson"    # Ljava/lang/String;
    .param p3, "handlesPointsJson"    # Ljava/lang/String;
    .param p4, "contentRangeJson"    # Ljava/lang/String;
    .param p5, "text"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 287
    invoke-direct {p0, p4}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->parseTextRange(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v5

    .line 290
    .local v5, "textRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v6, 0x0

    .line 293
    .local v6, "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$15;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 302
    return-void

    .line 290
    .end local v6    # "textContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    :cond_0
    new-instance v6, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    const-string v0, ""

    const-string v1, ""

    invoke-direct {v6, v0, p5, v1}, Lcom/google/android/apps/books/annotations/AnnotationTextualContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoadedRangeDataBulk(ILjava/lang/String;)V
    .locals 18
    .param p1, "requestId"    # I
    .param p2, "result"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 308
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v6

    .line 310
    .local v6, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    invoke-static/range {p2 .. p2}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 312
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 313
    .local v2, "array":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v14

    div-int/lit8 v9, v14, 0x2

    .line 314
    .local v9, "numIds":I
    const/4 v7, 0x0

    .local v7, "ii":I
    :goto_0
    if-ge v7, v9, :cond_1

    .line 315
    mul-int/lit8 v5, v7, 0x2

    .line 316
    .local v5, "idIndex":I
    invoke-virtual {v2, v5}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "id":Ljava/lang/String;
    add-int/lit8 v14, v5, 0x1

    invoke-virtual {v2, v14}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v11

    .line 318
    .local v11, "points":Lorg/json/JSONArray;
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I

    move-result v14

    div-int/lit8 v10, v14, 0x4

    .line 319
    .local v10, "numRects":I
    const/4 v8, 0x0

    .local v8, "jj":I
    :goto_1
    if-ge v8, v10, :cond_0

    .line 320
    mul-int/lit8 v13, v8, 0x4

    .line 321
    .local v13, "rectIndex":I
    new-instance v12, Landroid/graphics/Rect;

    invoke-virtual {v11, v13}, Lorg/json/JSONArray;->getInt(I)I

    move-result v14

    add-int/lit8 v15, v13, 0x1

    invoke-virtual {v11, v15}, Lorg/json/JSONArray;->getInt(I)I

    move-result v15

    add-int/lit8 v16, v13, 0x2

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v16

    add-int/lit8 v17, v13, 0x3

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v12, v14, v15, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 324
    .local v12, "rect":Landroid/graphics/Rect;
    invoke-interface {v6, v4, v12}, Lcom/google/common/collect/Multimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 314
    .end local v12    # "rect":Landroid/graphics/Rect;
    .end local v13    # "rectIndex":I
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 327
    .end local v2    # "array":Lorg/json/JSONArray;
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "idIndex":I
    .end local v7    # "ii":I
    .end local v8    # "jj":I
    .end local v9    # "numIds":I
    .end local v10    # "numRects":I
    .end local v11    # "points":Lorg/json/JSONArray;
    :catch_0
    move-exception v3

    .line 328
    .local v3, "e":Lorg/json/JSONException;
    const-string v14, "ReaderBridgeAdapter"

    const/4 v15, 0x6

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 329
    const-string v14, "ReaderBridgeAdapter"

    const-string v15, "Malformed bulk range JSON from reader"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v15, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v15, v0, v1, v6}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;ILcom/google/common/collect/Multimap;)V

    invoke-interface {v14, v15}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 340
    return-void
.end method

.method public onMissingPosition(I)V
    .locals 2
    .param p1, "requestId"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$11;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$11;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 219
    return-void
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 8
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 357
    iget-object v7, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;ILjava/lang/String;ILjava/lang/String;I)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 364
    return-void
.end method

.method public onPageData(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "readingPosition"    # Ljava/lang/String;
    .param p4, "allPositionsOnThisPage"    # Ljava/lang/String;
    .param p5, "touchableItemsJson"    # Ljava/lang/String;
    .param p6, "pageBoundsJson"    # Ljava/lang/String;
    .param p7, "firstPositionOnNextPageInPassage"    # Ljava/lang/String;
    .param p8, "pageText"    # Ljava/lang/String;
    .param p9, "debugText"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 184
    move-object/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->parseTouchableItems(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 185
    .local v7, "touchableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->parsePositions(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 186
    .local v6, "positions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    move-object/from16 v0, p6

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->parseBounds(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v8

    .line 188
    .local v8, "bounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    iget-object v12, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;

    move-object v2, p0

    move v3, p1

    move v4, p2

    move-object/from16 v5, p3

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$9;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v12, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 197
    return-void
.end method

.method public onPageLoaded(III)V
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "requestId"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$7;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;III)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method public onPageRangeReady(II)V
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "lastReadyPageIndex"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$6;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;II)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method public onPassageMoListReady(IILjava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "moElementsJson"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$14;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 281
    return-void
.end method

.method public onPassageTextReady(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positions"    # Ljava/lang/String;
    .param p5, "offsets"    # Ljava/lang/String;
    .param p6, "altStrings"    # Ljava/lang/String;
    .param p7, "altOffsets"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 230
    :try_start_0
    invoke-static/range {p4 .. p5}, Lcom/google/android/apps/books/model/LabelMap;->from(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v9

    .line 231
    .local v9, "positionMap":Lcom/google/android/apps/books/model/LabelMap;
    invoke-static/range {p6 .. p7}, Lcom/google/android/apps/books/model/LabelMap;->from(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/LabelMap;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 240
    .local v7, "altStringsOffsets":Lcom/google/android/apps/books/model/LabelMap;
    :goto_0
    move-object v5, v9

    .line 241
    .local v5, "finalPositions":Lcom/google/android/apps/books/model/LabelMap;
    move-object v6, v7

    .line 244
    .local v6, "finalAltMap":Lcom/google/android/apps/books/model/LabelMap;
    iget-object v10, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    invoke-interface {v10, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 252
    return-void

    .line 232
    .end local v5    # "finalPositions":Lcom/google/android/apps/books/model/LabelMap;
    .end local v6    # "finalAltMap":Lcom/google/android/apps/books/model/LabelMap;
    .end local v7    # "altStringsOffsets":Lcom/google/android/apps/books/model/LabelMap;
    .end local v9    # "positionMap":Lcom/google/android/apps/books/model/LabelMap;
    :catch_0
    move-exception v8

    .line 233
    .local v8, "e":Lorg/json/JSONException;
    const-string v0, "ReaderBridgeAdapter"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    const-string v0, "ReaderBridgeAdapter"

    const-string v1, "Malformed JSON from reader"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/model/LabelMap;->empty()Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v9

    .line 237
    .restart local v9    # "positionMap":Lcom/google/android/apps/books/model/LabelMap;
    invoke-static {}, Lcom/google/android/apps/books/model/LabelMap;->empty()Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v7

    .restart local v7    # "altStringsOffsets":Lcom/google/android/apps/books/model/LabelMap;
    goto :goto_0
.end method

.method public onPassagesPurged(ILjava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "passages"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$8;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$8;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 174
    return-void
.end method

.method public onPreferencesApplied()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$3;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 105
    return-void
.end method

.method public onReaderInitialized()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$2;-><init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 94
    return-void
.end method
