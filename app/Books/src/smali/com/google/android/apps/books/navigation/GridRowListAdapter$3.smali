.class Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;
.super Ljava/lang/Object;
.source "GridRowListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

.field final synthetic val$isCollapsed:Z

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/GridRowListAdapter;IZ)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    iput p2, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->val$position:I

    iput-boolean p3, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->val$isCollapsed:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapsedChapters:Landroid/util/SparseBooleanArray;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$300(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)Landroid/util/SparseBooleanArray;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->val$position:I

    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->val$isCollapsed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$400(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;->val$position:I

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->rebuildView(I)V

    .line 293
    return-void

    .line 291
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
