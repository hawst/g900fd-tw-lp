.class public Lcom/google/android/apps/books/util/UiThreadConsumer;
.super Ljava/lang/Object;
.source "UiThreadConsumer.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mWrappedConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/apps/books/util/UiThreadConsumer;, "Lcom/google/android/apps/books/util/UiThreadConsumer<TT;>;"
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/books/util/UiThreadConsumer;->mWrappedConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/UiThreadConsumer;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/UiThreadConsumer;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/android/apps/books/util/UiThreadConsumer;->mWrappedConsumer:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method public static proxyTo(Lcom/google/android/apps/books/util/Eventual;)Lcom/google/android/apps/books/util/UiThreadConsumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/books/util/Eventual",
            "<TT;>;)",
            "Lcom/google/android/apps/books/util/UiThreadConsumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "eventual":Lcom/google/android/apps/books/util/Eventual;, "Lcom/google/android/apps/books/util/Eventual<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Eventual;->loadingConsumer()Lcom/google/android/ublib/utils/Consumer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/UiThreadConsumer;->proxyTo(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/apps/books/util/UiThreadConsumer;

    move-result-object v0

    return-object v0
.end method

.method public static proxyTo(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/apps/books/util/UiThreadConsumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)",
            "Lcom/google/android/apps/books/util/UiThreadConsumer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    new-instance v0, Lcom/google/android/apps/books/util/UiThreadConsumer;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/util/UiThreadConsumer;-><init>(Lcom/google/android/ublib/utils/Consumer;)V

    return-object v0
.end method


# virtual methods
.method public take(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/apps/books/util/UiThreadConsumer;, "Lcom/google/android/apps/books/util/UiThreadConsumer<TT;>;"
    .local p1, "t":Ljava/lang/Object;, "TT;"
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/util/UiThreadConsumer$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/util/UiThreadConsumer$1;-><init>(Lcom/google/android/apps/books/util/UiThreadConsumer;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 37
    return-void
.end method
