.class public Lcom/google/android/apps/books/render/JavaScriptExecutors;
.super Ljava/lang/Object;
.source "JavaScriptExecutors.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/JavaScriptExecutors$PreKitKatExecutor;,
        Lcom/google/android/apps/books/render/JavaScriptExecutors$KitKatExecutor;,
        Lcom/google/android/apps/books/render/JavaScriptExecutors$AbstractExecutor;
    }
.end annotation


# direct methods
.method public static newExecutor(Landroid/webkit/WebView;)Lcom/google/android/apps/books/render/JavaScriptExecutor;
    .locals 1
    .param p0, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 78
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Lcom/google/android/apps/books/render/JavaScriptExecutors$KitKatExecutor;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/JavaScriptExecutors$KitKatExecutor;-><init>(Landroid/webkit/WebView;)V

    .line 81
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/render/JavaScriptExecutors$PreKitKatExecutor;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/JavaScriptExecutors$PreKitKatExecutor;-><init>(Landroid/webkit/WebView;)V

    goto :goto_0
.end method
