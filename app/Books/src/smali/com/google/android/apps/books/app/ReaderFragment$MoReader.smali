.class Lcom/google/android/apps/books/app/ReaderFragment$MoReader;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoReader"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 7603
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 7603
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)V
    .locals 3
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 7607
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 7608
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7609
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "activateMediaElement, passage="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", offset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", el="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7612
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/PagesViewController;->activateMediaElement(IILjava/lang/String;)V

    .line 7614
    :cond_1
    return-void
.end method

.method public finishedPlayback(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 6
    .param p1, "reason"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    const/4 v2, 0x0

    .line 7634
    const-string v1, "ReaderFragment"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7635
    const-string v1, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Finished speaking due to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", releasing wakelock"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7638
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->DOWNLOAD_ERROR:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-ne p1, v1, :cond_1

    .line 7639
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v3, 0x7f0f0080

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 7642
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 7645
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 7646
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v0

    .line 7648
    .local v0, "controller":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v5

    if-nez v0, :cond_2

    move v1, v2

    :goto_0
    invoke-interface {v3, v4, v5, v2, v1}, Lcom/google/android/apps/books/app/ReaderMenu;->setMoItemState(Landroid/content/Context;ZZZ)V

    .line 7651
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 7652
    return-void

    .line 7648
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->canResume()Z

    move-result v1

    goto :goto_0
.end method

.method public startedPlaying()V
    .locals 2

    .prologue
    .line 7618
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7619
    const-string v0, "ReaderFragment"

    const-string v1, "startedPlaying, acquiring wakelock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7621
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 7622
    return-void
.end method

.method public stoppedPlaying()V
    .locals 2

    .prologue
    .line 7626
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7627
    const-string v0, "ReaderFragment"

    const-string v1, "stoppedPlaying, releasing wakelock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7629
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 7630
    return-void
.end method
