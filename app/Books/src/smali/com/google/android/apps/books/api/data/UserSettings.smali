.class public Lcom/google/android/apps/books/api/data/UserSettings;
.super Ljava/lang/Object;
.source "UserSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    }
.end annotation


# instance fields
.field public kind:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "kind"
    .end annotation
.end field

.field public notesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "notesExport"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static createUserSettingsForNotesExport(ZLjava/lang/String;)Lcom/google/android/apps/books/api/data/UserSettings;
    .locals 2
    .param p0, "isEnabled"    # Z
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    invoke-direct {v0}, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;-><init>()V

    .line 49
    .local v0, "notesExport":Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    iput-boolean p0, v0, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->isEnabled:Z

    .line 50
    iput-object p1, v0, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->folderName:Ljava/lang/String;

    .line 52
    new-instance v1, Lcom/google/android/apps/books/api/data/UserSettings;

    invoke-direct {v1}, Lcom/google/android/apps/books/api/data/UserSettings;-><init>()V

    .line 53
    .local v1, "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    iput-object v0, v1, Lcom/google/android/apps/books/api/data/UserSettings;->notesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    .line 54
    return-object v1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 39
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "kind"

    iget-object v2, p0, Lcom/google/android/apps/books/api/data/UserSettings;->kind:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "notesExport"

    iget-object v2, p0, Lcom/google/android/apps/books/api/data/UserSettings;->notesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
