.class Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$1;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$OnLoadedRangeDataBulkHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;)V
    .locals 0

    .prologue
    .line 1298
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(ILcom/google/common/collect/Multimap;)V
    .locals 2
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1301
    .local p2, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$LoadRangeDataBulkRequest;->mExternalRequestId:I

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/books/render/ReaderListener;->onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V

    .line 1302
    return-void
.end method
