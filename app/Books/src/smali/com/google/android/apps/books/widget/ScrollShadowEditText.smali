.class public Lcom/google/android/apps/books/widget/ScrollShadowEditText;
.super Landroid/widget/EditText;
.source "ScrollShadowEditText.java"


# instance fields
.field private mIsScrollable:Z

.field private mShadowBackground:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->init(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->init(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->init(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 37
    const/4 v2, 0x1

    new-array v1, v2, [I

    const v2, 0x7f010184

    aput v2, v1, v3

    .line 38
    .local v1, "attributeIds":[I
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 39
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->mShadowBackground:Landroid/graphics/drawable/Drawable;

    .line 40
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/widget/EditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 64
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->mIsScrollable:Z

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->mShadowBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 68
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 70
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v4, 0x0

    .line 45
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onSizeChanged(IIII)V

    .line 47
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 48
    .local v1, "measureSpec":I
    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->measure(II)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getMeasuredHeight()I

    move-result v0

    .line 51
    .local v0, "height":I
    if-le v0, p2, :cond_0

    const/4 v4, 0x1

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->mIsScrollable:Z

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getPaddingRight()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 55
    .local v2, "paddingX":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->getPaddingBottom()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 57
    .local v3, "paddingY":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ScrollShadowEditText;->mShadowBackground:Landroid/graphics/drawable/Drawable;

    sub-int v5, p1, v2

    sub-int v6, p2, v3

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 58
    return-void
.end method
