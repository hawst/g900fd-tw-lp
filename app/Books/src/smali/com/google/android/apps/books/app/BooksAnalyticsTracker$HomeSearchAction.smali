.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HomeSearchAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

.field public static final enum HOME_MENU_SEARCH_REQUESTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

.field public static final enum HOME_SEARCH_SELECT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

.field public static final enum HOME_SEARCH_STORE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

.field public static final enum HOME_SEARCH_SUBMITTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    const-string v1, "HOME_MENU_SEARCH_REQUESTED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_MENU_SEARCH_REQUESTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    .line 146
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    const-string v1, "HOME_SEARCH_SUBMITTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_SUBMITTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    .line 150
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    const-string v1, "HOME_SEARCH_SELECT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_SELECT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    .line 154
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    const-string v1, "HOME_SEARCH_STORE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_STORE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    .line 138
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_MENU_SEARCH_REQUESTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_SUBMITTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_SELECT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->HOME_SEARCH_STORE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 138
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;
    .locals 1

    .prologue
    .line 138
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSearchAction;

    return-object v0
.end method
