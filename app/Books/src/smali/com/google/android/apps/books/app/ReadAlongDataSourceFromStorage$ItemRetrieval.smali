.class abstract Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;
.super Ljava/lang/Object;
.source "ReadAlongDataSourceFromStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "ItemRetrieval"
.end annotation


# instance fields
.field protected final mPassageSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegmentContents:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)V
    .locals 2
    .param p2, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mSegmentContents:Landroid/util/SparseArray;

    .line 113
    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {p1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$000(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPassage()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageSegments(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mPassageSegments:Ljava/util/List;

    .line 114
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mSegmentContents:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->publishResult()V

    return-void
.end method

.method private publishResult()V
    .locals 6

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->getXmlHandler()Lcom/google/android/apps/books/model/ContentXmlHandler;

    move-result-object v2

    .line 145
    .local v2, "handler":Lcom/google/android/apps/books/model/ContentXmlHandler;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mSegmentContents:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 146
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mSegmentContents:Landroid/util/SparseArray;

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    .line 147
    .local v0, "contents":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 149
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/model/ContentXmlHandler;->parse(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 150
    :catch_0
    move-exception v1

    .line 151
    .local v1, "e":Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
    const-string v4, "MoSourceFromStorage"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 152
    const-string v4, "MoSourceFromStorage"

    const-string v5, "Exception parsing TTS segment text"

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->onFailedParse()V

    .line 164
    .end local v0    # "contents":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    .end local v1    # "e":Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
    :goto_1
    return-void

    .line 158
    .restart local v0    # "contents":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->onFailedParse()V

    goto :goto_1

    .line 163
    .end local v0    # "contents":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->onSuccessfulParse()V

    goto :goto_1
.end method


# virtual methods
.method protected abstract getXmlHandler()Lcom/google/android/apps/books/model/ContentXmlHandler;
.end method

.method protected abstract onFailedParse()V
.end method

.method protected abstract onSuccessfulParse()V
.end method

.method public submit()V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mPassageSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    .line 118
    .local v9, "numSegments":I
    const/4 v10, 0x0

    .line 119
    .local v10, "segmentIndexInPassage":I
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->mPassageSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Segment;

    .line 120
    .local v2, "segment":Lcom/google/android/apps/books/model/Segment;
    add-int/lit8 v11, v10, 0x1

    .end local v10    # "segmentIndexInPassage":I
    .local v11, "segmentIndexInPassage":I
    move v12, v10

    .line 121
    .local v12, "thisSegmentIndexInPassage":I
    new-instance v3, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;

    invoke-direct {v3, p0, v12, v9}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval$1;-><init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;II)V

    .line 138
    .local v3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$400(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ItemRetrieval;->this$0:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;->access$000(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object v5, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->getSegmentContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    move v10, v11

    .line 140
    .end local v11    # "segmentIndexInPassage":I
    .restart local v10    # "segmentIndexInPassage":I
    goto :goto_0

    .line 141
    .end local v2    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v3    # "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    .end local v12    # "thisSegmentIndexInPassage":I
    :cond_0
    return-void
.end method
