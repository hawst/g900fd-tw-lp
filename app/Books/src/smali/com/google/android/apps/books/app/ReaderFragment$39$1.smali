.class Lcom/google/android/apps/books/app/ReaderFragment$39$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$39;->onAnimationEnd(Landroid/view/animation/Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$39;

.field final synthetic val$fadeCoverFrame:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$39;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 8303
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$39;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->val$fadeCoverFrame:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 8309
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->val$fadeCoverFrame:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8313
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$39;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8314
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$39;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/widget/PagesView;->setExecutingInitialLoadTransition(Z)V

    .line 8317
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$39;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 8318
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$39$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$39;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$39;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->setExecutingInitialLoadTransition(Z)V

    .line 8320
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 8323
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 8305
    return-void
.end method
