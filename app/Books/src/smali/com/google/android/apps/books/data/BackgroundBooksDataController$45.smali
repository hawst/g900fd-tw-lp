.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$fromServer:Z

.field final synthetic val$myEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0

    .prologue
    .line 1310
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-boolean p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$fromServer:Z

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$myEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mMyEbooksSubcontroller:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$fromServer:Z

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$myEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->getMyEbooks(Lcom/google/android/apps/books/data/ControlTaskServices;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 1315
    return-void
.end method
