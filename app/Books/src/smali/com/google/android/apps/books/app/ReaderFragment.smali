.class public Lcom/google/android/apps/books/app/ReaderFragment;
.super Lcom/google/android/apps/books/app/BooksFragment;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;
.implements Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReaderFragment$42;,
        Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;,
        Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;,
        Lcom/google/android/apps/books/app/ReaderFragment$EnsureClipsTaskFactory;,
        Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;,
        Lcom/google/android/apps/books/app/ReaderFragment$MoReader;,
        Lcom/google/android/apps/books/app/ReaderFragment$VolumeAccessException;,
        Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;,
        Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;,
        Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;,
        Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;,
        Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;,
        Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;,
        Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;,
        Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;,
        Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;,
        Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;,
        Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;,
        Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;,
        Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;,
        Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;,
        Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;,
        Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;,
        Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;,
        Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final FETCHED_LAYERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TEXTVIEWS_FOR_SEPIA:[I

.field private static final TRANSLATION_RESTRICTED_PUBLISHER_PATTERNS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field

.field private static final VIEWS_FOR_SEPIA:[I

.field private static sAveragePerformanceTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

.field private static sInstanceNumber:I


# instance fields
.field private mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

.field private mAccessibilityEnabled:Z

.field private mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

.field private mAnnotationEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

.field private final mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

.field private mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

.field private mAnnouncePositionTask:Ljava/lang/Runnable;

.field private final mAudioViewPlaying:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAutoTtsWhenScreenReading:Z

.field private mBackgroundColor:I

.field private mBookContentFormats:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;",
            ">;"
        }
    .end annotation
.end field

.field private mBookView:Lcom/google/android/apps/books/widget/BookView;

.field private final mBookmarkController:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/app/BookmarkController;",
            ">;"
        }
    .end annotation
.end field

.field private mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

.field private mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;

.field private final mConnReceiver:Landroid/content/BroadcastReceiver;

.field private final mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/UnloadableEventual",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;>;"
        }
    .end annotation
.end field

.field private mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

.field private mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

.field private mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

.field private mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

.field private mDismissSystemUI:Ljava/lang/Runnable;

.field private mDisplayedInitialPosition:Z

.field private mDownloadedManifest:Z

.field private mEOBCardsShown:Z

.field private mEncounteredFatalError:Z

.field private mEndOfBookBodyRecommendationsDismissed:Z

.field private final mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

.field private final mEventualAnnotationController:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeAnnotationController;",
            ">;"
        }
    .end annotation
.end field

.field private final mEventualMetadata:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            ">;"
        }
    .end annotation
.end field

.field private mEventualReadingView:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mExecutingInitialLoadTransition:Z

.field private mExitSearchListener:Landroid/view/View$OnClickListener;

.field private mExpireRentalHandler:Landroid/os/Handler;

.field private mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

.field private mGeoLayerEnabled:Z

.field private mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

.field private final mHeldReferences:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mIgnoreTouchesHorizontalMargin:F

.field private mInHoverGracePeriod:Z

.field private mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

.field private final mInfoCardsHelperCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

.field private mIsViewingSearchResult:Z

.field private mJustUndidJump:Z

.field private final mKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

.field private mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mLastOnStopTime:J

.field private mLastPauseWasConfigurationChange:Z

.field private mLastServerAccessShown:J

.field private mLastServerPositionShown:Lcom/google/android/apps/books/common/Position;

.field private mLastSpokenTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

.field private mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mLastStartedTurnFromUser:Z

.field private final mLayerAnnotationLoaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/geo/LayerAnnotationLoader;",
            ">;"
        }
    .end annotation
.end field

.field private mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mMainTouchAreaInsets:Landroid/graphics/Rect;

.field private mMatchDescriptionOnClickListener:Landroid/view/View$OnClickListener;

.field private mMaxQuickBookmarksShown:I

.field private mMediaPlaybackCanceled:Z

.field private final mMediaViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

.field private final mMenuCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

.field private mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

.field private mMoEnabled:Z

.field private mMoPlaybackEnabled:Z

.field private mMoStartTime:J

.field private mNeedReadingPositionSync:Z

.field private mNonZeroViewSize:Landroid/graphics/Point;

.field private mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

.field private final mNotificationBlock:Ljava/lang/String;

.field private mPagesView:Lcom/google/android/apps/books/widget/PagesView;

.field private mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

.field private final mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

.field private mPassageCount:I

.field private mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

.field private mPlaceCardProvider:Lcom/google/android/apps/books/geo/PlaceCardProvider;

.field private final mPreviousNextSearchResultListener:Landroid/view/View$OnClickListener;

.field private mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

.field private mReaderTheme:Ljava/lang/String;

.field private final mReadingAccessDelegate:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

.field private mReadingPositionFetchInProgress:Z

.field private mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

.field private mRecommendationLoader:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

.field private mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private final mRequestedVolumeDownloadFormats:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;

.field private final mResumeTtsHandler:Landroid/os/Handler;

.field mSavedLicenseAction:Z

.field final mScaleAnimationCallback:Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

.field private mScheduledPositionAnnouncement:Z

.field private mScreenBrightLock:Landroid/os/PowerManager$WakeLock;

.field private final mScreenTimeout:Ljava/lang/Runnable;

.field private mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

.field private mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

.field private mScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private mSearchModeActive:Z

.field private mSearchQuery:Ljava/lang/String;

.field private mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

.field private mSearchResultView:Landroid/view/View;

.field private mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

.field private mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

.field private mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private mSearchSequenceNumber:I

.field private final mSearchesConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mSelectionState:Lcom/google/android/apps/books/app/SelectionState;

.field private final mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

.field private mServerAccess:J

.field private mServerPosition:Lcom/google/android/apps/books/common/Position;

.field private mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

.field private final mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

.field private mShowingActionBar:Z

.field private mShowingEobPage:Z

.field private mShowingSearchResultsList:Z

.field private mShowingSettings:Z

.field private mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private final mSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end field

.field private mStartOfSkimPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mStartedDownloadProgressLoad:Z

.field private mStartedLoadingMetadata:Z

.field private mStatusBarHeight:I

.field private mStillNeedToShowDisplaySettings:Z

.field private mSyncController:Lcom/google/android/apps/books/sync/SyncController;

.field private final mSynthesizerFactory:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;

.field private final mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

.field private mSystemUi:Lcom/google/android/ublib/view/SystemUi;

.field private final mSystemUiListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

.field private mSystemUiVisible:Z

.field private mTOCActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

.field private mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

.field private mTopView:Landroid/view/View;

.field private mTransitionCover:Landroid/widget/ImageView;

.field private mTransitionCoverFrame:Landroid/view/View;

.field private mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

.field private mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

.field private mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

.field private mTtsEnabled:Z

.field private mTtsEnabledSetByUser:Z

.field private mTtsStartTime:J

.field private mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

.field private mUpdatedLastLocalAccess:Z

.field private mUsableContentFormats:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;",
            ">;"
        }
    .end annotation
.end field

.field private mUserInteractedWithCurrentUiMode:Z

.field private final mUtilityHandler:Landroid/os/Handler;

.field private mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

.field private mVerticalRut:I

.field private mViewLastBottom:I

.field private mViewLastLeft:I

.field private mViewLastRight:I

.field private mViewLastTop:I

.field private mVolumeDownKeyIsUp:Z

.field private mVolumeDownloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

.field private mVolumeUpKeyIsUp:Z

.field private final mVolumeUsageKey:Ljava/lang/Object;

.field private mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

.field private mWakeLockHandler:Landroid/os/Handler;

.field private final mWhenResumed:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mZoomEnabled:Z

.field private mZoomMaxSpan:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 425
    new-array v0, v4, [I

    const v1, 0x7f0e01f4

    aput v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/app/ReaderFragment;->TEXTVIEWS_FOR_SEPIA:[I

    .line 430
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/app/ReaderFragment;->VIEWS_FOR_SEPIA:[I

    .line 436
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v4, [Ljava/util/regex/Pattern;

    const-string v2, ".*mcgraw.*hill.*"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/apps/books/app/ReaderFragment;->TRANSLATION_RESTRICTED_PUBLISHER_PATTERNS:Ljava/util/Set;

    .line 440
    sput v3, Lcom/google/android/apps/books/app/ReaderFragment;->sInstanceNumber:I

    .line 7978
    new-array v0, v5, [Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    aput-object v1, v0, v4

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/ReaderFragment;->FETCHED_LAYERS:Ljava/util/List;

    return-void

    .line 430
    :array_0
    .array-data 4
        0x7f0e01f5
        0x7f0e01f3
        0x7f0e01f6
    .end array-data
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksFragment;-><init>()V

    .line 442
    new-instance v0, Lcom/google/android/apps/books/app/SystemBarManager;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/SystemBarManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    .line 451
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;

    .line 468
    new-instance v0, Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/ScrubBarController;-><init>(Lcom/google/android/apps/books/widget/ScrubBar$OnScrubListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    .line 480
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualReadingView:Lcom/google/android/apps/books/util/Eventual;

    .line 493
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    .line 505
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWhenResumed:Ljava/util/Queue;

    .line 508
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualMetadata:Lcom/google/android/apps/books/util/Eventual;

    .line 511
    const-string v0, "reading"

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNotificationBlock:Ljava/lang/String;

    .line 512
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUpdatedLastLocalAccess:Z

    .line 576
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    .line 578
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEOBCardsShown:Z

    .line 588
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mZoomEnabled:Z

    .line 595
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastOnStopTime:J

    .line 597
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z

    .line 620
    sget-object v0, Lcom/google/android/apps/books/app/SelectionState;->NONE:Lcom/google/android/apps/books/app/SelectionState;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;

    .line 676
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$2;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mConnReceiver:Landroid/content/BroadcastReceiver;

    .line 691
    new-instance v0, Lcom/google/android/apps/books/app/SelectionUiHelper;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReaderFragment$3;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    sget-object v2, Lcom/google/android/apps/books/annotations/AnnotationUtils;->DRAWING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    .line 846
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGeoLayerEnabled:Z

    .line 881
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    .line 886
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    .line 890
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z

    .line 905
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    .line 910
    iput-wide v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsStartTime:J

    .line 911
    iput-wide v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoStartTime:J

    .line 917
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z

    .line 934
    invoke-static {}, Lcom/google/android/apps/books/util/UnloadableEventual;->create()Lcom/google/android/apps/books/util/UnloadableEventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;

    .line 950
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    .line 954
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageKey:Ljava/lang/Object;

    .line 958
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualAnnotationController:Lcom/google/android/apps/books/util/Eventual;

    .line 977
    const v0, -0xffff01

    iput v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I

    .line 992
    const-class v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRequestedVolumeDownloadFormats:Ljava/util/EnumSet;

    .line 1008
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 1014
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 1021
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mJustUndidJump:Z

    .line 1026
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUtilityHandler:Landroid/os/Handler;

    .line 1039
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$4;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 1277
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLayerAnnotationLoaders:Ljava/util/Map;

    .line 1316
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment$InfoCardsHelperCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelperCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    .line 1994
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    .line 2008
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$13;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$13;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDismissSystemUI:Ljava/lang/Runnable;

    .line 2592
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    .line 3188
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$16;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$16;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExitSearchListener:Landroid/view/View$OnClickListener;

    .line 3200
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$17;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$17;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMatchDescriptionOnClickListener:Landroid/view/View$OnClickListener;

    .line 3337
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAnnotationListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    .line 3339
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 3790
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$18;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$18;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingAccessDelegate:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    .line 3981
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mHeldReferences:Ljava/util/WeakHashMap;

    .line 4305
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenuCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    .line 4418
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$22;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$22;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPreviousNextSearchResultListener:Landroid/view/View$OnClickListener;

    .line 4694
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$24;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$24;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenTimeout:Ljava/lang/Runnable;

    .line 6256
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSettings:Z

    .line 6259
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStillNeedToShowDisplaySettings:Z

    .line 6261
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$32;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$32;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    .line 7183
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$33;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$33;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScaleAnimationCallback:Lcom/google/android/apps/books/util/ConstrainedScaleScroll$AnimationCallback;

    .line 7588
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPassageCount:I

    .line 7590
    sget-object v0, Lcom/google/android/apps/books/tts/TtsUnit;->SENTENCE:Lcom/google/android/apps/books/tts/TtsUnit;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    .line 7714
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$36;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$36;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSynthesizerFactory:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;

    .line 7861
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    invoke-direct {v0, v5}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 8125
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createResumeTtsHandler(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mResumeTtsHandler:Landroid/os/Handler;

    .line 8158
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z

    .line 8159
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z

    .line 8207
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAudioViewPlaying:Ljava/util/Set;

    .line 8446
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$40;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$40;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    .line 8539
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$41;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$41;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeExpireRental()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;

    return-object v0
.end method

.method static synthetic access$10000(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->startMoAtCurrentPosition(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    return-void
.end method

.method static synthetic access$10100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    return-void
.end method

.method static synthetic access$10200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStart(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V

    return-void
.end method

.method static synthetic access$10300(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V

    return-void
.end method

.method static synthetic access$10400(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStopped()V

    return-void
.end method

.method static synthetic access$10500(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V

    return-void
.end method

.method static synthetic access$10600(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    return v0
.end method

.method static synthetic access$10602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    return p1
.end method

.method static synthetic access$10702(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    return p1
.end method

.method static synthetic access$10800(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onInternalUserInteraction()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/InfoCardsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    return-object v0
.end method

.method static synthetic access$11000(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V

    return-void
.end method

.method static synthetic access$11100(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$11200(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchSequenceNumber:I

    return v0
.end method

.method static synthetic access$11300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/ReaderFragment;->addSearchResultBatch(Ljava/util/List;ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$11400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualReadingView:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method static synthetic access$11500(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenBrightLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$11700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$11800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    return-object v0
.end method

.method static synthetic access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;

    return-object v0
.end method

.method static synthetic access$11902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/SelectionState;)Lcom/google/android/apps/books/app/SelectionState;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$12000(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mZoomEnabled:Z

    return v0
.end method

.method static synthetic access$12200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->firstAnnotationInSelection()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    return-object v0
.end method

.method static synthetic access$12400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    return-object v0
.end method

.method static synthetic access$12500(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->displayTwoPages()Z

    move-result v0

    return v0
.end method

.method static synthetic access$12600()Ljava/util/Set;
    .locals 1

    .prologue
    .line 302
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment;->TRANSLATION_RESTRICTED_PUBLISHER_PATTERNS:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$12700(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/common/Position;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->navigateToLinkPosition(Lcom/google/android/apps/books/common/Position;)V

    return-void
.end method

.method static synthetic access$12800(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z

    return v0
.end method

.method static synthetic access$12802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z

    return p1
.end method

.method static synthetic access$12900(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getShareUrl(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13000(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$13100(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Landroid/support/v7/view/ActionMode$Callback;

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->startActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13202(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSettings:Z

    return p1
.end method

.method static synthetic access$13300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    return-void
.end method

.method static synthetic access$13400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->saveReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    return-void
.end method

.method static synthetic access$13500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .param p2, "x2"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    return-void
.end method

.method static synthetic access$13600(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->applySettingsIfChanged(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$13700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    return-object v0
.end method

.method static synthetic access$13800(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cycleUiMode()V

    return-void
.end method

.method static synthetic access$13900(Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)Landroid/os/Handler$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    .prologue
    .line 302
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createScrollCallback(Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)Landroid/os/Handler$Callback;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAuthor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14000(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->isEdgeTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    return-object v0
.end method

.method static synthetic access$14200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    return-object v0
.end method

.method static synthetic access$14300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "x2"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    return-void
.end method

.method static synthetic access$14400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/TouchableItem;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->handleVideoItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V

    return-void
.end method

.method static synthetic access$14500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/TouchableItem;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->handleLinkItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V

    return-void
.end method

.method static synthetic access$14600(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/TouchableItem;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->handleAudioItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V

    return-void
.end method

.method static synthetic access$14700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$14800(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVerticalRut:I

    return v0
.end method

.method static synthetic access$14900(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onZoomChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$15000(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mZoomMaxSpan:I

    return v0
.end method

.method static synthetic access$15100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/common/Position;J)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "x2"    # J

    .prologue
    .line 302
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeHandleServerPosition(Lcom/google/android/apps/books/common/Position;J)V

    return-void
.end method

.method static synthetic access$15202(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingPositionFetchInProgress:Z

    return p1
.end method

.method static synthetic access$15300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    return-object v0
.end method

.method static synthetic access$15400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$15500(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onLoadedVolumeMetadata(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method

.method static synthetic access$15600(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onLoadedVolumeMetadataAndResumed(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method

.method static synthetic access$15700(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$15800(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    return v0
.end method

.method static synthetic access$15900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    return-object v0
.end method

.method static synthetic access$15902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenuCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    return-object v0
.end method

.method static synthetic access$16000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TextToSpeechController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    return-object v0
.end method

.method static synthetic access$16200(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInHoverGracePeriod:Z

    return p1
.end method

.method static synthetic access$16700(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePausePlayback(Z)V

    return-void
.end method

.method static synthetic access$16800(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    return v0
.end method

.method static synthetic access$16900(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->endHoverGracePeriodDelayed()V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    return-object v0
.end method

.method static synthetic access$17000(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTransitionCoverFrame()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$17202(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeDownloadProgress;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    return-object p1
.end method

.method static synthetic access$17300(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetUsableContentFormats()V

    return-void
.end method

.method static synthetic access$17400(I)Z
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 302
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isVolumeKey(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$17500(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isVolumeKeyPageTurnsEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$17600(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAnyAudioPlaying()Z

    move-result v0

    return v0
.end method

.method static synthetic access$17700(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowVolumeKeyPageTurnDialog()V

    return-void
.end method

.method static synthetic access$17800(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->restartTts()V

    return-void
.end method

.method static synthetic access$17900(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z

    return v0
.end method

.method static synthetic access$17902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUpKeyIsUp:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowRecentSearchesPopup(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$18000(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isNotTurning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$18100(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z

    return v0
.end method

.method static synthetic access$18102(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownKeyIsUp:Z

    return p1
.end method

.method static synthetic access$18200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/tts/TtsUnit;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    return-object v0
.end method

.method static synthetic access$18202(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/TtsUnit;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/tts/TtsUnit;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    return-object p1
.end method

.method static synthetic access$18302(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mJustUndidJump:Z

    return p1
.end method

.method static synthetic access$18400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearJumpBit(Lcom/google/android/apps/books/app/MoveType;)V

    return-void
.end method

.method static synthetic access$18500(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V

    return-void
.end method

.method static synthetic access$18600(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "x2"    # Ljava/lang/Boolean;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowEOBBRecommendations(Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$18700(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v0

    return v0
.end method

.method static synthetic access$18802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNeedReadingPositionSync:Z

    return p1
.end method

.method static synthetic access$18900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    return-object v0
.end method

.method static synthetic access$18902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDownloadedManifest:Z

    return v0
.end method

.method static synthetic access$19000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    return-object v0
.end method

.method static synthetic access$19002(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    return-object p1
.end method

.method static synthetic access$1902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDownloadedManifest:Z

    return p1
.end method

.method static synthetic access$19100(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageKey:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$19200(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeRequestVolumeDownload()V

    return-void
.end method

.method static synthetic access$19300(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGeoLayerEnabled:Z

    return v0
.end method

.method static synthetic access$19400(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    return-void
.end method

.method static synthetic access$19502(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastStartedTurnFromUser:Z

    return p1
.end method

.method static synthetic access$19600(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUnPauseMo()V

    return-void
.end method

.method static synthetic access$19700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/tts/TextToSpeechController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$19800(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePauseMo(Z)V

    return-void
.end method

.method static synthetic access$19900(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onShowedCards(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onDeviceConnectionChanged(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    return-object v0
.end method

.method static synthetic access$20000(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->textSelectionHasEnded()V

    return-void
.end method

.method static synthetic access$20100(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->showEndOfBookCards(Z)V

    return-void
.end method

.method static synthetic access$20200(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetZoom()V

    return-void
.end method

.method static synthetic access$20300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V

    return-void
.end method

.method static synthetic access$20400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 302
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    return-void
.end method

.method static synthetic access$20600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SearchResultMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    return-object v0
.end method

.method static synthetic access$20700(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I

    return v0
.end method

.method static synthetic access$20800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/ScrubBarController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    return-object v0
.end method

.method static synthetic access$20900(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isTtsSpeaking()Z

    move-result v0

    return v0
.end method

.method static synthetic access$21000(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setSystemBarsVisible(Z)V

    return-void
.end method

.method static synthetic access$21100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setBarVisibilityFromMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    return-void
.end method

.method static synthetic access$21200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEOBCardsShown:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEOBCardsShown:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    return p1
.end method

.method static synthetic access$2602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SystemBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/graphics/Rect;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->updateMainTouchAreaInsets(Landroid/graphics/Rect;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastLeft:I

    return v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastTop:I

    return v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastRight:I

    return v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/books/app/ReaderFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastBottom:I

    return v0
.end method

.method static synthetic access$3500(Lcom/google/android/apps/books/app/ReaderFragment;IIII)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 302
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/app/ReaderFragment;->onViewSizeChanged(IIII)V

    return-void
.end method

.method static synthetic access$3600(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->purchaseBook()V

    return-void
.end method

.method static synthetic access$3802(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->dismissActiveSelection()V

    return-void
.end method

.method static synthetic access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    return-void
.end method

.method static synthetic access$4200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/TextLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V

    return-void
.end method

.method static synthetic access$4500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4600(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z

    return v0
.end method

.method static synthetic access$4602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z

    return p1
.end method

.method static synthetic access$4700(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowSystemUi(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4800(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDismissSystemUI:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowActionBar()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5100(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setActionBarVisible(Z)V

    return-void
.end method

.method static synthetic access$5400(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdatePriceInScrubBar()V

    return-void
.end method

.method static synthetic access$5600(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TranslateViewController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTranslateViewController()Lcom/google/android/apps/books/widget/TranslateViewController;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6000(Lcom/google/android/apps/books/app/ReaderFragment;Z)Landroid/view/View$OnHoverListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->createPagesViewOnHoverListener(Z)Landroid/view/View$OnHoverListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6400(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V

    return-void
.end method

.method static synthetic access$6600(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6700(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowErrorDialog()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Config;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6900(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Landroid/support/v4/app/Fragment;
    .param p2, "x2"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p3, "x3"    # Ljava/lang/Exception;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/ReaderFragment;->showFatalErrorDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/ublib/view/SystemUi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$7100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createBookmarkListener()Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7200(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/Object;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->keepAliveAsLong(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$7300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/List;

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->handleBookmarkAccessibility(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$7400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/UnloadableEventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateQuickBookmarks()V

    return-void
.end method

.method static synthetic access$7600(Lcom/google/android/apps/books/app/ReaderFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTtsEnabled(Z)V

    return-void
.end method

.method static synthetic access$7700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7702(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7800(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingActionBar:Z

    return v0
.end method

.method static synthetic access$7900(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z

    return v0
.end method

.method static synthetic access$7902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowRecentSearchesPopup()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8100(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->requestRecentSearches()V

    return-void
.end method

.method static synthetic access$8200(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearSearchMatches()V

    return-void
.end method

.method static synthetic access$8300(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    return-void
.end method

.method static synthetic access$8400(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->startSearch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method static synthetic access$8600(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->backOutOfSearch()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8900(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->showingSearchResults()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TableOfContents;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    return-object v0
.end method

.method static synthetic access$9100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getOtherMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    return-object v0
.end method

.method static synthetic access$9400(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    return-object v0
.end method

.method static synthetic access$9502(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/widget/BookView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/BookView;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    return-object p1
.end method

.method static synthetic access$9600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Eventual;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method static synthetic access$9700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9800(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z

    return v0
.end method

.method static synthetic access$9802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z

    return p1
.end method

.method static synthetic access$9900(Lcom/google/android/apps/books/app/ReaderFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 302
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isMoSpeaking()Z

    move-result v0

    return v0
.end method

.method private addSearchResultBatch(Ljava/util/List;ZLjava/lang/String;)V
    .locals 5
    .param p2, "isDone"    # Z
    .param p3, "chapterHeading"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 4562
    .local p1, "newResults":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SearchResult;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/SearchResult;

    .line 4563
    .local v3, "searchResult":Lcom/google/android/apps/books/model/SearchResult;
    invoke-virtual {v3}, Lcom/google/android/apps/books/model/SearchResult;->getAnchorLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v4

    iget-object v2, v4, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    .line 4564
    .local v2, "position":Lcom/google/android/apps/books/common/Position;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v1

    .line 4567
    .local v1, "passageIndex":I
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v4, :cond_0

    .line 4577
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->clearSearchMatchHighlights(I)V

    .line 4580
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v4, v3}, Lcom/google/android/apps/books/app/SearchResultMap;->addSearchResult(Lcom/google/android/apps/books/model/SearchResult;)V

    goto :goto_0

    .line 4583
    .end local v1    # "passageIndex":I
    .end local v2    # "position":Lcom/google/android/apps/books/common/Position;
    .end local v3    # "searchResult":Lcom/google/android/apps/books/model/SearchResult;
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    invoke-virtual {v4, p3, p1, p2}, Lcom/google/android/apps/books/app/SearchResultsController;->addSearchResults(Ljava/lang/String;Ljava/util/List;Z)V

    .line 4584
    return-void
.end method

.method private adjustScrubberVisibility(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 6529
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isSelectionActive()Z

    move-result v3

    if-nez v3, :cond_2

    .line 6532
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-eqz v3, :cond_0

    move v0, v1

    .line 6535
    .local v0, "useSearchScrubber":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setSearchScrubberVisible(Z)V

    .line 6536
    if-nez v0, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setScrubberVisible(Z)V

    .line 6541
    .end local v0    # "useSearchScrubber":Z
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 6532
    goto :goto_0

    .restart local v0    # "useSearchScrubber":Z
    :cond_1
    move v1, v2

    .line 6536
    goto :goto_1

    .line 6538
    .end local v0    # "useSearchScrubber":Z
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->setScrubberVisible(Z)V

    .line 6539
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->setSearchScrubberVisible(Z)V

    goto :goto_2
.end method

.method private allowTtsForThisBook()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3649
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v1, :cond_0

    .line 3669
    :goto_0
    :pswitch_0
    return v0

    .line 3656
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$42;->$SwitchMap$com$google$android$apps$books$model$VolumeData$TtsPermission:[I

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeData;->getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3659
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 3662
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0

    .line 3656
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private applySettingsIfChanged(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 6118
    new-instance v0, Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTextZoomForVolume()F

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getLineHeightForVolume()F

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getValidFontValues()[Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/books/render/ReaderSettings;-><init>(Landroid/content/Context;FF[Ljava/lang/CharSequence;)V

    .line 6121
    .local v0, "settings":Lcom/google/android/apps/books/render/ReaderSettings;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/ReaderSettings;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 6122
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 6126
    :cond_0
    :goto_0
    return-void

    .line 6123
    :cond_1
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6124
    const-string v1, "ReaderFragment"

    const-string v2, "applySettings() skipping because settings already match"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private areSystemBarsVisible()Z
    .locals 2

    .prologue
    .line 6449
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 6450
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 6451
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->isShowing()Z

    move-result v1

    .line 6453
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private backOutOfSearch()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 4268
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSearchResultsList:Z

    if-nez v1, :cond_0

    .line 4269
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V

    .line 4270
    const/4 v0, 0x0

    .line 4273
    :goto_0
    return v0

    .line 4272
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->exitSearch()V

    goto :goto_0
.end method

.method private static buildContextSupportIntent(Lcom/google/android/apps/books/util/Config;)Landroid/content/Intent;
    .locals 3
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 5946
    invoke-static {p0}, Lcom/google/android/apps/books/util/OceanUris;->getContentBlockedUrl(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;

    move-result-object v0

    .line 5947
    .local v0, "contentBlockedUrl":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method private static buildMarketUpdateIntent(Lcom/google/android/apps/books/util/Config;)Landroid/content/Intent;
    .locals 4
    .param p0, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 5940
    const-string v1, "market://details"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/Config;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 5942
    .local v0, "marketUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method private canFitWidth()Z
    .locals 1

    .prologue
    .line 7396
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->canFitWidth()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canMoveToPosition()Z
    .locals 1

    .prologue
    .line 4897
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canSearch(Lcom/google/android/apps/books/model/VolumeMetadata;)Z
    .locals 2
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 3756
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_SEARCH_ON_UPLOADED_PDF:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3763
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v0

    .line 3767
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasReadableSections()Z

    move-result v0

    goto :goto_0
.end method

.method private cancelDismissSystemUI()V
    .locals 2

    .prologue
    .line 2018
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    if-eqz v0, :cond_0

    .line 2019
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDismissSystemUI:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2021
    :cond_0
    return-void
.end method

.method private cancelEndHoverGracePeriod()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8121
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mResumeTtsHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 8122
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInHoverGracePeriod:Z

    .line 8123
    return-void
.end method

.method private cancelPositionAnnouncement()Z
    .locals 2

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1873
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUtilityHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1874
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    .line 1875
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    .line 1876
    const/4 v0, 0x1

    .line 1878
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private clearMediaViews()V
    .locals 5

    .prologue
    .line 4876
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 4877
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/view/View;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4878
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 4879
    .local v2, "v":Landroid/view/View;
    instance-of v4, v2, Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    move-object v3, v2

    .line 4882
    check-cast v3, Landroid/webkit/WebView;

    .line 4883
    .local v3, "wv":Landroid/webkit/WebView;
    const-string v4, "about:blank"

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 4887
    .end local v3    # "wv":Landroid/webkit/WebView;
    :cond_1
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 4888
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 4889
    .local v0, "bookView":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 4890
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 4893
    .end local v0    # "bookView":Landroid/view/ViewGroup;
    .end local v2    # "v":Landroid/view/View;
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 4894
    return-void
.end method

.method private clearPagesViewController()V
    .locals 1

    .prologue
    .line 1687
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    .line 1688
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->stop()V

    .line 1690
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    .line 1691
    return-void
.end method

.method private clearPlaybackPosition()V
    .locals 1

    .prologue
    .line 8144
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePausePlayback(Z)V

    .line 8145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 8146
    return-void
.end method

.method private clearPlaybackState()V
    .locals 1

    .prologue
    .line 7835
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    if-eqz v0, :cond_0

    .line 7836
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    .line 7838
    :cond_0
    return-void
.end method

.method private clearSearchHighlights()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4390
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearSearchMatches()V

    .line 4392
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4416
    :goto_0
    return-void

    .line 4397
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-eqz v0, :cond_1

    .line 4398
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 4399
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showScrubber:Z

    if-eqz v0, :cond_1

    .line 4400
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setScrubberVisible(Z)V

    .line 4404
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v1, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v0, v1, :cond_2

    .line 4411
    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->setIsViewingSearchResult(Z)V

    .line 4415
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearPlaybackPosition()V

    goto :goto_0

    .line 4408
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    goto :goto_1
.end method

.method private clearSearchMatches()V
    .locals 1

    .prologue
    .line 4378
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    if-eqz v0, :cond_0

    .line 4379
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SearchResultMap;->clear()V

    .line 4381
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_1

    .line 4382
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->clearSearchMatchHighlights()V

    .line 4384
    :cond_1
    return-void
.end method

.method private closeTranslateViewController()V
    .locals 1

    .prologue
    .line 2266
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    if-eqz v0, :cond_0

    .line 2267
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TranslateViewController;->close()V

    .line 2268
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    .line 2270
    :cond_0
    return-void
.end method

.method private static createAnnouncementTask(Ljava/lang/String;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/app/ReaderFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1851
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$11;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private createBookmarkListener()Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
    .locals 1

    .prologue
    .line 3899
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$20;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$20;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-object v0
.end method

.method private createDataSource()Lcom/google/android/apps/books/app/ReadAlongController$DataSource;
    .locals 6

    .prologue
    .line 7779
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->USE_TTS_CONTENT_FROM_STORAGE:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7782
    new-instance v0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage;-><init>(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/widget/TtsLoadListener;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 7786
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$RendererReadAlongDataSource;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    goto :goto_0
.end method

.method private createDefaultFatalErrorDialog(I)Landroid/support/v4/app/Fragment;
    .locals 7
    .param p1, "messageId"    # I

    .prologue
    const/4 v1, 0x0

    .line 5973
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const v3, 0x7f0f00fe

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {}, Lcom/google/android/apps/books/app/ErrorFragment;->getFeedbackIntent()Landroid/content/Intent;

    move-result-object v6

    move-object v3, v1

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    return-object v0
.end method

.method private static createExpireRentalHandler(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;
    .locals 2
    .param p0, "readerFragment"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 525
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReaderFragment$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    return-object v0
.end method

.method private static createNoLinkCardsRunnable(Lcom/google/android/apps/books/render/TouchableItem;Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/LeakSafeRunnable;
    .locals 2
    .param p0, "item"    # Lcom/google/android/apps/books/render/TouchableItem;
    .param p1, "readerFragment"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            "Lcom/google/android/apps/books/app/ReaderFragment;",
            ")",
            "Lcom/google/android/apps/books/app/LeakSafeRunnable",
            "<",
            "Lcom/google/android/apps/books/app/ReaderFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5603
    new-instance v0, Lcom/google/android/apps/books/app/LeakSafeRunnable;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$28;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReaderFragment$28;-><init>(Lcom/google/android/apps/books/render/TouchableItem;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/books/app/LeakSafeRunnable;-><init>(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    return-object v0
.end method

.method private createPagesViewOnHoverListener(Z)Landroid/view/View$OnHoverListener;
    .locals 1
    .param p1, "isForEob"    # Z

    .prologue
    .line 7989
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$37;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$37;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    return-object v0
.end method

.method private static createResumeTtsHandler(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;
    .locals 2
    .param p0, "readerFragment"    # Lcom/google/android/apps/books/app/ReaderFragment;

    .prologue
    .line 8128
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$38;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/ReaderFragment$38;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    return-object v0
.end method

.method private static createScrollCallback(Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)Landroid/os/Handler$Callback;
    .locals 1
    .param p0, "listener"    # Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    .prologue
    .line 7200
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$34;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReaderFragment$34;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)V

    return-object v0
.end method

.method private cycleUiMode()V
    .locals 3

    .prologue
    .line 5533
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    sget-object v2, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v1, v2, :cond_2

    .line 5534
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    if-nez v1, :cond_1

    .line 5543
    :cond_0
    :goto_0
    return-void

    .line 5538
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 5542
    .local v0, "newMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    goto :goto_0

    .line 5540
    .end local v0    # "newMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    :cond_2
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .restart local v0    # "newMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    goto :goto_1
.end method

.method private disableQuickScaleOnKitKat(Landroid/view/ScaleGestureDetector;)V
    .locals 1
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 4751
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKat()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4752
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ScaleGestureDetector;->setQuickScaleEnabled(Z)V

    .line 4754
    :cond_0
    return-void
.end method

.method private dismissActiveSelection()V
    .locals 1

    .prologue
    .line 2469
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->dismissTextSelection()V

    .line 2471
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    .line 2472
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissAnnotationSelection()V

    .line 2474
    :cond_0
    return-void
.end method

.method private dismissTextSelection()V
    .locals 1

    .prologue
    .line 2458
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->isTextSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->dismissTextSelection()V

    .line 2466
    :goto_0
    return-void

    .line 2464
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    goto :goto_0
.end method

.method private displayTwoPages()Z
    .locals 1

    .prologue
    .line 7392
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private enableSearchScrubBar()V
    .locals 2

    .prologue
    .line 5189
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setScrubberVisible(Z)V

    .line 5191
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 5192
    return-void
.end method

.method private endHoverGracePeriodDelayed()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8110
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mResumeTtsHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 8111
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mResumeTtsHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 8112
    return-void
.end method

.method private firstAnnotationInSelection()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3

    .prologue
    .line 5546
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    sget-object v2, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/SelectionState;->getContainingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method private getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 7404
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private getAddToMyEBooks()Z
    .locals 1

    .prologue
    .line 2497
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getAddToMyEBooks(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    .locals 4

    .prologue
    .line 3295
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getContentDataSource()Lcom/google/android/apps/books/render/FetchingDataSource;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationContextSearchImpl;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/render/ReaderDataSource;Lcom/google/android/apps/books/model/SegmentSource;)V

    return-object v0
.end method

.method private getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;
    .locals 1

    .prologue
    .line 4625
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

    if-nez v0, :cond_0

    .line 4626
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->getForegroundAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

    .line 4628
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationEditor:Lcom/google/android/apps/books/annotations/UserChangesEditor;

    return-object v0
.end method

.method private getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8373
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAutoTtsWhenScreenReadingPreference()Z
    .locals 2

    .prologue
    .line 931
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAutoReadAloud()Z

    move-result v0

    return v0
.end method

.method private getBookView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 1557
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    if-eqz v0, :cond_0

    .line 1558
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getContentView()Landroid/view/ViewGroup;

    move-result-object v0

    .line 1560
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBuyUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
    .locals 1
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 3784
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v0, :cond_0

    .line 3785
    const/4 v0, 0x0

    .line 3787
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getBuyUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;

    move-result-object v0

    goto :goto_0
.end method

.method private getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .locals 1

    .prologue
    .line 7388
    invoke-static {p0}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private getCanonicalUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3776
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final getCloseBookReason(Ljava/lang/Exception;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .locals 1
    .param p0, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 5914
    instance-of v0, p0, Ljava/io/EOFException;

    if-eqz v0, :cond_0

    .line 5915
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EOF_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5927
    :goto_0
    return-object v0

    .line 5916
    :cond_0
    instance-of v0, p0, Ljava/util/zip/DataFormatException;

    if-eqz v0, :cond_1

    .line 5917
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->ZIP_DATA_FORMAT_EXCEPTION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_0

    .line 5918
    :cond_1
    instance-of v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$ResourceNotFoundException;

    if-eqz v0, :cond_2

    .line 5919
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->RESOURCE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_0

    .line 5920
    :cond_2
    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_3

    .line 5921
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->SOCKET_TIMEOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_0

    .line 5922
    :cond_3
    instance-of v0, p0, Ljava/io/FileNotFoundException;

    if-eqz v0, :cond_4

    .line 5923
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->FILE_NOT_FOUND:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_0

    .line 5924
    :cond_4
    instance-of v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeAccessException;

    if-eqz v0, :cond_5

    .line 5925
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->BAD_ACCESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_0

    .line 5927
    :cond_5
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OTHER:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_0
.end method

.method private getConfig()Lcom/google/android/apps/books/util/Config;
    .locals 2

    .prologue
    .line 5768
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 5769
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 5770
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    .line 5772
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getContentDataSource()Lcom/google/android/apps/books/render/FetchingDataSource;
    .locals 5

    .prologue
    .line 4632
    new-instance v0, Lcom/google/android/apps/books/render/FetchingDataSource;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/render/FetchingDataSource;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/util/Logger;)V

    return-object v0
.end method

.method private getCurrentPosition()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 3

    .prologue
    .line 3639
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eqz v0, :cond_0

    .line 3640
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 3644
    :goto_0
    return-object v0

    .line 3641
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_1

    .line 3642
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    goto :goto_0

    .line 3644
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDataController()Lcom/google/android/apps/books/data/BooksDataController;
    .locals 3

    .prologue
    .line 7565
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    if-nez v1, :cond_0

    .line 7566
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 7567
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 7568
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 7572
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v1
.end method

.method private getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 2

    .prologue
    .line 7556
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    return-object v0
.end method

.method private getDownloadProgress(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    .locals 1
    .param p1, "contentFormat"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 3362
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    if-nez v0, :cond_0

    .line 3363
    const/4 v0, 0x0

    .line 3365
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->getProgress(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v0

    goto :goto_0
.end method

.method private getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    .locals 4
    .param p1, "layerId"    # Ljava/lang/String;

    .prologue
    .line 8339
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLayerAnnotationLoaders:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    .line 8340
    .local v1, "loader":Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    if-nez v1, :cond_0

    .line 8341
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    move-result-object v0

    .line 8344
    .local v0, "annotationController":Lcom/google/android/apps/books/annotations/AnnotationController;
    new-instance v1, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    .end local v1    # "loader":Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    invoke-direct {v1, v2, p1, v0, v3}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/geo/AnnotationSet;)V

    .line 8347
    .restart local v1    # "loader":Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLayerAnnotationLoaders:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8349
    .end local v0    # "annotationController":Lcom/google/android/apps/books/annotations/AnnotationController;
    :cond_0
    return-object v1
.end method

.method private getLineHeightForVolume()F
    .locals 1

    .prologue
    .line 2912
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    if-eqz v0, :cond_0

    .line 2913
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    iget v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->lineHeight:F

    .line 2915
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3fc00000    # 1.5f

    goto :goto_0
.end method

.method private getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 8241
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getLocale()Ljava/util/Locale;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .locals 8
    .param p1, "createIfNecessary"    # Z

    .prologue
    .line 7754
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 7755
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7756
    const-string v0, "ReaderFragment"

    const-string v1, "getMoController creating new mMoController"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7759
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createDataSource()Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment$MoReader;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->displayTwoPages()Z

    move-result v3

    new-instance v4, Lcom/google/android/apps/books/app/ReaderFragment$EnsureClipsTaskFactory;

    invoke-direct {v4}, Lcom/google/android/apps/books/app/ReaderFragment$EnsureClipsTaskFactory;-><init>()V

    new-instance v5, Lcom/google/android/apps/books/app/AudioClipPlayer;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/apps/books/app/AudioClipPlayer;-><init>(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;-><init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$ControlledReader;ZLcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;Lcom/google/android/apps/books/data/BooksDataController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .line 7764
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPassageCount:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 7765
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPassageCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->setPassageCount(I)V

    .line 7767
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_3

    .line 7768
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->setMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 7775
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    return-object v0

    .line 7770
    :cond_3
    const-string v0, "ReaderFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 7771
    const-string v0, "ReaderFragment"

    const-string v1, "Metadata null upon MediaOverlaysController creation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getOtherFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .locals 1
    .param p1, "format"    # Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .prologue
    .line 2891
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2892
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    goto :goto_0
.end method

.method private getOtherMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 2
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 2884
    if-nez p1, :cond_0

    .line 2885
    const/4 v0, 0x0

    .line 2887
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getOtherFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getModeForContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    goto :goto_0
.end method

.method private getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;
    .locals 2

    .prologue
    .line 1574
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    .line 1575
    .local v0, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    if-eqz v0, :cond_0

    .line 1576
    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v1

    .line 1578
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getPagesViewsContainer()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 8414
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v1, 0x7f0e0112

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 5507
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v0, :cond_0

    .line 5508
    const/4 v0, -0x1

    .line 5510
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    goto :goto_0
.end method

.method private getPositionForMetadataPageIndex(I)Lcom/google/android/apps/books/common/Position;
    .locals 3
    .param p1, "metadataPageIndex"    # I

    .prologue
    .line 6154
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v1, :cond_0

    .line 6155
    const/4 v1, 0x0

    .line 6158
    :goto_0
    return-object v1

    .line 6157
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v0

    .line 6158
    .local v0, "pageId":Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/books/common/Position;

    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getPromptBeforeAddingToMyEbooks()Z
    .locals 1

    .prologue
    .line 2501
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getPromptBeforeAddingToMyEbooks(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private getQueryEmphasisColor()I
    .locals 3

    .prologue
    .line 4458
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f010198

    const v2, -0xffff01

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/util/ReaderUtils;->getColorFromAttr(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method private getReaderSettings()Lcom/google/android/apps/books/app/ReaderSettingsController;
    .locals 6

    .prologue
    .line 6230
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6231
    .local v0, "activity":Landroid/app/Activity;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-nez v3, :cond_1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v3, :cond_1

    .line 6232
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v4, "getReaderSettings"

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v2

    .line 6233
    .local v2, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    new-instance v3, Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/books/app/ReaderSettingsController;-><init>(Landroid/view/Window;Landroid/view/ViewGroup;)V

    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    .line 6235
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getLineHeightForVolume()F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setLineHeight(F)V

    .line 6237
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v1

    .line 6238
    .local v1, "readingMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v1, :cond_0

    .line 6239
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 6241
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->useMinimalFontSet()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setMinimalFontMenu(Z)V

    .line 6242
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setListener(Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;)V

    .line 6243
    invoke-interface {v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 6245
    .end local v1    # "readingMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .end local v2    # "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    return-object v3
.end method

.method private getShareUrl(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 7416
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    .line 7417
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/util/OceanUris;->getShareUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 7419
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getShouldAutoAdvanceTts()Z
    .locals 2

    .prologue
    .line 7858
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    sget-object v1, Lcom/google/android/apps/books/tts/TtsUnit;->SENTENCE:Lcom/google/android/apps/books/tts/TtsUnit;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSpeechIsAvailable()Z
    .locals 1

    .prologue
    .line 7854
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->canProvideText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTextZoomForVolume()F
    .locals 1

    .prologue
    .line 2901
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    if-eqz v0, :cond_0

    .line 2902
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    iget v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->textZoom:F

    .line 2904
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getDefaultTextZoom()F

    move-result v0

    goto :goto_0
.end method

.method private getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8366
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTransitionCover()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 8245
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCover:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 8246
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v1, 0x7f0e0115

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCover:Landroid/widget/ImageView;

    .line 8248
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCover:Landroid/widget/ImageView;

    return-object v0
.end method

.method private getTransitionCoverFrame()Landroid/view/View;
    .locals 2

    .prologue
    .line 8252
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCoverFrame:Landroid/view/View;

    if-nez v0, :cond_0

    .line 8253
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v1, 0x7f0e0114

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCoverFrame:Landroid/view/View;

    .line 8255
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCoverFrame:Landroid/view/View;

    return-object v0
.end method

.method private getTranslateViewController()Lcom/google/android/apps/books/widget/TranslateViewController;
    .locals 5

    .prologue
    .line 6178
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    if-nez v2, :cond_0

    .line 6180
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 6181
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f0a01ed

    invoke-direct {v1, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 6182
    .local v1, "lightContext":Landroid/content/Context;
    new-instance v2, Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/books/app/ReaderFragment$30;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/app/ReaderFragment$30;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-direct {v2, v1, v3, v4}, Lcom/google/android/apps/books/widget/TranslateViewController;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/google/android/apps/books/widget/TranslateViewController$Delegate;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    .line 6192
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$31;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/ReaderFragment$31;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/TranslateViewController;->setOnDismissListener(Lcom/google/android/apps/books/widget/TranslateViewController$OnDismissListener;)V

    .line 6205
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "lightContext":Landroid/content/Context;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    return-object v2
.end method

.method private getTranslationHelper()Lcom/google/android/ublib/view/TranslationHelper;
    .locals 1

    .prologue
    .line 1564
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    if-nez v0, :cond_0

    .line 1565
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 1567
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    return-object v0
.end method

.method private getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;
    .locals 7
    .param p1, "createIfNecessary"    # Z

    .prologue
    const/4 v6, 0x0

    .line 7725
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getSpeechIsAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 7726
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getUseNetworkTts()Z

    move-result v4

    .line 7727
    .local v4, "useNetworkTts":Z
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 7729
    .local v5, "altTextPrefix":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/tts/TextToSpeechController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createDataSource()Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSynthesizerFactory:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;

    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;

    invoke-direct {v3, p0, v6}, Lcom/google/android/apps/books/app/ReaderFragment$TtsReader;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/tts/TextToSpeechController;-><init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;ZLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    .line 7732
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPassageCount:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 7733
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPassageCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->setPassageCount(I)V

    .line 7736
    .end local v4    # "useNetworkTts":Z
    .end local v5    # "altTextPrefix":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    return-object v0

    .restart local v4    # "useNetworkTts":Z
    :cond_1
    move-object v5, v6

    .line 7727
    goto :goto_0
.end method

.method private getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1

    .prologue
    .line 6499
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    return-object v0
.end method

.method private getUpdateVolumeOverview()Z
    .locals 1

    .prologue
    .line 2493
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getUpdateVolumeOverview(Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private getValidFontValues()[Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2934
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->useMinimalFontSet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2935
    :cond_0
    const/4 v0, 0x0

    .line 2937
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method private getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1989
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 3

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    .line 2091
    new-instance v0, Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getContentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getWebViewContainer()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 8422
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v1, 0x7f0e0111

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1

    .prologue
    .line 8139
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/WritingDirection;->LEFT_TO_RIGHT:Lcom/google/android/apps/books/util/WritingDirection;

    goto :goto_0
.end method

.method private getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;
    .locals 2

    .prologue
    .line 6616
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-eqz v1, :cond_0

    .line 6617
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookView;->getPageSpread()Lcom/google/android/apps/books/widget/BaseSpreadView;

    move-result-object v0

    .line 6618
    .local v0, "mainSpread":Lcom/google/android/apps/books/widget/BaseSpreadView;
    if-eqz v0, :cond_0

    .line 6619
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BaseSpreadView;->getZoomHelper()Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    move-result-object v1

    .line 6622
    .end local v0    # "mainSpread":Lcom/google/android/apps/books/widget/BaseSpreadView;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleAudioItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 10
    .param p1, "item"    # Lcom/google/android/apps/books/render/TouchableItem;

    .prologue
    const/4 v6, 0x0

    .line 5616
    iput-boolean v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z

    .line 5619
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 5620
    .local v1, "activity":Landroid/app/Activity;
    if-nez v1, :cond_1

    .line 5650
    :cond_0
    :goto_0
    return-void

    .line 5624
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->mediaItemIsActive(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5627
    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    .line 5628
    .local v8, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f04002b

    const/4 v3, 0x0

    invoke-virtual {v8, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/widget/AudioView;

    .line 5629
    .local v5, "audioView":Lcom/google/android/apps/books/widget/AudioView;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_2

    .line 5630
    iget-object v0, p1, Lcom/google/android/apps/books/render/TouchableItem;->source:Ljava/lang/String;

    invoke-virtual {v5, p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->setListener(Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;Ljava/lang/String;)V

    .line 5632
    :cond_2
    iget-object v7, p1, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    .line 5633
    .local v7, "bounds":Landroid/graphics/Rect;
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-direct {v9, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5635
    .local v9, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v0, v7, Landroid/graphics/Rect;->left:I

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 5636
    iget v0, v7, Landroid/graphics/Rect;->top:I

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 5642
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v5, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5643
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5646
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v2

    .line 5647
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/google/android/apps/books/util/MediaUrlFetcher;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/util/MediaUrlFetcher;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/render/TouchableItem;Ljava/lang/String;Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;I)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/books/widget/AudioView;->setMediaUrlFetcher(Lcom/google/android/apps/books/util/MediaUrlFetcher;)V

    .line 5649
    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/AudioView;->startPlaying()V

    goto :goto_0
.end method

.method private handleBookmarkAccessibility(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "oldViewableBookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p2, "viewableBookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    const/4 v4, 0x0

    .line 3937
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAdded()Z

    move-result v5

    if-nez v5, :cond_1

    .line 3978
    :cond_0
    :goto_0
    return-void

    .line 3941
    :cond_1
    if-nez p1, :cond_3

    move v1, v4

    .line 3943
    .local v1, "oldBookmarksSize":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 3946
    .local v0, "newBookmarksSize":I
    iget-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    if-eqz v5, :cond_0

    if-eq v1, v0, :cond_0

    .line 3949
    if-le v1, v0, :cond_4

    const v2, 0x7f0f018f

    .line 3956
    .local v2, "stringId":I
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isTtsSpeaking()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3957
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTtsEnabled(Z)V

    .line 3964
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    new-instance v5, Lcom/google/android/apps/books/app/ReaderFragment$21;

    invoke-direct {v5, p0}, Lcom/google/android/apps/books/app/ReaderFragment$21;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->post(Ljava/lang/Runnable;)Z

    .line 3974
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 3975
    .local v3, "stringToSay":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/books/util/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 3976
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->temporarilyOverrideWindowAccessibilityAnnouncements(Ljava/lang/String;)V

    goto :goto_0

    .line 3941
    .end local v0    # "newBookmarksSize":I
    .end local v1    # "oldBookmarksSize":I
    .end local v2    # "stringId":I
    .end local v3    # "stringToSay":Ljava/lang/String;
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_1

    .line 3949
    .restart local v0    # "newBookmarksSize":I
    .restart local v1    # "oldBookmarksSize":I
    :cond_4
    const v2, 0x7f0f018e

    goto :goto_2
.end method

.method private handleLinkItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 8
    .param p1, "item"    # Lcom/google/android/apps/books/render/TouchableItem;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 5585
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/TouchableItem;->getUri()Landroid/net/Uri;

    move-result-object v7

    .line 5586
    .local v7, "url":Landroid/net/Uri;
    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    .line 5589
    .local v6, "scheme":Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v0, "file"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5590
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    iget v2, v1, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->textZoom:F

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    iget-object v3, v1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    iget v4, v1, Landroid/graphics/Point;->y:I

    invoke-static {p1, p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createNoLinkCardsRunnable(Lcom/google/android/apps/books/render/TouchableItem;Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/LeakSafeRunnable;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/app/InfoCardsHelper;->showLinkCards(Lcom/google/android/apps/books/render/TouchableItem;FLjava/lang/String;ILjava/lang/Runnable;)V

    .line 5599
    :cond_1
    :goto_0
    return-void

    .line 5592
    :cond_2
    const-string v0, "http"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5593
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 5595
    :cond_3
    const-string v0, "ReaderFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5596
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleLinkItemTap illegal link: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleVideoItemTap(Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 13
    .param p1, "item"    # Lcom/google/android/apps/books/render/TouchableItem;

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x2

    .line 5657
    iput-boolean v12, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z

    .line 5659
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 5660
    .local v1, "outerContext":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 5744
    :goto_0
    return-void

    .line 5663
    :cond_0
    new-instance v9, Landroid/widget/ProgressBar;

    invoke-direct {v9, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 5664
    .local v9, "progressBar":Landroid/widget/ProgressBar;
    new-instance v10, Landroid/widget/FrameLayout;

    invoke-direct {v10, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 5665
    .local v10, "progressBarContainer":Landroid/widget/FrameLayout;
    iget-object v7, p1, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    .line 5666
    .local v7, "bounds":Landroid/graphics/Rect;
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-direct {v8, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 5668
    .local v8, "overlayMediaElement":Landroid/widget/RelativeLayout$LayoutParams;
    iget v3, v7, Landroid/graphics/Rect;->left:I

    iput v3, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 5669
    iget v3, v7, Landroid/graphics/Rect;->top:I

    iput v3, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 5670
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 5671
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v4, 0x11

    invoke-direct {v3, v11, v11, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v10, v9, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5674
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3, v10, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5675
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5677
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v2

    .line 5678
    .local v2, "account":Landroid/accounts/Account;
    new-instance v5, Lcom/google/android/apps/books/app/ReaderFragment$29;

    invoke-direct {v5, p0, p1, v1, v10}, Lcom/google/android/apps/books/app/ReaderFragment$29;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/TouchableItem;Landroid/content/Context;Landroid/widget/FrameLayout;)V

    .line 5735
    .local v5, "mediaFoundListener":Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;
    const-string v3, "window"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v6

    .line 5739
    .local v6, "targetWidth":I
    new-instance v0, Lcom/google/android/apps/books/util/MediaUrlFetcher;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/util/MediaUrlFetcher;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/render/TouchableItem;Ljava/lang/String;Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;I)V

    .line 5742
    .local v0, "urlFetcher":Lcom/google/android/apps/books/util/MediaUrlFetcher;
    invoke-virtual {v9, v12}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 5743
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchMedia()V

    goto :goto_0
.end method

.method private hideDisplayOptions()V
    .locals 1

    .prologue
    .line 4606
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-eqz v0, :cond_0

    .line 4609
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->hide()V

    .line 4611
    :cond_0
    return-void
.end method

.method private inManualPageTurn()Z
    .locals 1

    .prologue
    .line 3561
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isTurning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastStartedTurnFromUser:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAnyAudioPlaying()Z
    .locals 1

    .prologue
    .line 8177
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAudioPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAudioPlaying()Z
    .locals 1

    .prologue
    .line 8222
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAudioViewPlaying:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEdgeTouch(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 4702
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 4703
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 4704
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 4705
    .local v2, "x":F
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 4706
    .local v1, "w":I
    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v4, v2

    iget v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mIgnoreTouchesHorizontalMargin:F

    cmpg-float v4, v4, v5

    if-lez v4, :cond_0

    int-to-float v4, v1

    sub-float/2addr v4, v2

    iget v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mIgnoreTouchesHorizontalMargin:F

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 4709
    .end local v1    # "w":I
    .end local v2    # "x":F
    :cond_1
    return v3
.end method

.method private isMoSpeaking()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 7794
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    if-nez v1, :cond_1

    .line 7797
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->isSpeaking()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isNotTurning()Z
    .locals 1

    .prologue
    .line 5122
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->isTurning()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isReaderSettingsVisible()Z
    .locals 1

    .prologue
    .line 6249
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSelectionActive()Z
    .locals 1

    .prologue
    .line 3565
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->isSelectionActive()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTtsSpeaking()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7845
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v0

    .line 7846
    .local v0, "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-nez v0, :cond_0

    .line 7849
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->isSpeaking()Z

    move-result v1

    goto :goto_0
.end method

.method private isTurning()Z
    .locals 1

    .prologue
    .line 5115
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->isTurning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewingSearchResult()Z
    .locals 1

    .prologue
    .line 4312
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mIsViewingSearchResult:Z

    return v0
.end method

.method private static isVolumeKey(I)Z
    .locals 1
    .param p0, "keyCode"    # I

    .prologue
    .line 8162
    const/16 v0, 0x18

    if-eq p0, v0, :cond_0

    const/16 v0, 0x19

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isVolumeKeyPageTurnsEnabled()Z
    .locals 3

    .prologue
    .line 8167
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 8168
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 8169
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 8170
    .local v1, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getVolumeKeyPageTurn()Z

    move-result v2

    .line 8172
    .end local v1    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private keepAliveAsLong(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 3995
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mHeldReferences:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3996
    return-void
.end method

.method private logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V
    .locals 6
    .param p1, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .param p2, "byUser"    # Z

    .prologue
    .line 6439
    if-eqz p2, :cond_0

    .line 6440
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->displayTwoPages()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/model/VolumeManifest$Mode;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 6443
    :cond_0
    return-void
.end method

.method private maybeAnnouncePosition(Lcom/google/android/apps/books/common/Position;)V
    .locals 12
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 1808
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScheduledPositionAnnouncement:Z

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v7, :cond_1

    .line 1848
    :cond_0
    :goto_0
    return-void

    .line 1812
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1813
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 1817
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScheduledPositionAnnouncement:Z

    .line 1819
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    if-eqz v7, :cond_0

    .line 1824
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v1

    .line 1825
    .local v1, "chapterIndex":I
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/model/Chapter;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 1827
    .local v2, "chapterTitle":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 1828
    .local v6, "pageTitle":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v8}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 1831
    .local v4, "lastPageTitle":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v2, v6, v4}, Lcom/google/android/apps/books/util/AccessibilityUtils;->getPositionDescription(Landroid/content/res/Resources;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 1833
    .local v5, "message":Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment;->createAnnouncementTask(Ljava/lang/String;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/google/android/ublib/utils/Runnables;->withWeakReference(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Ljava/lang/Runnable;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    .line 1835
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUtilityHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    const-wide/16 v10, 0x1f4

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1840
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1842
    .end local v1    # "chapterIndex":I
    .end local v2    # "chapterTitle":Ljava/lang/String;
    .end local v4    # "lastPageTitle":Ljava/lang/String;
    .end local v5    # "message":Ljava/lang/String;
    .end local v6    # "pageTitle":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1843
    .local v3, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    .line 1844
    const-string v7, "ReaderFragment"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1845
    const-string v7, "ReaderFragment"

    const-string v8, "Error announcing position"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private maybeClearJumpBit(Lcom/google/android/apps/books/app/MoveType;)V
    .locals 1
    .param p1, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 5055
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/MoveType;->shouldClearJumpBit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5058
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 5060
    :cond_0
    return-void
.end method

.method private maybeClearOffset(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 1
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 8440
    if-eqz p1, :cond_0

    .line 8441
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/SpreadIdentifier;->clearOffset()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 8443
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeComputeUsableContentFormats()V
    .locals 6

    .prologue
    .line 3388
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookContentFormats:Ljava/util/Set;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v5, :cond_1

    .line 3417
    :cond_0
    :goto_0
    return-void

    .line 3391
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 3392
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 3395
    invoke-static {v1}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v0

    .line 3396
    .local v0, "connected":Z
    if-eqz v0, :cond_3

    .line 3397
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookContentFormats:Ljava/util/Set;

    iput-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    .line 3410
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSelectReadingMode()V

    .line 3416
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    goto :goto_0

    .line 3398
    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeDownloadProgress:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    if-eqz v5, :cond_5

    .line 3399
    const-class v5, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {v5}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    .line 3400
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookContentFormats:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 3401
    .local v2, "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getDownloadProgress(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;

    move-result-object v4

    .line 3402
    .local v4, "progress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;->isFullyDownloaded()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 3403
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3407
    .end local v2    # "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "progress":Lcom/google/android/apps/books/model/ContentFormatDownloadProgress;
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLoadDownloadProgress()V

    goto :goto_1
.end method

.method private maybeCreateInfoCardsHelper()V
    .locals 21

    .prologue
    .line 1225
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    .line 1226
    .local v18, "activity":Landroid/app/Activity;
    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    if-nez v2, :cond_4

    .line 1228
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v19

    .line 1232
    .local v19, "cardProviders":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/InfoCardProvider;>;"
    new-instance v3, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f0a01ed

    move-object/from16 v0, v18

    invoke-direct {v3, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1235
    .local v3, "lightContext":Landroid/content/Context;
    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationServer(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationServer;

    move-result-object v5

    .line 1238
    .local v5, "annotationServer":Lcom/google/android/apps/books/annotations/AnnotationServer;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mGeoLayerEnabled:Z

    if-eqz v2, :cond_0

    .line 1239
    new-instance v2, Lcom/google/android/apps/books/geo/PlaceCardProvider;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/geo/PlaceCardProvider;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/annotations/VolumeAnnotationController;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPlaceCardProvider:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    .line 1240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPlaceCardProvider:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1243
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v9

    .line 1245
    .local v9, "offlineDictionaryEnabled":Z
    new-instance v2, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getLocale()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationServer;Ljava/util/Locale;Ljava/util/Locale;Lcom/google/android/apps/books/data/BooksDataController;Z)V

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1250
    if-eqz v9, :cond_1

    .line 1251
    new-instance v10, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getLocale()Ljava/util/Locale;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getForegroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v15

    move-object v11, v3

    invoke-direct/range {v10 .. v15}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;-><init>(Landroid/content/Context;Ljava/util/Locale;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/data/BooksDataController;Landroid/support/v4/app/FragmentManager;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1256
    :cond_1
    new-instance v10, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    new-instance v12, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v2}, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/render/ResourceContentStoreUtils;->makeResourceStore(Lcom/google/android/apps/books/data/BooksDataController;)Lcom/google/android/apps/books/render/ResourceContentStore;

    move-result-object v13

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v16

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getContentDataSource()Lcom/google/android/apps/books/render/FetchingDataSource;

    move-result-object v17

    move-object/from16 v11, v18

    invoke-direct/range {v10 .. v17}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;Lcom/google/android/apps/books/render/ResourceContentStore;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/render/ReaderDataSource;)V

    move-object/from16 v0, v19

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRecommendationLoader:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    if-nez v2, :cond_2

    .line 1262
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRecommendationLoader:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    .line 1265
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRecommendationLoader:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mayRequireProvider()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1266
    new-instance v20, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 1268
    .local v20, "endOfBookCardProvider":Lcom/google/android/apps/books/eob/EndOfBookCardProvider;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRecommendationLoader:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->setProvider(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)V

    .line 1269
    invoke-interface/range {v19 .. v20}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1272
    .end local v20    # "endOfBookCardProvider":Lcom/google/android/apps/books/eob/EndOfBookCardProvider;
    :cond_3
    new-instance v2, Lcom/google/android/apps/books/app/InfoCardsHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelperCallbacks:Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsView()Lcom/google/android/ublib/infocards/SuggestionGridLayout;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-direct {v2, v0, v4, v6}, Lcom/google/android/apps/books/app/InfoCardsHelper;-><init>(Ljava/util/List;Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;Lcom/google/android/ublib/infocards/SuggestionGridLayout;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    .line 1275
    .end local v3    # "lightContext":Landroid/content/Context;
    .end local v5    # "annotationServer":Lcom/google/android/apps/books/annotations/AnnotationServer;
    .end local v9    # "offlineDictionaryEnabled":Z
    .end local v19    # "cardProviders":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/InfoCardProvider;>;"
    :cond_4
    return-void
.end method

.method private maybeCreatePagesViewController()V
    .locals 15

    .prologue
    .line 1727
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    .line 1729
    .local v10, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    if-eqz v10, :cond_0

    .line 1730
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v12

    .line 1731
    .local v12, "displayTwoPages":Z
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->shouldFitWidth()Z

    move-result v11

    .line 1736
    .local v11, "displayFitWidth":Z
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0, v12, v11}, Lcom/google/android/apps/books/widget/PagesView;->setDisplayTwoPages(ZZ)V

    .line 1738
    invoke-direct {p0, v10}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldMakePagesAccessible(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v4, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;

    if-eqz v12, :cond_1

    const/4 v0, 0x2

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/books/reader/HiddenTextViewsAccessiblePages;-><init>(ILcom/google/android/apps/books/widget/PagesView;)V

    .line 1742
    .local v4, "selectionAutomator":Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->x:I

    if-eqz v12, :cond_3

    const/4 v0, 0x2

    :goto_2
    div-int v13, v1, v0

    .line 1743
    .local v13, "onePageWidth":I
    new-instance v8, Landroid/graphics/Point;

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v8, v13, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 1745
    .local v8, "onePageSize":Landroid/graphics/Point;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    iget-boolean v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getQueryEmphasisColor()I

    move-result v9

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/books/widget/PagesViewController;->create(Lcom/google/android/apps/books/widget/PagesView;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;Lcom/google/android/apps/books/util/Logger;ZLcom/google/android/apps/books/model/VolumeMetadata;Landroid/graphics/Point;I)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v14

    .line 1749
    .local v14, "pagesViewController":Lcom/google/android/apps/books/widget/PagesViewController;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v14, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->setDrawAnnotations(Z)V

    .line 1752
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$10;

    invoke-direct {v1, p0, v14}, Lcom/google/android/apps/books/app/ReaderFragment$10;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/widget/PagesViewController;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 1759
    iput-object v14, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    .line 1761
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePublishSpecialPageBitmaps()V

    .line 1763
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setupFullGestureDetector(Landroid/view/View;)V

    .line 1767
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToResumedPosition()V

    .line 1769
    .end local v4    # "selectionAutomator":Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;
    .end local v8    # "onePageSize":Landroid/graphics/Point;
    .end local v11    # "displayFitWidth":Z
    .end local v12    # "displayTwoPages":Z
    .end local v13    # "onePageWidth":I
    .end local v14    # "pagesViewController":Lcom/google/android/apps/books/widget/PagesViewController;
    :cond_0
    return-void

    .line 1738
    .restart local v11    # "displayFitWidth":Z
    .restart local v12    # "displayTwoPages":Z
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/google/android/apps/books/reader/AccessiblePagesStub;

    invoke-direct {v4}, Lcom/google/android/apps/books/reader/AccessiblePagesStub;-><init>()V

    goto :goto_1

    .line 1742
    .restart local v4    # "selectionAutomator":Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .line 1749
    .restart local v8    # "onePageSize":Landroid/graphics/Point;
    .restart local v13    # "onePageWidth":I
    .restart local v14    # "pagesViewController":Lcom/google/android/apps/books/widget/PagesViewController;
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private maybeCreateRenderer()V
    .locals 21

    .prologue
    .line 2820
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    .line 2822
    .local v19, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    if-eqz v2, :cond_0

    if-nez v19, :cond_1

    .line 2869
    :cond_0
    :goto_0
    return-void

    .line 2828
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    .line 2830
    .local v3, "account":Landroid/accounts/Account;
    new-instance v7, Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getHeight()I

    move-result v4

    invoke-direct {v7, v2, v4}, Landroid/graphics/Point;-><init>(II)V

    .line 2832
    .local v7, "screenSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldMakePagesAccessible(Landroid/content/Context;)Z

    move-result v8

    .line 2834
    .local v8, "makePagesAccessible":Z
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    .line 2835
    .local v20, "res":Landroid/content/res/Resources;
    const v2, 0x7f090156

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 2838
    .local v9, "marginNoteIconTapSize":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-eq v2, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v2, v4, :cond_4

    .line 2839
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getWebViewContainer()Landroid/view/ViewGroup;

    move-result-object v10

    .line 2840
    .local v10, "webViewParent":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->AFL_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v2, v4, :cond_3

    .line 2841
    new-instance v2, Lcom/google/android/apps/books/render/IframeRenderer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/books/render/IframeRenderer;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/util/Logger;Landroid/graphics/Point;ZILandroid/view/ViewGroup;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 2856
    .end local v10    # "webViewParent":Landroid/view/ViewGroup;
    :goto_1
    const-string v2, "RENDERERCALLS"

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 2859
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetPagesViewController()V

    .line 2861
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    .line 2865
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/BookView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksApplication;->isScreenReaderActive(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAutoTtsWhenScreenReading:Z

    if-eqz v2, :cond_0

    .line 2867
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTtsEnabled(Z)V

    goto/16 :goto_0

    .line 2845
    .restart local v10    # "webViewParent":Landroid/view/ViewGroup;
    :cond_3
    new-instance v2, Lcom/google/android/apps/books/render/WebViewRenderer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/books/render/WebViewRenderer;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/util/Logger;Landroid/graphics/Point;ZILandroid/view/ViewGroup;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    goto :goto_1

    .line 2849
    .end local v10    # "webViewParent":Landroid/view/ViewGroup;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v4, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v2, v4, :cond_5

    .line 2850
    new-instance v11, Lcom/google/android/apps/books/render/ImageModeRenderer;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    move-object/from16 v17, v0

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/books/render/ImageModeRenderer$Config;->fromContext(Landroid/content/Context;)Lcom/google/android/apps/books/render/ImageModeRenderer$Config;

    move-result-object v18

    move-object v15, v7

    move/from16 v16, v8

    invoke-direct/range {v11 .. v18}, Lcom/google/android/apps/books/render/ImageModeRenderer;-><init>(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Landroid/graphics/Point;ZLcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/render/ImageModeRenderer$Config;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    goto :goto_1

    .line 2853
    :cond_5
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unsupported reader mode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private maybeExpireRental()Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 5067
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    if-nez v3, :cond_1

    .line 5085
    :cond_0
    :goto_0
    return v2

    .line 5070
    :cond_1
    const-string v3, "ReaderFragment"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5071
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v0

    .line 5072
    .local v0, "expiration":J
    const-string v3, "ReaderFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "maybeExpireRental(): book expires in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v0, v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " seconds"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5076
    .end local v0    # "expiration":J
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/util/RentalUtils;->isExpired(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5077
    const-string v2, "ReaderFragment"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5078
    const-string v2, "ReaderFragment"

    const-string v3, "Rental period has expired"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5082
    :cond_3
    new-instance v2, Lcom/google/android/apps/books/util/BlockedContentReason$NonSampleExpiredRentalException;

    const-string v3, "expired rental"

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/util/BlockedContentReason$NonSampleExpiredRentalException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V

    .line 5083
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private maybeFetchLatestServerSideReadingPosition()V
    .locals 4

    .prologue
    .line 1971
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1972
    .local v0, "context":Landroid/content/Context;
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingPositionFetchInProgress:Z

    if-nez v2, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1973
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingPositionFetchInProgress:Z

    .line 1974
    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, p0}, Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;-><init>(Lcom/google/android/apps/books/net/BooksServer;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 1976
    .local v1, "task":Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v1, v2}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 1986
    .end local v1    # "task":Lcom/google/android/apps/books/app/ReaderFragment$MyFetchReadingPositionTask;
    :cond_0
    :goto_0
    return-void

    .line 1977
    :cond_1
    const-string v2, "ReaderFragment"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1980
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingPositionFetchInProgress:Z

    if-eqz v2, :cond_2

    .line 1981
    const-string v2, "ReaderFragment"

    const-string v3, "Skipping position fetch (already in progress)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1983
    :cond_2
    const-string v2, "ReaderFragment"

    const-string v3, "Skipping position fetch (not connected)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private maybeHandleServerPosition(Lcom/google/android/apps/books/common/Position;J)V
    .locals 20
    .param p1, "serverPosition"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "serverAccess"    # J

    .prologue
    .line 7324
    invoke-static/range {p1 .. p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7326
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v4, :cond_1

    .line 7328
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mServerPosition:Lcom/google/android/apps/books/common/Position;

    .line 7329
    move-wide/from16 v0, p2

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/books/app/ReaderFragment;->mServerAccess:J

    .line 7385
    :cond_0
    :goto_0
    return-void

    .line 7333
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v16

    .line 7334
    .local v16, "localPosition":Lcom/google/android/apps/books/common/Position;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getLastAccess()J

    move-result-wide v14

    .line 7337
    .local v14, "localAccess":J
    cmp-long v4, p2, v14

    if-lez v4, :cond_0

    .line 7341
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v17

    .line 7342
    .local v17, "serverPageId":Ljava/lang/String;
    if-nez v16, :cond_3

    const/4 v13, 0x0

    .line 7346
    .local v13, "localPageId":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, v17

    invoke-static {v0, v13}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 7350
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-nez v4, :cond_4

    const/16 v18, 0x0

    .line 7354
    .local v18, "visiblePageId":Ljava/lang/String;
    :goto_2
    invoke-static/range {v17 .. v18}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 7358
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastServerAccessShown:J

    cmp-long v4, v4, p2

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastServerPositionShown:Lcom/google/android/apps/books/common/Position;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 7368
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v11

    .line 7369
    .local v11, "chapterIndex":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapters()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/Chapter;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v8

    .line 7371
    .local v8, "chapterTitle":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/model/Page;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v9

    .line 7372
    .local v9, "pageTitle":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    .line 7373
    .local v10, "arguments":Landroid/os/Bundle;
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mLastServerPositionShown:Lcom/google/android/apps/books/common/Position;

    .line 7374
    move-wide/from16 v0, p2

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/books/app/ReaderFragment;->mLastServerAccessShown:J

    .line 7375
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v4

    invoke-static {v10}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v5

    invoke-static {v10}, Lcom/google/android/apps/books/util/LoaderParams;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v7, p1

    invoke-interface/range {v4 .. v9}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->showNewPositionAvailableDialog(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 7377
    .end local v9    # "pageTitle":Ljava/lang/String;
    .end local v10    # "arguments":Landroid/os/Bundle;
    :catch_0
    move-exception v12

    .line 7380
    .local v12, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v4, "ReaderFragment"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 7381
    const-string v4, "ReaderFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to find server reading position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in volume "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 7342
    .end local v8    # "chapterTitle":Ljava/lang/String;
    .end local v11    # "chapterIndex":I
    .end local v12    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .end local v13    # "localPageId":Ljava/lang/String;
    .end local v18    # "visiblePageId":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/common/Position;->extractPageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1

    .line 7350
    .restart local v13    # "localPageId":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v4, v4, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v4}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_2
.end method

.method private maybeInitializeAnnotationController()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 3233
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    if-nez v6, :cond_0

    .line 3234
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 3235
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 3236
    .local v1, "activity":Landroid/app/Activity;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v5

    .line 3237
    .local v5, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    if-nez v5, :cond_1

    .line 3277
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "activity":Landroid/app/Activity;
    .end local v5    # "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    :cond_0
    :goto_0
    return-void

    .line 3242
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "activity":Landroid/app/Activity;
    .restart local v5    # "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    :cond_1
    iget-object v6, v5, Lcom/google/android/apps/books/annotations/VolumeVersion;->contentVersion:Ljava/lang/String;

    if-nez v6, :cond_3

    .line 3243
    const-string v6, "ReaderFragment"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3244
    const-string v6, "ReaderFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Loaded volume metadata without content version: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/google/android/apps/books/annotations/VolumeVersion;->volumeId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3246
    :cond_2
    sget-object v6, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->NO_CONTENT_VERSION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;)V

    goto :goto_0

    .line 3249
    :cond_3
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 3250
    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    move-result-object v3

    .line 3254
    .local v3, "mainController":Lcom/google/android/apps/books/annotations/AnnotationController;
    new-array v6, v10, [Lcom/google/android/apps/books/annotations/AnnotationListener;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    aput-object v7, v6, v9

    invoke-interface {v3, v5, v6}, Lcom/google/android/apps/books/annotations/AnnotationController;->weaklyAddListeners(Lcom/google/android/apps/books/annotations/VolumeVersion;[Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 3256
    new-instance v6, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    invoke-direct {v6, v3, v5}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;-><init>(Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    iput-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .line 3257
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualAnnotationController:Lcom/google/android/apps/books/util/Eventual;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 3259
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    move-result-object v4

    .line 3261
    .local v4, "search":Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    new-instance v2, Lcom/google/android/apps/books/app/BookmarkController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionComparator()Ljava/util/Comparator;

    move-result-object v7

    invoke-direct {v2, v6, v4, v7}, Lcom/google/android/apps/books/app/BookmarkController;-><init>(Lcom/google/android/apps/books/annotations/UserChangesEditor;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;Ljava/util/Comparator;)V

    .line 3264
    .local v2, "bookmarkController":Lcom/google/android/apps/books/app/BookmarkController;
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    new-array v7, v10, [Lcom/google/android/apps/books/annotations/AnnotationListener;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BookmarkController;->getAnnotationListener()Lcom/google/android/apps/books/annotations/AnnotationListener;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->weaklyAddListeners([Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 3266
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 3268
    new-instance v6, Lcom/google/android/apps/books/app/ClipboardCopyController;

    invoke-direct {v6}, Lcom/google/android/apps/books/app/ClipboardCopyController;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;

    .line 3269
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    new-array v7, v10, [Lcom/google/android/apps/books/annotations/AnnotationListener;

    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;

    invoke-virtual {v8}, Lcom/google/android/apps/books/app/ClipboardCopyController;->getAnnotationListener()Lcom/google/android/apps/books/annotations/AnnotationListener;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->weaklyAddListeners([Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 3274
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->fetchLayers(Lcom/google/android/apps/books/annotations/VolumeAnnotationController;)V

    goto/16 :goto_0
.end method

.method private maybeLoadDownloadProgress()V
    .locals 3

    .prologue
    .line 3502
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartedDownloadProgressLoad:Z

    if-eqz v1, :cond_1

    .line 3511
    :cond_0
    :goto_0
    return-void

    .line 3505
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 3506
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 3509
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->loadDownloadProgress(Ljava/lang/String;)V

    .line 3510
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartedDownloadProgressLoad:Z

    goto :goto_0
.end method

.method private maybeLoadSpecialPageBitmaps()V
    .locals 8

    .prologue
    const v7, 0x7f0e01f5

    const/4 v5, 0x3

    .line 2537
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    if-eqz v4, :cond_5

    .line 2538
    const-string v4, "ReaderFragment"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2539
    const-string v4, "ReaderFragment"

    const-string v5, "Loading special page bitmaps"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2543
    :cond_0
    new-instance v1, Lcom/google/common/collect/ImmutableMap$Builder;

    invoke-direct {v1}, Lcom/google/common/collect/ImmutableMap$Builder;-><init>()V

    .line 2544
    .local v1, "mapBuilder":Lcom/google/common/collect/ImmutableMap$Builder;, "Lcom/google/common/collect/ImmutableMap$Builder<Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v3

    .line 2546
    .local v3, "parent":Landroid/view/ViewGroup;
    const v4, 0x7f0e01f6

    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->snapshotView(Landroid/view/ViewGroup;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2547
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 2548
    sget-object v4, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-virtual {v1, v4, v0}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    .line 2550
    :cond_1
    const v4, 0x7f0e01f3

    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->snapshotView(Landroid/view/ViewGroup;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2551
    if-eqz v0, :cond_2

    .line 2552
    sget-object v4, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-virtual {v1, v4, v0}, Lcom/google/common/collect/ImmutableMap$Builder;->put(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap$Builder;

    .line 2555
    :cond_2
    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableMap$Builder;->build()Lcom/google/common/collect/ImmutableMap;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    .line 2557
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v2

    .line 2558
    .local v2, "pagesView3D":Lcom/google/android/apps/books/widget/PagesView3D;
    if-eqz v2, :cond_3

    .line 2560
    invoke-static {v3, v7}, Lcom/google/android/apps/books/app/ReaderFragment;->snapshotView(Landroid/view/ViewGroup;I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/books/widget/PagesView3D;->setLoadingBitmap(Landroid/graphics/Bitmap;)V

    .line 2563
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePublishSpecialPageBitmaps()V

    .line 2570
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "mapBuilder":Lcom/google/common/collect/ImmutableMap$Builder;, "Lcom/google/common/collect/ImmutableMap$Builder<Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;>;"
    .end local v2    # "pagesView3D":Lcom/google/android/apps/books/widget/PagesView3D;
    .end local v3    # "parent":Landroid/view/ViewGroup;
    :cond_4
    :goto_0
    return-void

    .line 2565
    :cond_5
    const-string v4, "ReaderFragment"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2566
    const-string v4, "ReaderFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bitmaps="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", loading texture height="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private maybeLogPlaybackStart(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V
    .locals 6
    .param p1, "action"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 3610
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->START_READ_ALOUD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsStartTime:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 3611
    invoke-static {p1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPlaybackAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;Ljava/lang/Long;)V

    .line 3612
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsStartTime:J

    .line 3618
    :cond_0
    :goto_0
    return-void

    .line 3613
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->START_MEDIA_OVERLAY_PLAYBACK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoStartTime:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 3615
    invoke-static {p1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPlaybackAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;Ljava/lang/Long;)V

    .line 3616
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoStartTime:J

    goto :goto_0
.end method

.method private maybeLogPlaybackStopped()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 3621
    iget-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsStartTime:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 3622
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->STOP_READ_ALOUD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsStartTime:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPlaybackAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;Ljava/lang/Long;)V

    .line 3625
    iput-wide v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsStartTime:J

    .line 3632
    :cond_0
    :goto_0
    return-void

    .line 3626
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoStartTime:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 3627
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->STOP_MEDIA_OVERLAY_PLAYBACK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoStartTime:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logPlaybackAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;Ljava/lang/Long;)V

    .line 3630
    iput-wide v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoStartTime:J

    goto :goto_0
.end method

.method private maybePauseMo(Z)V
    .locals 4
    .param p1, "preventResume"    # Z

    .prologue
    .line 5092
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    if-eqz v1, :cond_3

    .line 5093
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5094
    const-string v1, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "maybePauseMo("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5096
    :cond_0
    const/4 v0, 0x0

    .line 5097
    .local v0, "updateMenu":Z
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->isSpeaking()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5098
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->stopSpeaking()V

    .line 5099
    const/4 v0, 0x1

    .line 5101
    :cond_1
    if-eqz p1, :cond_2

    .line 5102
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->cancelResume()V

    .line 5103
    const/4 v0, 0x1

    .line 5105
    :cond_2
    if-eqz v0, :cond_3

    .line 5106
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    .line 5109
    .end local v0    # "updateMenu":Z
    :cond_3
    return-void
.end method

.method private maybePausePlayback(Z)V
    .locals 0
    .param p1, "preventResume"    # Z

    .prologue
    .line 8358
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    .line 8359
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePauseMo(Z)V

    .line 8360
    return-void
.end method

.method private maybePublishSpecialPageBitmaps()V
    .locals 2

    .prologue
    .line 2733
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    .line 2734
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->setSpecialPageBitmaps(Lcom/google/common/collect/ImmutableMap;)V

    .line 2736
    :cond_0
    return-void
.end method

.method private maybeRequestVolumeDownload()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 3284
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v1

    .line 3285
    .local v1, "contentFormat":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRequestedVolumeDownloadFormats:Ljava/util/EnumSet;

    invoke-virtual {v4, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    .local v3, "volumeId":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .local v2, "context":Landroid/content/Context;
    if-eqz v2, :cond_0

    .line 3288
    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v3, v5, v6

    invoke-interface {v4, v6, v5}, Lcom/google/android/apps/books/sync/SyncController;->requestManualVolumeContentSyncAsync(Z[Ljava/lang/String;)V

    .line 3290
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRequestedVolumeDownloadFormats:Ljava/util/EnumSet;

    invoke-virtual {v4, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 3292
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "context":Landroid/content/Context;
    .end local v3    # "volumeId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private maybeResetInfoCards()V
    .locals 1

    .prologue
    .line 6591
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    if-eqz v0, :cond_0

    .line 6592
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/InfoCardsHelper;->reset()V

    .line 6594
    :cond_0
    return-void
.end method

.method private maybeSelectReadingMode()V
    .locals 8

    .prologue
    .line 3424
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    if-nez v6, :cond_1

    .line 3473
    :cond_0
    :goto_0
    return-void

    .line 3429
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getUserPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v5

    .line 3430
    .local v5, "userMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v5, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    invoke-virtual {v5}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3431
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    goto :goto_0

    .line 3438
    :cond_2
    if-nez v5, :cond_3

    iget-boolean v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    sget-object v7, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 3439
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    sget-object v7, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->EPUB:Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getModeForContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    .line 3440
    .local v0, "accessibleMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v0, :cond_3

    .line 3441
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    goto :goto_0

    .line 3445
    .end local v0    # "accessibleMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getBookPreferredReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v1

    .line 3446
    .local v1, "bookMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v1, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 3447
    if-eqz v5, :cond_4

    .line 3448
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->onChangedToOfflineAvailableMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 3450
    :cond_4
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    goto :goto_0

    .line 3454
    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    .line 3455
    .local v2, "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getModeForContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v4

    .line 3456
    .local v4, "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v4, :cond_6

    .line 3457
    if-eqz v5, :cond_7

    .line 3458
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->onChangedToOfflineAvailableMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 3460
    :cond_7
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    goto :goto_0

    .line 3465
    .end local v2    # "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .end local v4    # "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_8
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 3466
    sget-object v6, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->UNSUPPORTED_UPLOADED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const v7, 0x7f0f00fb

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;I)V

    goto/16 :goto_0

    .line 3472
    :cond_9
    sget-object v6, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    const-string v7, "No reading modes available offline"

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private maybeSetTextSetting()V
    .locals 2

    .prologue
    .line 2923
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    if-eqz v0, :cond_0

    .line 2924
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    iget v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->textZoom:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setTextZoom(F)V

    .line 2925
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    iget v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;->lineHeight:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setLineHeight(F)V

    .line 2927
    :cond_0
    return-void
.end method

.method private maybeSetTtsEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 3520
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->allowTtsForThisBook()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldAllowScreenReading()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getSpeechIsAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3522
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    .line 3523
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    .line 3524
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    .line 3526
    :cond_1
    return-void
.end method

.method private maybeSetupSearchScrubBar(Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V
    .locals 4
    .param p1, "timer"    # Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .prologue
    .line 3214
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v1, :cond_0

    .line 3215
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v2, 0x7f0e01c4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/SearchScrubBar;

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    .line 3216
    const/4 v0, 0x0

    .line 3217
    .local v0, "scrubAvoidsBarWithPadding":Z
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/app/SystemBarManager;->registerSystemBarAvoidingView(Landroid/view/View;Z)V

    .line 3219
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setupPagingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V

    .line 3220
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPreviousNextSearchResultListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3221
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExitSearchListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setExitSearchListener(Landroid/view/View$OnClickListener;)V

    .line 3222
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMatchDescriptionOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setMatchDescriptionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3223
    new-instance v1, Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    invoke-direct {v1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 3224
    const-string v1, "#searchscrubberview"

    invoke-interface {p1, v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3226
    .end local v0    # "scrubAvoidsBarWithPadding":Z
    :cond_0
    return-void
.end method

.method private maybeSetupTOCAndActionItem()V
    .locals 2

    .prologue
    .line 3841
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTOCActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTOCActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->needsTableOfContents()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TableOfContents;->needsActionItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3843
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTOCActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;->setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V

    .line 3844
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTOCActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TableOfContents;->setActionItem(Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;)V

    .line 3846
    :cond_0
    return-void
.end method

.method private maybeShowEOBBRecommendations(Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "fromPageFlip"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 5224
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z

    if-eqz v5, :cond_1

    .line 5248
    :cond_0
    :goto_0
    return-void

    .line 5228
    :cond_1
    const/4 v2, 0x0

    .line 5229
    .local v2, "pastEobb":Z
    const/4 v1, 0x0

    .line 5231
    .local v1, "lastPositionPastEobb":Z
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getEndOfBookBody(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 5232
    .local v0, "contentEnd":Lcom/google/android/apps/books/common/Position;
    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eqz v5, :cond_2

    .line 5233
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionComparator()Ljava/util/Comparator;

    move-result-object v5

    iget-object v6, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v5, v0, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-gtz v5, :cond_4

    move v2, v3

    .line 5234
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionComparator()Ljava/util/Comparator;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v6, v6, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v5, v0, v6}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-lez v5, :cond_5

    move v1, v3

    .line 5239
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/PagesViewController;->showingEndOfBookPage()Z

    move-result v4

    or-int/2addr v2, v4

    .line 5240
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    or-int/2addr v1, v4

    .line 5245
    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v1, :cond_0

    .line 5246
    :cond_3
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->showEndOfBookCards(Z)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 5233
    goto :goto_1

    :cond_5
    move v1, v4

    .line 5234
    goto :goto_2
.end method

.method private maybeShowRecentSearchesPopup(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1058
    .local p1, "searches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowRecentSearchesPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/ReaderMenu;->showRecentSearchesPopup(Ljava/util/List;)V

    .line 1061
    :cond_0
    return-void
.end method

.method private maybeShowVolumeKeyPageTurnDialog()V
    .locals 6

    .prologue
    .line 8196
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowVolumeKeyPageTurnDialog()Z

    move-result v2

    if-nez v2, :cond_1

    .line 8205
    :cond_0
    :goto_0
    return-void

    .line 8198
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 8199
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 8200
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/books/app/VolumeKeyPageTurnDialog;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 8202
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 8203
    .local v1, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setSawVolumeKeyPageTurnDialog()V

    goto :goto_0
.end method

.method private maybeStartPlayback()V
    .locals 2

    .prologue
    .line 5551
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v0, v1, :cond_0

    .line 5552
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUnPauseMo()V

    .line 5553
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    .line 5555
    :cond_0
    return-void
.end method

.method private maybeUnPauseMo()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 5129
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldBeReadingAlong()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5130
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->isSpeaking()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5131
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->canResume()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5132
    const-string v0, "ReaderFragment"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5133
    const-string v0, "ReaderFragment"

    const-string v1, "maybeUnpauseMo() resuming playback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5135
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->resume()V

    .line 5143
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    .line 5145
    :cond_2
    return-void

    .line 5137
    :cond_3
    const-string v0, "ReaderFragment"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5138
    const-string v0, "ReaderFragment"

    const-string v1, "maybeUnpauseMo() restarting playback"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5140
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->startMoAtCurrentPosition(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    goto :goto_0
.end method

.method private maybeUpdateBuyButtonVisibility()V
    .locals 3

    .prologue
    .line 2941
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v1, :cond_0

    .line 2942
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v0

    .line 2945
    .local v0, "canBuy":Z
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBar;->getBuyButton()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2947
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdatePriceInScrubBar()V

    .line 2949
    .end local v0    # "canBuy":Z
    :cond_0
    return-void

    .line 2945
    .restart local v0    # "canBuy":Z
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private maybeUpdateMenu()V
    .locals 14

    .prologue
    .line 3678
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-nez v9, :cond_1

    .line 3753
    :cond_0
    :goto_0
    return-void

    .line 3683
    :cond_1
    const-string v9, "ReaderFragment"

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 3684
    const-string v9, "ReaderFragment"

    const-string v10, "maybeUpdateMenu()"

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3687
    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v9, :cond_5

    const/4 v7, 0x1

    .line 3688
    .local v7, "validMetadata":Z
    :goto_1
    if-eqz v7, :cond_3

    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    if-eqz v9, :cond_3

    .line 3689
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/app/SearchResultsController;->setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 3690
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    invoke-interface {v9, v10, v11}, Lcom/google/android/apps/books/app/ReaderMenu;->setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)V

    .line 3691
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/app/SearchResultsController;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 3692
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-interface {v9, v10}, Lcom/google/android/apps/books/app/ReaderMenu;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 3695
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v9

    if-nez v9, :cond_4

    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSearchResultsList:Z

    if-eqz v9, :cond_6

    :cond_4
    const/4 v8, 0x1

    .line 3699
    .local v8, "viewingSearchResultOrList":Z
    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v9, v8}, Lcom/google/android/apps/books/app/ReaderMenu;->setSearchMode(Z)V

    .line 3700
    if-nez v8, :cond_0

    .line 3705
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    if-eqz v9, :cond_7

    .line 3706
    const/4 v9, 0x1

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v1

    .line 3707
    .local v1, "controller":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    iget-boolean v11, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isMoSpeaking()Z

    move-result v12

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->canResume()Z

    move-result v13

    invoke-interface {v9, v10, v11, v12, v13}, Lcom/google/android/apps/books/app/ReaderMenu;->setMoItemState(Landroid/content/Context;ZZZ)V

    .line 3712
    .end local v1    # "controller":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    :goto_3
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    if-nez v9, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->allowTtsForThisBook()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getSpeechIsAvailable()Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v6, 0x1

    .line 3714
    .local v6, "readAloudAvailable":Z
    :goto_4
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    if-nez v9, :cond_9

    const/4 v9, 0x1

    :goto_5
    iget-boolean v11, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-interface {v10, v9, v11, v6, v12}, Lcom/google/android/apps/books/app/ReaderMenu;->setTtsItemState(ZZZLandroid/app/Activity;)V

    .line 3718
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/app/ReaderFragment;->getOtherMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v3

    .line 3719
    .local v3, "otherMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    if-eqz v9, :cond_a

    if-eqz v3, :cond_a

    .line 3720
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getContentFormat()Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 3724
    .local v4, "otherModeVisible":Z
    :goto_6
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v9, v4, v3}, Lcom/google/android/apps/books/app/ReaderMenu;->setReadingModeItemState(ZLcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 3727
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v11, Lcom/google/android/apps/books/app/ReaderMenu$Item;->DISPLAY_OPTIONS:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-eqz v9, :cond_b

    const/4 v9, 0x1

    :goto_7
    invoke-interface {v10, v11, v9}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3729
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v11, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SEARCH:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    if-eqz v7, :cond_c

    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/app/ReaderFragment;->canSearch(Lcom/google/android/apps/books/model/VolumeMetadata;)Z

    move-result v9

    if-eqz v9, :cond_c

    const/4 v9, 0x1

    :goto_8
    invoke-interface {v10, v11, v9}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3731
    if-eqz v7, :cond_d

    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v9

    if-nez v9, :cond_d

    const/4 v2, 0x1

    .line 3732
    .local v2, "isntUploaded":Z
    :goto_9
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v10, Lcom/google/android/apps/books/app/ReaderMenu$Item;->SHARE:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    invoke-interface {v9, v10, v2}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3734
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v11, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    if-nez v9, :cond_e

    const/4 v9, 0x1

    :goto_a
    invoke-interface {v10, v11, v9}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3736
    if-eqz v7, :cond_f

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v9

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->canStartAboutVolume(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    if-eqz v2, :cond_f

    const/4 v0, 0x1

    .line 3739
    .local v0, "canAbout":Z
    :goto_b
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v10, Lcom/google/android/apps/books/app/ReaderMenu$Item;->ABOUT:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    invoke-interface {v9, v10, v0}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3742
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v9, v10, :cond_11

    .line 3743
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-nez v9, :cond_10

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3749
    .local v5, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    :goto_c
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->updateBookmarkMenuItemTextForPages(Ljava/util/List;)V

    .line 3752
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v9}, Lcom/google/android/apps/books/app/ReaderMenu;->updateIconVisibilities()V

    goto/16 :goto_0

    .line 3687
    .end local v0    # "canAbout":Z
    .end local v2    # "isntUploaded":Z
    .end local v3    # "otherMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .end local v4    # "otherModeVisible":Z
    .end local v5    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    .end local v6    # "readAloudAvailable":Z
    .end local v7    # "validMetadata":Z
    .end local v8    # "viewingSearchResultOrList":Z
    :cond_5
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 3695
    .restart local v7    # "validMetadata":Z
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 3709
    .restart local v8    # "viewingSearchResultOrList":Z
    :cond_7
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-interface {v9, v10, v11, v12, v13}, Lcom/google/android/apps/books/app/ReaderMenu;->setMoItemState(Landroid/content/Context;ZZZ)V

    goto/16 :goto_3

    .line 3712
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 3714
    .restart local v6    # "readAloudAvailable":Z
    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 3722
    .restart local v3    # "otherMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :cond_a
    const/4 v4, 0x0

    .restart local v4    # "otherModeVisible":Z
    goto/16 :goto_6

    .line 3727
    :cond_b
    const/4 v9, 0x0

    goto/16 :goto_7

    .line 3729
    :cond_c
    const/4 v9, 0x0

    goto :goto_8

    .line 3731
    :cond_d
    const/4 v2, 0x0

    goto :goto_9

    .line 3734
    .restart local v2    # "isntUploaded":Z
    :cond_e
    const/4 v9, 0x0

    goto :goto_a

    .line 3736
    :cond_f
    const/4 v0, 0x0

    goto :goto_b

    .line 3743
    .restart local v0    # "canAbout":Z
    :cond_10
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v9}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/books/navigation/BookNavView;->getCenterPageRenderings()Ljava/util/List;

    move-result-object v5

    goto :goto_c

    .line 3746
    :cond_11
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;

    invoke-virtual {v9}, Lcom/google/android/apps/books/util/UnloadableEventual;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .restart local v5    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    goto :goto_c
.end method

.method private maybeUpdatePriceInScrubBar()V
    .locals 6

    .prologue
    .line 2573
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$3700(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v3

    .line 2574
    .local v3, "purchaseInfo":Lcom/google/android/apps/books/app/PurchaseInfo;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 2576
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$3700(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/books/util/RentalUtils;->getSaleOrRentalText(Lcom/google/android/apps/books/app/PurchaseInfo;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 2579
    .local v2, "priceDetails":Ljava/lang/String;
    iget-boolean v4, v3, Lcom/google/android/apps/books/app/PurchaseInfo;->hasMultipleOffers:Z

    if-eqz v4, :cond_1

    .line 2580
    move-object v1, v2

    .line 2585
    .local v1, "price":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/ScrubBar;->getBuyButton()Landroid/widget/TextView;

    move-result-object v0

    .line 2586
    .local v0, "buyButton":Landroid/widget/TextView;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2588
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2590
    .end local v0    # "buyButton":Landroid/widget/TextView;
    .end local v1    # "price":Ljava/lang/String;
    .end local v2    # "priceDetails":Ljava/lang/String;
    :cond_0
    return-void

    .line 2582
    .restart local v2    # "priceDetails":Ljava/lang/String;
    :cond_1
    iget-object v1, v3, Lcom/google/android/apps/books/app/PurchaseInfo;->lowestPriceString:Ljava/lang/String;

    .restart local v1    # "price":Ljava/lang/String;
    goto :goto_0
.end method

.method private maybeUpdateTtsController()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 3573
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldBeSpeaking()Z

    move-result v3

    .line 3574
    .local v3, "shouldBeSpeaking":Z
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isTtsSpeaking()Z

    move-result v1

    .line 3575
    .local v1, "isSpeaking":Z
    if-nez v1, :cond_4

    if-eqz v3, :cond_4

    .line 3576
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v5

    .line 3577
    .local v5, "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-eqz v5, :cond_1

    .line 3578
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    if-eqz v7, :cond_3

    .line 3580
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-virtual {v5, v7, v8}, Lcom/google/android/apps/books/tts/TextToSpeechController;->nearestPhraseWithUnit(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    move-result-object v2

    .line 3582
    .local v2, "newPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getShouldAutoAdvanceTts()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 3584
    .local v4, "shouldStartSpeaking":Z
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 3585
    invoke-direct {p0, v5, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->startSpeakingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    .line 3586
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpokenTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 3606
    .end local v2    # "newPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .end local v4    # "shouldStartSpeaking":Z
    .end local v5    # "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    :cond_1
    :goto_1
    return-void

    .line 3582
    .restart local v2    # "newPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .restart local v5    # "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpokenTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    invoke-virtual {v2, v7}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v4, v6

    goto :goto_0

    .line 3589
    .end local v2    # "newPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 3591
    .local v0, "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v0, :cond_1

    .line 3594
    iget-object v6, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-direct {p0, v5, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->startSpeakingAtPosition(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/common/Position;)V

    goto :goto_1

    .line 3598
    .end local v0    # "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v5    # "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    :cond_4
    if-eqz v1, :cond_1

    if-nez v3, :cond_1

    .line 3599
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v5

    .line 3600
    .restart local v5    # "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-eqz v5, :cond_1

    .line 3601
    invoke-virtual {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController;->stopSpeaking()V

    .line 3602
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->onStoppedSpeaking(Z)V

    .line 3603
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v6}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_1
.end method

.method private maybeUpdateViews()V
    .locals 3

    .prologue
    .line 3346
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v1, :cond_0

    .line 3359
    :goto_0
    return-void

    .line 3351
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 3352
    .local v0, "title":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, " "

    .end local v0    # "title":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->populateReaderActionBar(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 3354
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-eqz v1, :cond_2

    .line 3355
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->useMinimalFontSet()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setMinimalFontMenu(Z)V

    .line 3358
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setPassageCount(I)V

    goto :goto_0
.end method

.method private mediaItemIsActive(Ljava/lang/String;)Z
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 4860
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 4861
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/view/View;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4862
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 4864
    .local v2, "view":Landroid/view/View;
    instance-of v3, v2, Lcom/google/android/apps/books/widget/AudioView;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 4866
    check-cast v0, Lcom/google/android/apps/books/widget/AudioView;

    .line 4867
    .local v0, "audioView":Lcom/google/android/apps/books/widget/AudioView;
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/AudioView;->getMediaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4868
    const/4 v3, 0x1

    .line 4872
    .end local v0    # "audioView":Lcom/google/android/apps/books/widget/AudioView;
    .end local v2    # "view":Landroid/view/View;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private moveToPosition(Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZZ)V
    .locals 13
    .param p1, "moveType"    # Lcom/google/android/apps/books/app/MoveType;
    .param p2, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p3, "savePosition"    # Z
    .param p4, "highlight"    # Ljava/lang/String;
    .param p5, "onlyIfChangingLocation"    # Z
    .param p6, "showEobPage"    # Z

    .prologue
    .line 4920
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mJustUndidJump:Z

    .line 4922
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-nez v0, :cond_1

    .line 5052
    :cond_0
    :goto_0
    return-void

    .line 4932
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/books/app/MoveType;->nonNull(Lcom/google/android/apps/books/app/MoveType;)Lcom/google/android/apps/books/app/MoveType;

    move-result-object p1

    .line 4934
    if-eqz p3, :cond_2

    .line 4935
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V

    .line 4939
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->dismissActiveSelection()V

    .line 4941
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V

    .line 4943
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/MoveType;->shouldZoomToMinWhenMoving()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4946
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->zoomToMin()V

    .line 4953
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDisplayedInitialPosition:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getWarnOnSample(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getDefaultPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-nez v0, :cond_3

    .line 4956
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0f0150

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 4958
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDisplayedInitialPosition:Z

    .line 4962
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    .line 4965
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePauseMo(Z)V

    .line 4967
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeExpireRental()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4971
    move-object v1, p2

    .line 4976
    .local v1, "validSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v2, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v2}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4985
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v12

    .line 4987
    .local v12, "writingDirection":Lcom/google/android/apps/books/util/WritingDirection;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v2, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4988
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    sget-object v2, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    const-string v3, "calling moveToPosition"

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 4991
    :cond_4
    const/4 v10, 0x1

    .line 4994
    .local v10, "isPositionViewable":Z
    :try_start_1
    iget v0, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-nez v0, :cond_5

    .line 4995
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v2, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/render/Renderer;->isPositionEnabled(Lcom/google/android/apps/books/common/Position;)Z
    :try_end_1
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v10

    .line 5009
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->getSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v7

    .line 5011
    .local v7, "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-nez p5, :cond_b

    .line 5012
    const/4 v11, 0x1

    .line 5021
    .local v11, "performMove":Z
    :goto_4
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/MoveType;->isJump()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5022
    iput-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 5025
    :cond_6
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearJumpBit(Lcom/google/android/apps/books/app/MoveType;)V

    .line 5027
    if-eqz v11, :cond_0

    .line 5028
    if-eqz v10, :cond_7

    if-eqz p6, :cond_e

    :cond_7
    const/4 v9, 0x1

    .line 5029
    .local v9, "finalShowEob":Z
    :goto_5
    if-eqz v9, :cond_8

    .line 5031
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    .line 5034
    :cond_8
    iget-object v0, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeAnnouncePosition(Lcom/google/android/apps/books/common/Position;)V

    .line 5036
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    const/4 v6, 0x0

    move/from16 v2, p3

    move-object v3, p1

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V

    .line 5039
    if-eqz v9, :cond_f

    .line 5040
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->requestLoadEndOfBookPage()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$12100(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V

    .line 5041
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->showEndOfBook()V

    .line 5048
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    if-nez v0, :cond_0

    .line 5049
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeStartPlayback()V

    goto/16 :goto_0

    .line 4950
    .end local v1    # "validSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v7    # "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v9    # "finalShowEob":Z
    .end local v10    # "isPositionViewable":Z
    .end local v11    # "performMove":Z
    .end local v12    # "writingDirection":Lcom/google/android/apps/books/util/WritingDirection;
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetZoom()V

    goto/16 :goto_1

    .line 4977
    .restart local v1    # "validSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :catch_0
    move-exception v8

    .line 4978
    .local v8, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v0, "ReaderFragment"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 4979
    const-string v0, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to find requested position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in volume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4982
    :cond_a
    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    .end local v1    # "validSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getDefaultPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .restart local v1    # "validSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    goto/16 :goto_2

    .line 5001
    .end local v8    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .restart local v10    # "isPositionViewable":Z
    .restart local v12    # "writingDirection":Lcom/google/android/apps/books/util/WritingDirection;
    :catch_1
    move-exception v8

    .line 5002
    .restart local v8    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v0, "ReaderFragment"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5003
    const-string v0, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to determine whether position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is enabled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 5014
    .end local v8    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .restart local v7    # "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_b
    if-nez v7, :cond_c

    .line 5015
    const/4 v11, 0x1

    .restart local v11    # "performMove":Z
    goto/16 :goto_4

    .line 5017
    .end local v11    # "performMove":Z
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0, v1, v7}, Lcom/google/android/apps/books/render/Renderer;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I

    move-result v0

    if-eqz v0, :cond_d

    const/4 v11, 0x1

    .restart local v11    # "performMove":Z
    :goto_7
    goto/16 :goto_4

    .end local v11    # "performMove":Z
    :cond_d
    const/4 v11, 0x0

    goto :goto_7

    .line 5028
    .restart local v11    # "performMove":Z
    :cond_e
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 5043
    .restart local v9    # "finalShowEob":Z
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->hideEndOfBook()V

    goto/16 :goto_6
.end method

.method private moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V
    .locals 7
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "savePosition"    # Z
    .param p3, "highlight"    # Ljava/lang/String;
    .param p4, "showEobPage"    # Z
    .param p5, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 4902
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZZ)V

    .line 4903
    return-void
.end method

.method private moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V
    .locals 7
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "onlyIfChangingLocation"    # Z
    .param p3, "savePosition"    # Z
    .param p4, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    const/4 v4, 0x0

    .line 665
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->canMoveToPosition()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p4

    move-object v2, p1

    move v3, p3

    move v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZZ)V

    .line 670
    :goto_0
    return-void

    .line 668
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    invoke-direct {v0, p1, p4, v4}, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    goto :goto_0
.end method

.method private moveToResumedPosition()V
    .locals 7

    .prologue
    .line 1774
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v6

    .line 1777
    .local v6, "uiMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    if-eqz v0, :cond_1

    .line 1778
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    iget-object v1, v0, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;->spread:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 1779
    .local v1, "positionToResumeTo":Lcom/google/android/apps/books/render/SpreadIdentifier;
    const/4 v2, 0x1

    .line 1780
    .local v2, "fromUser":Z
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    iget-object v5, v0, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;->moveType:Lcom/google/android/apps/books/app/MoveType;

    .line 1789
    .local v5, "moveType":Lcom/google/android/apps/books/app/MoveType;
    :goto_0
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 1791
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v6, v0, :cond_0

    .line 1793
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartOfSkimPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setStartOfSkimPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    .line 1795
    :cond_0
    return-void

    .line 1782
    .end local v1    # "positionToResumeTo":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v2    # "fromUser":Z
    .end local v5    # "moveType":Lcom/google/android/apps/books/app/MoveType;
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v6, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 1784
    .restart local v1    # "positionToResumeTo":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_1
    const/4 v2, 0x0

    .line 1785
    .restart local v2    # "fromUser":Z
    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->RESUME_POSITION:Lcom/google/android/apps/books/app/MoveType;

    .restart local v5    # "moveType":Lcom/google/android/apps/books/app/MoveType;
    goto :goto_0

    .line 1782
    .end local v1    # "positionToResumeTo":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v2    # "fromUser":Z
    .end local v5    # "moveType":Lcom/google/android/apps/books/app/MoveType;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCurrentPosition()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    goto :goto_1
.end method

.method private moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
    .locals 3
    .param p1, "matchLocation"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 4443
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V

    .line 4444
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->setIsViewingSearchResult(Z)V

    .line 4445
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v1

    .line 4447
    .local v1, "passageIndex":I
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v2, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToSearchResult(Lcom/google/android/apps/books/annotations/TextLocation;I)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4451
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->enableSearchScrubBar()V

    .line 4453
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V

    .line 4454
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/ReaderMenu;->clearSearchViewFocus()V

    .line 4455
    return-void

    .line 4448
    :catch_0
    move-exception v0

    .line 4449
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private navigateToLinkPosition(Lcom/google/android/apps/books/common/Position;)V
    .locals 6
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    const/4 v4, 0x0

    .line 5580
    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {v1, p1, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->FOLLOW_LINK:Lcom/google/android/apps/books/app/MoveType;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 5581
    return-void
.end method

.method private onAnyAudioPlayingChanged()V
    .locals 2

    .prologue
    .line 8181
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    .line 8182
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateAccessiblePages(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 8184
    :cond_0
    return-void
.end method

.method private onChangedToOfflineAvailableMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 3
    .param p1, "newMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 3480
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 3481
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 3486
    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne p1, v2, :cond_1

    const v1, 0x7f0f01cb

    .line 3489
    .local v1, "messageId":I
    :goto_0
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3496
    .end local v1    # "messageId":I
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->saveReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 3497
    return-void

    .line 3486
    :cond_1
    const v1, 0x7f0f01ca

    goto :goto_0
.end method

.method private onDeviceConnectionChanged(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 820
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetUsableContentFormats()V

    .line 821
    return-void
.end method

.method private onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;)V
    .locals 1
    .param p1, "reason"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .prologue
    .line 5978
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V

    .line 5979
    return-void
.end method

.method private onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;I)V
    .locals 1
    .param p1, "reason"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p2, "dialogMessageId"    # I

    .prologue
    .line 5992
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;ILjava/lang/String;)V

    .line 5993
    return-void
.end method

.method private onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;ILjava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p2, "dialogMessageId"    # I
    .param p3, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 6001
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError()Z

    move-result v1

    if-nez v1, :cond_1

    .line 6016
    :cond_0
    :goto_0
    return-void

    .line 6005
    :cond_1
    const-string v1, "ReaderFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 6006
    if-nez p3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6008
    .local v0, "message":Ljava/lang/String;
    :goto_1
    const-string v1, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Closing book due to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6011
    .end local v0    # "message":Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowErrorDialog()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6015
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->createDefaultFatalErrorDialog(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->showFatalErrorDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V

    goto :goto_0

    .line 6006
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p2, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 5985
    const v0, 0x7f0f00fc

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;ILjava/lang/String;)V

    .line 5986
    return-void
.end method

.method private onFatalError()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5782
    iget-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEncounteredFatalError:Z

    if-nez v5, :cond_3

    move v0, v3

    .line 5784
    .local v0, "isFirst":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 5787
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z

    .line 5788
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    .line 5789
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isMoSpeaking()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 5790
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v1

    .line 5791
    .local v1, "moController":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    if-eqz v1, :cond_0

    .line 5792
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->stopSpeaking()V

    .line 5796
    .end local v1    # "moController":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    if-eqz v5, :cond_1

    .line 5797
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v2

    .line 5798
    .local v2, "ttsController":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-eqz v2, :cond_1

    .line 5799
    invoke-virtual {v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->stopSpeaking()V

    .line 5803
    .end local v2    # "ttsController":Lcom/google/android/apps/books/tts/TextToSpeechController;
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEncounteredFatalError:Z

    .line 5806
    :cond_2
    return v0

    .end local v0    # "isFirst":Z
    :cond_3
    move v0, v4

    .line 5782
    goto :goto_0
.end method

.method private onFatalException(Ljava/lang/Exception;)V
    .locals 19
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 5817
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalError()Z

    move-result v4

    if-nez v4, :cond_1

    .line 5906
    :cond_0
    :goto_0
    return-void

    .line 5821
    :cond_1
    const-string v4, "ReaderFragment"

    const/4 v6, 0x6

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 5822
    const-string v4, "ReaderFragment"

    const-string v6, "Closing book due to Exception"

    move-object/from16 v0, p1

    invoke-static {v4, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5825
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowErrorDialog()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5829
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v13

    .line 5830
    .local v13, "config":Lcom/google/android/apps/books/util/Config;
    if-eqz v13, :cond_0

    .line 5841
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/util/SessionKeyFactory$RootKeyExpiredException;

    if-eqz v4, :cond_3

    .line 5843
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->ROOT_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5844
    .local v17, "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;

    const/4 v4, 0x0

    const v6, 0x7f0f00e1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    const v6, 0x7f0f00e2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v13}, Lcom/google/android/apps/books/app/ReaderFragment;->buildMarketUpdateIntent(Lcom/google/android/apps/books/util/Config;)Landroid/content/Intent;

    move-result-object v7

    const/high16 v8, 0x1040000

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .line 5905
    .local v3, "fragment":Landroid/support/v4/app/Fragment;
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p1

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->showFatalErrorDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V

    goto :goto_0

    .line 5849
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :cond_3
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;

    if-eqz v4, :cond_4

    .line 5851
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE_LIMIT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5852
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->NO_LICENSE_WHEN_OPENING:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logBookDownloadFailed(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;Ljava/lang/Throwable;)V

    move-object/from16 v16, p1

    .line 5854
    check-cast v16, Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;

    .line 5855
    .local v16, "ole":Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 5856
    .local v18, "res":Landroid/content/res/Resources;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;->getDeviceLimit()I

    move-result v15

    .line 5857
    .local v15, "maxDevices":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v13, v4, v6, v7}, Lcom/google/android/apps/books/util/OceanUris;->getOfflineLimitUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 5858
    .local v14, "infoUri":Landroid/net/Uri;
    const/high16 v4, 0x7f110000

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v15, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 5860
    .local v5, "message":Ljava/lang/String;
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;

    const/4 v4, 0x0

    const v6, 0x7f0f00e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v7, v8, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v8, 0x1040000

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .line 5864
    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    goto :goto_1

    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v5    # "message":Ljava/lang/String;
    .end local v14    # "infoUri":Landroid/net/Uri;
    .end local v15    # "maxDevices":I
    .end local v16    # "ole":Lcom/google/android/apps/books/util/BlockedContentReason$OfflineLimitException;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .end local v18    # "res":Landroid/content/res/Resources;
    :cond_4
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/util/BlockedContentReason$NonSampleExpiredRentalException;

    if-eqz v4, :cond_5

    .line 5866
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->RENTAL_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5867
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    const v4, 0x7f0f016c

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->createDefaultFatalErrorDialog(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    goto :goto_1

    .line 5869
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :cond_5
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;

    if-eqz v4, :cond_6

    .line 5871
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->BLOCKED_CONTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5872
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;

    const/4 v7, 0x0

    const v4, 0x7f0f00e7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    const v4, 0x7f0f00e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v13}, Lcom/google/android/apps/books/app/ReaderFragment;->buildContextSupportIntent(Lcom/google/android/apps/books/util/Config;)Landroid/content/Intent;

    move-result-object v10

    const/high16 v4, 0x1040000

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    const/4 v12, 0x0

    move-object v6, v3

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    goto/16 :goto_1

    .line 5876
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :cond_6
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;

    if-eqz v4, :cond_7

    .line 5878
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->SESSION_KEY_EXPIRED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5879
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    const v4, 0x7f0f00e3

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->createDefaultFatalErrorDialog(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    goto/16 :goto_1

    .line 5881
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :cond_7
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    if-eqz v4, :cond_8

    .line 5883
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->OFFLINE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5884
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;

    const v4, 0x7f0f00f8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    const v4, 0x7f0f00fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v6, v3

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    goto/16 :goto_1

    .line 5887
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :cond_8
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    if-nez v4, :cond_9

    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/provider/ExternalStorageInconsistentException;

    if-eqz v4, :cond_b

    .line 5890
    :cond_9
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/google/android/apps/books/provider/ExternalStorageUnavailableException;

    if-eqz v4, :cond_a

    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EXTERNAL_STORAGE_UNAVAILABLE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    .line 5893
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :goto_2
    const v4, 0x7f0f00fd

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->createDefaultFatalErrorDialog(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 5894
    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->onExternalStorageException()V

    goto/16 :goto_1

    .line 5890
    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    :cond_a
    sget-object v17, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;->EXTERNAL_STORAGE_INCONSISTENT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    goto :goto_2

    .line 5899
    :cond_b
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getCloseBookReason(Ljava/lang/Exception;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;

    move-result-object v17

    .line 5900
    .restart local v17    # "reason":Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;

    const/4 v7, 0x0

    const v4, 0x7f0f00fc

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const v4, 0x7f0f00fe

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-static {}, Lcom/google/android/apps/books/app/ErrorFragment;->getFeedbackIntent()Landroid/content/Intent;

    move-result-object v12

    move-object v6, v3

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/books/app/ReaderFragment$CloseBookFragment;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/Intent;)V

    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    goto/16 :goto_1
.end method

.method private onInternalUserInteraction()V
    .locals 1

    .prologue
    .line 4669
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUserInteractedWithCurrentUiMode:Z

    .line 4671
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelPositionAnnouncement()Z

    .line 4672
    return-void
.end method

.method private onLoadedVolumeMetadata(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7522
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeMetadata;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7523
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->onLoadedVolumeMetadataAndResumed(Lcom/google/android/apps/books/util/ExceptionOr;)V

    .line 7532
    :goto_0
    return-void

    .line 7525
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWhenResumed:Ljava/util/Queue;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$35;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$35;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private onLoadedVolumeMetadataAndResumed(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7539
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeMetadata;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 7540
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 7550
    :cond_0
    :goto_0
    return-void

    .line 7542
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v0

    .line 7543
    .local v0, "e":Ljava/lang/Exception;
    instance-of v1, v0, Landroid/database/sqlite/SQLiteException;

    if-eqz v1, :cond_2

    .line 7544
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/app/TabletBooksApplication;->maybeRecommendUninstallApp(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 7548
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onPaginationChanged()V
    .locals 2

    .prologue
    .line 8431
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearOffset(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 8432
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartOfSkimPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearOffset(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartOfSkimPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 8433
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearOffset(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 8434
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    if-eqz v0, :cond_0

    .line 8435
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPendingNavigationCommand:Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;->spread:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearOffset(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/books/app/ReaderFragment$NavigationCommand;->spread:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 8437
    :cond_0
    return-void
.end method

.method private onShowedCards(Z)V
    .locals 0
    .param p1, "forEobb"    # Z

    .prologue
    .line 2489
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEOBCardsShown:Z

    .line 2490
    return-void
.end method

.method private onStoppedSpeaking(Z)V
    .locals 1
    .param p1, "refreshPage"    # Z

    .prologue
    .line 7928
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    .line 7929
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->clearHighlight()V

    .line 7930
    if-eqz p1, :cond_0

    .line 7932
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 7935
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V

    .line 7936
    return-void
.end method

.method private onUserSelectedNewPosition()V
    .locals 0

    .prologue
    .line 8154
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearPlaybackPosition()V

    .line 8155
    return-void
.end method

.method private onViewSizeChanged(IIII)V
    .locals 7
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    const/4 v6, 0x3

    .line 2798
    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastLeft:I

    if-ne v1, p1, :cond_1

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastTop:I

    if-ne v1, p2, :cond_1

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastRight:I

    if-ne v1, p3, :cond_1

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastBottom:I

    if-ne v1, p4, :cond_1

    .line 2817
    :cond_0
    :goto_0
    return-void

    .line 2803
    :cond_1
    const-string v1, "ReaderFragment"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2804
    const-string v1, "ReaderFragment"

    const-string v2, "New view size: %d/%d/%d/%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2807
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2808
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 2812
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/app/ReaderFragment;->saveViewSize(IIII)V

    .line 2814
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLoadSpecialPageBitmaps()V

    .line 2816
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreateRenderer()V

    goto :goto_0
.end method

.method private onZoomChanged(Lcom/google/android/apps/books/util/ConstrainedScaleScroll;)V
    .locals 4
    .param p1, "helper"    # Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    .prologue
    .line 7192
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_0

    .line 7193
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScale()F

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScrollX()F

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->getScrollY()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->scaleAndScroll(FFF)V

    .line 7196
    :cond_0
    return-void
.end method

.method private purchaseBook()V
    .locals 6

    .prologue
    .line 1537
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    .line 1539
    .local v1, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0f00b7

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnectedElseToast(Landroid/content/Context;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1541
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    .line 1542
    .local v2, "volumeId":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getConfig()Lcom/google/android/apps/books/util/Config;

    move-result-object v3

    const-string v4, "books_inapp_reader_skim_buy"

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->getBuyUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;

    move-result-object v0

    .line 1544
    .local v0, "buyUrl":Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
    if-nez v0, :cond_1

    .line 1554
    .end local v0    # "buyUrl":Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
    .end local v2    # "volumeId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1550
    .restart local v0    # "buyUrl":Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
    .restart local v2    # "volumeId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;->getIsBuyUrl()Z

    move-result v5

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$3700(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Lcom/google/android/apps/books/app/PurchaseInfo;

    move-result-object v3

    :goto_1
    invoke-interface {v1, v2, v4, v5, v3}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 1552
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_SKIM_BUY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    goto :goto_0

    .line 1550
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private releaseWakeLock()V
    .locals 2

    .prologue
    .line 4690
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenBrightLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 4691
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWakeLockHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4692
    return-void
.end method

.method private renewWakeLock()V
    .locals 4

    .prologue
    .line 4684
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenBrightLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 4685
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWakeLockHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4686
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWakeLockHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenTimeout:Ljava/lang/Runnable;

    const-wide/32 v2, 0x57e40

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4687
    return-void
.end method

.method private requestRecentSearches()V
    .locals 3

    .prologue
    .line 1147
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    .line 1148
    .local v0, "volumeId":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchesConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeSearches(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1149
    return-void
.end method

.method private resetInfoCardsHelper()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1152
    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    .line 1153
    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPlaceCardProvider:Lcom/google/android/apps/books/geo/PlaceCardProvider;

    .line 1154
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreateInfoCardsHelper()V

    .line 1155
    return-void
.end method

.method private resetPagesViewController()V
    .locals 0

    .prologue
    .line 1698
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearPagesViewController()V

    .line 1699
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreatePagesViewController()V

    .line 1700
    return-void
.end method

.method private resetSpecialPageBitmaps()V
    .locals 2

    .prologue
    .line 2528
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2529
    const-string v0, "ReaderFragment"

    const-string v1, "Clearing special page bitmaps"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2531
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    .line 2533
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLoadSpecialPageBitmaps()V

    .line 2534
    return-void
.end method

.method private resetUsableContentFormats()V
    .locals 1

    .prologue
    .line 3379
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUsableContentFormats:Ljava/util/Set;

    .line 3380
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeComputeUsableContentFormats()V

    .line 3381
    return-void
.end method

.method private resetZoom()V
    .locals 2

    .prologue
    .line 4844
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->isZoomedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4845
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->setScale(F)V

    .line 4847
    :cond_0
    return-void
.end method

.method private restartTts()V
    .locals 2

    .prologue
    .line 7703
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v0

    .line 7704
    .local v0, "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-nez v0, :cond_0

    .line 7712
    :goto_0
    return-void

    .line 7708
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->stopSpeaking()V

    .line 7710
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->onStoppedSpeaking(Z)V

    .line 7711
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    goto :goto_0
.end method

.method private saveLastSpreadInSkim()V
    .locals 3

    .prologue
    .line 2316
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-eqz v0, :cond_1

    .line 2317
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->getStableSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 2318
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget v0, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    if-eqz v0, :cond_0

    .line 2320
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v1, v1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 2325
    :cond_0
    :goto_0
    return-void

    .line 2323
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpreadInSkim:Lcom/google/android/apps/books/render/SpreadIdentifier;

    goto :goto_0
.end method

.method private saveReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 2
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 6253
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/data/BooksDataController;->setUserSelectedMode(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 6254
    return-void
.end method

.method private saveViewSize(IIII)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2301
    iput p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastLeft:I

    .line 2302
    iput p2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastTop:I

    .line 2303
    iput p3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastRight:I

    .line 2304
    iput p4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mViewLastBottom:I

    .line 2306
    sub-int v1, p3, p1

    .line 2307
    .local v1, "width":I
    sub-int v0, p4, p2

    .line 2308
    .local v0, "height":I
    if-lez v1, :cond_0

    if-lez v0, :cond_0

    .line 2309
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    .line 2313
    :goto_0
    return-void

    .line 2311
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNonZeroViewSize:Landroid/graphics/Point;

    goto :goto_0
.end method

.method private selectPagesView()V
    .locals 18

    .prologue
    .line 1586
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1588
    .local v1, "activity":Landroid/app/Activity;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getPagesViewsContainer()Landroid/view/ViewGroup;

    move-result-object v11

    .line 1589
    .local v11, "pagesView2DContainer":Landroid/view/ViewGroup;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v12

    .line 1590
    .local v12, "pagesView3D":Lcom/google/android/apps/books/widget/PagesView3D;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->use3DPageTurns()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1591
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    if-eqz v15, :cond_0

    .line 1601
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v15}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v8

    .line 1602
    .local v8, "pages2D":Landroid/view/View;
    invoke-virtual {v11, v8}, Landroid/view/ViewGroup;->clearChildFocus(Landroid/view/View;)V

    .line 1603
    invoke-virtual {v11, v8}, Landroid/view/ViewGroup;->clearChildFocus(Landroid/view/View;)V

    .line 1605
    .end local v8    # "pages2D":Landroid/view/View;
    :cond_0
    move-object v9, v12

    .line 1607
    .local v9, "pagesView":Lcom/google/android/apps/books/widget/PagesView;
    :try_start_0
    invoke-interface {v9}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1634
    invoke-virtual {v11}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1658
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v15, :cond_8

    .line 1659
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->displayTwoPages()Z

    move-result v4

    .line 1660
    .local v4, "displayTwoPages":Z
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldFitWidth()Z

    move-result v13

    .line 1665
    .local v13, "shouldFitWidth":Z
    :goto_1
    invoke-interface {v9, v4, v13}, Lcom/google/android/apps/books/widget/PagesView;->setDisplayTwoPages(ZZ)V

    .line 1667
    invoke-interface {v9}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/books/app/ReaderFragment;->setupLoadingGestureDetector(Landroid/view/View;)V

    .line 1669
    invoke-interface {v9}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15}, Landroid/view/View;->requestFocus()Z

    move-result v6

    .line 1670
    .local v6, "focused":Z
    if-nez v6, :cond_1

    const-string v15, "ReaderFragment"

    const/16 v16, 0x3

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 1671
    const-string v15, "ReaderFragment"

    const-string v16, "Failed to focus pages view"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1674
    :cond_1
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    .line 1676
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I

    move/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Lcom/google/android/apps/books/widget/PagesView;->setPageBackgroundColor(I)V

    .line 1677
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v15}, Lcom/google/android/apps/books/widget/PagesView;->setPagesLoading()V

    .line 1679
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v15}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/books/util/AccessibilityUtils;->setImportantForAccessibility(Landroid/view/View;Z)V

    .line 1681
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetPagesViewController()V

    .line 1683
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mExecutingInitialLoadTransition:Z

    move/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Lcom/google/android/apps/books/widget/PagesView;->setExecutingInitialLoadTransition(Z)V

    .line 1684
    return-void

    .line 1608
    .end local v4    # "displayTwoPages":Z
    .end local v6    # "focused":Z
    .end local v13    # "shouldFitWidth":Z
    :catch_0
    move-exception v5

    .line 1610
    .local v5, "e":Ljava/lang/NullPointerException;
    const-string v7, "pagesView.getView().setVisibility(View.VISIBLE) failing"

    .line 1611
    .local v7, "newMessage":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v2

    .line 1612
    .local v2, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    if-nez v2, :cond_4

    .line 1613
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\ngetCallbacks() returned null"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1624
    :cond_2
    :goto_2
    if-nez v9, :cond_3

    .line 1625
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\npagesView is null"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1628
    :cond_3
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\nactivity is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1630
    new-instance v15, Ljava/lang/NullPointerException;

    invoke-direct {v15, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 1615
    :cond_4
    invoke-interface {v2}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v15

    if-nez v15, :cond_2

    .line 1616
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\ngetPagesView3D() returned null"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1617
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    if-eqz v15, :cond_2

    .line 1618
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v16, 0x7f0e0157

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 1619
    .local v14, "view":Landroid/view/View;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\nfindViewById(R.id.reader) returned "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    .line 1636
    .end local v2    # "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .end local v5    # "e":Ljava/lang/NullPointerException;
    .end local v7    # "newMessage":Ljava/lang/String;
    .end local v9    # "pagesView":Lcom/google/android/apps/books/widget/PagesView;
    .end local v14    # "view":Landroid/view/View;
    :cond_5
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 1637
    .local v3, "childCount":I
    if-lez v3, :cond_7

    .line 1638
    const-string v15, "ReaderFragment"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1639
    const-string v15, "ReaderFragment"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "2D Container still holding "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " old views."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    :cond_6
    invoke-virtual {v11}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1643
    :cond_7
    new-instance v10, Lcom/google/android/apps/books/widget/PagesView2D;

    invoke-direct {v10, v1}, Lcom/google/android/apps/books/widget/PagesView2D;-><init>(Landroid/content/Context;)V

    .line 1644
    .local v10, "pagesView2D":Lcom/google/android/apps/books/widget/PagesView2D;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTranslationHelper()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v15

    invoke-virtual {v10, v15}, Lcom/google/android/apps/books/widget/PagesView2D;->setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V

    .line 1645
    invoke-virtual {v11, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1651
    const/16 v15, 0x8

    invoke-static {v12, v15}, Lcom/google/android/apps/books/util/ViewUtils;->setVisibility(Landroid/view/View;I)V

    .line 1652
    move-object v9, v10

    .restart local v9    # "pagesView":Lcom/google/android/apps/books/widget/PagesView;
    goto/16 :goto_0

    .line 1662
    .end local v3    # "childCount":I
    .end local v10    # "pagesView2D":Lcom/google/android/apps/books/widget/PagesView2D;
    :cond_8
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->isTablet()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-static {v1}, Lcom/google/android/apps/books/util/ReaderUtils;->currentlyInLandscape(Landroid/content/Context;)Z

    move-result v15

    if-eqz v15, :cond_9

    const/4 v4, 0x1

    .line 1663
    .restart local v4    # "displayTwoPages":Z
    :goto_3
    const/4 v13, 0x0

    .restart local v13    # "shouldFitWidth":Z
    goto/16 :goto_1

    .line 1662
    .end local v4    # "displayTwoPages":Z
    .end local v13    # "shouldFitWidth":Z
    :cond_9
    const/4 v4, 0x0

    goto :goto_3
.end method

.method private setActionBarVisible(Z)V
    .locals 7
    .param p1, "visible"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6549
    const-string v4, "ReaderFragment"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 6550
    const-string v4, "ReaderFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setActionBarVisible() called with "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6554
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->areSystemBarsVisible()Z

    move-result v4

    if-eq p1, v4, :cond_4

    .line 6555
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    .line 6556
    .local v1, "scene":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 6558
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-nez p1, :cond_1

    .line 6559
    if-eqz v0, :cond_1

    .line 6560
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 6564
    :cond_1
    if-eqz p1, :cond_4

    .line 6567
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingActionBar:Z

    .line 6570
    if-eqz v0, :cond_2

    .line 6571
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 6573
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v4, :cond_3

    .line 6574
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/ReaderMenu;->clearSearchViewFocus()V

    .line 6576
    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingActionBar:Z

    .line 6580
    .end local v0    # "actionBar":Landroid/support/v7/app/ActionBar;
    .end local v1    # "scene":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    :cond_4
    if-eqz p1, :cond_7

    .line 6581
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v4

    iget-boolean v4, v4, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showActionBarShadow:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSearchResultsList:Z

    if-eqz v4, :cond_6

    :cond_5
    move v2, v3

    :cond_6
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->showActionBarShadow(Z)V

    .line 6583
    :cond_7
    return-void
.end method

.method private setBarVisibilityFromMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 4364
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowSystemUi(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setSystemBarsVisible(Z)V

    .line 4365
    iget-boolean v0, p1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showScrubber:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->adjustScrubberVisibility(Z)V

    .line 4368
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeStartPlayback()V

    .line 4369
    return-void
.end method

.method private setGestureListener(Landroid/view/View;Landroid/view/GestureDetector$OnGestureListener;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "listener"    # Landroid/view/GestureDetector$OnGestureListener;

    .prologue
    .line 4735
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 4736
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/view/GestureDetector;

    invoke-direct {v1, v0, p2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 4737
    .local v1, "gestureDetector":Landroid/view/GestureDetector;
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$25;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$25;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/view/GestureDetector;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4744
    return-void
.end method

.method private setOrientationBasedOnMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 4
    .param p1, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 4643
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 4645
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeManifest;->getRenditionOrientation()Ljava/lang/String;

    move-result-object v2

    .line 4652
    .local v2, "rotationLock":Ljava/lang/String;
    :goto_0
    const-string v3, "portrait"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->spreadsDesiredForLandscape()Z

    move-result v3

    if-nez v3, :cond_1

    .line 4654
    const/4 v1, 0x7

    .line 4661
    .local v1, "orientation":I
    :goto_1
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 4662
    return-void

    .line 4645
    .end local v1    # "orientation":I
    .end local v2    # "rotationLock":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 4655
    .restart local v2    # "rotationLock":Ljava/lang/String;
    :cond_1
    const-string v3, "landscape"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4656
    const/4 v1, 0x6

    .restart local v1    # "orientation":I
    goto :goto_1

    .line 4658
    .end local v1    # "orientation":I
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v1

    .restart local v1    # "orientation":I
    goto :goto_1
.end method

.method private setPassageCount(I)V
    .locals 3
    .param p1, "count"    # I

    .prologue
    const/4 v2, 0x0

    .line 7805
    iput p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPassageCount:I

    .line 7807
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v1

    .local v1, "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-eqz v1, :cond_0

    .line 7808
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->setPassageCount(I)V

    .line 7812
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v0

    .local v0, "moc":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    if-eqz v0, :cond_1

    .line 7813
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->setPassageCount(I)V

    .line 7815
    :cond_1
    return-void
.end method

.method private setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 6
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2744
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-eq p1, v2, :cond_0

    .line 2745
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .line 2746
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeRequestVolumeDownload()V

    .line 2748
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->saveLastSpreadInSkim()V

    .line 2749
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setOrientationBasedOnMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 2752
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->teardownRenderer()V

    .line 2753
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->tearDownPlayback()V

    .line 2754
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearPlaybackState()V

    .line 2756
    iput-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mZoomEnabled:Z

    .line 2758
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->setSpecialPageColors()V

    .line 2760
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    sget-object v3, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->IMAGE:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    if-eqz v2, :cond_0

    .line 2762
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2763
    .local v1, "resources":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    sget-object v3, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->FLOWING_TEXT:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2764
    const v2, 0x7f0f013c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const v4, 0x7f0f0069

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2781
    .local v0, "messageString":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2786
    .end local v0    # "messageString":Ljava/lang/String;
    .end local v1    # "resources":Landroid/content/res/Resources;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/ScrubBarController;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 2787
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ScrubBar;->invalidate()V

    .line 2789
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-eqz v2, :cond_1

    .line 2790
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 2794
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreateRenderer()V

    .line 2795
    return-void

    .line 2768
    .restart local v1    # "resources":Landroid/content/res/Resources;
    :cond_2
    const v2, 0x7f0f013b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "messageString":Ljava/lang/String;
    goto :goto_0
.end method

.method private setScrubberVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 4340
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 4341
    return-void
.end method

.method private setSearchScrubberVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 4372
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    if-eqz v0, :cond_0

    .line 4373
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0, p1}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(Z)V

    .line 4375
    :cond_0
    return-void
.end method

.method private setShowSearchResults(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 4290
    if-eqz p1, :cond_0

    .line 4291
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V

    .line 4293
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultView:Landroid/view/View;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setVisibleIfTrue(Landroid/view/View;Z)V

    .line 4294
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSearchResultsList:Z

    .line 4295
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V

    .line 4296
    return-void
.end method

.method private setSpecialPageColors()V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 6082
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    if-nez v10, :cond_0

    .line 6113
    :goto_0
    return-void

    .line 6086
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->usingBlackMangaLetterboxing()Z

    move-result v4

    .line 6087
    .local v4, "isBlackManga":Z
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v10}, Lcom/google/android/apps/books/model/VolumeMetadata;->isAppleFixedLayout()Z

    move-result v10

    if-eqz v10, :cond_1

    move v5, v8

    .line 6090
    .local v5, "isFixedLayout":Z
    :goto_1
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderTheme:Ljava/lang/String;

    const-string v11, "2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    if-nez v4, :cond_2

    if-nez v5, :cond_2

    .line 6091
    const-string v8, "2"

    invoke-static {v8}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedForegroundColor(Ljava/lang/String;)I

    move-result v7

    .line 6098
    .local v7, "textColor":I
    :goto_2
    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment;->TEXTVIEWS_FOR_SEPIA:[I

    .local v1, "arr$":[I
    array-length v6, v1

    .local v6, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_3
    if-ge v2, v6, :cond_3

    aget v3, v1, v2

    .line 6099
    .local v3, "id":I
    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v8, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 6098
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .end local v1    # "arr$":[I
    .end local v2    # "i$":I
    .end local v3    # "id":I
    .end local v5    # "isFixedLayout":Z
    .end local v6    # "len$":I
    .end local v7    # "textColor":I
    :cond_1
    move v5, v9

    .line 6087
    goto :goto_1

    .line 6093
    .restart local v5    # "isFixedLayout":Z
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v10

    new-array v8, v8, [I

    const v11, 0x1010212

    aput v11, v8, v9

    invoke-virtual {v10, v8}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 6095
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v9, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    .line 6096
    .restart local v7    # "textColor":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_2

    .line 6102
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .restart local v1    # "arr$":[I
    .restart local v2    # "i$":I
    .restart local v6    # "len$":I
    :cond_3
    if-eqz v4, :cond_4

    const/high16 v8, -0x1000000

    :goto_4
    iput v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I

    .line 6104
    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment;->VIEWS_FOR_SEPIA:[I

    array-length v6, v1

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v6, :cond_6

    aget v3, v1, v2

    .line 6105
    .restart local v3    # "id":I
    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v8, v3}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iget v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I

    invoke-virtual {v8, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 6104
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 6102
    .end local v3    # "id":I
    :cond_4
    if-eqz v5, :cond_5

    const/4 v8, -0x1

    goto :goto_4

    :cond_5
    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderTheme:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v8

    goto :goto_4

    .line 6108
    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    if-eqz v8, :cond_7

    .line 6109
    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I

    invoke-interface {v8, v9}, Lcom/google/android/apps/books/widget/PagesView;->setPageBackgroundColor(I)V

    .line 6112
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetSpecialPageBitmaps()V

    goto/16 :goto_0
.end method

.method private setStartOfSkimPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    .locals 7
    .param p1, "markPosition"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 5747
    if-nez p1, :cond_1

    .line 5765
    :cond_0
    :goto_0
    return-void

    .line 5750
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartOfSkimPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 5752
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v4, p1, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v4}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v1

    .line 5753
    .local v1, "pageIndex":I
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/books/widget/ScrubBarController;->setStartOfSkimPosition(I)V

    .line 5754
    new-instance v2, Lcom/google/android/apps/books/render/RenderPosition;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartOfSkimPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/books/render/SpreadPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;I)V

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/16 v6, 0x8

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;I)V

    .line 5756
    .local v2, "rp":Lcom/google/android/apps/books/render/RenderPosition;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/ScrubBarController;->getNewThumbnailRenderConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/books/render/Renderer;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5757
    .end local v1    # "pageIndex":I
    .end local v2    # "rp":Lcom/google/android/apps/books/render/RenderPosition;
    :catch_0
    move-exception v0

    .line 5760
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v3, "ReaderFragment"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 5761
    const-string v3, "ReaderFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to find undo position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in volume "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setSystemBarsVisible(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6510
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 6525
    :cond_0
    :goto_0
    return-void

    .line 6514
    :cond_1
    const-string v2, "ReaderFragment"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 6515
    const-string v2, "ReaderFragment"

    const-string v3, "setSystemBarsVisible(%b)"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6519
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    invoke-interface {v2, p1}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    .line 6521
    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowActionBar()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setActionBarVisible(Z)V

    .line 6522
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v0, :cond_0

    .line 6523
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderMenu;->clearSearchViewFocus()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 6521
    goto :goto_1
.end method

.method private setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V
    .locals 1
    .param p1, "toc"    # Lcom/google/android/apps/books/widget/TableOfContents;

    .prologue
    .line 3826
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    if-ne p1, v0, :cond_0

    .line 3834
    :goto_0
    return-void

    .line 3829
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    if-eqz v0, :cond_1

    .line 3830
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TableOfContents;->onDestroy()V

    .line 3832
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    .line 3833
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetupTOCAndActionItem()V

    goto :goto_0
.end method

.method private setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    const/4 v0, 0x1

    .line 6495
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    .line 6496
    return-void
.end method

.method private setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V
    .locals 2
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .param p2, "updatePagesViewController"    # Z
    .param p3, "animate"    # Z

    .prologue
    const/4 v1, 0x0

    .line 6466
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne p1, v0, :cond_0

    .line 6492
    :goto_0
    return-void

    .line 6470
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .line 6472
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-eq p1, v0, :cond_2

    .line 6473
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V

    .line 6474
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_1

    .line 6475
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->hideEndOfBook()V

    .line 6477
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setStartOfSkimPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    .line 6478
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    .line 6479
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePausePlayback(Z)V

    .line 6482
    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_3

    .line 6483
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/books/widget/PagesViewController;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V

    .line 6486
    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showSystemUi:Z

    if-nez v0, :cond_4

    .line 6487
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->setBarVisibilityFromMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 6490
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUserInteractedWithCurrentUiMode:Z

    .line 6491
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelPositionAnnouncement()Z

    goto :goto_0
.end method

.method private setVisibleIfTrue(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "show"    # Z

    .prologue
    .line 4299
    if-eqz p1, :cond_0

    .line 4301
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 4303
    :cond_0
    return-void

    .line 4301
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 25
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 2955
    const-string v2, "null VolumeMetadata"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2957
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3166
    :cond_0
    :goto_0
    return-void

    .line 2962
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v3, "ReaderFragment#setVolumeMetaData"

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v22

    .line 2965
    .local v22, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    const-string v2, "ReaderFragment"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2966
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loaded metadata for volume "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2969
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 2970
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v24

    .line 2971
    .local v24, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/books/util/RentalUtils;->isNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2972
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->createExpireRentalHandler(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/os/Handler;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    .line 2974
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeExpireRental()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2978
    invoke-interface/range {v24 .. v24}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v12, v2, v6

    .line 2979
    .local v12, "delay":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    const-wide/16 v6, 0x64

    add-long/2addr v6, v12

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move-result v20

    .line 2981
    .local v20, "sent":Z
    const-string v2, "ReaderFragment"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2982
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Book expires in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-wide/16 v6, 0x3e8

    div-long v6, v12, v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds, sent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2990
    .end local v12    # "delay":J
    .end local v20    # "sent":Z
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetupSearchScrubBar(Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V

    .line 2992
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualMetadata:Lcom/google/android/apps/books/util/Eventual;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 2996
    new-instance v2, Lcom/google/android/apps/books/app/SearchResultMapImpl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getQueryEmphasisColor()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/app/SearchResultMapImpl;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;

    .line 2998
    invoke-static {}, Lcom/google/android/apps/books/util/Config;->getUseOnMediaOverlays()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasMediaClips()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v2

    if-nez v2, :cond_c

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    .line 3003
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getTextZoom()F

    move-result v21

    .line 3006
    .local v21, "textZoom":F
    const/4 v2, 0x0

    cmpg-float v2, v21, v2

    if-gtz v2, :cond_4

    .line 3007
    new-instance v16, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 3008
    .local v16, "lp":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/preference/LocalPreferences;->getTextZoom()F

    move-result v21

    .line 3012
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v21

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/books/data/BooksDataController;->setTextZoom(Ljava/lang/String;F)V

    .line 3017
    .end local v16    # "lp":Lcom/google/android/apps/books/preference/LocalPreferences;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    if-nez v2, :cond_5

    .line 3018
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getLineHeight()F

    move-result v3

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;FF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mValidTextSettings:Lcom/google/android/apps/books/app/ReaderFragment$TextSettings;

    .line 3021
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTextSetting()V

    .line 3022
    const-string v2, "#setTextZoom"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3024
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 3026
    .local v5, "activity":Landroid/app/Activity;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->loadPrice()V

    .line 3029
    new-instance v2, Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTextZoomForVolume()F

    move-result v3

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getLineHeightForVolume()F

    move-result v4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getValidFontValues()[Ljava/lang/CharSequence;

    move-result-object v6

    invoke-direct {v2, v5, v3, v4, v6}, Lcom/google/android/apps/books/render/ReaderSettings;-><init>(Landroid/content/Context;FF[Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 3032
    const-string v2, "#newReaderSettings"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3034
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/widget/ScrubBarController;->maybeInitializeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/content/res/Resources;)V

    .line 3036
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateBuyButtonVisibility()V

    .line 3038
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUpdateVolumeOverview()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAddToMyEBooks()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isInMyEBooksCollection()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3040
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getPromptBeforeAddingToMyEbooks()Z

    move-result v6

    invoke-interface {v2, v3, v4, v6}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->addVolumeToMyEBooks(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 3042
    const-string v2, "#addVolumeToShelf"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3046
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mServerPosition:Lcom/google/android/apps/books/common/Position;

    if-eqz v2, :cond_7

    .line 3048
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mServerPosition:Lcom/google/android/apps/books/common/Position;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mServerAccess:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6, v7}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeHandleServerPosition(Lcom/google/android/apps/books/common/Position;J)V

    .line 3049
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mServerPosition:Lcom/google/android/apps/books/common/Position;

    .line 3050
    const-string v2, "#handleServerPosition"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3053
    :cond_7
    invoke-interface/range {v24 .. v24}, Lcom/google/android/apps/books/model/VolumeData;->usesExplicitOfflineLicenseManagement()Z

    move-result v23

    .line 3054
    .local v23, "usesEolm":Z
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSavedLicenseAction:Z

    if-nez v2, :cond_d

    const/16 v19, 0x1

    .line 3055
    .local v19, "saveLicenseActionRead":Z
    :goto_2
    invoke-static {v5}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3056
    if-eqz v23, :cond_f

    .line 3057
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasNeverBeenOpened()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3060
    const/16 v19, 0x0

    .line 3061
    invoke-interface/range {v24 .. v24}, Lcom/google/android/apps/books/model/VolumeData;->getMaxOfflineDevices()I

    move-result v17

    .line 3062
    .local v17, "maxOfflineDevices":I
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 3063
    .local v8, "args":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 3064
    .local v18, "res":Landroid/content/res/Resources;
    new-instance v2, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    invoke-direct {v2, v8}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;-><init>(Landroid/os/Bundle;)V

    const v3, 0x7f0f01d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v2

    const v3, 0x7f0f01d1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setCancelLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v9

    .line 3069
    .local v9, "argumentsBuilder":Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    if-nez v17, :cond_e

    .line 3072
    const v2, 0x7f0f01cf

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    .line 3079
    :goto_3
    new-instance v2, Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    invoke-direct {v2, v8}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;-><init>(Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    .line 3082
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v2

    const-class v3, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v8, v4}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 3097
    .end local v8    # "args":Landroid/os/Bundle;
    .end local v9    # "argumentsBuilder":Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .end local v17    # "maxOfflineDevices":I
    .end local v18    # "res":Landroid/content/res/Resources;
    :cond_8
    :goto_4
    if-eqz v19, :cond_9

    .line 3100
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/data/BooksDataController;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    .line 3101
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSavedLicenseAction:Z

    .line 3104
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccess()Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/model/VolumeData$Access;->PURCHASED:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 3105
    new-instance v2, Lcom/google/android/apps/books/app/ReadingAccessManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingAccessDelegate:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    if-eqz v23, :cond_10

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasOfflineLicense()Z

    move-result v6

    if-nez v6, :cond_10

    const/4 v6, 0x1

    :goto_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/app/ReadingAccessManager;-><init>(Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;Ljava/lang/String;Landroid/content/Context;ZLcom/google/android/apps/books/data/BooksDataController;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    .line 3108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadingAccessManager;->start()V

    .line 3109
    const-string v2, "#readingAccessManager"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3114
    :cond_a
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->onDeviceConnectionChanged(Landroid/content/Context;)V

    .line 3115
    const-string v2, "#updateDeviceConnected"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3117
    new-instance v2, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;-><init>(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .line 3118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    sget-object v3, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/books/app/ReaderFragment$14;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/books/app/ReaderFragment$14;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->addAnnotationSetChangeListener(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;)V

    .line 3127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualAnnotationController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v3, Lcom/google/android/apps/books/app/ReaderFragment$15;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/books/app/ReaderFragment$15;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 3134
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeInitializeAnnotationController()V

    .line 3135
    const-string v2, "#InitializeAnnotionController"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3138
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateViews()V

    .line 3139
    const-string v2, "#maybeUpdateViews"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3142
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreateInfoCardsHelper()V

    .line 3143
    const-string v2, "#maybeCreateInfoCardsHelper"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3145
    const-class v2, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookContentFormats:Ljava/util/Set;

    .line 3146
    invoke-static {}, Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;->values()[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;

    move-result-object v10

    .local v10, "arr$":[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    array-length v15, v10

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_6
    if-ge v14, v15, :cond_11

    aget-object v11, v10, v14

    .line 3147
    .local v11, "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2, v11}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasContentFormat(Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 3148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookContentFormats:Ljava/util/Set;

    invoke-interface {v2, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3146
    :cond_b
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 2998
    .end local v5    # "activity":Landroid/app/Activity;
    .end local v10    # "arr$":[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .end local v11    # "format":Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v19    # "saveLicenseActionRead":Z
    .end local v21    # "textZoom":F
    .end local v23    # "usesEolm":Z
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 3054
    .restart local v5    # "activity":Landroid/app/Activity;
    .restart local v21    # "textZoom":F
    .restart local v23    # "usesEolm":Z
    :cond_d
    const/16 v19, 0x0

    goto/16 :goto_2

    .line 3074
    .restart local v8    # "args":Landroid/os/Bundle;
    .restart local v9    # "argumentsBuilder":Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .restart local v17    # "maxOfflineDevices":I
    .restart local v18    # "res":Landroid/content/res/Resources;
    .restart local v19    # "saveLicenseActionRead":Z
    :cond_e
    const v2, 0x7f11000c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setBody(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    move-result-object v2

    const v3, 0x7f0f01d2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;->setOkLabel(Ljava/lang/String;)Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;

    goto/16 :goto_3

    .line 3084
    .end local v8    # "args":Landroid/os/Bundle;
    .end local v9    # "argumentsBuilder":Lcom/google/android/apps/books/app/BookConfirmationDialogFragment$ArgumentsBuilder;
    .end local v17    # "maxOfflineDevices":I
    .end local v18    # "res":Landroid/content/res/Resources;
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->hasOfflineLicense()Z

    move-result v2

    if-nez v2, :cond_8

    .line 3091
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/books/app/ReaderFragment$UpdateOfflineLicenseCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/net/OfflineLicenseManager;->requestOfflineLicense(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController;)V

    goto/16 :goto_4

    .line 3105
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 3152
    .restart local v10    # "arr$":[Lcom/google/android/apps/books/model/VolumeManifest$ContentFormat;
    .restart local v14    # "i$":I
    .restart local v15    # "len$":I
    :cond_11
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeComputeUsableContentFormats()V

    .line 3156
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mStillNeedToShowDisplaySettings:Z

    if-eqz v2, :cond_12

    .line 3157
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->showDisplayOptions()V

    .line 3158
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mStillNeedToShowDisplaySettings:Z

    .line 3162
    :cond_12
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->SETTINGS_LOADED_FROM_METADATA:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Z)V

    .line 3163
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logBookLoaded()V

    .line 3164
    const-string v2, "#logMetadataLoaded"

    move-object/from16 v0, v22

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3165
    invoke-interface/range {v22 .. v22}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    goto/16 :goto_0
.end method

.method private setupFullGestureDetector(Landroid/view/View;)V
    .locals 6
    .param p1, "target"    # Landroid/view/View;

    .prologue
    .line 4760
    const-string v4, "setupGestureDetector missing view"

    invoke-static {p1, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4761
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    const-string v5, "null PagesViewController"

    invoke-static {v4, v5}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4763
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 4765
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4766
    .local v2, "gestureListener":Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    .line 4767
    new-instance v1, Landroid/support/v4/view/GestureDetectorCompat;

    invoke-direct {v1, v0, v2}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 4769
    .local v1, "gestureDetector":Landroid/support/v4/view/GestureDetectorCompat;
    new-instance v3, Landroid/view/ScaleGestureDetector;

    invoke-direct {v3, v0, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 4772
    .local v3, "scaleGestureDetector":Landroid/view/ScaleGestureDetector;
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->disableQuickScaleOnKitKat(Landroid/view/ScaleGestureDetector;)V

    .line 4775
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/support/v4/view/GestureDetectorCompat;->setIsLongpressEnabled(Z)V

    .line 4777
    new-instance v4, Lcom/google/android/apps/books/app/ReaderFragment$26;

    invoke-direct {v4, p0, v1, v3, v2}, Lcom/google/android/apps/books/app/ReaderFragment$26;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/support/v4/view/GestureDetectorCompat;Landroid/view/ScaleGestureDetector;Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;)V

    invoke-virtual {p1, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4834
    return-void
.end method

.method private setupLoadingGestureDetector(Landroid/view/View;)V
    .locals 2
    .param p1, "target"    # Landroid/view/View;

    .prologue
    .line 4731
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setGestureListener(Landroid/view/View;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 4732
    return-void
.end method

.method private shouldAllowScreenReading()Z
    .locals 1

    .prologue
    .line 3516
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoEnabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAutoTtsWhenScreenReading:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldBeReadingAlong()Z
    .locals 3

    .prologue
    .line 3542
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 3543
    .local v0, "activity":Landroid/app/Activity;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isEffectivelyResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->inManualPageTurn()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isSelectionActive()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInHoverGracePeriod:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isReaderSettingsVisible()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->showingSearchResults()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnouncePositionTask:Ljava/lang/Runnable;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private shouldBeSpeaking()Z
    .locals 1

    .prologue
    .line 3554
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->shouldBeReadingAlong()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldFitWidth()Z
    .locals 1

    .prologue
    .line 7400
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->shouldFitWidth()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldMakePagesAccessible(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1719
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnJellyBeanOrLater()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->LOAD_TEXT_FOR_UI_AUTOMATOR:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowActionBar()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2103
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSettings:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStillNeedToShowDisplaySettings:Z

    if-eqz v1, :cond_1

    .line 2112
    :cond_0
    :goto_0
    return v0

    .line 2105
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->isSelectionActive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2108
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/TranslateViewController;->getVisible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2112
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private shouldShowErrorDialog()Z
    .locals 1

    .prologue
    .line 5810
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowRecentSearchesPopup()Z
    .locals 1

    .prologue
    .line 1054
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingActionBar:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowSystemUi(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)Z
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 4353
    iget-boolean v0, p1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showSystemUi:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->showingSearchResults()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSettings:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStillNeedToShowDisplaySettings:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldShowVolumeKeyPageTurnDialog()Z
    .locals 3

    .prologue
    .line 8187
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 8188
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 8189
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 8190
    .local v1, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getShowVolumeKeyPageTurnDialog()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 8192
    .end local v1    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private showActionBarShadow(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 6586
    if-eqz p1, :cond_0

    const/high16 v0, 0x41800000    # 16.0f

    .line 6587
    .local v0, "elevation":F
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->setActionBarElevation(F)V

    .line 6588
    return-void

    .line 6586
    .end local v0    # "elevation":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showEndOfBookCards(Z)V
    .locals 1
    .param p1, "fromEobb"    # Z

    .prologue
    .line 2482
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    if-eqz v0, :cond_0

    .line 2483
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/InfoCardsHelper;->showEndOfBookCards(Z)V

    .line 2484
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->onShowedCards(Z)V

    .line 2486
    :cond_0
    return-void
.end method

.method private showFatalErrorDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "reason"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 5935
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->addFragment(Landroid/support/v4/app/Fragment;)V

    .line 5936
    invoke-static {p2, p3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logForceClosedBook(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$CloseBookReason;Ljava/lang/Exception;)V

    .line 5937
    return-void
.end method

.method private showingSearchResults()Z
    .locals 1

    .prologue
    .line 4286
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static snapshotView(Landroid/view/ViewGroup;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "viewId"    # I

    .prologue
    .line 2523
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2524
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->snapshotView(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private static snapshotView(Landroid/view/ViewGroup;Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2505
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 2506
    .local v3, "height":I
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 2508
    .local v5, "width":I
    const-string v2, "ReaderFragment#snapshotView"

    .line 2510
    .local v2, "debugString":Ljava/lang/String;
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2511
    .local v1, "config":Landroid/graphics/Bitmap$Config;
    const-string v6, "ReaderFragment#snapshotView"

    invoke-static {v6, v5, v3, v1}, Lcom/google/android/apps/books/util/BitmapUtils;->createBitmapInReader(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2512
    .local v4, "result":Landroid/graphics/Bitmap;
    const-string v6, "ReaderFragment"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2513
    const-string v6, "ReaderFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Created new snapshot bitmap. Dimensions "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2516
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2517
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2519
    return-object v4
.end method

.method private spreadsDesiredForLandscape()Z
    .locals 2

    .prologue
    .line 4636
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeManifest;->getRenditionSpread()Ljava/lang/String;

    move-result-object v0

    .line 4638
    .local v0, "syntheticSpreads":Ljava/lang/String;
    :goto_0
    const-string v1, "landscape"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "both"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 4636
    .end local v0    # "syntheticSpreads":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 4638
    .restart local v0    # "syntheticSpreads":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private startMoAtCurrentPosition(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    .locals 7
    .param p1, "moc"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .prologue
    const/4 v6, 0x3

    const/4 v5, -0x1

    const/4 v4, 0x5

    .line 7865
    if-nez p1, :cond_1

    .line 7866
    const-string v2, "ReaderFragment"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7867
    const-string v2, "ReaderFragment"

    const-string v3, "Missing controller, can\'t start media overlays"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 7909
    :cond_0
    :goto_0
    return-void

    .line 7872
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-nez v2, :cond_2

    .line 7873
    const-string v2, "ReaderFragment"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7874
    const-string v2, "ReaderFragment"

    const-string v3, "startMoAtCurrentPosition -- no PVC"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7879
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    .line 7880
    .local v1, "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-nez v1, :cond_3

    .line 7881
    const-string v2, "ReaderFragment"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7882
    const-string v2, "ReaderFragment"

    const-string v3, "Missing current spread, can\'t start MO"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7886
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 7891
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/SpreadItems;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v2}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v0

    .line 7892
    .local v0, "passageIndex":I
    if-ne v0, v5, :cond_4

    .line 7895
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadPageHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/SpreadItems;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v2}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPageIndex()I

    move-result v0

    .line 7897
    :cond_4
    if-ne v0, v5, :cond_5

    .line 7898
    const-string v2, "ReaderFragment"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7899
    const-string v2, "ReaderFragment"

    const-string v3, "Couldn\'t find chapter index to start MO"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 7904
    :cond_5
    const-string v2, "ReaderFragment"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 7905
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "starting MO at chapter "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7908
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->startMediaAtPassage(ILcom/google/android/apps/books/common/Position;)V

    goto/16 :goto_0
.end method

.method private startPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 6
    .param p1, "screenDirection"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "finishAutomatically"    # Z

    .prologue
    const/4 v5, 0x1

    .line 7210
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7211
    const-string v0, "ReaderFragment"

    const-string v1, "startPageTurn(%s,finishAuto=%b)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ScreenDirection;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7215
    :cond_0
    invoke-direct {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePausePlayback(Z)V

    .line 7216
    if-eqz p2, :cond_1

    .line 7217
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V

    .line 7220
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v0, :cond_2

    .line 7221
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment$42;->$SwitchMap$com$google$android$apps$books$app$ReaderFragment$ReadingUiMode:[I

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 7231
    :cond_2
    :goto_0
    return-void

    .line 7223
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->enqueueStartTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    goto :goto_0

    .line 7226
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/navigation/BookNavView;->scrollPage(Lcom/google/android/apps/books/util/ScreenDirection;)V

    goto :goto_0

    .line 7221
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private startSearch(Ljava/lang/String;)V
    .locals 16
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 4463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-nez v2, :cond_0

    .line 4558
    :goto_0
    return-void

    .line 4466
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/app/SearchResultsController;->beginSearch(Ljava/lang/String;)V

    .line 4467
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    .line 4468
    .local v13, "context":Landroid/content/Context;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v5

    .line 4469
    .local v5, "usefulThreads":I
    new-instance v14, Lcom/google/android/apps/books/util/ProductionLogger;

    invoke-direct {v14, v13}, Lcom/google/android/apps/books/util/ProductionLogger;-><init>(Landroid/content/Context;)V

    .line 4470
    .local v14, "logger":Lcom/google/android/apps/books/util/ProductionLogger;
    new-instance v1, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getContentDataSource()Lcom/google/android/apps/books/render/FetchingDataSource;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->shouldSubstringSearch()Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getLocale()Ljava/util/Locale;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getQueryEmphasisColor()I

    move-result v10

    const-string v2, "Searching "

    invoke-static {v14, v2}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v11

    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->SENTENCE_BEFORE_AND_AFTER:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2, v13}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v12

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/render/ReaderDataSource;IZLjava/util/Locale;Lcom/google/android/apps/books/geo/AnnotationSet;Lcom/google/android/apps/books/model/VolumeMetadata;ILcom/google/android/apps/books/util/Logging$PerformanceTracker;Z)V

    .line 4475
    .local v1, "loader":Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchSequenceNumber:I

    add-int/lit8 v15, v2, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchSequenceNumber:I

    .line 4476
    .local v15, "thisSearchSequenceNumber":I
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$23;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v15}, Lcom/google/android/apps/books/app/ReaderFragment$23;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->load(Lcom/google/android/apps/books/model/SearchResultListener;)V

    goto :goto_0
.end method

.method private startSpeakingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V
    .locals 1
    .param p1, "tts"    # Lcom/google/android/apps/books/tts/TextToSpeechController;
    .param p2, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 7918
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getShouldAutoAdvanceTts()Z

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->startSpeakingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Z)V

    .line 7919
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onStartedSpeaking()V

    .line 7920
    return-void
.end method

.method private startSpeakingAtPosition(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/common/Position;)V
    .locals 3
    .param p1, "tts"    # Lcom/google/android/apps/books/tts/TextToSpeechController;
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 7912
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getShouldAutoAdvanceTts()Z

    move-result v2

    invoke-virtual {p1, v0, p2, v1, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->startSpeakingAtPosition(ILcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/tts/TtsUnit;Z)V

    .line 7914
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onStartedSpeaking()V

    .line 7915
    return-void
.end method

.method private tearDownPlayback()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 7818
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    if-eqz v0, :cond_0

    .line 7819
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->shutdown()V

    .line 7820
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    .line 7821
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastSpokenTtsPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 7822
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->onStoppedSpeaking(Z)V

    .line 7824
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    if-eqz v0, :cond_2

    .line 7825
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7826
    const-string v0, "ReaderFragment"

    const-string v1, "tearDownPlayback() is clearing mMoController"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7828
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->stopSpeaking()V

    .line 7829
    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMoController:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    .line 7830
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->onStoppedSpeaking(Z)V

    .line 7832
    :cond_2
    return-void
.end method

.method private teardownGestureDetector(Landroid/view/View;)V
    .locals 2
    .param p1, "target"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 4837
    const-string v0, "missing PagesView"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4838
    invoke-virtual {p1, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4839
    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    .line 4840
    return-void
.end method

.method private teardownRenderer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1703
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    if-eqz v0, :cond_0

    .line 1704
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/Renderer;->setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 1705
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->destroy()V

    .line 1706
    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 1708
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UnloadableEventual;->unload()V

    .line 1709
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onPaginationChanged()V

    .line 1711
    :cond_0
    return-void
.end method

.method private textSelectionHasEnded()V
    .locals 1

    .prologue
    .line 2477
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 2478
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V

    .line 2479
    return-void
.end method

.method private updateBarsVisibility()V
    .locals 1

    .prologue
    .line 4345
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setBarVisibilityFromMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4346
    return-void
.end method

.method private updateBookmarkMenuItemTextForPages(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5251
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5252
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v1, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 5273
    :goto_0
    return-void

    .line 5259
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$27;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$27;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private updateMainTouchAreaInsets(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 4717
    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStatusBarHeight:I

    if-lez v0, :cond_4

    .line 4718
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->left:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStatusBarHeight:I

    :goto_0
    iput v0, v2, Landroid/graphics/Rect;->left:I

    .line 4719
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStatusBarHeight:I

    :goto_1
    iput v0, v2, Landroid/graphics/Rect;->top:I

    .line 4720
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v0, p1, Landroid/graphics/Rect;->right:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStatusBarHeight:I

    :goto_2
    iput v0, v2, Landroid/graphics/Rect;->right:I

    .line 4721
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    if-lez v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStatusBarHeight:I

    :cond_0
    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 4728
    :goto_3
    return-void

    :cond_1
    move v0, v1

    .line 4718
    goto :goto_0

    :cond_2
    move v0, v1

    .line 4719
    goto :goto_1

    :cond_3
    move v0, v1

    .line 4720
    goto :goto_2

    .line 4723
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 4724
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 4725
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 4726
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMainTouchAreaInsets:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_3
.end method

.method private updateQuickBookmarks()V
    .locals 4

    .prologue
    .line 648
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    if-nez v1, :cond_0

    .line 656
    :goto_0
    return-void

    .line 652
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    sget-object v2, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMaxQuickBookmarksShown:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->getRecentAnnotationsForLayer(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    .line 655
    .local v0, "bookmarks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/ScrubBarController;->updateQuickBookmarks(Ljava/util/List;)V

    goto :goto_0
.end method

.method private use3DPageTurns()Z
    .locals 2

    .prologue
    .line 2120
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getPageTurnMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "turn3d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->supportsOpenGLES2()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private usingBlackMangaLetterboxing()Z
    .locals 3

    .prologue
    .line 6071
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v0

    .line 6072
    .local v0, "mode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getLayoutStyle()Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;->FIXED:Lcom/google/android/apps/books/model/VolumeManifest$LayoutStyle;

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/books/util/Config;->getEnableBlackManga()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private zoomToMin()V
    .locals 1

    .prologue
    .line 4850
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    if-eqz v0, :cond_0

    .line 4851
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGestureListener:Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderGestureListener;->setScaleToMin()V

    .line 4853
    :cond_0
    return-void
.end method


# virtual methods
.method public acceptNewPosition(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;)V
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    const/4 v4, 0x0

    .line 5202
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    .line 5203
    .local v6, "arguments":Landroid/os/Bundle;
    invoke-static {v6}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 5204
    invoke-static {v6}, Lcom/google/android/apps/books/util/LoaderParams;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkArgument(Z)V

    .line 5208
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z

    if-eqz v0, :cond_0

    .line 5209
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->exitSearch()V

    .line 5213
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {v1, p3, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->ACCEPT_NEW_POSITION_FROM_SERVER:Lcom/google/android/apps/books/app/MoveType;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 5215
    return-void
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 6
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    const/4 v5, 0x0

    .line 6021
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->updateTheme(Ljava/lang/String;)Z

    move-result v2

    .line 6023
    .local v2, "finished":Z
    if-eqz v2, :cond_1

    .line 6068
    :cond_0
    :goto_0
    return-void

    .line 6029
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onPaginationChanged()V

    .line 6031
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v3, :cond_2

    .line 6032
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 6035
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v3, :cond_3

    .line 6036
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v3, p1}, Lcom/google/android/apps/books/app/ReaderMenu;->updateSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 6039
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderTheme:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 6041
    iget-object v3, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderTheme:Ljava/lang/String;

    .line 6042
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->setSpecialPageColors()V

    .line 6044
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 6046
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_4

    .line 6048
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v3, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->getBrightness()I

    move-result v0

    .line 6049
    .local v0, "brightness":I
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    invoke-interface {v3, v0, v5}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onBrightnessChanged(IZ)V

    .line 6053
    .end local v0    # "brightness":I
    .end local v1    # "context":Landroid/content/Context;
    :cond_4
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 6056
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    .line 6058
    iget-boolean v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z

    if-eqz v3, :cond_0

    .line 6060
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v3}, Lcom/google/android/apps/books/app/ReaderMenu;->exitSearch()V

    .line 6061
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/SearchResultsController;->clearSearchResults()V

    .line 6064
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6065
    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->setIsViewingSearchResult(Z)V

    goto :goto_0
.end method

.method public exitSearch()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;

    .line 4326
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z

    .line 4328
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v0, :cond_0

    .line 4329
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderMenu;->exitSearch()V

    .line 4331
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V

    .line 4332
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SearchResultsController;->clearSearchResults()V

    .line 4333
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearSearchHighlights()V

    .line 4336
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V

    .line 4337
    return-void
.end method

.method public fetchLayers(Lcom/google/android/apps/books/annotations/VolumeAnnotationController;)V
    .locals 1
    .param p1, "annotationController"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    .prologue
    .line 8226
    sget-object v0, Lcom/google/android/apps/books/app/ReaderFragment;->FETCHED_LAYERS:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->fetchLayers(Ljava/util/List;)V

    .line 8237
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->fetchCopyQuota()V

    .line 8238
    return-void
.end method

.method protected getBackgroundDataController()Lcom/google/android/apps/books/data/BooksDataController;
    .locals 2

    .prologue
    .line 8383
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    return-object v0
.end method

.method protected getForegroundDataController()Lcom/google/android/apps/books/data/BooksDataController;
    .locals 2

    .prologue
    .line 8387
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v0

    return-object v0
.end method

.method public getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    .locals 1

    .prologue
    .line 2876
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    return-object v0
.end method

.method public maybeHideTransitionCover()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 8259
    const/16 v0, 0xc8

    .line 8260
    .local v0, "FADE_DURATION_MS":I
    const/16 v1, 0x190

    .line 8262
    .local v1, "SLIDE_OUT_DURATION_MS":I
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExecutingInitialLoadTransition:Z

    if-nez v7, :cond_0

    .line 8335
    :goto_0
    return v5

    .line 8268
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v7, v6}, Lcom/google/android/apps/books/widget/PagesViewController;->setExecutingInitialLoadTransition(Z)V

    .line 8269
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExecutingInitialLoadTransition:Z

    .line 8271
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 8274
    .local v3, "interpolator":Landroid/view/animation/AccelerateInterpolator;
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    const v7, 0x7f0e0116

    invoke-virtual {v5, v7}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 8275
    .local v4, "overlay":Landroid/view/View;
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    invoke-direct {v2, v5, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 8276
    .local v2, "fadeOut":Landroid/view/animation/Animation;
    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 8277
    const-wide/16 v8, 0xc8

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 8278
    new-instance v5, Lcom/google/android/apps/books/app/ReaderFragment$39;

    invoke-direct {v5, p0, v4, v3}, Lcom/google/android/apps/books/app/ReaderFragment$39;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/view/View;Landroid/view/animation/AccelerateInterpolator;)V

    invoke-virtual {v2, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 8333
    invoke-virtual {v4, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move v5, v6

    .line 8335
    goto :goto_0
.end method

.method public maybeLoadEndOfBookPage()V
    .locals 1

    .prologue
    .line 8377
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->maybeNeedsReset()Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$17100(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8378
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->requestLoadEndOfBookPage()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$12100(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V

    .line 8380
    :cond_0
    return-void
.end method

.method public moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 2
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 673
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V

    .line 674
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1937
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1939
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v3, "onActivityCreate"

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    .line 1940
    .local v1, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1942
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getVolumeUsageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/VolumeUsageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    .line 1943
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageKey:Ljava/lang/Object;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/common/VolumeUsageManager;->acquireVolumeLock(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1946
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeInitializeAnnotationController()V

    .line 1950
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->resetInfoCardsHelper()V

    .line 1953
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mResolver:Landroid/content/ContentResolver;

    .line 1956
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    if-nez v2, :cond_0

    .line 1957
    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment$MyDataListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    .line 1958
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataListener:Lcom/google/android/apps/books/model/BooksDataListener;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/data/BooksDataController;->weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V

    .line 1961
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    if-nez v2, :cond_1

    .line 1962
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    .line 1965
    :cond_1
    invoke-interface {v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1967
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageKey:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/data/BooksDataController;->startedOpeningBook(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1968
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 7410
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1884
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onAttach(Landroid/app/Activity;)V

    .line 1886
    sget-object v2, Lcom/google/android/apps/books/app/ReaderFragment;->sAveragePerformanceTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    if-nez v2, :cond_0

    .line 1887
    new-instance v2, Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    invoke-direct {v2}, Lcom/google/android/apps/books/util/AveragePerformanceTracker;-><init>()V

    sput-object v2, Lcom/google/android/apps/books/app/ReaderFragment;->sAveragePerformanceTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    .line 1890
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    if-nez v2, :cond_1

    .line 1891
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getLogger(Landroid/content/Context;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v0

    .line 1892
    .local v0, "appLogger":Lcom/google/android/apps/books/util/Logger;
    new-instance v2, Lcom/google/android/apps/books/util/LoggerWrapper;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ReaderFragment #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/google/android/apps/books/app/ReaderFragment;->sInstanceNumber:I

    add-int/lit8 v5, v4, 0x1

    sput v5, Lcom/google/android/apps/books/app/ReaderFragment;->sInstanceNumber:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/app/ReaderFragment;->sAveragePerformanceTracker:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/apps/books/util/LoggerWrapper;-><init>(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/AveragePerformanceTracker;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 1896
    .end local v0    # "appLogger":Lcom/google/android/apps/books/util/Logger;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    if-nez v2, :cond_2

    .line 1897
    new-instance v2, Lcom/google/android/apps/books/app/SearchResultsController;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/books/app/ReaderFragment$12;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/app/ReaderFragment$12;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getQueryEmphasisColor()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/SearchResultsController;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/app/SearchResultsController$SearchResultCallbacks;I)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    .line 1917
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v3, "onAttach#getGeoLayerEnabled"

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    .line 1919
    .local v1, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->GEO_LAYER:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mGeoLayerEnabled:Z

    .line 1920
    invoke-interface {v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1921
    return-void
.end method

.method public onAudioStartPlaying(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 8211
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAudioViewPlaying:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 8212
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V

    .line 8213
    return-void
.end method

.method public onAudioStopPlaying(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 8217
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAudioViewPlaying:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 8218
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V

    .line 8219
    return-void
.end method

.method public onBackPressed()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 7243
    const-string v4, "ReaderFragment"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 7244
    const-string v4, "ReaderFragment"

    const-string v5, "onBackPressed()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 7247
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->FOREGROUND:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v4, v5, :cond_2

    .line 7248
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCardsState(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)Z

    .line 7295
    :cond_1
    :goto_0
    return v2

    .line 7253
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getVisible()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 7254
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->hideDisplayOptions()V

    goto :goto_0

    .line 7264
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/TranslateViewController;->getVisible()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 7265
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslateViewController:Lcom/google/android/apps/books/widget/TranslateViewController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/widget/TranslateViewController;->dismiss()V

    goto :goto_0

    .line 7270
    :cond_4
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z

    if-eqz v4, :cond_6

    .line 7271
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v4, v5, :cond_5

    .line 7272
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->backOutOfSearch()Z

    goto :goto_0

    .line 7274
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v4, v5, :cond_6

    .line 7275
    sget-object v3, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    goto :goto_0

    .line 7280
    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-nez v4, :cond_8

    const/4 v0, 0x0

    .line 7283
    .local v0, "bookNavView":Lcom/google/android/apps/books/navigation/BookNavView;
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mJustUndidJump:Z

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v4, v5, :cond_7

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->scrollToSkimEnterPosition()Z

    move-result v4

    if-nez v4, :cond_1

    .line 7288
    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eqz v4, :cond_9

    .line 7289
    const/4 v1, 0x1

    .line 7290
    .local v1, "savePosition":Z
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpreadBeforeJump:Lcom/google/android/apps/books/render/SpreadIdentifier;

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->UNDO_JUMP:Lcom/google/android/apps/books/app/MoveType;

    invoke-direct {p0, v4, v3, v1, v5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V

    .line 7291
    iput-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mJustUndidJump:Z

    goto :goto_0

    .line 7280
    .end local v0    # "bookNavView":Lcom/google/android/apps/books/navigation/BookNavView;
    .end local v1    # "savePosition":Z
    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    goto :goto_1

    .restart local v0    # "bookNavView":Lcom/google/android/apps/books/navigation/BookNavView;
    :cond_9
    move v2, v3

    .line 7295
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 1066
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1068
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v10

    .line 1072
    .local v10, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v2, "Rendering first page"

    new-instance v11, Lcom/google/android/apps/books/app/ReaderFragment$5;

    invoke-direct {v11, p0}, Lcom/google/android/apps/books/app/ReaderFragment$5;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-static {v1, v2, v11}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 1080
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v2, "Loading notes"

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 1082
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/LoaderParams;->getShowDisplaySettings(Landroid/os/Bundle;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStillNeedToShowDisplaySettings:Z

    .line 1084
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualMetadata:Lcom/google/android/apps/books/util/Eventual;

    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$6;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderFragment$6;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 1091
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMaxQuickBookmarksShown:I

    .line 1093
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    .line 1094
    .local v7, "activity":Landroid/app/Activity;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAutoTtsWhenScreenReadingPreference()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAutoTtsWhenScreenReading:Z

    .line 1095
    invoke-static {v7}, Lcom/google/android/apps/books/app/BooksApplication;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    .line 1097
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartedLoadingMetadata:Z

    if-nez v1, :cond_0

    .line 1098
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    .line 1099
    .local v8, "args":Landroid/os/Bundle;
    invoke-static {v8}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v3

    .line 1100
    .local v3, "account":Landroid/accounts/Account;
    invoke-static {v8}, Lcom/google/android/apps/books/util/LoaderParams;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    .line 1101
    .local v4, "volumeId":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUpdateVolumeOverview()Z

    move-result v5

    .line 1105
    .local v5, "updateVolumeOverview":Z
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAddToMyEBooks()Z

    move-result v6

    .line 1107
    .local v6, "checkShelfMembership":Z
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;ZZ)V

    .line 1109
    .local v0, "task":Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;
    const-string v1, "#continuing"

    invoke-interface {v10, v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 1114
    new-array v1, v12, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 1116
    iput-boolean v13, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mStartedLoadingMetadata:Z

    .line 1119
    .end local v0    # "task":Lcom/google/android/apps/books/app/ReaderFragment$VolumeMetadataLoadTask;
    .end local v3    # "account":Landroid/accounts/Account;
    .end local v4    # "volumeId":Ljava/lang/String;
    .end local v5    # "updateVolumeOverview":Z
    .end local v6    # "checkShelfMembership":Z
    .end local v8    # "args":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {p0, v13}, Lcom/google/android/apps/books/app/ReaderFragment;->setHasOptionsMenu(Z)V

    .line 1122
    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PowerManager;

    .line 1125
    .local v9, "pm":Landroid/os/PowerManager;
    const/16 v1, 0xa

    const-string v2, "ReaderFragment"

    invoke-virtual {v9, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenBrightLock:Landroid/os/PowerManager$WakeLock;

    .line 1126
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScreenBrightLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v12}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 1127
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWakeLockHandler:Landroid/os/Handler;

    .line 1129
    const/4 v1, 0x6

    const-string v2, "ReaderFragment"

    invoke-virtual {v9, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1130
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v12}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 1132
    invoke-static {v7}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    .line 1134
    if-eqz p1, :cond_1

    .line 1135
    const-string v1, "ttsActive"

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    .line 1137
    const-string v1, "ttsActiveSetByUser"

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    .line 1139
    const-string v1, "eobbRecommendationsDismissed"

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z

    .line 1143
    :cond_1
    invoke-interface {v10}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1144
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 3850
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/app/BooksFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 3852
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v2, "onCreateOptionsMenu"

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v9

    .line 3854
    .local v9, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    .line 3862
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    if-nez v1, :cond_0

    .line 3863
    new-instance v1, Lcom/google/android/apps/books/widget/TableOfContents;

    invoke-direct {v1}, Lcom/google/android/apps/books/widget/TableOfContents;-><init>()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V

    .line 3866
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/app/ReaderMenuImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenuCallbacks:Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;

    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/app/ReaderMenuImpl;-><init>(Landroid/content/Context;Landroid/view/Menu;Landroid/view/MenuInflater;Lcom/google/android/apps/books/util/Navigator;Landroid/support/v7/app/ActionBar;Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/TableOfContents;)V

    .line 3870
    .local v0, "readerMenu":Lcom/google/android/apps/books/app/ReaderMenu;
    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    .line 3871
    const-string v1, "#menuImpl"

    invoke-interface {v9, v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 3879
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 3880
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v2, Lcom/google/android/apps/books/app/ReaderFragment$19;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment$19;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderMenu;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 3889
    invoke-interface {v9}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 3890
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 25
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1348
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    move-object/from16 v21, v0

    const-string v22, "onCreateView"

    invoke-static/range {v21 .. v22}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v18

    .line 1350
    .local v18, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    move-object/from16 v21, v0

    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/app/SystemBarManager;->init(Landroid/content/Context;)V

    .line 1352
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    .line 1354
    .local v8, "context":Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 1356
    .local v14, "resources":Landroid/content/res/Resources;
    const v21, 0x7f09011b

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mVerticalRut:I

    .line 1357
    const v21, 0x7f09011c

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mIgnoreTouchesHorizontalMargin:F

    .line 1359
    const v21, 0x7f0900ff

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mZoomMaxSpan:I

    .line 1361
    const-string v21, "status_bar_height"

    const-string v22, "dimen"

    const-string v23, "android"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v14, v0, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 1362
    .local v17, "statusHResId":I
    if-lez v17, :cond_2

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    :goto_0
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mStatusBarHeight:I

    .line 1365
    const v21, 0x7f040053

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move-object/from16 v2, p2

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mTopView:Landroid/view/View;

    .line 1366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mTopView:Landroid/view/View;

    move-object/from16 v21, v0

    const v22, 0x7f0e010f

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .line 1367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualReadingView:Lcom/google/android/apps/books/util/Eventual;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 1368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mTopView:Landroid/view/View;

    move-object/from16 v21, v0

    const v22, 0x7f0e01c2

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultView:Landroid/view/View;

    .line 1370
    const/4 v15, 0x0

    .line 1371
    .local v15, "searchAvoidsBarWithPadding":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultView:Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Lcom/google/android/apps/books/app/SystemBarManager;->registerSystemBarAvoidingView(Landroid/view/View;Z)V

    .line 1374
    const/4 v13, 0x1

    .line 1375
    .local v13, "navAvoidsBarWithPadding":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mTopView:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f0e00c1

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    const/16 v23, 0x1

    invoke-virtual/range {v21 .. v23}, Lcom/google/android/apps/books/app/SystemBarManager;->registerSystemBarAvoidingView(Landroid/view/View;Z)V

    .line 1378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultView:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/app/SearchResultsController;->onAttachView(Landroid/view/View;)V

    .line 1380
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/app/BooksApplication;->consumeTransitionBitmap()Landroid/graphics/Bitmap;

    move-result-object v19

    .line 1382
    .local v19, "transitionBitmap":Landroid/graphics/Bitmap;
    if-eqz v19, :cond_3

    .line 1383
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mExecutingInitialLoadTransition:Z

    .line 1384
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTransitionCover()Landroid/widget/ImageView;

    move-result-object v9

    .line 1385
    .local v9, "cover":Landroid/widget/ImageView;
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1386
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1388
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTransitionCoverFrame()Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    .line 1393
    .end local v9    # "cover":Landroid/widget/ImageView;
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v7

    .line 1394
    .local v7, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v7, v0}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    .line 1395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-interface/range {v21 .. v22}, Lcom/google/android/ublib/view/SystemUi;->setViewFullscreen(Z)V

    .line 1397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v21, v0

    new-instance v22, Lcom/google/android/apps/books/app/ReaderFragment$7;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$7;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setCallbacks(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;)V

    .line 1423
    invoke-static {v8}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getReaderTheme(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderTheme:Ljava/lang/String;

    .line 1424
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->setSpecialPageColors()V

    .line 1429
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->selectPagesView()V

    .line 1440
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    .line 1441
    .local v4, "account":Landroid/accounts/Account;
    invoke-static {v8}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    move-result-object v12

    .line 1443
    .local v12, "mainController":Lcom/google/android/apps/books/annotations/AnnotationController;
    invoke-interface {v12}, Lcom/google/android/apps/books/annotations/AnnotationController;->needsDefaultImageDimensions()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1446
    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v21

    const v22, 0x7f040056

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-virtual/range {v21 .. v24}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup;

    .line 1448
    .local v20, "viewGroup":Landroid/view/ViewGroup;
    const v21, 0x7f0e0121

    invoke-virtual/range {v20 .. v21}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/widget/GeoAnnotationView;

    .line 1450
    .local v10, "geoView":Lcom/google/android/apps/books/widget/GeoAnnotationView;
    invoke-virtual {v10}, Lcom/google/android/apps/books/widget/GeoAnnotationView;->getMapSize()Landroid/graphics/Point;

    move-result-object v11

    .line 1451
    .local v11, "imageSize":Landroid/graphics/Point;
    iget v0, v11, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    iget v0, v11, Landroid/graphics/Point;->y:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v12, v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationController;->setDefaultImageDimensions(II)V

    .line 1454
    .end local v10    # "geoView":Lcom/google/android/apps/books/widget/GeoAnnotationView;
    .end local v11    # "imageSize":Landroid/graphics/Point;
    .end local v20    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v21, v0

    new-instance v22, Lcom/google/android/apps/books/app/ReaderFragment$8;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$8;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setOnLayoutChangeListener(Lcom/google/android/ublib/view/ViewCompat$OnLayoutChangeListener;)V

    .line 1468
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetupTOCAndActionItem()V

    .line 1470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v21, v0

    const v22, 0x7f0e01b0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/widget/ScrubBar;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    .line 1471
    const/16 v16, 0x0

    .line 1472
    .local v16, "statusAvoidsBarWithPadding":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemBarManager:Lcom/google/android/apps/books/app/SystemBarManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Lcom/google/android/apps/books/app/SystemBarManager;->registerSystemBarAvoidingView(Landroid/view/View;Z)V

    .line 1474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/widget/ScrubBarController;->onScrubBarCreated(Lcom/google/android/apps/books/widget/ScrubBar;)V

    .line 1476
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mExecutingInitialLoadTransition:Z

    move/from16 v21, v0

    if-nez v21, :cond_1

    .line 1477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/widget/ScrubBar;->setVisibility(I)V

    .line 1479
    :cond_1
    new-instance v21, Lcom/google/android/ublib/view/FadeAnimationController;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    move-object/from16 v22, v0

    invoke-direct/range {v21 .. v22}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 1481
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/widget/ScrubBar;->getBuyButton()Landroid/widget/TextView;

    move-result-object v21

    new-instance v22, Lcom/google/android/apps/books/app/ReaderFragment$9;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment$9;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1488
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateBuyButtonVisibility()V

    .line 1495
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetupSearchScrubBar(Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V

    .line 1497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setBarVisibilityFromMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 1501
    new-instance v21, Lcom/google/android/apps/books/preference/LocalPreferences;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/preference/LocalPreferences;->getBrightness()I

    move-result v6

    .line 1502
    .local v6, "brightness":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v0, v6, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onBrightnessChanged(IZ)V

    .line 1503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-interface/range {v21 .. v22}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onThemeChanged(Z)V

    .line 1510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mConnReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v21, v0

    new-instance v22, Landroid/content/IntentFilter;

    const-string v23, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct/range {v22 .. v23}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-object/from16 v21, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getTranslationHelper()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->setTranslationHelper(Lcom/google/android/ublib/view/TranslationHelper;)V

    .line 1515
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeCreateInfoCardsHelper()V

    .line 1523
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateViews()V

    .line 1525
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingSearchResultsList:Z

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V

    .line 1527
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/app/ReadingActivity;

    .line 1529
    .local v5, "activity":Lcom/google/android/apps/books/app/ReadingActivity;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mKeyListener:Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/google/android/apps/books/app/ReadingActivity;->setOnKeyListener(Lcom/google/android/apps/books/app/BaseBooksActivity$OnKeyListener;)V

    .line 1531
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment;->mTopView:Landroid/view/View;

    move-object/from16 v21, v0

    return-object v21

    .line 1362
    .end local v4    # "account":Landroid/accounts/Account;
    .end local v5    # "activity":Lcom/google/android/apps/books/app/ReadingActivity;
    .end local v6    # "brightness":I
    .end local v7    # "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    .end local v12    # "mainController":Lcom/google/android/apps/books/annotations/AnnotationController;
    .end local v13    # "navAvoidsBarWithPadding":Z
    .end local v15    # "searchAvoidsBarWithPadding":Z
    .end local v16    # "statusAvoidsBarWithPadding":Z
    .end local v19    # "transitionBitmap":Landroid/graphics/Bitmap;
    :cond_2
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 1390
    .restart local v13    # "navAvoidsBarWithPadding":Z
    .restart local v15    # "searchAvoidsBarWithPadding":Z
    .restart local v19    # "transitionBitmap":Landroid/graphics/Bitmap;
    :cond_3
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/books/app/ReaderFragment;->mExecutingInitialLoadTransition:Z

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2421
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    if-eqz v1, :cond_0

    .line 2422
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReadingAccessManager;->stop()V

    .line 2423
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    .line 2426
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStopped()V

    .line 2430
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBarController;->destroy()V

    .line 2431
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBar:Lcom/google/android/apps/books/widget/ScrubBar;

    .line 2432
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 2433
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;

    .line 2434
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubberAnimationController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 2435
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    .line 2436
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    .line 2438
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 2439
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2440
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mExpireRentalHandler:Landroid/os/Handler;

    .line 2442
    :cond_1
    iput-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 2444
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2445
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageKey:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/common/VolumeUsageManager;->releaseVolumeLock(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2447
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V

    .line 2449
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getDataController()Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    .line 2450
    .local v0, "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    if-eqz v0, :cond_2

    .line 2451
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mVolumeUsageKey:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->finishedOpeningBook(Ljava/lang/Object;)V

    .line 2454
    :cond_2
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDestroy()V

    .line 2455
    return-void
.end method

.method public onDestroyOptionsMenu()V
    .locals 1

    .prologue
    .line 4010
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V

    .line 4012
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDestroyOptionsMenu()V

    .line 4013
    return-void
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2329
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->saveLastSpreadInSkim()V

    .line 2331
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultsController:Lcom/google/android/apps/books/app/SearchResultsController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/SearchResultsController;->onDetachView()V

    .line 2333
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2334
    const-string v1, "ReaderFragment"

    const-string v2, "onDestroyView"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2337
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v1, :cond_1

    .line 2338
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->stop()V

    .line 2341
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2342
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mConnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2344
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    .line 2345
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEventualReadingView:Lcom/google/android/apps/books/util/Eventual;

    .line 2347
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTranslationHelper:Lcom/google/android/ublib/view/TranslationHelper;

    .line 2354
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 2355
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->clearPopup()V

    .line 2358
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    .line 2360
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v1, :cond_2

    .line 2361
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderMenu;->tearDown()V

    .line 2366
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mHeldReferences:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->clear()V

    .line 2368
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReaderSettings:Lcom/google/android/apps/books/app/ReaderSettingsController;

    .line 2371
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->teardownRenderer()V

    .line 2372
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearPagesViewController()V

    .line 2373
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->tearDownPlayback()V

    .line 2375
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    .line 2377
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->teardownGestureDetector(Landroid/view/View;)V

    .line 2379
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    if-eqz v1, :cond_3

    .line 2380
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesView;->onDestroy()V

    .line 2381
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    .line 2386
    :cond_3
    invoke-direct {p0, v4, v4, v4, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->saveViewSize(IIII)V

    .line 2388
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTOCActionItem:Lcom/google/android/apps/books/widget/TableOfContents$TOCActionItem;

    .line 2395
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->setTableOfContents(Lcom/google/android/apps/books/widget/TableOfContents;)V

    .line 2399
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->reset()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->access$5200(Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;)V

    .line 2401
    sget-object v1, Lcom/google/android/apps/books/app/SelectionState;->NONE:Lcom/google/android/apps/books/app/SelectionState;

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;

    .line 2404
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCoverFrame:Landroid/view/View;

    .line 2405
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTransitionCover:Landroid/widget/ImageView;

    .line 2407
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-eqz v1, :cond_4

    .line 2408
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookView;->destroy()V

    .line 2409
    iput-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    .line 2412
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ScrubBarController;->onDestroyScrubBar()V

    .line 2414
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDestroyView()V

    .line 2415
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 1925
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onDetach()V

    .line 1930
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->dismissTextSelection()V

    .line 1931
    return-void
.end method

.method public onMenuOpened()V
    .locals 0

    .prologue
    .line 7238
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V

    .line 7239
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 4615
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v0, :cond_0

    .line 4616
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/ReaderMenu;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 4619
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onInternalUserInteraction()V

    .line 4621
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2214
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onPause()V

    .line 2217
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v2}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->flushNotifications()V

    .line 2218
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    const-string v3, "reading"

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->removeNotificationBlock(Ljava/lang/Object;)V

    .line 2220
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v2, :cond_0

    .line 2221
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->onPause()V

    .line 2224
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    .line 2225
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->maybePauseMo(Z)V

    .line 2226
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelEndHoverGracePeriod()V

    .line 2228
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v1

    .line 2229
    .local v1, "changingConfigurations":Z
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastPauseWasConfigurationChange:Z

    .line 2231
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->cancelPositionAnnouncement()Z

    move-result v0

    .line 2233
    .local v0, "canceledPositionAnnouncement":Z
    if-eqz v1, :cond_1

    if-eqz v0, :cond_2

    .line 2236
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScheduledPositionAnnouncement:Z

    .line 2241
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastPauseWasConfigurationChange:Z

    if-nez v2, :cond_3

    .line 2244
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->releaseWakeLock()V

    .line 2245
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2249
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    if-eqz v2, :cond_3

    .line 2250
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/ReadingAccessManager;->stop()V

    .line 2258
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 2259
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->closeTranslateViewController()V

    .line 2260
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->hideDisplayOptions()V

    .line 2262
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V

    .line 2263
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 4000
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 4001
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    .line 4002
    return-void
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 2126
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onResume()V

    .line 2127
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v8, "onResume"

    invoke-static {v7, v8}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v4

    .line 2128
    .local v4, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/books/app/BooksApplication;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z

    .line 2131
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    if-eqz v7, :cond_0

    .line 2132
    new-instance v7, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/google/android/apps/books/preference/LocalPreferences;->getUseNetworkTts()Z

    move-result v5

    .line 2133
    .local v5, "useNetworkTts":Z
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsController:Lcom/google/android/apps/books/tts/TextToSpeechController;

    invoke-virtual {v7}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getUseNetworkTts()Z

    move-result v7

    if-eq v7, v5, :cond_0

    .line 2134
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->tearDownPlayback()V

    .line 2138
    .end local v5    # "useNetworkTts":Z
    :cond_0
    new-instance v7, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/google/android/apps/books/preference/LocalPreferences;->getPageTurnMode()Ljava/lang/String;

    move-result-object v1

    .line 2139
    .local v1, "pageTurnMode":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v7, v1}, Lcom/google/android/apps/books/widget/PagesView;->supportsPageTurnMode(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2140
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->selectPagesView()V

    .line 2143
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v7, :cond_2

    .line 2144
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->onResume()V

    .line 2147
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->renewWakeLock()V

    .line 2148
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    if-eqz v7, :cond_3

    .line 2149
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAccessManager:Lcom/google/android/apps/books/app/ReadingAccessManager;

    invoke-virtual {v7}, Lcom/google/android/apps/books/app/ReadingAccessManager;->start()V

    .line 2154
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReadingMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/apps/books/app/ReaderFragment;->setOrientationBasedOnMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    .line 2156
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    const-string v8, "reading"

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->addNotificationBlock(Ljava/lang/Object;)V

    .line 2160
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAutoTtsWhenScreenReadingPreference()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAutoTtsWhenScreenReading:Z

    .line 2162
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/books/app/BaseBooksActivity;->maybeShowDogfoodDialog(Landroid/content/Context;)V

    .line 2167
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    sget-object v8, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->SKIM:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v7, v8, :cond_5

    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastPauseWasConfigurationChange:Z

    if-nez v7, :cond_5

    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUserInteractedWithCurrentUiMode:Z

    if-nez v7, :cond_5

    .line 2169
    sget-object v7, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-direct {p0, v7, v0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    .line 2174
    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    if-eqz v7, :cond_4

    .line 2175
    iget-object v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/PagesViewController;->getSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    .line 2176
    .local v3, "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v3, :cond_4

    .line 2177
    iget-object v7, v3, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-direct {p0, v7}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeAnnouncePosition(Lcom/google/android/apps/books/common/Position;)V

    .line 2181
    .end local v3    # "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_4
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    if-nez v7, :cond_7

    .line 2182
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mAutoTtsWhenScreenReading:Z

    if-eqz v7, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/books/app/BooksApplication;->isScreenReaderActive(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2185
    .local v0, "autoReadAloud":Z
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTtsEnabled(Z)V

    .line 2191
    .end local v0    # "autoReadAloud":Z
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUnPauseMo()V

    .line 2192
    invoke-interface {v4}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 2195
    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mWhenResumed:Ljava/util/Queue;

    invoke-interface {v6}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    .local v2, "runnable":Ljava/lang/Runnable;
    if-eqz v2, :cond_8

    .line 2196
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_3

    .line 2171
    .end local v2    # "runnable":Ljava/lang/Runnable;
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V

    goto :goto_0

    :cond_6
    move v0, v6

    .line 2182
    goto :goto_1

    .line 2188
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V

    goto :goto_2

    .line 2201
    .restart local v2    # "runnable":Ljava/lang/Runnable;
    :cond_8
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->showActionBarShadow:Z

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/app/ReaderFragment;->showActionBarShadow(Z)V

    .line 2202
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 1321
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1323
    const-string v0, "ttsActive"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1324
    const-string v0, "ttsActiveSetByUser"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1325
    const-string v0, "eobbRecommendationsDismissed"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEndOfBookBodyRecommendationsDismissed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1327
    return-void
.end method

.method public onScrubBookmarkTap(I)V
    .locals 6
    .param p1, "itemIndex"    # I

    .prologue
    const/4 v4, 0x0

    .line 6170
    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/ScrubBarController;->getQuickBookmarkPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->TAP_QUICK_BOOKMARK:Lcom/google/android/apps/books/app/MoveType;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 6172
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateQuickBookmarks()V

    .line 6173
    return-void
.end method

.method public onScrubFinished(I)V
    .locals 7
    .param p1, "itemPosition"    # I

    .prologue
    const/4 v4, 0x0

    .line 6135
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getPositionForMetadataPageIndex(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v6

    .line 6136
    .local v6, "position":Lcom/google/android/apps/books/common/Position;
    if-eqz v6, :cond_0

    .line 6137
    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {v1, v6, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->END_OF_SCRUB:Lcom/google/android/apps/books/app/MoveType;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 6142
    :cond_0
    return-void
.end method

.method public onScrubStarted()V
    .locals 0

    .prologue
    .line 6130
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onInternalUserInteraction()V

    .line 6131
    return-void
.end method

.method public onScrubUndo()V
    .locals 1

    .prologue
    .line 6163
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->scrollToSkimEnterPosition()Z

    .line 6164
    return-void
.end method

.method public onScrubUpdated(I)V
    .locals 6
    .param p1, "itemPosition"    # I

    .prologue
    .line 6146
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->getPositionForMetadataPageIndex(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 6147
    .local v0, "position":Lcom/google/android/apps/books/common/Position;
    if-eqz v0, :cond_0

    .line 6148
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;

    new-instance v2, Lcom/google/android/apps/books/render/SpreadIdentifier;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/apps/books/app/MoveType;->SCRUB_CONTINUES:Lcom/google/android/apps/books/app/MoveType;

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;)V

    .line 6151
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 4020
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->canSearch(Lcom/google/android/apps/books/model/VolumeMetadata;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4021
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->DEVICE_SEARCH_BUTTON:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 4024
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->setActionBarVisible(Z)V

    .line 4025
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderMenu;->onSearchRequested()V

    .line 4029
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 2048
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v10, "onStart"

    invoke-static {v9, v10}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v5

    .line 2049
    .local v5, "timer":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onStart()V

    .line 2053
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastOnStopTime:J

    sub-long v6, v10, v12

    .line 2054
    .local v6, "timeSinceLastOnStop":J
    const-wide/32 v10, 0x493e0

    cmp-long v9, v6, v10

    if-gtz v9, :cond_0

    iget-wide v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastOnStopTime:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-gtz v9, :cond_1

    .line 2056
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeFetchLatestServerSideReadingPosition()V

    .line 2061
    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiListener:Lcom/google/android/ublib/view/SystemUi$VisibilityListener;

    invoke-interface {v9, v10}, Lcom/google/android/ublib/view/SystemUi;->setOnSystemUiVisibilityChangeListener(Lcom/google/android/ublib/view/SystemUi$VisibilityListener;)V

    .line 2063
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    .line 2066
    .local v1, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->use3DPageTurns()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2067
    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/google/android/apps/books/util/ViewUtils;->setVisibility(Landroid/view/View;I)V

    .line 2070
    :cond_2
    iget-boolean v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUpdatedLastLocalAccess:Z

    if-nez v9, :cond_3

    .line 2071
    iput-boolean v14, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mUpdatedLastLocalAccess:Z

    .line 2073
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 2074
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v8

    .line 2075
    .local v8, "volumeId":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2076
    .local v2, "lastLocalAccess":J
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v9, v8, v2, v3, v14}, Lcom/google/android/apps/books/data/BooksDataController;->updateLastLocalAccess(Ljava/lang/String;JZ)V

    .line 2081
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v2    # "lastLocalAccess":J
    .end local v8    # "volumeId":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/app/BooksApplication;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncUi()Lcom/google/android/apps/books/service/SyncService$SyncUi;

    move-result-object v4

    .line 2082
    .local v4, "syncUi":Lcom/google/android/apps/books/service/SyncService$SyncUi;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/books/util/LoaderParams;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v9}, Lcom/google/android/apps/books/service/SyncService$SyncUi;->cancelDownloadNotification(Ljava/lang/String;)V

    .line 2084
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logReadStart(Landroid/content/Context;)V

    .line 2086
    invoke-interface {v5}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 2087
    return-void
.end method

.method public onStartedSpeaking()V
    .locals 1

    .prologue
    .line 5526
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mReadAlongWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 5527
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V

    .line 5528
    return-void
.end method

.method public onStop()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2274
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onStop()V

    .line 2276
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mLastOnStopTime:J

    .line 2279
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;

    invoke-interface {v1, v5}, Lcom/google/android/ublib/view/SystemUi;->setOnSystemUiVisibilityChangeListener(Lcom/google/android/ublib/view/SystemUi$VisibilityListener;)V

    .line 2283
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNeedReadingPositionSync:Z

    if-eqz v1, :cond_1

    .line 2284
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    if-eqz v1, :cond_0

    .line 2285
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    invoke-interface {v1}, Lcom/google/android/apps/books/sync/SyncController;->requestManualUploadOnlySync()V

    .line 2287
    :cond_0
    iput-boolean v4, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mNeedReadingPositionSync:Z

    .line 2292
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksContract$Collections;->dirUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2293
    .local v0, "collectionDirUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0, v5, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 2295
    .end local v0    # "collectionDirUri":Landroid/net/Uri;
    :cond_1
    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    .prologue
    .line 4680
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->renewWakeLock()V

    .line 4681
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 2207
    if-eqz p1, :cond_0

    .line 2208
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V

    .line 2210
    :cond_0
    return-void
.end method

.method public requestRatingInfoUpdate()V
    .locals 1

    .prologue
    .line 2728
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mEobManager:Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment$EndOfBookViewManager;->requestRatingInfoUpdate()V

    .line 2729
    return-void
.end method

.method public setIsViewingSearchResult(Z)V
    .locals 0
    .param p1, "isViewingSearchResult"    # Z

    .prologue
    .line 4316
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mIsViewingSearchResult:Z

    .line 4317
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V

    .line 4318
    return-void
.end method

.method public showDisplayOptions()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4588
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->getReaderSettings()Lcom/google/android/apps/books/app/ReaderSettingsController;

    move-result-object v0

    .line 4589
    .local v0, "readerSettings":Lcom/google/android/apps/books/app/ReaderSettingsController;
    if-eqz v0, :cond_0

    .line 4590
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTextSetting()V

    .line 4593
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment;->canFitWidth()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isFitWidth()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setFitWidth(ZZ)V

    .line 4595
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->show()V

    .line 4597
    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    invoke-direct {p0, v1, v3, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    .line 4599
    :cond_0
    return-void
.end method
