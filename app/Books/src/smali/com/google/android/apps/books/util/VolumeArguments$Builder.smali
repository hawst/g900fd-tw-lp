.class public Lcom/google/android/apps/books/util/VolumeArguments$Builder;
.super Ljava/lang/Object;
.source "VolumeArguments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/VolumeArguments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mBundle:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    .line 41
    return-void
.end method


# virtual methods
.method public setAccount(Landroid/accounts/Account;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 49
    return-object p0
.end method

.method public setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;
    .locals 2
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    const-string v1, "volumeAuthor"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-object p0
.end method

.method public setCoverImageUri(Landroid/net/Uri;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    const-string v1, "coverImageUri"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    const-string v1, "volumeId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->mBundle:Landroid/os/Bundle;

    const-string v1, "volumeTitle"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-object p0
.end method
