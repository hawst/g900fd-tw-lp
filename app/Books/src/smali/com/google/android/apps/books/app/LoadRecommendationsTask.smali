.class public Lcom/google/android/apps/books/app/LoadRecommendationsTask;
.super Lcom/google/android/apps/books/app/ApiaryLoadTask;
.source "LoadRecommendationsTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/app/ApiaryLoadTask",
        "<",
        "Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

.field private final mRecommendationsContext:Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/BooksApplication;Landroid/accounts/Account;Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "app"    # Lcom/google/android/apps/books/app/BooksApplication;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "recommendationsContext"    # Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/app/BooksApplication;",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p4, "resultConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;>;"
    const-class v0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/google/android/apps/books/app/ApiaryLoadTask;-><init>(Lcom/google/android/apps/books/app/BooksApplication;Landroid/accounts/Account;Ljava/lang/Class;Lcom/google/android/ublib/utils/Consumer;)V

    .line 139
    iput-object p3, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mRecommendationsContext:Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    .line 140
    return-void
.end method

.method public static cachedRecsAreValid(Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;)Z
    .locals 8
    .param p0, "cachedRecs"    # Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    .prologue
    const/4 v2, 0x0

    .line 83
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;->getRecommendedBooks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v2

    .line 87
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;->lastModifiedMillis:J

    sub-long v0, v4, v6

    .line 93
    .local v0, "ageInMillis":J
    const-wide/32 v4, 0xdbba0

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->getBooksApplication()Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    return-object v0
.end method

.method private static getMockTestingRecommendationData()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 170
    .local v0, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    new-instance v2, Ljava/io/File;

    sget-object v3, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    const-string v4, "recommendations.json"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 174
    .local v2, "file":Ljava/io/File;
    :try_start_0
    invoke-static {v0, v2}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->parseRecommendationsJson(Ljava/util/List;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :cond_0
    :goto_0
    return-object v0

    .line 175
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LoadRecommendationsTask"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 177
    const-string v3, "LoadRecommendationsTask"

    const-string v4, "Failed to load \'testing-recommendations.json\'"

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 178
    const-string v3, "LoadRecommendationsTask"

    const-string v4, "Did you run \'adb push tablet/assets_for_testing/recommendations.json /sdcard/Download/\'?"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static parseApiaryRecommendedBookList(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)Ljava/util/List;
    .locals 5
    .param p0, "list"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 44
    .local v1, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    if-eqz p0, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;->items:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 45
    iget-object v4, p0, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    .line 46
    .local v3, "jsonBook":Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
    invoke-static {v3}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->parseJson(Lcom/google/android/apps/books/app/data/JsonRecommendedBook;)Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    move-result-object v0

    .line 47
    .local v0, "book":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    if-eqz v0, :cond_0

    .line 48
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 52
    .end local v0    # "book":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "jsonBook":Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
    :cond_1
    return-object v1
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Void;

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->getBooksApplication()Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-static {}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->getMockTestingRecommendationData()Ljava/util/List;

    move-result-object v1

    .line 78
    :goto_0
    return-object v1

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mRecommendationsContext:Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->isForHome()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 63
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getCachedRecommendations()Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    invoke-static {v1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->cachedRecsAreValid(Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    const-string v1, "LoadRecommendationsTask"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    const-string v1, "LoadRecommendationsTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restored "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;->getRecommendedBooks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cached recommendations"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;->getRecommendedBooks()Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LoadRecommendationsTask"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 73
    const-string v1, "LoadRecommendationsTask"

    const-string v2, "Could not parse cached recommendations"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 78
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/ApiaryLoadTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    goto :goto_0
.end method

.method protected getUrl(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;
    .locals 1
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mRecommendationsContext:Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->getUrl(Lcom/google/android/apps/books/util/Config;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic process(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->process(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected process(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)Ljava/util/List;
    .locals 3
    .param p1, "books"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    if-eqz p1, :cond_1

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mRecommendationsContext:Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->isForHome()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/BooksDataStore;->setCachedRecommendations(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :cond_0
    :goto_0
    invoke-static {p1}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->parseApiaryRecommendedBookList(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)Ljava/util/List;

    move-result-object v1

    .line 164
    :goto_1
    return-object v1

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "LoadRecommendationsTask"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    const-string v1, "LoadRecommendationsTask"

    const-string v2, "Could not cache recommendations"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 164
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/LoadRecommendationsTask;->mCachedRecs:Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;->getRecommendedBooks()Ljava/util/List;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method
