.class public Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;
.super Ljava/lang/Object;
.source "MultiViewAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/MultiViewAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewAnimationState"
.end annotation


# instance fields
.field public alpha:F

.field public pivotX:F

.field public pivotY:F

.field public rotation:F

.field public final scaleX:F

.field public final scaleY:F

.field public final translationX:F

.field public final translationY:F


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getPivotX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotX:F

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getPivotY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->pivotY:F

    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->translationX:F

    .line 44
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->translationY:F

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleX:F

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleY:F

    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getRotation()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->rotation:F

    .line 50
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->alpha:F

    .line 51
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/google/android/apps/books/util/MultiViewAnimator$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lcom/google/android/apps/books/util/MultiViewAnimator$1;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->needsPivot()Z

    move-result v0

    return v0
.end method

.method private needsPivot()Z
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 54
    iget v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->scaleY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;->rotation:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
