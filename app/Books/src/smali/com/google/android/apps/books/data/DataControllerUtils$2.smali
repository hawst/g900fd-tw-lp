.class final Lcom/google/android/apps/books/data/DataControllerUtils$2;
.super Ljava/lang/Object;
.source "DataControllerUtils.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InputStreamSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;->buildContentStreamSource(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/data/InputStreamSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

.field final synthetic val$key:Lcom/google/android/apps/books/model/LocalSessionKey;

.field final synthetic val$scheme:Lcom/google/android/apps/books/data/EncryptionScheme;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$scheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    iput-object p2, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    iput-object p3, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$key:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/DataControllerBlob;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$scheme:Lcom/google/android/apps/books/data/EncryptionScheme;

    iget-object v2, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v2}, Lcom/google/android/apps/books/data/DataControllerBlob;->openInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/DataControllerUtils$2;->val$key:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/data/EncryptionScheme;->decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;
    :try_end_0
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 450
    :catch_0
    move-exception v0

    .line 451
    .local v0, "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    new-instance v1, Lcom/google/android/ublib/utils/WrappedIoException;

    invoke-direct {v1, v0}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 452
    .end local v0    # "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    :catch_1
    move-exception v0

    .line 453
    .local v0, "e":Ljava/security/GeneralSecurityException;
    new-instance v1, Lcom/google/android/ublib/utils/WrappedIoException;

    invoke-direct {v1, v0}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
