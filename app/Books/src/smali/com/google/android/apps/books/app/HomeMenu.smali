.class public interface abstract Lcom/google/android/apps/books/app/HomeMenu;
.super Ljava/lang/Object;
.source "HomeMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/HomeMenu$Callbacks;,
        Lcom/google/android/apps/books/app/HomeMenu$Item;
    }
.end annotation


# virtual methods
.method public abstract onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end method

.method public abstract onSearchRequested()Z
.end method

.method public abstract setItemVisible(Lcom/google/android/apps/books/app/HomeMenu$Item;Z)V
.end method

.method public abstract tearDown()V
.end method
