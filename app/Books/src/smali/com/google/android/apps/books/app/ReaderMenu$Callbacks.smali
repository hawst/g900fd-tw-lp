.class public interface abstract Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;
.super Ljava/lang/Object;
.source "ReaderMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract onItemSelected(Lcom/google/android/apps/books/app/ReaderMenu$Item;)V
.end method

.method public abstract onSearchClosed()Z
.end method

.method public abstract onSearchFieldFocusChanged(Z)V
.end method

.method public abstract onSearchQuerySubmitted(Ljava/lang/String;)V
.end method

.method public abstract startTableOfContentsActivity(Lcom/google/android/apps/books/app/ContentsView$Arguments;)V
.end method
