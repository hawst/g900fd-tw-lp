.class public Lcom/google/android/apps/books/preference/LineHeightPreference;
.super Lcom/google/android/apps/books/preference/PlusMinusPreference;
.source "LineHeightPreference.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/LineHeightPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/preference/PlusMinusPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public static computeDisplayPercent(F)I
    .locals 2
    .param p0, "value"    # F

    .prologue
    .line 13
    const/high16 v0, 0x3fc00000    # 1.5f

    div-float v0, p0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static setToNearestIncrement(F)F
    .locals 3
    .param p0, "value"    # F

    .prologue
    const/high16 v2, 0x3e400000    # 0.1875f

    .line 17
    const/high16 v0, 0x3e400000    # 0.1875f

    .line 18
    .local v0, "increment":F
    div-float v1, p0, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    return v1
.end method


# virtual methods
.method protected canBeLarger(F)Z
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method protected canBeSmaller(F)Z
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 31
    invoke-static {p1}, Lcom/google/android/apps/books/preference/LineHeightPreference;->setToNearestIncrement(F)F

    move-result v0

    const/high16 v1, 0x3f900000    # 1.125f

    invoke-static {v1}, Lcom/google/android/apps/books/preference/LineHeightPreference;->setToNearestIncrement(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constrainValue(F)F
    .locals 2
    .param p1, "value"    # F

    .prologue
    const/high16 v0, 0x3f900000    # 1.125f

    .line 61
    cmpg-float v1, p1, v0

    if-gez v1, :cond_0

    move p1, v0

    .end local p1    # "value":F
    :cond_0
    return p1
.end method

.method protected getSettingTextString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/LineHeightPreference;->getValue()F

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/books/preference/LineHeightPreference;->computeDisplayPercent(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected stepValue(FLcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;)F
    .locals 4
    .param p1, "value"    # F
    .param p2, "step"    # Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    .prologue
    const/high16 v3, 0x3e400000    # 0.1875f

    .line 42
    move v1, p1

    .line 43
    .local v1, "newValue":F
    const/high16 v0, 0x3e400000    # 0.1875f

    .line 44
    .local v0, "increment":F
    sget-object v2, Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;->DOWN:Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    if-ne p2, v2, :cond_1

    .line 45
    sub-float/2addr v1, v3

    .line 56
    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/google/android/apps/books/preference/LineHeightPreference;->setToNearestIncrement(F)F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/preference/LineHeightPreference;->constrainValue(F)F

    move-result v2

    return v2

    .line 46
    :cond_1
    sget-object v2, Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;->UP:Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;

    if-ne p2, v2, :cond_0

    .line 50
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/LineHeightPreference;->canBeSmaller(F)Z

    move-result v2

    if-nez v2, :cond_2

    .line 51
    const/high16 v1, 0x3f900000    # 1.125f

    .line 53
    :cond_2
    add-float/2addr v1, v3

    goto :goto_0
.end method
