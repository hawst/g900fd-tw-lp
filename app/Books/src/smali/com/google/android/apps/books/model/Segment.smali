.class public interface abstract Lcom/google/android/apps/books/model/Segment;
.super Ljava/lang/Object;
.source "Segment.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Identifiable;


# virtual methods
.method public abstract getFixedLayoutVersion()I
.end method

.method public abstract getFixedViewportHeight()I
.end method

.method public abstract getFixedViewportWidth()I
.end method

.method public abstract getPageCount()I
.end method

.method public abstract getStartPosition()Ljava/lang/String;
.end method

.method public abstract getStartPositionObject()Lcom/google/android/apps/books/common/Position;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method

.method public abstract isViewable()Z
.end method
