.class public Lcom/google/android/apps/books/model/ImmutableResource;
.super Ljava/lang/Object;
.source "ImmutableResource.java"

# interfaces
.implements Lcom/google/android/apps/books/model/Resource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ImmutableResource$1;,
        Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    }
.end annotation


# instance fields
.field private final mId:Ljava/lang/String;

.field private final mIsDefault:Z

.field private final mLanguage:Ljava/lang/String;

.field private final mMd5Hash:Ljava/lang/String;

.field private final mMimeType:Ljava/lang/String;

.field private final mOverlay:Ljava/lang/String;

.field private final mShared:Z

.field private final mUrl:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "language"    # Ljava/lang/String;
    .param p5, "md5Hash"    # Ljava/lang/String;
    .param p6, "shared"    # Z
    .param p7, "isDefault"    # Z
    .param p8, "overlay"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "null ID"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mId:Ljava/lang/String;

    .line 32
    const-string v0, "null URL"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mUrl:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mMimeType:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mLanguage:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mMd5Hash:Ljava/lang/String;

    .line 36
    iput-boolean p6, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mShared:Z

    .line 37
    iput-boolean p7, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mIsDefault:Z

    .line 38
    iput-object p8, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mOverlay:Ljava/lang/String;

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Lcom/google/android/apps/books/model/ImmutableResource$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;
    .param p6, "x5"    # Z
    .param p7, "x6"    # Z
    .param p8, "x7"    # Ljava/lang/String;
    .param p9, "x8"    # Lcom/google/android/apps/books/model/ImmutableResource$1;

    .prologue
    .line 6
    invoke-direct/range {p0 .. p8}, Lcom/google/android/apps/books/model/ImmutableResource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    return-void
.end method

.method public static builder()Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 88
    instance-of v0, p1, Lcom/google/android/apps/books/model/Resource;

    if-eqz v0, :cond_0

    .line 89
    check-cast p1, Lcom/google/android/apps/books/model/Resource;

    .end local p1    # "other":Ljava/lang/Object;
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/ResourceUtils;->equals(Lcom/google/android/apps/books/model/Resource;Lcom/google/android/apps/books/model/Resource;)Z

    move-result v0

    .line 91
    :goto_0
    return v0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getIsDefault()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mIsDefault:Z

    return v0
.end method

.method public getIsShared()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mShared:Z

    return v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5Hash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mMd5Hash:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getOverlay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mOverlay:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/model/ImmutableResource;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 83
    invoke-static {p0}, Lcom/google/android/apps/books/model/ResourceUtils;->hashCode(Lcom/google/android/apps/books/model/Resource;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    invoke-static {p0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "ID"

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/ImmutableResource;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "mimeType"

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/ImmutableResource;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
