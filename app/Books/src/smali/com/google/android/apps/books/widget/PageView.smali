.class public Lcom/google/android/apps/books/widget/PageView;
.super Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;
.source "PageView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;
    }
.end annotation


# static fields
.field private static final mTempSize:Landroid/graphics/Point;


# instance fields
.field private final mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

.field private final mProgressView:Lcom/google/android/apps/books/widget/CenteredProgressView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;-><init>(Landroid/content/Context;ZLcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    .line 37
    new-instance v0, Lcom/google/android/apps/books/widget/CenteredProgressView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/widget/CenteredProgressView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mProgressView:Lcom/google/android/apps/books/widget/CenteredProgressView;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mProgressView:Lcom/google/android/apps/books/widget/CenteredProgressView;

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newCenterLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/PageView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 39
    new-instance v0, Lcom/google/android/apps/books/widget/SimpleDrawableView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/widget/SimpleDrawableView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/PageView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    return-void
.end method

.method private onContentChanged(Z)V
    .locals 4
    .param p1, "haveContent"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 85
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageView;->mProgressView:Lcom/google/android/apps/books/widget/CenteredProgressView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/widget/CenteredProgressView;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setVisibility(I)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PageView;->requestLayout()V

    .line 88
    return-void

    :cond_0
    move v0, v2

    .line 85
    goto :goto_0

    :cond_1
    move v2, v1

    .line 86
    goto :goto_1
.end method


# virtual methods
.method public invalidateContent()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->invalidate()V

    .line 92
    return-void
.end method

.method public setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 1
    .param p1, "drawable"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p2, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;)V

    .line 54
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/widget/PageView;->attachBookmarkAnimator(Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 55
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PageView;->onContentChanged(Z)V

    .line 56
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLoading(Landroid/graphics/Point;)V
    .locals 2
    .param p1, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView;->mProgressView:Lcom/google/android/apps/books/widget/CenteredProgressView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/CenteredProgressView;->setSize(Landroid/graphics/Point;)V

    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PageView;->onContentChanged(Z)V

    .line 47
    return-void
.end method

.method public setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V
    .locals 7
    .param p1, "pageType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p2, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p3, "displaySize"    # Landroid/graphics/Point;

    .prologue
    const/4 v6, 0x0

    .line 63
    sget-object v4, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    invoke-interface {p2, v4}, Lcom/google/android/apps/books/util/SimpleDrawable;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 64
    sget-object v4, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    invoke-virtual {v4, p3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 66
    move-object v0, p2

    .line 79
    .local v0, "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageView;->mContentView:Lcom/google/android/apps/books/widget/SimpleDrawableView;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/books/widget/SimpleDrawableView;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PageView;->hideAllBookmarks()V

    .line 81
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/apps/books/widget/PageView;->onContentChanged(Z)V

    .line 82
    return-void

    .line 67
    .end local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    :cond_0
    sget-object v4, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    if-ne p1, v4, :cond_1

    .line 68
    iget v4, p3, Landroid/graphics/Point;->x:I

    sget-object v5, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 69
    .local v1, "leftOffset":F
    iget v4, p3, Landroid/graphics/Point;->y:I

    sget-object v5, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 70
    .local v3, "topOffset":F
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 71
    .local v2, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v2, v1, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 72
    new-instance v0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;

    invoke-direct {v0, p2, p3, v2}, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;Landroid/graphics/Matrix;)V

    .line 73
    .restart local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    goto :goto_0

    .line 74
    .end local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    .end local v1    # "leftOffset":F
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v3    # "topOffset":F
    :cond_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 75
    .restart local v2    # "matrix":Landroid/graphics/Matrix;
    iget v4, p3, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    sget-object v5, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    iget v5, p3, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    sget-object v6, Lcom/google/android/apps/books/widget/PageView;->mTempSize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 77
    new-instance v0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;

    invoke-direct {v0, p2, p3, v2}, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;Landroid/graphics/Matrix;)V

    .restart local v0    # "adjustedContent":Lcom/google/android/apps/books/util/SimpleDrawable;
    goto :goto_0
.end method
