.class public abstract Lcom/google/android/apps/books/preference/PlusMinusPreference;
.super Landroid/widget/LinearLayout;
.source "PlusMinusPreference.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;
    }
.end annotation


# instance fields
.field private mBinding:Z

.field private final mButtonsListener:Landroid/view/View$OnClickListener;

.field private final mKey:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mMinus:Landroid/view/View;

.field private mPlus:Landroid/view/View;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mSettingText:Landroid/widget/TextView;

.field private mShouldBeEnabled:Z

.field private mValue:F

.field private mValueWasSet:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput-boolean v2, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mBinding:Z

    .line 154
    new-instance v1, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference$1;-><init>(Lcom/google/android/apps/books/preference/PlusMinusPreference;)V

    iput-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mButtonsListener:Landroid/view/View$OnClickListener;

    .line 61
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->Preference:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mKey:Ljava/lang/String;

    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/preference/PlusMinusPreference;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/PlusMinusPreference;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mBinding:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/preference/PlusMinusPreference;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/PlusMinusPreference;

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/preference/PlusMinusPreference;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/PlusMinusPreference;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mMinus:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/preference/PlusMinusPreference;F)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/PlusMinusPreference;
    .param p1, "x1"    # F

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->updateValue(F)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/preference/PlusMinusPreference;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/PlusMinusPreference;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->setMinusPlusButtonsEnabled(Z)V

    return-void
.end method

.method private setMinusPlusButtonsEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    iget-object v3, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mMinus:Landroid/view/View;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->canBeSmaller(F)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPlus:Landroid/view/View;

    if-eqz p1, :cond_1

    iget v3, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->canBeLarger(F)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mSettingText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 123
    return-void

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 121
    goto :goto_1
.end method

.method private updateSettingText()V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mSettingText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->getSettingTextString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method

.method private updateValue(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->constrainValue(F)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    .line 172
    const-string v0, "PlusMinusPreference"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "PlusMinusPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->getSettingTextString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->updateSettingText()V

    .line 176
    return-void
.end method


# virtual methods
.method public bindPreference()V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mBinding:Z

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mBinding:Z

    .line 142
    return-void
.end method

.method protected abstract canBeLarger(F)Z
.end method

.method protected abstract canBeSmaller(F)Z
.end method

.method protected abstract constrainValue(F)F
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 90
    invoke-super {p0}, Landroid/widget/LinearLayout;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 91
    .local v0, "title":Ljava/lang/CharSequence;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->getSettingTextString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected getSettingTextString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    return v0
.end method

.method protected maybeEnable()V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mShouldBeEnabled:Z

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->setEnabled(Z)V

    .line 107
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 74
    const v0, 0x7f0e019e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mMinus:Landroid/view/View;

    .line 75
    const v0, 0x7f0e01a0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPlus:Landroid/view/View;

    .line 76
    const v0, 0x7f0e019f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mSettingText:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mMinus:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mButtonsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPlus:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mButtonsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->setEnabled(Z)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mShouldBeEnabled:Z

    .line 86
    return-void
.end method

.method public persistPreference()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPrefs:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mKey:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValue:F

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mKey:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;->onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 152
    :cond_1
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mShouldBeEnabled:Z

    .line 98
    iget-boolean v1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValueWasSet:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 99
    .local v0, "enableNow":Z
    :goto_0
    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 100
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->setMinusPlusButtonsEnabled(Z)V

    .line 101
    return-void

    .line 98
    .end local v0    # "enableNow":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mValueWasSet:Z

    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->updateValue(F)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->maybeEnable()V

    .line 113
    return-void
.end method

.method public setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V
    .locals 0
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "listener"    # Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mPrefs:Landroid/content/SharedPreferences;

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/books/preference/PlusMinusPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 129
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/PlusMinusPreference;->bindPreference()V

    .line 130
    return-void
.end method

.method protected abstract stepValue(FLcom/google/android/apps/books/preference/PlusMinusPreference$StepDirection;)F
.end method
