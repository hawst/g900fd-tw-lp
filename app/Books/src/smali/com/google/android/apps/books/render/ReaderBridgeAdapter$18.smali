.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$chapterIndex:I

.field final synthetic val$offset:I

.field final synthetic val$offsetFromPosition:I

.field final synthetic val$position:Ljava/lang/String;

.field final synthetic val$str:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;ILjava/lang/String;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$chapterIndex:I

    iput-object p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$position:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$offsetFromPosition:I

    iput-object p5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$str:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$offset:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$chapterIndex:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$position:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$offsetFromPosition:I

    iget-object v4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$str:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$18;->val$offset:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/ReaderListener;->onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V

    .line 362
    return-void
.end method
