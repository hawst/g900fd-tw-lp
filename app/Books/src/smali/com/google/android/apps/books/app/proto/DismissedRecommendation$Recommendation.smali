.class public final Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "DismissedRecommendation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/proto/DismissedRecommendation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Recommendation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;


# instance fields
.field private hasVolumeId:Z

.field private memoizedSerializedSize:I

.field private volumeId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .line 259
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation;->internalForceInit()V

    .line 260
    sget-object v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->initFields()V

    .line 261
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->volumeId_:Ljava/lang/String;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->volumeId_:Ljava/lang/String;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->hasVolumeId:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->volumeId_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->defaultInstance:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 1

    .prologue
    .line 130
    # invokes: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->access$100()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->memoizedSerializedSize:I

    .line 52
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 60
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 54
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->hasVolumeId()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 59
    :cond_1
    iput v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->memoizedSerializedSize:I

    move v1, v0

    .line 60
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->volumeId_:Ljava/lang/String;

    return-object v0
.end method

.method public hasVolumeId()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->hasVolumeId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->getSerializedSize()I

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->hasVolumeId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 47
    :cond_0
    return-void
.end method
