.class Lcom/google/android/apps/books/app/ReaderFragment$EnsureClipsTaskFactory;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTaskFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EnsureClipsTaskFactory"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 7743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;Ljava/util/List;ILcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;Lcom/google/android/apps/books/data/BooksDataController;)Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "requestId"    # I
    .param p4, "ensureClipsTaskCallback"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/MediaClips$MediaClip;",
            ">;I",
            "Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ")",
            "Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractEnsureClipsTask;"
        }
    .end annotation

    .prologue
    .line 7748
    .local p2, "clips":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/util/MediaClips$MediaClip;>;"
    new-instance v0, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/mo/EnsureClipsTask;-><init>(Ljava/lang/String;Ljava/util/List;ILcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;Lcom/google/android/apps/books/data/BooksDataController;)V

    return-object v0
.end method
