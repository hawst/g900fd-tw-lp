.class public Lcom/google/android/apps/books/model/JsonVolumeData;
.super Lcom/google/android/apps/books/model/BaseVolumeData;
.source "JsonVolumeData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;,
        Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;
    }
.end annotation


# static fields
.field private static final sNewToOldViewabilityMap:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

.field private final mLocalCoverUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;->ALL_PAGES:Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;

    const-string v1, "http://schemas.google.com/books/2008#view_all_pages"

    sget-object v2, Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;->PARTIAL:Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;

    const-string v3, "http://schemas.google.com/books/2008#view_partial"

    sget-object v4, Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;->NO_PAGES:Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;

    const-string v5, "http://schemas.google.com/books/2008#view_no_pages"

    sget-object v6, Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;->UNKNOWN:Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;

    const-string v7, "http://schemas.google.com/books/2008#view_unknown"

    invoke-static/range {v0 .. v7}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/JsonVolumeData;->sNewToOldViewabilityMap:Lcom/google/common/collect/ImmutableMap;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/api/data/ApiaryVolume;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "volume"    # Lcom/google/android/apps/books/api/data/ApiaryVolume;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/books/model/BaseVolumeData;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    .line 32
    iget-object v0, p1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildCoverUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mLocalCoverUri:Landroid/net/Uri;

    .line 33
    return-void
.end method

.method private parseTime(Ljava/lang/String;)J
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 191
    new-instance v1, Landroid/text/format/Time;

    const-string v2, "UTC"

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 192
    .local v1, "time":Landroid/text/format/Time;
    invoke-virtual {v1, p1}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    move-result v0

    .line 193
    .local v0, "parsed":Z
    if-nez v0, :cond_0

    .line 194
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot parse time "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 196
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    return-wide v2
.end method


# virtual methods
.method public getAuthor()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->authors:Ljava/util/List;

    .line 48
    .local v0, "authors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 51
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBuyUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/JsonVolumeData;->isLimitedPreview()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->saleInfo:Lcom/google/android/apps/books/app/data/JsonSaleInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/app/data/JsonSaleInfo;->buyLink:Ljava/lang/String;

    .line 160
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCanonicalUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->canonicalVolumeLink:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->publishedDate:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getLastAccess()J
    .locals 4

    .prologue
    .line 178
    const-wide/16 v0, 0x0

    .line 179
    .local v0, "result":J
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    if-eqz v2, :cond_1

    .line 180
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->readingPosition:Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;

    if-eqz v2, :cond_0

    .line 181
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->readingPosition:Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;->updated:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/JsonVolumeData;->parseTime(Ljava/lang/String;)J

    move-result-wide v0

    .line 183
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->updated:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 184
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->updated:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/JsonVolumeData;->parseTime(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 187
    :cond_1
    return-wide v0
.end method

.method public getLocalCoverUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mLocalCoverUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getMaxOfflineDevices()I
    .locals 2

    .prologue
    .line 294
    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v0, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    .line 295
    .local v0, "downloadAccess":Lcom/google/android/apps/books/api/data/DownloadAccessResponse;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->restricted:Z

    if-nez v1, :cond_1

    .line 296
    :cond_0
    const v1, 0x7fffffff

    .line 298
    :goto_0
    return v1

    :cond_1
    iget v1, v0, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->maxDownloadDevices:I

    goto :goto_0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->pageCount:I

    return v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->publisher:Ljava/lang/String;

    return-object v0
.end method

.method public getReadingPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->readingPosition:Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->readingPosition:Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$ReadingPosition;->textPosition:Ljava/lang/String;

    .line 173
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRentalExpiration()J
    .locals 6

    .prologue
    .line 273
    const-wide v2, 0x7fffffffffffffffL

    .line 274
    .local v2, "result":J
    iget-object v4, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v4, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    .line 275
    .local v1, "userInfo":Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;
    if-eqz v1, :cond_0

    .line 276
    iget-object v0, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->rentalPeriod:Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;

    .line 277
    .local v0, "rentalPeriod":Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    if-eqz v0, :cond_0

    .line 279
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;->endUtcSec:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "000"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 284
    .end local v0    # "rentalPeriod":Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    :cond_0
    :goto_0
    return-wide v2

    .line 280
    .restart local v0    # "rentalPeriod":Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public getRentalStart()J
    .locals 6

    .prologue
    .line 257
    const-wide/16 v2, 0x0

    .line 258
    .local v2, "result":J
    iget-object v4, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v4, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    .line 259
    .local v1, "userInfo":Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;
    if-eqz v1, :cond_0

    .line 260
    iget-object v0, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->rentalPeriod:Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;

    .line 261
    .local v0, "rentalPeriod":Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    if-eqz v0, :cond_0

    .line 263
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;->startUtcSec:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "000"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 268
    .end local v0    # "rentalPeriod":Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    :cond_0
    :goto_0
    return-wide v2

    .line 264
    .restart local v0    # "rentalPeriod":Lcom/google/android/apps/books/api/data/ApiaryVolume$RentalPeriod;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public getRentalState()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    .line 252
    .local v0, "userInfo":Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->rentalState:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getServerCoverUri()Ljava/lang/String;
    .locals 4

    .prologue
    .line 131
    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->imageLinks:Lcom/google/android/apps/books/api/data/ApiaryVolume$ImageLinks;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->imageLinks:Lcom/google/android/apps/books/api/data/ApiaryVolume$ImageLinks;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$ImageLinks;->thumbnail:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->imageLinks:Lcom/google/android/apps/books/api/data/ApiaryVolume$ImageLinks;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$ImageLinks;->thumbnail:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/util/Config;->makeRelative(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 140
    .local v0, "relativeUri":Landroid/net/Uri;
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "zoom"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "edge"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "l"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "w"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "h"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/UriUtils;->dropQueryParam(Landroid/net/Uri;[Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 142
    .end local v0    # "relativeUri":Landroid/net/Uri;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTextToSpeechPermission()Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    .locals 4

    .prologue
    .line 239
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v1, v1, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->textToSpeechPermission:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/model/VolumeDataUtils;->stringToTtsPermission(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$TtsPermission;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 245
    :goto_0
    return-object v1

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "JsonVolumeData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    const-string v1, "JsonVolumeData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error accessing TTS permission: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/VolumeData$TtsPermission;->NOT_ALLOWED:Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getViewability()Ljava/lang/String;
    .locals 3

    .prologue
    .line 116
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->viewability:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;->NO_PAGES:Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;

    .line 119
    .local v1, "viewability":Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;
    :goto_0
    sget-object v2, Lcom/google/android/apps/books/model/JsonVolumeData;->sNewToOldViewabilityMap:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v2, v1}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 121
    .end local v1    # "viewability":Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;
    :goto_1
    return-object v2

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->viewability:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/JsonVolumeData$Viewability;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "http://schemas.google.com/books/2008#view_no_pages"

    goto :goto_1
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->id:Ljava/lang/String;

    return-object v0
.end method

.method public isLimitedPreview()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 57
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v3, v3, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v3, v3, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->accessViewStatus:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;

    move-result-object v0

    .line 59
    .local v0, "accessViewStatus":Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;
    sget-object v3, Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;->SAMPLE:Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    .line 61
    .end local v0    # "accessViewStatus":Lcom/google/android/apps/books/model/JsonVolumeData$AccessViewStatus;
    :cond_0
    :goto_0
    return v2

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public isPublicDomain()Z
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->publicDomain:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->publicDomain:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 233
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isQuoteSharingAllowed()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->quoteSharingAllowed:Z

    return v0
.end method

.method public isUploaded()Z
    .locals 4

    .prologue
    .line 73
    const-string v2, "CheatTIsUpload"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    .line 74
    .local v0, "cheating":Z
    if-eqz v0, :cond_0

    .line 75
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->volumeInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$VolumeInfo;->title:Ljava/lang/String;

    const-string v3, "T"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    .line 76
    .local v1, "startsWithT":Z
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/JsonVolumeData;->isLimitedPreview()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    const/4 v2, 0x1

    .line 83
    .end local v1    # "startsWithT":Z
    :goto_0
    return v2

    .line 80
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    if-eqz v2, :cond_1

    .line 81
    iget-object v2, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume;->userInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;

    iget-boolean v2, v2, Lcom/google/android/apps/books/api/data/ApiaryVolume$UserInfo;->isUploaded:Z

    goto :goto_0

    .line 83
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public usesExplicitOfflineLicenseManagement()Z
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/books/model/JsonVolumeData;->mApiaryVolume:Lcom/google/android/apps/books/api/data/ApiaryVolume;

    iget-object v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume;->accessInfo:Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;

    iget-boolean v0, v0, Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;->explicitOfflineLicenseManagement:Z

    return v0
.end method
