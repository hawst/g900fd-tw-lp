.class Lcom/google/android/apps/books/service/VolumeSyncer$5;
.super Ljava/lang/Object;
.source "VolumeSyncer.java"

# interfaces
.implements Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/service/VolumeSyncer;->planPageStructure(Lcom/google/android/apps/books/model/Page;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

.field final synthetic val$page:Lcom/google/android/apps/books/model/Page;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/VolumeSyncer;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$5;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    iput-object p2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$5;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/service/VolumeSyncer$5;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ensure()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/service/VolumeSyncer$5;->this$0:Lcom/google/android/apps/books/service/VolumeSyncer;

    # getter for: Lcom/google/android/apps/books/service/VolumeSyncer;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v1}, Lcom/google/android/apps/books/service/VolumeSyncer;->access$400(Lcom/google/android/apps/books/service/VolumeSyncer;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/service/VolumeSyncer$5;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/service/VolumeSyncer$5;->val$page:Lcom/google/android/apps/books/model/Page;

    sget-object v4, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensurePageStructure(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v1

    throw v1
.end method
