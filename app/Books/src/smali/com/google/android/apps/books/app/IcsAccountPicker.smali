.class public Lcom/google/android/apps/books/app/IcsAccountPicker;
.super Ljava/lang/Object;
.source "IcsAccountPicker.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AccountPicker;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public pickAccount(Landroid/app/Activity;Landroid/accounts/Account;I)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "currentAccount"    # Landroid/accounts/Account;
    .param p3, "requestCode"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 24
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "com.google"

    aput-object v0, v2, v4

    .line 25
    .local v2, "accountTypes":[Ljava/lang/String;
    new-array v6, v4, [Ljava/lang/String;

    move-object v0, p2

    move-object v4, v1

    move-object v5, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    .line 27
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v8, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 28
    return-void
.end method
