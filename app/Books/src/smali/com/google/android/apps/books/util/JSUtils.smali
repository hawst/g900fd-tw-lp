.class public Lcom/google/android/apps/books/util/JSUtils;
.super Ljava/lang/Object;
.source "JSUtils.java"


# direct methods
.method public static varargs buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 8
    .param p0, "method"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 22
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .local v5, "out":Ljava/lang/StringBuilder;
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x28

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 27
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/Object;
    :try_start_0
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_5

    aget-object v0, v1, v3

    .line 28
    .local v0, "arg":Ljava/lang/Object;
    instance-of v6, v0, Lorg/json/JSONArray;

    if-nez v6, :cond_0

    instance-of v6, v0, Lorg/json/JSONObject;

    if-eqz v6, :cond_1

    .line 29
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .end local v0    # "arg":Ljava/lang/Object;
    :goto_1
    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 27
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 30
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_1
    instance-of v6, v0, Ljava/lang/Boolean;

    if-eqz v6, :cond_2

    .line 31
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 42
    .end local v0    # "arg":Ljava/lang/Object;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catch_0
    move-exception v2

    .line 43
    .local v2, "e":Lorg/json/JSONException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "problem escaping arguments"

    invoke-direct {v6, v7, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 32
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v0    # "arg":Ljava/lang/Object;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_2
    :try_start_1
    instance-of v6, v0, Ljava/lang/Number;

    if-eqz v6, :cond_3

    .line 33
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "arg":Ljava/lang/Object;
    invoke-static {v0}, Lorg/json/JSONObject;->numberToString(Ljava/lang/Number;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 34
    .restart local v0    # "arg":Ljava/lang/Object;
    :cond_3
    if-nez v0, :cond_4

    .line 35
    const-string v6, "undefined"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 37
    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 47
    .end local v0    # "arg":Ljava/lang/Object;
    :cond_5
    if-eqz p1, :cond_6

    array-length v6, p1

    if-lez v6, :cond_6

    .line 48
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 51
    :cond_6
    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
