.class public final Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "UploadProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/upload/proto/UploadProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadsList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;


# instance fields
.field private memoizedSerializedSize:I

.field private upload_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 883
    new-instance v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    .line 884
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto;->internalForceInit()V

    .line 885
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    invoke-direct {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->initFields()V

    .line 886
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 611
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;

    .line 635
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->memoizedSerializedSize:I

    .line 596
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->initFields()V

    .line 597
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/upload/proto/UploadProto$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$1;

    .prologue
    .line 592
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 611
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;

    .line 635
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->memoizedSerializedSize:I

    .line 598
    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 592
    iput-object p1, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .locals 1

    .prologue
    .line 602
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->defaultInstance:Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 622
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    .locals 1

    .prologue
    .line 716
    # invokes: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->create()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->access$1800()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 674
    invoke-static {}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->newBuilder()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;

    # invokes: Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->buildParsed()Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;
    invoke-static {v0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;->access$1700(Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList$Builder;)Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getSerializedSize()I
    .locals 5

    .prologue
    .line 637
    iget v2, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->memoizedSerializedSize:I

    .line 638
    .local v2, "size":I
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    .line 646
    .end local v2    # "size":I
    .local v3, "size":I
    :goto_0
    return v3

    .line 640
    .end local v3    # "size":I
    .restart local v2    # "size":I
    :cond_0
    const/4 v2, 0x0

    .line 641
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->getUploadList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 642
    .local v0, "element":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    .line 644
    goto :goto_1

    .line 645
    .end local v0    # "element":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    :cond_1
    iput v2, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->memoizedSerializedSize:I

    move v3, v2

    .line 646
    .end local v2    # "size":I
    .restart local v3    # "size":I
    goto :goto_0
.end method

.method public getUploadList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;",
            ">;"
        }
    .end annotation

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->upload_:Ljava/util/List;

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 624
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->getSerializedSize()I

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/books/upload/proto/UploadProto$UploadsList;->getUploadList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;

    .line 631
    .local v0, "element":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    goto :goto_0

    .line 633
    .end local v0    # "element":Lcom/google/android/apps/books/upload/proto/UploadProto$Upload;
    :cond_0
    return-void
.end method
