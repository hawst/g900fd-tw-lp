.class public Lcom/google/android/apps/books/util/GlueWarpListAdapter;
.super Ljava/lang/Object;
.source "GlueWarpListAdapter.java"

# interfaces
.implements Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;"
    }
.end annotation


# instance fields
.field private final mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mLeftGluePoint:I

.field private final mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mRightGluePoint:I


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;ILcom/google/android/ublib/widget/AbsWarpListView$Adapter;I)V
    .locals 0
    .param p2, "leftGluePoint"    # I
    .param p4, "rightGluePoint"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;ITT;I)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    .local p1, "leftAdapter":Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;, "TT;"
    .local p3, "rightAdapter":Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 29
    iput-object p3, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    .line 31
    iput p2, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftGluePoint:I

    .line 32
    iput p4, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightGluePoint:I

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    .local p1, "leftAdapter":Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;, "TT;"
    .local p2, "rightAdapter":Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;, "TT;"
    invoke-interface {p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v0

    invoke-interface {p2}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v1

    neg-int v1, v1

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;ILcom/google/android/ublib/widget/AbsWarpListView$Adapter;I)V

    .line 37
    return-void
.end method


# virtual methods
.method public getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .locals 1
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftGluePoint:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    goto :goto_0
.end method

.method public getChildPosition(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 56
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftGluePoint:I

    if-ge p1, v0, :cond_0

    .end local p1    # "position":I
    :goto_0
    return p1

    .restart local p1    # "position":I
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftGluePoint:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightGluePoint:I

    add-int p1, v0, v1

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 102
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightGluePoint:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftGluePoint:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 113
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    .line 114
    .local v0, "adapter":Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemId(I)J

    move-result-wide v4

    const/4 v1, 0x1

    shl-long v2, v4, v1

    .line 115
    .local v2, "itemId":J
    iget-object v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    if-ne v0, v1, :cond_0

    .end local v2    # "itemId":J
    :goto_0
    return-wide v2

    .restart local v2    # "itemId":J
    :cond_0
    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 130
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    .line 131
    .local v0, "adapter":Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v2

    invoke-interface {v0, v2}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getItemViewType(I)I

    move-result v1

    .line 132
    .local v1, "itemType":I
    iget-object v2, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    if-ne v0, v2, :cond_0

    .end local v1    # "itemType":I
    :goto_0
    return v1

    .restart local v1    # "itemType":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v2}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewTypeCount()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method public getLeftAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 146
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    return-object v0
.end method

.method public getNegativeCount()I
    .locals 1

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getNegativeCount()I

    move-result v0

    return v0
.end method

.method public getRightAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 150
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 125
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewSize(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 76
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewSize(I)I

    move-result v0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 2

    .prologue
    .line 137
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewTypeCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->getViewTypeCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 120
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 142
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isViewPinned(Landroid/view/View;I)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->isViewPinned(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public preload(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->preload(I)V

    .line 62
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 2
    .param p1, "dataSetObserver"    # Landroid/database/DataSetObserver;

    .prologue
    .line 86
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iget-object v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    if-eq v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 90
    :cond_0
    return-void
.end method

.method public setViewVisible(Landroid/view/View;IZ)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "isVisible"    # Z

    .prologue
    .line 66
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, p1, v1, p3}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->setViewVisible(Landroid/view/View;IZ)V

    .line 67
    return-void
.end method

.method public transformView(Landroid/view/View;I)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 81
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildAdapter(I)Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->getChildPosition(I)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->transformView(Landroid/view/View;I)V

    .line 82
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 2
    .param p1, "dataSetObserver"    # Landroid/database/DataSetObserver;

    .prologue
    .line 94
    .local p0, "this":Lcom/google/android/apps/books/util/GlueWarpListAdapter;, "Lcom/google/android/apps/books/util/GlueWarpListAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mLeftAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    iget-object v1, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    if-eq v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/util/GlueWarpListAdapter;->mRightAdapter:Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 98
    :cond_0
    return-void
.end method
