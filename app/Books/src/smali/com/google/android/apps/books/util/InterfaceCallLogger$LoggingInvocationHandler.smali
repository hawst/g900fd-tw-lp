.class Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;
.super Ljava/lang/Object;
.source "InterfaceCallLogger.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/InterfaceCallLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoggingInvocationHandler"
.end annotation


# instance fields
.field final mBaseInstance:Ljava/lang/Object;

.field final mMessageLogger:Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;


# direct methods
.method constructor <init>(Ljava/lang/Object;Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;)V
    .locals 3
    .param p1, "baseInstance"    # Ljava/lang/Object;
    .param p2, "messageLogger"    # Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mBaseInstance:Ljava/lang/Object;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mMessageLogger:Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mMessageLogger:Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting trace on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    # getter for: Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->access$000()Ljava/lang/ThreadLocal;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 41
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 18
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    .line 75
    .local v2, "declaringClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v11, Ljava/lang/Object;

    invoke-virtual {v2, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const-class v11, Lcom/google/android/apps/books/util/InterfaceCallLogger$Quiet;

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 78
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mBaseInstance:Ljava/lang/Object;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v11, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 120
    :goto_0
    return-object v10

    .line 79
    :catch_0
    move-exception v3

    .line 80
    .local v3, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v11

    throw v11

    .line 84
    .end local v3    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    .local v7, "msg":Ljava/lang/StringBuilder;
    # getter for: Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->access$000()Ljava/lang/ThreadLocal;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 86
    .local v6, "indent":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v6, :cond_2

    .line 87
    const-string v11, "v "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 90
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    const/16 v11, 0x2e

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 92
    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const/16 v11, 0x28

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 94
    invoke-static/range {p3 .. p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v11}, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->logIterable(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)V

    .line 95
    const-string v11, ")"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 98
    .local v12, "startNanos":J
    const/4 v4, 0x0

    .line 101
    .local v4, "error":Ljava/lang/Throwable;
    :try_start_1
    # getter for: Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->access$000()Ljava/lang/ThreadLocal;

    move-result-object v11

    add-int/lit8 v14, v6, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 102
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mBaseInstance:Ljava/lang/Object;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v11, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .line 103
    .local v10, "result":Ljava/lang/Object;
    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v11

    sget-object v14, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-virtual {v11, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 104
    const-string v11, " = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v10}, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->logObject(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    :cond_3
    sget-object v11, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    sub-long/2addr v14, v12

    sget-object v16, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v16

    invoke-virtual {v11, v14, v15, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    .line 115
    .local v8, "millis":J
    const-string v11, " ["

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 117
    const-string v11, "ms]"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mMessageLogger:Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v14, v4}, Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 119
    # getter for: Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->access$000()Ljava/lang/ThreadLocal;

    move-result-object v11

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 108
    .end local v8    # "millis":J
    .end local v10    # "result":Ljava/lang/Object;
    :catch_1
    move-exception v3

    .line 109
    .restart local v3    # "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    .line 110
    const-string v11, " FAILED"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 113
    .end local v3    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catchall_0
    move-exception v11

    sget-object v14, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    sub-long v16, v16, v12

    sget-object v15, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1, v15}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    .line 115
    .restart local v8    # "millis":J
    const-string v14, " ["

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 117
    const-string v14, "ms]"

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->mMessageLogger:Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15, v4}, Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;->log(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 119
    # getter for: Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;
    invoke-static {}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->access$000()Ljava/lang/ThreadLocal;

    move-result-object v14

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 120
    throw v11
.end method

.method logIterable(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)V
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/Iterable",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p2, "iterable":Ljava/lang/Iterable;, "Ljava/lang/Iterable<*>;"
    const/4 v1, 0x1

    .line 45
    .local v1, "isFirst":Z
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 46
    .local v2, "obj":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 47
    const-string v3, ", "

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    :cond_0
    const/4 v1, 0x0

    .line 50
    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->logObject(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    .end local v2    # "obj":Ljava/lang/Object;
    :cond_1
    return-void
.end method

.method logObject(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 55
    if-nez p2, :cond_0

    .line 56
    const-string v1, "null"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 58
    const/16 v1, 0x5b

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 59
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 60
    if-eqz v0, :cond_1

    .line 61
    const-string v1, ", "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    :cond_1
    invoke-static {p2, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;->logObject(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 65
    :cond_2
    const/16 v1, 0x5d

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 67
    .end local v0    # "i":I
    :cond_3
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
