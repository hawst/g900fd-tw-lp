.class public Lcom/google/android/apps/books/service/SyncService$StubSyncUi;
.super Ljava/lang/Object;
.source "SyncService.java"

# interfaces
.implements Lcom/google/android/apps/books/service/SyncService$SyncUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/SyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StubSyncUi"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addedVolumes(Landroid/accounts/Account;Ljava/util/List;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p2, "volumeInfo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    return-void
.end method

.method public cancelDownloadNotification(Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 224
    return-void
.end method

.method public finishedAllVolumeDownloads()V
    .locals 0

    .prologue
    .line 216
    return-void
.end method

.method public finishedVolumeDownload(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "fullyDownloaded"    # Z

    .prologue
    .line 212
    return-void
.end method

.method public shouldNotifyByDefault()Z
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    return v0
.end method

.method public startingVolumeDownload(Lcom/google/android/apps/books/model/VolumeData;IILandroid/accounts/Account;)V
    .locals 0
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "volumeNumber"    # I
    .param p3, "volumeCount"    # I
    .param p4, "account"    # Landroid/accounts/Account;

    .prologue
    .line 203
    return-void
.end method
