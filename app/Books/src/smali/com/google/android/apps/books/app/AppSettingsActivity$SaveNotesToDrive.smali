.class Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AppSettingsItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveNotesToDrive"
.end annotation


# instance fields
.field private mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

.field private final mUserSettingsConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/UserSettings;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mUserSettingsController:Lcom/google/android/apps/books/app/UserSettingsController;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V
    .locals 5
    .param p2, "context"    # Lcom/google/android/apps/books/app/AppSettingsActivity;

    .prologue
    .line 564
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 565
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v1, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 566
    .local v0, "account":Landroid/accounts/Account;
    new-instance v1, Lcom/google/android/apps/books/app/UserSettingsController;

    invoke-static {p2, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/UserSettingsController;-><init>(Lcom/google/android/apps/books/net/BooksServer;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mUserSettingsController:Lcom/google/android/apps/books/app/UserSettingsController;

    .line 570
    new-instance v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mUserSettingsConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 612
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mUserSettingsController:Lcom/google/android/apps/books/app/UserSettingsController;

    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mUserSettingsConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/UserSettingsController;->getUserSettings(Lcom/google/android/ublib/utils/Consumer;)V

    .line 613
    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;)Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    .prologue
    .line 557
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;)Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;
    .param p1, "x1"    # Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    .prologue
    .line 557
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    .prologue
    .line 557
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->defaultFolderName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 557
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->onFinished(ZLjava/lang/String;)V

    return-void
.end method

.method private defaultFolderName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 645
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 646
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0f0164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private onFinished(ZLjava/lang/String;)V
    .locals 3
    .param p1, "isEnabled"    # Z
    .param p2, "folderName"    # Ljava/lang/String;

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mUserSettingsController:Lcom/google/android/apps/books/app/UserSettingsController;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->safeFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mUserSettingsConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/books/app/UserSettingsController;->updateSaveNotes(ZLjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 657
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_SAVE_NOTES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    # invokes: Lcom/google/android/apps/books/app/AppSettingsActivity;->logDisplayOptionsAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->access$400(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;Landroid/content/Context;)V

    .line 659
    return-void
.end method

.method private safeFolderName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 650
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->defaultFolderName()Ljava/lang/String;

    move-result-object p1

    .end local p1    # "folderName":Ljava/lang/String;
    :cond_0
    return-object p1
.end method


# virtual methods
.method public getView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 617
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 618
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040027

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 619
    .local v3, "view":Landroid/view/View;
    const v4, 0x7f0e00af

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 620
    .local v1, "tv":Landroid/widget/TextView;
    const v4, 0x7f0f0160

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 623
    const v4, 0x7f0e00b0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 624
    .local v2, "tv_summary":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    if-nez v4, :cond_0

    .line 625
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 631
    :goto_0
    return-object v3

    .line 626
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    iget-boolean v4, v4, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->isEnabled:Z

    if-nez v4, :cond_1

    .line 627
    const v4, 0x7f0f0161

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 629
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    iget-object v4, v4, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->folderName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 636
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    if-eqz v1, :cond_0

    .line 637
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    iget-boolean v1, v1, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->isEnabled:Z

    iget-object v2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->mNotesExport:Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;

    iget-object v2, v2, Lcom/google/android/apps/books/api/data/UserSettings$NotesExport;->folderName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->newInstance(ZLjava/lang/String;)Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    move-result-object v0

    .line 639
    .local v0, "dialog":Lcom/google/android/apps/books/app/NotesExportDialogFragment;
    iget-object v1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/AppSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "NotesExportDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 642
    .end local v0    # "dialog":Lcom/google/android/apps/books/app/NotesExportDialogFragment;
    :cond_0
    return-void
.end method
