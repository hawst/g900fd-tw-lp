.class public interface abstract Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SessionKeyValidator"
.end annotation


# virtual methods
.method public abstract validate(Lcom/google/android/apps/books/model/SessionKey;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;
        }
    .end annotation
.end method
