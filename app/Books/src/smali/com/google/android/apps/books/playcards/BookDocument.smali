.class public Lcom/google/android/apps/books/playcards/BookDocument;
.super Lcom/google/android/ublib/cardlib/model/Document;
.source "BookDocument.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/playcards/BookDocument$2;,
        Lcom/google/android/apps/books/playcards/BookDocument$Saleability;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCanonicalVolumeLink:Ljava/lang/String;

.field private mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

.field private mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

.field private mVolumeId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/apps/books/playcards/BookDocument$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/playcards/BookDocument$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/playcards/BookDocument;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/model/Document;-><init>()V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/books/playcards/BookDocument;->reset()V

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;Landroid/content/Context;)V
    .locals 2
    .param p1, "book"    # Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/ublib/cardlib/model/Document;-><init>()V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/books/playcards/BookDocument;->reset()V

    .line 42
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setTitle(Ljava/lang/String;)V

    .line 43
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->author:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setSubTitle(Ljava/lang/String;)V

    .line 44
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->reason:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setReason1(Ljava/lang/String;)V

    .line 45
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->saleability:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setSaleability(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/books/playcards/BookDocument;->getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->FREE:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    if-ne v0, v1, :cond_0

    .line 47
    const v0, 0x7f0f008c

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setPrice(Ljava/lang/String;)V

    .line 51
    :goto_0
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->coverUri:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setThumbnailURI(Landroid/net/Uri;)V

    .line 52
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setVolumeId(Ljava/lang/String;)V

    .line 53
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->canonicalVolumeLink:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setCanonicalVolumeLink(Ljava/lang/String;)V

    .line 54
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->purchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setPurchaseInfo(Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 55
    return-void

    .line 49
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->purchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/app/PurchaseInfo;->lowestPriceString:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/playcards/BookDocument;->setPrice(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/playcards/BookDocument;Landroid/os/Parcel;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p1, "x1"    # Landroid/os/Parcel;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/playcards/BookDocument;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/playcards/BookDocument;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/books/playcards/BookDocument;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mCanonicalVolumeLink:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/apps/books/app/PurchaseInfo;)Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    return-object p1
.end method

.method private setPurchaseInfo(Lcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 0
    .param p1, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    .line 98
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public getActionTextId()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 146
    sget-object v0, Lcom/google/android/apps/books/playcards/BookDocument$2;->$SwitchMap$com$google$android$apps$books$playcards$BookDocument$Saleability:[I

    invoke-virtual {p0}, Lcom/google/android/apps/books/playcards/BookDocument;->getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 156
    const-string v0, "BookDocument"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "BookDocument"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received Invalid saleability: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 150
    :pswitch_0
    const v0, 0x7f0f008a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 152
    :pswitch_1
    const v0, 0x7f0f008b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 154
    :pswitch_2
    const v0, 0x7f0f008d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCanonicalVolumeLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mCanonicalVolumeLink:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseInfo()Lcom/google/android/apps/books/app/PurchaseInfo;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    return-object v0
.end method

.method public getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    return-object v0
.end method

.method public getVolumeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-super {p0}, Lcom/google/android/ublib/cardlib/model/Document;->reset()V

    .line 64
    iput-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    .line 66
    return-void
.end method

.method public setCanonicalVolumeLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "canonicalVolumeLink"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mCanonicalVolumeLink:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setSaleability(Ljava/lang/String;)V
    .locals 4
    .param p1, "saleability"    # Ljava/lang/String;

    .prologue
    .line 82
    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BookDocument"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    const-string v1, "BookDocument"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Illegal saleability "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for volume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->NOT_FOR_SALE:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    iput-object v1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    goto :goto_0
.end method

.method public setVolumeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BookDocument [mVolumeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/google/android/ublib/cardlib/model/Document;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Lcom/google/android/ublib/cardlib/model/Document;->writeToParcel(Landroid/os/Parcel;I)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mVolumeId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mSaleability:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    invoke-virtual {v0}, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mCanonicalVolumeLink:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/books/playcards/BookDocument;->mPurchaseInfo:Lcom/google/android/apps/books/app/PurchaseInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/PurchaseInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 117
    return-void
.end method
