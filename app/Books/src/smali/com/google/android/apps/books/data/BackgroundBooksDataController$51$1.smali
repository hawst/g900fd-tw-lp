.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;->run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 0

    .prologue
    .line 1385
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 1388
    .local p1, "sessionKeyResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1389
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;

    iget-object v0, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;->val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1396
    :goto_0
    return-void

    .line 1394
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;

    iget-object v1, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;

    iget-object v3, v3, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;->val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->fetchDictionaryMetadata(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$5100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1385
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
