.class Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingPage"
.end annotation


# instance fields
.field final bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

.field final cookie:Lcom/google/android/apps/books/util/RenderRequestContext;

.field final page:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final painter:Lcom/google/android/apps/books/render/PagePainter;

.field final transitionStartTime:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p5, "transitionStartTime"    # J

    .prologue
    .line 859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 860
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 861
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->cookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    .line 862
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->painter:Lcom/google/android/apps/books/render/PagePainter;

    .line 863
    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 864
    iput-wide p5, p0, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->transitionStartTime:J

    .line 865
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;JLcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 1
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x1"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "x2"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "x3"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p5, "x4"    # J
    .param p7, "x5"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 850
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;-><init>(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V

    return-void
.end method
