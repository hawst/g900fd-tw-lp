.class Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenuCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 4033
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 4033
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Lcom/google/android/apps/books/app/ReaderMenu$Item;)V
    .locals 14
    .param p1, "item"    # Lcom/google/android/apps/books/app/ReaderMenu$Item;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 4111
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    .line 4112
    .local v1, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4113
    sget-object v11, Lcom/google/android/apps/books/app/ReaderFragment$42;->$SwitchMap$com$google$android$apps$books$app$ReaderMenu$Item:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReaderMenu$Item;->ordinal()I

    move-result v12

    aget v11, v11, v12

    packed-switch v11, :pswitch_data_0

    .line 4238
    :cond_0
    :goto_0
    return-void

    .line 4115
    :pswitch_0
    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->onHomePressed()V

    goto :goto_0

    .line 4118
    :pswitch_1
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TableOfContents;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 4119
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TableOfContents;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/widget/TableOfContents;->show(Landroid/view/View;)V

    goto :goto_0

    .line 4123
    :pswitch_2
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v11

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getOtherMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-static {v10, v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v4

    .line 4124
    .local v4, "newMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSettingsListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v10

    invoke-interface {v10, v4, v9}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onReadingModeChanged(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Z)V

    goto :goto_0

    .line 4128
    .end local v4    # "newMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->showDisplayOptions()V

    .line 4129
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->onUserInteraction()V

    goto :goto_0

    .line 4133
    :pswitch_4
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    const v10, 0x7f0f00b9

    invoke-static {v9, v10}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnectedElseToast(Landroid/content/Context;I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 4135
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v8

    .line 4136
    .local v8, "volumeId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCanonicalUrl()Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9400(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v2

    .line 4137
    .local v2, "canonicalUrl":Ljava/lang/String;
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v9}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 4138
    const-string v9, "books_inapp_reader_options_about"

    invoke-interface {v1, v8, v2, v9}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4144
    .end local v2    # "canonicalUrl":Ljava/lang/String;
    .end local v8    # "volumeId":Ljava/lang/String;
    :pswitch_5
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookmarkController:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;

    invoke-direct {v10, p0}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;)V

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 4169
    :pswitch_6
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v10, Lcom/google/android/apps/books/util/FinskyCampaignIds;->READER_OPTIONS_SHARE:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getShareUrl(Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1300(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 4170
    .local v7, "uri":Landroid/net/Uri;
    sget-object v9, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->READER_ACTION_SHARE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v9}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 4171
    if-eqz v7, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 4172
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTitle()Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1500(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAuthor()Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1400(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v1, v9, v10, v7}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startShare(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 4177
    .end local v7    # "uri":Landroid/net/Uri;
    :pswitch_7
    invoke-static {}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHelp()V

    .line 4178
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 4179
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getRootView()Landroid/view/View;

    move-result-object v9

    invoke-static {v9, v0}, Lcom/google/android/apps/books/app/HelpUtils;->getScreenshot(Landroid/view/View;Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 4182
    .local v5, "screenshot":Landroid/graphics/Bitmap;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v9

    const-string v10, "mobile_book_object"

    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getAccount()Landroid/accounts/Account;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5600(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/accounts/Account;

    move-result-object v11

    invoke-interface {v9, v10, v11, v0, v5}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 4186
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v5    # "screenshot":Landroid/graphics/Bitmap;
    :pswitch_8
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    invoke-static {v11, v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v3

    .line 4187
    .local v3, "moController":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v12, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isMoSpeaking()Z
    invoke-static {v12}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v12

    if-nez v12, :cond_1

    :goto_1
    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z
    invoke-static {v11, v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 4188
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMoPlaybackEnabled:Z
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 4189
    invoke-virtual {v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->canResume()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 4190
    invoke-virtual {v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->resume()V

    .line 4194
    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v10, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4195
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v10, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;->START_MEDIA_OVERLAY_PLAYBACK:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStart(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V
    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$PlaybackAction;)V

    .line 4200
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->updateBarsVisibility()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4205
    :goto_3
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4206
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onAnyAudioPlayingChanged()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto/16 :goto_0

    :cond_1
    move v9, v10

    .line 4187
    goto :goto_1

    .line 4192
    :cond_2
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startMoAtCurrentPosition(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    invoke-static {v9, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10000(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    goto :goto_2

    .line 4202
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->stopSpeaking()V

    .line 4203
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStopped()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10400(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_3

    .line 4210
    .end local v3    # "moController":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    :pswitch_9
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabled:Z
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v11

    if-nez v11, :cond_4

    move v6, v9

    .line 4211
    .local v6, "startSpeech":Z
    :goto_4
    if-eqz v6, :cond_5

    .line 4212
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v10, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 4217
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;

    invoke-direct {v10, p0}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks$2;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;)V

    const-wide/16 v12, 0x12c

    invoke-virtual {v9, v10, v12, v13}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4230
    :goto_5
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto/16 :goto_0

    .end local v6    # "startSpeech":Z
    :cond_4
    move v6, v10

    .line 4210
    goto :goto_4

    .line 4226
    .restart local v6    # "startSpeech":Z
    :cond_5
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTtsEnabledSetByUser:Z
    invoke-static {v11, v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10702(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 4227
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeSetTtsEnabled(Z)V
    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7600(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 4228
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeLogPlaybackStopped()V
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10400(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_5

    .line 4234
    .end local v6    # "startSpeech":Z
    :pswitch_a
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-static {v9, v10}, Lcom/google/android/apps/books/app/BaseBooksActivity;->showAppSettingsActivity(Landroid/app/Activity;Z)V

    goto/16 :goto_0

    .line 4113
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public onSearchClosed()Z
    .locals 2

    .prologue
    .line 4073
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4074
    const-string v0, "ReaderFragment"

    const-string v1, "mSearchMenu.onMenuItemActionCollapse()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4086
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4087
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->backOutOfSearch()Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    .line 4091
    :goto_0
    return v0

    .line 4089
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->clearSearchMatches()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8200(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4091
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSearchFieldFocusChanged(Z)V
    .locals 6
    .param p1, "hasFocus"    # Z

    .prologue
    const/4 v3, 0x1

    .line 4041
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v0, 0x0

    .line 4042
    .local v0, "currentQuery":Ljava/lang/CharSequence;
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    .line 4045
    .local v2, "searchQueryIsCurrent":Z
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->showingSearchResults()Z

    move-result v1

    .line 4047
    .local v1, "resultsCanBeShown":Z
    if-eqz p1, :cond_1

    .line 4048
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingActionBar:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v1, :cond_4

    .line 4050
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V
    invoke-static {v4, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6400(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 4054
    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4056
    :cond_1
    return-void

    .line 4041
    .end local v0    # "currentQuery":Ljava/lang/CharSequence;
    .end local v1    # "resultsCanBeShown":Z
    .end local v2    # "searchQueryIsCurrent":Z
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/app/ReaderMenu;->getSearchQuery()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 4042
    .restart local v0    # "currentQuery":Ljava/lang/CharSequence;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 4051
    .restart local v1    # "resultsCanBeShown":Z
    .restart local v2    # "searchQueryIsCurrent":Z
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowRecentSearchesPopup()Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4052
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->requestRecentSearches()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8100(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_2
.end method

.method public onSearchQuerySubmitted(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 4060
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7702(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 4061
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7902(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 4062
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->clearSearchMatches()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8200(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4064
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setShowSearchResults(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6400(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 4065
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4067
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->startSearch(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8400(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)V

    .line 4068
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getVolumeId()Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/data/BooksDataController;->setLastVolumeSearch(Ljava/lang/String;Ljava/lang/String;)V

    .line 4069
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 4257
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onInternalUserInteraction()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 4258
    return-void
.end method

.method public showDisplayOptions()V
    .locals 1

    .prologue
    .line 4242
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4243
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReaderMenu;->clearSearchViewFocus()V

    .line 4245
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->showDisplayOptions()V

    .line 4246
    return-void
.end method

.method public showingSearchResults()Z
    .locals 1

    .prologue
    .line 4106
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->showingSearchResults()Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    return v0
.end method

.method public startTableOfContentsActivity(Lcom/google/android/apps/books/app/ContentsView$Arguments;)V
    .locals 3
    .param p1, "arguments"    # Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .prologue
    .line 4250
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$MenuCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->prepareTableOfContentsActivity(Landroid/content/Context;Lcom/google/android/apps/books/app/ContentsView$Arguments;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 4253
    return-void
.end method
