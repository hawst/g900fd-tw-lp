.class Lcom/google/android/apps/books/data/OffersSubcontroller$5;
.super Ljava/lang/Object;
.source "OffersSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/OffersSubcontroller;->saveServerOffersOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

.field final synthetic val$apiaryOffers:Lcom/google/android/apps/books/api/data/ApiaryOffers;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->val$apiaryOffers:Lcom/google/android/apps/books/api/data/ApiaryOffers;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 9
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    new-instance v1, Lcom/google/android/apps/books/model/CachedOffersImpl;

    iget-object v3, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->val$apiaryOffers:Lcom/google/android/apps/books/api/data/ApiaryOffers;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/apps/books/model/CachedOffersImpl;-><init>(Lcom/google/android/apps/books/api/data/ApiaryOffers;J)V

    # setter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$702(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->getFilteredCachedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$300(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v2

    .line 284
    .local v2, "filteredOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    invoke-static {v0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$700(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;->getLastModifiedMillis()J

    move-result-wide v6

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->publishResult(Ljava/util/List;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;J)V
    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$800(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/util/List;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;J)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    invoke-static {v1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$700(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;->getLastModifiedMillis()J

    move-result-wide v4

    move-object v1, p1

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->notifyListeners(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$400(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V

    .line 289
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->val$apiaryOffers:Lcom/google/android/apps/books/api/data/ApiaryOffers;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->setCachedOffers(Lcom/google/android/apps/books/api/data/ApiaryOffers;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/OffersSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    invoke-static {v0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$900(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    .end local v2    # "filteredOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v8

    .line 292
    .local v8, "e":Ljava/io/IOException;
    const-string v0, "OffersSubcontroller"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    const-string v0, "OffersSubcontroller"

    const-string v1, "Could not cache offers"

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/OffersSubcontroller;

    # invokes: Lcom/google/android/apps/books/data/OffersSubcontroller;->publishFailure(Ljava/lang/Exception;)V
    invoke-static {v0, v8}, Lcom/google/android/apps/books/data/OffersSubcontroller;->access$1000(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/lang/Exception;)V

    goto :goto_0
.end method
