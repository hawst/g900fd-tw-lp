.class public Lcom/google/android/apps/books/app/HomeFragment$DeleteFromLibraryDialog;
.super Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteFromLibraryDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2509
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected onOkClicked(Z)V
    .locals 8
    .param p1, "checkboxChecked"    # Z

    .prologue
    .line 2512
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$DeleteFromLibraryDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 2513
    .local v2, "args":Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 2514
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    .line 2515
    .local v3, "volumeId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$DeleteFromLibraryDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2516
    .local v1, "activity":Landroid/app/Activity;
    sget-object v4, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;->HOME_OVERFLOW_DELETE_FROM_LIBRARY_CONFIRMED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;

    const/4 v5, 0x0

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeOverflowAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeOverflowAction;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/Context;)V

    .line 2520
    new-instance v4, Lcom/google/android/apps/books/model/CollectionVolumesEditor;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v5

    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/books/model/CollectionVolumesEditor;-><init>(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;)V

    const-wide/16 v6, 0x7

    invoke-virtual {v4, v6, v7, v3}, Lcom/google/android/apps/books/model/CollectionVolumesEditor;->backgroundRemove(JLjava/lang/String;)Landroid/os/AsyncTask;

    .line 2523
    return-void
.end method
