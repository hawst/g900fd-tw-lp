.class public Lcom/google/android/apps/books/model/PublisherCssUtils;
.super Ljava/lang/Object;
.source "PublisherCssUtils.java"


# direct methods
.method public static buildSegmentIdToCssIndices(Ljava/util/Collection;Ljava/util/Map;Z)Ljava/util/Map;
    .locals 22
    .param p2, "favorVertical"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "segmentResources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SegmentResource;>;"
    .local p1, "cssResourceIdToCssIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/Collection;->size()I

    move-result v20

    .line 55
    .local v20, "segmentResourcesCount":I
    const/16 v21, 0x2

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v21

    div-int/lit8 v11, v21, 0x2

    .line 58
    .local v11, "estimatedSegments":I
    invoke-static {v11}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v18

    .line 61
    .local v18, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/Integer;>;>;"
    const/4 v2, 0x0

    .line 62
    .local v2, "alternateCssTitleToResIds":Lcom/google/common/collect/LinkedHashMultimap;, "Lcom/google/common/collect/LinkedHashMultimap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 68
    .local v3, "alternateStyleTitleToScore":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .line 69
    .local v10, "currentSegmentId":Ljava/lang/String;
    const/16 v16, 0x0

    .line 70
    .local v16, "preferredSetTitle":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/collect/Sets;->newLinkedHashSet()Ljava/util/LinkedHashSet;

    move-result-object v7

    .line 72
    .local v7, "cssIndicesForCurrentSegment":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/apps/books/model/SegmentResource;

    .line 73
    .local v19, "segmentResource":Lcom/google/android/apps/books/model/SegmentResource;
    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/model/SegmentResource;->getResourceId()Ljava/lang/String;

    move-result-object v17

    .line 75
    .local v17, "resourceId":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 76
    .local v6, "cssIndex":Ljava/lang/Integer;
    if-eqz v6, :cond_0

    .line 81
    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/model/SegmentResource;->getSegmentId()Ljava/lang/String;

    move-result-object v15

    .line 82
    .local v15, "possiblyNewSegmentId":Ljava/lang/String;
    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/model/SegmentResource;->getCssClass()Ljava/lang/String;

    move-result-object v5

    .line 83
    .local v5, "cssClass":Ljava/lang/String;
    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/model/SegmentResource;->getTitle()Ljava/lang/String;

    move-result-object v9

    .line 85
    .local v9, "cssTitle":Ljava/lang/String;
    if-nez v10, :cond_2

    .line 87
    invoke-static {}, Lcom/google/common/collect/LinkedHashMultimap;->create()Lcom/google/common/collect/LinkedHashMultimap;

    move-result-object v2

    .line 88
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 89
    move-object v10, v15

    .line 110
    :cond_1
    :goto_1
    invoke-static {v9}, Lcom/google/android/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 113
    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {v10, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_1

    .line 94
    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v21

    if-eqz v21, :cond_3

    invoke-virtual {v2}, Lcom/google/common/collect/LinkedHashMultimap;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_4

    .line 96
    :cond_3
    move-object/from16 v0, p1

    invoke-static {v7, v0, v2, v3}, Lcom/google/android/apps/books/model/PublisherCssUtils;->chooseAlternateStyleSet(Ljava/util/Collection;Ljava/util/Map;Lcom/google/common/collect/LinkedHashMultimap;Ljava/util/Map;)V

    .line 98
    invoke-static {v7}, Lcom/google/common/collect/ImmutableSet;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-virtual {v2}, Lcom/google/common/collect/LinkedHashMultimap;->clear()V

    .line 103
    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 104
    invoke-interface {v7}, Ljava/util/Set;->clear()V

    .line 106
    :cond_4
    move-object v10, v15

    .line 107
    const/16 v16, 0x0

    goto :goto_1

    .line 115
    :cond_5
    if-nez v16, :cond_6

    .line 119
    move-object/from16 v16, v9

    .line 120
    const-string v21, "preferred"

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-static {v9, v0, v3, v1}, Lcom/google/android/apps/books/model/PublisherCssUtils;->updateScoreForTag(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 126
    :cond_6
    const-string v21, "\\s+"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .local v4, "arr$":[Ljava/lang/String;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_2
    if-ge v13, v14, :cond_7

    aget-object v8, v4, v13

    .line 127
    .local v8, "cssTag":Ljava/lang/String;
    move/from16 v0, p2

    invoke-static {v9, v8, v3, v0}, Lcom/google/android/apps/books/model/PublisherCssUtils;->updateScoreForTag(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V

    .line 126
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 130
    .end local v8    # "cssTag":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, v17

    invoke-virtual {v2, v9, v0}, Lcom/google/common/collect/LinkedHashMultimap;->put(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 134
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v5    # "cssClass":Ljava/lang/String;
    .end local v6    # "cssIndex":Ljava/lang/Integer;
    .end local v9    # "cssTitle":Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "possiblyNewSegmentId":Ljava/lang/String;
    .end local v17    # "resourceId":Ljava/lang/String;
    .end local v19    # "segmentResource":Lcom/google/android/apps/books/model/SegmentResource;
    :cond_8
    move-object/from16 v0, p1

    invoke-static {v7, v0, v2, v3}, Lcom/google/android/apps/books/model/PublisherCssUtils;->chooseAlternateStyleSet(Ljava/util/Collection;Ljava/util/Map;Lcom/google/common/collect/LinkedHashMultimap;Ljava/util/Map;)V

    .line 136
    invoke-static {v7}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-object v18
.end method

.method private static chooseAlternateStyleSet(Ljava/util/Collection;Ljava/util/Map;Lcom/google/common/collect/LinkedHashMultimap;Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/common/collect/LinkedHashMultimap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p0, "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .local p1, "cssResourceIdToCssIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p2, "titleToResourceIds":Lcom/google/common/collect/LinkedHashMultimap;, "Lcom/google/common/collect/LinkedHashMultimap<Ljava/lang/String;Ljava/lang/String;>;"
    .local p3, "titleToScore":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 156
    .local v2, "bestStyleSetTitle":Ljava/lang/String;
    const/4 v4, 0x0

    .line 157
    .local v4, "highestScore":I
    if-eqz p3, :cond_3

    .line 158
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 159
    .local v6, "title":Ljava/lang/String;
    invoke-interface {p3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 160
    .local v3, "currentScore":I
    if-le v3, v4, :cond_0

    .line 161
    move-object v2, v6

    .line 162
    move v4, v3

    goto :goto_0

    .line 167
    .end local v3    # "currentScore":I
    .end local v6    # "title":Ljava/lang/String;
    :cond_1
    if-eqz v2, :cond_3

    .line 168
    invoke-virtual {p2, v2}, Lcom/google/common/collect/LinkedHashMultimap;->get(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 170
    .local v1, "alternateStyleResourceIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 171
    .local v0, "alternateStyleResourceId":Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p0, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 172
    const-string v7, "PublisherCssUtils"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 173
    const-string v7, "PublisherCssUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Using alternate style set: \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 178
    .end local v0    # "alternateStyleResourceId":Ljava/lang/String;
    .end local v1    # "alternateStyleResourceIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method private static getScoreForTag(Ljava/lang/String;Z)I
    .locals 3
    .param p0, "cssTag"    # Ljava/lang/String;
    .param p1, "favorVertical"    # Z

    .prologue
    const/4 v0, 0x4

    const/4 v1, -0x1

    .line 188
    const-string v2, "vertical"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    if-eqz p1, :cond_0

    .line 201
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 189
    goto :goto_0

    .line 190
    :cond_1
    const-string v2, "horizontal"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 191
    if-eqz p1, :cond_2

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1

    .line 195
    :cond_3
    const-string v0, "night"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 196
    const/4 v0, -0x2

    goto :goto_0

    .line 198
    :cond_4
    const-string v0, "preferred"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 199
    const/4 v0, 0x1

    goto :goto_0

    .line 201
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static updateScoreForTag(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 3
    .param p0, "cssTitle"    # Ljava/lang/String;
    .param p1, "cssTag"    # Ljava/lang/String;
    .param p3, "favorVertical"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 210
    .local p2, "titleToScore":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {p1, p3}, Lcom/google/android/apps/books/model/PublisherCssUtils;->getScoreForTag(Ljava/lang/String;Z)I

    move-result v0

    .line 211
    .local v0, "score":I
    if-eqz v0, :cond_0

    .line 212
    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 213
    .local v1, "totalScore":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 214
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    .end local v1    # "totalScore":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-void

    .line 216
    .restart local v1    # "totalScore":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
