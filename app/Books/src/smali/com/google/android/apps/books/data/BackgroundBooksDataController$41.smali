.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->updateLastLocalAccess(Ljava/lang/String;JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$lastLocalAccess:J

.field final synthetic val$notifyContentChanged:Z

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;JZ)V
    .locals 1

    .prologue
    .line 1233
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->val$volumeId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->val$lastLocalAccess:J

    iput-boolean p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->val$notifyContentChanged:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1236
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->val$volumeId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->val$lastLocalAccess:J

    iget-boolean v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;->val$notifyContentChanged:Z

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->updateLastLocalAccess(Ljava/lang/String;JZ)V

    .line 1237
    return-void
.end method
