.class Lcom/google/android/apps/books/util/UIThreadTaskManager$1;
.super Ljava/util/PriorityQueue;
.source "UIThreadTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/UIThreadTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/PriorityQueue",
        "<",
        "Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$1;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-direct {p0}, Ljava/util/PriorityQueue;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z
    .locals 2
    .param p1, "queuedTask"    # Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .prologue
    .line 80
    # getter for: Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mPriority:I
    invoke-static {p1}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->access$000(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)I

    move-result v0

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$1;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # operator++ for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->mRequestSeq:I
    invoke-static {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$208(Lcom/google/android/apps/books/util/UIThreadTaskManager;)I

    move-result v0

    # setter for: Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->mRequestId:I
    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;->access$102(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;I)I

    .line 82
    invoke-super {p0, p1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 77
    check-cast p1, Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/UIThreadTaskManager$1;->add(Lcom/google/android/apps/books/util/UIThreadTaskManager$QueuedTask;)Z

    move-result v0

    return v0
.end method
