.class Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;
.super Lcom/google/android/apps/books/render/JoiningConsumer;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapAndStructureConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/render/JoiningConsumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Ljava/io/InputStream;",
        ">;",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
        ">;>;"
    }
.end annotation


# instance fields
.field final mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)V
    .locals 0
    .param p2, "request"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .prologue
    .line 760
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/JoiningConsumer;-><init>()V

    .line 761
    iput-object p2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .line 762
    return-void
.end method


# virtual methods
.method protected take(Lcom/google/android/apps/books/util/ExceptionOr;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/io/InputStream;",
            ">;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 767
    .local p1, "page":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/io/InputStream;>;"
    .local p2, "structure":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->canAbandon(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$900(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 768
    const-string v0, "ImageModeRenderer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    const-string v0, "ImageModeRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Abandoning request before opening content: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    iget-object v2, v2, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 772
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 797
    :cond_1
    :goto_0
    return-void

    .line 778
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 779
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    iget-object v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->mRequest:Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/render/ImageModeRenderer;->deliverErrorOnUiThread(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$1000(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;Ljava/lang/Exception;)V

    goto :goto_0

    .line 782
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mBitmapDecodeExecutor:Ljava/util/concurrent/ThreadPoolExecutor;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$1200(Lcom/google/android/apps/books/render/ImageModeRenderer;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer$1;-><init>(Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;Lcom/google/android/apps/books/util/ExceptionOr;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected bridge synthetic take(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 756
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/render/ImageModeRenderer$BitmapAndStructureConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
