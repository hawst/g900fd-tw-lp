.class final Lcom/google/android/apps/books/annotations/AnnotationDataParser$2;
.super Ljava/lang/Object;
.source "AnnotationDataParser.java"

# interfaces
.implements Lcom/google/android/apps/books/util/JsonUtils$NodeParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationDataParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
        "<",
        "Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;
    .locals 11
    .param p1, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 239
    const-string v0, "syllabification"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 240
    .local v1, "syllabification":Ljava/lang/String;
    const-string v0, "pronunciation"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 241
    .local v2, "pronunciation":Ljava/lang/String;
    const-string v0, "partOfSpeech"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 242
    .local v3, "partOfSpeech":Ljava/lang/String;
    const-string v0, "pronunciationUrl"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 243
    .local v4, "pronunciationUrl":Ljava/lang/String;
    const-string v0, "definitions"

    invoke-virtual {p1, v0}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->DEFINITION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$400()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v5

    .line 245
    .local v5, "definitions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;>;"
    const-string v0, "examples"

    invoke-virtual {p1, v0}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$100()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v6

    .line 247
    .local v6, "examples":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    const-string v0, "synonyms"

    invoke-virtual {p1, v0}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$100()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v7

    .line 249
    .local v7, "synonyms":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;>;"
    const-string v0, "conjugations"

    invoke-virtual {p1, v0}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->CONJUGATION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$500()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    move-result-object v10

    invoke-static {v0, v10}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v8

    .line 251
    .local v8, "conjugations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;>;"
    const-string v0, "source"

    invoke-virtual {p1, v0}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAttribution(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$200(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    move-result-object v9

    .line 252
    .local v9, "attribution":Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    new-instance v0, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 236
    check-cast p1, Lorg/codehaus/jackson/JsonNode;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$2;->apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;

    move-result-object v0

    return-object v0
.end method
