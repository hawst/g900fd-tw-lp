.class Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;
.super Landroid/os/AsyncTask;
.source "AccountsReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AccountsReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandleAccountsChangedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AccountsReceiver;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AccountsReceiver;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->this$0:Lcom/google/android/apps/books/app/AccountsReceiver;

    .line 62
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->mContext:Landroid/content/Context;

    .line 64
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 15
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 74
    :try_start_0
    iget-object v13, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->mContext:Landroid/content/Context;

    invoke-static {v13}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 75
    .local v2, "am":Landroid/accounts/AccountManager;
    const-string v13, "com.google"

    invoke-virtual {v2, v13}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v12

    .line 79
    .local v12, "systemAccounts":[Landroid/accounts/Account;
    iget-object v13, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/apps/books/provider/BooksProvider;->getAllAccountNames(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v10

    .line 81
    .local v10, "providerAccountNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v10, :cond_2

    .line 82
    move-object v4, v12

    .local v4, "arr$":[Landroid/accounts/Account;
    array-length v8, v4

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v0, v4, v7

    .line 83
    .local v0, "account":Landroid/accounts/Account;
    iget-object v13, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v10, v13}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 87
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 88
    .local v1, "accountToRemove":Ljava/lang/String;
    new-instance v0, Landroid/accounts/Account;

    const-string v13, "com.google"

    invoke-direct {v0, v1, v13}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .restart local v0    # "account":Landroid/accounts/Account;
    iget-object v13, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v3

    .line 91
    .local v3, "app":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v13

    invoke-interface {v13}, Lcom/google/android/apps/books/model/BooksDataStore;->deleteAllContent()V

    .line 92
    invoke-virtual {v3, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getAnnotationController(Landroid/accounts/Account;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->deleteAll()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 105
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountToRemove":Ljava/lang/String;
    .end local v2    # "am":Landroid/accounts/AccountManager;
    .end local v3    # "app":Lcom/google/android/apps/books/app/BooksApplication;
    .end local v4    # "arr$":[Landroid/accounts/Account;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "len$":I
    .end local v10    # "providerAccountNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v12    # "systemAccounts":[Landroid/accounts/Account;
    :catch_0
    move-exception v6

    .line 106
    .local v6, "e":Landroid/database/sqlite/SQLiteException;
    const-string v13, "AccountsReceiver"

    const/4 v14, 0x6

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 107
    const-string v13, "AccountsReceiver"

    const-string v14, "SQLiteException occurred while clearing Books data"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    .end local v6    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_1
    :goto_2
    const/4 v13, 0x0

    return-object v13

    .line 97
    .restart local v2    # "am":Landroid/accounts/AccountManager;
    .restart local v10    # "providerAccountNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v12    # "systemAccounts":[Landroid/accounts/Account;
    :cond_2
    :try_start_1
    new-instance v9, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v13, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->mContext:Landroid/content/Context;

    invoke-direct {v9, v13}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 98
    .local v9, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v9}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAccount()Landroid/accounts/Account;

    move-result-object v11

    .line 99
    .local v11, "selectedAccount":Landroid/accounts/Account;
    if-eqz v11, :cond_1

    # invokes: Lcom/google/android/apps/books/app/AccountsReceiver;->arrayContains([Ljava/lang/Object;Ljava/lang/Object;)Z
    invoke-static {v12, v11}, Lcom/google/android/apps/books/app/AccountsReceiver;->access$000([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 100
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 101
    iget-object v13, p0, Lcom/google/android/apps/books/app/AccountsReceiver$HandleAccountsChangedTask;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v5

    .line 102
    .local v5, "bbc":Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    invoke-interface {v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 103
    invoke-interface {v5}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->flushNotifications()V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 109
    .end local v2    # "am":Landroid/accounts/AccountManager;
    .end local v5    # "bbc":Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    .end local v9    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    .end local v10    # "providerAccountNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v11    # "selectedAccount":Landroid/accounts/Account;
    .end local v12    # "systemAccounts":[Landroid/accounts/Account;
    :catch_1
    move-exception v6

    .line 110
    .local v6, "e":Ljava/lang/Exception;
    const-string v13, "AccountsReceiver"

    const/4 v14, 0x6

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 111
    const-string v13, "AccountsReceiver"

    const-string v14, "Exception occurred while clearing Books data"

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
