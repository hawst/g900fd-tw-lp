.class public Lcom/google/android/apps/books/api/ApiaryClientImpl;
.super Ljava/lang/Object;
.source "ApiaryClientImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/api/ApiaryClient;


# instance fields
.field private final mDeveloperKey:Ljava/lang/String;

.field private final mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

.field private final mSourceParam:Ljava/lang/String;

.field private final mUserAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/net/HttpHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "httpHelper"    # Lcom/google/android/apps/books/net/HttpHelper;
    .param p2, "developerKey"    # Ljava/lang/String;
    .param p3, "sourceParam"    # Ljava/lang/String;
    .param p4, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/net/HttpHelper;

    iput-object v0, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    .line 59
    invoke-static {p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mDeveloperKey:Ljava/lang/String;

    .line 60
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mSourceParam:Ljava/lang/String;

    .line 61
    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mUserAgent:Ljava/lang/String;

    .line 62
    return-void
.end method

.method private buildSignedRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;
    .locals 5
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Lcom/google/api/client/http/GenericUrl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    const-string v2, "key"

    iget-object v3, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mDeveloperKey:Ljava/lang/String;

    invoke-virtual {p2, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    const-string v2, "source"

    iget-object v3, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mSourceParam:Ljava/lang/String;

    invoke-virtual {p2, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->createTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v1

    .line 126
    .local v1, "transport":Lcom/google/api/client/http/HttpTransport;
    const-string v2, "ApiaryClient"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    const-string v2, "ApiaryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Making "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " request with url "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_0
    invoke-virtual {v1}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, v3}, Lcom/google/api/client/http/HttpRequestFactory;->buildRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 132
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    new-instance v2, Lcom/google/api/client/json/JsonObjectParser;

    new-instance v3, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v3}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-direct {v2, v3}, Lcom/google/api/client/json/JsonObjectParser;-><init>(Lcom/google/api/client/json/JsonFactory;)V

    invoke-virtual {v0, v2}, Lcom/google/api/client/http/HttpRequest;->setParser(Lcom/google/api/client/util/ObjectParser;)Lcom/google/api/client/http/HttpRequest;

    .line 133
    return-object v0
.end method

.method public static createTransport()Lcom/google/api/client/http/HttpTransport;
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-direct {v0}, Lcom/google/api/client/http/javanet/NetHttpTransport;-><init>()V

    return-object v0
.end method

.method public static getLogString(Lcom/google/api/client/http/HttpRequest;)Ljava/lang/String;
    .locals 3
    .param p0, "request"    # Lcom/google/api/client/http/HttpRequest;

    .prologue
    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "URL: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/google/api/client/http/HttpRequest;->getUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/books/util/LogUtil;->maybeGetHeaders(Lcom/google/api/client/http/HttpRequest;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private makeRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;
    .locals 4
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Lcom/google/api/client/http/GenericUrl;
    .param p3, "requestData"    # Ljava/lang/Object;
    .param p4, "content"    # Lcom/google/api/client/http/HttpContent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->buildSignedRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 108
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    const-string v1, "ApiaryClient"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    const-string v1, "ApiaryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding requestData: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    invoke-virtual {v0, p4}, Lcom/google/api/client/http/HttpRequest;->setContent(Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    .line 114
    return-object v0
.end method

.method private setHeaders(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V
    .locals 3
    .param p1, "httpRequest"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 260
    new-instance v0, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v0}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 262
    .local v0, "headers":Lcom/google/api/client/http/HttpHeaders;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BooksAndroid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpHeaders;->setUserAgent(Ljava/lang/String;)Lcom/google/api/client/http/HttpHeaders;

    .line 263
    const-string v1, "GData-Version"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/http/HttpHeaders;->set(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/http/HttpHeaders;

    .line 266
    if-eqz p2, :cond_0

    .line 267
    iget-object v1, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/books/net/HttpHelper;->setAuthToken(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)V

    .line 269
    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setHeaders(Lcom/google/api/client/http/HttpHeaders;)Lcom/google/api/client/http/HttpRequest;

    .line 270
    return-void
.end method

.method private statusIsOneOf(I[I)Z
    .locals 5
    .param p1, "statusCode"    # I
    .param p2, "okCodes"    # [I

    .prologue
    .line 273
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, v0, v1

    .line 274
    .local v3, "okCode":I
    if-ne v3, p1, :cond_0

    const/4 v4, 0x1

    .line 276
    .end local v3    # "okCode":I
    :goto_1
    return v4

    .line 273
    .restart local v3    # "okCode":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 276
    .end local v3    # "okCode":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static verifySignature(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/api/data/VerifiableResponse;)V
    .locals 7
    .param p0, "expectedNonce"    # Ljava/lang/String;
    .param p1, "expectedSource"    # Ljava/lang/String;
    .param p2, "sessionKey"    # Ljava/lang/String;
    .param p3, "response"    # Lcom/google/android/apps/books/api/data/VerifiableResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 302
    const-string v3, "expectedNonce is null"

    invoke-static {v3, p0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    const-string v3, "expectedSource is null"

    invoke-static {v3, p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    const-string v3, "sessionKey is null"

    invoke-static {v3, p2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v3, p3, Lcom/google/android/apps/books/api/data/VerifiableResponse;->nonce:Ljava/lang/String;

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 307
    new-instance v3, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad nonce value from server. Expected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Actual: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p3, Lcom/google/android/apps/books/api/data/VerifiableResponse;->nonce:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 311
    :cond_0
    iget-object v3, p3, Lcom/google/android/apps/books/api/data/VerifiableResponse;->source:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 312
    new-instance v3, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad source value from server. Expected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Actual: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p3, Lcom/google/android/apps/books/api/data/VerifiableResponse;->source:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 315
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/books/api/data/VerifiableResponse;->getConcatenatedData()Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "data":Ljava/lang/String;
    invoke-static {p2, v0}, Lcom/google/android/apps/books/util/EncryptionUtils;->createSignature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 318
    .local v2, "expectedSignature":Ljava/lang/String;
    iget-object v3, p3, Lcom/google/android/apps/books/api/data/VerifiableResponse;->signature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 325
    const-string v3, "Bad signature returned from server. Expected %s Actual %s Key %s data %s "

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v5, 0x1

    iget-object v6, p3, Lcom/google/android/apps/books/api/data/VerifiableResponse;->signature:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p2, v4, v5

    const/4 v5, 0x3

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "errorMsg":Ljava/lang/String;
    const-string v3, "ApiaryClient"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 330
    const-string v3, "ApiaryClient"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    .end local v1    # "errorMsg":Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public varargs execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;
    .locals 10
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "okCodes"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/http/HttpRequest;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/accounts/Account;",
            "[I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, "resultDataClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v9, 0x3

    .line 139
    const-string v6, "ApiaryClient"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    .line 140
    .local v3, "isDebugLoggable":Z
    if-eqz v3, :cond_0

    .line 141
    const-string v6, "ApiaryClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Executing apiary request "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " with content "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getContent()Lcom/google/api/client/http/HttpContent;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_0
    const/4 v0, 0x0

    .line 148
    .local v0, "errCount":I
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->executeOnce(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    move-result-object v4

    .line 149
    .local v4, "result":Ljava/lang/Object;, "TT;"
    if-eqz v4, :cond_2

    const-string v6, "ApiaryClient"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 150
    const-string v6, "ApiaryClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Received apiary response "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_2
    return-object v4

    .line 153
    .end local v4    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v2

    .line 154
    .local v2, "ioe":Ljava/io/IOException;
    add-int/lit8 v0, v0, 0x1

    .line 155
    move-object v1, v2

    .line 156
    .local v1, "ioException":Ljava/io/IOException;
    const-string v6, "ApiaryClient"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 157
    const-string v6, "ApiaryClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "errCount = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/api/client/http/GenericUrl;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Lcom/google/android/apps/books/net/HttpHelper;->shouldSkipRetry(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v5

    .line 161
    .local v5, "wrapException":Ljava/io/IOException;
    if-eqz v5, :cond_4

    .line 162
    throw v5

    .line 165
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v6}, Lcom/google/android/apps/books/net/HttpHelper;->sleep()V

    .line 167
    if-lt v0, v9, :cond_1

    .line 169
    throw v1
.end method

.method protected executeOnce(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;
    .locals 11
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "okCodes"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/http/HttpRequest;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/accounts/Account;",
            "[I)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    .local p2, "resultDataClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->getAuthTokenFor(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "authToken":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->setHeaders(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V

    .line 188
    const-string v7, "ApiaryClient"

    const/4 v8, 0x2

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "eng"

    sget-object v8, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "userdebug"

    sget-object v8, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 190
    :cond_0
    const-string v7, "ApiaryClient"

    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/api/client/http/HttpHeaders;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/books/net/HttpUtils;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v4

    .line 195
    .local v4, "response":Lcom/google/api/client/http/HttpResponse;
    const-class v7, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    if-eq p2, v7, :cond_5

    .line 196
    invoke-virtual {v4, p2}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    .line 197
    .local v5, "result":Ljava/lang/Object;, "TT;"
    if-nez v5, :cond_4

    .line 199
    const-string v7, "ApiaryClient"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 200
    const-string v7, "ApiaryClient"

    const-string v8, "HttpResponse.parse() returned null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_2
    new-instance v7, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;

    const-string v8, "HttpResponse.parseAs() returned null"

    invoke-direct {v7, v8}, Lcom/google/android/apps/books/net/HttpHelper$ServerIoException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Lcom/google/api/client/http/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 208
    .end local v4    # "response":Lcom/google/api/client/http/HttpResponse;
    .end local v5    # "result":Ljava/lang/Object;, "TT;"
    :catch_0
    move-exception v2

    .line 209
    .local v2, "e":Lcom/google/api/client/http/HttpResponseException;
    invoke-virtual {v2}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v6

    .line 210
    .local v6, "statusCode":I
    invoke-direct {p0, v6, p4}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->statusIsOneOf(I[I)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 214
    const-string v7, "ApiaryClient"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 215
    const-string v7, "ApiaryClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Treating response code "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " as success."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_3
    const/4 v5, 0x0

    .end local v2    # "e":Lcom/google/api/client/http/HttpResponseException;
    .end local v6    # "statusCode":I
    :cond_4
    :goto_0
    return-object v5

    .line 206
    .restart local v4    # "response":Lcom/google/api/client/http/HttpResponse;
    :cond_5
    const/4 v5, 0x0

    goto :goto_0

    .line 219
    .end local v4    # "response":Lcom/google/api/client/http/HttpResponse;
    .restart local v2    # "e":Lcom/google/api/client/http/HttpResponseException;
    .restart local v6    # "statusCode":I
    :cond_6
    const-string v7, "ApiaryClient"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 220
    const-string v7, "ApiaryClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error response from books API: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/google/api/client/http/HttpResponseException;->getContent()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_7
    const-string v7, "%d: %s\n%s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v2}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v2}, Lcom/google/api/client/http/HttpResponseException;->getStatusMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {p1}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->getLogString(Lcom/google/api/client/http/HttpRequest;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 225
    .local v1, "description":Ljava/lang/String;
    invoke-static {v6, v1, v2}, Lcom/google/android/apps/books/net/HttpHelper;->wrapExceptionBasedOnStatus(ILjava/lang/String;Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v3

    .line 227
    .local v3, "exceptionToThrow":Ljava/io/IOException;
    if-eqz v3, :cond_9

    .line 228
    instance-of v7, v3, Lcom/google/android/apps/books/net/HttpHelper$TokenExpiredException;

    if-eqz v7, :cond_8

    if-eqz v0, :cond_8

    .line 230
    iget-object v7, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/net/HttpHelper;->invalidateAuthToken(Ljava/lang/String;)V

    .line 232
    :cond_8
    throw v3

    .line 234
    :cond_9
    new-instance v7, Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown error code from apiary: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Lcom/google/android/apps/books/net/HttpHelper$UncategorizedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 237
    .end local v1    # "description":Ljava/lang/String;
    .end local v2    # "e":Lcom/google/api/client/http/HttpResponseException;
    .end local v3    # "exceptionToThrow":Ljava/io/IOException;
    .end local v6    # "statusCode":I
    :catch_1
    move-exception v2

    .line 238
    .local v2, "e":Ljava/io/IOException;
    invoke-static {p1}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->getLogString(Lcom/google/api/client/http/HttpRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/apps/books/net/HttpHelper;->wrapException(Ljava/io/IOException;Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v7

    throw v7
.end method

.method public executeRaw(Lcom/google/api/client/http/HttpRequest;Landroid/accounts/Account;)Lcom/google/api/client/http/HttpResponse;
    .locals 2
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/api/client/http/HttpResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->getAuthTokenFor(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "authToken":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->setHeaders(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V

    .line 177
    invoke-static {p1}, Lcom/google/android/apps/books/net/HttpUtils;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    return-object v1
.end method

.method protected getAuthTokenFor(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/net/HttpHelper;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHttpHelper()Lcom/google/android/apps/books/net/HttpHelper;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mHttpHelper:Lcom/google/android/apps/books/net/HttpHelper;

    return-object v0
.end method

.method public makeDeleteRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;
    .locals 1
    .param p1, "url"    # Lcom/google/api/client/http/GenericUrl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    const-string v0, "DELETE"

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->buildSignedRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    return-object v0
.end method

.method public makeGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;
    .locals 4
    .param p1, "url"    # Lcom/google/api/client/http/GenericUrl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    const-string v2, "key"

    iget-object v3, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mDeveloperKey:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string v2, "source"

    iget-object v3, p0, Lcom/google/android/apps/books/api/ApiaryClientImpl;->mSourceParam:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/api/client/http/GenericUrl;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-static {}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->createTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v1

    .line 86
    .local v1, "transport":Lcom/google/api/client/http/HttpTransport;
    invoke-virtual {v1}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/api/client/http/HttpRequestFactory;->buildGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 87
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    new-instance v2, Lcom/google/api/client/json/JsonObjectParser;

    new-instance v3, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v3}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-direct {v2, v3}, Lcom/google/api/client/json/JsonObjectParser;-><init>(Lcom/google/api/client/json/JsonFactory;)V

    invoke-virtual {v0, v2}, Lcom/google/api/client/http/HttpRequest;->setParser(Lcom/google/api/client/util/ObjectParser;)Lcom/google/api/client/http/HttpRequest;

    .line 88
    return-object v0
.end method

.method public makeJsonRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;
    .locals 2
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "url"    # Lcom/google/api/client/http/GenericUrl;
    .param p3, "requestData"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    new-instance v0, Lcom/google/api/client/http/json/JsonHttpContent;

    new-instance v1, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v1}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-direct {v0, v1, p3}, Lcom/google/api/client/http/json/JsonHttpContent;-><init>(Lcom/google/api/client/json/JsonFactory;Ljava/lang/Object;)V

    .line 101
    .local v0, "content":Lcom/google/api/client/http/json/JsonHttpContent;
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->makeRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    return-object v1
.end method

.method public makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;
    .locals 2
    .param p1, "url"    # Lcom/google/api/client/http/GenericUrl;
    .param p2, "requestData"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    const-string v0, "POST"

    new-instance v1, Lcom/google/api/client/http/UrlEncodedContent;

    invoke-direct {v1, p2}, Lcom/google/api/client/http/UrlEncodedContent;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/google/android/apps/books/api/ApiaryClientImpl;->makeRequest(Ljava/lang/String;Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    return-object v0
.end method
