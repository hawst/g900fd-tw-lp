.class Lcom/google/android/apps/books/render/ImageModeRenderer$1;
.super Lcom/google/android/apps/books/render/EmptyPageHandle;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;->getFirstPageAfterLastViewablePage()Lcom/google/android/apps/books/render/PageHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$1;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/EmptyPageHandle;-><init>()V

    return-void
.end method


# virtual methods
.method public getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 4

    .prologue
    .line 388
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$1;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPages()Ljava/util/List;

    move-result-object v1

    .line 389
    .local v1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$1;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    # getter for: Lcom/google/android/apps/books/render/ImageModeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    invoke-static {v2}, Lcom/google/android/apps/books/render/ImageModeRenderer;->access$100(Lcom/google/android/apps/books/render/ImageModeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getLastViewableBookPageIndex()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "pageId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v2

    return-object v2
.end method
