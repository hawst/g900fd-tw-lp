.class Lcom/google/android/apps/books/app/ClipboardCopyController$1;
.super Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;
.source "ClipboardCopyController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ClipboardCopyController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ClipboardCopyController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ClipboardCopyController;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/android/apps/books/app/ClipboardCopyController$1;->this$0:Lcom/google/android/apps/books/app/ClipboardCopyController;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public layerAnnotationsLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    const/4 v7, 0x6

    .line 24
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 25
    iget-object v4, p0, Lcom/google/android/apps/books/app/ClipboardCopyController$1;->this$0:Lcom/google/android/apps/books/app/ClipboardCopyController;

    const/4 v5, 0x0

    # setter for: Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I
    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/ClipboardCopyController;->access$002(Lcom/google/android/apps/books/app/ClipboardCopyController;I)I

    .line 26
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v4}, Lcom/google/common/collect/ImmutableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 27
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_3

    .line 28
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getNormalizedSelectedText()Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "normalizedText":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 30
    iget-object v4, p0, Lcom/google/android/apps/books/app/ClipboardCopyController$1;->this$0:Lcom/google/android/apps/books/app/ClipboardCopyController;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    # += operator for: Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I
    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/ClipboardCopyController;->access$012(Lcom/google/android/apps/books/app/ClipboardCopyController;I)I

    goto :goto_0

    .line 32
    :cond_1
    const-string v4, "ClipboardCopyController"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 33
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "unnormalizedText":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 35
    const-string v4, "ClipboardCopyController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "normalized text is empty (unnormalized text: \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\')"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 38
    :cond_2
    const-string v4, "ClipboardCopyController"

    const-string v5, "selected text is empty!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 43
    .end local v2    # "normalizedText":Ljava/lang/String;
    .end local v3    # "unnormalizedText":Ljava/lang/String;
    :cond_3
    const-string v4, "ClipboardCopyController"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 44
    const-string v4, "ClipboardCopyController"

    const-string v5, "trying to load an empty annotation!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 49
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_4
    return-void
.end method

.method public layerQuotaLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "quota":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/ClipboardCopyController$1;->this$0:Lcom/google/android/apps/books/app/ClipboardCopyController;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    :goto_0
    # setter for: Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ClipboardCopyController;->access$102(Lcom/google/android/apps/books/app/ClipboardCopyController;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .line 54
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
