.class public Lcom/google/android/apps/books/provider/IcingGlobalSearchHelper;
.super Ljava/lang/Object;
.source "IcingGlobalSearchHelper.java"


# static fields
.field public static final TABLE_STORAGE_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

.field public static final TABLE_STORAGE_SPECS:[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 51
    const-string v0, "volumes"

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->builder(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->uriColumn(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->version(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "CASE viewability WHEN \'http://schemas.google.com/books/2008#view_all_pages\' THEN 100 WHEN \'http://schemas.google.com/books/2008#view_partial\' THEN 10 ELSE 0 END"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->score(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "title"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "title"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->weight(I)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->indexPrefixes(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "creator"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "creator"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x14

    invoke-virtual {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->weight(I)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->indexPrefixes(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "volume_id"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "volume_id"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->noIndex(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "account_name"

    new-instance v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    const-string v3, "account_name"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->noIndex(Z)Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo$Builder;->build()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addSectionForColumn(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "text1"

    const v2, 0x7f0f01f2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "text2"

    const v2, 0x7f0f01f3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "icon"

    const v2, 0x7f0f01f4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "intent_data_id"

    const v2, 0x7f0f01f5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    const-string v1, "intent_extra_data"

    const v2, 0x7f0f01f6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->addGlobalSearchSectionTemplate(Ljava/lang/String;I)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;->build()Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/IcingGlobalSearchHelper;->TABLE_STORAGE_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .line 91
    new-array v0, v4, [Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/provider/IcingGlobalSearchHelper;->TABLE_STORAGE_SPEC:Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/provider/IcingGlobalSearchHelper;->TABLE_STORAGE_SPECS:[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    return-void
.end method
