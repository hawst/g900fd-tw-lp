.class public Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;
.super Ljava/lang/Object;
.source "BooksConnectivityManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/net/BooksConnectivityManager;


# instance fields
.field private final mConnectivityManager:Landroid/net/ConnectivityManager;


# direct methods
.method public constructor <init>(Landroid/net/ConnectivityManager;)V
    .locals 0
    .param p1, "connectivityManager"    # Landroid/net/ConnectivityManager;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 17
    return-void
.end method


# virtual methods
.method public isDeviceConnected()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/apps/books/net/BooksConnectivityManagerImpl;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-static {v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/net/ConnectivityManager;)Z

    move-result v0

    return v0
.end method
