.class public interface abstract Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SelectionInfo"
.end annotation


# virtual methods
.method public abstract createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;
.end method

.method public abstract createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
.end method

.method public abstract getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;
.end method

.method public abstract getHighlightScreenRect()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
.end method

.method public abstract getMarginNoteScreenRect()Landroid/graphics/Rect;
.end method

.method public abstract getNormalizedSelectedText()Ljava/lang/CharSequence;
.end method

.method public abstract getSelectedText()Ljava/lang/CharSequence;
.end method

.method public abstract isClipboardCopyingLimited()Z
.end method

.method public abstract isClipboardCopyingPermitted(I)Z
.end method

.method public abstract isEditable()Z
.end method

.method public abstract isQuoteSharingPermitted()Z
.end method

.method public abstract isSearchingPermitted()Z
.end method

.method public abstract isTranslationPermitted()Z
.end method

.method public abstract onAnnotationChanged(Lcom/google/android/apps/books/annotations/Annotation;)V
.end method
