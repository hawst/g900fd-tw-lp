.class public final enum Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;
.super Ljava/lang/Enum;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UploadAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

.field public static final enum CANCEL:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

.field public static final enum PAUSE:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

.field public static final enum RESUME:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;


# instance fields
.field private final mUiStringResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    const-string v1, "CANCEL"

    const v2, 0x7f0f01ea

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->CANCEL:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    .line 168
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    const-string v1, "PAUSE"

    const v2, 0x7f0f01eb

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->PAUSE:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    .line 169
    new-instance v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    const-string v1, "RESUME"

    const v2, 0x7f0f01ec

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->RESUME:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    .line 166
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->CANCEL:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->PAUSE:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->RESUME:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->$VALUES:[Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "uiStringResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 174
    iput p3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->mUiStringResourceId:I

    .line 175
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    .prologue
    .line 166
    iget v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->mUiStringResourceId:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 166
    const-class v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->$VALUES:[Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    return-object v0
.end method
