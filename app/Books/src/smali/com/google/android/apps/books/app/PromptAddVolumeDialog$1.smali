.class Lcom/google/android/apps/books/app/PromptAddVolumeDialog$1;
.super Ljava/lang/Object;
.source "PromptAddVolumeDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/PromptAddVolumeDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/PromptAddVolumeDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/PromptAddVolumeDialog;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$1;->this$0:Lcom/google/android/apps/books/app/PromptAddVolumeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 48
    iget-object v4, p0, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$1;->this$0:Lcom/google/android/apps/books/app/PromptAddVolumeDialog;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/PromptAddVolumeDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 49
    .local v2, "args":Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$Arguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 50
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$Arguments;->getVolumeId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    .line 51
    .local v3, "volumeId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$1;->this$0:Lcom/google/android/apps/books/app/PromptAddVolumeDialog;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/PromptAddVolumeDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 52
    .local v1, "activity":Landroid/app/Activity;
    invoke-static {v1, v0, v3}, Lcom/google/android/apps/books/app/AddVolumeTask;->addVolume(Landroid/app/Activity;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 53
    iget-object v4, p0, Lcom/google/android/apps/books/app/PromptAddVolumeDialog$1;->this$0:Lcom/google/android/apps/books/app/PromptAddVolumeDialog;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/PromptAddVolumeDialog;->dismiss()V

    .line 54
    return-void
.end method
