.class public Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
.super Ljava/lang/Object;
.source "SequenceBimap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$1;,
        Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "LeftValue:Ljava/lang/Object;",
        "RightValue:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mCurrentLeftOffset:I

.field private mCurrentRightOffset:I

.field private mLeftSpanStarts:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo",
            "<T",
            "LeftValue;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRightSpanStarts:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo",
            "<TRightValue;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mLeftSpanStarts:Ljava/util/TreeMap;

    .line 51
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mRightSpanStarts:Ljava/util/TreeMap;

    return-void
.end method

.method private checkBounds(II)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "exclusiveLimit"    # I

    .prologue
    .line 134
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    if-ltz p1, :cond_0

    if-lt p1, p2, :cond_1

    .line 135
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "SequenceBimap index out of bounds"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_1
    return-void
.end method

.method private static mapIndex(ILjava/util/TreeMap;Ljava/util/Map;)I
    .locals 7
    .param p0, "sourceIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<SourceValue:",
            "Ljava/lang/Object;",
            "DestValue:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo",
            "<TSourceValue;>;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo",
            "<TDestValue;>;>;)I"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "sourceSpanStarts":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo<TSourceValue;>;>;"
    .local p2, "destSpanStarts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo<TDestValue;>;>;"
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    .line 116
    .local v3, "sourceSpanEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo<TSourceValue;>;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;

    iget v4, v5, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->offsetToOther:I

    .line 117
    .local v4, "sourceToDestOffset":I
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int v1, v5, v4

    .line 118
    .local v1, "destSpanStartIndex":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;

    .line 121
    .local v0, "destSpan":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo<TDestValue;>;"
    const/4 v5, 0x0

    iget v6, v0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->length:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int v2, v1, v5

    .line 122
    .local v2, "lastIndexInDestSpan":I
    add-int v5, p0, v4

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    return v5
.end method


# virtual methods
.method public addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V
    .locals 6
    .param p2, "leftLength"    # I
    .param p4, "rightLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "LeftValue;",
            "ITRightValue;I)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    .local p1, "leftValue":Ljava/lang/Object;, "TLeftValue;"
    .local p3, "rightValue":Ljava/lang/Object;, "TRightValue;"
    const/4 v5, 0x0

    .line 74
    iget v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    iget v2, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    sub-int v0, v1, v2

    .line 75
    .local v0, "leftToRightOffset":I
    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mLeftSpanStarts:Ljava/util/TreeMap;

    iget v2, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;

    invoke-direct {v3, p2, p1, v0, v5}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;-><init>(ILjava/lang/Object;ILcom/google/android/apps/books/render/prepaginated/SequenceBimap$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mRightSpanStarts:Ljava/util/TreeMap;

    iget v2, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;

    neg-int v4, v0

    invoke-direct {v3, p4, p3, v4, v5}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;-><init>(ILjava/lang/Object;ILcom/google/android/apps/books/render/prepaginated/SequenceBimap$1;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    add-int/2addr v1, p2

    iput v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    .line 80
    iget v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    add-int/2addr v1, p4

    iput v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    .line 81
    return-void
.end method

.method public getLeftSize()I
    .locals 1

    .prologue
    .line 84
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    return v0
.end method

.method public getRightSize()I
    .locals 1

    .prologue
    .line 88
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    return v0
.end method

.method public leftIndexToRightIndex(I)I
    .locals 2
    .param p1, "leftIndex"    # I

    .prologue
    .line 102
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->checkBounds(II)V

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mLeftSpanStarts:Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mRightSpanStarts:Ljava/util/TreeMap;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mapIndex(ILjava/util/TreeMap;Ljava/util/Map;)I

    move-result v0

    return v0
.end method

.method public leftIndexToValue(I)Ljava/lang/Object;
    .locals 2
    .param p1, "leftIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)T",
            "LeftValue;"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentLeftOffset:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->checkBounds(II)V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mLeftSpanStarts:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public rightIndexToLeftIndex(I)I
    .locals 2
    .param p1, "rightIndex"    # I

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->checkBounds(II)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mRightSpanStarts:Ljava/util/TreeMap;

    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mLeftSpanStarts:Ljava/util/TreeMap;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mapIndex(ILjava/util/TreeMap;Ljava/util/Map;)I

    move-result v0

    return v0
.end method

.method public rightIndexToValue(I)Ljava/lang/Object;
    .locals 2
    .param p1, "rightIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TRightValue;"
        }
    .end annotation

    .prologue
    .line 97
    .local p0, "this":Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;, "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap<TLeftValue;TRightValue;>;"
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mCurrentRightOffset:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->checkBounds(II)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->mRightSpanStarts:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;

    iget-object v0, v0, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap$SpanInfo;->value:Ljava/lang/Object;

    return-object v0
.end method
