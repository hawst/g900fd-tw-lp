.class Lcom/google/android/apps/books/data/ConsumerList;
.super Ljava/lang/Object;
.source "ConsumerList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mConsumers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerList;, "Lcom/google/android/apps/books/data/ConsumerList<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ConsumerList;->mConsumers:Ljava/util/List;

    return-void
.end method

.method private static logExtraConsumerInList()V
    .locals 2

    .prologue
    .line 77
    const-string v0, "ConsumerList"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "ConsumerList"

    const-string v1, "Extra consumer is also in the list!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    return-void
.end method

.method private removeConsumers()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerList;, "Lcom/google/android/apps/books/data/ConsumerList<TV;>;"
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 71
    .local v0, "removedConsumers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/utils/Consumer<TV;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/ConsumerList;->mConsumers:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/books/data/ConsumerList;->mConsumers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 73
    return-object v0
.end method


# virtual methods
.method addConsumer(Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerList;, "Lcom/google/android/apps/books/data/ConsumerList<TV;>;"
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/ConsumerList;->mConsumers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 37
    .local v0, "wasEmpty":Z
    if-eqz p1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/books/data/ConsumerList;->mConsumers:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_0
    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 63
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerList;, "Lcom/google/android/apps/books/data/ConsumerList<TV;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ConsumerList;->mConsumers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerList;, "Lcom/google/android/apps/books/data/ConsumerList<TV;>;"
    .local p1, "result":Ljava/lang/Object;, "TV;"
    .local p2, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    if-eqz p2, :cond_0

    .line 50
    invoke-interface {p2, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 52
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ConsumerList;->removeConsumers()Ljava/util/Collection;

    move-result-object v1

    .line 53
    .local v1, "consumers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/ublib/utils/Consumer<TV;>;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/utils/Consumer;

    .line 54
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    if-ne v0, p2, :cond_1

    .line 55
    invoke-static {}, Lcom/google/android/apps/books/data/ConsumerList;->logExtraConsumerInList()V

    goto :goto_0

    .line 57
    :cond_1
    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    .end local v0    # "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    :cond_2
    return-void
.end method
