.class public Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;
.super Ljava/lang/Object;
.source "XmlPullParserFactorySingleton.java"


# static fields
.field private static sFactory:Lorg/xmlpull/v1/XmlPullParserFactory;


# direct methods
.method public static declared-synchronized newPullParserLocked()Lorg/xmlpull/v1/XmlPullParser;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 11
    const-class v1, Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;->sFactory:Lorg/xmlpull/v1/XmlPullParserFactory;

    if-nez v0, :cond_0

    .line 12
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;->sFactory:Lorg/xmlpull/v1/XmlPullParserFactory;

    .line 14
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/net/XmlPullParserFactorySingleton;->sFactory:Lorg/xmlpull/v1/XmlPullParserFactory;

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 11
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
