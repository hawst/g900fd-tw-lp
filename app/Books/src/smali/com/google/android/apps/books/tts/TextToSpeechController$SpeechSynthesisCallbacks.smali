.class public interface abstract Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;
.super Ljava/lang/Object;
.source "TextToSpeechController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/tts/TextToSpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SpeechSynthesisCallbacks"
.end annotation


# virtual methods
.method public abstract onInitializationError()V
.end method

.method public abstract onPhraseFinished(Ljava/lang/String;)V
.end method

.method public abstract onPhraseStarted(Ljava/lang/String;)V
.end method
