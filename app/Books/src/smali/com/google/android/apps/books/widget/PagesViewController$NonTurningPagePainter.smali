.class Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;
.super Lcom/google/android/apps/books/widget/DecoratingPagePainter;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NonTurningPagePainter"
.end annotation


# instance fields
.field private final mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/Logger;)V
    .locals 1
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 3013
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    .line 3014
    const/4 v0, 0x0

    invoke-direct {p0, p3, v0, p4}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;-><init>(Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/util/Logger;)V

    .line 3015
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 3016
    return-void
.end method


# virtual methods
.method protected decorate(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3020
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3600(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 3021
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mNonTurningPageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->mPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mTempSize:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3600(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->drawOnPageCanvas(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 3022
    return-void
.end method

.method public getOwnedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 3041
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public getSharedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 3031
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
