.class public Lcom/google/android/apps/books/util/LongPointF;
.super Ljava/lang/Object;
.source "LongPointF.java"


# direct methods
.method public static getX(J)F
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 48
    long-to-int v0, p0

    invoke-static {v0}, Lcom/google/android/apps/books/util/IntOrFloat;->toFloat(I)F

    move-result v0

    return v0
.end method

.method public static getY(J)F
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 52
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    long-to-int v0, v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/IntOrFloat;->toFloat(I)F

    move-result v0

    return v0
.end method

.method public static with(FF)J
    .locals 8
    .param p0, "x"    # F
    .param p1, "y"    # F

    .prologue
    .line 23
    invoke-static {p0}, Lcom/google/android/apps/books/util/IntOrFloat;->with(F)I

    move-result v0

    .line 24
    .local v0, "encX":I
    invoke-static {p1}, Lcom/google/android/apps/books/util/IntOrFloat;->with(F)I

    move-result v1

    .line 26
    .local v1, "encY":I
    int-to-long v2, v1

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    int-to-long v4, v0

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    return-wide v2
.end method
