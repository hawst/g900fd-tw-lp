.class public Lcom/google/android/apps/books/util/ProductionLogger;
.super Ljava/lang/Object;
.source "ProductionLogger.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Logger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;
    }
.end annotation


# static fields
.field private static final sCategoryDatas:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/util/Logger$Category;",
            "Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 32
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/util/ProductionLogger;->sCategoryDatas:Ljava/util/Map;

    .line 34
    sget-object v0, Lcom/google/android/apps/books/util/ProductionLogger;->sCategoryDatas:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    new-instance v2, Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;

    const-string v3, "BooksPerformance"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->PERFORMANCE_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/util/ProductionLogger;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public declared-synchronized log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V
    .locals 1
    .param p1, "category"    # Lcom/google/android/apps/books/util/Logger$Category;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/books/util/ProductionLogger;->sCategoryDatas:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;

    iget-object v0, v0, Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;->tag:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    monitor-exit p0

    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void
.end method

.method public declared-synchronized shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z
    .locals 2
    .param p1, "category"    # Lcom/google/android/apps/books/util/Logger$Category;

    .prologue
    .line 46
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/books/util/ProductionLogger;->sCategoryDatas:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;

    iget-object v0, v0, Lcom/google/android/apps/books/util/ProductionLogger$CategoryData;->key:Lcom/google/android/apps/books/util/ConfigValue;

    iget-object v1, p0, Lcom/google/android/apps/books/util/ProductionLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public shouldLog(Ljava/lang/String;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 56
    const/4 v0, 0x3

    invoke-static {p1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method
