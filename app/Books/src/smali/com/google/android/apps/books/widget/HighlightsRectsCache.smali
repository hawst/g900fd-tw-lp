.class public Lcom/google/android/apps/books/widget/HighlightsRectsCache;
.super Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;
.source "HighlightsRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl",
        "<",
        "Lcom/google/android/apps/books/widget/PassageHighlightRects;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mLayerIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final mRenderCallbacks:Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;)V
    .locals 0
    .param p2, "loader"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
    .param p3, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;
    .param p5, "renderCallbacks"    # Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            "Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mLayerIds:Ljava/util/Set;

    .line 27
    iput-object p5, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;

    .line 28
    return-void
.end method


# virtual methods
.method protected finishProcessRequestResult()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onCacheUpdated()V

    .line 55
    return-void
.end method

.method protected getPaintables(I)Ljava/util/List;
    .locals 2
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mLayerIds:Ljava/util/Set;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getPaintableAnnotations(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected makeCachedData(ILjava/util/List;)Lcom/google/android/apps/books/widget/PassageHighlightRects;
    .locals 5
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)",
            "Lcom/google/android/apps/books/widget/PassageHighlightRects;"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "rects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/LabeledRect;

    .line 38
    .local v0, "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mRenderCallbacks:Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;->onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;)V

    goto :goto_0

    .line 41
    .end local v0    # "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    sget-object v4, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    invoke-interface {v3, p1, v4}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getPaintableAnnotations(ILjava/util/Set;)Ljava/util/List;

    move-result-object v1

    .line 44
    .local v1, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    new-instance v3, Lcom/google/android/apps/books/widget/PassageHighlightRects;

    const/4 v4, 0x0

    invoke-direct {v3, v1, p2, v4}, Lcom/google/android/apps/books/widget/PassageHighlightRects;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    return-object v3
.end method

.method protected bridge synthetic makeCachedData(ILjava/util/List;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->makeCachedData(ILjava/util/List;)Lcom/google/android/apps/books/widget/PassageHighlightRects;

    move-result-object v0

    return-object v0
.end method
