.class public Lcom/google/android/apps/books/app/AudioClipPlayer;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"

# interfaces
.implements Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;,
        Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;,
        Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;,
        Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;,
        Lcom/google/android/apps/books/app/AudioClipPlayer$StubAudioClipPlayerListener;,
        Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;,
        Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    }
.end annotation


# instance fields
.field private mAudioClipPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;

.field private final mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mClipQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;",
            ">;"
        }
    .end annotation
.end field

.field private mClipStartAtLastSeek:J

.field private mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

.field private final mExecutor:Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;

.field private final mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

.field private mPreparingOrSeeking:Z

.field private mRequestId:I

.field private mTimeAtLastSeek:J

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;)V
    .locals 2
    .param p1, "executor"    # Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;
    .param p2, "mediaPlayer"    # Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
    .param p3, "booksDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p4, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipQueue:Ljava/util/Queue;

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mRequestId:I

    .line 381
    new-instance v0, Lcom/google/android/apps/books/app/AudioClipPlayer$StubAudioClipPlayerListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$StubAudioClipPlayerListener;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mAudioClipPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;

    .line 382
    if-eqz p1, :cond_0

    .end local p1    # "executor":Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;
    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mExecutor:Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;

    .line 383
    if-eqz p2, :cond_1

    .end local p2    # "mediaPlayer":Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
    :goto_1
    iput-object p2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->createMediaPlayerListener()Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->setListener(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;)V

    .line 385
    iput-object p3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 386
    iput-object p4, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mVolumeId:Ljava/lang/String;

    .line 387
    return-void

    .line 382
    .restart local p1    # "executor":Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;
    .restart local p2    # "mediaPlayer":Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
    :cond_0
    new-instance p1, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;

    .end local p1    # "executor":Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;
    invoke-direct {p1, p0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$1;)V

    goto :goto_0

    .line 383
    :cond_1
    new-instance p2, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;

    .end local p2    # "mediaPlayer":Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
    invoke-direct {p2, p0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultMediaPlayer;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$1;)V

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;)V
    .locals 1
    .param p1, "booksDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 365
    invoke-direct {p0, v0, v0, p1, p2}, Lcom/google/android/apps/books/app/AudioClipPlayer;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/AudioClipPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mRequestId:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->onFinishedPlayingCurrentClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    return-void
.end method

.method static synthetic access$1102(Lcom/google/android/apps/books/app/AudioClipPlayer;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p1, "x1"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipStartAtLastSeek:J

    return-wide p1
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/books/app/AudioClipPlayer;J)J
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p1, "x1"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mTimeAtLastSeek:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->startedNextClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/app/AudioClipPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->playSilentClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/AudioClipPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method private createMediaPlayerListener()Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;
    .locals 1

    .prologue
    .line 572
    new-instance v0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/AudioClipPlayer$2;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;)V

    return-object v0
.end method

.method private getNextClip()Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    return-object v0
.end method

.method private onClipStartedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 3
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 520
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    const-string v0, "AudioClipPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClipStartedPlaying "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mAudioClipPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;->onClipStartedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 524
    return-void
.end method

.method private onFinishedPlayingCurrentClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 3
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 527
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    const-string v0, "AudioClipPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFinishedPlayingCurrentClip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->isSilentClip()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mAudioClipPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;->onClipFinishedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 542
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->playNextClip()V

    .line 543
    return-void
.end method

.method private playNextClip()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x3

    .line 464
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->getNextClip()Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    move-result-object v1

    .line 465
    .local v1, "nextClip":Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    if-nez v1, :cond_1

    .line 466
    const-string v2, "AudioClipPlayer"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 467
    const-string v2, "AudioClipPlayer"

    const-string v3, "playNextClip, no next clip"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->reset()V

    .line 506
    :goto_0
    return-void

    .line 473
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->sameAudioSource(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 474
    iget v2, v1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    iget-object v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    iget v3, v3, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    sub-int v0, v2, v3

    .line 478
    .local v0, "clipGap":I
    const/16 v2, 0x9c4

    if-ge v0, v2, :cond_3

    const/16 v2, -0x64

    if-lt v0, v2, :cond_3

    .line 480
    const-string v2, "AudioClipPlayer"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 481
    const-string v2, "AudioClipPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playNextClip, same file, keep going: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->startedNextClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 505
    .end local v0    # "clipGap":I
    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    goto :goto_0

    .line 486
    .restart local v0    # "clipGap":I
    :cond_3
    const-string v2, "AudioClipPlayer"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 487
    const-string v2, "AudioClipPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playNextClip, same file, seek: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_4
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z

    .line 490
    iget-object v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    iget v3, v1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->seekTo(I)V

    goto :goto_1

    .line 492
    .end local v0    # "clipGap":I
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->isSilentClip()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 493
    const-string v2, "AudioClipPlayer"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 494
    const-string v2, "AudioClipPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delaying : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    iget v5, v1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->playSilentClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    goto :goto_1

    .line 498
    :cond_7
    const-string v2, "AudioClipPlayer"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 499
    const-string v2, "AudioClipPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playNextClip, new file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->reset()V

    .line 502
    iget-object v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    iget v3, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mRequestId:I

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->setDataSourceAndPreparePlayer(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;I)V

    .line 503
    iput-boolean v5, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z

    goto/16 :goto_1
.end method

.method private playSilentClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 2
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 513
    iget v0, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipStartAtLastSeek:J

    .line 514
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mTimeAtLastSeek:J

    .line 515
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z

    .line 516
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->startedNextClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 517
    return-void
.end method

.method private startedNextClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 10
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 546
    const-string v1, "AudioClipPlayer"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    const-string v1, "AudioClipPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startedNextClip()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->onClipStartedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 551
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->isSilentClip()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    .line 552
    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->start()V

    .line 555
    :cond_1
    iget-wide v4, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mTimeAtLastSeek:J

    iget v1, p1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->endTime:I

    int-to-long v6, v1

    iget-wide v8, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipStartAtLastSeek:J

    sub-long/2addr v6, v8

    add-long v2, v4, v6

    .line 556
    .local v2, "timeAtClipEnd":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v2, v4

    long-to-int v0, v4

    .line 558
    .local v0, "clipEndDelay":I
    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mExecutor:Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;

    new-instance v4, Lcom/google/android/apps/books/app/AudioClipPlayer$1;

    invoke-direct {v4, p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$1;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    invoke-interface {v1, v4, v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;->runAfter(Ljava/lang/Runnable;I)V

    .line 564
    return-void
.end method


# virtual methods
.method public addClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    .locals 3
    .param p1, "clip"    # Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .prologue
    .line 429
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const-string v0, "AudioClipPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addClip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->duration()I

    move-result v0

    if-lez v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 436
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 437
    invoke-direct {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->playNextClip()V

    .line 439
    :cond_2
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 395
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const-string v0, "AudioClipPlayer"

    const-string v1, "close()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->reset()V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->release()V

    .line 400
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->isSilentClip()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 408
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    const-string v0, "AudioClipPlayer"

    const-string v1, "reset()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mExecutor:Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;->reset()V

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->reset()V

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    .line 415
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z

    .line 416
    iget v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mRequestId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mRequestId:I

    .line 417
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;

    .prologue
    .line 601
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer;->mAudioClipPlayerListener:Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;

    .line 602
    return-void
.end method
