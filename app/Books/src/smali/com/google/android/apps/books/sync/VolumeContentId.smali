.class public final Lcom/google/android/apps/books/sync/VolumeContentId;
.super Ljava/lang/Object;
.source "VolumeContentId.java"


# instance fields
.field public final contentId:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "contentId"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "missing volume ID"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    .line 15
    const-string v0, "missing content ID"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 38
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 39
    goto :goto_0

    .line 40
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 41
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 42
    check-cast v0, Lcom/google/android/apps/books/sync/VolumeContentId;

    .line 43
    .local v0, "other":Lcom/google/android/apps/books/sync/VolumeContentId;
    iget-object v3, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 44
    iget-object v3, v0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 45
    goto :goto_0

    .line 46
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 47
    goto :goto_0

    .line 48
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 49
    iget-object v3, v0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 50
    goto :goto_0

    .line 51
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 52
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 27
    const/16 v0, 0x1f

    .line 28
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 29
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 30
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 31
    return v1

    .line 29
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 30
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 20
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "volumeId"

    iget-object v2, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "contentId"

    iget-object v2, p0, Lcom/google/android/apps/books/sync/VolumeContentId;->contentId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
