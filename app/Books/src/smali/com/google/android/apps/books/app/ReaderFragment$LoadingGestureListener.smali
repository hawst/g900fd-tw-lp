.class Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ReaderFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadingGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 6599
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 6599
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 6602
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6603
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUi:Lcom/google/android/ublib/view/SystemUi;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/ublib/view/SystemUi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/ublib/view/SystemUi;->setSystemUiVisible(Z)V

    .line 6607
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 6605
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$LoadingGestureListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cycleUiMode()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$13800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_0
.end method
