.class public Lcom/google/android/apps/books/widget/BookCoverContainerView;
.super Lcom/google/android/apps/books/widget/CoverContainerView;
.source "BookCoverContainerView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/CoverContainerView",
        "<",
        "Lcom/google/android/ublib/cardlib/layout/PlayCardView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/CoverContainerView;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method public hasCoverImage()Z
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->getCoverView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 23
    .local v0, "coverView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->hasCoverImage()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCoverViewMeasurable()Z
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->hasCoverImage()Z

    move-result v0

    return v0
.end method

.method public loadCoverView(Z)V
    .locals 2
    .param p1, "inLayout"    # Z

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->getCoverView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .line 29
    .local v0, "originalCoverView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-super {p0, p1}, Lcom/google/android/apps/books/widget/CoverContainerView;->loadCoverView(Z)V

    .line 30
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->getCoverView()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 33
    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->maybeLoadCoverImage()V

    .line 35
    :cond_0
    return-void
.end method
