.class abstract Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;
.super Ljava/lang/Object;
.source "NativeHeapBitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/NativeHeapBitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "BitmapSource"
.end annotation


# instance fields
.field protected final mConfig:Landroid/graphics/Bitmap$Config;

.field protected final mHeight:I

.field protected final mWidth:I

.field final synthetic this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;IILandroid/graphics/Bitmap$Config;)V
    .locals 0
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 134
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.BitmapSource;"
    iput-object p1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput p2, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->mWidth:I

    .line 136
    iput p3, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->mHeight:I

    .line 137
    iput-object p4, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->mConfig:Landroid/graphics/Bitmap$Config;

    .line 138
    return-void
.end method


# virtual methods
.method abstract getBitmap()Landroid/graphics/Bitmap;
.end method

.method getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "point"    # Landroid/graphics/Point;

    .prologue
    .line 143
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache<TT;>.BitmapSource;"
    iget v0, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$BitmapSource;->mHeight:I

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 144
    return-object p1
.end method
