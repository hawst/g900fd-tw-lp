.class Lcom/google/android/apps/books/app/ReaderFragment$20;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->createBookmarkListener()Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 3899
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarksChanged(Ljava/util/SortedMap;Ljava/util/SortedMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3906
    .local p1, "oldBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    .local p2, "newBookmarks":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/UnloadableEventual;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$20$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/books/app/ReaderFragment$20$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$20;Ljava/util/SortedMap;Ljava/util/SortedMap;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UnloadableEventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 3921
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$20;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->updateQuickBookmarks()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 3922
    return-void
.end method
