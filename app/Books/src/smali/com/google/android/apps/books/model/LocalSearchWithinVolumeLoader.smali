.class public Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;
.super Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;
.source "LocalSearchWithinVolumeLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;
    }
.end annotation


# static fields
.field private static final DONE:Lcom/google/android/apps/books/model/SearchResult;

.field private static final NOTES_LAYERS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAnnotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

.field private final mContentSource:Lcom/google/android/apps/books/render/ReaderDataSource;

.field private final mIncludeSentenceBeforeAndAfter:Z

.field private final mLocale:Ljava/util/Locale;

.field private final mNumThreads:I

.field private final mPattern:Ljava/util/regex/Pattern;

.field private final mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

.field private final mTracker:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 106
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->NOTES_LAYERS:Ljava/util/Set;

    .line 109
    new-instance v0, Lcom/google/android/apps/books/model/SearchResult;

    invoke-direct {v0, v2, v2, v1, v1}, Lcom/google/android/apps/books/model/SearchResult;-><init>(Lcom/google/android/apps/books/model/Snippet;Ljava/util/List;ZI)V

    sput-object v0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->DONE:Lcom/google/android/apps/books/model/SearchResult;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/render/ReaderDataSource;IZLjava/util/Locale;Lcom/google/android/apps/books/geo/AnnotationSet;Lcom/google/android/apps/books/model/VolumeMetadata;ILcom/google/android/apps/books/util/Logging$PerformanceTracker;Z)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "contentSource"    # Lcom/google/android/apps/books/render/ReaderDataSource;
    .param p4, "numThreads"    # I
    .param p5, "shouldSubstringSearch"    # Z
    .param p6, "locale"    # Ljava/util/Locale;
    .param p7, "annotationSet"    # Lcom/google/android/apps/books/geo/AnnotationSet;
    .param p8, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p9, "queryEmphasisColor"    # I
    .param p10, "tracker"    # Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    .param p11, "includeSentenceBeforeAndAfter"    # Z

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/SearchWithinVolumeLoader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iput-object p10, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mTracker:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 129
    iput-object p3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mContentSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    .line 130
    iput p4, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mNumThreads:I

    .line 131
    iput-object p6, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mLocale:Ljava/util/Locale;

    .line 132
    iput-object p7, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mAnnotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

    .line 133
    iput-boolean p11, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mIncludeSentenceBeforeAndAfter:Z

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p5}, Lcom/google/android/apps/books/util/StringUtils;->getQueryPattern(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mPattern:Ljava/util/regex/Pattern;

    .line 136
    iput-object p8, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 137
    const/4 v0, 0x0

    invoke-static {v0, p9}, Lcom/google/android/apps/books/model/Snippet;->builder(II)Lcom/google/android/apps/books/model/Snippet$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

    .line 138
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Lcom/google/android/apps/books/geo/AnnotationSet;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mAnnotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Ljava/util/regex/Pattern;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Lcom/google/android/apps/books/model/Snippet$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;)Lcom/google/android/apps/books/render/ReaderDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mContentSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/android/apps/books/model/SearchResult;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->DONE:Lcom/google/android/apps/books/model/SearchResult;

    return-object v0
.end method

.method private buildProcessor(Lcom/google/android/apps/books/annotations/TextLocationRange;)Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
    .locals 9
    .param p1, "positionRangeForSegment"    # Lcom/google/android/apps/books/annotations/TextLocationRange;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mAnnotationSet:Lcom/google/android/apps/books/geo/AnnotationSet;

    sget-object v1, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->NOTES_LAYERS:Ljava/util/Set;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/geo/AnnotationSet;->getAnnotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;Ljava/util/Set;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    .line 199
    .local v8, "annotationsInSegment":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    new-instance v3, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;

    invoke-direct {v3, p0, v8}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;-><init>(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;Lcom/google/common/collect/ImmutableList;)V

    .line 201
    .local v3, "progressListener":Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$AnnotationAddingSearchProgressListener;
    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;

    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mPattern:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mLocale:Ljava/util/Locale;

    iget-object v5, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

    iget-boolean v6, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mIncludeSentenceBeforeAndAfter:Z

    iget-object v7, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mTracker:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;-><init>(Ljava/util/regex/Pattern;ILcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;Ljava/util/Locale;Lcom/google/android/apps/books/model/Snippet$Builder;ZLcom/google/android/apps/books/util/Logging$PerformanceTracker;)V

    return-object v0
.end method

.method private collectResults(Ljava/util/List;Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 9
    .param p2, "listener"    # Lcom/google/android/apps/books/model/SearchResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/ChapterSearchResults;",
            ">;",
            "Lcom/google/android/apps/books/model/SearchResultListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 232
    .local p1, "chapterSearchResultsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/ChapterSearchResults;>;"
    const/4 v6, 0x0

    .line 233
    .local v6, "resultsAdded":I
    const/4 v2, -0x1

    .line 234
    .local v2, "currentChapterIndex":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/ChapterSearchResults;

    .line 236
    .local v1, "chapterSearchResults":Lcom/google/android/apps/books/model/ChapterSearchResults;
    :try_start_0
    iget-object v0, v1, Lcom/google/android/apps/books/model/ChapterSearchResults;->chapterQueue:Ljava/util/concurrent/BlockingQueue;

    .line 238
    .local v0, "chapterQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/apps/books/model/SearchResult;>;"
    :cond_1
    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/model/SearchResult;

    .local v7, "searchResult":Lcom/google/android/apps/books/model/SearchResult;
    sget-object v8, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->DONE:Lcom/google/android/apps/books/model/SearchResult;

    if-eq v7, v8, :cond_4

    .line 239
    invoke-direct {p0, v7}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->getChapterIndex(Lcom/google/android/apps/books/model/SearchResult;)I

    move-result v5

    .line 240
    .local v5, "resultChapterIndex":I
    if-eq v5, v2, :cond_2

    .line 241
    iget-object v8, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v8, v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterTitleForChapterIndex(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v8}, Lcom/google/android/apps/books/model/SearchResultListener;->onChapterStart(Ljava/lang/String;)Z

    .line 243
    move v2, v5

    .line 245
    :cond_2
    invoke-interface {p2, v7}, Lcom/google/android/apps/books/model/SearchResultListener;->onFound(Lcom/google/android/apps/books/model/SearchResult;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    add-int/lit8 v6, v6, 0x1

    const/16 v8, 0x3e8

    if-lt v6, v8, :cond_1

    .line 257
    .end local v0    # "chapterQueue":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/apps/books/model/SearchResult;>;"
    .end local v1    # "chapterSearchResults":Lcom/google/android/apps/books/model/ChapterSearchResults;
    .end local v5    # "resultChapterIndex":I
    .end local v7    # "searchResult":Lcom/google/android/apps/books/model/SearchResult;
    :cond_3
    :goto_0
    return-void

    .line 250
    .restart local v1    # "chapterSearchResults":Lcom/google/android/apps/books/model/ChapterSearchResults;
    :catch_0
    move-exception v3

    .line 251
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-interface {p2, v3}, Lcom/google/android/apps/books/model/SearchResultListener;->onError(Ljava/lang/Throwable;)V

    .line 253
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_4
    invoke-interface {p2}, Lcom/google/android/apps/books/model/SearchResultListener;->onChapterDone()Z

    move-result v8

    if-nez v8, :cond_0

    goto :goto_0
.end method

.method private getChapterIndex(Lcom/google/android/apps/books/model/SearchResult;)I
    .locals 3
    .param p1, "searchResult"    # Lcom/google/android/apps/books/model/SearchResult;

    .prologue
    .line 225
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/SearchResult;->getMatchRanges()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v0

    return v0
.end method

.method private getPositionRangeForSegmentIndex(Ljava/util/List;I)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 5
    .param p2, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;I)",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;"
        }
    .end annotation

    .prologue
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    const/4 v4, 0x0

    .line 206
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "startPosition":Ljava/lang/String;
    add-int/lit8 v2, p2, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    const/4 v0, 0x0

    .line 209
    .local v0, "endPosition":Ljava/lang/String;
    :goto_0
    new-instance v2, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v2, v1, v4, v0, v4}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-object v2

    .line 207
    .end local v0    # "endPosition":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, p2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected getSearchType()Ljava/lang/String;
    .locals 4

    .prologue
    .line 142
    const-string v0, "Local (%s threads)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mNumThreads:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleParseException(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 214
    const-string v0, "LSWVLoader"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "LSWVLoader"

    const-string v1, "Exception in parsing"

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 217
    :cond_0
    return-void
.end method

.method protected loadInBackgroundTimed(Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/model/SearchResultListener;

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->loadResults(Lcom/google/android/apps/books/model/SearchResultListener;)V

    .line 148
    return-void
.end method

.method public loadResults(Lcom/google/android/apps/books/model/SearchResultListener;)V
    .locals 11
    .param p1, "listener"    # Lcom/google/android/apps/books/model/SearchResultListener;

    .prologue
    .line 155
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 156
    .local v2, "doneParsing":Ljava/util/concurrent/atomic/AtomicBoolean;
    iget v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mNumThreads:I

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    .line 157
    .local v7, "executor":Ljava/util/concurrent/ExecutorService;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 158
    .local v6, "chapterSearchResultsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/ChapterSearchResults;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->mContentSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderDataSource;->getSegments()Ljava/util/List;

    move-result-object v10

    .line 159
    .local v10, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    const/4 v9, 0x0

    .local v9, "segmentIndex":I
    :goto_0
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 160
    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    .line 161
    .local v3, "segment":Lcom/google/android/apps/books/model/Segment;
    new-instance v5, Lcom/google/android/apps/books/model/ChapterSearchResults;

    invoke-direct {v5}, Lcom/google/android/apps/books/model/ChapterSearchResults;-><init>()V

    .line 162
    .local v5, "chapterSearchResults":Lcom/google/android/apps/books/model/ChapterSearchResults;
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, v5, Lcom/google/android/apps/books/model/ChapterSearchResults;->chapterQueue:Ljava/util/concurrent/BlockingQueue;

    .line 163
    invoke-direct {p0, v10, v9}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->getPositionRangeForSegmentIndex(Ljava/util/List;I)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->buildProcessor(Lcom/google/android/apps/books/annotations/TextLocationRange;)Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;

    move-result-object v8

    .line 165
    .local v8, "processor":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
    new-instance v4, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;

    iget-object v0, v5, Lcom/google/android/apps/books/model/ChapterSearchResults;->chapterQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {v4, v8, v2, v0}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;-><init>(Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/Collection;)V

    .line 171
    .local v4, "handler":Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    new-instance v0, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader$1;-><init>(Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;Lcom/google/android/apps/books/model/ChapterSearchResults;)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 159
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 188
    .end local v3    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v4    # "handler":Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
    .end local v5    # "chapterSearchResults":Lcom/google/android/apps/books/model/ChapterSearchResults;
    .end local v8    # "processor":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
    :cond_0
    invoke-interface {v7}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 189
    invoke-direct {p0, v6, p1}, Lcom/google/android/apps/books/model/LocalSearchWithinVolumeLoader;->collectResults(Ljava/util/List;Lcom/google/android/apps/books/model/SearchResultListener;)V

    .line 190
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 191
    return-void
.end method
