.class public Lcom/google/android/apps/books/util/IdentifiableCollection;
.super Ljava/lang/Object;
.source "IdentifiableCollection.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/util/Identifiable;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mCollection:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mIdToValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Collection;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TT;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Lcom/google/android/apps/books/util/IdentifiableCollection;, "Lcom/google/android/apps/books/util/IdentifiableCollection<TT;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    .local p2, "idToValue":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mCollection:Ljava/util/Collection;

    .line 23
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mIdToValue:Ljava/util/Map;

    .line 24
    return-void

    .line 23
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public static withLazyMap(Ljava/util/Collection;)Lcom/google/android/apps/books/util/IdentifiableCollection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/apps/books/util/Identifiable;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "Lcom/google/android/apps/books/util/IdentifiableCollection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "collection":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    new-instance v0, Lcom/google/android/apps/books/util/IdentifiableCollection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/util/IdentifiableCollection;-><init>(Ljava/util/Collection;Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getIdToValue()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/apps/books/util/IdentifiableCollection;, "Lcom/google/android/apps/books/util/IdentifiableCollection<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mIdToValue:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mCollection:Ljava/util/Collection;

    invoke-static {v0}, Lcom/google/android/apps/books/util/Identifiables;->buildIdToValue(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mIdToValue:Ljava/util/Map;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mIdToValue:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getValues()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/google/android/apps/books/util/IdentifiableCollection;, "Lcom/google/android/apps/books/util/IdentifiableCollection<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mCollection:Ljava/util/Collection;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/google/android/apps/books/util/IdentifiableCollection;, "Lcom/google/android/apps/books/util/IdentifiableCollection<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/util/IdentifiableCollection;->mCollection:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
