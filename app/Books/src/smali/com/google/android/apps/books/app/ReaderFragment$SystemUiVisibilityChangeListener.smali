.class Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/view/SystemUi$VisibilityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SystemUiVisibilityChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 2023
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 2023
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChanged(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    .line 2027
    const-string v1, "ReaderFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2028
    const-string v1, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSystemUiVisibilityChange() with visibility="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2031
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z
    invoke-static {v1, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4602(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 2032
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    .line 2033
    .local v0, "mode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v0, v1, :cond_1

    .line 2034
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowSystemUi(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)Z
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4700(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2035
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDismissSystemUI:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4800(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2039
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSystemUiVisible:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->shouldShowActionBar()Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setActionBarVisible(Z)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5100(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 2041
    :cond_1
    return-void

    .line 2037
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$SystemUiVisibilityChangeListener;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->cancelDismissSystemUI()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    goto :goto_0

    .line 2039
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
