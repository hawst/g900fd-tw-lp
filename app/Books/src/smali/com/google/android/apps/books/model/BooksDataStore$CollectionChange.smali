.class public Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;
.super Ljava/lang/Object;
.source "BooksDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CollectionChange"
.end annotation


# instance fields
.field public final type:Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 430
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->type:Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;

    .line 431
    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->volumeId:Ljava/lang/String;

    .line 432
    return-void
.end method
