.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->fetchVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getListResult(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)Lcom/google/android/apps/books/util/ExceptionOr;
    .locals 6
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;",
            ")",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 362
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v4, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->layerKey:Lcom/google/android/apps/books/annotations/Layer$Key;

    iget-object v5, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->contentVersion:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getLayer(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$500(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v2

    .line 363
    .local v2, "layer":Lcom/google/android/apps/books/annotations/Layer;
    if-nez v2, :cond_0

    .line 364
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Unable to find volume layer info"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    .end local v2    # "layer":Lcom/google/android/apps/books/annotations/Layer;
    :catch_0
    move-exception v1

    .line 380
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v3

    .line 366
    .restart local v2    # "layer":Lcom/google/android/apps/books/annotations/Layer;
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->contentVersion:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 368
    const-string v3, "AnnotationC"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 369
    const-string v3, "AnnotationC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Mismatched content versions: request="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->contentVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", layer="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Layer;->getContentVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_1
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Wrong content version"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 376
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v4, v2, Lcom/google/android/apps/books/annotations/Layer;->layerVersion:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->loadVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)Ljava/util/List;
    invoke-static {v3, p1, v4}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$600(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 378
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 348
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->getListResult(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    .line 349
    .local v0, "listResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->getVolumeVersion()Lcom/google/android/apps/books/annotations/VolumeVersion;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;Lcom/google/android/apps/books/util/ExceptionOr;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 356
    return-void
.end method
