.class public Lcom/google/android/apps/books/api/data/ApiaryVolume$AccessInfo;
.super Ljava/lang/Object;
.source "ApiaryVolume.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/data/ApiaryVolume;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AccessInfo"
.end annotation


# instance fields
.field public accessViewStatus:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public explicitOfflineLicenseManagement:Z
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public publicDomain:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public quoteSharingAllowed:Z
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public textToSpeechPermission:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public viewability:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
