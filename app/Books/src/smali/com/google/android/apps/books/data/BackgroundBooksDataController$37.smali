.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$lastAccess:J

.field final synthetic val$lastAction:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

.field final synthetic val$notifyContentChanged:Z

.field final synthetic val$position:Ljava/lang/String;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
    .locals 0

    .prologue
    .line 1191
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$position:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$lastAccess:J

    iput-object p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$lastAction:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    iput-boolean p7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$notifyContentChanged:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$position:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$lastAccess:J

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$lastAction:Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    iget-boolean v7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;->val$notifyContentChanged:Z

    invoke-interface/range {v1 .. v7}, Lcom/google/android/apps/books/model/BooksDataStore;->setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    .line 1196
    return-void
.end method
