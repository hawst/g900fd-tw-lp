.class Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;
.super Ljava/lang/Object;
.source "EndOfBookRecommendationsView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/RecommendationViewPopulator$OnStoreLinkClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->recommendedSubdivision(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Ljava/util/List;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Z)Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

.field final synthetic val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    iput-object p2, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;->val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/google/android/apps/books/app/StoreLink;)V
    .locals 3
    .param p1, "link"    # Lcom/google/android/apps/books/app/StoreLink;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;->this$0:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    # getter for: Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mRecommendationAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;
    invoke-static {v0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->access$100(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;->val$callbacks:Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/books/app/StoreLink;->viewStorePage(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Z)V

    .line 152
    return-void
.end method
