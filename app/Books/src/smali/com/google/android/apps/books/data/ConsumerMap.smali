.class Lcom/google/android/apps/books/data/ConsumerMap;
.super Ljava/lang/Object;
.source "ConsumerMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mConsumers:Lcom/google/common/collect/Multimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multimap",
            "<TK;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerMap;, "Lcom/google/android/apps/books/data/ConsumerMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ConsumerMap;->mConsumers:Lcom/google/common/collect/Multimap;

    return-void
.end method

.method private static logExtraConsumerInMap()V
    .locals 2

    .prologue
    .line 77
    const-string v0, "ConsumerMap"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "ConsumerMap"

    const-string v1, "Extra consumer is also in the map!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    return-void
.end method

.method private removeConsumers(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;>;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerMap;, "Lcom/google/android/apps/books/data/ConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ConsumerMap;->mConsumers:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Multimap;->removeAll(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerMap;, "Lcom/google/android/apps/books/data/ConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/ConsumerMap;->mConsumers:Lcom/google/common/collect/Multimap;

    invoke-interface {v2, p1}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 39
    .local v0, "consumers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/ublib/utils/Consumer<TV;>;>;"
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    .line 40
    .local v1, "wasEmpty":Z
    if-eqz p2, :cond_0

    .line 41
    invoke-interface {v0, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_0
    return v1
.end method

.method public hasConsumersForKey(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerMap;, "Lcom/google/android/apps/books/data/ConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ConsumerMap;->mConsumers:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, p1}, Lcom/google/common/collect/Multimap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lcom/google/android/apps/books/data/ConsumerMap;, "Lcom/google/android/apps/books/data/ConsumerMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "result":Ljava/lang/Object;, "TV;"
    .local p3, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    if-eqz p3, :cond_0

    .line 53
    invoke-interface {p3, p2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 55
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ConsumerMap;->removeConsumers(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    .line 56
    .local v1, "consumers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/ublib/utils/Consumer<TV;>;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/utils/Consumer;

    .line 57
    .local v0, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    if-ne v0, p3, :cond_1

    .line 58
    invoke-static {}, Lcom/google/android/apps/books/data/ConsumerMap;->logExtraConsumerInMap()V

    goto :goto_0

    .line 60
    :cond_1
    invoke-interface {v0, p2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    .end local v0    # "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TV;>;"
    :cond_2
    return-void
.end method
