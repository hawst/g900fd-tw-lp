.class public final Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
.super Ljava/lang/Object;
.source "DictionaryMetadata.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final mDictionaryName:Ljava/lang/String;

.field private final mDictionarySizeInBytes:J

.field private final mDownloadUrl:Ljava/lang/String;

.field private final mEncryptedDictionaryKey:[B

.field private final mLanguageCode:Ljava/lang/String;

.field private final mSessionKeyVersion:Ljava/lang/String;

.field private final mVersion:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    .line 153
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/books/dictionary/DictionaryMetadata$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J[BLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "languageCode"    # Ljava/lang/String;
    .param p2, "version"    # J
    .param p4, "encryptedDictionaryKey"    # [B
    .param p5, "downloadUrl"    # Ljava/lang/String;
    .param p6, "dictionarySizeInBytes"    # J
    .param p8, "sessionKeyVersion"    # Ljava/lang/String;
    .param p9, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    .line 34
    iput-wide p2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    .line 35
    array-length v0, p4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    .line 38
    iput-wide p6, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    .line 39
    iput-object p8, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    .line 40
    iput-object p9, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    array-length v1, p4

    invoke-static {p4, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 45
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p0, p1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v1

    .line 84
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 85
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 88
    check-cast v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 90
    .local v0, "that":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 91
    goto :goto_0

    .line 94
    :cond_4
    iget-wide v4, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    iget-wide v6, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v1, v2

    .line 95
    goto :goto_0

    .line 97
    :cond_5
    iget-wide v4, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    iget-wide v6, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    move v1, v2

    .line 98
    goto :goto_0

    .line 101
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 102
    goto :goto_0

    .line 104
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    iget-object v4, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 105
    goto :goto_0

    .line 107
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 108
    goto :goto_0

    .line 110
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 111
    goto :goto_0

    .line 113
    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 114
    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getDictionaryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    return-object v0
.end method

.method public getDictionarySizeInBytes()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    return-wide v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptedDictionaryKey()[B
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    return-object v0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionKeyVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 168
    invoke-static {p0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "language code"

    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "version"

    iget-wide v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "dictionary name"

    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "download url"

    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "size"

    iget-wide v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;J)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "session key version"

    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "account name"

    iget-object v2, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/repackaged/com/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mLanguageCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    iget-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mVersion:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mEncryptedDictionaryKey:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionaryName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDownloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-wide v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mDictionarySizeInBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mSessionKeyVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->mAccountName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    return-void
.end method
