.class Lcom/google/android/apps/books/app/HomeFragment$10;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$10;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 864
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$10;->take(Ljava/util/List;)V

    return-void
.end method

.method public take(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 867
    .local p1, "t":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$10;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->maybeSetupRecommendationFooter(Ljava/util/List;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/HomeFragment;->access$2200(Lcom/google/android/apps/books/app/HomeFragment;Ljava/util/List;)V

    .line 868
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$10;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/HomeFragment$10$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$10$1;-><init>(Lcom/google/android/apps/books/app/HomeFragment$10;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 874
    return-void
.end method
