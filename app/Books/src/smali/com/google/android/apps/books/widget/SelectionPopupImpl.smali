.class public Lcom/google/android/apps/books/widget/SelectionPopupImpl;
.super Ljava/lang/Object;
.source "SelectionPopupImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/SelectionPopup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;,
        Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;,
        Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
    }
.end annotation


# instance fields
.field private mContentsReady:Z

.field private final mContext:Landroid/content/Context;

.field private final mFadeController:Lcom/google/android/ublib/view/FadeAnimationController;

.field private mLocationReady:Z

.field private final mScreenHeight:I

.field private final mScreenWidth:I

.field private final mSelectionInstanceProvider:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;

.field private mSelectionLocation:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;

.field protected mShowRequestId:I

.field private final mVertical:Z

.field protected final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;)V
    .locals 6
    .param p1, "delegate"    # Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    .param p2, "selectionInstanceProvider"    # Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;

    .prologue
    const/4 v5, -0x2

    const/4 v3, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContentsReady:Z

    .line 58
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mLocationReady:Z

    .line 61
    iput v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mShowRequestId:I

    .line 70
    invoke-interface {p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->isVertical()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mVertical:Z

    .line 71
    iput-object p2, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mSelectionInstanceProvider:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;

    .line 72
    invoke-interface {p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContext:Landroid/content/Context;

    .line 74
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 76
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f0400be

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    .line 81
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 84
    .local v2, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-interface {p1, v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->attachSelectionPopup(Landroid/view/View;)V

    .line 86
    new-instance v4, Lcom/google/android/ublib/view/FadeAnimationController;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    const/4 v5, 0x4

    invoke-direct {v4, v3, v5}, Lcom/google/android/ublib/view/FadeAnimationController;-><init>(Landroid/view/View;I)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mFadeController:Lcom/google/android/ublib/view/FadeAnimationController;

    .line 88
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 90
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenWidth:I

    .line 91
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenHeight:I

    .line 92
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mVertical:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/SelectionPopupImpl;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SelectionPopupImpl;

    .prologue
    .line 24
    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mScreenWidth:I

    return v0
.end method

.method private getSelectionRect()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
    .locals 2

    .prologue
    .line 154
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mSelectionInstanceProvider:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;->getCurrentSelectionInstance()Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    move-result-object v0

    .line 156
    .local v0, "selectionInstance":Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getSelectionInfo()Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getHighlightScreenRect()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    move-result-object v1

    .line 159
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private refreshContents()V
    .locals 2

    .prologue
    .line 146
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mSelectionInstanceProvider:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionInstanceProvider;->getCurrentSelectionInstance()Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    move-result-object v0

    .line 148
    .local v0, "selectionInstance":Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    if-eqz v0, :cond_0

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->setupPopup(Landroid/view/View;)V

    .line 151
    :cond_0
    return-void
.end method

.method private relocate()V
    .locals 10

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->getSelectionRect()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    move-result-object v9

    .line 128
    .local v9, "selectionInfo":Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
    if-nez v9, :cond_0

    .line 138
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-virtual {v9}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    .line 133
    .local v8, "selectionBounds":Landroid/graphics/Rect;
    new-instance v0, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;

    iget v2, v8, Landroid/graphics/Rect;->left:I

    iget v3, v8, Landroid/graphics/Rect;->right:I

    iget v4, v8, Landroid/graphics/Rect;->top:I

    iget v5, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->getHandleLongerAxis()I

    move-result v6

    # getter for: Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->mHandleAnchorPoint:Landroid/graphics/Point;
    invoke-static {v9}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;->access$000(Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;)Landroid/graphics/Point;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;-><init>(Lcom/google/android/apps/books/widget/SelectionPopupImpl;IIIIILandroid/graphics/Point;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mSelectionLocation:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->layout()V

    goto :goto_0
.end method


# virtual methods
.method public getShowRequestId()I
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mShowRequestId:I

    return v0
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mFadeController:Lcom/google/android/ublib/view/FadeAnimationController;

    invoke-virtual {v0}, Lcom/google/android/ublib/view/FadeAnimationController;->getVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-void

    .line 119
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContentsReady:Z

    .line 120
    iput-boolean v3, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mLocationReady:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mFadeController:Lcom/google/android/ublib/view/FadeAnimationController;

    const/16 v1, 0x113

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZILcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    goto :goto_0
.end method

.method public layout()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mSelectionLocation:Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;

    # invokes: Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->layout()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;->access$100(Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionLocation;)V

    .line 143
    return-void
.end method

.method public maybeShow()V
    .locals 4

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mLocationReady:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContentsReady:Z

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->refreshContents()V

    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->relocate()V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mFadeController:Lcom/google/android/ublib/view/FadeAnimationController;

    const/4 v1, 0x1

    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/ublib/view/FadeAnimationController;->setVisible(ZILcom/google/android/ublib/view/FadeAnimationController$OnVisibilityChangedListener;)V

    .line 111
    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mShowRequestId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mShowRequestId:I

    goto :goto_0
.end method

.method public setContentReady()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mContentsReady:Z

    .line 96
    return-void
.end method

.method public setLocationReady()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionPopupImpl;->mLocationReady:Z

    .line 100
    return-void
.end method
