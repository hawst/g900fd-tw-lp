.class Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

.field final synthetic val$app:Lcom/google/android/apps/books/app/BooksApplication;

.field final synthetic val$this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/BooksApplication;)V
    .locals 0

    .prologue
    .line 1193
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->val$this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->val$app:Lcom/google/android/apps/books/app/BooksApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Boolean;)V
    .locals 6
    .param p1, "recommendationsEnabled"    # Ljava/lang/Boolean;

    .prologue
    .line 1196
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1205
    :goto_0
    return-void

    .line 1199
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v3, v3, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    .line 1200
    .local v2, "volumeId":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v3, v3, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;->forLastPage(Ljava/lang/String;Z)Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;

    move-result-object v0

    .line 1202
    .local v0, "recsContext":Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;
    new-instance v1, Lcom/google/android/apps/books/app/LoadRecommendationsTask;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->val$app:Lcom/google/android/apps/books/app/BooksApplication;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v4, v4, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mConsumer:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;
    invoke-static {v5}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->access$2100(Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;)Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;

    move-result-object v5

    invoke-direct {v1, v3, v4, v0, v5}, Lcom/google/android/apps/books/app/LoadRecommendationsTask;-><init>(Lcom/google/android/apps/books/app/BooksApplication;Landroid/accounts/Account;Lcom/google/android/apps/books/app/LoadRecommendationsTask$RecommendationsContext;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1204
    .local v1, "task":Lcom/google/android/apps/books/app/LoadRecommendationsTask;
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-static {v1, v3}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1193
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$1;->take(Ljava/lang/Boolean;)V

    return-void
.end method
