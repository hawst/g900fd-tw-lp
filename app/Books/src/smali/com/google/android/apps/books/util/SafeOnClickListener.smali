.class public abstract Lcom/google/android/apps/books/util/SafeOnClickListener;
.super Ljava/lang/Object;
.source "SafeOnClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 18
    invoke-static {p1}, Lcom/google/android/apps/books/util/ViewUtils;->getActivityFromView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 20
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/util/SafeOnClickListener;->onClickSafely(Landroid/view/View;Landroid/app/Activity;)V

    .line 23
    :cond_0
    return-void
.end method

.method public abstract onClickSafely(Landroid/view/View;Landroid/app/Activity;)V
.end method
