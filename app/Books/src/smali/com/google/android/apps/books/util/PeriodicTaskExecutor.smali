.class public interface abstract Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
.super Ljava/lang/Object;
.source "PeriodicTaskExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;,
        Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;
    }
.end annotation


# virtual methods
.method public abstract delay(J)V
.end method

.method public abstract schedule()V
.end method

.method public abstract stop()V
.end method
