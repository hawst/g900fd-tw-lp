.class Lcom/google/android/apps/books/app/HomeSideDrawer$2;
.super Ljava/lang/Object;
.source "HomeSideDrawer.java"

# interfaces
.implements Landroid/support/v4/widget/DrawerLayout$DrawerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeSideDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeSideDrawer;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$2;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$2;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$000(Lcom/google/android/apps/books/app/HomeSideDrawer;)Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->onSideDrawerClosed()V

    .line 156
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeSideDrawer$2;->this$0:Lcom/google/android/apps/books/app/HomeSideDrawer;

    # getter for: Lcom/google/android/apps/books/app/HomeSideDrawer;->mCallbacks:Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer;->access$000(Lcom/google/android/apps/books/app/HomeSideDrawer;)Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/HomeSideDrawer$Callbacks;->onSideDrawerOpened()V

    .line 151
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 146
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 0
    .param p1, "newState"    # I

    .prologue
    .line 161
    return-void
.end method
