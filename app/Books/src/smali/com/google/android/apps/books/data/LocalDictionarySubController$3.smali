.class Lcom/google/android/apps/books/data/LocalDictionarySubController$3;
.super Ljava/lang/Object;
.source "LocalDictionarySubController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/LocalDictionarySubController;->downloadDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

.field final synthetic val$completedCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

.field final synthetic val$inputStream:Ljava/io/InputStream;

.field final synthetic val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

.field final synthetic val$networkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/model/BooksDataStore$Committer;Ljava/io/InputStream;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    iput-object p3, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$completedCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    iput-object p4, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$inputStream:Ljava/io/InputStream;

    iput-object p5, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$networkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 309
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    # invokes: Lcom/google/android/apps/books/data/LocalDictionarySubController;->isInProgress(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$100(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$completedCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->commit()V

    .line 315
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    iget-object v3, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$networkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    # invokes: Lcom/google/android/apps/books/data/LocalDictionarySubController;->finishedDownload(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$200(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    .line 316
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_DOWNLOAD_FINISHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logDictionaryAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;)V

    .line 324
    :goto_1
    return-void

    .line 312
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$completedCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->bestEffortAbort()V

    .line 313
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$inputStream:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$inputStream:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 321
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$completedCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->bestEffortAbort()V

    .line 322
    iget-object v1, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->this$0:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$metadata:Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    iget-object v3, p0, Lcom/google/android/apps/books/data/LocalDictionarySubController$3;->val$networkTaskServices:Lcom/google/android/apps/books/data/NetworkTaskServices;

    # invokes: Lcom/google/android/apps/books/data/LocalDictionarySubController;->downloadFailed(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->access$300(Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/dictionary/DictionaryMetadata;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    goto :goto_1
.end method
