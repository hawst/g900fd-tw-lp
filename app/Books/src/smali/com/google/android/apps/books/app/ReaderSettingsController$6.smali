.class Lcom/google/android/apps/books/app/ReaderSettingsController$6;
.super Ljava/lang/Object;
.source "ReaderSettingsController.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderSettingsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$6;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$6;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$6;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$6;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextSize:Lcom/google/android/apps/books/preference/TextZoomPreference;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$200(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/preference/TextZoomPreference;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/TextZoomPreference;->getValue()F

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onTextZoomSettingChanged(FZ)V

    .line 449
    :cond_0
    return-void
.end method
