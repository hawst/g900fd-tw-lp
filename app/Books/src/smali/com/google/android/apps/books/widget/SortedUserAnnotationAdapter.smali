.class public Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SortedUserAnnotationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAnnotationChangeListener:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

.field private final mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

.field private mCopyOfCurrentAnnotations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayerId:Ljava/lang/String;

.field private mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "annotationSet"    # Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    .param p4, "layerId"    # Ljava/lang/String;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 63
    new-instance v0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$1;-><init>(Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationChangeListener:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

    .line 76
    iput-object p3, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    .line 77
    iput-object p4, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mLayerId:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationChangeListener:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

    invoke-virtual {v0, p4, v1}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->addAnnotationSetChangeListener(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;)V

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 80
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mCopyOfCurrentAnnotations:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;)Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    return-object p1
.end method

.method private getAnnotations()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mCopyOfCurrentAnnotations:Ljava/util/List;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mLayerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->getAnnotationListCopy(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mCopyOfCurrentAnnotations:Ljava/util/List;

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mCopyOfCurrentAnnotations:Ljava/util/List;

    return-object v0
.end method

.method private getBimap()Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 100
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    if-nez v6, :cond_4

    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getAnnotations()Ljava/util/List;

    move-result-object v1

    .line 102
    .local v1, "currentAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    const/4 v2, -0x1

    .line 103
    .local v2, "currentChapterIndex":I
    const/4 v3, 0x0

    .line 104
    .local v3, "currentSpanLength":I
    new-instance v6, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-direct {v6}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    .line 105
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 106
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-nez v0, :cond_0

    .line 110
    const-string v6, "AnnotationAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Annotation should not be null! Layer: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mLayerId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v5

    .line 114
    .local v5, "newChapterIndex":I
    if-eq v2, v5, :cond_2

    .line 115
    if-lez v3, :cond_1

    .line 116
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v7, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-virtual {v6, v11, v3, v7, v3}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 118
    const/4 v3, 0x0

    .line 120
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v7, v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterTitleForChapterIndex(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    sget-object v9, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 123
    move v2, v5

    .line 125
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 126
    goto :goto_0

    .line 128
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v5    # "newChapterIndex":I
    :cond_3
    if-lez v3, :cond_4

    .line 129
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v7, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-virtual {v6, v11, v3, v7, v3}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 133
    .end local v1    # "currentAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .end local v2    # "currentChapterIndex":I
    .end local v3    # "currentSpanLength":I
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mViewIndexToAnnotationIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    return-object v6
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return v0
.end method

.method public getAnnotationAtViewPosition(I)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 2
    .param p1, "viewPosition"    # I

    .prologue
    .line 158
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getChapterHeading(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 159
    const/4 v1, 0x0

    .line 162
    :goto_0
    return-object v1

    .line 161
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getBimap()Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v0

    .line 162
    .local v0, "annotationPosition":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getAnnotations()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/Annotation;

    goto :goto_0
.end method

.method public getChapterHeading(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 238
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getBimap()Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getBimap()Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getLeftSize()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Integer;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 89
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getItem(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getChapterHeading(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    .line 207
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOnItemClickListener(Lcom/google/android/apps/books/app/ContentsView$Callbacks;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p1, "callbacks"    # Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .prologue
    .line 243
    new-instance v0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;-><init>(Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V

    return-object v0
.end method

.method protected getPageTitle(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 194
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 196
    .local v1, "position":Lcom/google/android/apps/books/common/Position;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 198
    :goto_0
    return-object v2

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, ""

    goto :goto_0
.end method

.method protected getSnippet(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 180
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getSnippet()Ljava/lang/String;

    move-result-object v2

    .line 181
    .local v2, "snippet":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 189
    .end local v2    # "snippet":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 185
    .restart local v2    # "snippet":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getBestPositionForToc()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 187
    .local v1, "position":Lcom/google/android/apps/books/common/Position;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3, v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, ""

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "viewPosition"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getChapterHeading(I)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "chapterHeading":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 141
    const v2, 0x7f040024

    invoke-static {p3, p2, v2}, Lcom/google/android/apps/books/util/ViewUtils;->maybeInflateAdapterItemView(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 143
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0e00c4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getAnnotationAtViewPosition(I)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    invoke-virtual {p0, p3, p2, v2}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getViewForAnnotation(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/books/annotations/Annotation;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method protected getViewForAnnotation(Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/apps/books/annotations/Annotation;)Landroid/view/View;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 172
    const v1, 0x7f040035

    invoke-static {p1, p2, v1}, Lcom/google/android/apps/books/util/ViewUtils;->maybeInflateAdapterItemView(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 174
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e003d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getSnippet(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    const v1, 0x7f0e003e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0, p3}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getPageTitle(Lcom/google/android/apps/books/annotations/Annotation;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getChapterHeading(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mLayerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->hasLoaded(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationSet:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mLayerId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->mAnnotationChangeListener:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->removeAnnotationSetChangeListener(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;)V

    .line 222
    return-void
.end method
