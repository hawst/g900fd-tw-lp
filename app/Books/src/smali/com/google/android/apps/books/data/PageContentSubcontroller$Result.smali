.class Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;
.super Ljava/lang/Object;
.source "PageContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/PageContentSubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Result"
.end annotation


# instance fields
.field final blob:Lcom/google/android/apps/books/data/DataControllerBlob;

.field final ccBox:Lcom/google/common/base/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;"
        }
    .end annotation
.end field

.field final sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/common/base/Supplier;)V
    .locals 0
    .param p1, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/DataControllerBlob;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/common/base/Supplier",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p3, "ccBox":Lcom/google/common/base/Supplier;, "Lcom/google/common/base/Supplier<Lcom/google/android/apps/books/model/CcBox;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 64
    iput-object p3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;->ccBox:Lcom/google/common/base/Supplier;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/common/base/Supplier;Lcom/google/android/apps/books/data/PageContentSubcontroller$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x2"    # Lcom/google/common/base/Supplier;
    .param p4, "x3"    # Lcom/google/android/apps/books/data/PageContentSubcontroller$1;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/common/base/Supplier;)V

    return-void
.end method
