.class Lcom/google/android/apps/books/widget/PagesViewController$3;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0

    .prologue
    .line 903
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccessibleSelectionChanged(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;II)V
    .locals 1
    .param p1, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "selStart"    # I
    .param p4, "selEnd"    # I

    .prologue
    .line 909
    if-ne p3, p4, :cond_1

    .line 910
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->isSelectionLocked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 914
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1000(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 927
    :cond_0
    :goto_0
    return-void

    .line 917
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->isTextSelected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 918
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1100(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onStartedSelection()V

    .line 920
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->invalidatePageContent()V

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->setupSelectionOverlay()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1300(Lcom/google/android/apps/books/widget/PagesViewController;)V

    .line 922
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->hideSelectionHandles()V

    .line 925
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$3;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v0

    invoke-interface {v0, p3, p4, p1}, Lcom/google/android/apps/books/render/Renderer;->onAccessibleSelectionChanged(IILcom/google/android/apps/books/widget/DevicePageRendering;)V

    goto :goto_0
.end method
