.class final enum Lcom/google/android/apps/books/app/LibraryFilter$1;
.super Lcom/google/android/apps/books/app/LibraryFilter;
.source "LibraryFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LibraryFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "x0"    # I

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/app/LibraryFilter;-><init>(Ljava/lang/String;IILcom/google/android/apps/books/app/LibraryFilter$1;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/books/widget/CardData;)Z
    .locals 1
    .param p1, "data"    # Lcom/google/android/apps/books/widget/CardData;

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->hasVolumeData()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/CardData;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/LibraryFilter$1;->passesCurrentDownloadFilter(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/apps/books/widget/CardData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LibraryFilter$1;->apply(Lcom/google/android/apps/books/widget/CardData;)Z

    move-result v0

    return v0
.end method

.method public shouldShowWhenEmpty(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method
