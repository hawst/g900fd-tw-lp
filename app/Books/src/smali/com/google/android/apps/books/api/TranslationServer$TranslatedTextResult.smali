.class public Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;
.super Ljava/lang/Object;
.source "TranslationServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/api/TranslationServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TranslatedTextResult"
.end annotation


# instance fields
.field public final detectedLanguage:Ljava/lang/String;

.field public final translation:Ljava/lang/CharSequence;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;->detectedLanguage:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;->translation:Ljava/lang/CharSequence;

    .line 23
    return-void
.end method
