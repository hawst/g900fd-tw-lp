.class public Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;,
        Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;,
        Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;,
        Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;
    }
.end annotation


# static fields
.field public static final DATESTAMP_IN_THE_DISTANT_PAST:I = 0x1

.field private static final DEFAULT_ANNOTATION_STORAGE_POLICY:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;


# instance fields
.field private final mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

.field private final mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mCaches:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;

.field private mDefaultImageHeight:I

.field private mDefaultImageWidth:I

.field private final mImageController:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

.field private final mLayerCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$Key;",
            "Lcom/google/android/apps/books/annotations/Layer;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoadedLayers:Lcom/google/common/collect/Multimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/Multimap",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLoadingAnnotationDatas:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            ">;"
        }
    .end annotation
.end field

.field private final mPolicy:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;

.field private final mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->DEFAULT_ANNOTATION_STORAGE_POLICY:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;Lcom/google/android/apps/books/annotations/AnnotationServer;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "db"    # Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    .param p4, "server"    # Lcom/google/android/apps/books/annotations/AnnotationServer;

    .prologue
    .line 141
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->makeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v2

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getResponseGetter(Landroid/content/Context;)Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v5

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->makeBlobStore(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/BlobStore;

    move-result-object v6

    sget-object v8, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->DEFAULT_ANNOTATION_STORAGE_POLICY:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;

    move-object v0, p0

    move-object v4, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;Lcom/google/android/apps/books/net/ResponseGetter;Lcom/google/android/apps/books/annotations/BlobStore;Lcom/google/android/apps/books/annotations/AnnotationServer;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;)V

    .line 145
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;Lcom/google/android/apps/books/net/ResponseGetter;Lcom/google/android/apps/books/annotations/BlobStore;Lcom/google/android/apps/books/annotations/AnnotationServer;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;)V
    .locals 2
    .param p1, "callbacks"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;
    .param p2, "uiThreadExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "backgroundExecutor"    # Ljava/util/concurrent/ExecutorService;
    .param p4, "db"    # Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    .param p5, "responseGetter"    # Lcom/google/android/apps/books/net/ResponseGetter;
    .param p6, "blobStore"    # Lcom/google/android/apps/books/annotations/BlobStore;
    .param p7, "server"    # Lcom/google/android/apps/books/annotations/AnnotationServer;
    .param p8, "policy"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;

    .prologue
    const/4 v1, -0x1

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCaches:Ljava/util/Map;

    .line 84
    invoke-static {}, Lcom/google/common/collect/HashMultimap;->create()Lcom/google/common/collect/HashMultimap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadedLayers:Lcom/google/common/collect/Multimap;

    .line 88
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadingAnnotationDatas:Ljava/util/Set;

    .line 95
    iput v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mDefaultImageWidth:I

    .line 96
    iput v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mDefaultImageHeight:I

    .line 128
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLayerCache:Ljava/util/Map;

    .line 180
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCallbacks:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;

    .line 181
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    .line 182
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 183
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    .line 184
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    invoke-direct {v0, p6, p5, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;-><init>(Lcom/google/android/apps/books/annotations/BlobStore;Lcom/google/android/apps/books/net/ResponseGetter;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mImageController:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    .line 186
    iput-object p8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mPolicy:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;

    .line 187
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationServerController;

    invoke-direct {v0, p7}, Lcom/google/android/apps/books/annotations/AnnotationServerController;-><init>(Lcom/google/android/apps/books/annotations/AnnotationServer;)V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    .line 188
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Ljava/util/Collection;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->syncOnBackgroundThread(Ljava/util/List;Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "x2"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer$Key;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "x2"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getCopyQuotaFromServer(Lcom/google/android/apps/books/annotations/Layer$Key;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Layer;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->onNewLayer(Lcom/google/android/apps/books/annotations/Layer;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Layer$Key;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getLocalLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyEditFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyAddFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeVersion;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getAnnotationCache(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Ljava/lang/Runnable;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "x2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getLayer(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .param p2, "x2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->loadVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "x2"    # Ljava/util/Map;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->deliverAnnotationDataResults(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/AnnotationServerController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadingAnnotationDatas:Ljava/util/Set;

    return-object v0
.end method

.method private deliverAnnotationDataResults(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Map;)V
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 475
    .local p2, "results":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$11;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/Map;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 484
    return-void
.end method

.method private downloadAnnotationProcessor(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)Lcom/google/android/apps/books/annotations/AnnotationProcessor;
    .locals 7
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;
    .param p3, "lastSyncDate"    # J

    .prologue
    .line 873
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$16;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)V

    return-object v0
.end method

.method private downloadUpdatesForVolumes(Ljava/util/List;Ljava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 847
    .local p1, "volumeVersions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeVersion;>;"
    .local p2, "layers":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 848
    .local v6, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 849
    .local v4, "layer":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mPolicy:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;

    invoke-interface {v7, v4}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationStoragePolicy;->shouldStoreSyncedAnnotations(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 852
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCallbacks:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;

    invoke-interface {v7}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;->shouldAlwaysAskForFullAnnotationRefresh()Z

    move-result v7

    if-eqz v7, :cond_2

    const-wide/16 v2, 0x1

    .line 856
    .local v2, "lastSyncDate":J
    :goto_1
    new-instance v5, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;

    invoke-direct {v5, v6, v4, v2, v3}, Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;-><init>(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)V

    .line 858
    .local v5, "request":Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    invoke-direct {p0, v6, v4, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->downloadAnnotationProcessor(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;J)Lcom/google/android/apps/books/annotations/AnnotationProcessor;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->downloadUserAnnotations(Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V

    goto :goto_0

    .line 852
    .end local v2    # "lastSyncDate":J
    .end local v5    # "request":Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v7, v6, v4}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->getLatestServerTimestamp(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_1

    .line 862
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "layer":Ljava/lang/String;
    .end local v6    # "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    :cond_3
    return-void
.end method

.method private getAnnotationCache(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;
    .locals 2
    .param p1, "volumeVersion"    # Lcom/google/android/apps/books/annotations/VolumeVersion;

    .prologue
    .line 990
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCaches:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 991
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCaches:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-direct {v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCaches:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    return-object v0
.end method

.method private getCopyQuotaFromServer(Lcom/google/android/apps/books/annotations/Layer$Key;Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .locals 5
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 488
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 489
    .local v0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    invoke-virtual {v2, p2, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->getLayerCharacterQuota(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 491
    .local v1, "serverQuotas":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    return-object v2
.end method

.method private getLayer(Lcom/google/android/apps/books/annotations/Layer$Key;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 7
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;
    .param p2, "contentVersion"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 669
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getLocalLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v1

    .line 670
    .local v1, "localResult":Lcom/google/android/apps/books/annotations/Layer;
    if-eqz v1, :cond_0

    .line 689
    .end local v1    # "localResult":Lcom/google/android/apps/books/annotations/Layer;
    :goto_0
    return-object v1

    .line 674
    .restart local v1    # "localResult":Lcom/google/android/apps/books/annotations/Layer;
    :cond_0
    new-instance v5, Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v6, p1, Lcom/google/android/apps/books/annotations/Layer$Key;->volumeId:Ljava/lang/String;

    invoke-direct {v5, v6, p2}, Lcom/google/android/apps/books/annotations/VolumeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    .local v5, "vv":Lcom/google/android/apps/books/annotations/VolumeVersion;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    invoke-virtual {v6, v5}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->getVolumeLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;)Ljava/util/List;

    move-result-object v4

    .line 676
    .local v4, "serverLayers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    const/4 v2, 0x0

    .line 678
    .local v2, "result":Lcom/google/android/apps/books/annotations/Layer;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/Layer;

    .line 679
    .local v3, "serverLayer":Lcom/google/android/apps/books/annotations/Layer;
    iget-object v6, v3, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/books/annotations/Layer$Key;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 681
    move-object v2, v3

    .line 686
    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->onNewLayer(Lcom/google/android/apps/books/annotations/Layer;)V

    goto :goto_1

    .end local v3    # "serverLayer":Lcom/google/android/apps/books/annotations/Layer;
    :cond_2
    move-object v1, v2

    .line 689
    goto :goto_0
.end method

.method private getLocalLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;
    .locals 3
    .param p1, "key"    # Lcom/google/android/apps/books/annotations/Layer$Key;

    .prologue
    .line 717
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLayerCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer;

    .line 718
    .local v0, "cached":Lcom/google/android/apps/books/annotations/Layer;
    if-eqz v0, :cond_0

    .line 728
    .end local v0    # "cached":Lcom/google/android/apps/books/annotations/Layer;
    :goto_0
    return-object v0

    .line 722
    .restart local v0    # "cached":Lcom/google/android/apps/books/annotations/Layer;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->getLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v1

    .line 723
    .local v1, "fromDatabase":Lcom/google/android/apps/books/annotations/Layer;
    if-eqz v1, :cond_1

    .line 724
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLayerCache:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 725
    goto :goto_0

    .line 728
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p3, "runnable"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 739
    .local p2, "layersToSync":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/Runnable;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 754
    return-void
.end method

.method private isCopyAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Z
    .locals 2
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 803
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private loadVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .param p2, "version"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 395
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 396
    .local v5, "serverAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/ServerAnnotation;>;"
    new-instance v2, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$7;

    invoke-direct {v2, p0, v5}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$7;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;)V

    .line 402
    .local v2, "processor":Lcom/google/android/apps/books/annotations/AnnotationProcessor;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->makeLayer(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v7

    iget v8, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->segmentIndex:I

    invoke-interface {v6, v7, v8}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->getSegmentVolumeAnnotations(Lcom/google/android/apps/books/annotations/Layer;I)Ljava/util/List;

    move-result-object v0

    .line 404
    .local v0, "cachedAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-eqz v0, :cond_0

    .line 417
    .end local v0    # "cachedAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :goto_0
    return-object v0

    .line 408
    .restart local v0    # "cachedAnnotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    invoke-virtual {v6, p1, p2, v2}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->downloadVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V

    .line 409
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->makeLayer(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v7

    iget v8, p1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->segmentIndex:I

    const/4 v9, 0x1

    invoke-interface {v6, v7, v8, v5, v9}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->setSegmentVolumeAnnotations(Lcom/google/android/apps/books/annotations/Layer;ILjava/util/List;Z)V

    .line 413
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 414
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .line 415
    .local v4, "serverAnnotation":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    iget-object v6, v4, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .end local v4    # "serverAnnotation":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    :cond_1
    move-object v0, v3

    .line 417
    goto :goto_0
.end method

.method private locallyApplyAddFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 2
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 998
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    sget-object v1, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->UNKNOWN_SEGMENT_INDEX:Ljava/lang/Integer;

    invoke-interface {v0, p1, p3, v1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->addUserAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/Integer;)V

    .line 1000
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 1006
    return-void
.end method

.method private locallyApplyEditFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/ServerAnnotation;

    .prologue
    .line 1015
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v0, p1, p3}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->editAnnotationFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    .line 1016
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$19;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/ServerAnnotation;Ljava/lang/String;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 1045
    return-void
.end method

.method private static makeBlobStore(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/BlobStore;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 166
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    .line 167
    .local v3, "filesDir":Ljava/io/File;
    invoke-static {v3, p1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 168
    .local v1, "accountDir":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v5, "blob_index"

    invoke-direct {v4, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 169
    .local v4, "indexFile":Ljava/io/File;
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 171
    .local v0, "accountCacheDir":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v5, "blobs"

    invoke-direct {v2, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 172
    .local v2, "blobsDir":Ljava/io/File;
    new-instance v5, Lcom/google/android/apps/books/annotations/DiskBlobStore;

    const/high16 v6, 0xa00000

    invoke-direct {v5, v4, v2, v6}, Lcom/google/android/apps/books/annotations/DiskBlobStore;-><init>(Ljava/io/File;Ljava/io/File;I)V

    return-object v5
.end method

.method private static makeCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$2;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    .locals 2
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "op"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;

    .prologue
    .line 981
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$17;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$17;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 987
    return-void
.end method

.method private onNewLayer(Lcom/google/android/apps/books/annotations/Layer;)V
    .locals 5
    .param p1, "layer"    # Lcom/google/android/apps/books/annotations/Layer;

    .prologue
    .line 696
    iget-object v2, p1, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getLocalLayer(Lcom/google/android/apps/books/annotations/Layer$Key;)Lcom/google/android/apps/books/annotations/Layer;

    move-result-object v1

    .line 697
    .local v1, "local":Lcom/google/android/apps/books/annotations/Layer;
    invoke-static {v1, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v0, 0x1

    .line 698
    .local v0, "different":Z
    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 699
    const-string v2, "AnnotationC"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 700
    const-string v2, "AnnotationC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Layer changed: old="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", new="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Layer;->isVolumeLayer()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 704
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v2, v1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->removeAnnotationsForLayer(Lcom/google/android/apps/books/annotations/Layer;)V

    .line 707
    :cond_1
    if-eqz v0, :cond_2

    .line 708
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLayerCache:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/apps/books/annotations/Layer;->key:Lcom/google/android/apps/books/annotations/Layer$Key;

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->updateLayer(Lcom/google/android/apps/books/annotations/Layer;)V

    .line 711
    :cond_2
    return-void

    .line 697
    .end local v0    # "different":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private syncOnBackgroundThread(Ljava/util/List;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 759
    .local p1, "volumeVersions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeVersion;>;"
    .local p2, "layers":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->uploadAllPendingOperations()V

    .line 760
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->downloadUpdatesForVolumes(Ljava/util/List;Ljava/util/Collection;)V

    .line 761
    return-void
.end method

.method private updateLocalAnnotationFromServer(Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;)V
    .locals 10
    .param p1, "aa"    # Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;
    .param p2, "receipt"    # Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;

    .prologue
    .line 820
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 821
    .local v0, "local":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v6, p2, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->annotationFromServer:Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .line 824
    .local v6, "fromServer":Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    invoke-static {v6}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->contextFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    move-result-object v1

    .line 826
    .local v1, "serverContext":Lcom/google/android/apps/books/annotations/AnnotationTextualContext;
    invoke-static {v6}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->positionRangeFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    .line 828
    .local v2, "serverTextRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-static {v6}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->imageRangeFromJson(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v3

    .line 830
    .local v3, "serverImageRange":Lcom/google/android/apps/books/render/PageStructureLocationRange;
    iget-object v4, p2, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    iget-wide v4, v4, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverTimestamp:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/Annotation;->updateFromServer(Lcom/google/android/apps/books/annotations/AnnotationTextualContext;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/render/PageStructureLocationRange;J)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v9

    .line 834
    .local v9, "updated":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v9, :cond_0

    .line 835
    new-instance v8, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    iget-object v4, p2, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    invoke-direct {v8, v9, v4}, Lcom/google/android/apps/books/annotations/ServerAnnotation;-><init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    .line 837
    .local v8, "serverAnnotation":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v7

    .line 840
    .local v7, "layerId":Ljava/lang/String;
    iget-object v4, p1, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {p0, v4, v7, v8}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyEditFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    .line 842
    .end local v7    # "layerId":Ljava/lang/String;
    .end local v8    # "serverAnnotation":Lcom/google/android/apps/books/annotations/ServerAnnotation;
    :cond_0
    return-void
.end method

.method private updateSyncedCopyAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V
    .locals 1
    .param p1, "volumeVersion"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "localId"    # Ljava/lang/String;
    .param p3, "characterQuota"    # Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .prologue
    .line 808
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$15;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    .line 816
    return-void
.end method

.method private uploadAllPendingOperations()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 765
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCallbacks:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;

    invoke-interface {v8}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$Callbacks;->isDeviceConnected()Z

    move-result v8

    if-nez v8, :cond_0

    .line 766
    new-instance v8, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;

    const-string v9, "Skipping sync: not connected"

    invoke-direct {v8, v9}, Lcom/google/android/apps/books/net/HttpHelper$OfflineIoException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 770
    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v8}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->unsyncedDeletedServerIds()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 771
    .local v5, "serverId":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->deleteAnnotation(Ljava/lang/String;)V

    .line 772
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v8, v5}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->expungeServerId(Ljava/lang/String;)V

    goto :goto_0

    .line 775
    .end local v5    # "serverId":Ljava/lang/String;
    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v8}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->unsyncedEdits()Ljava/util/List;

    move-result-object v7

    .line 776
    .local v7, "unsyncedEdits":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;

    .line 777
    .local v6, "unsyncedEdit":Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;
    iget-object v1, v6, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 778
    .local v1, "ea":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v3

    .line 779
    .local v3, "localId":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    iget-object v9, v6, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v10, v6, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;->serverId:Ljava/lang/String;

    invoke-virtual {v8, v9, v10, v1}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->editAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    move-result-object v4

    .line 781
    .local v4, "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v8, v3, v4}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->updateServerReceipt(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    goto :goto_1

    .line 784
    .end local v1    # "ea":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v3    # "localId":Ljava/lang/String;
    .end local v4    # "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    .end local v6    # "unsyncedEdit":Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedEdit;
    :cond_2
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    invoke-interface {v8}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->unsyncedAdditions()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;

    .line 785
    .local v0, "aa":Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mServerController:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    iget-object v9, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v10, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;

    move-result-object v4

    .line 787
    .local v4, "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;
    iget-object v8, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-direct {p0, v8}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->isCopyAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 790
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    iget-object v9, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v9}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->removeLocalAnnotation(Ljava/lang/String;)V

    .line 791
    iget-object v8, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->volumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v9, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v9}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v4, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    iget-object v10, v10, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->characterQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->updateSyncedCopyAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    goto :goto_2

    .line 795
    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    iget-object v9, v0, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v9}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v4, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;->receipt:Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    invoke-interface {v8, v9, v10}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->updateServerReceipt(Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    .line 797
    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->updateLocalAnnotationFromServer(Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;)V

    goto :goto_2

    .line 800
    .end local v0    # "aa":Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase$UnsyncedAnnotation;
    .end local v4    # "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;
    :cond_4
    return-void
.end method


# virtual methods
.method public deleteAll()V
    .locals 2

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$20;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$20;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 1055
    return-void
.end method

.method public fetchAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V
    .locals 8
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "key"    # Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .param p3, "imageW"    # I
    .param p4, "imageH"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "II",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 424
    .local p5, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getAnnotationCache(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    move-result-object v0

    invoke-virtual {v0, p2, p5}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->whenAnnotationDataLoaded(Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadingAnnotationDatas:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadingAnnotationDatas:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 427
    new-instance v4, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;

    invoke-direct {v4, p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$8;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    .line 442
    .local v4, "processor":Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;
    iget-object v7, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$9;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;II)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 460
    .end local v4    # "processor":Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;
    :cond_0
    return-void
.end method

.method public fetchCopyQuota(Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 2
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$12;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 543
    return-void
.end method

.method public fetchImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "cachePolicy"    # Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 641
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mImageController:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->fetchImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V

    .line 642
    return-void
.end method

.method public fetchLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)V
    .locals 7
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p2, "layers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mLoadedLayers:Lcom/google/common/collect/Multimap;

    invoke-interface {v4, p1}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    .line 308
    .local v2, "loaded":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 309
    .local v3, "toLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 310
    .local v1, "layer":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 311
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 315
    .end local v1    # "layer":Ljava/lang/String;
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 316
    const-string v4, "AnnotationC"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 317
    const-string v4, "AnnotationC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Loading layers "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in volume "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :cond_2
    invoke-interface {v2, v3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 320
    new-instance v4, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;

    invoke-direct {v4, p0, v3, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V

    .line 335
    :cond_3
    return-void
.end method

.method public fetchVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 384
    return-void
.end method

.method public getForegroundAnnotationEditor(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/UserChangesEditor;
    .locals 2
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getAnnotationCache(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    move-result-object v0

    .line 223
    .local v0, "cache":Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;
    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;Lcom/google/android/apps/books/annotations/VolumeVersion;)V

    return-object v1
.end method

.method public getRecentAnnotationsForLayer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;I)Ljava/util/List;
    .locals 1
    .param p1, "volumeVersion"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "layer"    # Ljava/lang/String;
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mCaches:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getRecentAnnotationsForLayer(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public needsDefaultImageDimensions()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 198
    iget v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mDefaultImageWidth:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mDefaultImageHeight:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeLocalVolumeAnnotationsForVolume(Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 471
    return-void
.end method

.method public setDefaultImageDimensions(II)V
    .locals 0
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mDefaultImageWidth:I

    .line 193
    iput p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mDefaultImageHeight:I

    .line 194
    return-void
.end method

.method public startServerSync(Ljava/util/List;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/Void;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "vvs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeVersion;>;"
    .local p2, "layers":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p3, "onComplete":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/Void;>;>;"
    const-string v0, "AnnotationC"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "AnnotationC"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting sync for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " volumes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$3;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 218
    return-void
.end method

.method public updateLayers(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 646
    .local p1, "layers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Layer;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 661
    return-void
.end method

.method public varargs weaklyAddListeners(Lcom/google/android/apps/books/annotations/VolumeVersion;[Lcom/google/android/apps/books/annotations/AnnotationListener;)V
    .locals 5
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "listeners"    # [Lcom/google/android/apps/books/annotations/AnnotationListener;

    .prologue
    .line 300
    move-object v0, p2

    .local v0, "arr$":[Lcom/google/android/apps/books/annotations/AnnotationListener;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 301
    .local v3, "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getAnnotationCache(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->weaklyAddListener(Lcom/google/android/apps/books/annotations/AnnotationListener;)V

    .line 300
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 303
    .end local v3    # "listener":Lcom/google/android/apps/books/annotations/AnnotationListener;
    :cond_0
    return-void
.end method
