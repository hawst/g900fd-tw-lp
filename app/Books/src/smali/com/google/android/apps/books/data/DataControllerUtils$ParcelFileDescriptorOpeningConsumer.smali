.class Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;
.super Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;
.source "DataControllerUtils.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ParcelFileDescriptorOpeningConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/data/InputStreamSource;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mGetCalled:Z

.field result:Lcom/google/android/apps/books/util/ExceptionOr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/os/ParcelFileDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;-><init>()V

    .line 227
    return-void
.end method


# virtual methods
.method public getResult()Landroid/os/ParcelFileDescriptor;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->mGetCalled:Z

    if-eqz v0, :cond_0

    .line 251
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "get() called more than once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->mGetCalled:Z

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->block()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->throwIfFailure()V

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 231
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 233
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/data/InputStreamSource;

    .line 234
    .local v1, "input":Lcom/google/android/apps/books/data/InputStreamSource;
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    .end local v1    # "input":Lcom/google/android/apps/books/data/InputStreamSource;
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->unblock()V

    .line 243
    return-void

    .line 234
    .restart local v1    # "input":Lcom/google/android/apps/books/data/InputStreamSource;
    :cond_0
    :try_start_1
    invoke-interface {v1}, Lcom/google/android/apps/books/data/InputStreamSource;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_0

    .line 236
    .end local v1    # "input":Lcom/google/android/apps/books/data/InputStreamSource;
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    goto :goto_1

    .line 240
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->result:Lcom/google/android/apps/books/util/ExceptionOr;

    goto :goto_1
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 220
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/DataControllerUtils$ParcelFileDescriptorOpeningConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
