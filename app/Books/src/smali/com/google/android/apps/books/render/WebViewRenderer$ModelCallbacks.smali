.class Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;
.super Ljava/lang/Object;
.source "WebViewRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModelCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRenderer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;)V
    .locals 0

    .prologue
    .line 844
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/render/WebViewRenderer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/WebViewRenderer$1;

    .prologue
    .line 844
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;)V

    return-void
.end method


# virtual methods
.method public canDrawPages()Z
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p2, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 911
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRenderer;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    .line 912
    return-void
.end method

.method public dispatchRenderError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/WebViewRenderer;->dispatchRenderError(Ljava/lang/Exception;)V

    .line 893
    return-void
.end method

.method public drawPage(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "pageBounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 855
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$500(Lcom/google/android/apps/books/render/WebViewRenderer;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;
    invoke-static {v5}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$900(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/PageMargins;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/books/render/PageMargins;->horizontal:I

    mul-int/lit8 v5, v5, 0x2

    sub-int v6, v0, v5

    .line 857
    .local v6, "pageWidth":I
    iget v8, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    .line 858
    .local v8, "translateX":I
    iget v9, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    .line 859
    .local v9, "translateY":I
    iget v0, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageBottom:I

    iget v5, p1, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageTop:I

    sub-int v7, v0, v5

    .line 861
    .local v7, "textHeight":I
    invoke-virtual {p3}, Landroid/graphics/Canvas;->save()I

    .line 862
    neg-int v0, v8

    iget-object v5, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;
    invoke-static {v5}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$900(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/PageMargins;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/books/render/PageMargins;->horizontal:I

    add-int/2addr v0, v5

    int-to-float v0, v0

    neg-int v5, v9

    iget-object v10, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mViewMargins:Lcom/google/android/apps/books/render/PageMargins;
    invoke-static {v10}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$900(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/PageMargins;

    move-result-object v10

    iget v10, v10, Lcom/google/android/apps/books/render/PageMargins;->vertical:I

    add-int/2addr v5, v10

    int-to-float v5, v5

    invoke-virtual {p3, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 864
    int-to-float v1, v8

    .line 865
    .local v1, "clipLeft":F
    int-to-float v2, v9

    .line 866
    .local v2, "clipTop":F
    add-int v0, v6, v8

    int-to-float v3, v0

    .line 867
    .local v3, "clipRight":F
    add-int v0, v7, v9

    int-to-float v4, v0

    .line 868
    .local v4, "clipBottom":F
    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 873
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mWebView:Lcom/google/android/apps/books/render/SnapshottingWebView;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$1000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/SnapshottingWebView;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/books/render/SnapshottingWebView;->snapShot(Landroid/graphics/Canvas;)V

    .line 874
    invoke-virtual {p3}, Landroid/graphics/Canvas;->restore()V

    .line 883
    return-void
.end method

.method public getPageSize(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$500(Lcom/google/android/apps/books/render/WebViewRenderer;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$ModelCallbacks;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mPageSize:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$500(Lcom/google/android/apps/books/render/WebViewRenderer;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 848
    return-object p2
.end method
