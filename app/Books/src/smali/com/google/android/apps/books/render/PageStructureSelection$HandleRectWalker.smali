.class Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;
.super Ljava/lang/Object;
.source "PageStructureSelection.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/PageStructureSelection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HandleRectWalker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<",
        "Landroid/graphics/Rect;",
        ">;"
    }
.end annotation


# instance fields
.field private mIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/PageStructureSelection;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/PageStructureSelection;)V
    .locals 1

    .prologue
    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->this$0:Lcom/google/android/apps/books/render/PageStructureSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->mIndex:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/PageStructureSelection;Lcom/google/android/apps/books/render/PageStructureSelection$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/PageStructureSelection;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/PageStructureSelection$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;-><init>(Lcom/google/android/apps/books/render/PageStructureSelection;)V

    return-void
.end method


# virtual methods
.method public next(Landroid/graphics/Rect;)Z
    .locals 3
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    const/4 v0, 0x1

    .line 28
    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->mIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->mIndex:I

    packed-switch v1, :pswitch_data_0

    .line 38
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 30
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->this$0:Lcom/google/android/apps/books/render/PageStructureSelection;

    # getter for: Lcom/google/android/apps/books/render/PageStructureSelection;->mFirst:Lcom/google/android/apps/books/render/PageStructureHandle;
    invoke-static {v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->access$000(Lcom/google/android/apps/books/render/PageStructureSelection;)Lcom/google/android/apps/books/render/PageStructureHandle;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->getRect(Landroid/graphics/Rect;)V

    .line 31
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 34
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->this$0:Lcom/google/android/apps/books/render/PageStructureSelection;

    # getter for: Lcom/google/android/apps/books/render/PageStructureSelection;->mLast:Lcom/google/android/apps/books/render/PageStructureHandle;
    invoke-static {v1}, Lcom/google/android/apps/books/render/PageStructureSelection;->access$100(Lcom/google/android/apps/books/render/PageStructureSelection;)Lcom/google/android/apps/books/render/PageStructureHandle;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->getRect(Landroid/graphics/Rect;)V

    .line 35
    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, p1, Landroid/graphics/Rect;->left:I

    goto :goto_0

    .line 28
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 18
    check-cast p1, Landroid/graphics/Rect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->next(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureSelection$HandleRectWalker;->mIndex:I

    .line 24
    return-void
.end method
