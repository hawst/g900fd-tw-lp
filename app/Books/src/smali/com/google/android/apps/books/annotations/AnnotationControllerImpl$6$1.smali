.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;

.field final synthetic val$listResult:Lcom/google/android/apps/books/util/ExceptionOr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;->val$listResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V
    .locals 3
    .param p1, "cache"    # Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->getLayerId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    iget v1, v1, Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;->segmentIndex:I

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$6$1;->val$listResult:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->addVolumeAnnotations(Ljava/lang/String;ILcom/google/android/apps/books/util/ExceptionOr;)V

    .line 354
    return-void
.end method
