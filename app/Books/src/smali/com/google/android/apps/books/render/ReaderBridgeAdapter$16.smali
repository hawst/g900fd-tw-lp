.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onLoadedRangeDataBulk(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$idToRects:Lcom/google/common/collect/Multimap;

.field final synthetic val$requestId:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;ILcom/google/common/collect/Multimap;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;->val$requestId:I

    iput-object p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;->val$idToRects:Lcom/google/common/collect/Multimap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;->val$requestId:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$16;->val$idToRects:Lcom/google/common/collect/Multimap;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/render/ReaderListener;->onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V

    .line 338
    return-void
.end method
