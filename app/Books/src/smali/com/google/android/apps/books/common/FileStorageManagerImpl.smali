.class public Lcom/google/android/apps/books/common/FileStorageManagerImpl;
.super Ljava/lang/Object;
.source "FileStorageManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/common/FileStorageManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLock:Ljava/util/concurrent/locks/ReadWriteLock;

.field private mSequenceNumber:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 42
    iput-object p1, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method


# virtual methods
.method public getLock()Ljava/util/concurrent/locks/Lock;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    return-object v0
.end method

.method public getSequenceNumber()I
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->getLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    .line 53
    .local v0, "lock":Ljava/util/concurrent/locks/Lock;
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 54
    iget v1, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mSequenceNumber:I

    .line 55
    .local v1, "result":I
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 56
    return v1
.end method

.method public resetFileStorageLocation()V
    .locals 2

    .prologue
    .line 94
    iget-object v1, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    .line 95
    .local v0, "lock":Ljava/util/concurrent/locks/Lock;
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 97
    :try_start_0
    iget v1, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mSequenceNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mSequenceNumber:I

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/books/common/FileStorageManagerImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksProvider;->resetFileStorageLocation(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 102
    return-void

    .line 100
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method
