.class Lcom/google/android/apps/books/data/UploadsControllerImpl$16$1;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Updater;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl$16;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Updater",
        "<",
        "Lcom/google/android/apps/books/upload/Upload$Uploads;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$16;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$16;)V
    .locals 0

    .prologue
    .line 509
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$16$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$16;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 2
    .param p1, "uploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 512
    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$16$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$16;

    iget-object v1, v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$16;->val$id:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/upload/Upload$Uploads;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/Upload;

    .line 513
    .local v0, "upload":Lcom/google/android/apps/books/upload/Upload;
    if-eqz v0, :cond_0

    .line 514
    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$16$1;->this$1:Lcom/google/android/apps/books/data/UploadsControllerImpl$16;

    iget-object v1, v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$16;->val$updater:Lcom/google/android/ublib/utils/Updater;

    invoke-interface {v1, v0}, Lcom/google/android/ublib/utils/Updater;->update(Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    :cond_0
    return-object p1
.end method

.method public bridge synthetic update(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 509
    check-cast p1, Lcom/google/android/apps/books/upload/Upload$Uploads;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$16$1;->update(Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    return-object v0
.end method
