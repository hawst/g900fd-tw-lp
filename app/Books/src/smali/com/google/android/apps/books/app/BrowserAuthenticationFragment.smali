.class public Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;
.super Lcom/google/android/apps/books/app/BooksFragment;
.source "BrowserAuthenticationFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$1;,
        Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;,
        Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksFragment;-><init>()V

    .line 62
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->getCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    return-object v0
.end method

.method private getCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getFragmentCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onStart()V
    .locals 5

    .prologue
    .line 52
    invoke-super {p0}, Lcom/google/android/apps/books/app/BooksFragment;->onStart()V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelper;

    new-instance v2, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$AuthHelperClient;-><init>(Lcom/google/android/apps/books/app/BrowserAuthenticationFragment;Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$1;)V

    # invokes: Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->getAccount(Landroid/os/Bundle;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->access$100(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->getUrl(Landroid/os/Bundle;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/app/BrowserAuthenticationFragment$Arguments;->access$200(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/util/BooksBrowserAuthenticationHelper;-><init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method
