.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SampleVolumeConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/api/data/SampleVolumes;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mInnerRequestId:I

.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;I)V
    .locals 0
    .param p2, "innerRequestId"    # I

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput p2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->mInnerRequestId:I

    .line 93
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;ILcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;I)V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/SampleVolumes;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "listExceptionOr":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/SampleVolumes;>;"
    const/4 v8, 0x5

    .line 97
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I
    invoke-static {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->mInnerRequestId:I

    if-eq v3, v4, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 102
    const-string v3, "OnboardSampleQuizPage"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 103
    const-string v3, "OnboardSampleQuizPage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "categoriesConsumer take failed:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v3, v3, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-virtual {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->handleNoConnection()V

    goto :goto_0

    .line 116
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/SampleVolumes;

    .line 117
    .local v2, "volumes":Lcom/google/android/apps/books/api/data/SampleVolumes;
    iget-object v3, v2, Lcom/google/android/apps/books/api/data/SampleVolumes;->volumes:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, v2, Lcom/google/android/apps/books/api/data/SampleVolumes;->volumes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v4, v2, Lcom/google/android/apps/books/api/data/SampleVolumes;->nextPageToken:Ljava/lang/String;

    # setter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mNextPageToken:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$502(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Ljava/lang/String;)Ljava/lang/String;

    .line 122
    iget-object v3, v2, Lcom/google/android/apps/books/api/data/SampleVolumes;->volumes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;

    .line 123
    .local v1, "volume":Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$600(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    const/16 v4, 0x64

    if-ge v3, v4, :cond_0

    .line 127
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;
    invoke-static {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$600(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 128
    const-string v3, "OnboardSampleQuizPage"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 129
    const-string v3, "OnboardSampleQuizPage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Requesting cover reqId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->mInnerRequestId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "volId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mNumSamples="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mSampleVolumeIds:Ljava/util/Set;
    invoke-static {v5}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$600(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # operator++ for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mOutstandingImageFetches:I
    invoke-static {v3}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$108(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v3, v3, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mController:Lcom/google/android/apps/books/app/OnboardingController;

    iget-object v4, v1, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->volumeInfo:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;

    iget-object v4, v4, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;->imageLinks:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;

    iget-object v4, v4, Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;->thumbnail:Ljava/lang/String;

    new-instance v5, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;

    iget-object v6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mRequestId:I
    invoke-static {v7}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)I

    move-result v7

    invoke-direct {v5, v6, v1, v7}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$CoverConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Lcom/google/android/apps/books/app/data/JsonRecommendedBook;I)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/books/app/OnboardingController;->getVolumeImage(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    goto/16 :goto_1

    .line 137
    :cond_6
    const-string v3, "OnboardSampleQuizPage"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 138
    const-string v3, "OnboardSampleQuizPage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Duplicate sample volume ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 86
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$SampleVolumeConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
