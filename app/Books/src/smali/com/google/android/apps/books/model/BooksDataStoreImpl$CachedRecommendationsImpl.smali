.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;
.super Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CachedRecommendationsImpl"
.end annotation


# instance fields
.field private final recommendationsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)V
    .locals 1

    .prologue
    .line 2107
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;-><init>()V

    .line 2109
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;->recommendationsList:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/model/BooksDataStoreImpl;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;

    .prologue
    .line 2107
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)V

    return-void
.end method


# virtual methods
.method public getRecommendedBooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2113
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;->recommendationsList:Ljava/util/List;

    return-object v0
.end method
