.class Lcom/google/android/apps/books/data/PageContentSubcontroller$9;
.super Ljava/lang/Object;
.source "PageContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageContentSubcontroller;->getKeyToDecryptLocalContent(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/Page;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$page:Lcom/google/android/apps/books/model/Page;

.field final synthetic val$services:Lcom/google/android/apps/books/data/ControlTaskServices;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/Page;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iput-object p4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p1, "key":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$100(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    .line 335
    :goto_0
    return-void

    .line 330
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/data/LocalBlob;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    invoke-interface {v1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, v2, Lcom/google/android/apps/books/sync/VolumeContentId;->volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$page:Lcom/google/android/apps/books/model/Page;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getPageImageFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/data/LocalBlob;-><init>(Lcom/google/android/apps/books/data/InputStreamSource;)V

    .line 332
    .local v0, "blob":Lcom/google/android/apps/books/data/DataControllerBlob;
    iget-object v2, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v4, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    new-instance v5, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/LocalSessionKey;

    iget-object v6, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->this$0:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    iget-object v7, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$services:Lcom/google/android/apps/books/data/ControlTaskServices;

    iget-object v8, p0, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->localCcBoxSupplier(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/common/base/Supplier;
    invoke-static {v6, v7, v8}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$900(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;)Lcom/google/common/base/Supplier;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v5, v0, v1, v6, v7}, Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/common/base/Supplier;Lcom/google/android/apps/books/data/PageContentSubcontroller$1;)V

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/data/PageContentSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V
    invoke-static {v2, v3, v4, v5, v1}, Lcom/google/android/apps/books/data/PageContentSubcontroller;->access$800(Lcom/google/android/apps/books/data/PageContentSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageContentSubcontroller$Result;Z)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 322
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/PageContentSubcontroller$9;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
