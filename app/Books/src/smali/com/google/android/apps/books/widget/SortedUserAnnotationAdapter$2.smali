.class Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;
.super Ljava/lang/Object;
.source "SortedUserAnnotationAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getOnItemClickListener(Lcom/google/android/apps/books/app/ContentsView$Callbacks;)Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

.field final synthetic val$callbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;->this$0:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;->val$callbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;->this$0:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getAnnotationAtViewPosition(I)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 247
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_1

    .line 248
    invoke-static {v0, p3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTocAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;I)V

    .line 249
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter$2;->val$callbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/ContentsView$Callbacks;->onAnnotationClick(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    const-string v1, "AnnotationAdapter"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    const-string v1, "AnnotationAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ignoring tap on non-existent annotation at position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
