.class public Lcom/google/android/apps/books/data/BackgroundBooksDataController;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/BooksDataController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/BackgroundBooksDataController$54;,
        Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;,
        Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;,
        Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;

.field private final mCoverSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

.field private mCurrentlyOpeningVolumeId:Ljava/lang/String;

.field private mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;

.field private final mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

.field private final mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

.field private final mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

.field private mLastSearch:Ljava/lang/String;

.field private final mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

.field private final mManifestConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field

.field private final mMyEbooksSubcontroller:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

.field private final mNetworkExecutor:Ljava/util/concurrent/Executor;

.field private final mNetworkTasks:Lcom/google/android/apps/books/data/NetworkTaskQueue;

.field private final mOffersSubcontroller:Lcom/google/android/apps/books/data/OffersSubcontroller;

.field private mOnDictionaryKeyError:Ljava/lang/Runnable;

.field private final mPageContentSubcontroller:Lcom/google/android/apps/books/data/PageContentSubcontroller;

.field private final mPageStructureSubcontroller:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

.field private final mPendingActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;",
            ">;"
        }
    .end annotation
.end field

.field private final mResourceContentSubcontroller:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field private final mSegmentContentSubcontroller:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

.field private final mServer:Lcom/google/android/apps/books/net/BooksServer;

.field private final mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

.field private final mSharedResourceSubcontroller:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

.field private final mThumbnailSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

.field private final mVolumeDataConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeIdToManifest:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;IILcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;Lcom/google/android/apps/books/net/BooksServer;Landroid/accounts/Account;Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/app/DeviceInfo;Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/DataControllerStore;)V
    .locals 11
    .param p1, "backgroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "maxConcurrentBackgroundNetworkTasks"    # I
    .param p4, "maxConcurrentNetworkTasks"    # I
    .param p5, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p6, "annotationController"    # Lcom/google/android/apps/books/annotations/AnnotationController;
    .param p7, "broadcaster"    # Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;
    .param p8, "server"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p9, "account"    # Landroid/accounts/Account;
    .param p10, "keyValidator"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;
    .param p11, "encryptionScheme"    # Lcom/google/android/apps/books/data/EncryptionScheme;
    .param p12, "config"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;
    .param p13, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p14, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;
    .param p15, "localDictionarySubController"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p16, "dataControllerStore"    # Lcom/google/android/apps/books/model/DataControllerStore;

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPendingActions:Ljava/util/List;

    .line 386
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 388
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 496
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 498
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 506
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;

    .line 1319
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$46;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$46;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOnDictionaryKeyError:Ljava/lang/Runnable;

    .line 178
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    .line 179
    const-string v2, "missing background executor"

    invoke-static {p1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 181
    const-string v2, "missing network executor"

    invoke-static {p2, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkExecutor:Ljava/util/concurrent/Executor;

    .line 182
    const-string v2, "missing data store"

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/BooksDataStore;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 183
    const-string v2, "missing annotation controller"

    move-object/from16 v0, p6

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/AnnotationController;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    .line 185
    const-string v2, "missing broadcaster"

    move-object/from16 v0, p7

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;

    .line 186
    const-string v2, "missing server"

    move-object/from16 v0, p8

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/net/BooksServer;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mServer:Lcom/google/android/apps/books/net/BooksServer;

    .line 187
    const-string v2, "missing account"

    move-object/from16 v0, p9

    invoke-static {v0, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAccount:Landroid/accounts/Account;

    .line 188
    new-instance v2, Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    move-object/from16 v0, p10

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    .line 189
    new-instance v2, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    invoke-static {}, Lcom/google/android/apps/books/data/ResourcePolicies;->get()Lcom/google/android/apps/books/data/ResourcePolicy;

    move-result-object v3

    move-object/from16 v0, p11

    move-object/from16 v1, p9

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;-><init>(Lcom/google/android/apps/books/data/EncryptionScheme;Lcom/google/android/apps/books/data/ResourcePolicy;Landroid/accounts/Account;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mResourceContentSubcontroller:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    .line 191
    new-instance v2, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    move-object/from16 v0, p11

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller;-><init>(Lcom/google/android/apps/books/data/EncryptionScheme;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSegmentContentSubcontroller:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    .line 192
    new-instance v2, Lcom/google/android/apps/books/data/PageContentSubcontroller;

    move-object/from16 v0, p11

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/data/PageContentSubcontroller;-><init>(Lcom/google/android/apps/books/data/EncryptionScheme;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPageContentSubcontroller:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    .line 193
    new-instance v2, Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    move-object/from16 v0, p11

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;-><init>(Lcom/google/android/apps/books/data/EncryptionScheme;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPageStructureSubcontroller:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    .line 194
    new-instance v2, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    invoke-direct {v2}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSharedResourceSubcontroller:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    .line 196
    new-instance v2, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAccount:Landroid/accounts/Account;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->syncState:Lcom/google/android/apps/books/sync/SyncAccountsState;
    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->access$000(Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;)Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/sync/SyncAccountsState;Lcom/google/android/apps/books/annotations/AnnotationController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mMyEbooksSubcontroller:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    .line 199
    new-instance v2, Lcom/google/android/apps/books/data/VolumeCoverSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->coverHeight:I
    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->access$100(Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;)I

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/data/VolumeCoverSubcontroller;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCoverSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    .line 200
    new-instance v2, Lcom/google/android/apps/books/data/VolumeThumbnailSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->thumbnailHeight:I
    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;->access$200(Lcom/google/android/apps/books/data/BackgroundBooksDataController$DataControllerConfig;)I

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/data/VolumeThumbnailSubcontroller;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mThumbnailSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    .line 203
    new-instance v2, Lcom/google/android/apps/books/data/OffersSubcontroller;

    move-object/from16 v0, p14

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/data/OffersSubcontroller;-><init>(Lcom/google/android/apps/books/app/DeviceInfo;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOffersSubcontroller:Lcom/google/android/apps/books/data/OffersSubcontroller;

    .line 205
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    .line 207
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mServer:Lcom/google/android/apps/books/net/BooksServer;

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkExecutor:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v9, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    iget-object v10, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    move-object v3, p0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$MyDispatcher;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/net/BooksServer;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/DataControllerStore;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    .line 210
    new-instance v2, Lcom/google/android/apps/books/data/NetworkTaskQueue;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    move v7, p4

    move-object/from16 v8, p13

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/data/NetworkTaskQueue;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/data/NetworkTaskServices;IILcom/google/android/apps/books/util/Logger;)V

    iput-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkTasks:Lcom/google/android/apps/books/data/NetworkTaskQueue;

    .line 213
    invoke-direct {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->initializeDataStore()V

    .line 214
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;
    .param p4, "x4"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeData(Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeDataOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->saveVolumeDataOnMainThread(Lcom/google/android/apps/books/model/VolumeData;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeDataExceptionOnMainThread(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeDataException(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/Set;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Lcom/google/android/ublib/utils/Consumer;
    .param p5, "x5"    # Lcom/google/android/ublib/utils/Consumer;
    .param p6, "x6"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 83
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeManifestInternal(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->verifyManifest(Lcom/google/android/apps/books/model/VolumeManifest;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/model/VolumeManifest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 83
    invoke-static {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->hasSharedResources(Lcom/google/android/apps/books/model/VolumeManifest;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifestOnMainThread(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishManifestExceptionOnMainThread(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishManifestException(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSharedResourceSubcontroller:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/OffersSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOffersSubcontroller:Lcom/google/android/apps/books/data/OffersSubcontroller;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSegmentContentSubcontroller:Lcom/google/android/apps/books/data/SegmentContentSubcontroller;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mResourceContentSubcontroller:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getResource(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/PageContentSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPageContentSubcontroller:Lcom/google/android/apps/books/data/PageContentSubcontroller;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/PageStructureSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPageStructureSubcontroller:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCoverSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mThumbnailSubcontroller:Lcom/google/android/apps/books/data/BaseVolumeCoverSubcontroller;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->flushQueuedActions()V

    return-void
.end method

.method static synthetic access$3800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getSessionKeyToUploadCollectionChanges(JLcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p4, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->uploadCollectionChanges(JLcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$4100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x3"    # Ljava/util/Collection;
    .param p4, "x4"    # J
    .param p6, "x5"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->uploadCollectionChanges(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/util/Collection;
    .param p4, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->handleUploadCollectionChangesResult(JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getSessionKeyToGetVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$4400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p4, "x4"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$4500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onDictionaryMetadataInvalidated()V

    return-void
.end method

.method static synthetic access$4700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPendingActions:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mMyEbooksSubcontroller:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOnDictionaryKeyError:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->releaseVolumeLicense(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/DataControllerStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->fetchDictionaryMetadata(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$5200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->handleNewDictionaries(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$5300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getLanguageCodes(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/annotations/AnnotationController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/SessionKey;

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->releaseVolumeLicenseOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/NetworkTaskQueue;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mNetworkTasks:Lcom/google/android/apps/books/data/NetworkTaskQueue;

    return-object v0
.end method

.method private clearFocusedVolume()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 994
    iput-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;

    .line 995
    iput-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeIdKey:Ljava/lang/Object;

    .line 996
    return-void
.end method

.method private deliverExceptionOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Ljava/lang/Exception;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1086
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<TT;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;

    invoke-direct {v0, p0, p3, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 1092
    return-void
.end method

.method private fetchDictionaryMetadata(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1408
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p3, "finishConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 1432
    return-void
.end method

.method private flushQueuedActions()V
    .locals 4

    .prologue
    .line 983
    invoke-direct {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->clearFocusedVolume()V

    .line 986
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPendingActions:Ljava/util/List;

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 987
    .local v1, "pendingActionsCopy":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPendingActions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 988
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/data/ControlTask;

    .line 989
    .local v2, "task":Lcom/google/android/apps/books/data/ControlTask;
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/data/ControlTask;->run(Lcom/google/android/apps/books/data/ControlTaskServices;)V

    goto :goto_0

    .line 991
    .end local v2    # "task":Lcom/google/android/apps/books/data/ControlTask;
    :cond_0
    return-void
.end method

.method private getLanguageCodes(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1487
    .local p1, "metadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1488
    .local v1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 1489
    .local v2, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v2}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1491
    .end local v2    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_0
    return-object v1
.end method

.method private getListeners()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->copyListeners()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getManifestOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 582
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onNetworkThread(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 623
    return-void
.end method

.method private getResource(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 864
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mPendingActions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;

    .line 865
    .local v0, "action":Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    invoke-interface {v0, v3, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;->getResource(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v2

    .line 866
    .local v2, "resource":Lcom/google/android/apps/books/model/Resource;
    if-eqz v2, :cond_0

    .line 870
    .end local v0    # "action":Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;
    .end local v2    # "resource":Lcom/google/android/apps/books/model/Resource;
    :goto_0
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v3, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStore;->getResource(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v2

    goto :goto_0
.end method

.method private getSessionKeyToGetVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "licenseType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1108
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->getValidAccountSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1121
    return-void
.end method

.method private getSessionKeyToUploadCollectionChanges(JLcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1011
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$30;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->getValidAccountSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1022
    return-void
.end method

.method private getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "licenseType"    # Ljava/lang/String;
    .param p3, "sessionKey"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/SessionKey;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1127
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;>;"
    iget-object v8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$36;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 1149
    return-void
.end method

.method private getVolumeDataOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 427
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;

    invoke-direct {v0, p0, p2, p1, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$9;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onNetworkThread(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 440
    return-void
.end method

.method private getVolumeManifestInternal(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 24
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "fromServer"    # Z
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 527
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mCurrentlyOpeningVolumeId:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 533
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v4, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$13;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-virtual {v12, v4}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 579
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    move-object/from16 v20, p4

    .line 544
    .local v20, "remainingConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    if-eqz v20, :cond_2

    .line 545
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeIdToManifest:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/apps/books/model/VolumeManifest;

    .line 546
    .local v22, "pendingSaveManifest":Lcom/google/android/apps/books/model/VolumeManifest;
    if-eqz v22, :cond_2

    .line 547
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    move-object/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishServerManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V

    .line 548
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    goto :goto_0

    .line 553
    .end local v22    # "pendingSaveManifest":Lcom/google/android/apps/books/model/VolumeManifest;
    :cond_2
    if-nez p3, :cond_3

    .line 555
    :try_start_0
    invoke-static {}, Lcom/google/api/client/util/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v7

    .line 556
    .local v7, "localSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/api/client/util/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v8

    .line 557
    .local v8, "localResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/api/client/util/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v9

    .line 558
    .local v9, "localPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/api/client/util/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v10

    .line 559
    .local v10, "localStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-interface/range {v4 .. v10}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolumeManifest(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v14

    .line 562
    .local v14, "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    const/16 v19, 0x0

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object v15, v7

    move-object/from16 v16, v8

    move-object/from16 v17, v9

    move-object/from16 v18, v10

    move-object/from16 v21, p5

    invoke-direct/range {v11 .. v21}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeManifest(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 572
    .end local v7    # "localSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8    # "localResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9    # "localPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v10    # "localStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v14    # "manifest":Lcom/google/android/apps/books/model/VolumeManifest;
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-object/from16 v2, p5

    invoke-static {v0, v4, v5, v1, v2}, Lcom/google/android/apps/books/data/ConsumerMaps;->addConsumers(Ljava/lang/Object;Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v23

    .line 574
    .local v23, "startNewTask":Z
    if-eqz v23, :cond_0

    .line 578
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getManifestOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_0

    .line 567
    .end local v23    # "startNewTask":Z
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method private handleNewDictionaries(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1440
    .local p2, "newDictionaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p3, "finishConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getServerDictionaryMetadataList()Ljava/util/List;

    move-result-object v0

    .line 1441
    .local v0, "oldMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->revokeUnsupportedLanguages(Ljava/util/List;Ljava/util/List;)V

    .line 1442
    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->insertNewDictionaryMetadata(Ljava/util/List;Ljava/util/List;)V

    .line 1443
    sget-object v1, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p3, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1444
    return-void
.end method

.method private handleUploadCollectionChangesResult(JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1080
    .local p3, "removedVolumeIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/model/BooksDataStore;->purgeDeletedCollectionVolumes(JLjava/util/Collection;)V

    .line 1081
    sget-object v0, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p4, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1082
    return-void
.end method

.method private static hasSharedResources(Lcom/google/android/apps/books/model/VolumeManifest;)Z
    .locals 3
    .param p0, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 674
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IdentifiableCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Resource;

    .line 675
    .local v1, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 676
    const/4 v2, 0x1

    .line 679
    .end local v1    # "resource":Lcom/google/android/apps/books/model/Resource;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private initializeDataStore()V
    .locals 1

    .prologue
    .line 218
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$1;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 225
    return-void
.end method

.method private insertNewDictionaryMetadata(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1464
    .local p1, "newMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p2, "oldMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1465
    .local v2, "oldMetadataSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 1466
    .local v1, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1467
    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/books/data/LocalDictionarySubController;->insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    goto :goto_0

    .line 1470
    .end local v1    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_1
    return-void
.end method

.method private static logIfImageModeInconsistent(Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 4
    .param p0, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 663
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 664
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Page;

    .line 665
    .local v1, "page":Lcom/google/android/apps/books/model/Page;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 671
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "page":Lcom/google/android/apps/books/model/Page;
    :cond_1
    :goto_0
    return-void

    .line 669
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    const-string v2, "BgDataController"

    const-string v3, "Image mode allowed, but no pages allowed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static logIfTextModeInconsistent(Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 4
    .param p0, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 649
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 650
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Segment;

    .line 651
    .local v1, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 657
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_1
    :goto_0
    return-void

    .line 655
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    const-string v2, "BgDataController"

    const-string v3, "Text mode allowed, but no segments allowed"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onDictionaryMetadataInvalidated()V
    .locals 1

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->clearLastDictionaryMetadataSyncTime()V

    .line 1330
    return-void
.end method

.method private onMainBackgroundThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 313
    return-void
.end method

.method private onNetworkThread(Lcom/google/android/apps/books/data/NetworkTask;)V
    .locals 1
    .param p1, "task"    # Lcom/google/android/apps/books/data/NetworkTask;

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 317
    return-void
.end method

.method private publishManifestException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 778
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 779
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 780
    return-void
.end method

.method private publishManifestExceptionOnMainThread(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 769
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$16;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$16;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 775
    return-void
.end method

.method private publishServerManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 11
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p3, "extraConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    const/4 v2, 0x0

    .line 728
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v4

    .line 729
    .local v4, "localSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v5

    .line 730
    .local v5, "localResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v6

    .line 731
    .local v6, "localPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v7

    .line 732
    .local v7, "localStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v9, p3

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->publishVolumeManifest(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z

    .line 734
    return-void
.end method

.method private publishServerManifestOnMainThread(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    .line 684
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$15;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 722
    return-void
.end method

.method private publishVolumeData(Lcom/google/android/apps/books/model/VolumeData;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 5
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "saved"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 467
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeData;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    if-eqz p1, :cond_2

    .line 468
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 469
    if-eqz p2, :cond_0

    .line 470
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v2, v3, v4, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 473
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getListeners()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 474
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeData(Lcom/google/android/apps/books/model/VolumeData;)V

    goto :goto_0

    .line 476
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_1
    const/4 v2, 0x1

    .line 478
    .end local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private publishVolumeDataException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mVolumeDataSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 494
    return-void
.end method

.method private publishVolumeDataExceptionOnMainThread(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 483
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$11;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$11;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 489
    return-void
.end method

.method private publishVolumeManifest(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)Z
    .locals 11
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p8, "fromServer"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 749
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "localSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p5, "localResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p6, "localPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p7, "localStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p9, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    .local p10, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    if-eqz p3, :cond_2

    .line 750
    new-instance v1, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;-><init>(Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Z)V

    .line 753
    .local v1, "response":Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v10

    .line 754
    .local v10, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-object/from16 v0, p9

    invoke-virtual {v2, p1, v10, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 755
    if-nez p8, :cond_0

    .line 756
    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mManifestSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    sget-object v3, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    move-object/from16 v0, p10

    invoke-virtual {v2, p1, v3, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 759
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getListeners()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 760
    .local v9, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v9, p1, p2, v1}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeManifest(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;)V

    goto :goto_0

    .line 762
    .end local v9    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_1
    const/4 v2, 0x1

    .line 764
    .end local v1    # "response":Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;"
    :goto_1
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private releaseVolumeLicense(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$5;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$5;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->getValidAccountSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;)V

    .line 300
    return-void
.end method

.method private releaseVolumeLicenseOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "key"    # Lcom/google/android/apps/books/model/SessionKey;

    .prologue
    .line 303
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onNetworkThread(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 309
    return-void
.end method

.method private revokeUnsupportedLanguages(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1477
    .local p1, "newMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local p2, "oldMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getLanguageCodes(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1478
    .local v3, "newLanguages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .line 1479
    .local v2, "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    invoke-virtual {v2}, Lcom/google/android/apps/books/dictionary/DictionaryMetadata;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    .line 1480
    .local v1, "languageCode":Ljava/lang/String;
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1481
    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v4, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->revokeDictionaryLanguage(Ljava/lang/String;)V

    goto :goto_0

    .line 1484
    .end local v1    # "languageCode":Ljava/lang/String;
    .end local v2    # "metadata":Lcom/google/android/apps/books/dictionary/DictionaryMetadata;
    :cond_1
    return-void
.end method

.method private saveVolumeDataOnMainThread(Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 1
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 443
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$10;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 454
    return-void
.end method

.method private uploadCollectionChanges(JLcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 11
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1027
    .local p3, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStore;->getCollectionChanges(J)Ljava/util/Collection;

    move-result-object v5

    .line 1029
    .local v5, "ops":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;>;"
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1030
    sget-object v0, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {p4, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1039
    :goto_0
    return-void

    .line 1033
    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v4, p3

    move-wide v6, p1

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$31;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v9, v0}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    goto :goto_0
.end method

.method private uploadCollectionChanges(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V
    .locals 12
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p4, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;",
            ">;J",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1046
    .local p2, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p3, "ops":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;>;"
    .local p6, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v11

    .line 1047
    .local v11, "server":Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 1048
    .local v6, "removedVolumeIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;

    .line 1049
    .local v10, "op":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;
    sget-object v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$54;->$SwitchMap$com$google$android$apps$books$model$BooksDataStore$CollectionChangeType:[I

    iget-object v3, v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->type:Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;

    invoke-virtual {v3}, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1053
    :pswitch_0
    iget-object v2, v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->volumeId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v11, v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->addVolumeToMyEbooks(Ljava/lang/String;Lcom/google/android/apps/books/api/OceanApiaryUrls$AddVolumeReason;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1072
    .end local v6    # "removedVolumeIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "op":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;
    .end local v11    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :catch_0
    move-exception v8

    .line 1073
    .local v8, "e":Ljava/io/IOException;
    move-object/from16 v0, p6

    invoke-direct {p0, p1, v8, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->deliverExceptionOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1075
    .end local v8    # "e":Ljava/io/IOException;
    :goto_1
    return-void

    .line 1059
    .restart local v6    # "removedVolumeIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "op":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;
    .restart local v11    # "server":Lcom/google/android/apps/books/net/BooksServer;
    :pswitch_1
    :try_start_1
    iget-object v2, v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->volumeId:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    invoke-interface {v11, v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->releaseOfflineLicense(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V

    .line 1061
    iget-object v2, v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->volumeId:Ljava/lang/String;

    invoke-interface {v11, v2}, Lcom/google/android/apps/books/net/BooksServer;->removeVolumeFromMyEbooks(Ljava/lang/String;)V

    .line 1062
    iget-object v2, v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;->volumeId:Ljava/lang/String;

    invoke-interface {v6, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1066
    .end local v10    # "op":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;
    :cond_0
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;

    move-object v3, p0

    move-wide/from16 v4, p4

    move-object/from16 v7, p6

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {p1, v2}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1049
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static verifyManifest(Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 2
    .param p0, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 629
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getContentVersion()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 636
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Missing content version in manifest"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 639
    :cond_0
    const-string v0, "BgDataController"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 640
    invoke-static {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->logIfTextModeInconsistent(Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 641
    invoke-static {p0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->logIfImageModeInconsistent(Lcom/google/android/apps/books/model/VolumeManifest;)V

    .line 643
    :cond_1
    return-void
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 808
    .local p2, "volumeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "onResult":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$19;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 814
    return-void
.end method

.method public addDismissedRecommendation(Ljava/lang/String;)V
    .locals 2
    .param p1, "dismissedRecVolumeId"    # Ljava/lang/String;

    .prologue
    .line 375
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dismissOffer(Ljava/lang/String;)V
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 797
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$18;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$18;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 803
    return-void
.end method

.method public finishedOpeningBook(Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 968
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$28;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$28;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 979
    return-void
.end method

.method public getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p1, "fromServer"    # Z
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1310
    .local p2, "myEbooksConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$45;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1317
    return-void
.end method

.method public getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 6
    .param p1, "localOnly"    # Z
    .param p2, "reloadLibrary"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 786
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 793
    return-void
.end method

.method public getPageContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 892
    .local p3, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "ccBoxConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/CcBox;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$23;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$23;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 899
    return-void
.end method

.method public getPageStructure(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 905
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$24;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$24;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 912
    return-void
.end method

.method public getRequestedDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1349
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$48;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1355
    return-void
.end method

.method public getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p7, "onlyIfLocal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 837
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$21;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$21;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 844
    return-void
.end method

.method public getResourceContent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p7, "onlyIfLocal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 852
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 860
    return-void
.end method

.method public getSegmentContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p6, "ignoreResources"    # Z
    .param p7, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Segment;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;Z",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 822
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    .local p4, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$20;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 829
    return-void
.end method

.method public getServerDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1359
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$49;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$49;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1365
    return-void
.end method

.method public getSharedResourceContent(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "resId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/BooksDataStore;->getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "licenseType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1097
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$34;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$34;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1103
    return-void
.end method

.method public getVolumeCover(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 918
    .local p2, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$25;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 925
    return-void
.end method

.method public getVolumeData(Ljava/lang/String;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fromServer"    # Z
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 397
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeData;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$8;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;ZLjava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 424
    return-void
.end method

.method public getVolumeManifest(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "fromServer"    # Z
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 514
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$12;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$12;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 521
    return-void
.end method

.method public getVolumeSearches(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1283
    .local p2, "searchesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Ljava/lang/String;>;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$44;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1305
    return-void
.end method

.method public getVolumeThumbnail(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 931
    .local p2, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$26;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$26;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 938
    return-void
.end method

.method public loadDismissedRecommendations()V
    .locals 2

    .prologue
    .line 370
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public loadDownloadProgress(Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 365
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public loadedLocalVolumeData(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public loadedVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public lookup(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "languageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1335
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;>;"
    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1336
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$47;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1344
    return-void
.end method

.method public removeListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 238
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$3;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/BooksDataListener;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 244
    return-void
.end method

.method public setFitWidth(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fitWidth"    # Z

    .prologue
    .line 1222
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$40;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$40;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1228
    return-void
.end method

.method public setForceDownload(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "forceDownload"    # Z

    .prologue
    .line 360
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setHasOfflineLicense(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "hasOfflineLicense"    # Z

    .prologue
    .line 355
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLastVolumeSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lastSearch"    # Ljava/lang/String;

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLastSearch:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLastSearch:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1262
    :goto_0
    return-void

    .line 1247
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mLastSearch:Ljava/lang/String;

    .line 1248
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$42;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$42;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .prologue
    .line 248
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 285
    return-void
.end method

.method public setLineHeight(Ljava/lang/String;F)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lineHeight"    # F

    .prologue
    .line 1202
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$38;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$38;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;F)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1208
    return-void
.end method

.method public setPinned(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z

    .prologue
    .line 331
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$7;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 340
    return-void
.end method

.method public setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z
    .param p3, "hasOfflineLicense"    # Z

    .prologue
    .line 350
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "lastAccess"    # J
    .param p5, "lastAction"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .param p6, "notifyContentChanged"    # Z

    .prologue
    .line 1191
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$37;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1198
    return-void
.end method

.method public setRecommendations(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 380
    .local p1, "newRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRequestedDictionaryLanguages(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1369
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$50;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 1376
    return-void
.end method

.method public setTextZoom(Ljava/lang/String;F)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "textZoom"    # F

    .prologue
    .line 1212
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$39;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$39;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;F)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1218
    return-void
.end method

.method public setUserSelectedMode(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 344
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startedOpeningBook(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 942
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$27;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 964
    return-void
.end method

.method public syncDictionaryMetadata(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1382
    .local p1, "finishConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$51;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 1400
    return-void
.end method

.method public syncRequestedDictionaries(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1448
    .local p1, "finishConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$53;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 1457
    return-void
.end method

.method public updateLastLocalAccess(Ljava/lang/String;JZ)V
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lastLocalAccess"    # J
    .param p4, "notifyContentChanged"    # Z

    .prologue
    .line 1233
    new-instance v1, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$41;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;JZ)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1239
    return-void
.end method

.method public uploadCollectionChanges(JLcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1001
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$29;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$29;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLcom/google/android/ublib/utils/Consumer;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 1007
    return-void
.end method

.method public weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 228
    new-instance v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$2;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/model/BooksDataListener;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->onMainBackgroundThread(Ljava/lang/Runnable;)V

    .line 234
    return-void
.end method
