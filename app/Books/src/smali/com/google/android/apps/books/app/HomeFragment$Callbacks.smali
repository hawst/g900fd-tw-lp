.class public interface abstract Lcom/google/android/apps/books/app/HomeFragment$Callbacks;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract consumeTransitionView()Landroid/view/View;
.end method

.method public abstract getLibrarySortOrderFromPrefs()Lcom/google/android/apps/books/app/LibraryComparator;
.end method

.method public abstract onSelectedLibrarySortOrder(Lcom/google/android/apps/books/app/LibraryComparator;)V
.end method

.method public abstract setTransitionView(Landroid/view/View;)V
.end method

.method public abstract showLibrarySortMenu(Lcom/google/android/apps/books/app/LibraryComparator;)V
.end method

.method public abstract startUnpinProcess(Ljava/lang/String;)V
.end method
