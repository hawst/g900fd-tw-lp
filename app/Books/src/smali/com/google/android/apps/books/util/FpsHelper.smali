.class public Lcom/google/android/apps/books/util/FpsHelper;
.super Ljava/lang/Object;
.source "FpsHelper.java"


# instance fields
.field private mDisplayString:Ljava/lang/String;

.field private mFPS:F

.field private mFrameCount:I

.field private mFrameStart:J

.field private mInitFinished:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRenderTimeHistory:[J

.field private mTimeStart:J


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "textColor"    # I

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mPaint:Landroid/graphics/Paint;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mDisplayString:Ljava/lang/String;

    .line 23
    iput-wide v2, p0, Lcom/google/android/apps/books/util/FpsHelper;->mTimeStart:J

    .line 25
    iput-wide v2, p0, Lcom/google/android/apps/books/util/FpsHelper;->mFrameStart:J

    .line 27
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/FpsHelper;->mInitFinished:Z

    .line 31
    iput v1, p0, Lcom/google/android/apps/books/util/FpsHelper;->mFrameCount:I

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mFPS:F

    .line 35
    const/16 v0, 0x32

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mRenderTimeHistory:[J

    .line 39
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mTimeStart:J

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/util/FpsHelper;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 43
    return-void
.end method
