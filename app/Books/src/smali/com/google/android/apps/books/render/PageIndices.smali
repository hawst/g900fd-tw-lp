.class public Lcom/google/android/apps/books/render/PageIndices;
.super Ljava/lang/Object;
.source "PageIndices.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/render/PageIndices;",
        ">;"
    }
.end annotation


# instance fields
.field public final pageIndex:I

.field public final passageIndex:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    .line 37
    iput p2, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    .line 38
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/render/PageIndices;)I
    .locals 5
    .param p1, "other"    # Lcom/google/android/apps/books/render/PageIndices;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 42
    if-eqz p1, :cond_4

    .line 43
    iget v3, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v4, p1, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    if-ge v3, v4, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    iget v3, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v4, p1, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    if-le v3, v4, :cond_2

    move v0, v1

    .line 46
    goto :goto_0

    .line 47
    :cond_2
    iget v3, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    iget v4, p1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-lt v3, v4, :cond_0

    .line 49
    iget v0, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    iget v3, p1, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-le v0, v3, :cond_3

    move v0, v1

    .line 50
    goto :goto_0

    :cond_3
    move v0, v2

    .line 52
    goto :goto_0

    :cond_4
    move v0, v2

    .line 55
    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/apps/books/render/PageIndices;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PageIndices;->compareTo(Lcom/google/android/apps/books/render/PageIndices;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    if-ne p0, p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v1

    .line 80
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 81
    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 83
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 84
    check-cast v0, Lcom/google/android/apps/books/render/PageIndices;

    .line 85
    .local v0, "other":Lcom/google/android/apps/books/render/PageIndices;
    iget v3, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    iget v4, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 86
    goto :goto_0

    .line 87
    :cond_4
    iget v3, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v4, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 88
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 68
    const/16 v0, 0x1f

    .line 69
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 70
    .local v1, "result":I
    iget v2, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    add-int/lit8 v1, v2, 0x1f

    .line 71
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    add-int v1, v2, v3

    .line 72
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "passage"

    iget v2, p0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "page"

    iget v2, p0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
