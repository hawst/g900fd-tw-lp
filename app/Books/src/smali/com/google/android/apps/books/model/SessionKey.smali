.class public Lcom/google/android/apps/books/model/SessionKey;
.super Ljava/lang/Object;
.source "SessionKey.java"


# instance fields
.field public final encryptedKey:[B

.field public final rootKeyVersion:I

.field public final version:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;[B)V
    .locals 0
    .param p1, "rootKeyVersion"    # I
    .param p2, "version"    # Ljava/lang/String;
    .param p3, "encryptedKey"    # [B

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    .line 29
    iput-object p2, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 35
    instance-of v2, p1, Lcom/google/android/apps/books/model/SessionKey;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/google/android/apps/books/model/SessionKey;

    .line 37
    .local v0, "o":Lcom/google/android/apps/books/model/SessionKey;
    iget v2, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    iget v3, v0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    iget-object v3, v0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 40
    .end local v0    # "o":Lcom/google/android/apps/books/model/SessionKey;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "rootKeyVersion"

    iget v2, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "version"

    iget-object v2, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "encryptedKey"

    iget-object v2, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
