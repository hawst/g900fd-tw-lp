.class public Lcom/google/android/apps/books/annotations/Updateables;
.super Ljava/lang/Object;
.source "Updateables.java"


# direct methods
.method public static finished(Ljava/lang/Object;)Lcom/google/android/apps/books/annotations/Updateable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/google/android/apps/books/annotations/Updateable",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 6
    .local p0, "value":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/apps/books/annotations/Updateables$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/annotations/Updateables$1;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static forPlaceholder(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;)Lcom/google/android/apps/books/annotations/Updateable;
    .locals 1
    .param p0, "placeholder"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p1, "annotationContextSearch"    # Lcom/google/android/apps/books/annotations/AnnotationContextSearch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/android/apps/books/annotations/AnnotationContextSearch;",
            ")",
            "Lcom/google/android/apps/books/annotations/Updateable",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v0

    if-nez v0, :cond_0

    .line 27
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/Updateables;->finished(Ljava/lang/Object;)Lcom/google/android/apps/books/annotations/Updateable;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/annotations/Updateables$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/annotations/Updateables$2;-><init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;)V

    goto :goto_0
.end method
