.class public Lcom/google/android/apps/books/render/PageStructureLocationRange;
.super Lcom/google/android/apps/books/annotations/LocationRange;
.source "PageStructureLocationRange.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/annotations/LocationRange",
        "<",
        "Lcom/google/android/apps/books/annotations/PageStructureLocation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/PageStructureLocation;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V
    .locals 3
    .param p1, "start"    # Lcom/google/android/apps/books/annotations/PageStructureLocation;
    .param p2, "end"    # Lcom/google/android/apps/books/annotations/PageStructureLocation;

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/annotations/LocationRange;-><init>(Ljava/lang/Object;Ljava/lang/Object;Z)V

    .line 21
    return-void
.end method

.method public static parseFromCfis(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/render/PageStructureLocationRange;
    .locals 3
    .param p0, "startCfi"    # Ljava/lang/String;
    .param p1, "endCfi"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/PageStructureLocation$ParseException;
        }
    .end annotation

    .prologue
    .line 25
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 26
    :cond_0
    const/4 v0, 0x0

    .line 28
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/books/render/PageStructureLocationRange;

    invoke-static {p0}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->parseCfi(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->parseCfi(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/PageStructureLocationRange;-><init>(Lcom/google/android/apps/books/annotations/PageStructureLocation;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V

    goto :goto_0
.end method


# virtual methods
.method protected getCorrespondingRangeFromAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/LocationRange;
    .locals 1
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ")",
            "Lcom/google/android/apps/books/annotations/LocationRange",
            "<",
            "Lcom/google/android/apps/books/annotations/PageStructureLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getPageStructureLocationRange()Lcom/google/android/apps/books/render/PageStructureLocationRange;

    move-result-object v0

    return-object v0
.end method
