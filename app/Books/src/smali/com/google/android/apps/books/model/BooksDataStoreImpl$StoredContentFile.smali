.class Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;
.super Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StoredContentFile"
.end annotation


# instance fields
.field private final mFile:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

.field private final mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

.field final synthetic this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V
    .locals 0
    .param p2, "file"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .param p3, "listener"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    .prologue
    .line 968
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->this$0:Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    .line 969
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;-><init>(Lcom/google/android/apps/books/data/VolumeContentFile;)V

    .line 970
    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->mFile:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    .line 971
    iput-object p3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    .line 972
    return-void
.end method


# virtual methods
.method public createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 976
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->mFile:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;->mListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v0

    .line 978
    .local v0, "fileSaver":Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    new-instance v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile$1;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;)V

    return-object v1
.end method
