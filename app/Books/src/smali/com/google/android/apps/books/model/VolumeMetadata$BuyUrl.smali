.class public Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;
.super Ljava/lang/Object;
.source "VolumeMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/VolumeMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BuyUrl"
.end annotation


# instance fields
.field private final mIsBuyUrl:Z

.field private final mUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "isBuyUrl"    # Z

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;->mUrl:Ljava/lang/String;

    .line 53
    iput-boolean p2, p0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;->mIsBuyUrl:Z

    .line 54
    return-void
.end method


# virtual methods
.method public getIsBuyUrl()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;->mIsBuyUrl:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/VolumeMetadata$BuyUrl;->mUrl:Ljava/lang/String;

    return-object v0
.end method
