.class public Lcom/google/android/apps/books/render/PageStructureHandle;
.super Ljava/lang/Object;
.source "PageStructureHandle.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/books/render/PageStructureHandle;",
        ">;"
    }
.end annotation


# instance fields
.field private mBlockIndex:I

.field private mParaIndex:I

.field private mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

.field private mWordIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->setInvalid()V

    .line 49
    return-void
.end method

.method private currentBlock()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlock(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v0

    return-object v0
.end method

.method private currentPara()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentBlock()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraph(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v0

    return-object v0
.end method

.method private currentWordIsEmpty()Z
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->inPageBounds()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWordBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->isEmpty(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static distance2FromBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;II)I
    .locals 8
    .param p0, "box"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v7, 0x0

    .line 33
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v0

    .line 34
    .local v0, "bx":I
    sub-int v4, v0, p1

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int v5, p1, v0

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int v2, v4, v5

    .line 36
    .local v2, "dx":I
    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getY()I

    move-result v1

    .line 37
    .local v1, "by":I
    sub-int v4, v1, p2

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int v5, p2, v1

    invoke-virtual {p0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    add-int v3, v4, v5

    .line 39
    .local v3, "dy":I
    mul-int v4, v2, v2

    mul-int v5, v3, v3

    add-int/2addr v4, v5

    return v4
.end method

.method private getWordBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;
    .locals 2

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentPara()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    invoke-virtual {v0, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordbox(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v0

    return-object v0
.end method

.method public static normalizeMatchString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "matchString"    # Ljava/lang/String;

    .prologue
    .line 110
    const-string v0, "\\s+"

    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V
    .locals 0
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p2, "blockIndex"    # I
    .param p3, "paraIndex"    # I
    .param p4, "wordIndex"    # I

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .line 167
    iput p2, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    .line 168
    iput p3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    .line 169
    iput p4, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    .line 170
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/books/render/PageStructureHandle;)I
    .locals 2
    .param p1, "other"    # Lcom/google/android/apps/books/render/PageStructureHandle;

    .prologue
    .line 178
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget v1, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    if-ne v0, v1, :cond_1

    .line 179
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    iget v1, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    if-ne v0, v1, :cond_0

    .line 180
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    iget v1, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    sub-int/2addr v0, v1

    .line 184
    :goto_0
    return v0

    .line 182
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    iget v1, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 184
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget v1, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/apps/books/render/PageStructureHandle;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->compareTo(Lcom/google/android/apps/books/render/PageStructureHandle;)I

    move-result v0

    return v0
.end method

.method public copyFrom(Lcom/google/android/apps/books/render/PageStructureHandle;)V
    .locals 4
    .param p1, "other"    # Lcom/google/android/apps/books/render/PageStructureHandle;

    .prologue
    .line 173
    iget-object v0, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    iget v1, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget v2, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    iget v3, p1, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/render/PageStructureHandle;->set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V

    .line 174
    return-void
.end method

.method public decrement()V
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->decrementOnce()V

    .line 222
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentWordIsEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->decrementOnce()V

    goto :goto_0

    .line 225
    :cond_0
    return-void
.end method

.method public decrementOnce()V
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    .line 229
    :goto_0
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    if-gez v0, :cond_0

    .line 230
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    .line 231
    :goto_1
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    if-gez v0, :cond_2

    .line 232
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    .line 233
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    if-gez v0, :cond_1

    .line 242
    :cond_0
    return-void

    .line 238
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentBlock()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraphCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    goto :goto_1

    .line 240
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentPara()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 284
    if-ne p0, p1, :cond_1

    .line 306
    :cond_0
    :goto_0
    return v1

    .line 287
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 288
    goto :goto_0

    .line 290
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 291
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 293
    check-cast v0, Lcom/google/android/apps/books/render/PageStructureHandle;

    .line 294
    .local v0, "other":Lcom/google/android/apps/books/render/PageStructureHandle;
    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget v4, v0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 295
    goto :goto_0

    .line 297
    :cond_4
    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    iget v4, v0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 298
    goto :goto_0

    .line 300
    :cond_5
    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    iget v4, v0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 301
    goto :goto_0

    .line 303
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    iget-object v4, v0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 304
    goto :goto_0
.end method

.method public getRect(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->isInvalid()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 254
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 263
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWordBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v1

    .line 258
    .local v1, "box":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getX()I

    move-result v2

    .line 259
    .local v2, "left":I
    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getY()I

    move-result v4

    .line 260
    .local v4, "top":I
    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v5

    add-int v3, v2, v5

    .line 261
    .local v3, "right":I
    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v5

    add-int v0, v4, v5

    .line 262
    .local v0, "bottom":I
    invoke-virtual {p1, v2, v4, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public getWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWordBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getWord()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWordString(Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Rect;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "startOffsetToRect":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Landroid/graphics/Rect;>;"
    const/4 v0, 0x1

    .line 121
    .local v0, "isFirst":Z
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .local v3, "sb":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->inPageBounds()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 128
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWord()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/render/PageStructureHandle;->normalizeMatchString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "normalizedWord":Ljava/lang/String;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 133
    .local v2, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->getRect(Landroid/graphics/Rect;)V

    .line 134
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->increment()V

    goto :goto_0

    .line 126
    .end local v1    # "normalizedWord":Ljava/lang/String;
    .end local v2    # "rect":Landroid/graphics/Rect;
    :cond_0
    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 138
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 273
    const/16 v0, 0x1f

    .line 274
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 275
    .local v1, "result":I
    iget v2, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    add-int/lit8 v1, v2, 0x1f

    .line 276
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    add-int v1, v2, v3

    .line 277
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    add-int v1, v2, v3

    .line 278
    return v1
.end method

.method public inPageBounds()Z
    .locals 2

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentBlock()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraphCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentPara()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public increment()V
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->incrementOnce()V

    .line 197
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentWordIsEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->incrementOnce()V

    goto :goto_0

    .line 200
    :cond_0
    return-void
.end method

.method public incrementOnce()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    .line 204
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentPara()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxCount()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 205
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    .line 206
    iput v2, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    .line 207
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->currentBlock()Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraphCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 208
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    .line 209
    iput v2, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    .line 210
    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    invoke-virtual {v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 217
    :cond_2
    return-void
.end method

.method public isAfter(Lcom/google/android/apps/books/render/PageStructureHandle;)Z
    .locals 1
    .param p1, "other"    # Lcom/google/android/apps/books/render/PageStructureHandle;

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/render/PageStructureHandle;->compareTo(Lcom/google/android/apps/books/render/PageStructureHandle;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Z
    .locals 1
    .param p1, "box"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getW()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;->getH()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInvalid()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAfterLastWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V
    .locals 2
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockCount()I

    move-result v0

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->decrement()V

    .line 68
    return-void
.end method

.method public setBeforeFirstWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;)V
    .locals 1
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1, v0, v0, v0}, Lcom/google/android/apps/books/render/PageStructureHandle;->set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V

    .line 63
    return-void
.end method

.method public setInvalid()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1, v1}, Lcom/google/android/apps/books/render/PageStructureHandle;->set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V

    .line 53
    return-void
.end method

.method public setTo(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;Lcom/google/android/apps/books/annotations/PageStructureLocation;)V
    .locals 3
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p2, "loc"    # Lcom/google/android/apps/books/annotations/PageStructureLocation;

    .prologue
    .line 321
    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->getBlockIndex()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->getParaIndex()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/PageStructureLocation;->getWordIndex()I

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V

    .line 322
    return-void
.end method

.method public setToClosestWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;FF)Z
    .locals 14
    .param p1, "structure"    # Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;
    .param p2, "fx"    # F
    .param p3, "fy"    # F

    .prologue
    .line 76
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 77
    .local v10, "x":I
    invoke-static/range {p3 .. p3}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 78
    .local v11, "y":I
    const/16 v2, 0x19

    .line 79
    .local v2, "blockSlopSquared":I
    const/4 v4, 0x0

    .line 80
    .local v4, "found":Z
    const v5, 0x7fffffff

    .line 81
    .local v5, "minDistance2":I
    const/4 v1, 0x0

    .local v1, "blockI":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlockCount()I

    move-result v12

    if-ge v1, v12, :cond_4

    .line 82
    invoke-virtual {p1, v1}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;->getBlock(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;

    move-result-object v0

    .line 83
    .local v0, "block":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v12

    invoke-static {v12, v10, v11}, Lcom/google/android/apps/books/render/PageStructureHandle;->distance2FromBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;II)I

    move-result v12

    const/16 v13, 0x19

    if-ge v12, v13, :cond_3

    .line 84
    const/4 v7, 0x0

    .local v7, "paraI":I
    :goto_1
    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraphCount()I

    move-result v12

    if-ge v7, v12, :cond_3

    .line 85
    invoke-virtual {v0, v7}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;->getParagraph(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;

    move-result-object v6

    .line 86
    .local v6, "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    const/4 v9, 0x0

    .local v9, "wordI":I
    :goto_2
    invoke-virtual {v6}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordboxCount()I

    move-result v12

    if-ge v9, v12, :cond_2

    .line 87
    invoke-virtual {v6, v9}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;->getWordbox(I)Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v8

    .line 90
    .local v8, "wbox":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    invoke-virtual {p0, v8}, Lcom/google/android/apps/books/render/PageStructureHandle;->isEmpty(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 91
    invoke-static {v8, v10, v11}, Lcom/google/android/apps/books/render/PageStructureHandle;->distance2FromBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;II)I

    move-result v3

    .line 92
    .local v3, "distance2":I
    if-ge v3, v5, :cond_1

    .line 93
    move v5, v3

    .line 94
    invoke-direct {p0, p1, v1, v7, v9}, Lcom/google/android/apps/books/render/PageStructureHandle;->set(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;III)V

    .line 95
    if-nez v3, :cond_0

    .line 96
    const/4 v12, 0x1

    .line 105
    .end local v0    # "block":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    .end local v3    # "distance2":I
    .end local v6    # "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .end local v7    # "paraI":I
    .end local v8    # "wbox":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .end local v9    # "wordI":I
    :goto_3
    return v12

    .line 98
    .restart local v0    # "block":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    .restart local v3    # "distance2":I
    .restart local v6    # "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .restart local v7    # "paraI":I
    .restart local v8    # "wbox":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    .restart local v9    # "wordI":I
    :cond_0
    const/4 v4, 0x1

    .line 86
    .end local v3    # "distance2":I
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 84
    .end local v8    # "wbox":Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 81
    .end local v6    # "para":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block$Paragraph;
    .end local v7    # "paraI":I
    .end local v9    # "wordI":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "block":Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure$Block;
    :cond_4
    move v12, v4

    .line 105
    goto :goto_3
.end method

.method public toLocation(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PageStructureLocation;
    .locals 6
    .param p1, "pageIndex"    # I
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 311
    new-instance v0, Lcom/google/android/apps/books/annotations/PageStructureLocation;

    iget v3, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    iget v4, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    iget v5, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    move v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/PageStructureLocation;-><init>(ILjava/lang/String;III)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PSL("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mBlockIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mParaIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mWordIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageStructureHandle;->getWordBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ocean/frontend/javascript/proto/PageInfo$WordBox;->getBox()Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/render/PageStructureHandle;->distance2FromBox(Lcom/google/ocean/frontend/javascript/proto/PageInfo$OFEBox;II)I

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 150
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageStructureHandle;->mStructure:Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/render/PageStructureHandle;->setToClosestWord(Lcom/google/ocean/frontend/javascript/proto/PageInfo$PageStructure;FF)Z

    move-result v0

    goto :goto_0
.end method
