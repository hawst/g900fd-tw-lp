.class interface abstract Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "VolumeStatesQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field public static final STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2052
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "position"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "last_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "pinned"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "has_offline_license"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "last_access"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "text_zoom"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "force_download"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "line_height"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "license_action"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "last_local_access"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "fit_width"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;->PROJECTION:[Ljava/lang/String;

    .line 2066
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method
