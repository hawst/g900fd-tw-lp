.class Lcom/google/android/apps/books/view/pages/PageTurnTexture;
.super Ljava/lang/Object;
.source "PageTurnTexture.java"


# instance fields
.field mBookmark:I

.field final mContext:Landroid/content/Context;

.field mEmptyDark:I

.field mEmptyLight:I

.field mEmptySepia:I

.field mLoading:I

.field private final mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

.field mShadow:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;)V
    .locals 1
    .param p1, "scene"    # Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    iget-object v0, v0, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public static uploadGlTexture(ILandroid/graphics/Bitmap;)V
    .locals 1
    .param p0, "texId"    # I
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;Z)V

    .line 74
    return-void
.end method

.method public static uploadGlTexture(ILandroid/graphics/Bitmap;Z)V
    .locals 5
    .param p0, "texId"    # I
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "recycle"    # Z

    .prologue
    .line 82
    if-lez p0, :cond_0

    if-nez p1, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const/16 v2, 0xde1

    :try_start_0
    invoke-static {v2, p0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 86
    const/16 v2, 0xde1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, p1, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 87
    const-string v2, "texImage2D"

    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 88
    :catch_0
    move-exception v1

    .line 89
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    const-string v2, "PageTurnTexture"

    const-string v3, "glError in calling textImage2D for resource"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    const/4 v4, 0x1

    invoke-static {p1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 92
    .local v0, "bitmap2":Landroid/graphics/Bitmap;
    const/16 v2, 0xde1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 93
    const-string v2, "texImage2D"

    invoke-static {v2}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .end local v0    # "bitmap2":Landroid/graphics/Bitmap;
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    throw v2
.end method


# virtual methods
.method initialize()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 42
    new-array v4, v8, [I

    .line 43
    .local v4, "textureIdMappingArray":[I
    invoke-static {v8, v4, v5}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 44
    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mLoading:I

    .line 45
    aget v5, v4, v7

    iput v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mShadow:I

    .line 46
    const/4 v5, 0x2

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptyLight:I

    .line 47
    const/4 v5, 0x3

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptyDark:I

    .line 48
    const/4 v5, 0x4

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mBookmark:I

    .line 49
    const/4 v5, 0x5

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptySepia:I

    .line 51
    const v5, 0x84c0

    invoke-static {v5}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 53
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_0

    .line 54
    const/16 v5, 0xde1

    aget v6, v4, v1

    invoke-static {v5, v6}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 55
    invoke-static {}, Lcom/google/android/opengl/carousel/GL2Helper;->setDefaultNPOTTextureState()V

    .line 53
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_0
    const-string v5, "gl detect error"

    invoke-static {v5}, Lcom/google/android/opengl/carousel/GL2Helper;->checkGlError(Ljava/lang/String;)V

    .line 58
    iget-object v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 59
    .local v2, "res":Landroid/content/res/Resources;
    const v5, 0x7f0201fb

    invoke-static {v2, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 60
    .local v3, "shadowBm":Landroid/graphics/Bitmap;
    const v5, 0x7f020044

    invoke-static {v2, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 63
    .local v0, "bookmarkBm":Landroid/graphics/Bitmap;
    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mShadow:I

    invoke-static {v5, v3, v7}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;Z)V

    .line 64
    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mBookmark:I

    invoke-static {v5, v0, v7}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;Z)V

    .line 66
    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptyLight:I

    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mEmptyBitmapLight:Landroid/graphics/Bitmap;

    invoke-static {v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;)V

    .line 67
    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptyDark:I

    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mEmptyBitmapDark:Landroid/graphics/Bitmap;

    invoke-static {v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;)V

    .line 68
    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mEmptySepia:I

    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mEmptyBitmapSepia:Landroid/graphics/Bitmap;

    invoke-static {v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;)V

    .line 69
    iget v5, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mLoading:I

    iget-object v6, p0, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mSetting:Lcom/google/android/apps/books/view/pages/PageTurnSetting;

    iget-object v6, v6, Lcom/google/android/apps/books/view/pages/PageTurnSetting;->mLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v5, v6}, Lcom/google/android/apps/books/view/pages/PageTurnTexture;->uploadGlTexture(ILandroid/graphics/Bitmap;)V

    .line 70
    return-void
.end method
