.class Lcom/google/android/apps/books/widget/PagesViewController$6;
.super Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 1

    .prologue
    .line 2991
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$6;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    return-void
.end method


# virtual methods
.method draw(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "clipRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2994
    const-string v0, "PagesViewHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2995
    const-string v0, "PagesViewHelper"

    const-string v1, "Painting non-turning decorations"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2998
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$6;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3800(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/LabeledRect;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2999
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$6;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController;->access$2700(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/PageCanvasPainter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$6;->this$0:Lcom/google/android/apps/books/widget/PagesViewController;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController;->access$3800(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/LabeledRect;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/render/PageCanvasPainter;->paintPressedVolumeAnnotationRect(Landroid/graphics/Canvas;Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;)V

    .line 3002
    :cond_1
    return-void
.end method
