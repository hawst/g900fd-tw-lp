.class Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;
.super Ljava/lang/Object;
.source "OfflineDictionaryDownloadCardProvider.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->maybeGetOfflineCard()Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

.field final synthetic val$cardView:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;->this$0:Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;->val$cardView:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;->this$0:Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;

    # getter for: Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;->access$100(Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setShouldShowOfflineDictionaryCard(Z)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/OfflineDictionaryDownloadCardProvider$2;->val$cardView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 122
    return-void
.end method
