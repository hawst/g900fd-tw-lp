.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GeoAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

.field public static final enum GEO_ANNOTATION_GOOGLE_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

.field public static final enum GEO_ANNOTATION_MAP_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

.field public static final enum GEO_ANNOTATION_TAPPED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

.field public static final enum GEO_ANNOTATION_WIKIPEDIA_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 442
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    const-string v1, "GEO_ANNOTATION_TAPPED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_TAPPED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    .line 446
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    const-string v1, "GEO_ANNOTATION_MAP_LAUNCHED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_MAP_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    .line 450
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    const-string v1, "GEO_ANNOTATION_WIKIPEDIA_LAUNCHED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_WIKIPEDIA_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    .line 454
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    const-string v1, "GEO_ANNOTATION_GOOGLE_LAUNCHED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_GOOGLE_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    .line 438
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_TAPPED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_MAP_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_WIKIPEDIA_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_GOOGLE_LAUNCHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 438
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 438
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;
    .locals 1

    .prologue
    .line 438
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    return-object v0
.end method
