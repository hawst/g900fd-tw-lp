.class public final enum Lcom/google/android/apps/books/provider/BooksContract$PathParam;
.super Ljava/lang/Enum;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PathParam"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/provider/BooksContract$PathParam;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum ACCOUNT_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum ACCOUNT_NAME:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum CHAPTER_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum COLLECTION_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum PAGE_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum QUERY:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum RES_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum SEGMENT_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum SESSION_KEY_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

.field public static final enum VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "ACCOUNT_NAME"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->ACCOUNT_NAME:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "VOLUME_ID"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "SEGMENT_ID"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->SEGMENT_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "CHAPTER_ID"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->CHAPTER_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "RES_ID"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->RES_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "PAGE_ID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->PAGE_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "SESSION_KEY_ID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->SESSION_KEY_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "QUERY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->QUERY:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    .line 77
    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "COLLECTION_ID"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->COLLECTION_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    new-instance v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    const-string v1, "ACCOUNT_ID"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/provider/BooksContract$PathParam;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->ACCOUNT_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    .line 74
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->ACCOUNT_NAME:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->VOLUME_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->SEGMENT_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->CHAPTER_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->RES_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->PAGE_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->SESSION_KEY_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->QUERY:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->COLLECTION_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->ACCOUNT_ID:Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$PathParam;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/provider/BooksContract$PathParam;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$PathParam;->$VALUES:[Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/provider/BooksContract$PathParam;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/provider/BooksContract$PathParam;

    return-object v0
.end method


# virtual methods
.method public valueFrom(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 84
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract;->getUriType(Landroid/net/Uri;)Lcom/google/android/apps/books/provider/BooksContract$BooksUri;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/google/android/apps/books/provider/BooksContract$BooksUri;->getWildcard(Landroid/net/Uri;Lcom/google/android/apps/books/provider/BooksContract$PathParam;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
