.class Lcom/google/android/apps/books/app/BooksImageManager;
.super Ljava/lang/Object;
.source "BooksImageManager.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;
.implements Lcom/google/android/apps/books/common/ImageManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;
    }
.end annotation


# instance fields
.field private final mCallbackHandler:Landroid/os/Handler;

.field private final mCallbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/ublib/util/ImageSpecifier;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/common/ImageCallback;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mImageCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Lcom/google/android/ublib/util/ImageSpecifier;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/apps/books/net/ResponseGetter;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resolver"    # Landroid/content/ContentResolver;
    .param p3, "responseGetter"    # Lcom/google/android/apps/books/net/ResponseGetter;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    const-string v1, "missing resolver"

    invoke-static {p2, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mResolver:Landroid/content/ContentResolver;

    .line 103
    const-string v1, "missing responseGetter"

    invoke-static {p3, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/net/ResponseGetter;

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    .line 105
    new-instance v1, Lcom/google/android/apps/books/app/BitmapLruCache;

    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksImageManager;->getCacheSize(Landroid/content/Context;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/app/BitmapLruCache;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    .line 106
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    .line 108
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/MathUtils;->constrain(III)I

    move-result v0

    .line 110
    .local v0, "poolSize":I
    const-string v1, "BooksImageManager"

    const/4 v2, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/books/util/ExecutorUtils;->createThreadPoolExecutor(Ljava/lang/String;IILjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mExecutor:Ljava/util/concurrent/Executor;

    .line 111
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbackHandler:Landroid/os/Handler;

    .line 112
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/BooksImageManager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksImageManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Landroid/net/Uri;

    .prologue
    .line 64
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksImageManager;->isRemoteUri(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/BooksImageManager;)Lcom/google/android/apps/books/net/ResponseGetter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksImageManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/BooksImageManager;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksImageManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$500(Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Ljava/util/Set;

    .prologue
    .line 64
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksImageManager;->asList(Ljava/util/Set;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/BooksImageManager;Lcom/google/android/ublib/util/ImageSpecifier;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksImageManager;
    .param p1, "x1"    # Lcom/google/android/ublib/util/ImageSpecifier;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/BooksImageManager;->cacheBitmap(Lcom/google/android/ublib/util/ImageSpecifier;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/BooksImageManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksImageManager;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addCallback(Lcom/google/android/ublib/util/ImageSpecifier;Lcom/google/android/apps/books/common/ImageCallback;)Z
    .locals 5
    .param p1, "spec"    # Lcom/google/android/ublib/util/ImageSpecifier;
    .param p2, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 181
    const/4 v1, 0x0

    .line 182
    .local v1, "needToStartTask":Z
    const-string v2, "BooksImageManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 183
    const-string v2, "BooksImageManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding image callback "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_0
    if-nez p1, :cond_1

    .line 186
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "uri is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 188
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 189
    .local v0, "callbacks":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/common/ImageCallback;>;"
    if-nez v0, :cond_2

    .line 190
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "callbacks":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/common/ImageCallback;>;"
    const/4 v2, 0x4

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 191
    .restart local v0    # "callbacks":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/common/ImageCallback;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    const/4 v1, 0x1

    .line 194
    :cond_2
    if-eqz p2, :cond_3

    .end local p2    # "callback":Lcom/google/android/apps/books/common/ImageCallback;
    :goto_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 195
    return v1

    .restart local p2    # "callback":Lcom/google/android/apps/books/common/ImageCallback;
    :cond_3
    move-object p2, p0

    .line 194
    goto :goto_0
.end method

.method private static asList(Ljava/util/Set;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 228
    .local p0, "source":Ljava/util/Set;, "Ljava/util/Set<TT;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private cacheBitmap(Lcom/google/android/ublib/util/ImageSpecifier;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "spec"    # Lcom/google/android/ublib/util/ImageSpecifier;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    return-void
.end method

.method private static getCacheSize(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 408
    invoke-static {p0}, Lcom/google/android/apps/books/util/ReaderUtils;->maybeInitialize(Landroid/content/Context;)V

    .line 409
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->getFullColorScreenBytes()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method private static isRemoteUri(Landroid/net/Uri;)Z
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 216
    if-eqz p0, :cond_1

    .line 217
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "scheme":Ljava/lang/String;
    const-string v2, "http"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "https"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 220
    .end local v0    # "scheme":Ljava/lang/String;
    :cond_1
    return v1
.end method


# virtual methods
.method public cancelCallback(Lcom/google/android/apps/books/common/ImageCallback;)V
    .locals 6
    .param p1, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 199
    const-string v3, "BooksImageManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    const-string v3, "BooksImageManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cancelling image callback "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/ublib/util/ImageSpecifier;

    .line 203
    .local v2, "spec":Lcom/google/android/ublib/util/ImageSpecifier;
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 206
    .local v0, "callbacks":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/common/ImageCallback;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 207
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    iget-object v3, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    .end local v0    # "callbacks":Ljava/util/Set;, "Ljava/util/Set<Lcom/google/android/apps/books/common/ImageCallback;>;"
    .end local v2    # "spec":Lcom/google/android/ublib/util/ImageSpecifier;
    :cond_2
    return-void
.end method

.method public clear()V
    .locals 3

    .prologue
    .line 158
    const-string v0, "BooksImageManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "BooksImageManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Image Cache Eviction: size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2}, Landroid/support/v4/util/LruCache;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hits: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2}, Landroid/support/v4/util/LruCache;->hitCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " misses: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2}, Landroid/support/v4/util/LruCache;->missCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " evict: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2}, Landroid/support/v4/util/LruCache;->evictionCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 169
    return-void
.end method

.method public getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "ensurer"    # Lcom/google/android/apps/books/common/ImageManager$Ensurer;
    .param p4, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;
    .param p5, "imageStore"    # Lcom/google/android/apps/books/model/RemoteFileCache;

    .prologue
    .line 118
    if-nez p1, :cond_0

    .line 119
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 126
    :cond_0
    new-instance v1, Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-direct {v1, p1, p2}, Lcom/google/android/ublib/util/ImageSpecifier;-><init>(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;)V

    .line 127
    .local v1, "spec":Lcom/google/android/ublib/util/ImageSpecifier;
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mImageCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v2, v1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 129
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 130
    if-eqz p4, :cond_1

    .line 131
    const/4 v2, 0x0

    invoke-interface {p4, v0, v2}, Lcom/google/android/apps/books/common/ImageCallback;->onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V

    .line 132
    const-string v2, "BooksImageManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    const-string v2, "BooksImageManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Image found in cache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_1
    new-instance v2, Lcom/google/android/apps/books/common/NullImageFuture;

    invoke-direct {v2}, Lcom/google/android/apps/books/common/NullImageFuture;-><init>()V

    .line 144
    :goto_0
    return-object v2

    .line 140
    :cond_2
    invoke-direct {p0, v1, p4}, Lcom/google/android/apps/books/app/BooksImageManager;->addCallback(Lcom/google/android/ublib/util/ImageSpecifier;Lcom/google/android/apps/books/common/ImageCallback;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 141
    iget-object v2, p0, Lcom/google/android/apps/books/app/BooksImageManager;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    invoke-direct {v3, p0, p3, v1, p5}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;-><init>(Lcom/google/android/apps/books/app/BooksImageManager;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/ublib/util/ImageSpecifier;Lcom/google/android/apps/books/model/RemoteFileCache;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 144
    :cond_3
    new-instance v2, Lcom/google/android/apps/books/app/BooksImageManager$1;

    invoke-direct {v2, p0, p4}, Lcom/google/android/apps/books/app/BooksImageManager$1;-><init>(Lcom/google/android/apps/books/app/BooksImageManager;Lcom/google/android/apps/books/common/ImageCallback;)V

    goto :goto_0
.end method

.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 96
    return-void
.end method
