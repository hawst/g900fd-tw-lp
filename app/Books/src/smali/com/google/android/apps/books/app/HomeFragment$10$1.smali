.class Lcom/google/android/apps/books/app/HomeFragment$10$1;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment$10;->take(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/HomeFragment$10;

.field final synthetic val$t:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment$10;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 868
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$10$1;->this$1:Lcom/google/android/apps/books/app/HomeFragment$10;

    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$10$1;->val$t:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 1
    .param p1, "controller"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$10$1;->val$t:Ljava/util/List;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->setRecommendations(Ljava/util/List;)V

    .line 872
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 868
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$10$1;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
