.class Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;
.super Ljava/lang/Object;
.source "PagesViewController.java"

# interfaces
.implements Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->onRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

.field final synthetic val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final synthetic val$painter:Lcom/google/android/apps/books/render/PagePainter;

.field final synthetic val$pvc:Lcom/google/android/apps/books/widget/PagesViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 0

    .prologue
    .line 1097
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->val$pvc:Lcom/google/android/apps/books/widget/PagesViewController;

    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->val$painter:Lcom/google/android/apps/books/render/PagePainter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .prologue
    .line 1100
    const/4 v0, 0x1

    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->val$pvc:Lcom/google/android/apps/books/widget/PagesViewController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->val$page:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->this$0:Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    # getter for: Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->access$2000(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)Lcom/google/android/apps/books/util/RenderRequestContext;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer$1;->val$painter:Lcom/google/android/apps/books/render/PagePainter;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForFullView(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->access$1900(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V

    .line 1106
    return-void
.end method
