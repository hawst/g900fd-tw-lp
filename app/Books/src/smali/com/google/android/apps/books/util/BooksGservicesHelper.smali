.class public final Lcom/google/android/apps/books/util/BooksGservicesHelper;
.super Ljava/lang/Object;
.source "BooksGservicesHelper.java"


# direct methods
.method public static getDeveloperKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const-string v0, "books:plus_one_developer_key"

    const-string v1, "AIzaSyCuLL2piIVBGOtu196oSi3-ndISBYPOjCU"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFontTestString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    const-string v0, "books:font_test_string"

    const-string v1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-!@#$%^&*()_+=,./<>?;:\"|[]{}#\u2022\u2023\u2190\u2191\u2192\u2193\u2194\u2195\u2460\u2461\u2462\u2463\u0394\u03c6\u231c\u231d\u231e\u231f\u2639\u00dd\u03b7\u03b9\u03ba\u03bf\u0950\u017d\u017e\u2153\u2154\uff61\uff71\uff0e\u2208\u2242\u201c\u201d\u0905\u0917\u039b\u03b3\'\u03c0"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->getString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHatsSampleDays(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 148
    const-string v1, "books:hats_sample_days"

    const/4 v2, 0x7

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/books/util/GservicesHelper;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 149
    .local v0, "result":I
    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public static getHatsSampleGroups(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    const-string v1, "books:hats_sample_groups"

    const/16 v2, 0x64

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/books/util/GservicesHelper;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 141
    .local v0, "result":I
    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method public static getMaxUploadSizeMB(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    const-string v0, "books:max_upload_size_mb"

    const/16 v1, 0x64

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static isInSupportedCountry(Landroid/content/Context;)Z
    .locals 3
    .param p0, "resolver"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "defaultValue":Z
    const-string v1, "books:launched_in_current_country"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/books/util/GservicesHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static shouldGetAccessLock(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const-string v0, "books_phone:get_access_lock"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static shouldShowNotesExport(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 132
    const-string v0, "books:notes_export_enabled"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/books/util/GservicesHelper;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
