.class public Lcom/google/android/apps/books/model/AccountSessionKeyId;
.super Ljava/lang/Object;
.source "AccountSessionKeyId.java"

# interfaces
.implements Lcom/google/android/apps/books/model/SessionKeyId;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/AccountSessionKeyId$Query;
    }
.end annotation


# static fields
.field private static final INSTANCE:Lcom/google/android/apps/books/model/AccountSessionKeyId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/AccountSessionKeyId;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/model/AccountSessionKeyId;->INSTANCE:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->keyFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    .line 64
    .local v0, "key":Lcom/google/android/apps/books/model/SessionKey;
    if-nez v0, :cond_0

    .line 69
    const/4 v1, 0x0

    .line 71
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/google/android/apps/books/model/AccountSessionKeyId;->INSTANCE:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v1

    goto :goto_0
.end method

.method public static get()Lcom/google/android/apps/books/model/AccountSessionKeyId;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/books/model/AccountSessionKeyId;->INSTANCE:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    return-object v0
.end method

.method public static keyFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/SessionKey;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 47
    const/4 v1, 0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "sessionKeyVersion":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 53
    const/4 v1, 0x0

    .line 55
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/SessionKey;

    const/4 v2, 0x3

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/books/model/SessionKey;-><init>(ILjava/lang/String;[B)V

    goto :goto_0
.end method

.method public static setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V
    .locals 2
    .param p0, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 100
    const-string v0, "session_key_version"

    iget-object v1, p0, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v0, "root_key_version"

    iget v1, p0, Lcom/google/android/apps/books/model/SessionKey;->rootKeyVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 102
    const-string v0, "session_key_blob"

    iget-object v1, p0, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 103
    return-void
.end method


# virtual methods
.method public addToContentValues(Landroid/content/ContentValues;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 81
    const-string v0, "storage_format"

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->ACCOUNT_KEY_ENCRYPTED:Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    invoke-virtual {v1}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->getDatabaseValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 83
    const-string v0, "session_key_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public deleteKeyAndEncryptedContent(Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 0
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 88
    invoke-interface {p1, p0}, Lcom/google/android/apps/books/model/BooksDataStore;->removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/AccountSessionKeyId;)V

    .line 89
    return-void
.end method

.method public load(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/SessionKey;
    .locals 2
    .param p1, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 36
    invoke-interface {p1}, Lcom/google/android/apps/books/model/BooksDataStore;->getAccountSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    .line 37
    .local v0, "localKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/AccountSessionKeyId;>;"
    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v1

    .line 40
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public update(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/model/BooksDataStore;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p2, "store"    # Lcom/google/android/apps/books/model/BooksDataStore;

    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/model/BooksDataStore;->updateAccountSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 77
    return-void
.end method
