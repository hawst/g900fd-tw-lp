.class Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;
.super Ljava/lang/Object;
.source "StaggeredCoverView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationRecord"
.end annotation


# instance fields
.field public final location:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

.field public final timestamp:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView;Landroid/view/View;)V
    .locals 5
    .param p2, "child"    # Landroid/view/View;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;->this$0:Lcom/google/android/apps/books/widget/StaggeredCoverView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    invoke-virtual {p2}, Landroid/view/View;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 106
    .local v0, "left":I
    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 107
    .local v1, "top":I
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;->location:Landroid/graphics/Rect;

    .line 108
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$LocationRecord;->timestamp:J

    .line 109
    return-void
.end method
