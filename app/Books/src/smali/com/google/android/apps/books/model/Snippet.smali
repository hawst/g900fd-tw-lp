.class public Lcom/google/android/apps/books/model/Snippet;
.super Ljava/lang/Object;
.source "Snippet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/Snippet$Builder;
    }
.end annotation


# static fields
.field public static EMPTY:Lcom/google/android/apps/books/model/Snippet;


# instance fields
.field private final mString:Landroid/text/Spanned;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/apps/books/model/Snippet;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/Snippet;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lcom/google/android/apps/books/model/Snippet;->EMPTY:Lcom/google/android/apps/books/model/Snippet;

    return-void
.end method

.method public constructor <init>(Landroid/text/Spanned;)V
    .locals 0
    .param p1, "spanned"    # Landroid/text/Spanned;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 64
    invoke-static {p1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/Snippet;-><init>(Landroid/text/Spanned;)V

    .line 65
    return-void
.end method

.method public static builder(II)Lcom/google/android/apps/books/model/Snippet$Builder;
    .locals 1
    .param p0, "style"    # I
    .param p1, "color"    # I

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/apps/books/model/Snippet$1;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/model/Snippet$1;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public concat(Lcom/google/android/apps/books/model/Snippet;)Lcom/google/android/apps/books/model/Snippet;
    .locals 4
    .param p1, "other"    # Lcom/google/android/apps/books/model/Snippet;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 78
    .end local p1    # "other":Lcom/google/android/apps/books/model/Snippet;
    :goto_0
    return-object p1

    .line 75
    .restart local p1    # "other":Lcom/google/android/apps/books/model/Snippet;
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object p1, p0

    .line 76
    goto :goto_0

    .line 78
    :cond_2
    new-instance v0, Lcom/google/android/apps/books/model/Snippet;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, " "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p1, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    aput-object v3, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/Snippet;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public getStyledText()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withoutSpans()Lcom/google/android/apps/books/model/Snippet;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/apps/books/model/Snippet;

    iget-object v1, p0, Lcom/google/android/apps/books/model/Snippet;->mString:Landroid/text/Spanned;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/Snippet;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method
