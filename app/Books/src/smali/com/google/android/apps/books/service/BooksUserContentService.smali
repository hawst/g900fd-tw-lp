.class public Lcom/google/android/apps/books/service/BooksUserContentService;
.super Landroid/app/Service;
.source "BooksUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;,
        Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;,
        Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;,
        Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinderHoneycomb;,
        Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;
    }
.end annotation


# instance fields
.field private mBinder:Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 374
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService;->mBinder:Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 75
    new-instance v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinderHoneycomb;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinderHoneycomb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService;->mBinder:Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService;->mBinder:Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;

    goto :goto_0
.end method
