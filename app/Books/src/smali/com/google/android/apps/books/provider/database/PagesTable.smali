.class Lcom/google/android/apps/books/provider/database/PagesTable;
.super Ljava/lang/Object;
.source "PagesTable.java"


# direct methods
.method public static getCreationSql()Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    const-string v0, "REFERENCES chapters(chapter_id)"

    .line 37
    .local v0, "refChapterId":Ljava/lang/String;
    const-string v1, "REFERENCES segments(segment_id)"

    .line 39
    .local v1, "refSectionId":Ljava/lang/String;
    const-string v2, "REFERENCES session_keys(_id)"

    .line 41
    .local v2, "refSessionKeyId":Ljava/lang/String;
    const-string v3, "CREATE TABLE pages (_id INTEGER PRIMARY KEY AUTOINCREMENT, account_name TEXT NOT NULL, volume_id TEXT NOT NULL, page_id TEXT NOT NULL, title TEXT, page_order INTEGER NOT NULL, remote_url TEXT, cc_box_x INTEGER, cc_box_y INTEGER, cc_box_w INTEGER, cc_box_h INTEGER, first_section_id TEXT REFERENCES segments(segment_id), first_chapter_id TEXT REFERENCES chapters(chapter_id), content_status INTEGER NOT NULL DEFAULT 0, session_key_id INTEGER REFERENCES session_keys(_id), structure_status INTEGER NOT NULL DEFAULT 0, storage_format INTEGER, FOREIGN KEY(account_name, volume_id) REFERENCES volumes(account_name, volume_id), UNIQUE (account_name, volume_id, page_id) ON CONFLICT REPLACE)"

    return-object v3
.end method

.method public static getIndexSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string v0, "CREATE INDEX IF NOT EXISTS pages_chapter_index ON pages (account_name, volume_id);"

    return-object v0
.end method

.method public static getViewSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, "CREATE VIEW view_pages AS SELECT pages.*, session_keys.session_key_version AS session_key_version, session_keys.root_key_version AS root_key_version, session_keys.session_key_blob AS session_key_blob FROM pages LEFT OUTER JOIN session_keys ON session_keys._id=pages.session_key_id"

    return-object v0
.end method
