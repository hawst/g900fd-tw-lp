.class public Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
.super Ljava/lang/Object;
.source "GeoAnnotationPayload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    }
.end annotation


# instance fields
.field public final cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

.field public final countryCode:Ljava/lang/String;

.field public final latitude:F

.field public final longitude:F

.field public final resolution:Ljava/lang/String;

.field public final zoom:I


# direct methods
.method public constructor <init>(FFILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;)V
    .locals 0
    .param p1, "latitude"    # F
    .param p2, "longitude"    # F
    .param p3, "zoom"    # I
    .param p4, "countryCode"    # Ljava/lang/String;
    .param p5, "resolution"    # Ljava/lang/String;
    .param p6, "cachePolicy"    # Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput p1, p0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->latitude:F

    .line 53
    iput p2, p0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->longitude:F

    .line 54
    iput p3, p0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->zoom:I

    .line 55
    iput-object p4, p0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->countryCode:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->resolution:Ljava/lang/String;

    .line 57
    iput-object p6, p0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;->cachePolicy:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .line 58
    return-void
.end method
