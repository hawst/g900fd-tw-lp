.class Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/api/data/UserSettings;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

.field final synthetic val$context:Lcom/google/android/apps/books/app/AppSettingsActivity;

.field final synthetic val$this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;Lcom/google/android/apps/books/app/AppSettingsActivity;Lcom/google/android/apps/books/app/AppSettingsActivity;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->this$1:Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive;

    iput-object p2, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->val$this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    iput-object p3, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->val$context:Lcom/google/android/apps/books/app/AppSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/UserSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 573
    .local p1, "settingsOr":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;"
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1$1;-><init>(Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 609
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 570
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/AppSettingsActivity$SaveNotesToDrive$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
