.class public interface abstract Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$Callbacks;
.super Ljava/lang/Object;
.source "TransientInfoCardsLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract getActionBarBottom()I
.end method

.method public abstract onCardsLayoutHidden()V
.end method

.method public abstract onSystemWindowInsetsChanged(Landroid/graphics/Rect;)V
.end method
