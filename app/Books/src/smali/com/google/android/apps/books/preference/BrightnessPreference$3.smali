.class Lcom/google/android/apps/books/preference/BrightnessPreference$3;
.super Ljava/lang/Object;
.source "BrightnessPreference.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/preference/BrightnessPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/preference/BrightnessPreference;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 166
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mBinding:Z
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$100(Lcom/google/android/apps/books/preference/BrightnessPreference;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mWindow:Landroid/view/Window;
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$600(Lcom/google/android/apps/books/preference/BrightnessPreference;)Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # invokes: Lcom/google/android/apps/books/preference/BrightnessPreference;->getBrightnessValue()I
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$700(Lcom/google/android/apps/books/preference/BrightnessPreference;)I

    move-result v0

    .line 171
    .local v0, "brightnessValue":I
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$800(Lcom/google/android/apps/books/preference/BrightnessPreference;)Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$800(Lcom/google/android/apps/books/preference/BrightnessPreference;)Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;->onBrightnessChange(I)V

    goto :goto_0

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    # getter for: Lcom/google/android/apps/books/preference/BrightnessPreference;->mWindow:Landroid/view/Window;
    invoke-static {v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$600(Lcom/google/android/apps/books/preference/BrightnessPreference;)Landroid/view/Window;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/WindowUtils;->setBrightness(ILandroid/view/Window;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/preference/BrightnessPreference;->setBrightnessAutoChecked(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/preference/BrightnessPreference;->access$300(Lcom/google/android/apps/books/preference/BrightnessPreference;Z)V

    .line 162
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/books/preference/BrightnessPreference$3;->this$0:Lcom/google/android/apps/books/preference/BrightnessPreference;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/BrightnessPreference;->persistPreference()V

    .line 157
    return-void
.end method
