.class public Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RecommendationsConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
        ">;>;"
    }
.end annotation


# instance fields
.field public mProvider:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

.field final synthetic this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;)V
    .locals 0

    .prologue
    .line 1158
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private maybeSetRecommendations()V
    .locals 3

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->mProvider:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v0, v0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1171
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recommendations"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->mProvider:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iget-object v1, v1, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->setRecommendations(Ljava/util/List;)V

    .line 1175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->mProvider:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .line 1177
    :cond_1
    return-void
.end method


# virtual methods
.method public setProvider(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)V
    .locals 0
    .param p1, "provider"    # Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .prologue
    .line 1180
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->mProvider:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    .line 1181
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->maybeSetRecommendations()V

    .line 1182
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1158
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->take(Ljava/util/List;)V

    return-void
.end method

.method public take(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1162
    .local p1, "t":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1163
    const-string v0, "ReaderFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recommendations"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->this$1:Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;

    iput-object p1, v0, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader;->mRecommendations:Ljava/util/List;

    .line 1166
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$RecommendationLoader$RecommendationsConsumer;->maybeSetRecommendations()V

    .line 1167
    return-void
.end method
