.class public Lcom/google/android/apps/books/util/PassagePages;
.super Ljava/lang/Object;
.source "PassagePages.java"


# instance fields
.field private mLastReadyPageIndex:I

.field private mPassageHasKnownSize:Z

.field private final mPassageIndex:I

.field private mPassageReady:Z

.field private mPreloading:Z

.field private final mRenderings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    .line 22
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageHasKnownSize:Z

    .line 23
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageReady:Z

    .line 24
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPreloading:Z

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mLastReadyPageIndex:I

    .line 35
    iput p1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageIndex:I

    .line 36
    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public getEndOfLastPage()I
    .locals 3

    .prologue
    .line 130
    iget-object v1, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 131
    .local v0, "lastPageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    iget v1, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    return v1
.end method

.method public getLastReadyPageIndex()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mLastReadyPageIndex:I

    return v0
.end method

.method public getPageIndexForX(ILjava/lang/Integer;)I
    .locals 4
    .param p1, "xValue"    # I
    .param p2, "nearByPage"    # Ljava/lang/Integer;

    .prologue
    .line 102
    if-nez p2, :cond_2

    .line 103
    const/4 v1, 0x0

    .line 113
    .local v1, "currentPage":I
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v0

    .line 114
    .local v0, "curPageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageLeft:I

    if-ge p1, v2, :cond_5

    .line 115
    add-int/lit8 v1, v1, -0x1

    .line 122
    :goto_1
    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 123
    :cond_1
    const/4 v2, -0x1

    :goto_2
    return v2

    .line 105
    .end local v0    # "curPageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .end local v1    # "currentPage":I
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_3

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 106
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "currentPage":I
    goto :goto_0

    .line 108
    .end local v1    # "currentPage":I
    :cond_4
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .restart local v1    # "currentPage":I
    goto :goto_0

    .line 116
    .restart local v0    # "curPageBounds":Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    :cond_5
    iget v2, v0, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->pageRight:I

    if-lt p1, v2, :cond_6

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    move v2, v1

    .line 120
    goto :goto_2
.end method

.method public getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 2
    .param p1, "pageIndex"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/DevicePageRendering;

    return-object v0
.end method

.method public getPagesCount()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public getPassageIndex()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageIndex:I

    return v0
.end method

.method public hasKnownSize()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageHasKnownSize:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isPassageReady()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageReady:Z

    return v0
.end method

.method public isPreloading()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mPreloading:Z

    return v0
.end method

.method public markPurged()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/util/PassagePages;->setPreloading(Z)V

    .line 52
    iput-boolean v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageReady:Z

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mLastReadyPageIndex:I

    .line 54
    return-void
.end method

.method public markReadyAndSizeKnown()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/PassagePages;->getPagesCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/util/PassagePages;->mLastReadyPageIndex:I

    .line 58
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageHasKnownSize:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPassageReady:Z

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/util/PassagePages;->setPreloading(Z)V

    .line 61
    return-void
.end method

.method public setLastReadyPageIndex(I)V
    .locals 0
    .param p1, "lastReadyPageIndex"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/google/android/apps/books/util/PassagePages;->mLastReadyPageIndex:I

    .line 149
    return-void
.end method

.method public setPreloading(Z)V
    .locals 0
    .param p1, "preloading"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/google/android/apps/books/util/PassagePages;->mPreloading:Z

    .line 69
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/google/android/apps/books/util/PassagePages;->mRenderings:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 138
    .local v1, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 140
    .end local v1    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
