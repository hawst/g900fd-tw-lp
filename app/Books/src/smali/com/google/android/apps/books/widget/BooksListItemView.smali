.class public Lcom/google/android/apps/books/widget/BooksListItemView;
.super Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;
.source "BooksListItemView.java"


# instance fields
.field private mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;-><init>(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;)Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/BooksListItemView;
    .param p1, "x1"    # Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksListItemView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    return-object p1
.end method

.method public static createOrReuseView(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/books/widget/BooksListItemView;
    .locals 1
    .param p0, "convertView"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->createOrReuseView(Landroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;)Lcom/google/android/apps/books/widget/BooksListItemView;

    move-result-object v0

    return-object v0
.end method

.method public static createOrReuseView(Landroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;)Lcom/google/android/apps/books/widget/BooksListItemView;
    .locals 3
    .param p0, "convertView"    # Landroid/view/View;
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    if-nez p0, :cond_0

    .line 46
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksListItemView;

    .line 49
    .end local p0    # "convertView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 48
    .restart local p0    # "convertView":Landroid/view/View;
    :cond_0
    const v0, 0x7f0e00e7

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    check-cast p0, Lcom/google/android/apps/books/widget/BooksListItemView;

    .end local p0    # "convertView":Landroid/view/View;
    move-object v0, p0

    goto :goto_0
.end method

.method private getAuthorView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 105
    const v0, 0x7f0e00e8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private getPriceView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 109
    const v0, 0x7f0e00e9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private getTitleView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 101
    const v0, 0x7f0e0086

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public getActionTouchArea()Landroid/view/View;
    .locals 1

    .prologue
    .line 122
    const v0, 0x7f0e00eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getActionView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 117
    const v0, 0x7f0e00eb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getTitleView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getAuthorView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getPriceView()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/AccessibilityUtils;->getVolumeContentDescription(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCoverView()Lcom/google/android/apps/books/widget/RemoteCardImageView;
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f0e00e5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/RemoteCardImageView;

    return-object v0
.end method

.method public getReasonView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 113
    const v0, 0x7f0e00ea

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/apps/books/widget/ChildStretchingLinearLayout;->onDetachedFromWindow()V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksListItemView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksListItemView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    invoke-interface {v0}, Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;->dismiss()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksListItemView;->mOptionsMenu:Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu;

    .line 72
    :cond_0
    return-void
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getAuthorView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method

.method public setPrice(Ljava/lang/String;)V
    .locals 1
    .param p1, "price"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getPriceView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getReasonView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void
.end method

.method setupCoverView(Landroid/net/Uri;Lcom/google/android/apps/books/common/ImageManager;Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;)V
    .locals 2
    .param p1, "coverUri"    # Landroid/net/Uri;
    .param p2, "imageManager"    # Lcom/google/android/apps/books/common/ImageManager;
    .param p3, "imageListener"    # Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksListItemView;->getCoverView()Lcom/google/android/apps/books/widget/RemoteCardImageView;

    move-result-object v0

    .line 128
    .local v0, "coverView":Lcom/google/android/apps/books/widget/RemoteCardImageView;
    sget-object v1, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_WIDTH:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->setFillStyle(Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    .line 129
    invoke-virtual {v0, p3}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->setListener(Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;)V

    .line 130
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->setImageManager(Lcom/google/android/apps/books/common/ImageManager;)V

    .line 131
    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->setImageURI(Landroid/net/Uri;)V

    .line 132
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/RemoteCardImageView;->setShowDimmedCover(Z)V

    .line 133
    return-void
.end method

.method setupOverflowIconView(Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/ublib/view/SystemUi;)V
    .locals 2
    .param p1, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;
    .param p2, "menuHandlerFactory"    # Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;
    .param p3, "systemUi"    # Lcom/google/android/ublib/view/SystemUi;

    .prologue
    .line 138
    const v1, 0x7f0e00e1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/BooksListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 139
    .local v0, "overflowIcon":Landroid/view/View;
    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 140
    new-instance v1, Lcom/google/android/apps/books/widget/BooksListItemView$1;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/apps/books/widget/BooksListItemView$1;-><init>(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/ublib/view/SystemUi;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    return-void
.end method
