.class Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;
.super Ljava/lang/Object;
.source "TextContentMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/TextContentMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PassageBuilder"
.end annotation


# static fields
.field private static final EMPTY_CSS_INDICES:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mChapterIndex:I

.field mCssIndices:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mMaxSegmentCount:I

.field mSegmentCount:I

.field final mSegmentIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mStartPosition:Ljava/lang/String;

.field mViewable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 424
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->EMPTY_CSS_INDICES:Ljava/util/Collection;

    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "maxSegmentCount"    # I

    .prologue
    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentIndices:Ljava/util/List;

    .line 409
    iput p1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mMaxSegmentCount:I

    .line 410
    return-void
.end method


# virtual methods
.method add(IILcom/google/android/apps/books/model/Segment;Ljava/util/Collection;)V
    .locals 2
    .param p1, "chapterIndex"    # I
    .param p2, "segmentIndex"    # I
    .param p3, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/apps/books/model/Segment;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 433
    .local p4, "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iget v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentCount:I

    if-nez v0, :cond_1

    .line 434
    invoke-interface {p3}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mStartPosition:Ljava/lang/String;

    .line 435
    iput p1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mChapterIndex:I

    .line 436
    invoke-interface {p3}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mViewable:Z

    .line 437
    if-nez p4, :cond_0

    sget-object p4, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->EMPTY_CSS_INDICES:Ljava/util/Collection;

    .end local p4    # "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    :cond_0
    iput-object p4, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mCssIndices:Ljava/util/Collection;

    .line 439
    :cond_1
    invoke-interface {p3}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentIndices:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 442
    :cond_2
    iget v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentCount:I

    .line 443
    return-void
.end method

.method build()Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 447
    new-instance v0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentIndices:Ljava/util/List;

    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mCssIndices:Ljava/util/Collection;

    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mStartPosition:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;Lcom/google/android/apps/books/model/TextContentMetadata$1;)V

    .line 449
    .local v0, "result":Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentCount:I

    .line 450
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentIndices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 451
    iput-object v4, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mCssIndices:Ljava/util/Collection;

    .line 452
    return-object v0
.end method

.method compatible(ILcom/google/android/apps/books/model/Segment;)Z
    .locals 3
    .param p1, "chapterIndex"    # I
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;

    .prologue
    const/4 v0, 0x0

    .line 417
    iget v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentCount:I

    iget v2, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mMaxSegmentCount:I

    if-ne v1, v2, :cond_1

    .line 420
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mSegmentCount:I

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mViewable:Z

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Segment;->isViewable()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->mChapterIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
