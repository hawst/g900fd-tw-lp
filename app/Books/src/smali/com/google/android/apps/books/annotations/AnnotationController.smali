.class public interface abstract Lcom/google/android/apps/books/annotations/AnnotationController;
.super Ljava/lang/Object;
.source "AnnotationController.java"


# virtual methods
.method public abstract fetchAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;IILcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            "II",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract fetchCopyQuota(Lcom/google/android/apps/books/annotations/VolumeVersion;)V
.end method

.method public abstract fetchImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract fetchLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract fetchVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;)V
.end method

.method public abstract getForegroundAnnotationEditor(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/UserChangesEditor;
.end method

.method public abstract getRecentAnnotationsForLayer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract needsDefaultImageDimensions()Z
.end method

.method public abstract removeLocalVolumeAnnotationsForVolume(Ljava/lang/String;)V
.end method

.method public abstract setDefaultImageDimensions(II)V
.end method

.method public abstract startServerSync(Ljava/util/List;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/Void;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract updateLayers(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer;",
            ">;)V"
        }
    .end annotation
.end method

.method public varargs abstract weaklyAddListeners(Lcom/google/android/apps/books/annotations/VolumeVersion;[Lcom/google/android/apps/books/annotations/AnnotationListener;)V
.end method
