.class public Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;
.super Lcom/google/android/apps/books/app/InfoCardProvider;
.source "FootnoteCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;,
        Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

.field private final mCardBgPadding:Landroid/graphics/Rect;

.field private final mContext:Landroid/content/Context;

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

.field private final mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

.field private final mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;Lcom/google/android/apps/books/render/ResourceContentStore;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/render/ReaderDataSource;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;
    .param p3, "resourceContentStore"    # Lcom/google/android/apps/books/render/ResourceContentStore;
    .param p4, "account"    # Landroid/accounts/Account;
    .param p5, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p6, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p7, "dataSource"    # Lcom/google/android/apps/books/render/ReaderDataSource;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardProvider;-><init>()V

    .line 72
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCardBgPadding:Landroid/graphics/Rect;

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mContext:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    .line 79
    iput-object p3, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

    .line 80
    iput-object p4, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mAccount:Landroid/accounts/Account;

    .line 81
    iput-object p5, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 82
    iput-object p6, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 83
    iput-object p7, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    .line 84
    new-instance v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$1;-><init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/render/ReaderDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mDataSource:Lcom/google/android/apps/books/render/ReaderDataSource;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # F
    .param p5, "x5"    # Ljava/lang/String;
    .param p6, "x6"    # I
    .param p7, "x7"    # Lcom/google/android/ublib/utils/Consumer;

    .prologue
    .line 49
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->loadContent(Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    return-object v0
.end method

.method private getThemedBackgroundColor(Ljava/lang/String;)I
    .locals 2
    .param p1, "theme"    # Ljava/lang/String;

    .prologue
    .line 255
    const-string v0, "1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 258
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private getThemedHeaderBackgroundColor(Ljava/lang/String;)I
    .locals 3
    .param p1, "theme"    # Ljava/lang/String;

    .prologue
    .line 263
    iget-object v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 264
    .local v0, "dayColor":I
    const-string v1, "0"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    .end local v0    # "dayColor":I
    :goto_0
    return v0

    .restart local v0    # "dayColor":I
    :cond_0
    const v1, 0x3f333333    # 0.7f

    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v0

    goto :goto_0
.end method

.method private loadContent(Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
    .locals 24
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "textZoom"    # F
    .param p5, "theme"    # Ljava/lang/String;
    .param p6, "screenHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "F",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p7, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v22, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const v3, 0x7f04004f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/ViewUtils;->inflateWithContext(ILandroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v17

    .line 171
    .local v17, "footnoteView":Landroid/view/ViewGroup;
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    const v3, 0x7f0e00fe

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 174
    .local v18, "header":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mContext:Landroid/content/Context;

    const v4, 0x7f0f01fd

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->getThemedHeaderBackgroundColor(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 177
    const v3, 0x7f0e0100

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/Button;

    .line 178
    .local v15, "button":Landroid/widget/Button;
    new-instance v3, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$3;-><init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;Lcom/google/android/apps/books/common/Position;)V

    invoke-virtual {v15, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    const v3, 0x7f0e00fc

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 186
    .local v14, "bgView":Landroid/view/View;
    invoke-virtual {v14}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 187
    .local v12, "background":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCardBgPadding:Landroid/graphics/Rect;

    invoke-virtual {v12, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 189
    const v3, 0x7f0e00fd

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 191
    .local v16, "footnoteBody":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Landroid/widget/FrameLayout$LayoutParams;

    .line 193
    .local v19, "lp":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, v19

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCardBgPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    move-object/from16 v0, v19

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 194
    move-object/from16 v0, v19

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCardBgPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    move-object/from16 v0, v19

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 195
    move-object/from16 v0, v19

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCardBgPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    move-object/from16 v0, v19

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 196
    move-object/from16 v0, v19

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCardBgPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    move-object/from16 v0, v19

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 197
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 199
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->getThemedBackgroundColor(Ljava/lang/String;)I

    move-result v13

    .line 200
    .local v13, "bgColor":I
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Landroid/view/View;->setBackgroundColor(I)V

    .line 202
    const v3, 0x7f0e00ff

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;

    .line 205
    .local v2, "webView":Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningBeforeKitKat()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 212
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 215
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 216
    .local v21, "res":Landroid/content/res/Resources;
    mul-int/lit8 v3, p6, 0x3

    div-int/lit8 v3, v3, 0x4

    const v4, 0x7f09012d

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    const v4, 0x7f09012c

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int v20, v3, v4

    .line 219
    .local v20, "maxWebViewHeight":I
    move/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setMaxHeight(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    # invokes: Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setCallbacks(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->access$500(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;)V

    .line 221
    invoke-virtual {v2}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 222
    const/high16 v3, 0x42c80000    # 100.0f

    mul-float v3, v3, p4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setInitialScale(I)V

    .line 223
    const-string v3, "file:///android_asset/"

    const-string v5, "text/html"

    const-string v6, "UTF-8"

    const/4 v7, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v11, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$4;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$4;-><init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)V

    .line 237
    .local v11, "webViewClientDelegate":Landroid/webkit/WebViewClient;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mResourceContentStore:Lcom/google/android/apps/books/render/ResourceContentStore;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mResourceExceptionHandler:Lcom/google/android/ublib/utils/Consumer;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getManifest()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v6}, Lcom/google/android/apps/books/model/VolumeMetadata;->getResourceIdToResource()Lcom/google/common/base/Function;

    move-result-object v10

    move-object v6, v2

    invoke-interface/range {v3 .. v11}, Lcom/google/android/apps/books/render/ResourceContentStore;->configureWebView(Landroid/accounts/Account;Ljava/lang/String;Landroid/webkit/WebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Landroid/webkit/WebViewClient;)V

    .line 241
    new-instance v3, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$5;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$5;-><init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 248
    invoke-virtual {v2}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v23

    .line 249
    .local v23, "webSettings":Landroid/webkit/WebSettings;
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 251
    invoke-static/range {v22 .. v22}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-interface {v0, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 252
    return-void
.end method


# virtual methods
.method public loadLinkCards(Lcom/google/android/apps/books/render/TouchableItem;FLjava/lang/String;ILcom/google/android/ublib/utils/Consumer;)V
    .locals 12
    .param p1, "item"    # Lcom/google/android/apps/books/render/TouchableItem;
    .param p2, "textZoom"    # F
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "screenHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            "F",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p5, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/TouchableItem;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v7

    .line 99
    .local v7, "position":Lcom/google/android/apps/books/common/Position;
    iget-object v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "volumeId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1, v7}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v11

    .line 101
    .local v11, "segmentIndex":I
    iget-object v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/Segment;

    .line 103
    .local v4, "segment":Lcom/google/android/apps/books/model/Segment;
    iget v1, p1, Lcom/google/android/apps/books/render/TouchableItem;->isFootnote:I

    if-nez v1, :cond_0

    .line 104
    sget-object v1, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    move-object/from16 v0, p5

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 163
    :goto_0
    return-void

    .line 108
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;

    move-object v2, p0

    move-object v5, p1

    move-object v6, p3

    move v8, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$2;-><init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/apps/books/render/TouchableItem;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;FILcom/google/android/ublib/utils/Consumer;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v1, v2}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method
