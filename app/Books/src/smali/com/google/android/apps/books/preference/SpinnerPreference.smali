.class public Lcom/google/android/apps/books/preference/SpinnerPreference;
.super Landroid/widget/LinearLayout;
.source "SpinnerPreference.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/preference/SpinnerPreference$AccessibleSpinner;
    }
.end annotation


# instance fields
.field private final mDropdownItemLayoutId:I

.field private mEntries:[Ljava/lang/CharSequence;

.field private final mKey:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mPrefs:Landroid/content/SharedPreferences;

.field private mSpinner:Landroid/widget/Spinner;

.field private final mSpinnerLayoutId:I

.field private final mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mValues:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/SpinnerPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 95
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 272
    new-instance v1, Lcom/google/android/apps/books/preference/SpinnerPreference$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/SpinnerPreference$1;-><init>(Lcom/google/android/apps/books/preference/SpinnerPreference;)V

    iput-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 99
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->Preference:[I

    invoke-virtual {p1, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 101
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mKey:Ljava/lang/String;

    .line 102
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->ListPreference:[I

    invoke-virtual {p1, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "missing entries"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mEntries:[Ljava/lang/CharSequence;

    .line 111
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "missing values"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    .line 114
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 118
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->ListPreference:[I

    invoke-virtual {p1, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 122
    const/4 v1, 0x3

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mDropdownItemLayoutId:I

    .line 124
    iget v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mDropdownItemLayoutId:I

    if-nez v1, :cond_0

    .line 125
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing dropdown item layout ID in SpinnerPreference"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1

    .line 129
    :cond_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinnerLayoutId:I

    .line 131
    iget v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinnerLayoutId:I

    if-nez v1, :cond_1

    .line 132
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Missing spinner layout ID in SpinnerPreference"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/preference/SpinnerPreference;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/SpinnerPreference;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/preference/SpinnerPreference;->setLabelSelection(I)V

    return-void
.end method

.method private getEntry()Ljava/lang/String;
    .locals 2

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 205
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mEntries:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 208
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setLabelSelection(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 190
    const-string v2, "justification2"

    iget-object v3, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    const v2, 0x7f0e019b

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/preference/SpinnerPreference;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 192
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 193
    if-le p1, v1, :cond_1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 196
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    return-void

    .line 193
    .restart local v0    # "view":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindPreference()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mPrefs:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->bindPreferenceInternal(Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public bindPreferenceInternal(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 238
    if-nez p1, :cond_1

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 244
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->setSelection(I)V

    goto :goto_0

    .line 242
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected createAdapter([Ljava/lang/CharSequence;)Landroid/widget/ArrayAdapter;
    .locals 4
    .param p1, "entries"    # [Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/CharSequence;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinnerLayoutId:I

    const v3, 0x1020014

    invoke-direct {v0, v1, v2, v3, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 184
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    iget v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mDropdownItemLayoutId:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 185
    return-object v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroid/widget/LinearLayout;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->getEntry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 213
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 214
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    aget-object v1, v1, v0

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinnerListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mEntries:[Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/preference/SpinnerPreference;->setEntries([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V

    .line 150
    return-void
.end method

.method public persistPreference()V
    .locals 4

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mPrefs:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_2

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 267
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;->onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 169
    return-void
.end method

.method public setEntries([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "entries"    # [Ljava/lang/CharSequence;
    .param p2, "values"    # [Ljava/lang/CharSequence;

    .prologue
    .line 172
    const-string v1, "missing entries"

    invoke-static {p1, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mEntries:[Ljava/lang/CharSequence;

    .line 173
    const-string v1, "missing values"

    invoke-static {p2, v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mValues:[Ljava/lang/CharSequence;

    .line 174
    array-length v1, p1

    array-length v2, p2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "mismatched entries and values"

    invoke-static {v1, v2}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mEntries:[Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/SpinnerPreference;->createAdapter([Ljava/lang/CharSequence;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 178
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 179
    return-void

    .line 174
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/preference/SpinnerPreference;->setLabelSelection(I)V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 201
    return-void
.end method

.method public setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V
    .locals 0
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "listener"    # Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mPrefs:Landroid/content/SharedPreferences;

    .line 228
    iput-object p2, p0, Lcom/google/android/apps/books/preference/SpinnerPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/SpinnerPreference;->bindPreference()V

    .line 230
    return-void
.end method
