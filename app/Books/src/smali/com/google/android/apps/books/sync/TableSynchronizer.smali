.class public Lcom/google/android/apps/books/sync/TableSynchronizer;
.super Ljava/lang/Object;
.source "TableSynchronizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    }
.end annotation


# static fields
.field private static final EMPTY:Landroid/content/ContentValues;


# instance fields
.field private final mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/sync/TableSynchronizer;->EMPTY:Landroid/content/ContentValues;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/sync/Synchronizable;)V
    .locals 5
    .param p1, "synchronizable"    # Lcom/google/android/apps/books/sync/Synchronizable;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const-string v2, "missing synchronizable"

    invoke-static {p1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/sync/Synchronizable;

    iput-object v2, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    .line 82
    iget-object v2, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v2}, Lcom/google/android/apps/books/sync/Synchronizable;->getRowKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "missing rowKey"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 84
    .local v1, "rowKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v2}, Lcom/google/android/apps/books/sync/Synchronizable;->getWritableColumnToClass()Ljava/util/Map;

    move-result-object v0

    .line 85
    .local v0, "columnToClass":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rowKey "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not one of the table\'s columns "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 88
    return-void
.end method

.method private createKeyToDbRow()Ljava/util/HashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v5, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v5}, Lcom/google/android/apps/books/sync/Synchronizable;->queryAll()Landroid/database/Cursor;

    move-result-object v1

    .line 208
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v5}, Lcom/google/android/apps/books/sync/Synchronizable;->getRowKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 209
    .local v3, "rowKeyIndex":I
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Lcom/google/common/collect/Maps;->newHashMapWithExpectedSize(I)Ljava/util/HashMap;

    move-result-object v2

    .line 211
    .local v2, "keyToRow":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/content/ContentValues;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v5}, Lcom/google/android/apps/books/sync/Synchronizable;->getWritableColumnToClass()Ljava/util/Map;

    move-result-object v0

    .line 213
    .local v0, "columnToClass":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 214
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 215
    .local v4, "rowKeyValue":Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/google/android/apps/books/sync/SyncUtil;->cursorToValues(Landroid/database/Cursor;Ljava/util/Map;)Landroid/content/ContentValues;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 220
    .end local v0    # "columnToClass":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    .end local v2    # "keyToRow":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/content/ContentValues;>;"
    .end local v3    # "rowKeyIndex":I
    .end local v4    # "rowKeyValue":Ljava/lang/String;
    :catchall_0
    move-exception v5

    if-eqz v1, :cond_0

    .line 221
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v5

    .line 220
    .restart local v0    # "columnToClass":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    .restart local v2    # "keyToRow":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/content/ContentValues;>;"
    .restart local v3    # "rowKeyIndex":I
    :cond_1
    if-eqz v1, :cond_2

    .line 221
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v2
.end method

.method private createKeyToValues(Ljava/lang/Iterable;)Ljava/util/LinkedHashMap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/content/ContentValues;",
            ">;)",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "valuesIter":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 167
    .local v1, "keyToValues":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Landroid/content/ContentValues;>;"
    iget-object v7, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v7}, Lcom/google/android/apps/books/sync/Synchronizable;->getRowKey()Ljava/lang/String;

    move-result-object v4

    .line 168
    .local v4, "rowKey":Ljava/lang/String;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ContentValues;

    .line 169
    .local v6, "values":Landroid/content/ContentValues;
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/sync/TableSynchronizer;->filterValues(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v5

    .line 170
    .local v5, "subset":Landroid/content/ContentValues;
    invoke-virtual {v5, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "keyValue":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 172
    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Key "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " missing in subset "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 174
    :cond_1
    invoke-virtual {v1, v2, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    .line 175
    .local v3, "oldValues":Landroid/content/ContentValues;
    if-eqz v3, :cond_0

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 176
    new-instance v7, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Inconsistent values, overriding "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " with "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 180
    .end local v2    # "keyValue":Ljava/lang/String;
    .end local v3    # "oldValues":Landroid/content/ContentValues;
    .end local v5    # "subset":Landroid/content/ContentValues;
    .end local v6    # "values":Landroid/content/ContentValues;
    :cond_2
    return-object v1
.end method

.method private filterValues(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 188
    const/4 v2, 0x0

    .line 189
    .local v2, "subset":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v4}, Lcom/google/android/apps/books/sync/Synchronizable;->getWritableColumnToClass()Ljava/util/Map;

    move-result-object v0

    .line 190
    .local v0, "columnToClass":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class<*>;>;"
    invoke-virtual {p1}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 191
    .local v3, "value":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 192
    if-nez v2, :cond_1

    .line 193
    new-instance v2, Landroid/content/ContentValues;

    .end local v2    # "subset":Landroid/content/ContentValues;
    invoke-direct {v2, p1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 195
    .restart local v2    # "subset":Landroid/content/ContentValues;
    :cond_1
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    .end local v3    # "value":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_2
    if-nez v2, :cond_3

    .end local p1    # "values":Landroid/content/ContentValues;
    :goto_1
    return-object p1

    .restart local p1    # "values":Landroid/content/ContentValues;
    :cond_3
    move-object p1, v2

    goto :goto_1
.end method

.method private getCurrentRow(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 7
    .param p1, "subset"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 272
    iget-object v3, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v3, p1}, Lcom/google/android/apps/books/sync/Synchronizable;->query(Landroid/content/ContentValues;)Landroid/database/Cursor;

    move-result-object v0

    .line 274
    .local v0, "itemCursor":Landroid/database/Cursor;
    if-nez v0, :cond_1

    .line 291
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v2

    .line 277
    :cond_1
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 278
    .local v1, "rowCount":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_4

    .line 279
    if-eqz v1, :cond_2

    .line 280
    const-string v4, "TableSynchronizer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad row count: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v3, "TableSynchronizer"

    const/4 v6, 0x3

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " for "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    :cond_2
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 280
    :cond_3
    :try_start_1
    const-string v3, ""

    goto :goto_1

    .line 285
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_5

    .line 286
    const-string v3, "TableSynchronizer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not move to first: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 289
    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v2}, Lcom/google/android/apps/books/sync/Synchronizable;->getWritableColumnToClass()Ljava/util/Map;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/books/sync/SyncUtil;->cursorToValues(Landroid/database/Cursor;Ljava/util/Map;)Landroid/content/ContentValues;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 291
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 291
    .end local v1    # "rowCount":I
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_6

    .line 292
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2
.end method

.method private rawSyncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 8
    .param p1, "origRow"    # Landroid/content/ContentValues;
    .param p2, "subset"    # Landroid/content/ContentValues;

    .prologue
    .line 298
    const/4 v1, 0x0

    .line 299
    .local v1, "changes":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 300
    .local v0, "attempts":I
    move-object v2, p1

    .line 302
    .local v2, "currentRow":Landroid/content/ContentValues;
    :goto_0
    if-nez v1, :cond_1

    const/4 v3, 0x5

    if-ge v0, v3, :cond_1

    .line 303
    invoke-direct {p0, v2, p2}, Lcom/google/android/apps/books/sync/TableSynchronizer;->trySyncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v1

    .line 304
    if-nez v1, :cond_0

    .line 307
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/sync/TableSynchronizer;->getCurrentRow(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v2

    .line 309
    const-string v3, "TableSynchronizer"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 310
    const-string v3, "TableSynchronizer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Orig row: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", current row: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    :cond_1
    if-nez v1, :cond_2

    .line 318
    const-string v3, "TableSynchronizer"

    const-string v4, ""

    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot synchronize "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " (originally "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") with values "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 321
    :cond_2
    return-object v1
.end method

.method private trySyncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 7
    .param p1, "currentRow"    # Landroid/content/ContentValues;
    .param p2, "subset"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 235
    if-nez p1, :cond_1

    .line 237
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v4, p2}, Lcom/google/android/apps/books/sync/Synchronizable;->insertOrThrow(Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, p2

    .line 262
    :cond_0
    :goto_0
    return-object v2

    .line 239
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    const-string v4, "TableSynchronizer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Conflict when inserting "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nvalues: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 243
    goto :goto_0

    .line 246
    .end local v0    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v4, p1, p2}, Lcom/google/android/apps/books/sync/Synchronizable;->extractUpdates(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v2

    .line 247
    .local v2, "updates":Landroid/content/ContentValues;
    if-nez v2, :cond_2

    .line 248
    sget-object v2, Lcom/google/android/apps/books/sync/TableSynchronizer;->EMPTY:Landroid/content/ContentValues;

    goto :goto_0

    .line 250
    :cond_2
    invoke-virtual {v2}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 255
    iget-object v4, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v4, p1, v2}, Lcom/google/android/apps/books/sync/Synchronizable;->update(Landroid/content/ContentValues;Landroid/content/ContentValues;)I

    move-result v1

    .line 256
    .local v1, "rowCount":I
    if-eq v1, v5, :cond_0

    .line 258
    if-le v1, v5, :cond_3

    .line 259
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updated "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "rows, expected 0 or 1"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    :cond_3
    move-object v2, v3

    .line 262
    goto :goto_0
.end method


# virtual methods
.method public syncRow(Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 153
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public syncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "valuesHint"    # Landroid/content/ContentValues;

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/TableSynchronizer;->filterValues(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v1

    .line 141
    .local v1, "subset":Landroid/content/ContentValues;
    if-nez p2, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;->getCurrentRow(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v0

    .line 142
    .local v0, "currentRow":Landroid/content/ContentValues;
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;->rawSyncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v2

    if-eqz v2, :cond_1

    .end local v1    # "subset":Landroid/content/ContentValues;
    :goto_1
    return-object v1

    .end local v0    # "currentRow":Landroid/content/ContentValues;
    .restart local v1    # "subset":Landroid/content/ContentValues;
    :cond_0
    move-object v0, p2

    .line 141
    goto :goto_0

    .line 142
    .restart local v0    # "currentRow":Landroid/content/ContentValues;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public syncRows(Ljava/lang/Iterable;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Landroid/content/ContentValues;",
            ">;)",
            "Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "valuesList":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Landroid/content/ContentValues;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/TableSynchronizer;->createKeyToValues(Ljava/lang/Iterable;)Ljava/util/LinkedHashMap;

    move-result-object v5

    .line 103
    .local v5, "keyToValues":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Landroid/content/ContentValues;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/TableSynchronizer;->createKeyToDbRow()Ljava/util/HashMap;

    move-result-object v4

    .line 104
    .local v4, "keyToDbRow":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Landroid/content/ContentValues;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->size()I

    move-result v9

    invoke-direct {v7, v9}, Ljava/util/HashMap;-><init>(I)V

    .line 106
    .local v7, "modifiedValues":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/content/ContentValues;>;"
    new-instance v8, Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->size()I

    move-result v9

    invoke-direct {v8, v9}, Ljava/util/HashMap;-><init>(I)V

    .line 109
    .local v8, "originals":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/content/ContentValues;>;"
    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 110
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/content/ContentValues;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 113
    .local v6, "keyValue":Ljava/lang/String;
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 114
    .local v0, "currentRow":Landroid/content/ContentValues;
    invoke-interface {v8, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/ContentValues;

    invoke-direct {p0, v0, v9}, Lcom/google/android/apps/books/sync/TableSynchronizer;->rawSyncRow(Landroid/content/ContentValues;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    move-result-object v9

    invoke-interface {v7, v6, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 118
    .end local v0    # "currentRow":Landroid/content/ContentValues;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/content/ContentValues;>;"
    .end local v6    # "keyValue":Ljava/lang/String;
    :cond_0
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    .line 119
    .local v2, "excessRows":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/content/ContentValues;>;"
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 120
    iget-object v9, p0, Lcom/google/android/apps/books/sync/TableSynchronizer;->mSynchronizable:Lcom/google/android/apps/books/sync/Synchronizable;

    invoke-interface {v9, v2}, Lcom/google/android/apps/books/sync/Synchronizable;->delete(Ljava/util/Collection;)V

    .line 122
    :cond_1
    new-instance v9, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    invoke-direct {v9, v2, v8, v7}, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;-><init>(Ljava/util/Collection;Ljava/util/Map;Ljava/util/Map;)V

    return-object v9
.end method
