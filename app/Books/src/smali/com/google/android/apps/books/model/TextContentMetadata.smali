.class public Lcom/google/android/apps/books/model/TextContentMetadata;
.super Ljava/lang/Object;
.source "TextContentMetadata.java"

# interfaces
.implements Lcom/google/android/apps/books/common/Position$PageOrdering;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/TextContentMetadata$1;,
        Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;,
        Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    }
.end annotation


# instance fields
.field private final mChapterIdToChapterIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mChapterIdToStartPageIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageIdToPageIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

.field private final mPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;"
        }
    .end annotation
.end field

.field private final mPassages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/TextContentMetadata$Passage;",
            ">;"
        }
    .end annotation
.end field

.field private final mReadingPositionToChapterIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegmentIdToCssIndices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mSegmentIdToSegmentIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;"
        }
    .end annotation
.end field

.field private final mStartPageIndexToChapterIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final mStartPositionToSegmentIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mStartSegmentIndexToChapterIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mStartSegmentIndexToPassageIndex:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/TreeMap;Ljava/util/TreeMap;Ljava/util/TreeMap;Lcom/google/android/apps/books/common/Position$PageOrdering;Ljava/util/TreeMap;Ljava/util/TreeMap;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Map;ILjava/util/TreeMap;)V
    .locals 8
    .param p4, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p10, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p12, "maxSegmentsPerPassage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/books/common/Position$PageOrdering;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;I",
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "startSegmentIndexToPassageIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p2, "startPositionToSegmentIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    .local p3, "startSegmentIndexToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p5, "startPageIndexToChapterId":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p6, "chapterIdToStartPageId":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p7, "chapterIdToChapterIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p8, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p9, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p11, "segmentIdToCssIndices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/Integer;>;>;"
    .local p13, "readingPositionToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    iput-object p1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartSegmentIndexToPassageIndex:Ljava/util/TreeMap;

    .line 254
    iput-object p2, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartPositionToSegmentIndex:Ljava/util/TreeMap;

    .line 255
    iput-object p3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartSegmentIndexToChapterIndex:Ljava/util/TreeMap;

    .line 256
    iput-object p4, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    .line 257
    iput-object p5, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartPageIndexToChapterIndex:Ljava/util/TreeMap;

    .line 258
    iput-object p6, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mChapterIdToStartPageIndex:Ljava/util/TreeMap;

    .line 259
    iput-object p7, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mChapterIdToChapterIndex:Ljava/util/Map;

    .line 260
    invoke-interface/range {p10 .. p10}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegments:Ljava/util/List;

    .line 261
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPageIdToPageIndex:Ljava/util/Map;

    .line 262
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegmentIdToSegmentIndex:Ljava/util/Map;

    .line 263
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mReadingPositionToChapterIndex:Ljava/util/TreeMap;

    .line 264
    invoke-interface/range {p10 .. p10}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPages:Ljava/util/List;

    .line 265
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegmentIdToCssIndices:Ljava/util/Map;

    .line 267
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    .line 268
    invoke-interface/range {p10 .. p10}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    move-object v1, p0

    move-object/from16 v3, p11

    move-object v4, p3

    move/from16 v5, p12

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/model/TextContentMetadata;->buildPassages(Ljava/util/List;Ljava/util/Map;Ljava/util/TreeMap;ILjava/util/List;Ljava/util/TreeMap;)V

    .line 272
    const-string v1, "TextContentMetadata"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    const-string v1, "TextContentMetadata"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Split "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p10 .. p10}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " chapters into "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " passages"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :cond_0
    return-void
.end method

.method private buildPassages(Ljava/util/List;Ljava/util/Map;Ljava/util/TreeMap;ILjava/util/List;Ljava/util/TreeMap;)V
    .locals 7
    .param p4, "maxSegmentsPerPassage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/TextContentMetadata$Passage;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .local p2, "segmentIdToCssIndices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/Integer;>;>;"
    .local p3, "startSegmentIndexToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p5, "passages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/TextContentMetadata$Passage;>;"
    .local p6, "startSegmentIndexToPassageIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 463
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 482
    :goto_0
    return-void

    .line 466
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;

    invoke-direct {v0, p4}, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;-><init>(I)V

    .line 467
    .local v0, "builder":Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;
    const/4 v4, 0x0

    .line 469
    .local v4, "segmentIndex":I
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p6, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    .line 471
    .local v3, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 473
    .local v1, "chapterIndex":I
    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->compatible(ILcom/google/android/apps/books/model/Segment;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 474
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->build()Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    move-result-object v5

    invoke-interface {p5, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 475
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p6, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    :cond_1
    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    invoke-virtual {v0, v1, v4, v3, v5}, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->add(IILcom/google/android/apps/books/model/Segment;Ljava/util/Collection;)V

    .line 479
    add-int/lit8 v4, v4, 0x1

    .line 480
    goto :goto_1

    .line 481
    .end local v1    # "chapterIndex":I
    .end local v3    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/TextContentMetadata$PassageBuilder;->build()Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    move-result-object v5

    invoke-interface {p5, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static buildReadingPositionToChapterIndex(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/TreeMap;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;)",
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 506
    .local p0, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    .local p1, "positionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    invoke-static {p1}, Lcom/google/common/collect/Maps;->newTreeMap(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v4

    .line 507
    .local v4, "result":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 508
    .local v1, "chapterIndex":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    .line 509
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getReadingPosition()Ljava/lang/String;

    move-result-object v3

    .line 510
    .local v3, "readingPosition":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 511
    new-instance v5, Lcom/google/android/apps/books/common/Position;

    invoke-direct {v5, v3}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 514
    goto :goto_0

    .line 515
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v3    # "readingPosition":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method private static buildStartPageIdToChapterIndex(Ljava/util/List;Ljava/util/Map;Ljava/util/TreeMap;Ljava/util/TreeMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 541
    .local p0, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    .local p1, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p2, "startPageIndexToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .local p3, "chapterIdToStartPageIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 556
    :cond_0
    return-void

    .line 544
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 545
    .local v1, "chapterCount":I
    const/4 v3, -0x1

    .line 546
    .local v3, "lastStartPageIndex":I
    const/4 v2, 0x0

    .local v2, "chapterIndex":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 547
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    .line 549
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v4

    if-eq v4, v3, :cond_2

    .line 550
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p3, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartPageIndex()I

    move-result v3

    .line 546
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static buildStartPositionToSegmentIndex(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/TreeMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;)",
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 490
    .local p0, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .local p1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 492
    .local v2, "result":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 493
    .local v1, "index":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    .line 494
    .local v3, "segment":Lcom/google/android/apps/books/model/Segment;
    new-instance v4, Lcom/google/android/apps/books/common/Position;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getStartPosition()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    add-int/lit8 v1, v1, 0x1

    .line 496
    goto :goto_0

    .line 498
    .end local v3    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_0
    return-object v2
.end method

.method private static buildStartSegmentIndexToChapterIndex(Ljava/util/List;)Ljava/util/TreeMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Chapter;",
            ">;)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 523
    .local p0, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v3

    .line 524
    .local v3, "result":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 525
    .local v1, "chapterIndex":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/Chapter;

    .line 526
    .local v0, "chapter":Lcom/google/android/apps/books/model/Chapter;
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Chapter;->getStartSegmentIndex()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    add-int/lit8 v1, v1, 0x1

    .line 528
    goto :goto_0

    .line 529
    .end local v0    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    :cond_0
    return-object v3
.end method

.method public static from(Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Map;I)Lcom/google/android/apps/books/model/TextContentMetadata;
    .locals 3
    .param p0, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p2, "maxSegmentsPerPassage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;I)",
            "Lcom/google/android/apps/books/model/TextContentMetadata;"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "segmentIdToCssIndices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/Integer;>;>;"
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getIdToIndex()Ljava/util/Map;

    move-result-object v0

    .line 185
    .local v0, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getIdToIndex()Ljava/util/Map;

    move-result-object v1

    .line 186
    .local v1, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {p0, v0, v1, p1, p2}, Lcom/google/android/apps/books/model/TextContentMetadata;->from(Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;I)Lcom/google/android/apps/books/model/TextContentMetadata;

    move-result-object v2

    return-object v2
.end method

.method public static from(Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;I)Lcom/google/android/apps/books/model/TextContentMetadata;
    .locals 17
    .param p0, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p4, "maxSegmentsPerPassage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;I)",
            "Lcom/google/android/apps/books/model/TextContentMetadata;"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p2, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p3, "segmentIdToCssIndices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Collection<Ljava/lang/Integer;>;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v2

    .line 201
    .local v2, "startSegmentIndexToPassageIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/Identifiables;->buildIdToIndex(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v8

    .line 204
    .local v8, "chapterIdToChapterIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/model/TextContentMetadata;->buildStartSegmentIndexToChapterIndex(Ljava/util/List;)Ljava/util/TreeMap;

    move-result-object v4

    .line 207
    .local v4, "startSegmentIndexToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    new-instance v15, Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;

    invoke-direct {v15, v8}, Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;-><init>(Ljava/util/Map;)V

    .line 209
    .local v15, "chapterIdComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    invoke-static {v15}, Lcom/google/common/collect/Maps;->newTreeMap(Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v7

    .line 211
    .local v7, "chapterIdToStartPageIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v6

    .line 219
    .local v6, "startPageIndexToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0, v6, v7}, Lcom/google/android/apps/books/model/TextContentMetadata;->buildStartPageIdToChapterIndex(Ljava/util/List;Ljava/util/Map;Ljava/util/TreeMap;Ljava/util/TreeMap;)V

    .line 222
    new-instance v5, Lcom/google/android/apps/books/model/SimplePageOrdering;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/google/android/apps/books/model/SimplePageOrdering;-><init>(Ljava/util/Map;)V

    .line 223
    .local v5, "pageOrdering":Lcom/google/android/apps/books/common/Position$PageOrdering;
    invoke-static {v5}, Lcom/google/android/apps/books/common/Position;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;

    move-result-object v16

    .line 225
    .local v16, "positionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/google/android/apps/books/common/Position;>;"
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->buildStartPositionToSegmentIndex(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v3

    .line 229
    .local v3, "startPositionToSegmentIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->buildReadingPositionToChapterIndex(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/TreeMap;

    move-result-object v14

    .line 232
    .local v14, "readingPositionToChapterIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    new-instance v1, Lcom/google/android/apps/books/model/TextContentMetadata;

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p0

    move-object/from16 v12, p3

    move/from16 v13, p4

    invoke-direct/range {v1 .. v14}, Lcom/google/android/apps/books/model/TextContentMetadata;-><init>(Ljava/util/TreeMap;Ljava/util/TreeMap;Ljava/util/TreeMap;Lcom/google/android/apps/books/common/Position$PageOrdering;Ljava/util/TreeMap;Ljava/util/TreeMap;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/Map;ILjava/util/TreeMap;)V

    return-object v1
.end method

.method private static getSegmentIndexForPosition(Ljava/util/TreeMap;Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;
    .locals 2
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/books/common/Position;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 364
    .local p0, "startPositionToSegmentIndex":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    invoke-virtual {p0, p1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 365
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 366
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 368
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getChapterIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "chapterId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 573
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mChapterIdToChapterIndex:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 574
    .local v0, "result":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 575
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 577
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No such chapter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getChapterIndexForPageId(Ljava/lang/String;)I
    .locals 3
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 320
    iget-object v2, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPageIdToPageIndex:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 321
    .local v1, "pageIndex":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 322
    iget-object v2, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartPageIndexToChapterIndex:Ljava/util/TreeMap;

    invoke-virtual {v2, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 324
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 325
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 328
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 283
    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mReadingPositionToChapterIndex:Ljava/util/TreeMap;

    invoke-virtual {v3}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 284
    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mReadingPositionToChapterIndex:Ljava/util/TreeMap;

    invoke-virtual {v3, p1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 286
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    if-eqz v1, :cond_1

    .line 287
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 302
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/apps/books/common/Position;Ljava/lang/Integer;>;"
    :goto_0
    return v3

    .line 292
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartPositionToSegmentIndex:Ljava/util/TreeMap;

    invoke-static {v3, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIndexForPosition(Ljava/util/TreeMap;Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;

    move-result-object v2

    .line 294
    .local v2, "segmentIndex":Ljava/lang/Integer;
    if-eqz v2, :cond_1

    .line 295
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/TextContentMetadata;->getChapterIndexForSegmentIndex(I)Ljava/lang/Integer;

    move-result-object v0

    .line 296
    .local v0, "chapterIndexObj":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    .line 297
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_0

    .line 302
    .end local v0    # "chapterIndexObj":Ljava/lang/Integer;
    .end local v2    # "segmentIndex":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/TextContentMetadata;->getChapterIndexForPageId(Ljava/lang/String;)I

    move-result v3

    goto :goto_0
.end method

.method public getChapterIndexForSegmentIndex(I)Ljava/lang/Integer;
    .locals 3
    .param p1, "segmentIndex"    # I

    .prologue
    .line 306
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartSegmentIndexToChapterIndex:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 308
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 309
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 311
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFirstForbiddenPassageIndex()I
    .locals 3

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageCount()I

    move-result v0

    .line 387
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 388
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/model/TextContentMetadata;->isPassageViewable(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 393
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 387
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    .line 393
    goto :goto_1
.end method

.method public getNextPageIndexInDifferentChapter(I)I
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 687
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartPageIndexToChapterIndex:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 688
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 689
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 691
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method

.method public getNextSegmentIndexInDifferentChapter(I)I
    .locals 3
    .param p1, "segmentIndex"    # I

    .prologue
    .line 673
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartSegmentIndexToChapterIndex:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 675
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 676
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 678
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method

.method public getPageIndex(Ljava/lang/String;)I
    .locals 1
    .param p1, "pageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/common/Position$PageOrdering;->getPageIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getPassageCount()I
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPassageCssJsonArray()Lorg/json/JSONArray;
    .locals 5

    .prologue
    .line 616
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 617
    .local v2, "result":Lorg/json/JSONArray;
    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    .line 618
    .local v1, "passage":Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    new-instance v3, Lorg/json/JSONArray;

    # invokes: Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getCssIndices()Ljava/util/Collection;
    invoke-static {v1}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->access$300(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 620
    .end local v1    # "passage":Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    :cond_0
    return-object v2
.end method

.method public getPassageIndexForPosition(Lcom/google/android/apps/books/common/Position;)I
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    const/4 v2, 0x0

    .line 341
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;

    move-result-object v1

    .line 342
    .local v1, "segmentIndex":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartSegmentIndexToPassageIndex:Ljava/util/TreeMap;

    invoke-virtual {v3, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 345
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 346
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 351
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_0
    return v2
.end method

.method public getPassageIndexForSegmentId(Ljava/lang/String;)I
    .locals 2
    .param p1, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 639
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegmentIdToSegmentIndex:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 640
    .local v0, "segmentIndex":I
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageIndexForSegmentIndex(I)I

    move-result v1

    return v1
.end method

.method public getPassageIndexForSegmentIndex(I)I
    .locals 3
    .param p1, "segmentIndex"    # I

    .prologue
    .line 648
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartSegmentIndexToPassageIndex:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 650
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 651
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 653
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPassageSegmentIndices(I)Ljava/util/List;
    .locals 1
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    # invokes: Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getSegmentIndices()Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->access$200(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/util/List;

    move-result-object v0

    .line 591
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getPassageSegmentJsonArray()Lorg/json/JSONArray;
    .locals 9

    .prologue
    .line 599
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 600
    .local v3, "result":Lorg/json/JSONArray;
    iget-object v7, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    .line 601
    .local v2, "passage":Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 602
    .local v6, "segmentsJson":Lorg/json/JSONArray;
    # invokes: Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getSegmentIndices()Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->access$200(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 603
    .local v5, "segmentIndex":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegments:Ljava/util/List;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/Segment;

    .line 604
    .local v4, "segment":Lcom/google/android/apps/books/model/Segment;
    invoke-interface {v4}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 606
    .end local v4    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v5    # "segmentIndex":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 608
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "passage":Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
    .end local v6    # "segmentsJson":Lorg/json/JSONArray;
    :cond_1
    return-object v3
.end method

.method public getPassageStartPosition(I)Ljava/lang/String;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 628
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    # invokes: Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getStartPosition()Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->access$400(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/lang/String;

    move-result-object v0

    .line 631
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSegmentIdToCssIndices()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegmentIdToCssIndices:Ljava/util/Map;

    return-object v0
.end method

.method public getSegmentIndexForPosition(Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mStartPositionToSegmentIndex:Ljava/util/TreeMap;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/model/TextContentMetadata;->getSegmentIndexForPosition(Ljava/util/TreeMap;Lcom/google/android/apps/books/common/Position;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSegmentIndexForSegmentId(Ljava/lang/String;)I
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 660
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mSegmentIdToSegmentIndex:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 661
    .local v0, "result":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 664
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad segment ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getStartPageIndex(Ljava/lang/String;)I
    .locals 4
    .param p1, "chapterId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 562
    iget-object v1, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mChapterIdToStartPageIndex:Ljava/util/TreeMap;

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 563
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 564
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 566
    :cond_0
    new-instance v1, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No such chapter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isPassageViewable(I)Z
    .locals 3
    .param p1, "passageIndex"    # I

    .prologue
    .line 375
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/TextContentMetadata;->getPassageCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 376
    :cond_0
    const-string v0, "TextContentMetadata"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    const-string v0, "TextContentMetadata"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPassageViewable called with out of bounds passageIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_1
    const/4 v0, 0x0

    .line 382
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata;->mPassages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    # invokes: Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->isViewable()Z
    invoke-static {v0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->access$000(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Z

    move-result v0

    goto :goto_0
.end method
