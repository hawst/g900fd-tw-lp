.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;
.super Ljava/lang/Object;
.source "BooksCardsHomeView.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActionText(Lcom/google/android/ublib/cardlib/model/Document;)Ljava/lang/String;
    .locals 3
    .param p1, "doc"    # Lcom/google/android/ublib/cardlib/model/Document;

    .prologue
    .line 240
    check-cast p1, Lcom/google/android/apps/books/playcards/BookDocument;

    .end local p1    # "doc":Lcom/google/android/ublib/cardlib/model/Document;
    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getActionTextId()Ljava/lang/Integer;

    move-result-object v0

    .line 242
    .local v0, "actionTextId":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    iget-object v1, v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 246
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performAction(Lcom/google/android/ublib/cardlib/model/Document;)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/ublib/cardlib/model/Document;

    .prologue
    .line 223
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/books/playcards/BookDocument;

    .line 224
    .local v0, "bookDoc":Lcom/google/android/apps/books/playcards/BookDocument;
    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$4;->$SwitchMap$com$google$android$apps$books$playcards$BookDocument$Saleability:[I

    invoke-virtual {v0}, Lcom/google/android/apps/books/playcards/BookDocument;->getSaleability()Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 236
    :goto_0
    return-void

    .line 229
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->viewRecommendedSample(Ljava/lang/String;)V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$3;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/widget/BooksHomeController;->viewStoreLinkForRecommendedBook(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
