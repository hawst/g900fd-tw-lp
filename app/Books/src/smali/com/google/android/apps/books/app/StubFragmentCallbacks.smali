.class public Lcom/google/android/apps/books/app/StubFragmentCallbacks;
.super Ljava/lang/Object;
.source "StubFragmentCallbacks.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addFragment(Landroid/support/v4/app/Fragment;)V
    .locals 0
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 44
    return-void
.end method

.method public addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p2, "args"    # Landroid/os/Bundle;
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/support/v4/app/Fragment;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 114
    return-void
.end method

.method public addVolumeToMyEBooks(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "prompt"    # Z

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 99
    return-void
.end method

.method public authenticationFinished(Landroid/content/Intent;Ljava/lang/Exception;I)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "exception"    # Ljava/lang/Exception;
    .param p3, "requestId"    # I

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 89
    return-void
.end method

.method public canStartAboutVolume(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "canonicalUrl"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public getActionBar()Landroid/support/v7/app/ActionBar;
    .locals 1

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/apps/books/app/StubActionBar;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/StubActionBar;-><init>()V

    return-object v0
.end method

.method public getSystemUi()Lcom/google/android/ublib/view/SystemUi;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 139
    const/4 v0, 0x0

    return-object v0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method protected maybeLogMethodName()V
    .locals 5

    .prologue
    .line 24
    const-string v2, "StubScene"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    .line 26
    .local v1, "t":Ljava/lang/Throwable;
    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v0, v2, v3

    .line 27
    .local v0, "method":Ljava/lang/StackTraceElement;
    const-string v2, "StubScene"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "called stub "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    .end local v0    # "method":Ljava/lang/StackTraceElement;
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_0
    return-void
.end method

.method public moveToReader(Ljava/lang/String;Lcom/google/android/apps/books/app/BookOpeningFlags;Landroid/view/View;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "flags"    # Lcom/google/android/apps/books/app/BookOpeningFlags;
    .param p3, "coverView"    # Landroid/view/View;

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 134
    return-void
.end method

.method public onExternalStorageException()V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 109
    return-void
.end method

.method public startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "canonicalUrl"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 54
    return-void
.end method

.method public startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "buyUrl"    # Ljava/lang/String;
    .param p3, "directPurchaseAPI"    # Z
    .param p4, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 64
    return-void
.end method

.method public startForcedSync()V
    .locals 0

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 119
    return-void
.end method

.method public startHelpActivity(Ljava/lang/String;Landroid/accounts/Account;Landroid/app/Activity;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "helpContext"    # Ljava/lang/String;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "screenshot"    # Landroid/graphics/Bitmap;

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 146
    return-void
.end method

.method public startSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 74
    return-void
.end method

.method public startShare(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/net/Uri;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "author"    # Ljava/lang/CharSequence;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 104
    return-void
.end method

.method public startShop(Ljava/lang/String;)V
    .locals 0
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 69
    return-void
.end method

.method public startViewCollection(Ljava/lang/String;)V
    .locals 0
    .param p1, "viewUrl"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/StubFragmentCallbacks;->maybeLogMethodName()V

    .line 129
    return-void
.end method
