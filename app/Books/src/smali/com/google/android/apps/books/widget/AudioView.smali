.class public Lcom/google/android/apps/books/widget/AudioView;
.super Landroid/widget/FrameLayout;
.source "AudioView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcom/google/android/apps/books/util/MediaUrlFetcher$MediaFoundListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;
    }
.end annotation


# instance fields
.field private mAudioControlsLayout:Landroid/view/View;

.field private mAudioDuration:I

.field private mAudioViewListener:Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;

.field private mId:Ljava/lang/String;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMediaUrlFetcher:Lcom/google/android/apps/books/util/MediaUrlFetcher;

.field private final mPauseButtonDrawable:Landroid/graphics/drawable/Drawable;

.field private final mPlayButtonDrawable:Landroid/graphics/drawable/Drawable;

.field private mPlayPauseButton:Landroid/widget/ImageButton;

.field private mPlaybackCanceled:Z

.field private final mProgressUpdater:Ljava/lang/Runnable;

.field private mRewindButton:Landroid/widget/ImageButton;

.field private mScrubber:Landroid/widget/SeekBar;

.field private mSpinnerLayout:Landroid/view/View;

.field private mStartButton:Landroid/widget/ImageButton;

.field private mStartButtonLayout:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/AudioView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/AudioView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    .line 66
    new-instance v1, Lcom/google/android/apps/books/widget/AudioView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/AudioView$1;-><init>(Lcom/google/android/apps/books/widget/AudioView;)V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mProgressUpdater:Ljava/lang/Runnable;

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AudioView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 98
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPauseButtonDrawable:Landroid/graphics/drawable/Drawable;

    .line 99
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayButtonDrawable:Landroid/graphics/drawable/Drawable;

    .line 100
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/AudioView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/AudioView;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updateProgress()V

    return-void
.end method

.method private onPlaybackStarted()V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioViewListener:Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioViewListener:Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;->onAudioStartPlaying(Ljava/lang/String;)V

    .line 389
    :cond_0
    return-void
.end method

.method private onPlaybackStopped()V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioViewListener:Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioViewListener:Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;->onAudioStopPlaying(Ljava/lang/String;)V

    .line 395
    :cond_0
    return-void
.end method

.method private playPauseButtonTapped()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    if-gez v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mProgressUpdater:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 236
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->onPlaybackStopped()V

    .line 242
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updatePlayPauseButton()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updateProgress()V

    goto :goto_0

    .line 238
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 239
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->onPlaybackStarted()V

    goto :goto_1
.end method

.method private rewindButtonTapped()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    if-gez v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 252
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updatePlayPauseButton()V

    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updateProgress()V

    goto :goto_0
.end method

.method private startButtonTapped()V
    .locals 0

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AudioView;->startPlaying()V

    .line 225
    return-void
.end method

.method private startStreaming()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButtonLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mSpinnerLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 162
    return-void
.end method

.method private updatePlayPauseButton()V
    .locals 3

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPauseButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AudioView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f01ce

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayButtonDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AudioView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f01cd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateProgress()V
    .locals 4

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mScrubber:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mProgressUpdater:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/books/widget/AudioView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 264
    :cond_0
    return-void
.end method


# virtual methods
.method public getMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 337
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->playPauseButtonTapped()V

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mRewindButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 339
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->rewindButtonTapped()V

    goto :goto_0

    .line 340
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->startButtonTapped()V

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v1, 0x0

    .line 282
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mScrubber:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 285
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updatePlayPauseButton()V

    .line 287
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->onPlaybackStopped()V

    .line 288
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->onPlaybackStopped()V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 175
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mProgressUpdater:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    .line 180
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v6, 0x1

    .line 297
    const-string v1, "AudioView"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    const-string v1, "AudioView"

    const-string v2, "onError, what=%d, extra=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    if-eqz v1, :cond_1

    .line 311
    :goto_0
    return v6

    .line 304
    :cond_1
    if-ne p2, v6, :cond_2

    const/16 v1, -0x3ec

    if-ne p3, v1, :cond_2

    .line 306
    const v0, 0x7f0f014a

    .line 310
    .local v0, "errorMessageId":I
    :goto_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AudioView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 308
    .end local v0    # "errorMessageId":I
    :cond_2
    const v0, 0x7f0f014b

    .restart local v0    # "errorMessageId":I
    goto :goto_1
.end method

.method public onFileFound(Ljava/io/FileDescriptor;)V
    .locals 3
    .param p1, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 365
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    if-nez v1, :cond_0

    .line 366
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 368
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->startStreaming()V

    .line 377
    :cond_0
    return-void

    .line 369
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "AudioView"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 371
    const-string v1, "AudioView"

    const-string v2, "Couldn\'t open cached file for playback."

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 373
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 107
    const v0, 0x7f0e00be

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mScrubber:Landroid/widget/SeekBar;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mScrubber:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 110
    const v0, 0x7f0e00b9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mSpinnerLayout:Landroid/view/View;

    .line 111
    const v0, 0x7f0e00ba

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButtonLayout:Landroid/view/View;

    .line 112
    const v0, 0x7f0e00bc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioControlsLayout:Landroid/view/View;

    .line 114
    const v0, 0x7f0e00bb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButton:Landroid/widget/ImageButton;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    const v0, 0x7f0e00bd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v0, 0x7f0e00bf

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/AudioView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mRewindButton:Landroid/widget/ImageButton;

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mRewindButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButtonLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    if-eqz v0, :cond_0

    .line 278
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mScrubber:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mSpinnerLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioControlsLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 277
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->updateProgress()V

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 326
    if-eqz p3, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioDuration:I

    if-gez v0, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mScrubber:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 322
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 317
    return-void
.end method

.method public onUrlFetchSucceeded(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 349
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    if-nez v1, :cond_0

    .line 350
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 352
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->startStreaming()V

    .line 361
    :cond_0
    return-void

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "AudioView"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 355
    const-string v1, "AudioView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t fetch stream "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for playback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 357
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method public onUrlFetchedError()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mSpinnerLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButtonLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 383
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/apps/books/widget/AudioView;->mAudioViewListener:Lcom/google/android/apps/books/widget/AudioView$AudioViewListener;

    .line 133
    iput-object p2, p0, Lcom/google/android/apps/books/widget/AudioView;->mId:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setMediaUrlFetcher(Lcom/google/android/apps/books/util/MediaUrlFetcher;)V
    .locals 0
    .param p1, "fetcher"    # Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaUrlFetcher:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    .line 149
    return-void
.end method

.method public startPlaying()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaUrlFetcher:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    if-nez v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/AudioView;->onPlaybackStarted()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mStartButtonLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mSpinnerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/AudioView;->invalidate()V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/widget/AudioView;->mMediaUrlFetcher:Lcom/google/android/apps/books/util/MediaUrlFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/MediaUrlFetcher;->fetchMedia()V

    .line 215
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/AudioView;->mPlaybackCanceled:Z

    goto :goto_0
.end method
