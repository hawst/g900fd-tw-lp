.class public Lcom/google/android/apps/books/widget/KeyboardAwareEditText;
.super Landroid/widget/EditText;
.source "KeyboardAwareEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;
    }
.end annotation


# instance fields
.field final mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mKeyboardListener:Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 21
    return-void
.end method


# virtual methods
.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->mKeyboardListener:Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->mKeyboardListener:Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;->onKeyboardDismissed(Lcom/google/android/apps/books/widget/KeyboardAwareEditText;)V

    .line 33
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setKeyboardListener(Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;)V
    .locals 0
    .param p1, "keyboardListener"    # Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->mKeyboardListener:Lcom/google/android/apps/books/widget/KeyboardAwareEditText$KeyboardListener;

    .line 25
    return-void
.end method
