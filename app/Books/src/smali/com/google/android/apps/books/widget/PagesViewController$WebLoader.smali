.class public interface abstract Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WebLoader"
.end annotation


# virtual methods
.method public abstract loadHighlightRectsForQuery(Ljava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)I
.end method

.method public abstract loadRangeDataBulk(ILjava/util/Map;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract weaklyAddWebLoadListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V
.end method
