.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;
.super Lcom/google/android/libraries/bind/data/DataObserver;
.source "OnboardSampleQuizPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/DataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged(Lcom/google/android/libraries/bind/data/DataChange;)V
    .locals 3
    .param p1, "change"    # Lcom/google/android/libraries/bind/data/DataChange;

    .prologue
    .line 208
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$700(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Z

    move-result v0

    .line 209
    .local v0, "oldShowDoneValue":Z
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # invokes: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getSelectedItemIds()Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$800(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    # setter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z
    invoke-static {v2, v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$702(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Z)Z

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mShowDone:Z
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$700(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$1;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$900(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/play/onboard/OnboardHostControl;->invalidateControls()V

    .line 213
    :cond_0
    return-void

    .line 209
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
