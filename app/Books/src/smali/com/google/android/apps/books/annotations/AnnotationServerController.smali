.class public Lcom/google/android/apps/books/annotations/AnnotationServerController;
.super Ljava/lang/Object;
.source "AnnotationServerController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;,
        Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;
    }
.end annotation


# instance fields
.field private final mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationServer;)V
    .locals 0
    .param p1, "server"    # Lcom/google/android/apps/books/annotations/AnnotationServer;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/annotations/AnnotationServerController;)Lcom/google/android/apps/books/annotations/AnnotationServer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationServerController;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    return-object v0
.end method

.method private characterQuotaFromJsonAnnotation(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .locals 2
    .param p1, "jsonAnnotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 156
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerSummary:Lcom/google/android/apps/books/annotations/data/JsonLayer;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerSummary:Lcom/google/android/apps/books/annotations/data/JsonLayer;

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/books/annotations/data/JsonLayer;->layerId:Ljava/lang/String;

    .line 159
    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerSummary:Lcom/google/android/apps/books/annotations/data/JsonLayer;

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->fromJson(Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createAdditionReceipt(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;
    .locals 2
    .param p1, "addAnnotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->createReceipt(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    move-result-object v0

    .line 65
    .local v0, "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    new-instance v1, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;-><init>(Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)V

    return-object v1
.end method

.method private createReceipt(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    .locals 5
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->serverId:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->getTimestamp(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)J

    move-result-wide v2

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->characterQuotaFromJsonAnnotation(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;-><init>(Ljava/lang/String;JLcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    return-object v0
.end method

.method private deliverAnnotations(Ljava/util/List;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V
    .locals 6
    .param p2, "processor"    # Lcom/google/android/apps/books/annotations/AnnotationProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;",
            "Lcom/google/android/apps/books/annotations/AnnotationProcessor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/data/JsonAnnotation;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .line 179
    .local v2, "jsonAnnotation":Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->hasLayerSummary(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 181
    iget-object v4, v2, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerId:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->characterQuotaFromJsonAnnotation(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Lcom/google/android/apps/books/annotations/AnnotationProcessor;->updateCharacterQuota(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)V

    goto :goto_0

    .line 184
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->createReceipt(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    move-result-object v3

    .line 186
    .local v3, "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    iget-boolean v4, v2, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->isDeleted:Z

    if-eqz v4, :cond_1

    .line 187
    iget-object v4, v3, Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;->serverId:Ljava/lang/String;

    invoke-interface {p2, v4}, Lcom/google/android/apps/books/annotations/AnnotationProcessor;->delete(Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_1
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/JsonAnnotations;->createAnnotationFromJsonAnnotation(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 191
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    new-instance v4, Lcom/google/android/apps/books/annotations/ServerAnnotation;

    invoke-direct {v4, v0, v3}, Lcom/google/android/apps/books/annotations/ServerAnnotation;-><init>(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;)V

    invoke-interface {p2, v4}, Lcom/google/android/apps/books/annotations/AnnotationProcessor;->add(Lcom/google/android/apps/books/annotations/ServerAnnotation;)V

    goto :goto_0

    .line 195
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v2    # "jsonAnnotation":Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    .end local v3    # "receipt":Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    :cond_2
    return-void
.end method

.method private deliverPagedAnnotations(Lcom/google/android/apps/books/annotations/AnnotationProcessor;Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;)V
    .locals 3
    .param p1, "processor"    # Lcom/google/android/apps/books/annotations/AnnotationProcessor;
    .param p2, "source"    # Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 146
    const/4 v0, 0x0

    .line 148
    .local v0, "continuation":Ljava/lang/String;
    :cond_0
    invoke-interface {p2, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;->getPage(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v1

    .line 149
    .local v1, "resultPage":Lcom/google/android/apps/books/annotations/PaginatedResponse;, "Lcom/google/android/apps/books/annotations/PaginatedResponse<Lcom/google/android/apps/books/annotations/data/JsonAnnotation;>;"
    iget-object v2, v1, Lcom/google/android/apps/books/annotations/PaginatedResponse;->items:Ljava/util/List;

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->deliverAnnotations(Ljava/util/List;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V

    .line 150
    iget-object v0, v1, Lcom/google/android/apps/books/annotations/PaginatedResponse;->continuation:Ljava/lang/String;

    .line 151
    if-nez v0, :cond_0

    .line 152
    invoke-interface {p1}, Lcom/google/android/apps/books/annotations/AnnotationProcessor;->done()V

    .line 153
    return-void
.end method

.method private hasLayerSummary(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Z
    .locals 2
    .param p1, "jsonAnnotation"    # Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    .prologue
    .line 173
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/books/annotations/data/JsonAnnotation;->layerSummary:Lcom/google/android/apps/books/annotations/data/JsonLayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "a"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationServer;->addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->createAdditionReceipt(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationAdditionReceipt;

    move-result-object v0

    return-object v0
.end method

.method public deleteAnnotation(Ljava/lang/String;)V
    .locals 1
    .param p1, "serverId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/annotations/AnnotationServer;->deleteAnnotation(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public downloadAnnotationDatas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Collection;Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;II)V
    .locals 28
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p3, "consumer"    # Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;
    .param p4, "imageW"    # I
    .param p5, "imageH"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            ">;",
            "Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p2, "keys":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/AnnotationData$Key;>;"
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v13

    .line 221
    .local v13, "loadedIds":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/AnnotationData$Key;>;"
    const/16 v16, 0x0

    .line 222
    .local v16, "responseBody":Ljava/io/InputStream;
    const/4 v8, 0x0

    .line 225
    .local v8, "exception":Ljava/lang/Exception;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/annotations/AnnotationServer;->getAnnotationDatas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Collection;II)Ljava/io/InputStream;

    move-result-object v16

    .line 226
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v18

    .line 227
    .local v18, "start":J
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseApiaryListResponse(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v10

    .line 229
    .local v10, "itemJsons":Ljava/util/List;, "Ljava/util/List<Lorg/codehaus/jackson/JsonNode;>;"
    const-string v21, "AnnotationServerC"

    const/16 v22, 0x3

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 230
    const-string v21, "AnnotationServerC"

    const-string v22, "Time to parse annotation datas: %dms"

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    sub-long v26, v26, v18

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_0
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v21

    if-eqz v21, :cond_4

    const/16 v17, 0x0

    .line 242
    .local v17, "userLocale":Ljava/lang/String;
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v21

    if-eqz v21, :cond_5

    const/4 v7, 0x0

    .line 245
    .local v7, "entityLocale":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v20

    .line 246
    .local v20, "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v11

    .line 247
    .local v11, "jsons":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lorg/codehaus/jackson/JsonNode;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/codehaus/jackson/JsonNode;

    .line 248
    .local v14, "node":Lorg/codehaus/jackson/JsonNode;
    move-object/from16 v0, v17

    invoke-static {v14, v0, v7}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAnnotationData(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationData;

    move-result-object v15

    .line 250
    .local v15, "parsedItem":Lcom/google/android/apps/books/annotations/AnnotationData;
    iget-object v12, v15, Lcom/google/android/apps/books/annotations/AnnotationData;->key:Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 251
    .local v12, "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    invoke-interface {v13, v12}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-static {v15}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    invoke-interface {v11, v12, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 257
    .end local v7    # "entityLocale":Ljava/lang/String;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "itemJsons":Ljava/util/List;, "Ljava/util/List<Lorg/codehaus/jackson/JsonNode;>;"
    .end local v11    # "jsons":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lorg/codehaus/jackson/JsonNode;>;"
    .end local v12    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .end local v14    # "node":Lorg/codehaus/jackson/JsonNode;
    .end local v15    # "parsedItem":Lcom/google/android/apps/books/annotations/AnnotationData;
    .end local v17    # "userLocale":Ljava/lang/String;
    .end local v18    # "start":J
    .end local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    :catch_0
    move-exception v6

    .line 258
    .local v6, "e":Ljava/io/IOException;
    :try_start_1
    const-string v21, "AnnotationServerC"

    const/16 v22, 0x6

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 259
    const-string v21, "AnnotationServerC"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Error loading annotation data: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    :cond_1
    move-object v8, v6

    .line 271
    if-eqz v16, :cond_2

    .line 273
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 280
    .end local v6    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    invoke-interface {v13}, Ljava/util/Collection;->size()I

    move-result v21

    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->size()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 281
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v20

    .line 282
    .restart local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    .line 283
    .restart local v12    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    invoke-interface {v13, v12}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_3

    .line 284
    invoke-static {v8}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 240
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v12    # "key":Lcom/google/android/apps/books/annotations/AnnotationData$Key;
    .end local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    .restart local v10    # "itemJsons":Ljava/util/List;, "Ljava/util/List<Lorg/codehaus/jackson/JsonNode;>;"
    .restart local v18    # "start":J
    :cond_4
    :try_start_3
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->getUserLocaleString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_0

    .line 242
    .restart local v17    # "userLocale":Ljava/lang/String;
    :cond_5
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;->getEntityLocaleString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 255
    .restart local v7    # "entityLocale":Ljava/lang/String;
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v11    # "jsons":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lorg/codehaus/jackson/JsonNode;>;"
    .restart local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    :cond_6
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v11}, Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;->onAnnotationDatas(Ljava/util/Map;Ljava/util/Map;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 271
    if-eqz v16, :cond_2

    .line 273
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    .line 274
    :catch_1
    move-exception v21

    goto :goto_3

    .line 262
    .end local v7    # "entityLocale":Ljava/lang/String;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "itemJsons":Ljava/util/List;, "Ljava/util/List<Lorg/codehaus/jackson/JsonNode;>;"
    .end local v11    # "jsons":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lorg/codehaus/jackson/JsonNode;>;"
    .end local v17    # "userLocale":Ljava/lang/String;
    .end local v18    # "start":J
    .end local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    :catch_2
    move-exception v6

    .line 266
    .local v6, "e":Ljava/lang/NullPointerException;
    :try_start_5
    const-string v21, "AnnotationServerC"

    const/16 v22, 0x6

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 267
    const-string v21, "AnnotationServerC"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Error loading annotation data: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 269
    :cond_7
    move-object v8, v6

    .line 271
    if-eqz v16, :cond_2

    .line 273
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_3

    .line 274
    :catch_3
    move-exception v21

    goto/16 :goto_3

    .line 271
    .end local v6    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v21

    if-eqz v16, :cond_8

    .line 273
    :try_start_7
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 275
    :cond_8
    :goto_5
    throw v21

    .line 287
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    :cond_9
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v21

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/annotations/AnnotationServerController$AnnotationDataProcessor;->onAnnotationDatas(Ljava/util/Map;Ljava/util/Map;)V

    .line 289
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v20    # "values":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/AnnotationData;>;>;"
    :cond_a
    return-void

    .line 274
    .local v6, "e":Ljava/io/IOException;
    :catch_4
    move-exception v21

    goto/16 :goto_3

    .end local v6    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v22

    goto :goto_5
.end method

.method public downloadUserAnnotations(Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;
    .param p2, "processor"    # Lcom/google/android/apps/books/annotations/AnnotationProcessor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationServerController$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationServerController;Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->deliverPagedAnnotations(Lcom/google/android/apps/books/annotations/AnnotationProcessor;Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;)V

    .line 117
    return-void
.end method

.method public downloadVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;
    .param p2, "layerVersion"    # Ljava/lang/String;
    .param p3, "processor"    # Lcom/google/android/apps/books/annotations/AnnotationProcessor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;-><init>(Lcom/google/android/apps/books/annotations/AnnotationServerController;Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)V

    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->deliverPagedAnnotations(Lcom/google/android/apps/books/annotations/AnnotationProcessor;Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;)V

    .line 136
    return-void
.end method

.method public editAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;
    .locals 1
    .param p1, "volumeVersion"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p2, "serverId"    # Ljava/lang/String;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/annotations/AnnotationServer;->editAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->createReceipt(Lcom/google/android/apps/books/annotations/data/JsonAnnotation;)Lcom/google/android/apps/books/annotations/ServerAnnotationReceipt;

    move-result-object v0

    return-object v0
.end method

.method public getLayerCharacterQuota(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 303
    .local p2, "layerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationServer;->getCharacterQuotas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getVolumeLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;)Ljava/util/List;
    .locals 1
    .param p1, "vv"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/annotations/AnnotationServer;->getVolumeLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
