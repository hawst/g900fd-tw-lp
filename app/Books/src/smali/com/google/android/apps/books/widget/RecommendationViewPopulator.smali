.class public Lcom/google/android/apps/books/widget/RecommendationViewPopulator;
.super Ljava/lang/Object;
.source "RecommendationViewPopulator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/RecommendationViewPopulator$OnStoreLinkClickListener;
    }
.end annotation


# instance fields
.field private final mBookCampaignId:Ljava/lang/String;

.field private final mImageListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

.field private final mImageManager:Lcom/google/android/apps/books/common/ImageManager;

.field private final mMenuHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/ImageManager;Ljava/lang/String;Lcom/google/android/apps/books/widget/RecommendationViewPopulator$OnStoreLinkClickListener;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;)V
    .locals 1
    .param p1, "imageManager"    # Lcom/google/android/apps/books/common/ImageManager;
    .param p2, "bookCampaignId"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/google/android/apps/books/widget/RecommendationViewPopulator$OnStoreLinkClickListener;
    .param p4, "handlerFactory"    # Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;
    .param p5, "imageListener"    # Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mBookCampaignId:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mImageListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    .line 39
    iput-object p4, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mMenuHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    .line 40
    new-instance v0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$1;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$1;-><init>(Lcom/google/android/apps/books/widget/RecommendationViewPopulator;Lcom/google/android/apps/books/widget/RecommendationViewPopulator$OnStoreLinkClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 46
    return-void
.end method


# virtual methods
.method public fillView(Lcom/google/android/apps/books/widget/BooksListItemView;Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;ZLcom/google/android/apps/books/app/BooksFragmentCallbacks;)V
    .locals 7
    .param p1, "view"    # Lcom/google/android/apps/books/widget/BooksListItemView;
    .param p2, "book"    # Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    .param p3, "showReasons"    # Z
    .param p4, "callbacks"    # Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    .prologue
    .line 54
    iget-object v4, p2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->coverUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mImageListener:Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;

    invoke-virtual {p1, v4, v5, v6}, Lcom/google/android/apps/books/widget/BooksListItemView;->setupCoverView(Landroid/net/Uri;Lcom/google/android/apps/books/common/ImageManager;Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;)V

    .line 56
    new-instance v3, Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/BooksListItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, p2, v4}, Lcom/google/android/apps/books/playcards/BookDocument;-><init>(Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;Landroid/content/Context;)V

    .line 57
    .local v3, "bookDoc":Lcom/google/android/apps/books/playcards/BookDocument;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mMenuHandlerFactory:Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;

    invoke-interface {p4}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->getSystemUi()Lcom/google/android/ublib/view/SystemUi;

    move-result-object v5

    invoke-virtual {p1, v3, v4, v5}, Lcom/google/android/apps/books/widget/BooksListItemView;->setupOverflowIconView(Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/ublib/view/SystemUi;)V

    .line 58
    iget-object v4, p2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->title:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/google/android/apps/books/widget/BooksListItemView;->setTitle(Ljava/lang/String;)V

    .line 59
    iget-object v4, p2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->author:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/google/android/apps/books/widget/BooksListItemView;->setAuthor(Ljava/lang/String;)V

    .line 60
    if-eqz p3, :cond_1

    .line 61
    iget-object v4, p2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->reason:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lcom/google/android/apps/books/widget/BooksListItemView;->setReason(Ljava/lang/String;)V

    .line 66
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/BooksListItemView;->getActionTouchArea()Landroid/view/View;

    move-result-object v2

    .line 67
    .local v2, "actionTouchArea":Landroid/view/View;
    new-instance v4, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;

    invoke-direct {v4, p0, v3, p4}, Lcom/google/android/apps/books/widget/RecommendationViewPopulator$2;-><init>(Lcom/google/android/apps/books/widget/RecommendationViewPopulator;Lcom/google/android/apps/books/playcards/BookDocument;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {v3}, Lcom/google/android/apps/books/playcards/BookDocument;->getActionTextId()Ljava/lang/Integer;

    move-result-object v1

    .line 83
    .local v1, "actionTextResId":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/BooksListItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "actionText":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/BooksListItemView;->getActionView()Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 90
    .end local v0    # "actionText":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/apps/books/playcards/BookDocument;->getPrice()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/apps/books/widget/BooksListItemView;->setPrice(Ljava/lang/String;)V

    .line 92
    iget-object v4, p2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mBookCampaignId:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/books/app/StoreLink;->forBook(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/app/StoreLink;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/apps/books/widget/BooksListItemView;->setTag(Ljava/lang/Object;)V

    .line 93
    iget-object v4, p0, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v4}, Lcom/google/android/apps/books/widget/BooksListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    return-void

    .line 63
    .end local v1    # "actionTextResId":Ljava/lang/Integer;
    .end local v2    # "actionTouchArea":Landroid/view/View;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/BooksListItemView;->getReasonView()Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
