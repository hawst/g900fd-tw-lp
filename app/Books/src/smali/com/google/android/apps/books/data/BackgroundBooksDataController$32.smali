.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->uploadCollectionChanges(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/Collection;JLcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$collectionId:J

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$removedVolumeIds:Ljava/util/Collection;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-wide p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->val$collectionId:J

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->val$removedVolumeIds:Ljava/util/Collection;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-wide v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->val$collectionId:J

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->val$removedVolumeIds:Ljava/util/Collection;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$32;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->handleUploadCollectionChangesResult(JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V
    invoke-static {v0, v2, v3, v1, v4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4200(Lcom/google/android/apps/books/data/BackgroundBooksDataController;JLjava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1070
    return-void
.end method
