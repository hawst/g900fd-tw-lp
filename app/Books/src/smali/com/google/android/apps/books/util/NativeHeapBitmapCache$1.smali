.class Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;
.super Ljava/util/LinkedHashMap;
.source "NativeHeapBitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/util/NativeHeapBitmapCache;-><init>(ZILcom/google/android/apps/books/util/BitmapReusePool;Lcom/google/android/apps/books/util/BitmapCache;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<TT;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

.field final synthetic val$retainCount:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/NativeHeapBitmapCache;IFZI)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # F
    .param p4, "x2"    # Z

    .prologue
    .line 289
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache.1;"
    iput-object p1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;->this$0:Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    iput p5, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;->val$retainCount:I

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 292
    .local p0, "this":Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;, "Lcom/google/android/apps/books/util/NativeHeapBitmapCache.1;"
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TT;Ljava/lang/Boolean;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/util/NativeHeapBitmapCache$1;->val$retainCount:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
