.class final Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;
.super Ljava/lang/Object;
.source "MediaOverlaysController.java"

# interfaces
.implements Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EnsureClipsTaskCallbackImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/mo/MediaOverlaysController$1;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;-><init>(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    return-void
.end method


# virtual methods
.method public onClipsEnsured(ILjava/lang/Exception;)V
    .locals 2
    .param p1, "requestId"    # I
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 87
    if-eqz p2, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    sget-object v1, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->DOWNLOAD_ERROR:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    # invokes: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->finishSpeaking(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$000(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # getter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mCurrentReadableItemRequestId:I
    invoke-static {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$100(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->mClipsEnsured:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$202(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;Z)Z

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$EnsureClipsTaskCallbackImpl;->this$0:Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    # invokes: Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->maybeQueueClips()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->access$300(Lcom/google/android/apps/books/app/mo/MediaOverlaysController;)V

    goto :goto_0
.end method
