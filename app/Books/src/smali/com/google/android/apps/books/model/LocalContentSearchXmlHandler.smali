.class public Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
.super Lcom/google/android/apps/books/model/ContentXmlHandler;
.source "LocalContentSearchXmlHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;
    }
.end annotation


# instance fields
.field private final mProcessor:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;

.field private final mResultQueue:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;"
        }
    .end annotation
.end field

.field private final mStopParsing:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/Collection;)V
    .locals 0
    .param p1, "textProcessor"    # Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;
    .param p2, "stopParsing"    # Ljava/util/concurrent/atomic/AtomicBoolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p3, "resultQueue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/model/ContentXmlHandler;-><init>()V

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mStopParsing:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mProcessor:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mResultQueue:Ljava/util/Collection;

    .line 36
    return-void
.end method


# virtual methods
.method public endDocument()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mProcessor:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;

    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mResultQueue:Ljava/util/Collection;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;->handleEndDocument(Ljava/util/Collection;)V

    .line 59
    return-void
.end method

.method protected onText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mStopParsing:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    new-instance v0, Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;-><init>()V

    throw v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mProcessor:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;

    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mResultQueue:Ljava/util/Collection;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;->handleNewText(Ljava/lang/String;Ljava/util/Collection;)V

    .line 54
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/model/ContentXmlHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 41
    invoke-static {p3, p4}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->getReadingPosition(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "position":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->mProcessor:Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;->handleNewReadingPosition(Ljava/lang/String;)V

    .line 45
    :cond_0
    return-void
.end method
