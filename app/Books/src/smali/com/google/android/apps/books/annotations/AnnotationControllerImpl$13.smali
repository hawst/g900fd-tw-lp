.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->updateLayers(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$layers:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 646
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;->val$layers:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 653
    const-string v2, "AnnotationC"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 654
    const-string v2, "AnnotationC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received new layers: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;->val$layers:Ljava/util/List;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;->val$layers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/Layer;

    .line 657
    .local v1, "layer":Lcom/google/android/apps/books/annotations/Layer;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$13;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->onNewLayer(Lcom/google/android/apps/books/annotations/Layer;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$1100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/Layer;)V

    goto :goto_0

    .line 659
    .end local v1    # "layer":Lcom/google/android/apps/books/annotations/Layer;
    :cond_1
    return-void
.end method
