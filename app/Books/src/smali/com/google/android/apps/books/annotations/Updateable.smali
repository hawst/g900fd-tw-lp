.class public interface abstract Lcom/google/android/apps/books/annotations/Updateable;
.super Ljava/lang/Object;
.source "Updateable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract canBeUpdated()Z
.end method

.method public abstract currentBestValue()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract update()Lcom/google/android/apps/books/annotations/Updateable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/annotations/Updateable",
            "<TT;>;"
        }
    .end annotation
.end method
