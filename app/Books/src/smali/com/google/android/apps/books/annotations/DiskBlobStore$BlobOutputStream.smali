.class Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;
.super Ljava/io/OutputStream;
.source "DiskBlobStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/DiskBlobStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BlobOutputStream"
.end annotation


# instance fields
.field private mBytesWritten:I

.field private final mKey:Ljava/lang/String;

.field private final mOutputFile:Ljava/io/File;

.field private final mOutputStream:Ljava/io/OutputStream;

.field final synthetic this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/DiskBlobStore;Ljava/io/File;Ljava/lang/String;)V
    .locals 2
    .param p2, "outputFile"    # Ljava/io/File;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 206
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputFile:Ljava/io/File;

    .line 207
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputStream:Ljava/io/OutputStream;

    .line 208
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mKey:Ljava/lang/String;

    .line 209
    return-void
.end method


# virtual methods
.method public close()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 220
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    iget-object v6, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mKey:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/annotations/DiskBlobStore;->digestForKey(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$000(Lcom/google/android/apps/books/annotations/DiskBlobStore;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 221
    .local v1, "digest":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # invokes: Lcom/google/android/apps/books/annotations/DiskBlobStore;->pathForDigest(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5, v1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$100(Lcom/google/android/apps/books/annotations/DiskBlobStore;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 222
    .local v3, "finalPath":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputFile:Ljava/io/File;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 225
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$200(Lcom/google/android/apps/books/annotations/DiskBlobStore;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 226
    .local v4, "oldEntryInfo":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    if-eqz v4, :cond_0

    .line 227
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # invokes: Lcom/google/android/apps/books/annotations/DiskBlobStore;->removeEntry(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)V
    invoke-static {v5, v4}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$300(Lcom/google/android/apps/books/annotations/DiskBlobStore;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)V

    .line 229
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$200(Lcom/google/android/apps/books/annotations/DiskBlobStore;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->newBuilder()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->setSize(I)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->setName(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    .line 232
    .local v0, "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$200(Lcom/google/android/apps/books/annotations/DiskBlobStore;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->build()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    iget v6, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    # += operator for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I
    invoke-static {v5, v6}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$412(Lcom/google/android/apps/books/annotations/DiskBlobStore;I)I

    .line 235
    const-string v5, "BlobStore"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 236
    const-string v5, "BlobStore"

    const-string v6, "Wrote %d bytes for key %s to %s; new size %d/%d"

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mKey:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    aput-object v3, v7, v8

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I
    invoke-static {v9}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$400(Lcom/google/android/apps/books/annotations/DiskBlobStore;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    iget-object v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I
    invoke-static {v9}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$500(Lcom/google/android/apps/books/annotations/DiskBlobStore;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    .end local v0    # "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .end local v1    # "digest":Ljava/lang/String;
    .end local v3    # "finalPath":Ljava/lang/String;
    .end local v4    # "oldEntryInfo":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    :cond_1
    :goto_0
    return-void

    .line 241
    :catch_0
    move-exception v2

    .line 242
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 214
    return-void
.end method

.method public write(I)V
    .locals 3
    .param p1, "oneByte"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    new-array v0, p1, [B

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->write([BII)V

    .line 267
    return-void
.end method

.method public write([B)V
    .locals 2
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->write([BII)V

    .line 262
    return-void
.end method

.method public write([BII)V
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 250
    iget v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$400(Lcom/google/android/apps/books/annotations/DiskBlobStore;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    # getter for: Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I
    invoke-static {v1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$500(Lcom/google/android/apps/books/annotations/DiskBlobStore;)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->this$0:Lcom/google/android/apps/books/annotations/DiskBlobStore;

    iget v1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;->mBytesWritten:I

    # invokes: Lcom/google/android/apps/books/annotations/DiskBlobStore;->enforceMaxSizeWithPendingSize(I)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->access$600(Lcom/google/android/apps/books/annotations/DiskBlobStore;I)V

    .line 257
    :cond_0
    return-void
.end method
