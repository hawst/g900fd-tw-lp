.class public interface abstract Lcom/google/android/apps/books/app/ReaderMenu;
.super Ljava/lang/Object;
.source "ReaderMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReaderMenu$Callbacks;,
        Lcom/google/android/apps/books/app/ReaderMenu$Item;
    }
.end annotation


# virtual methods
.method public abstract clearSearchViewFocus()V
.end method

.method public abstract exitSearch()V
.end method

.method public abstract getSearchQuery()Ljava/lang/CharSequence;
.end method

.method public abstract onOptionsItemSelected(Landroid/view/MenuItem;)V
.end method

.method public abstract onRecentSearchesChanged(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onSearchRequested()V
.end method

.method public abstract setBookmarkIsAdd(Z)V
.end method

.method public abstract setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V
.end method

.method public abstract setMoItemState(Landroid/content/Context;ZZZ)V
.end method

.method public abstract setPosition(Lcom/google/android/apps/books/common/Position;)V
.end method

.method public abstract setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
.end method

.method public abstract setReadingModeItemState(ZLcom/google/android/apps/books/model/VolumeManifest$Mode;)V
.end method

.method public abstract setSearchMode(Z)V
.end method

.method public abstract setSearchQuery(Ljava/lang/CharSequence;Z)V
.end method

.method public abstract setTtsItemState(ZZZLandroid/app/Activity;)V
.end method

.method public abstract setVolumeMetadata(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)V
.end method

.method public abstract showRecentSearchesPopup(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract tearDown()V
.end method

.method public abstract updateIconVisibilities()V
.end method

.method public abstract updateSettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
.end method
