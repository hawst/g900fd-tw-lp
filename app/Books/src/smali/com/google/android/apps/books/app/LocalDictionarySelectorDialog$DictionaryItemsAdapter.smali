.class Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LocalDictionarySelectorDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DictionaryItemsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;->this$0:Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog;

    .line 136
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 137
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;

    .line 142
    .local v0, "item":Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/LocalDictionarySelectorDialog$DictionaryItemsAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/LocalDictionaryCheckableItem;->getView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method
