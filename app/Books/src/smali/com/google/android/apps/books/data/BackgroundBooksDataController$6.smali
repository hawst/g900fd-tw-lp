.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->releaseVolumeLicenseOnNetworkThread(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$key:Lcom/google/android/apps/books/model/SessionKey;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;->val$volumeId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;->val$key:Lcom/google/android/apps/books/model/SessionKey;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 306
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$6;->val$key:Lcom/google/android/apps/books/model/SessionKey;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/net/BooksServer;->releaseOfflineLicense(Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;)V

    .line 307
    return-void
.end method
