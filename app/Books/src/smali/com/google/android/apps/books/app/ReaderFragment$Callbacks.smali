.class public interface abstract Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/BooksFragmentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract acceptNewPosition(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;)V
.end method

.method public abstract closeBook()V
.end method

.method public abstract getPagesView3D()Lcom/google/android/apps/books/widget/PagesView3D;
.end method

.method public abstract getSystemUi(Landroid/view/View;)Lcom/google/android/ublib/view/SystemUi;
.end method

.method public abstract onHomePressed()V
.end method

.method public abstract populateReaderActionBar(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end method

.method public abstract restartCurrentActivity(Z)V
.end method

.method public abstract setActionBarElevation(F)V
.end method

.method public abstract showNewPositionAvailableDialog(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract temporarilyOverrideWindowAccessibilityAnnouncements(Ljava/lang/String;)V
.end method

.method public abstract updateTheme(Ljava/lang/String;)Z
.end method
