.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "BackgroundBooksDataController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->fetchDictionaryMetadata(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 1408
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;->val$finishConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 1413
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v4}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/net/BooksServer;->getDictionaryMetadata(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1415
    .local v0, "dictionaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52$1;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1429
    .end local v0    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;"
    .local v2, "next":Lcom/google/android/apps/books/data/ControlTask;
    :goto_0
    invoke-interface {p1, v2}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 1430
    return-void

    .line 1421
    .end local v2    # "next":Lcom/google/android/apps/books/data/ControlTask;
    :catch_0
    move-exception v1

    .line 1422
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52$2;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$52$2;-><init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$52;Ljava/io/IOException;)V

    .restart local v2    # "next":Lcom/google/android/apps/books/data/ControlTask;
    goto :goto_0
.end method
