.class Lcom/google/android/apps/books/app/InfoCardsHelper$2;
.super Ljava/lang/Object;
.source "InfoCardsHelper.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/InfoCardsHelper;->setTextSelection(Lcom/google/android/apps/books/app/SelectionState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/annotations/Annotation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

.field final synthetic val$annotationConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$newSequenceNumber:I

.field final synthetic val$provider:Lcom/google/android/apps/books/app/InfoCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/InfoCardsHelper;Lcom/google/android/ublib/utils/Consumer;ILcom/google/android/apps/books/app/InfoCardProvider;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    iput-object p2, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$annotationConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput p3, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$newSequenceNumber:I

    iput-object p4, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$provider:Lcom/google/android/apps/books/app/InfoCardProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "a"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 142
    if-nez p1, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$annotationConsumer:Lcom/google/android/ublib/utils/Consumer;

    sget-object v1, Lcom/google/android/apps/books/app/InfoCardProvider;->GENERIC_NO_OP:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$newSequenceNumber:I

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->this$0:Lcom/google/android/apps/books/app/InfoCardsHelper;

    # getter for: Lcom/google/android/apps/books/app/InfoCardsHelper;->mSelectionSequenceNumber:I
    invoke-static {v1}, Lcom/google/android/apps/books/app/InfoCardsHelper;->access$300(Lcom/google/android/apps/books/app/InfoCardsHelper;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$provider:Lcom/google/android/apps/books/app/InfoCardProvider;

    iget-object v1, p0, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->val$annotationConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/app/InfoCardProvider;->loadCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 139
    check-cast p1, Lcom/google/android/apps/books/annotations/Annotation;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/InfoCardsHelper$2;->take(Lcom/google/android/apps/books/annotations/Annotation;)V

    return-void
.end method
