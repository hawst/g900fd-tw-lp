.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getSessionKeyToGetVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/model/LocalSessionKey",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$licenseType:Ljava/lang/String;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 1109
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$licenseType:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 1112
    .local p1, "keyResult":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1113
    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$volumeId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$licenseType:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V
    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$4400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/ublib/utils/Consumer;)V

    .line 1119
    :goto_0
    return-void

    .line 1116
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1109
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController$35;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
