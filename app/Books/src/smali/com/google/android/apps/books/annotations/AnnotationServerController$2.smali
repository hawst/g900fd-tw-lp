.class Lcom/google/android/apps/books/annotations/AnnotationServerController$2;
.super Ljava/lang/Object;
.source "AnnotationServerController.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationServerController$PaginatedResponseSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationServerController;->downloadVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationProcessor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationServerController;

.field final synthetic val$layerVersion:Ljava/lang/String;

.field final synthetic val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationServerController;Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;->this$0:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;->val$layerVersion:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPage(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .locals 4
    .param p1, "continuation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;->this$0:Lcom/google/android/apps/books/annotations/AnnotationServerController;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationServerController;->mServer:Lcom/google/android/apps/books/annotations/AnnotationServer;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationServerController;->access$000(Lcom/google/android/apps/books/annotations/AnnotationServerController;)Lcom/google/android/apps/books/annotations/AnnotationServer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;->val$request:Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationServerController$2;->val$layerVersion:Ljava/lang/String;

    const/16 v3, 0x28

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/google/android/apps/books/annotations/AnnotationServer;->getVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;

    move-result-object v0

    return-object v0
.end method
