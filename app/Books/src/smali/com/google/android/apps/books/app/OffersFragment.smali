.class public Lcom/google/android/apps/books/app/OffersFragment;
.super Lcom/google/android/apps/books/app/BooksFragment;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;
.implements Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;,
        Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;,
        Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    }
.end annotation


# static fields
.field public static final OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;


# instance fields
.field private final mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

.field private mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;

.field private final mDataController:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController;",
            ">;"
        }
    .end annotation
.end field

.field private final mOfferClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback",
            "<",
            "Lcom/google/android/apps/books/playcards/BookDocument;",
            ">;"
        }
    .end annotation
.end field

.field private final mOfferImageProviderFactory:Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;

.field private final mOffers:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mOffersProxy:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mRedeemBookClickHandler:Landroid/view/View$OnClickListener;

.field private final mSkipThisOfferClickHandler:Landroid/view/View$OnClickListener;

.field private mView:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 86
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    const v1, 0x7f040033

    const v4, 0x3fb872b0    # 1.441f

    sget-object v5, Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;->FILL_TO_ASPECT_RATIO:Lcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;-><init>(IIIFLcom/google/android/ublib/cardlib/PlayCardArtImageView$FillStyle;)V

    sput-object v0, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksFragment;-><init>()V

    .line 93
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    invoke-direct {v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    .line 96
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    invoke-static {v0}, Lcom/google/android/apps/books/util/UiThreadConsumer;->proxyTo(Lcom/google/android/apps/books/util/Eventual;)Lcom/google/android/apps/books/util/UiThreadConsumer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOffersProxy:Lcom/google/android/ublib/utils/Consumer;

    .line 101
    new-instance v0, Lcom/google/android/apps/books/app/OffersFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/OffersFragment$1;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    invoke-static {v0}, Lcom/google/android/apps/books/util/Eventual;->create(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    .line 112
    new-instance v0, Lcom/google/android/apps/books/app/OffersFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/OffersFragment$2;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOfferClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    .line 120
    new-instance v0, Lcom/google/android/apps/books/app/OffersFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/OffersFragment$3;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mRedeemBookClickHandler:Landroid/view/View$OnClickListener;

    .line 134
    new-instance v0, Lcom/google/android/apps/books/app/OffersFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/OffersFragment$4;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mSkipThisOfferClickHandler:Landroid/view/View$OnClickListener;

    .line 158
    new-instance v0, Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;-><init>(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/app/OffersFragment$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOfferImageProviderFactory:Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;

    .line 398
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/ublib/utils/Consumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOffersProxy:Lcom/google/android/ublib/utils/Consumer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/playcards/BookDocument;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment;->showRedeemOfferDialog(Lcom/google/android/apps/books/playcards/BookDocument;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/playcards/BookDocument;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment;->viewAboutThisBook(Lcom/google/android/apps/books/playcards/BookDocument;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/api/data/JsonError;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/api/data/JsonError;

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/android/apps/books/app/OffersFragment;->isUnrecoverableError(Lcom/google/android/apps/books/api/data/JsonError;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/OffersFragment;Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment;->getDocumentForView(Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/OffersFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->dismissOffer()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/OffersFragment;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p4, "x4"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 63
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/app/OffersFragment;->getBookCoverImage(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/model/OfferData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/model/OfferData;)Lcom/google/android/apps/books/model/OfferData;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/OfferData;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/OffersFragment;Lcom/google/android/apps/books/model/OfferData;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/OfferData;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment;->createOfferCards(Lcom/google/android/apps/books/model/OfferData;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OffersFragment;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private createDocumentFromOfferedBook(Lcom/google/android/apps/books/model/OfferedBookData;)Lcom/google/android/apps/books/playcards/BookDocument;
    .locals 2
    .param p1, "book"    # Lcom/google/android/apps/books/model/OfferedBookData;

    .prologue
    .line 359
    new-instance v0, Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-direct {v0}, Lcom/google/android/apps/books/playcards/BookDocument;-><init>()V

    .line 360
    .local v0, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/OfferedBookData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/playcards/BookDocument;->setTitle(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/OfferedBookData;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/playcards/BookDocument;->setSubTitle(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/OfferedBookData;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/playcards/BookDocument;->setVolumeId(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/OfferedBookData;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/playcards/BookDocument;->setDescription(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/OfferedBookData;->getCoverUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/playcards/BookDocument;->setThumbnailURI(Landroid/net/Uri;)V

    .line 365
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/OfferedBookData;->getCanonicalVolumeLink()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/playcards/BookDocument;->setCanonicalVolumeLink(Ljava/lang/String;)V

    .line 366
    return-object v0
.end method

.method private createOfferCards(Lcom/google/android/apps/books/model/OfferData;)V
    .locals 17
    .param p1, "offer"    # Lcom/google/android/apps/books/model/OfferData;

    .prologue
    .line 320
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/model/OfferData;->getOfferedBooks()Ljava/util/List;

    move-result-object v15

    .line 321
    .local v15, "offeredBooks":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/apps/books/model/OfferedBookData;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/OffersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c000f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    .line 322
    .local v13, "nbColumns":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v4, v13

    add-int/lit8 v4, v4, -0x1

    div-int v14, v4, v13

    .line 324
    .local v14, "nbRows":I
    new-instance v2, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    sget-object v4, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v4

    mul-int/2addr v4, v13

    sget-object v5, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v5

    mul-int/2addr v5, v14

    invoke-direct {v2, v4, v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    .line 328
    .local v2, "metadata":Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;
    invoke-static {}, Lcom/google/android/ublib/cardlib/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 330
    .local v3, "docList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v4

    if-ge v12, v4, :cond_0

    .line 331
    div-int v4, v12, v13

    sget-object v5, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v5

    mul-int v16, v4, v5

    .line 332
    .local v16, "row":I
    rem-int v4, v12, v13

    sget-object v5, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v5}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v5

    mul-int v10, v4, v5

    .line 333
    .local v10, "column":I
    sget-object v4, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    move/from16 v0, v16

    invoke-virtual {v2, v4, v10, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;->addTile(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    .line 335
    invoke-interface {v15, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/model/OfferedBookData;

    .line 336
    .local v9, "book":Lcom/google/android/apps/books/model/OfferedBookData;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/apps/books/app/OffersFragment;->createDocumentFromOfferedBook(Lcom/google/android/apps/books/model/OfferedBookData;)Lcom/google/android/apps/books/playcards/BookDocument;

    move-result-object v11

    .line 337
    .local v11, "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    invoke-interface {v3, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 340
    .end local v9    # "book":Lcom/google/android/apps/books/model/OfferedBookData;
    .end local v10    # "column":I
    .end local v11    # "doc":Lcom/google/android/apps/books/playcards/BookDocument;
    .end local v16    # "row":I
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCardsContainer()Lcom/google/android/apps/books/widget/BookCardClusterViewContent;

    move-result-object v1

    .line 341
    .local v1, "cardsContainer":Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;, "Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent<Lcom/google/android/apps/books/playcards/BookDocument;>;"
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/app/OffersFragment;->mOfferClickCallback:Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/app/OffersFragment;->mOfferImageProviderFactory:Lcom/google/android/apps/books/app/OffersFragment$OfferImageProviderFactory;

    const/4 v7, 0x0

    move-object/from16 v8, p0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 343
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/OffersFragment;->mCardHeap:Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;

    invoke-virtual {v1, v4, v1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->inflateContent(Lcom/google/android/ublib/cardlib/layout/PlayCardHeap;Landroid/view/ViewGroup;)V

    .line 344
    invoke-virtual {v1}, Lcom/google/android/ublib/cardlib/PlayCardClusterViewContent;->createContent()V

    .line 345
    return-void
.end method

.method private dismissOffer()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/OffersFragment$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/OffersFragment$7;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 259
    :goto_0
    return-void

    .line 254
    :cond_0
    const-string v0, "OffersFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    const-string v0, "OffersFragment"

    const-string v1, "Current offer is empty!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->onOfferDismissed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/LoaderParams;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private static getBookCoverImage(Landroid/content/Context;Landroid/accounts/Account;Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageCallback;)Lcom/google/android/apps/books/common/ImageFuture;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p4, "callback"    # Lcom/google/android/apps/books/common/ImageCallback;

    .prologue
    .line 371
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v6

    .line 372
    .local v6, "booksApp":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v6}, Lcom/google/android/apps/books/app/BooksApplication;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v0

    .line 374
    .local v0, "sharedImageManager":Lcom/google/android/apps/books/common/ImageManager;
    invoke-virtual {v6, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getReadNowCoverStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/RemoteFileCache;

    move-result-object v5

    .line 376
    .local v5, "imageStore":Lcom/google/android/apps/books/model/RemoteFileCache;
    const/4 v3, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    move-result-object v7

    .line 378
    .local v7, "future":Lcom/google/android/apps/books/common/ImageFuture;
    return-object v7
.end method

.method private getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    .locals 1

    .prologue
    .line 270
    invoke-static {p0}, Lcom/google/android/apps/books/app/OffersActivity;->getOffersCallbacks(Landroid/support/v4/app/Fragment;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    return-object v0
.end method

.method private getCardsContainer()Lcom/google/android/apps/books/widget/BookCardClusterViewContent;
    .locals 3

    .prologue
    .line 314
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment;->mView:Landroid/view/ViewGroup;

    const v2, 0x7f0e010d

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BookCardClusterViewContent;

    .line 316
    .local v0, "cardsContainer":Lcom/google/android/apps/books/widget/BookCardClusterViewContent;
    return-object v0
.end method

.method private getDocumentForView(Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 348
    if-nez p1, :cond_1

    .line 354
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-object v1

    .line 350
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    instance-of v2, p1, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    if-eqz v2, :cond_2

    .line 351
    check-cast p1, Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->getDocument()Lcom/google/android/ublib/cardlib/model/Document;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/playcards/BookDocument;

    goto :goto_0

    .line 353
    .restart local p1    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 354
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/View;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/OffersFragment;->getDocumentForView(Landroid/view/View;)Lcom/google/android/apps/books/playcards/BookDocument;

    move-result-object v1

    goto :goto_0
.end method

.method static isDeviceConnectedElseToast(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 382
    const v0, 0x7f0f01e8

    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnectedElseToast(Landroid/content/Context;I)Z

    move-result v0

    return v0
.end method

.method private static isUnrecoverableError(Lcom/google/android/apps/books/api/data/JsonError;)Z
    .locals 2
    .param p0, "error"    # Lcom/google/android/apps/books/api/data/JsonError;

    .prologue
    .line 546
    const/16 v0, 0x194

    const-string v1, "notFound"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/api/data/JsonError;->hasCodeAndReason(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x190

    const-string v1, "alreadyAccepted"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/api/data/JsonError;->hasCodeAndReason(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeGetDataController(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 262
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 263
    .local v0, "account":Landroid/accounts/Account;
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    invoke-static {p1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 267
    :cond_0
    return-void
.end method

.method private showRedeemOfferDialog(Lcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/books/app/OffersFragment;->isDeviceConnectedElseToast(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_SHOW_REDEEM_DIALOG:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 388
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 389
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;-><init>(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setAccount(Landroid/accounts/Account;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getThumbnailURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/util/VolumeArguments$Builder;->setCoverImageUri(Landroid/net/Uri;)Lcom/google/android/apps/books/util/VolumeArguments$Builder;

    .line 394
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/books/app/OffersFragment$OfferActionDialog;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->addFragment(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 396
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method private viewAboutThisBook(Lcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/apps/books/playcards/BookDocument;

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0f00b9

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnectedElseToast(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 276
    if-eqz p1, :cond_1

    .line 277
    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    .line 278
    .local v1, "volumeId":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/books/playcards/BookDocument;->getCanonicalVolumeLink()Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "canonicalUrl":Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;->OFFERS_ACTION_ABOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    invoke-static {v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logStoreAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V

    .line 280
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v2

    const-string v3, "books_inapp_offers_options_about"

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->startAboutVolume(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    .end local v0    # "canonicalUrl":Ljava/lang/String;
    .end local v1    # "volumeId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v2, "OffersFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    const-string v2, "OffersFragment"

    const-string v3, "Can\'t find document for about button!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p2, "onResult":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mDataController:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/OffersFragment$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/app/OffersFragment$6;-><init>(Lcom/google/android/apps/books/app/OffersFragment;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 242
    :goto_0
    return-void

    .line 237
    :cond_0
    const-string v0, "OffersFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    const-string v0, "OffersFragment"

    const-string v1, "Current offer is empty!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->onOfferDismissed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public applyBindings(Lcom/google/android/ublib/cardlib/model/Document;Lcom/google/android/ublib/cardlib/layout/PlayCardView;)V
    .locals 7
    .param p1, "doc"    # Lcom/google/android/ublib/cardlib/model/Document;
    .param p2, "cardView"    # Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    .prologue
    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/ublib/cardlib/model/Document;->getSubTitle()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/books/util/AccessibilityUtils;->getVolumeContentDescription(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "contentDescriptionBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p2, v0}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 297
    const v3, 0x7f0e00d3

    invoke-virtual {p2, v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 298
    .local v2, "redeemButton":Landroid/view/View;
    iget-object v3, p0, Lcom/google/android/apps/books/app/OffersFragment;->mRedeemBookClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    const v3, 0x7f0e00d0

    invoke-virtual {p2, v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 301
    .local v1, "descriptionView":Landroid/widget/TextView;
    new-instance v3, Lcom/google/android/apps/books/app/OffersFragment$8;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/OffersFragment$8;-><init>(Lcom/google/android/apps/books/app/OffersFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/OffersFragment;->maybeGetDataController(Landroid/content/Context;)V

    .line 165
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksFragment;->onAttach(Landroid/app/Activity;)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/OffersFragment;->maybeGetDataController(Landroid/content/Context;)V

    .line 171
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 176
    const v0, 0x7f040052

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mView:Landroid/view/ViewGroup;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mView:Landroid/view/ViewGroup;

    const v1, 0x7f0e0109

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/widget/ObservableScrollView;

    .line 180
    .local v8, "scroller":Lcom/google/android/apps/books/widget/ObservableScrollView;
    invoke-virtual {v8, p0}, Lcom/google/android/apps/books/widget/ObservableScrollView;->setOnScrollListener(Lcom/google/android/apps/books/widget/ObservableScrollView$OnScrollListener;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mView:Landroid/view/ViewGroup;

    const v1, 0x7f0e010e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 183
    .local v9, "skipThisOfferButton":Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mSkipThisOfferClickHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCardsContainer()Lcom/google/android/apps/books/widget/BookCardClusterViewContent;

    move-result-object v0

    new-instance v1, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;

    sget-object v3, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v3}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/books/app/OffersFragment;->OFFER_CARD:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;-><init>(II)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/books/widget/BookCardClusterViewContent;->setMetadata(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata;Ljava/util/List;Lcom/google/android/ublib/cardlib/layout/PlayCardView$ContextMenuDelegate;Lcom/google/android/ublib/cardlib/model/DocumentClickHandler$Callback;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$Factory;Lcom/google/android/ublib/cardlib/layout/PlayCardView$OptionalActionCallback;Lcom/google/android/ublib/cardlib/layout/PlayCardView$CustomBindingsProvider;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mOffers:Lcom/google/android/apps/books/util/Eventual;

    new-instance v1, Lcom/google/android/apps/books/app/OffersFragment$5;

    invoke-direct {v1, p0, v9}, Lcom/google/android/apps/books/app/OffersFragment$5;-><init>(Lcom/google/android/apps/books/app/OffersFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment;->mView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onScrollChanged(Lcom/google/android/apps/books/widget/ObservableScrollView;IIII)V
    .locals 1
    .param p1, "scrollView"    # Lcom/google/android/apps/books/widget/ObservableScrollView;
    .param p2, "x"    # I
    .param p3, "y"    # I
    .param p4, "oldx"    # I
    .param p5, "oldy"    # I

    .prologue
    .line 219
    if-lez p3, :cond_0

    .line 220
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->getActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    goto :goto_0
.end method
