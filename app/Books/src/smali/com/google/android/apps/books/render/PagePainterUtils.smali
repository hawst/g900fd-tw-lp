.class public Lcom/google/android/apps/books/render/PagePainterUtils;
.super Ljava/lang/Object;
.source "PagePainterUtils.java"


# static fields
.field private static final sTmpPoint:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/render/PagePainterUtils;->sTmpPoint:Landroid/graphics/Point;

    return-void
.end method

.method public static drawToScale(Lcom/google/android/apps/books/render/PagePainter;Landroid/graphics/Bitmap;)Z
    .locals 4
    .param p0, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 18
    sget-object v1, Lcom/google/android/apps/books/render/PagePainterUtils;->sTmpPoint:Landroid/graphics/Point;

    invoke-interface {p0, v1}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 19
    sget-object v1, Lcom/google/android/apps/books/render/PagePainterUtils;->sTmpPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/books/render/PagePainterUtils;->sTmpPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-nez v1, :cond_1

    .line 20
    :cond_0
    const/4 v1, 0x0

    .line 28
    :goto_0
    return v1

    .line 23
    :cond_1
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 24
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 25
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sget-object v2, Lcom/google/android/apps/books/render/PagePainterUtils;->sTmpPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/books/render/PagePainterUtils;->sTmpPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 27
    invoke-interface {p0, v0}, Lcom/google/android/apps/books/render/PagePainter;->draw(Landroid/graphics/Canvas;)V

    .line 28
    const/4 v1, 0x1

    goto :goto_0
.end method
