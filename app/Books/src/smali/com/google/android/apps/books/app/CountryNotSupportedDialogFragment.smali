.class public Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "CountryNotSupportedDialogFragment.java"


# instance fields
.field private final mDismissClick:Landroid/content/DialogInterface$OnClickListener;

.field private final mMoreClick:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$1;-><init>(Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->mMoreClick:Landroid/content/DialogInterface$OnClickListener;

    .line 47
    new-instance v0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment$2;-><init>(Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->mDismissClick:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 27
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0f0114

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 28
    const v1, 0x7f0f0115

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 29
    const v1, 0x7f0f00ea

    iget-object v2, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->mMoreClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 30
    const v1, 0x7f0f0116

    iget-object v2, p0, Lcom/google/android/apps/books/app/CountryNotSupportedDialogFragment;->mDismissClick:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 32
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
