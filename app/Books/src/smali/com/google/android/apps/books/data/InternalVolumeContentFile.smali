.class public Lcom/google/android/apps/books/data/InternalVolumeContentFile;
.super Ljava/lang/Object;
.source "InternalVolumeContentFile.java"

# interfaces
.implements Lcom/google/android/apps/books/data/VolumeContentFile;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;,
        Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;,
        Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;,
        Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;
    }
.end annotation


# instance fields
.field private final mDecorator:Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;

.field private final mFile:Ljava/io/File;

.field private final mTempFileSource:Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;)V
    .locals 0
    .param p1, "file"    # Ljava/io/File;
    .param p2, "decorator"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;
    .param p3, "tempFileSource"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mDecorator:Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;

    .line 52
    iput-object p3, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mTempFileSource:Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->ensureParent()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mDecorator:Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mTempFileSource:Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Ljava/io/InputStream;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .param p1, "x1"    # Ljava/io/InputStream;
    .param p2, "x2"    # Ljava/io/File;
    .param p3, "x3"    # Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->writeToContentFile(Ljava/io/InputStream;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)V

    return-void
.end method

.method private ensureParent()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->ensureParent(Ljava/io/File;)Ljava/io/File;

    .line 182
    return-void
.end method

.method private writeToContentFile(Ljava/io/InputStream;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)V
    .locals 6
    .param p1, "data"    # Ljava/io/InputStream;
    .param p2, "tempFile"    # Ljava/io/File;
    .param p3, "listener"    # Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 163
    .local v1, "output":Ljava/io/OutputStream;
    :try_start_0
    invoke-static {p1, v1, p3}, Lcom/google/android/apps/books/util/IOUtils;->copyLarge(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 174
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-static {p2}, Lcom/google/android/apps/books/util/FileUtils;->freeBytesOnFilesystem(Ljava/io/File;)J

    move-result-wide v2

    const-wide/32 v4, 0x186a0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 167
    new-instance v2, Lcom/google/android/apps/books/data/OutOfSpaceException;

    invoke-direct {v2, p2}, Lcom/google/android/apps/books/data/OutOfSpaceException;-><init>(Ljava/io/File;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 172
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v2

    .line 169
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method


# virtual methods
.method public createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v0

    return-object v0
.end method

.method public createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/apps/books/util/IOUtils;->DONT_LISTEN:Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v0

    return-object v0
.end method

.method public createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .locals 2
    .param p1, "saveListener"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;
    .param p2, "progressListener"    # Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mTempFileSource:Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;

    invoke-interface {v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;->createTempFile()Ljava/io/File;

    move-result-object v0

    .line 141
    .local v0, "tempFile":Ljava/io/File;
    new-instance v1, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;

    invoke-direct {v1, p0, v0, p2, p1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;-><init>(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V

    return-object v1
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->openParcelFileDescriptor(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 58
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/io/FileNotFoundException;
    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mDecorator:Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;

    iget-object v2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;->decorateFileNotFoundException(Ljava/io/IOException;Ljava/io/File;)Ljava/io/FileNotFoundException;

    move-result-object v1

    throw v1
.end method

.method public openParcelFileDescriptor(I)Landroid/os/ParcelFileDescriptor;
    .locals 3
    .param p1, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 67
    const/high16 v1, 0x8000000

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    .line 68
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->ensureParent()V

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-static {v1, p1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 71
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mDecorator:Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;

    iget-object v2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$FileExceptionDecorator;->decorateFileNotFoundException(Ljava/io/IOException;Ljava/io/File;)Ljava/io/FileNotFoundException;

    move-result-object v1

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
