.class public Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;
.super Landroid/webkit/WebView;
.source "FootnoteCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FootnoteWebView"
.end annotation


# instance fields
.field private mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

.field private mMaxHeight:I

.field private mMotionStartY:F

.field private mScrolling:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 282
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mScrolling:Z

    .line 283
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mScrolling:Z

    .line 287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 290
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mScrolling:Z

    .line 291
    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;
    .param p1, "x1"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setCallbacks(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;)V

    return-void
.end method

.method private setCallbacks(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    .line 295
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 335
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onMeasure(II)V

    .line 336
    invoke-virtual {p0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mMaxHeight:I

    if-le v0, v1, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mMaxHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->setMeasuredDimension(II)V

    .line 339
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 306
    iget-object v3, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    invoke-interface {v3}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;->getCardsState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;->PEEKING:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    if-ne v3, v4, :cond_1

    move v0, v2

    .line 330
    :cond_0
    :goto_0
    return v0

    .line 310
    :cond_1
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 314
    .local v0, "result":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    .line 315
    iput-boolean v2, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mScrolling:Z

    .line 316
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mMotionStartY:F

    .line 317
    iget-object v2, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mCallbacks:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;

    invoke-interface {v2}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;->disallowVerticalCardSwipes()V

    .line 321
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 323
    .local v1, "vc":Landroid/view/ViewConfiguration;
    iget-boolean v2, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mScrolling:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mMotionStartY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 325
    iput-boolean v5, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mScrolling:Z

    .line 326
    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1, "maxHeight"    # I

    .prologue
    .line 298
    iput p1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$FootnoteWebView;->mMaxHeight:I

    .line 299
    return-void
.end method
