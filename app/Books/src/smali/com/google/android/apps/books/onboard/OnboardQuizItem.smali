.class public Lcom/google/android/apps/books/onboard/OnboardQuizItem;
.super Lcom/google/android/libraries/bind/widget/BindingLinearLayout;
.source "OnboardQuizItem.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# static fields
.field public static final CLICK_FORWARDER:Landroid/view/View$OnClickListener;

.field public static final EQUALITY_FIELDS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->EQUALITY_FIELDS:[I

    .line 144
    new-instance v0, Lcom/google/android/apps/books/onboard/OnboardQuizItem$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->CLICK_FORWARDER:Landroid/view/View$OnClickListener;

    return-void

    .line 55
    :array_0
    .array-data 4
        0x7f0e002f
        0x7f0e0035
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public static applyAlpha(II)I
    .locals 4
    .param p0, "srcColor"    # I
    .param p1, "alpha"    # I

    .prologue
    .line 83
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    mul-int/2addr v0, p1

    div-int/lit16 v0, v0, 0xff

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method protected isGenreItem()Z
    .locals 2

    .prologue
    .line 79
    const v0, 0x7f040072

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onDataUpdated(Lcom/google/android/libraries/bind/data/Data;)V

    .line 93
    if-eqz p1, :cond_0

    const v4, 0x7f0e0035

    invoke-virtual {p1, v4, v3}, Lcom/google/android/libraries/bind/data/Data;->getAsBoolean(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    .line 96
    .local v2, "selected":Z
    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->setSelected(Z)V

    .line 98
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 100
    .local v0, "colorResId":Ljava/lang/Integer;
    :goto_1
    if-nez v0, :cond_2

    move v1, v3

    .line 103
    .local v1, "opaqueColor":I
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->isGenreItem()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 104
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->updateGenreDrawableColors(I)V

    .line 108
    :goto_3
    return-void

    .end local v0    # "colorResId":Ljava/lang/Integer;
    .end local v1    # "opaqueColor":I
    .end local v2    # "selected":Z
    :cond_0
    move v2, v3

    .line 93
    goto :goto_0

    .line 98
    .restart local v2    # "selected":Z
    :cond_1
    const v4, 0x7f0e0037

    invoke-virtual {p1, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsInteger(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 100
    .restart local v0    # "colorResId":Ljava/lang/Integer;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_2

    .line 106
    .restart local v1    # "opaqueColor":I
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->updateBookOverlayColor(I)V

    goto :goto_3
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/BindingLinearLayout;->onFinishInflate()V

    .line 74
    const v0, 0x7f0e00d6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->CLICK_FORWARDER:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    return-void
.end method

.method protected setDiskColor(II)V
    .locals 4
    .param p1, "viewId"    # I
    .param p2, "argb"    # I

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 121
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 122
    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 123
    .local v1, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    const v3, 0x7f0e020b

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 125
    .local v0, "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 127
    .end local v0    # "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    .end local v1    # "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    :cond_0
    return-void
.end method

.method protected updateBookOverlayColor(I)V
    .locals 7
    .param p1, "opaqueColor"    # I

    .prologue
    const v6, 0x7f0e0154

    .line 130
    const v4, 0x7f0e0155

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 131
    .local v3, "overlayView":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0012

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-float v4, v4

    const v5, 0x40233333    # 2.55f

    mul-float/2addr v4, v5

    float-to-int v1, v4

    .line 135
    .local v1, "overlayAlpha":I
    invoke-static {p1, v1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->applyAlpha(II)I

    move-result v2

    .line 136
    .local v2, "overlayColor":I
    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 138
    .end local v1    # "overlayAlpha":I
    .end local v2    # "overlayColor":I
    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 139
    .local v0, "noImageFallback":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {p0, v6, p1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->setDiskColor(II)V

    .line 142
    :cond_1
    return-void
.end method

.method protected updateGenreDrawableColors(I)V
    .locals 4
    .param p1, "opaqueColor"    # I

    .prologue
    .line 111
    const v2, 0x7f0e00d6

    invoke-virtual {p0, v2, p1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->setDiskColor(II)V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x40233333    # 2.55f

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 115
    .local v0, "overlayAlpha":I
    invoke-static {p1, v0}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->applyAlpha(II)I

    move-result v1

    .line 116
    .local v1, "overlayColor":I
    const v2, 0x7f0e0155

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/books/onboard/OnboardQuizItem;->setDiskColor(II)V

    .line 117
    return-void
.end method
