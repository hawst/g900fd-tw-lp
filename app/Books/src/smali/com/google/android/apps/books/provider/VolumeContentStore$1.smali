.class final Lcom/google/android/apps/books/provider/VolumeContentStore$1;
.super Ljava/lang/Object;
.source "VolumeContentStore.java"

# interfaces
.implements Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/provider/VolumeContentStore;->contextCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/provider/VolumeContentStore$Callbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public externalStorageIsAvailable()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksProvider;->externalStorageIsAvailable(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public getInternalStorageDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/books/util/StorageUtils;->getInternalStorageDirectory(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
