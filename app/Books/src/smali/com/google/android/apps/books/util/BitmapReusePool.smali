.class public Lcom/google/android/apps/books/util/BitmapReusePool;
.super Ljava/lang/Object;
.source "BitmapReusePool.java"


# instance fields
.field private final mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentNavigableMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final mIdSeq:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mMaxBitmaps:I

.field private final mMaxSizePercent:I

.field private final mPendingIds:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 86
    const/16 v0, 0x10

    const/16 v1, 0x96

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/util/BitmapReusePool;-><init>(II)V

    .line 87
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "maxBitmaps"    # I
    .param p2, "maxSizePercent"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mIdSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentSkipListMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentSkipListMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    .line 37
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mPendingIds:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 52
    iput p1, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mMaxBitmaps:I

    .line 53
    iput p2, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mMaxSizePercent:I

    .line 54
    return-void
.end method

.method private getAllocSize(IILandroid/graphics/Bitmap$Config;)I
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    mul-int v1, p1, p2

    invoke-static {p3}, Lcom/google/android/apps/books/util/BitmapUtils;->getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I

    move-result v2

    mul-int/2addr v1, v2

    .line 71
    :goto_0
    return v1

    .line 69
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    shl-int/lit8 v1, v1, 0x1c

    shl-int/lit8 v2, p2, 0xe

    xor-int/2addr v1, v2

    xor-int v0, v1, p1

    .line 71
    .local v0, "hash":I
    const v1, 0x7ffffffe

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0
.end method

.method private getAllocSize(Landroid/graphics/Bitmap;)I
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 77
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    .line 80
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/util/BitmapReusePool;->getAllocSize(IILandroid/graphics/Bitmap$Config;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    .line 171
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mPendingIds:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .local v1, "id":Ljava/lang/Long;
    if-eqz v1, :cond_1

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-interface {v2, v1}, Ljava/util/concurrent/ConcurrentNavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 173
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 177
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-void
.end method

.method public obtain(IILandroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;
    .param p4, "needErase"    # Z

    .prologue
    const/16 v10, 0x20

    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/util/BitmapReusePool;->getAllocSize(IILandroid/graphics/Bitmap$Config;)I

    move-result v5

    .line 100
    .local v5, "needSize":I
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v6

    if-eqz v6, :cond_2

    iget v6, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mMaxSizePercent:I

    mul-int/2addr v6, v5

    div-int/lit8 v4, v6, 0x64

    .line 108
    .local v4, "maxSize":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    int-to-long v8, v5

    shl-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    add-int/lit8 v8, v4, 0x1

    int-to-long v8, v8

    shl-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/concurrent/ConcurrentNavigableMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/concurrent/ConcurrentNavigableMap;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/concurrent/ConcurrentNavigableMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 109
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Landroid/graphics/Bitmap;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 110
    .local v3, "key":Ljava/lang/Long;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 112
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-interface {v6, v3, v0}, Ljava/util/concurrent/ConcurrentNavigableMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 113
    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/util/BitmapUtils;->reconfigure(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 114
    if-eqz p4, :cond_1

    .line 115
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 124
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Landroid/graphics/Bitmap;>;"
    .end local v3    # "key":Ljava/lang/Long;
    :cond_1
    :goto_2
    return-object v0

    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "maxSize":I
    :cond_2
    move v4, v5

    .line 100
    goto :goto_0

    .line 119
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Landroid/graphics/Bitmap;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "key":Ljava/lang/Long;
    .restart local v4    # "maxSize":I
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 124
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Landroid/graphics/Bitmap;>;"
    .end local v3    # "key":Ljava/lang/Long;
    :cond_4
    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2
.end method

.method public release(Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 139
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 163
    :cond_0
    return-void

    .line 143
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/BitmapReusePool;->getAllocSize(Landroid/graphics/Bitmap;)I

    move-result v0

    .line 144
    .local v0, "allocSize":I
    int-to-long v6, v0

    const/16 v5, 0x20

    shl-long/2addr v6, v5

    iget-object v5, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mIdSeq:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v5

    const v8, 0x7fffffff

    and-int/2addr v5, v8

    int-to-long v8, v5

    or-long v2, v6, v8

    .line 146
    .local v2, "id":J
    iget-object v5, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6, p1}, Ljava/util/concurrent/ConcurrentNavigableMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v5, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mPendingIds:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 153
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-interface {v5}, Ljava/util/concurrent/ConcurrentNavigableMap;->size()I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mMaxBitmaps:I

    if-le v5, v6, :cond_0

    .line 154
    iget-object v5, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mPendingIds:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 155
    .local v4, "oldId":Ljava/lang/Long;
    if-eqz v4, :cond_0

    .line 158
    iget-object v5, p0, Lcom/google/android/apps/books/util/BitmapReusePool;->mBitmaps:Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-interface {v5, v4}, Ljava/util/concurrent/ConcurrentNavigableMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 159
    .local v1, "oldBitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_2

    .line 160
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method
