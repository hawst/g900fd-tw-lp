.class Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;
.super Ljava/lang/Object;
.source "MyEbooksSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Exception;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

.field final synthetic val$continuation:Ljava/lang/Runnable;

.field final synthetic val$e:Ljava/lang/Exception;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$continuation:Ljava/lang/Runnable;

    iput-object p4, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$e:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$continuation:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$continuation:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->this$0:Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;->val$e:Ljava/lang/Exception;

    # invokes: Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailure(Ljava/lang/Exception;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->access$100(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Ljava/lang/Exception;)V

    goto :goto_0
.end method
