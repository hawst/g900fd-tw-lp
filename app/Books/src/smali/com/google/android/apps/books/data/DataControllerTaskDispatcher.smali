.class public Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
.super Ljava/lang/Object;
.source "DataControllerTaskDispatcher.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTaskServices;
.implements Lcom/google/android/apps/books/data/NetworkTaskServices;


# instance fields
.field private final mControlTaskExecutor:Ljava/util/concurrent/Executor;

.field private final mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

.field private final mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

.field private final mNetworkTaskExecutor:Ljava/util/concurrent/Executor;

.field private final mServer:Lcom/google/android/apps/books/net/BooksServer;

.field private final mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/net/BooksServer;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/LocalDictionarySubController;Lcom/google/android/apps/books/model/DataControllerStore;)V
    .locals 1
    .param p1, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p2, "server"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p3, "controlTaskExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "sessionKeySubcontroller"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p6, "localDictionarySubController"    # Lcom/google/android/apps/books/data/LocalDictionarySubController;
    .param p7, "dataControllerStore"    # Lcom/google/android/apps/books/model/DataControllerStore;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mListeners:Ljava/util/Set;

    .line 43
    iput-object p1, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mServer:Lcom/google/android/apps/books/net/BooksServer;

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mControlTaskExecutor:Ljava/util/concurrent/Executor;

    .line 46
    iput-object p4, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mNetworkTaskExecutor:Ljava/util/concurrent/Executor;

    .line 47
    iput-object p5, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    .line 48
    iput-object p6, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mLocalDictionarySubController:Lcom/google/android/apps/books/data/LocalDictionarySubController;

    .line 49
    iput-object p7, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    .line 50
    return-void
.end method


# virtual methods
.method public copyListeners()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mListeners:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V
    .locals 2
    .param p1, "task"    # Lcom/google/android/apps/books/data/ControlTask;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mControlTaskExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher$2;-><init>(Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;Lcom/google/android/apps/books/data/ControlTask;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method public executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V
    .locals 2
    .param p1, "task"    # Lcom/google/android/apps/books/data/NetworkTask;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mNetworkTaskExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher$1;-><init>(Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;Lcom/google/android/apps/books/data/NetworkTask;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 87
    return-void
.end method

.method public getDataControllerStore()Lcom/google/android/apps/books/model/DataControllerStore;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    return-object v0
.end method

.method public getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    return-object v0
.end method

.method public getServer()Lcom/google/android/apps/books/net/BooksServer;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mServer:Lcom/google/android/apps/books/net/BooksServer;

    return-object v0
.end method

.method public getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->getValidAccountSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;)V

    .line 65
    return-void
.end method

.method public getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "keyId"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->getValidSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V

    .line 72
    return-void
.end method

.method public removeListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 124
    return-void
.end method

.method public removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mSessionKeySubcontroller:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 77
    return-void
.end method

.method public scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V
    .locals 0
    .param p1, "task"    # Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 111
    return-void
.end method

.method public weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    return-void
.end method
