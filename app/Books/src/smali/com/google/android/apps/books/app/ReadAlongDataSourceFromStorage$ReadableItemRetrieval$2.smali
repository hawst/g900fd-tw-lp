.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;
.super Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
.source "ReadAlongDataSourceFromStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->getXmlHandler()Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/Collection;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;
    .param p3, "x1"    # Ljava/util/concurrent/atomic/AtomicBoolean;

    .prologue
    .line 214
    .local p4, "x2":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;-><init>(Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 218
    const-string v2, "img"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 219
    const-string v2, "alt"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v0

    .line 220
    .local v0, "altIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 221
    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "altText":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTexts:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$1000(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mAltTextOffsets:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$1100(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval$2;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->mTextContent:Ljava/lang/StringBuilder;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;->access$800(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$ReadableItemRetrieval;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    .end local v0    # "altIndex":I
    .end local v1    # "altText":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    .line 227
    return-void
.end method
