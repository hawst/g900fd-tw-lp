.class Lcom/google/android/apps/books/app/ReaderFragment$16;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 3188
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$16;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3191
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;->SCRUBBER_EXIT_CLICKED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logInTheBookSearchAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$InTheBookSearchAction;Ljava/lang/Long;)V

    .line 3193
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$16;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->exitSearch()V

    .line 3194
    return-void
.end method
