.class public final enum Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
.super Ljava/lang/Enum;
.source "BooksDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/BooksDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentStatusEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

.field public static final enum FORBIDDEN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

.field public static final enum LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

.field public static final enum NETWORK:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

.field public static final enum UNKNOWN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;


# instance fields
.field private final mDbValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 189
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->UNKNOWN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 191
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    const-string v1, "FORBIDDEN"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->FORBIDDEN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 193
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->NETWORK:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 195
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 187
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->UNKNOWN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->FORBIDDEN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->NETWORK:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->$VALUES:[Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "dbValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 199
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 200
    iput p3, p0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->mDbValue:I

    .line 201
    return-void
.end method

.method public static fromInteger(I)Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .locals 5
    .param p0, "value"    # I

    .prologue
    .line 211
    invoke-static {}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->values()[Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 212
    .local v3, "status":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    invoke-virtual {v3}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->getDbValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 216
    .end local v3    # "status":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :goto_1
    return-object v3

    .line 211
    .restart local v3    # "status":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    .end local v3    # "status":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 187
    const-class v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->$VALUES:[Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    return-object v0
.end method


# virtual methods
.method public getDbValue()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->mDbValue:I

    return v0
.end method
