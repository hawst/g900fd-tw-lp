.class public interface abstract Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;
.super Ljava/lang/Object;
.source "BookmarkController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BookmarkController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BookmarkListener"
.end annotation


# virtual methods
.method public abstract onBookmarksChanged(Ljava/util/SortedMap;Ljava/util/SortedMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/util/SortedMap",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation
.end method
