.class Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;
.super Ljava/lang/Object;
.source "BooksImageManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksImageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImageTask"
.end annotation


# instance fields
.field private final mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

.field private final mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

.field private final mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

.field final synthetic this$0:Lcom/google/android/apps/books/app/BooksImageManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/BooksImageManager;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/ublib/util/ImageSpecifier;Lcom/google/android/apps/books/model/RemoteFileCache;)V
    .locals 1
    .param p2, "ensurer"    # Lcom/google/android/apps/books/common/ImageManager$Ensurer;
    .param p3, "spec"    # Lcom/google/android/ublib/util/ImageSpecifier;
    .param p4, "imageStore"    # Lcom/google/android/apps/books/model/RemoteFileCache;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object p4, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    .line 242
    if-nez p3, :cond_0

    .line 243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 245
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    .line 246
    iput-object p3, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    .line 247
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;)Lcom/google/android/ublib/util/ImageSpecifier;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 254
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;
    invoke-static {v12}, Lcom/google/android/apps/books/app/BooksImageManager;->access$000(Lcom/google/android/apps/books/app/BooksImageManager;)Ljava/util/HashMap;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-virtual {v12, v13}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 255
    const-string v12, "BooksImageManager"

    const/4 v13, 0x2

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 256
    const-string v12, "BooksImageManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Image cancelled: skip fetch and cache: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    const/4 v0, 0x0

    .line 262
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 264
    .local v4, "exception":Ljava/lang/Throwable;
    :try_start_0
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    if-eqz v12, :cond_2

    .line 265
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mEnsurer:Lcom/google/android/apps/books/common/ImageManager$Ensurer;

    invoke-interface {v12}, Lcom/google/android/apps/books/common/ImageManager$Ensurer;->ensure()V

    .line 268
    :cond_2
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v12, v12, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    # invokes: Lcom/google/android/apps/books/app/BooksImageManager;->isRemoteUri(Landroid/net/Uri;)Z
    invoke-static {v12}, Lcom/google/android/apps/books/app/BooksImageManager;->access$100(Landroid/net/Uri;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 269
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v12, :cond_4

    .line 270
    const/4 v5, 0x0

    .line 272
    .local v5, "input":Ljava/io/InputStream;
    :try_start_1
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    iget-object v13, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v13, v13, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    invoke-virtual {v12, v13}, Lcom/google/android/apps/books/model/RemoteFileCache;->getFile(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    .line 273
    if-eqz v5, :cond_3

    .line 274
    invoke-static {v5}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_3

    const-string v12, "BooksImageManager"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 276
    const-string v12, "BooksImageManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Retrieved cached cover "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    :cond_3
    if-eqz v5, :cond_4

    :try_start_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 284
    .end local v5    # "input":Ljava/io/InputStream;
    :cond_4
    if-nez v0, :cond_7

    .line 289
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    if-eqz v12, :cond_a

    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v12, v12, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    if-eqz v12, :cond_a

    .line 290
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v12, v12, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    iget-object v13, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v13, v13, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-static {v12, v13}, Lcom/google/android/apps/books/util/OceanUris;->appendFifeSizeOption(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;)Landroid/net/Uri;

    move-result-object v11

    .line 296
    .local v11, "uriPossiblyWithSize":Landroid/net/Uri;
    :goto_1
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 297
    .local v7, "req":Lorg/apache/http/client/methods/HttpGet;
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;
    invoke-static {v12}, Lcom/google/android/apps/books/app/BooksImageManager;->access$200(Lcom/google/android/apps/books/app/BooksImageManager;)Lcom/google/android/apps/books/net/ResponseGetter;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-array v14, v14, [I

    invoke-interface {v12, v7, v13, v14}, Lcom/google/android/apps/books/net/ResponseGetter;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 298
    .local v8, "resp":Lorg/apache/http/HttpResponse;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    .line 301
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    :try_start_3
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 302
    .restart local v5    # "input":Ljava/io/InputStream;
    invoke-static {v5}, Lcom/google/android/apps/books/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    .line 305
    .local v1, "bytes":[B
    const/4 v12, 0x0

    :try_start_4
    array-length v13, v1

    invoke-static {v1, v12, v13}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 306
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    if-eqz v12, :cond_5

    .line 308
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    iget-object v13, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v13, v13, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    invoke-virtual {v12, v13, v1}, Lcom/google/android/apps/books/model/RemoteFileCache;->setFile(Landroid/net/Uri;[B)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 311
    :cond_5
    if-eqz v5, :cond_6

    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 314
    :cond_6
    :try_start_6
    invoke-static {v3}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_2

    .line 343
    .end local v1    # "bytes":[B
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v5    # "input":Ljava/io/InputStream;
    .end local v7    # "req":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "resp":Lorg/apache/http/HttpResponse;
    .end local v11    # "uriPossiblyWithSize":Landroid/net/Uri;
    :cond_7
    :goto_2
    if-eqz v0, :cond_11

    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v12, v12, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    if-eqz v12, :cond_11

    .line 344
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mCallbacks:Ljava/util/HashMap;
    invoke-static {v12}, Lcom/google/android/apps/books/app/BooksImageManager;->access$000(Lcom/google/android/apps/books/app/BooksImageManager;)Ljava/util/HashMap;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-virtual {v12, v13}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_f

    .line 345
    const-string v12, "BooksImageManager"

    const/4 v13, 0x2

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 346
    const-string v12, "BooksImageManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Image cancelled: skip scale and cache: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 280
    .restart local v5    # "input":Ljava/io/InputStream;
    :catchall_0
    move-exception v12

    if-eqz v5, :cond_8

    :try_start_7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    :cond_8
    throw v12
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_2

    .line 325
    .end local v5    # "input":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 326
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    const-string v12, "BooksImageManager"

    const/4 v13, 0x6

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 327
    const-string v12, "BooksImageManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Insufficient memory to load image: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 329
    :cond_9
    move-object v4, v2

    .line 341
    goto :goto_2

    .line 293
    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    :cond_a
    :try_start_8
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v11, v12, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_2

    .restart local v11    # "uriPossiblyWithSize":Landroid/net/Uri;
    goto/16 :goto_1

    .line 311
    .restart local v1    # "bytes":[B
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v5    # "input":Ljava/io/InputStream;
    .restart local v7    # "req":Lorg/apache/http/client/methods/HttpGet;
    .restart local v8    # "resp":Lorg/apache/http/HttpResponse;
    :catchall_1
    move-exception v12

    if-eqz v5, :cond_b

    :try_start_9
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    :cond_b
    throw v12
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 314
    .end local v1    # "bytes":[B
    .end local v5    # "input":Ljava/io/InputStream;
    :catchall_2
    move-exception v12

    :try_start_a
    invoke-static {v3}, Lcom/google/android/apps/books/net/HttpHelper;->consumeContentAndException(Lorg/apache/http/HttpEntity;)V

    throw v12
    :try_end_a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_2

    .line 330
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "req":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "resp":Lorg/apache/http/HttpResponse;
    .end local v11    # "uriPossiblyWithSize":Landroid/net/Uri;
    :catch_1
    move-exception v2

    .line 331
    .local v2, "e":Ljava/io/IOException;
    const-string v12, "BooksImageManager"

    const/4 v13, 0x6

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 332
    const-string v12, "BooksImageManager"

    const-string v13, "IO error while loading image"

    invoke-static {v12, v13, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 334
    :cond_c
    move-object v4, v2

    .line 341
    goto/16 :goto_2

    .line 319
    .end local v2    # "e":Ljava/io/IOException;
    :cond_d
    :try_start_b
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v12}, Lcom/google/android/apps/books/app/BooksImageManager;->access$300(Lcom/google/android/apps/books/app/BooksImageManager;)Landroid/content/ContentResolver;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v13, v13, Lcom/google/android/ublib/util/ImageSpecifier;->uri:Landroid/net/Uri;

    invoke-virtual {v12, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 320
    .local v6, "is":Ljava/io/InputStream;
    if-eqz v6, :cond_7

    .line 321
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 322
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_2

    goto/16 :goto_2

    .line 336
    .end local v6    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v2

    .line 337
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v12, "BooksImageManager"

    const/4 v13, 0x6

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 338
    const-string v12, "BooksImageManager"

    const-string v13, "IllegalArgumentException while loading image"

    invoke-static {v12, v13, v2}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 340
    :cond_e
    move-object v4, v2

    goto/16 :goto_2

    .line 350
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :cond_f
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->mImageSpecifier:Lcom/google/android/ublib/util/ImageSpecifier;

    iget-object v12, v12, Lcom/google/android/ublib/util/ImageSpecifier;->constraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-static {v0, v12}, Lcom/google/android/apps/books/util/BitmapUtils;->shrinkToConstraints(Landroid/graphics/Bitmap;Lcom/google/android/ublib/util/ImageConstraints;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 358
    :cond_10
    :goto_3
    move-object v10, v4

    .line 359
    .local v10, "resultException":Ljava/lang/Throwable;
    move-object v9, v0

    .line 360
    .local v9, "resultBitmap":Landroid/graphics/Bitmap;
    iget-object v12, p0, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;->this$0:Lcom/google/android/apps/books/app/BooksImageManager;

    # getter for: Lcom/google/android/apps/books/app/BooksImageManager;->mCallbackHandler:Landroid/os/Handler;
    invoke-static {v12}, Lcom/google/android/apps/books/app/BooksImageManager;->access$700(Lcom/google/android/apps/books/app/BooksImageManager;)Landroid/os/Handler;

    move-result-object v12

    new-instance v13, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;

    invoke-direct {v13, p0, v9, v10}, Lcom/google/android/apps/books/app/BooksImageManager$ImageTask$1;-><init>(Lcom/google/android/apps/books/app/BooksImageManager$ImageTask;Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 352
    .end local v9    # "resultBitmap":Landroid/graphics/Bitmap;
    .end local v10    # "resultException":Ljava/lang/Throwable;
    :cond_11
    if-eqz v0, :cond_10

    const-string v12, "BooksImageManager"

    const/4 v13, 0x2

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 353
    const-string v12, "BooksImageManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unscaled image size ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
