.class Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;
.super Landroid/widget/ImageView;
.source "HtmlSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/HtmlSpreadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WebViewMirror"
.end annotation


# instance fields
.field private final mBackgroundPaint:Landroid/graphics/Paint;

.field private mMirror:Landroid/graphics/Bitmap;

.field private mMirrorCanvas:Landroid/graphics/Canvas;

.field private mMirrorInvalid:Z

.field private final mPageTransform:Landroid/graphics/Matrix;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HtmlSpreadView;Landroid/content/Context;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "backgroundColor"    # I

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    .line 386
    invoke-direct {p0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 378
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mPageTransform:Landroid/graphics/Matrix;

    .line 379
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mBackgroundPaint:Landroid/graphics/Paint;

    .line 383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorInvalid:Z

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 388
    return-void
.end method

.method private maybeRecreateCanvas()V
    .locals 4

    .prologue
    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->getMeasuredWidth()I

    move-result v1

    .line 406
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->getMeasuredHeight()I

    move-result v0

    .line 408
    .local v0, "height":I
    if-lez v1, :cond_0

    if-lez v0, :cond_0

    .line 409
    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    .line 410
    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 416
    :cond_2
    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->MIRROR_BITMAP_CONFIG:Landroid/graphics/Bitmap$Config;
    invoke-static {}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$800()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    .line 417
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    .line 418
    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirror:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 419
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->invalidateMirror()V

    goto :goto_0
.end method

.method private updateMirror()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageData:[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$600(Lcom/google/android/apps/books/widget/HtmlSpreadView;)[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;

    move-result-object v6

    .local v6, "arr$":[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    move v9, v8

    .end local v6    # "arr$":[Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    .end local v8    # "i$":I
    .end local v10    # "len$":I
    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_5

    aget-object v13, v6, v9

    .line 427
    .local v13, "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mPageTransform:Landroid/graphics/Matrix;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mPageFit:Landroid/graphics/Matrix;
    invoke-static {v13}, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->access$900(Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 430
    iget-object v0, v13, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserScale:F
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$1000(Lcom/google/android/apps/books/widget/HtmlSpreadView;)F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mPageTransform:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    iget-object v2, v13, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v2

    iget-object v2, v13, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContentSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v4, v2

    iget-object v5, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mBackgroundPaint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 440
    :cond_1
    iget-object v0, v13, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    if-eqz v0, :cond_4

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mPageTransform:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mUserZoom:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$1100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mPageTransform:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 447
    iget-object v7, v13, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mMasks:[Landroid/graphics/Rect;

    .local v7, "arr$":[Landroid/graphics/Rect;
    array-length v11, v7

    .local v11, "len$":I
    const/4 v8, 0x0

    .end local v9    # "i$":I
    .restart local v8    # "i$":I
    :goto_1
    if-ge v8, v11, :cond_3

    aget-object v12, v7, v8

    .line 448
    .local v12, "mask":Landroid/graphics/Rect;
    invoke-virtual {v12}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v12, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 447
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 454
    .end local v12    # "mask":Landroid/graphics/Rect;
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->this$0:Lcom/google/android/apps/books/widget/HtmlSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/HtmlSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/HtmlSpreadView;->access$100(Lcom/google/android/apps/books/widget/HtmlSpreadView;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 456
    iget-object v0, v13, Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;->mContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/util/SimpleDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 425
    .end local v7    # "arr$":[Landroid/graphics/Rect;
    .end local v8    # "i$":I
    .end local v11    # "len$":I
    :cond_4
    add-int/lit8 v8, v9, 0x1

    .restart local v8    # "i$":I
    move v9, v8

    .end local v8    # "i$":I
    .restart local v9    # "i$":I
    goto/16 :goto_0

    .line 460
    .end local v13    # "page":Lcom/google/android/apps/books/widget/HtmlSpreadView$PageData;
    :cond_5
    return-void
.end method


# virtual methods
.method invalidateMirror()V
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorInvalid:Z

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->invalidate()V

    .line 393
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 465
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorInvalid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorCanvas:Landroid/graphics/Canvas;

    if-eqz v0, :cond_0

    .line 466
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->updateMirror()V

    .line 467
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->mMirrorInvalid:Z

    .line 470
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 471
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 397
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 399
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/HtmlSpreadView$WebViewMirror;->maybeRecreateCanvas()V

    .line 400
    return-void
.end method
