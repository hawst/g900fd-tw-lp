.class public Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
.super Ljava/lang/Object;
.source "SingleBookUploaderImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/upload/SingleBookUploader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$4;,
        Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;,
        Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$JsonCloudloadResponse;
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mBackoffSync:Ljava/lang/Runnable;

.field private final mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

.field private final mContext:Landroid/content/Context;

.field private mProcessingDone:Z

.field private final mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

.field private mTransfer:Lcom/google/uploader/client/Transfer;

.field private final mUpload:Lcom/google/android/apps/books/upload/Upload;

.field private mUploadStarted:Z

.field private mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;Lcom/google/android/apps/books/upload/Upload;Lcom/google/android/apps/books/upload/UploadDataStorage;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "controller"    # Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    .param p4, "upload"    # Lcom/google/android/apps/books/upload/Upload;
    .param p5, "storage"    # Lcom/google/android/apps/books/upload/UploadDataStorage;
    .param p6, "scheduler"    # Ljava/util/concurrent/ScheduledExecutorService;

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-boolean v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStarted:Z

    .line 57
    iput-boolean v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mProcessingDone:Z

    .line 61
    new-instance v0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;-><init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V

    iput-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mBackoffSync:Ljava/lang/Runnable;

    .line 113
    iput-object p1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;

    .line 114
    iput-object p2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mAccount:Landroid/accounts/Account;

    .line 115
    iput-object p3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    .line 116
    iput-object p5, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

    .line 117
    new-instance v0, Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v0, p4}, Lcom/google/android/apps/books/upload/Upload;-><init>(Lcom/google/android/apps/books/upload/Upload;)V

    iput-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    .line 118
    iput-object p6, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 119
    return-void
.end method

.method private abortAndCleanUp(Ljava/lang/Exception;Z)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "processingError"    # Z

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->abortUpload(Ljava/lang/String;Ljava/lang/Exception;Z)V

    .line 209
    if-eqz p2, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->tryDeleteFromServer()V

    .line 212
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->cleanUp()V

    .line 213
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mProcessingDone:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mProcessingDone:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/uploader/client/Transfer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->sendApiaryRequest()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->deleteAndCleanUp()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/Upload;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Ljava/lang/Exception;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
    .param p1, "x1"    # Ljava/lang/Exception;
    .param p2, "x2"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mScheduler:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->getScottyContentId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->handleApiaryResponse(Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;)V

    return-void
.end method

.method private buildSettings()Lcom/google/uploader/client/UploadSettings;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    new-instance v4, Lcom/google/uploader/client/UploadSettings$Builder;

    invoke-direct {v4}, Lcom/google/uploader/client/UploadSettings$Builder;-><init>()V

    .line 194
    .local v4, "settingsBuilder":Lcom/google/uploader/client/UploadSettings$Builder;
    iget-object v6, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/apps/books/app/BooksApplication;->getApiaryClient(Landroid/content/Context;)Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v0

    .line 195
    .local v0, "apiaryClient":Lcom/google/android/apps/books/api/ApiaryClient;
    invoke-interface {v0}, Lcom/google/android/apps/books/api/ApiaryClient;->getHttpHelper()Lcom/google/android/apps/books/net/HttpHelper;

    move-result-object v3

    .line 197
    .local v3, "httpHelper":Lcom/google/android/apps/books/net/HttpHelper;
    iget-object v6, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/books/net/HttpHelper;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    .line 198
    .local v1, "authToken":Ljava/lang/String;
    new-instance v2, Lcom/google/api/client/http/HttpHeaders;

    invoke-direct {v2}, Lcom/google/api/client/http/HttpHeaders;-><init>()V

    .line 199
    .local v2, "headers":Lcom/google/api/client/http/HttpHeaders;
    invoke-virtual {v3, v2, v1}, Lcom/google/android/apps/books/net/HttpHelper;->setAuthToken(Lcom/google/api/client/http/HttpHeaders;Ljava/lang/String;)V

    .line 201
    sget-object v6, Lcom/google/android/apps/books/util/ConfigValue;->UPLOAD_URL:Lcom/google/android/apps/books/util/ConfigValue;

    iget-object v7, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/util/ConfigValue;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 202
    .local v5, "uploadsUrl":Ljava/lang/String;
    invoke-virtual {v4, v2}, Lcom/google/uploader/client/UploadSettings$Builder;->setHeaders(Lcom/google/api/client/http/HttpHeaders;)Lcom/google/uploader/client/UploadSettings$Builder;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file_name="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v8}, Lcom/google/android/apps/books/upload/Upload;->getOrigFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/uploader/client/UploadSettings$Builder;->setMetadata(Ljava/lang/String;)Lcom/google/uploader/client/UploadSettings$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/uploader/client/UploadSettings$Builder;->build(Ljava/lang/String;)Lcom/google/uploader/client/UploadSettings;

    move-result-object v6

    return-object v6
.end method

.method private cleanUp()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mProcessingDone:Z

    .line 233
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    .line 234
    return-void
.end method

.method private deleteAndCleanUp()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->deleteUpload(Ljava/lang/String;)V

    .line 228
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->cleanUp()V

    .line 229
    return-void
.end method

.method private getScottyContentId()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    check-cast v0, Lcom/google/android/libraries/uploader/AndroidTransfer;

    .line 262
    .local v0, "androidTransfer":Lcom/google/android/libraries/uploader/AndroidTransfer;
    invoke-virtual {v0}, Lcom/google/android/libraries/uploader/AndroidTransfer;->getResponseBody()Ljava/io/InputStream;

    move-result-object v1

    .line 263
    .local v1, "inputStream":Ljava/io/InputStream;
    new-instance v3, Lcom/google/api/client/json/JsonObjectParser;

    new-instance v4, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v4}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-direct {v3, v4}, Lcom/google/api/client/json/JsonObjectParser;-><init>(Lcom/google/api/client/json/JsonFactory;)V

    const-string v4, "UTF-8"

    invoke-static {v4}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v4

    const-class v5, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$JsonCloudloadResponse;

    invoke-virtual {v3, v1, v4, v5}, Lcom/google/api/client/json/JsonObjectParser;->parseAndClose(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$JsonCloudloadResponse;

    .line 265
    .local v2, "scottyResponse":Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$JsonCloudloadResponse;
    iget-object v3, v2, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$JsonCloudloadResponse;->contentId:Ljava/lang/String;

    return-object v3
.end method

.method private handleApiaryResponse(Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 238
    :try_start_0
    iget-object v2, p1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;->processingState:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;

    move-result-object v1

    .line 240
    .local v1, "state":Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;
    sget-object v2, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$4;->$SwitchMap$com$google$android$apps$books$api$data$CloudloadingAddBookResponse$ProcessingStateValues:[I

    invoke-virtual {v1}, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 247
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 248
    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;->volumeId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateVolumeId(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    iget-object v3, p1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;->volumeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/upload/Upload;->setVolumeId(Ljava/lang/String;)V

    .line 250
    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    sget-object v3, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/upload/Upload;->setUploadStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 252
    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mBackoffSync:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 258
    return-void

    .line 242
    :pswitch_0
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Processing on the books server failed"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V

    .line 244
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_COMPLETE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 245
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->deleteAndCleanUp()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 254
    .end local v1    # "state":Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse$ProcessingStateValues;
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown processing state recieved from apiary server: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/apps/books/api/data/CloudloadingAddBookResponse;->processingState:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendApiaryRequest()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mCallbacks:Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;

    new-instance v1, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$3;-><init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;->scheduleBackgroundTask(Ljava/lang/Runnable;)V

    .line 290
    return-void
.end method

.method private tryDeleteFromServer()V
    .locals 3

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getServer(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v0

    .line 218
    .local v0, "server":Lcom/google/android/apps/books/net/BooksServer;
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v1}, Lcom/google/android/apps/books/upload/Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/net/BooksServer;->deleteCloudloadedVolume(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public cancelUpload()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    invoke-interface {v0}, Lcom/google/uploader/client/Transfer;->cancel()Ljava/util/concurrent/Future;

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->cancel()V

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_PROCESSING:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v0}, Lcom/google/android/apps/books/upload/Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 337
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->tryDeleteFromServer()V

    .line 339
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    sget-object v1, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_CANCELED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/upload/Upload;->setUploadStatus(Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 340
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->cleanUp()V

    .line 341
    return-void
.end method

.method public pauseUpload()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    invoke-interface {v0}, Lcom/google/uploader/client/Transfer;->pause()Ljava/util/concurrent/Future;

    .line 348
    :cond_0
    return-void
.end method

.method public processingCompleted()V
    .locals 0

    .prologue
    .line 359
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->cleanUp()V

    .line 360
    return-void
.end method

.method public resumeUpload()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    invoke-interface {v0}, Lcom/google/uploader/client/Transfer;->run()Ljava/util/concurrent/Future;

    .line 355
    :cond_0
    return-void
.end method

.method public startUpload()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 123
    iget-boolean v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStarted:Z

    if-eqz v3, :cond_0

    .line 190
    :goto_0
    return-void

    .line 126
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStarted:Z

    .line 128
    sget-object v3, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$4;->$SwitchMap$com$google$android$apps$books$upload$proto$UploadProto$Upload$UploadStatus:[I

    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v4}, Lcom/google/android/apps/books/upload/Upload;->getUploadStatus()Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 132
    :pswitch_0
    new-instance v1, Lcom/google/android/libraries/uploader/AndroidClient;

    new-instance v3, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$ScottyHandler;-><init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$1;)V

    invoke-direct {v1, v3}, Lcom/google/android/libraries/uploader/AndroidClient;-><init>(Lcom/google/android/libraries/uploader/UploadHandler;)V

    .line 133
    .local v1, "scottyClient":Lcom/google/uploader/client/Client;
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v4}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/upload/UploadDataStorage;->getFileInputStream(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    .line 134
    .local v0, "inputStream":Ljava/io/InputStream;
    new-instance v3, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    invoke-direct {v3, v0}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    .line 135
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    new-instance v4, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl$2;-><init>(Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;->registerProgressListener(Lcom/google/android/apps/books/util/ProgressUpdatingInputStream$InputProgressListener;)V

    .line 145
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 146
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v3}, Lcom/google/android/apps/books/upload/Upload;->getTransferHandle()Lcom/google/uploader/client/ClientProto$TransferHandle;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    invoke-interface {v1, v3, v4}, Lcom/google/uploader/client/Client;->initTransfer(Lcom/google/uploader/client/ClientProto$TransferHandle;Ljava/io/InputStream;)Lcom/google/uploader/client/Transfer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    .line 156
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    invoke-interface {v3}, Lcom/google/uploader/client/Transfer;->run()Ljava/util/concurrent/Future;

    goto :goto_0

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->buildSettings()Lcom/google/uploader/client/UploadSettings;

    move-result-object v2

    .line 150
    .local v2, "uploadSettings":Lcom/google/uploader/client/UploadSettings;
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUploadStream:Lcom/google/android/apps/books/util/ProgressUpdatingInputStream;

    invoke-interface {v1, v2, v3}, Lcom/google/uploader/client/Client;->initTransfer(Lcom/google/uploader/client/UploadSettings;Ljava/io/InputStream;)Lcom/google/uploader/client/Transfer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mTransfer:Lcom/google/uploader/client/Transfer;

    goto :goto_1

    .line 160
    .end local v0    # "inputStream":Ljava/io/InputStream;
    .end local v1    # "scottyClient":Lcom/google/uploader/client/Client;
    .end local v2    # "uploadSettings":Lcom/google/uploader/client/UploadSettings;
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->sendApiaryRequest()V

    goto :goto_0

    .line 165
    :pswitch_2
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V

    goto :goto_0

    .line 169
    :pswitch_3
    const-string v3, "unknown"

    iget-object v4, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v4}, Lcom/google/android/apps/books/upload/Upload;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 172
    const-string v3, "SingleBookUploaderImpl"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 173
    const-string v3, "SingleBookUploaderImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Volume ID missing after apiary call. Deleting this upload: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mUpload:Lcom/google/android/apps/books/upload/Upload;

    invoke-virtual {v5}, Lcom/google/android/apps/books/upload/Upload;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->deleteAndCleanUp()V

    goto/16 :goto_0

    .line 178
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->mBackoffSync:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0

    .line 183
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->deleteAndCleanUp()V

    goto/16 :goto_0

    .line 187
    :pswitch_5
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-direct {p0, v3, v5}, Lcom/google/android/apps/books/upload/SingleBookUploaderImpl;->abortAndCleanUp(Ljava/lang/Exception;Z)V

    goto/16 :goto_0

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
