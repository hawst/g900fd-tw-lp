.class public Lcom/google/android/apps/books/sync/SyncControllerImpl;
.super Ljava/lang/Object;
.source "SyncControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/sync/SyncController;


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Landroid/content/ContentResolver;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "resolver"    # Landroid/content/ContentResolver;
    .param p3, "backgroundExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mResolver:Landroid/content/ContentResolver;

    .line 43
    iput-object p3, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/sync/SyncControllerImpl;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/sync/SyncControllerImpl;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->requestSync(Landroid/os/Bundle;)V

    return-void
.end method

.method private disablePeriodicSync()V
    .locals 8

    .prologue
    .line 178
    iget-object v3, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    const-string v4, "com.google.android.apps.books"

    invoke-static {v3, v4}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 180
    .local v1, "periodicSyncs":Ljava/util/List;, "Ljava/util/List<Landroid/content/PeriodicSync;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/PeriodicSync;

    .line 181
    .local v2, "sync":Landroid/content/PeriodicSync;
    const-string v3, "SyncController"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 182
    const-string v3, "SyncController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disabling periodic sync with interval "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v2, Landroid/content/PeriodicSync;->period:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    const-string v4, "com.google.android.apps.books"

    iget-object v5, v2, Landroid/content/PeriodicSync;->extras:Landroid/os/Bundle;

    invoke-static {v3, v4, v5}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 186
    .end local v2    # "sync":Landroid/content/PeriodicSync;
    :cond_1
    return-void
.end method

.method private static makeManualSyncExtras()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    return-object v0
.end method

.method private static varargs makeVolumeContentSyncExtras(Z[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p0, "displayProgress"    # Z
    .param p1, "volumeIds"    # [Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->makeManualSyncExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 68
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "SyncService.DISPLAY_PROGRESS"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 69
    invoke-static {v0, p1}, Lcom/google/android/apps/books/service/SyncService;->setVolumeIds(Landroid/os/Bundle;[Ljava/lang/String;)V

    .line 70
    return-object v0
.end method

.method private maybeUpdateSubscribedFeeds(Lcom/google/android/apps/books/util/Config;)V
    .locals 18
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 124
    const-wide/16 v2, 0x7

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/util/OceanUris;->getCollectionVolumesUrl(Lcom/google/android/apps/books/util/Config;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    .line 128
    .local v16, "subscriptionUrl":Ljava/lang/String;
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v11

    .line 129
    .local v11, "existingFeeds":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "feed"

    aput-object v3, v4, v2

    .line 132
    .local v4, "projection":[Ljava/lang/String;
    const-string v17, "_sync_account=? AND _sync_account_type=? AND authority=?"

    .line 135
    .local v17, "where":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v6, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v6, v2

    const/4 v2, 0x2

    const-string v3, "com.google.android.apps.books"

    aput-object v3, v6, v2

    .line 138
    .local v6, "values":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/gsf/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "_sync_account=? AND _sync_account_type=? AND authority=?"

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 141
    .local v8, "c":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 143
    .local v14, "id":J
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 144
    .local v12, "feed":Ljava/lang/String;
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v11, v12, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 147
    .end local v12    # "feed":Ljava/lang/String;
    .end local v14    # "id":J
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 151
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 153
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 154
    .local v9, "contentValues":Landroid/content/ContentValues;
    const-string v2, "_sync_account"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v2, "_sync_account_type"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v2, "feed"

    move-object/from16 v0, v16

    invoke-virtual {v9, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v2, "service"

    const-string v3, "print"

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    const-string v2, "authority"

    const-string v3, "com.google.android.apps.books"

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/gsf/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 167
    .end local v9    # "contentValues":Landroid/content/ContentValues;
    :goto_1
    invoke-virtual {v11}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 168
    .local v10, "existing":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 169
    .restart local v14    # "id":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/gsf/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    .line 163
    .end local v10    # "existing":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "id":J
    :cond_1
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 172
    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private requestSync(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    const-string v1, "com.google.android.apps.books"

    invoke-static {v0, v1, p1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 107
    return-void
.end method

.method private requestSyncAsync(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/sync/SyncControllerImpl$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/sync/SyncControllerImpl$1;-><init>(Lcom/google/android/apps/books/sync/SyncControllerImpl;Landroid/os/Bundle;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 103
    return-void
.end method


# virtual methods
.method public getSyncAutomatically()Z
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    const-string v1, "com.google.android.apps.books"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public requestManualSync()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->requestManualSync(Z)V

    .line 49
    return-void
.end method

.method public requestManualSync(Z)V
    .locals 2
    .param p1, "download"    # Z

    .prologue
    .line 53
    invoke-static {}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->makeManualSyncExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "SyncService.DOWNLOAD"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 55
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->requestSyncAsync(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method public requestManualUploadOnlySync()V
    .locals 3

    .prologue
    .line 60
    invoke-static {}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->makeManualSyncExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "upload"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 62
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->requestSyncAsync(Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method public varargs requestManualVolumeContentSync(Z[Ljava/lang/String;)V
    .locals 1
    .param p1, "displayProgress"    # Z
    .param p2, "volumeIds"    # [Ljava/lang/String;

    .prologue
    .line 92
    invoke-static {p1, p2}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->makeVolumeContentSyncExtras(Z[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 93
    .local v0, "extras":Landroid/os/Bundle;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->requestSync(Landroid/os/Bundle;)V

    .line 94
    return-void
.end method

.method public varargs requestManualVolumeContentSyncAsync(Z[Ljava/lang/String;)V
    .locals 1
    .param p1, "displayProgress"    # Z
    .param p2, "volumeIds"    # [Ljava/lang/String;

    .prologue
    .line 85
    invoke-static {p1, p2}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->makeVolumeContentSyncExtras(Z[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 86
    .local v0, "extras":Landroid/os/Bundle;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->requestSyncAsync(Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method public setIsSyncable(Z)V
    .locals 3
    .param p1, "isSyncable"    # Z

    .prologue
    .line 200
    iget-object v1, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    const-string v2, "com.google.android.apps.books"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 201
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSyncAutomatically(Z)V
    .locals 2
    .param p1, "isAutomatic"    # Z

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncControllerImpl;->mAccount:Landroid/accounts/Account;

    const-string v1, "com.google.android.apps.books"

    invoke-static {v0, v1, p1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 196
    return-void
.end method

.method public updateSyncSettings(Lcom/google/android/apps/books/util/Config;)V
    .locals 0
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->maybeUpdateSubscribedFeeds(Lcom/google/android/apps/books/util/Config;)V

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncControllerImpl;->disablePeriodicSync()V

    .line 114
    return-void
.end method
