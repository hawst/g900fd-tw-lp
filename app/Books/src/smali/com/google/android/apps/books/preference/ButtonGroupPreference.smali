.class public Lcom/google/android/apps/books/preference/ButtonGroupPreference;
.super Landroid/widget/LinearLayout;
.source "ButtonGroupPreference.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference;


# instance fields
.field private mBinding:Z

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private mCurrentView:Landroid/view/View;

.field private final mKey:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    iput-boolean v2, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mBinding:Z

    .line 121
    new-instance v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference$1;-><init>(Lcom/google/android/apps/books/preference/ButtonGroupPreference;)V

    iput-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mClickListener:Landroid/view/View$OnClickListener;

    .line 38
    sget-object v1, Lcom/google/android/apps/books/R$styleable;->Preference:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 40
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mKey:Ljava/lang/String;

    .line 41
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/preference/ButtonGroupPreference;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mBinding:Z

    return v0
.end method


# virtual methods
.method public bindPreference()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mBinding:Z

    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mKey:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->setValue(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mBinding:Z

    .line 108
    return-void

    .line 106
    :catchall_0
    move-exception v0

    iput-boolean v3, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mBinding:Z

    throw v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 57
    .local v0, "title":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    .end local v0    # "title":Ljava/lang/CharSequence;
    :cond_0
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->getChildCount()I

    move-result v0

    .line 49
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 50
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public persistPreference()V
    .locals 4

    .prologue
    .line 112
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 114
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 116
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    if-eqz v1, :cond_1

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    iget-object v2, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mKey:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;->onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 119
    :cond_1
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->getChildCount()I

    move-result v0

    .line 69
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 70
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 84
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mCurrentView:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 89
    :cond_1
    return-void
.end method

.method public setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V
    .locals 0
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "listener"    # Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    .line 94
    iput-object p2, p0, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->bindPreference()V

    .line 96
    return-void
.end method
