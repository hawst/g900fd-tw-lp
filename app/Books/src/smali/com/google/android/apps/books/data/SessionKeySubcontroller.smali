.class public Lcom/google/android/apps/books/data/SessionKeySubcontroller;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/SessionKeySubcontroller$ProductionKeyValidator;,
        Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;,
        Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;
    }
.end annotation


# static fields
.field private static final ADD_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

.field private static final NOOP:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

.field private static final UPGRADE_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;


# instance fields
.field private final mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

.field private final mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerMap",
            "<",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final mKeyIdToKey:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final mKeyValidator:Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$2;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->NOOP:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    .line 200
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$5;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$5;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->ADD_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    .line 243
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$7;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$7;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->UPGRADE_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$ProductionKeyValidator;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$ProductionKeyValidator;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;)V

    .line 74
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;)V
    .locals 1
    .param p1, "keyValidator"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->get()Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    .line 61
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyIdToKey:Ljava/util/Map;

    .line 64
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->createExceptionOrMap()Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyValidator:Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/model/AccountSessionKeyId;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->upgradeKeyOnNetworkThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->handleNewAccountKeyOnControlThread(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .param p2, "x2"    # Ljava/lang/Exception;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p2, "x2"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->handleUpgradedKeyOnControlThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/model/DataControllerStore;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;

    .prologue
    .line 37
    invoke-static {p0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->maybeInitializeUserKey(Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method static synthetic access$700()Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->ADD_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;
    .param p4, "x4"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;
    .param p5, "x5"    # Ljava/lang/Runnable;

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->checkAndPublishKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$900()Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->UPGRADE_KEY:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    return-object v0
.end method

.method private checkAndPublishKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p4, "processor"    # Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;
    .param p5, "onWrongRootKeyException"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;",
            "Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    .local p2, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyValidator:Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$SessionKeyValidator;->validate(Lcom/google/android/apps/books/model/SessionKey;)V

    .line 140
    invoke-interface {p4, p1, p2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;->process(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v1

    .line 141
    .local v1, "processedKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyIdToKey:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-virtual {v2, v3, v1, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishSuccess(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 153
    .end local v1    # "processedKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    if-eqz p5, :cond_0

    .line 145
    invoke-interface {p5}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    .line 149
    .end local v0    # "e":Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;
    :catch_1
    move-exception v0

    .line 150
    .local v0, "e":Ljava/security/GeneralSecurityException;
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-virtual {v2, v3, v0, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private fetchNewAccountKeyOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "callbacks"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;

    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 170
    return-void
.end method

.method private handleNewAccountKeyOnControlThread(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$6;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKey;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 241
    return-void
.end method

.method private handleUpgradedKeyOnControlThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p2, "callbacks"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            ")V"
        }
    .end annotation

    .prologue
    .line 254
    .local p1, "upgradedKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$8;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 260
    return-void
.end method

.method private static maybeInitializeUserKey(Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .param p0, "store"    # Lcom/google/android/apps/books/model/DataControllerStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/DataControllerStore;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/DataControllerStore;->getUserSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    if-nez v0, :cond_0

    .line 227
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/apps/books/model/DataControllerStore;->saveUserSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 229
    :cond_0
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 1
    .param p1, "keyId"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "callbacks"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 264
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$9;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$9;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;)V

    invoke-interface {p3, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 270
    return-void
.end method

.method private upgradeKeyOnNetworkThread(Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p2, "callbacks"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")V"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "savedKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;

    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$4;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 198
    return-void
.end method


# virtual methods
.method public getValidAccountSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 8
    .param p1, "callbacks"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyIdToKey:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 79
    .local v6, "cached":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-eqz v6, :cond_1

    .line 80
    invoke-static {v6}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/model/LocalSessionKey;->load(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v2

    .line 86
    .local v2, "fromStore":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-eqz v2, :cond_2

    .line 87
    new-instance v5, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;

    invoke-direct {v5, p0, p2, v2, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$1;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 101
    .local v5, "onWrongRootKeyException":Ljava/lang/Runnable;
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataControllerStore()Lcom/google/android/apps/books/model/DataControllerStore;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->maybeInitializeUserKey(Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 102
    sget-object v4, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->NOOP:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->checkAndPublishKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 107
    .end local v5    # "onWrongRootKeyException":Ljava/lang/Runnable;
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->addConsumer(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v7

    .line 108
    .local v7, "startNewTask":Z
    if-eqz v7, :cond_0

    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->fetchNewAccountKeyOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;)V

    goto :goto_0
.end method

.method public getValidSessionKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 9
    .param p1, "callbacks"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "keyId"    # Lcom/google/android/apps/books/model/SessionKeyId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/LocalSessionKey<*>;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyIdToKey:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 278
    .local v8, "cached":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-eqz v8, :cond_0

    .line 279
    invoke-static {v8}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {p3, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 310
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/apps/books/model/LocalSessionKey;->load(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v4

    .line 285
    .local v4, "fromStore":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    if-eqz v4, :cond_1

    .line 286
    new-instance v0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$10;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/SessionKeySubcontroller$10;-><init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/ControlTaskServices;)V

    .line 300
    .local v0, "onWrongRootKeyException":Ljava/lang/Runnable;
    sget-object v6, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->NOOP:Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;

    move-object v2, p0

    move-object v3, p1

    move-object v5, p3

    move-object v7, v0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->checkAndPublishKey(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 308
    .end local v0    # "onWrongRootKeyException":Ljava/lang/Runnable;
    :cond_1
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No match found for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {p3, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 2
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 314
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/SessionKeyId;->deleteKeyAndEncryptedContent(Lcom/google/android/apps/books/model/BooksDataStore;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mKeyIdToKey:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    return-void
.end method
