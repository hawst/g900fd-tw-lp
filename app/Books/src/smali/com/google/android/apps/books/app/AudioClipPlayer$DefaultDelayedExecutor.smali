.class Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AudioClipPlayer$DelayedExecutor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultDelayedExecutor"
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer;)V
    .locals 1

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/AudioClipPlayer;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/AudioClipPlayer$1;

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;-><init>(Lcom/google/android/apps/books/app/AudioClipPlayer;)V

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 173
    return-void
.end method

.method public runAfter(Ljava/lang/Runnable;I)V
    .locals 4
    .param p1, "task"    # Ljava/lang/Runnable;
    .param p2, "delayMillis"    # I

    .prologue
    .line 163
    if-gtz p2, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$DefaultDelayedExecutor;->mHandler:Landroid/os/Handler;

    int-to-long v2, p2

    invoke-virtual {v0, p1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
