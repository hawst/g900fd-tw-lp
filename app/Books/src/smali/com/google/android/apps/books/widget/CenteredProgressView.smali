.class public Lcom/google/android/apps/books/widget/CenteredProgressView;
.super Landroid/widget/FrameLayout;
.source "CenteredProgressView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 20
    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/CenteredProgressView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 22
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 23
    .local v0, "progressBar":Landroid/widget/ProgressBar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 24
    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newCenterLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/CenteredProgressView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 25
    return-void
.end method


# virtual methods
.method public setSize(Landroid/graphics/Point;)V
    .locals 3
    .param p1, "size"    # Landroid/graphics/Point;

    .prologue
    .line 29
    if-eqz p1, :cond_0

    .line 30
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 34
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/CenteredProgressView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    return-void

    .line 32
    .end local v0    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    .restart local v0    # "params":Landroid/widget/FrameLayout$LayoutParams;
    goto :goto_0
.end method
