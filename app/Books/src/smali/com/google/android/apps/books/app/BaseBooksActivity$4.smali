.class Lcom/google/android/apps/books/app/BaseBooksActivity$4;
.super Ljava/lang/Object;
.source "BaseBooksActivity.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AccountPicker$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 1067
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$4;, "Lcom/google/android/apps/books/app/BaseBooksActivity.4;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$4;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public finishedPickingAccount(Landroid/accounts/Account;)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 1079
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$4;, "Lcom/google/android/apps/books/app/BaseBooksActivity.4;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$4;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # invokes: Lcom/google/android/apps/books/app/BaseBooksActivity;->handleAccountPickerResult(Landroid/accounts/Account;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$1300(Lcom/google/android/apps/books/app/BaseBooksActivity;Landroid/accounts/Account;)V

    .line 1080
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$4;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getFragmentCallbacks()Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->isActive()Z

    move-result v0

    return v0
.end method

.method public showPreIcsAccountPicker()V
    .locals 6

    .prologue
    .line 1070
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$4;, "Lcom/google/android/apps/books/app/BaseBooksActivity.4;"
    iget-object v2, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$4;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 1071
    .local v0, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 1072
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    const-class v2, Lcom/google/android/apps/books/app/AccountPickerFragment;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$4;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    # getter for: Lcom/google/android/apps/books/app/BaseBooksActivity;->mAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/apps/books/app/BaseBooksActivity;->access$300(Lcom/google/android/apps/books/app/BaseBooksActivity;)Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/app/AccountPickerFragment$Arguments;->create(Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v1, v5}, Lcom/google/android/apps/books/app/BaseBooksActivity;->createAndAddFragment(Ljava/lang/Class;Ljava/lang/String;Landroid/os/Bundle;Landroid/support/v4/app/FragmentTransaction;Z)Landroid/support/v4/app/Fragment;

    .line 1074
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1075
    return-void
.end method
