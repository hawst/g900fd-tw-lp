.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DisplayOptionsAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_BRIGHTNESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_JUSTIFICATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_LINE_HEIGHT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_ORIENTATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_PAGE_LAYOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_PAGE_TURN_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_READING_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_SAVE_NOTES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_TEXT_ZOOM:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_THEME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_TYPEFACE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum CHANGE_VOLUME_KEY_BEHAVIOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

.field public static final enum SETTINGS_LOADED_FROM_METADATA:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 196
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_READING_MODE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_READING_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 200
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_BRIGHTNESS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_BRIGHTNESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 201
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_LINE_HEIGHT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_LINE_HEIGHT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 202
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_JUSTIFICATION"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_JUSTIFICATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 203
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_TYPEFACE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_TYPEFACE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 204
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_TEXT_ZOOM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_TEXT_ZOOM:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 205
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_THEME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_THEME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 206
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_ORIENTATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_ORIENTATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 210
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_PAGE_TURN_MODE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_PAGE_TURN_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 214
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_VOLUME_KEY_BEHAVIOR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_VOLUME_KEY_BEHAVIOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 218
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "SETTINGS_LOADED_FROM_METADATA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->SETTINGS_LOADED_FROM_METADATA:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 222
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_PAGE_LAYOUT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_PAGE_LAYOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 226
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    const-string v1, "CHANGE_SAVE_NOTES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_SAVE_NOTES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    .line 192
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_READING_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_BRIGHTNESS:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_LINE_HEIGHT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_JUSTIFICATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_TYPEFACE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_TEXT_ZOOM:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_THEME:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_ORIENTATION:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_PAGE_TURN_MODE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_VOLUME_KEY_BEHAVIOR:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->SETTINGS_LOADED_FROM_METADATA:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_PAGE_LAYOUT:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->CHANGE_SAVE_NOTES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 192
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DisplayOptionsAction;

    return-object v0
.end method
