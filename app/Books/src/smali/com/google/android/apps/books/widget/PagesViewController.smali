.class public Lcom/google/android/apps/books/widget/PagesViewController;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PagesViewController$8;,
        Lcom/google/android/apps/books/widget/PagesViewController$NavViewRenderResponseConsumer;,
        Lcom/google/android/apps/books/widget/PagesViewController$VolumeRectsCacheCallbacks;,
        Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;,
        Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;,
        Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;,
        Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;,
        Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;,
        Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;,
        Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;,
        Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;,
        Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;,
        Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;,
        Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;,
        Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;,
        Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;,
        Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;,
        Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;,
        Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;,
        Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;,
        Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;
    }
.end annotation


# static fields
.field private static final LANDSCAPE_VISIBLE_OFFSETS:[I

.field private static final PORTRAIT_VISIBLE_OFFSETS:[I

.field private static final PROCESS_PAGE_DELAY_MILLIS:J

.field public static final VISIBLE_USER_LAYERS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final VOLUME_LAYERS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final EOB_PAGE_SEQUENCE_LENGTH_ONE_UP:I

.field final EOB_PAGE_SEQUENCE_LENGTH_RTL_EVEN_LTR_ODD:I

.field final EOB_PAGE_SEQUENCE_LENGTH_RTL_ODD_LTR_EVEN:I

.field private final FAR_FROM_EOB:I

.field final mAccessibleSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

.field private final mBasePageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

.field private mBasePosition:Lcom/google/android/apps/books/common/Position;

.field private mBookView:Lcom/google/android/apps/books/widget/BookView;

.field private mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

.field final mBookmarkListener:Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

.field private final mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

.field private final mCustomOffsets:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;",
            ">;"
        }
    .end annotation
.end field

.field private final mDbg:Z

.field private final mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;

.field private final mDisplayTwoPages:Z

.field private mDrawAnnotations:Z

.field private mExecutingInitialLoadTransition:Z

.field private mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

.field private final mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

.field private mInitialTransShouldRenderSlot0:Z

.field private mInitialTransShouldRenderSlotNeg1:Z

.field private mInitialTransitionDelayedPages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;",
            ">;"
        }
    .end annotation
.end field

.field private mLastAction:Lcom/google/android/apps/books/app/MoveType;

.field private mLastCenterPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mLastHighlightParams:Ljava/lang/String;

.field private mLastPublishedEobStatus:Z

.field private mLastPublishedPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mLastPublishedVisiblePages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;"
        }
    .end annotation
.end field

.field private final mLastReceivedPageFittedSize:Landroid/graphics/Point;

.field private mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private mLastTurnFromUser:Z

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mMarginNoteIconTapSize:I

.field private final mMinMarginNoteIconSize:Landroid/graphics/Point;

.field private mMoElementHighlightRequestId:I

.field private mMoElementPageOffset:I

.field private final mNonTurningPageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

.field private mOffsetFromBasePosition:I

.field private mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

.field private mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

.field private final mOnePageSize:Landroid/graphics/Point;

.field private final mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;

.field private final mPageOffsetToFullSizeInfo:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageToBookmark:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/render/PositionPageIdentifier;",
            "Lcom/google/android/apps/books/view/pages/BookmarkAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageToRendering:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/render/PositionPageIdentifier;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageToViewMatrix:Landroid/graphics/Matrix;

.field private final mPageTurnCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;

.field private final mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

.field private final mPageZoomTransform:Landroid/graphics/Matrix;

.field private final mPageZoomTransformInverse:Landroid/graphics/Matrix;

.field private final mPagesView:Lcom/google/android/apps/books/widget/PagesView;

.field private final mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

.field private final mPaintableRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;

.field final mPendingPageRequests:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;",
            ">;"
        }
    .end annotation
.end field

.field private final mPendingPages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;",
            ">;"
        }
    .end annotation
.end field

.field private mPreparedNavView:Z

.field final mProcessPageCallback:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

.field final mProcessPageScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

.field private mReadableItemPassageIndex:I

.field private final mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

.field private mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

.field private final mRenderListener:Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;

.field private mRenderedFirstMainPage:Z

.field private final mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private final mRendererLogEnabled:Z

.field final mRendererToViewTransform:Landroid/graphics/Matrix;

.field private final mRendererTransform:Landroid/graphics/Matrix;

.field private final mReusableClipRect:Landroid/graphics/Rect;

.field private final mReusableClipRectAfterTransform:Landroid/graphics/Rect;

.field private final mReusableClipRectF:Landroid/graphics/RectF;

.field private final mReuseableInverseMatrix:Landroid/graphics/Matrix;

.field private mSaveLastMove:Z

.field private mSearchBarUpdatePending:Z

.field private final mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/widget/SearchMatchRectsCache",
            "<*>;"
        }
    .end annotation
.end field

.field private mSelectedAnnotationId:Ljava/lang/String;

.field private mSelectedAnnotationPassageIndex:I

.field final mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

.field private mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

.field private mSequenceNumber:I

.field private mShowingEndOfBookPage:Z

.field private mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mStopped:Z

.field private mSwipeDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field private mTappedAnnotationCandidateComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Holder",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempHighlightsSharingColor:Lcom/google/android/apps/books/widget/HighlightsSharingColor;

.field private final mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

.field private final mTempPoint:Landroid/graphics/Point;

.field private final mTempPointF:Landroid/graphics/PointF;

.field private final mTempSize:Landroid/graphics/Point;

.field private final mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/render/PageHandle;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempSpreadPageIndices:[I

.field private final mTextHighlightRenderer:Lcom/google/android/apps/books/render/TextHighlightRenderer;

.field private mTimeStartedLastTurn:J

.field private mTtsHighlightBoundsRequestId:I

.field private mTtsHighlightRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

.field private mTtsHighlightRectangles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserToWrapperConsumer:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;",
            "Lcom/google/android/apps/books/render/RenderResponseConsumer;",
            ">;"
        }
    .end annotation
.end field

.field private final mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private final mVolumeRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$VolumeRectsCacheCallbacks;

.field private final mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 406
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    .line 412
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/books/annotations/Annotation;->GEO_LAYER_ID:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/common/collect/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/widget/PagesViewController;->VOLUME_LAYERS:Ljava/util/Set;

    .line 885
    const-wide v0, 0x4040aaaaa0000000L    # 33.33333206176758

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/books/widget/PagesViewController;->PROCESS_PAGE_DELAY_MILLIS:J

    .line 2752
    new-array v0, v3, [I

    aput v2, v0, v2

    sput-object v0, Lcom/google/android/apps/books/widget/PagesViewController;->PORTRAIT_VISIBLE_OFFSETS:[I

    .line 2753
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/books/widget/PagesViewController;->LANDSCAPE_VISIBLE_OFFSETS:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        0x0
    .end array-data
.end method

.method constructor <init>(Lcom/google/android/apps/books/widget/PagesView;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;Lcom/google/android/apps/books/util/Logger;ZILcom/google/android/apps/books/render/PageCanvasPainter;Lcom/google/android/apps/books/model/VolumeMetadata;ILandroid/graphics/Point;)V
    .locals 7
    .param p1, "pagesView"    # Lcom/google/android/apps/books/widget/PagesView;
    .param p2, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p3, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p4, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p5, "selectionAutomator"    # Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;
    .param p6, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p7, "lastPublishedEobStatus"    # Z
    .param p8, "marginNoteIconTapSize"    # I
    .param p9, "pageCanvasPainter"    # Lcom/google/android/apps/books/render/PageCanvasPainter;
    .param p10, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p11, "searchHighlightColor"    # I
    .param p12, "onePageSize"    # Landroid/graphics/Point;

    .prologue
    .line 1003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v2, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    invoke-direct {v2}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempHighlightsSharingColor:Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .line 175
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    .line 178
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    .line 182
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    .line 241
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    .line 278
    const-string v2, "PagesViewHelper"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDbg:Z

    .line 282
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransform:Landroid/graphics/Matrix;

    .line 284
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransformInverse:Landroid/graphics/Matrix;

    .line 290
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMinMarginNoteIconSize:Landroid/graphics/Point;

    .line 297
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererToViewTransform:Landroid/graphics/Matrix;

    .line 306
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToViewMatrix:Landroid/graphics/Matrix;

    .line 322
    new-instance v2, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v2}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageOffsetToFullSizeInfo:Landroid/support/v4/util/SparseArrayCompat;

    .line 329
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToRendering:Ljava/util/Map;

    .line 344
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementPageOffset:I

    .line 357
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDrawAnnotations:Z

    .line 363
    new-instance v2, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v2}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCustomOffsets:Landroid/support/v4/util/SparseArrayCompat;

    .line 400
    new-instance v2, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v2}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    .line 609
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    .line 777
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/PagesViewController$1;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkListener:Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    .line 844
    new-instance v2, Lcom/google/android/apps/books/render/TextHighlightRenderer;

    invoke-direct {v2}, Lcom/google/android/apps/books/render/TextHighlightRenderer;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTextHighlightRenderer:Lcom/google/android/apps/books/render/TextHighlightRenderer;

    .line 869
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    .line 875
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlot0:Z

    .line 877
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlotNeg1:Z

    .line 879
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mExecutingInitialLoadTransition:Z

    .line 887
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/PagesViewController$2;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageCallback:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

    .line 898
    new-instance v2, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageCallback:Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;

    sget-wide v4, Lcom/google/android/apps/books/widget/PagesViewController;->PROCESS_PAGE_DELAY_MILLIS:J

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;-><init>(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;J)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    .line 902
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/PagesViewController$3;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mAccessibleSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    .line 931
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;

    .line 962
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;

    .line 967
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastReceivedPageFittedSize:Landroid/graphics/Point;

    .line 971
    const/4 v2, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadPageIndices:[I

    .line 1144
    const-string v2, "RENDERERCALLS"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererLogEnabled:Z

    .line 1982
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    .line 1983
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationPassageIndex:I

    .line 1985
    new-instance v2, Lcom/google/android/apps/books/render/MarginNote;

    invoke-direct {v2}, Lcom/google/android/apps/books/render/MarginNote;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    .line 1998
    new-instance v2, Lcom/google/android/apps/books/util/Holder;

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/util/Holder;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;

    .line 2050
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/PagesViewController$4;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTappedAnnotationCandidateComparator:Ljava/util/Comparator;

    .line 2290
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectAfterTransform:Landroid/graphics/Rect;

    .line 2291
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;

    .line 2292
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectF:Landroid/graphics/RectF;

    .line 2293
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReuseableInverseMatrix:Landroid/graphics/Matrix;

    .line 2295
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$5;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/PagesViewController$5;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

    .line 2489
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererTransform:Landroid/graphics/Matrix;

    .line 2613
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPointF:Landroid/graphics/PointF;

    .line 2928
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderListener:Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;

    .line 2929
    new-instance v2, Lcom/google/android/apps/books/render/DispatchRendererListener;

    invoke-direct {v2}, Lcom/google/android/apps/books/render/DispatchRendererListener;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;

    .line 2942
    const/4 v2, 0x5

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->FAR_FROM_EOB:I

    .line 2960
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSize:Landroid/graphics/Point;

    .line 2991
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$6;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/PagesViewController$6;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mNonTurningPageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

    .line 3499
    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->EOB_PAGE_SEQUENCE_LENGTH_RTL_EVEN_LTR_ODD:I

    .line 3508
    const/4 v2, 0x3

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->EOB_PAGE_SEQUENCE_LENGTH_RTL_ODD_LTR_EVEN:I

    .line 3512
    const/4 v2, 0x1

    iput v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->EOB_PAGE_SEQUENCE_LENGTH_ONE_UP:I

    .line 4726
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 4781
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mUserToWrapperConsumer:Ljava/util/Map;

    .line 1004
    const-string v2, "Missing pagesView"

    invoke-static {p1, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1005
    const-string v2, "Missing renderer"

    invoke-static {p2, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1006
    const-string v2, "Missing selectionAutomator"

    invoke-static {p5, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;

    .line 1009
    new-instance v2, Lcom/google/android/apps/books/widget/PageTurnController;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PageTurnCallbacks;

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/widget/PageTurnController;-><init>(Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    .line 1011
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 1012
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    .line 1014
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPaintableRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;

    .line 1016
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$VolumeRectsCacheCallbacks;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController$VolumeRectsCacheCallbacks;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$VolumeRectsCacheCallbacks;

    .line 1018
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderListener:Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/render/DispatchRendererListener;->weaklyAddListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 1020
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    const-string v2, "RENDERERCALLS"

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/render/Renderer;->setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 1023
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v2}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    .line 1025
    new-instance v2, Lcom/google/android/apps/books/render/SpreadItems;

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    .line 1027
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    .line 1028
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOnePageSize:Landroid/graphics/Point;

    .line 1030
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/PagesView;->setCallbacks(Lcom/google/android/apps/books/widget/PagesView$Callbacks;)V

    .line 1033
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    .line 1034
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/PagesView;->setWritingDirection(Lcom/google/android/apps/books/util/WritingDirection;)V

    .line 1036
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;

    .line 1038
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    invoke-interface {p3, v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->prepareBookView(Lcom/google/android/apps/books/widget/BookView$Callbacks;Z)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    .line 1040
    iput-object p5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

    .line 1041
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mAccessibleSelectionChangedListener:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;->setSelectionChangedListener(Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages$SelectionChangedListener;)V

    .line 1044
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PagesView;->resetZoom()V

    .line 1048
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    sget-object v3, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPaintableRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/google/android/apps/books/render/Renderer;->createHighlightsRectsCache(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    .line 1050
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/render/Renderer;->setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V

    .line 1051
    new-instance v2, Lcom/google/android/apps/books/widget/VolumeRectsCache;

    sget-object v3, Lcom/google/android/apps/books/widget/PagesViewController;->VOLUME_LAYERS:Ljava/util/Set;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$VolumeRectsCacheCallbacks;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/apps/books/widget/VolumeRectsCache;-><init>(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    .line 1054
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPaintableRectsCacheCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PaintableRectsCacheCallbacks;

    move/from16 v0, p11

    invoke-interface {v2, v3, v0, v4}, Lcom/google/android/apps/books/render/Renderer;->createSearchMatchRectsCache(Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;ILcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    .line 1057
    iput p8, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMarginNoteIconTapSize:I

    .line 1058
    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 1060
    iput-object p6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 1061
    iput-boolean p7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedEobStatus:Z

    .line 1062
    iput-boolean p7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mShowingEndOfBookPage:Z

    .line 1063
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 1064
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->setupSelectionOverlay()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/Renderer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isBusy()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "x3"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForFullView(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/PagesViewController;)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->onSpecialState(Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/RenderRequestContext;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->isCurrentRequest(Lcom/google/android/apps/books/util/RenderRequestContext;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/common/Position;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->onMissingPosition(Lcom/google/android/apps/books/common/Position;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/PageCanvasPainter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p3, "x3"    # Landroid/graphics/Canvas;
    .param p4, "x4"    # Landroid/graphics/Rect;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/widget/PagesViewController;->paintHighlightRectsAndMarginNotes(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->shouldPaintTtsHighlight(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/books/widget/PagesViewController;Landroid/graphics/Canvas;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Landroid/graphics/Canvas;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->paintTtsHighlight(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectAfterTransform:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/RectF;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReusableClipRectF:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReuseableInverseMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/google/android/apps/books/widget/PagesViewController;Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Landroid/graphics/Canvas;
    .param p2, "x2"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "x3"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "x4"    # Landroid/graphics/Point;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/widget/PagesViewController;->decoratePage(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)V

    return-void
.end method

.method static synthetic access$3800(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/LabeledRect;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mNonTurningPageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/google/android/apps/books/widget/PagesViewController;ILjava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeHighlightTtsRectangles(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4200(Lcom/google/android/apps/books/widget/PagesViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementHighlightRequestId:I

    return v0
.end method

.method static synthetic access$4300(Lcom/google/android/apps/books/widget/PagesViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementPageOffset:I

    return v0
.end method

.method static synthetic access$4400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PageTurnController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/google/android/apps/books/widget/PagesViewController;IILcom/google/android/apps/books/widget/PageTurnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/widget/PagesViewController;->turnToPage(IILcom/google/android/apps/books/widget/PageTurnController;)V

    return-void
.end method

.method static synthetic access$4600(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V

    return-void
.end method

.method static synthetic access$4800(Lcom/google/android/apps/books/widget/PagesViewController;Ljava/util/Collection;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->invalidatePassages(Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic access$4900(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingNavigationView()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/BookView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/google/android/apps/books/widget/PagesViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getRightMostVisiblePageOffsetFromBasePage()I

    move-result v0

    return v0
.end method

.method static synthetic access$5200(Lcom/google/android/apps/books/widget/PagesViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getLeftMostVisiblePageOffsetFromBasePage()I

    move-result v0

    return v0
.end method

.method static synthetic access$5300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/support/v4/util/SparseArrayCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/common/Position;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5700(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/google/android/apps/books/widget/PagesViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    return v0
.end method

.method static synthetic access$5812(Lcom/google/android/apps/books/widget/PagesViewController;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    return v0
.end method

.method static synthetic access$5900(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToRendering:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$6002(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSwipeDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    return-object p1
.end method

.method static synthetic access$6102(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/app/MoveType;)Lcom/google/android/apps/books/app/MoveType;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastAction:Lcom/google/android/apps/books/app/MoveType;

    return-object p1
.end method

.method static synthetic access$6202(Lcom/google/android/apps/books/widget/PagesViewController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSaveLastMove:Z

    return p1
.end method

.method static synthetic access$6300(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    return-object v0
.end method

.method static synthetic access$6402(Lcom/google/android/apps/books/widget/PagesViewController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mShowingEndOfBookPage:Z

    return p1
.end method

.method static synthetic access$6500(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateEobStatus()V

    return-void
.end method

.method static synthetic access$6600(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showBookView()V

    return-void
.end method

.method static synthetic access$6700(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastTurnFromUser:Z

    return v0
.end method

.method static synthetic access$6800()J
    .locals 2

    .prologue
    .line 142
    sget-wide v0, Lcom/google/android/apps/books/widget/PagesViewController;->PROCESS_PAGE_DELAY_MILLIS:J

    return-wide v0
.end method

.method static synthetic access$6900(Lcom/google/android/apps/books/widget/PagesViewController;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->atLeastOnePositionHasBookmark(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z

    return v0
.end method

.method static synthetic access$7000(Lcom/google/android/apps/books/widget/PagesViewController;[I)[I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # [I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getAbsoluteOffsets([I)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7100(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7200(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onOffsetChanged()V

    return-void
.end method

.method static synthetic access$7300(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderedFirstMainPage:Z

    return v0
.end method

.method static synthetic access$7400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/DispatchRendererListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDispatchListener:Lcom/google/android/apps/books/render/DispatchRendererListener;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderListener:Lcom/google/android/apps/books/widget/PagesViewController$MyRendererListener;

    return-object v0
.end method

.method static synthetic access$7600(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7700(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getOffsetFromBasePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v0

    return v0
.end method

.method static synthetic access$7800(Lcom/google/android/apps/books/widget/PagesViewController;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->isPendingPageRequest(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7900(Lcom/google/android/apps/books/widget/PagesViewController;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSequenceNumber:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/widget/PagesViewController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->processPendingPage()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8000(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/RenderRequestContext;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "x3"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "x4"    # Lcom/google/android/apps/books/util/RenderRequestContext;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/widget/PagesViewController;->publishPageData(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/RenderRequestContext;)V

    return-void
.end method

.method static synthetic access$8100(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/app/BookmarkController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    return-object v0
.end method

.method static synthetic access$8200(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPointF:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$8300(Lcom/google/android/apps/books/widget/PagesViewController;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/util/Holder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;

    return-object v0
.end method

.method static synthetic access$8500(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;ILcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Landroid/graphics/Point;
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/google/android/apps/books/util/Holder;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/widget/PagesViewController;->getTappedAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;ILcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8600(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/annotations/Annotation;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "x3"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p4, "x4"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/widget/PagesViewController;->editAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/annotations/Annotation;Z)V

    return-void
.end method

.method static synthetic access$8700(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigation()V

    return-void
.end method

.method static synthetic access$8800(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/render/ReaderSettings;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    return-object v0
.end method

.method static synthetic access$8900(Lcom/google/android/apps/books/widget/PagesViewController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigationDelayed()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/widget/PagesViewController;)Lcom/google/android/apps/books/widget/SelectionOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    return-object v0
.end method

.method static synthetic access$9002(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    return-object p1
.end method

.method static synthetic access$9202(Lcom/google/android/apps/books/widget/PagesViewController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchBarUpdatePending:Z

    return p1
.end method

.method static synthetic access$9300(Lcom/google/android/apps/books/widget/PagesViewController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mUserToWrapperConsumer:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$9400(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9500(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/PagesViewController;
    .param p1, "x1"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForAnyView(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    return-void
.end method

.method public static addAnnotationCandidatesAtTap(Lcom/google/android/apps/books/widget/Walker;Landroid/graphics/Point;ILjava/util/List;)V
    .locals 15
    .param p1, "tap"    # Landroid/graphics/Point;
    .param p2, "threshold"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;",
            "Landroid/graphics/Point;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2119
    .local p0, "passageRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    .local p3, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;>;"
    if-eqz p0, :cond_2

    .line 2120
    const/4 v13, 0x0

    .line 2121
    .local v13, "isMarginIcon":Z
    mul-int v14, p2, p2

    .line 2122
    .local v14, "thresholdSQ":I
    new-instance v10, Lcom/google/android/apps/books/render/LabeledRect;

    invoke-direct {v10}, Lcom/google/android/apps/books/render/LabeledRect;-><init>()V

    .line 2123
    .local v10, "highlightRect":Lcom/google/android/apps/books/render/LabeledRect;
    new-instance v12, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    invoke-direct {v12}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;-><init>()V

    .line 2124
    .local v12, "highlightsSharingColor":Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    :cond_0
    invoke-interface {p0, v12}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2125
    iget-object v11, v12, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->highlightRects:Lcom/google/android/apps/books/widget/Walker;

    .line 2127
    .local v11, "highlightRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/LabeledRect;>;"
    :cond_1
    :goto_0
    invoke-interface {v11, v10}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2128
    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Point;->y:I

    iget-object v5, v10, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/books/util/MathUtils;->squaredDistanceFromPointToRect(IILandroid/graphics/Rect;)J

    move-result-wide v8

    .line 2130
    .local v8, "distSQ":J
    const-wide/16 v4, 0x0

    cmp-long v2, v8, v4

    if-ltz v2, :cond_1

    int-to-long v4, v14

    cmp-long v2, v8, v4

    if-gez v2, :cond_1

    .line 2132
    long-to-int v3, v8

    .line 2133
    .local v3, "intDistQ":I
    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;

    iget-object v5, v10, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v4, v3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;-><init>(IILjava/lang/String;ZLcom/google/android/apps/books/widget/PagesViewController$1;)V

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2139
    .end local v3    # "intDistQ":I
    .end local v8    # "distSQ":J
    .end local v10    # "highlightRect":Lcom/google/android/apps/books/render/LabeledRect;
    .end local v11    # "highlightRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/LabeledRect;>;"
    .end local v12    # "highlightsSharingColor":Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    .end local v13    # "isMarginIcon":Z
    .end local v14    # "thresholdSQ":I
    :cond_2
    return-void
.end method

.method private addPendingPageRequest(ILcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)V
    .locals 2
    .param p1, "offset"    # I
    .param p2, "consumer"    # Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    .prologue
    .line 251
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    .line 252
    .local v0, "oldConsumer":Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    .line 253
    # invokes: Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->recycle()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->access$100(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)V

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 256
    return-void
.end method

.method private atLeastOnePositionHasBookmark(I)Z
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 3953
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 3954
    .local v0, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v0, :cond_0

    .line 3955
    const/4 v1, 0x0

    .line 3957
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->hasBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v1

    goto :goto_0
.end method

.method private clearObsoletePendingPages(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3082
    .local p1, "currentOffsets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3084
    .local v1, "pendingPages":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3085
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3086
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3087
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 3090
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;>;"
    :cond_1
    return-void
.end method

.method private clearPendingPageRequests()V
    .locals 2

    .prologue
    .line 244
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    # invokes: Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->recycle()V
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->access$100(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)V

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 248
    return-void
.end method

.method public static create(Lcom/google/android/apps/books/widget/PagesView;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;Lcom/google/android/apps/books/util/Logger;ZLcom/google/android/apps/books/model/VolumeMetadata;Landroid/graphics/Point;I)Lcom/google/android/apps/books/widget/PagesViewController;
    .locals 15
    .param p0, "pagesView"    # Lcom/google/android/apps/books/widget/PagesView;
    .param p1, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p2, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p3, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;
    .param p4, "selectionAutomator"    # Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;
    .param p5, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p6, "lastPublishedEobStatus"    # Z
    .param p7, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p8, "onePageSize"    # Landroid/graphics/Point;
    .param p9, "queryEmphasisColor"    # I

    .prologue
    .line 988
    invoke-interface {p0}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v13

    .line 989
    .local v13, "context":Landroid/content/Context;
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 990
    .local v14, "res":Landroid/content/res/Resources;
    const v0, 0x7f090156

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 992
    .local v8, "marginNoteIconTapSize":I
    new-instance v9, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;

    invoke-direct {v9, v13}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;-><init>(Landroid/content/Context;)V

    .line 993
    .local v9, "pageCanvasPainter":Lcom/google/android/apps/books/render/PageCanvasPainter;
    new-instance v0, Lcom/google/android/apps/books/widget/PagesViewController;

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v10, p7

    move/from16 v11, p9

    move-object/from16 v12, p8

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/books/widget/PagesViewController;-><init>(Lcom/google/android/apps/books/widget/PagesView;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;Lcom/google/android/apps/books/util/Logger;ZILcom/google/android/apps/books/render/PageCanvasPainter;Lcom/google/android/apps/books/model/VolumeMetadata;ILandroid/graphics/Point;)V

    return-object v0
.end method

.method private decoratePage(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "pageSize"    # Landroid/graphics/Point;

    .prologue
    .line 2320
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePageDecorator:Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;

    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/google/android/apps/books/widget/PagesViewController$PageDecorator;->drawOnPageCanvas(Landroid/graphics/Canvas;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 2321
    return-void
.end method

.method private displayTwoPages()Z
    .locals 1

    .prologue
    .line 4354
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    return v0
.end method

.method private editAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/annotations/Annotation;Z)V
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p4, "editNote"    # Z

    .prologue
    .line 2023
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 2024
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->updatePageToViewMatrix()V

    .line 2025
    if-eqz p4, :cond_0

    .line 2026
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0, p3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->editNote(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 2030
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v0

    invoke-virtual {p0, p3, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->setSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;I)V

    .line 2031
    return-void

    .line 2028
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0, p3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->editHighlight(Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0
.end method

.method private freeMemory()V
    .locals 1

    .prologue
    .line 1320
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->clearPendingPageRequests()V

    .line 1321
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageOffsetToFullSizeInfo:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 1322
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToRendering:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1323
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1324
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCustomOffsets:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 1325
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1326
    return-void
.end method

.method private getAbsoluteOffsets([I)[I
    .locals 4
    .param p1, "relativeOffsets"    # [I

    .prologue
    .line 2745
    array-length v2, p1

    new-array v0, v2, [I

    .line 2746
    .local v0, "absoluteOffsets":[I
    const/4 v1, 0x0

    .local v1, "x":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 2747
    aget v2, p1, v1

    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/2addr v2, v3

    aput v2, v0, v1

    .line 2746
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2749
    :cond_0
    return-object v0
.end method

.method public static getAnnotationWithId(Ljava/util/Collection;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 2507
    .local p0, "annotations":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 2508
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2512
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getAnnotationWithId(Ljava/util/Iterator;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .local p0, "annotations":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    const/4 v1, 0x0

    .line 2518
    if-nez p0, :cond_0

    move-object v0, v1

    .line 2526
    :goto_0
    return-object v0

    .line 2520
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2521
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    .line 2522
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_1
    move-object v0, v1

    .line 2526
    goto :goto_0
.end method

.method private getBestEffortSize(I)Landroid/graphics/Point;
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 1844
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageOffsetToFullSizeInfo:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;

    .line 1845
    .local v0, "renderedPage":Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;
    if-eqz v0, :cond_0

    .line 1846
    iget-object v1, v0, Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;->fittedSize:Landroid/graphics/Point;

    .line 1851
    :goto_0
    return-object v1

    .line 1848
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastReceivedPageFittedSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-eqz v1, :cond_1

    .line 1849
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastReceivedPageFittedSize:Landroid/graphics/Point;

    goto :goto_0

    .line 1851
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOnePageSize:Landroid/graphics/Point;

    goto :goto_0
.end method

.method private getBookView()Lcom/google/android/apps/books/widget/BookView;
    .locals 1

    .prologue
    .line 4286
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    return-object v0
.end method

.method private getBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .locals 4
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 3067
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v1

    .line 3068
    .local v1, "ppi":Lcom/google/android/apps/books/render/PositionPageIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 3069
    .local v0, "existing":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    if-eqz v0, :cond_0

    .line 3074
    .end local v0    # "existing":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    :goto_0
    return-object v0

    .line 3072
    .restart local v0    # "existing":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    :cond_0
    new-instance v2, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->hasBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;-><init>(Z)V

    .line 3073
    .local v2, "result":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    .line 3074
    goto :goto_0
.end method

.method private getCurrentRelevantOffsets()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4273
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I

    move-result-object v3

    .line 4274
    .local v3, "nearbyOffsets":[I
    new-instance v5, Ljava/util/LinkedHashSet;

    array-length v6, v3

    invoke-direct {v5, v6}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 4275
    .local v5, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object v0, v3

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget v4, v0, v1

    .line 4276
    .local v4, "offset":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 4275
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4278
    .end local v4    # "offset":I
    :cond_0
    return-object v5
.end method

.method private getInterpolatedPageSize(ILandroid/graphics/Point;)Landroid/graphics/Point;
    .locals 3
    .param p1, "offset"    # I
    .param p2, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 1824
    add-int/lit8 v2, p1, -0x1

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getBestEffortSize(I)Landroid/graphics/Point;

    move-result-object v1

    .line 1825
    .local v1, "previousSize":Landroid/graphics/Point;
    add-int/lit8 v2, p1, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getBestEffortSize(I)Landroid/graphics/Point;

    move-result-object v0

    .line 1826
    .local v0, "nextSize":Landroid/graphics/Point;
    invoke-static {v1, v0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->interpolate(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v2

    return-object v2
.end method

.method private getLeftMostVisiblePageOffsetFromBasePage()I
    .locals 2

    .prologue
    .line 2733
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x3

    :goto_0
    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private getNearbyPageOffsets()[I
    .locals 1

    .prologue
    .line 2725
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/PagesViewUtils;->getRelativePageOffsets(Z)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->getAbsoluteOffsets([I)[I

    move-result-object v0

    return-object v0
.end method

.method private getOffsetFromBasePosition(Lcom/google/android/apps/books/render/PageIdentifier;)I
    .locals 8
    .param p1, "pageId"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    const v4, 0x7fffffff

    .line 4246
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v4

    .line 4268
    :cond_0
    :goto_0
    return v1

    .line 4251
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    new-instance v5, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-interface {v3, v5, v6}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 4253
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_4

    .line 4254
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadHandles:Lcom/google/android/apps/books/render/SpreadItems;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v3}, Lcom/google/android/apps/books/render/PageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 4255
    .local v0, "basePageId":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v0, :cond_3

    .line 4253
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 4259
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3, p1, v0}, Lcom/google/android/apps/books/render/Renderer;->getScreenPageDifference(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/PageIdentifier;)I

    move-result v1

    .line 4261
    .local v1, "distance":I
    if-eq v1, v4, :cond_2

    .line 4262
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4263
    if-nez v2, :cond_0

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .end local v0    # "basePageId":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v1    # "distance":I
    :cond_4
    move v1, v4

    .line 4268
    goto :goto_0
.end method

.method private getPagePosition(I)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .locals 1
    .param p1, "absoluteOffset"    # I

    .prologue
    .line 4768
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4769
    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 4772
    :goto_0
    return-object v0

    .line 4769
    :cond_0
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_0

    .line 4772
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_0
.end method

.method private getPagesPerSpread()I
    .locals 1

    .prologue
    .line 4358
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getRenderingTheme()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2465
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getRenderingTheme(Lcom/google/android/apps/books/render/ReaderSettings;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRightMostVisiblePageOffsetFromBasePage()I
    .locals 2

    .prologue
    .line 2741
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .locals 1
    .param p1, "absoluteOffset"    # I

    .prologue
    .line 4777
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getPagePosition(I)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v0

    return-object v0
.end method

.method private getSpecialPageBitmap(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "type"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .prologue
    .line 2718
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ImmutableMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private getSpreadPageIdentifierIndices(I[I)[I
    .locals 8
    .param p1, "pageOffset"    # I
    .param p2, "outIndices"    # [I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1502
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v7

    iget v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    move v4, v5

    :goto_0
    if-ne v7, v4, :cond_0

    .line 1503
    add-int/lit8 p1, p1, 0x1

    .line 1505
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->readingOffsetFromScreenOffset(I)I

    move-result v1

    .line 1506
    .local v1, "readingOffset":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getPagesPerSpread()I

    move-result v0

    .line 1508
    .local v0, "pagesPerSpread":I
    invoke-static {v1, v0}, Lcom/google/android/apps/books/util/MathUtils;->divideRoundingDown(II)I

    move-result v2

    .line 1509
    .local v2, "spreadOffset":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    rem-int v3, v4, v0

    .line 1510
    .local v3, "spreadPageIndex":I
    aput v2, p2, v6

    .line 1511
    aput v3, p2, v5

    .line 1512
    return-object p2

    .end local v0    # "pagesPerSpread":I
    .end local v1    # "readingOffset":I
    .end local v2    # "spreadOffset":I
    .end local v3    # "spreadPageIndex":I
    :cond_1
    move v4, v6

    .line 1502
    goto :goto_0
.end method

.method private getStableSkimViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 1

    .prologue
    .line 4641
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->getStableSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    return-object v0
.end method

.method private getTappedAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;ILcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 21
    .param p1, "touchedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "pagePoint"    # Landroid/graphics/Point;
    .param p3, "threshold"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            "Landroid/graphics/Point;",
            "I",
            "Lcom/google/android/apps/books/util/Holder",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/apps/books/annotations/Annotation;"
        }
    .end annotation

    .prologue
    .line 2067
    .local p4, "outTappedMarginNoteIcon":Lcom/google/android/apps/books/util/Holder;, "Lcom/google/android/apps/books/util/Holder<Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mDrawAnnotations:Z

    if-nez v4, :cond_0

    .line 2068
    const/4 v4, 0x0

    .line 2113
    :goto_0
    return-object v4

    .line 2071
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/util/List;

    move-result-object v10

    .line 2074
    .local v10, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    if-eqz v10, :cond_3

    .line 2075
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v12, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2077
    .local v12, "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-interface {v4, v0, v5, v6}, Lcom/google/android/apps/books/render/Renderer;->getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v19

    .line 2080
    .local v19, "passageRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    move-object/from16 v0, v19

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v1, v2, v12}, Lcom/google/android/apps/books/widget/PagesViewController;->addAnnotationCandidatesAtTap(Lcom/google/android/apps/books/widget/Walker;Landroid/graphics/Point;ILjava/util/List;)V

    .line 2082
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mMarginNoteIconTapSize:I

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-interface {v4, v10, v0, v5, v6}, Lcom/google/android/apps/books/render/Renderer;->getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v13

    .line 2086
    .local v13, "icons":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    if-eqz v13, :cond_2

    .line 2087
    invoke-interface {v13}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 2090
    const/16 v18, 0x1

    .line 2091
    .local v18, "isMarginIcon":Z
    mul-int v20, p3, p3

    .line 2092
    .local v20, "thresholdSQ":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    invoke-interface {v13, v4}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2093
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->squaredDistanceFromPointToIcon(Lcom/google/android/apps/books/render/MarginNote;Landroid/graphics/Point;Z)J

    move-result-wide v16

    .line 2095
    .local v16, "distance":J
    const-wide/16 v4, 0x0

    cmp-long v4, v4, v16

    if-gtz v4, :cond_1

    move/from16 v0, v20

    int-to-long v4, v0

    cmp-long v4, v16, v4

    if-gez v4, :cond_1

    .line 2096
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v4, v1, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->squaredDistanceFromPointToIcon(Lcom/google/android/apps/books/render/MarginNote;Landroid/graphics/Point;Z)J

    move-result-wide v14

    .line 2099
    .local v14, "dist2":J
    new-instance v4, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;

    move-wide/from16 v0, v16

    long-to-int v5, v0

    long-to-int v6, v14

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    iget-object v7, v7, Lcom/google/android/apps/books/render/MarginNote;->annotationId:Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;-><init>(IILjava/lang/String;ZLcom/google/android/apps/books/widget/PagesViewController$1;)V

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2105
    .end local v14    # "dist2":J
    .end local v16    # "distance":J
    .end local v18    # "isMarginIcon":Z
    .end local v20    # "thresholdSQ":I
    :cond_2
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 2106
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTappedAnnotationCandidateComparator:Ljava/util/Comparator;

    invoke-static {v12, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2107
    const/4 v4, 0x0

    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;

    .line 2108
    .local v11, "best":Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;
    iget-boolean v4, v11, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->isMarginIcon:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/util/Holder;->setValue(Ljava/lang/Object;)V

    .line 2109
    iget-object v4, v11, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->annotationId:Ljava/lang/String;

    invoke-static {v10, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->getAnnotationWithId(Ljava/util/Collection;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v4

    goto/16 :goto_0

    .line 2113
    .end local v11    # "best":Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;
    .end local v12    # "candidates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;>;"
    .end local v13    # "icons":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    .end local v19    # "passageRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method private getTouchedPageEvent(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/widget/TouchedPageEvent;
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "offset"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1363
    const/4 v0, 0x2

    new-array v7, v0, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    aput v0, v7, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    aput v0, v7, v4

    .line 1364
    .local v7, "pointF":[F
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransformInverse:Landroid/graphics/Matrix;

    invoke-virtual {v0, v7}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1366
    new-instance v1, Landroid/graphics/Point;

    aget v0, v7, v2

    float-to-int v0, v0

    aget v2, v7, v4

    float-to-int v2, v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 1369
    .local v1, "point":Landroid/graphics/Point;
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1370
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView;->getWidth()I

    move-result v0

    div-int/lit8 v6, v0, 0x2

    .line 1371
    .local v6, "halfWidth":I
    iget v0, v1, Landroid/graphics/Point;->x:I

    if-ge v0, v6, :cond_0

    .line 1372
    add-int/lit8 p2, p2, -0x1

    .line 1373
    sget-object v3, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 1382
    .end local v6    # "halfWidth":I
    .local v3, "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v5

    .line 1383
    .local v5, "touchedPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    new-instance v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;

    move v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/TouchedPageEvent;-><init>(Landroid/graphics/Point;ILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/view/MotionEvent;Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    return-object v0

    .line 1375
    .end local v3    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .end local v5    # "touchedPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    .restart local v6    # "halfWidth":I
    :cond_0
    iget v0, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v6

    iput v0, v1, Landroid/graphics/Point;->x:I

    .line 1376
    sget-object v3, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .restart local v3    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    goto :goto_0

    .line 1379
    .end local v3    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .end local v6    # "halfWidth":I
    :cond_1
    sget-object v3, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .restart local v3    # "ppos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    goto :goto_0
.end method

.method private getVolumeAnnotationRect(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Lcom/google/android/apps/books/render/LabeledRect;
    .locals 9
    .param p1, "touchEvent"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    const/4 v5, 0x0

    .line 2191
    iget-object v6, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-nez v6, :cond_0

    move-object v0, v5

    .line 2212
    :goto_0
    return-object v0

    .line 2196
    :cond_0
    new-instance v4, Landroid/graphics/Point;

    iget-object v6, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    invoke-direct {v4, v6}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    .line 2197
    .local v4, "rendererTouchPoint":Landroid/graphics/Point;
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v7, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v8, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-interface {v6, v4, v7, v8}, Lcom/google/android/apps/books/render/Renderer;->convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v0, v5

    .line 2199
    goto :goto_0

    .line 2202
    :cond_1
    iget-object v6, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v2

    .line 2203
    .local v2, "passageIndex":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 2205
    .local v3, "rects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    if-nez v3, :cond_2

    move-object v0, v5

    goto :goto_0

    .line 2207
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/LabeledRect;

    .line 2208
    .local v0, "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    iget-object v6, v0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    iget v7, v4, Landroid/graphics/Point;->x:I

    iget v8, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_0

    .end local v0    # "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    :cond_4
    move-object v0, v5

    .line 2212
    goto :goto_0
.end method

.method private handleTapAsAnnotationSelection(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z
    .locals 3
    .param p1, "touchedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "pagePoint"    # Landroid/graphics/Point;
    .param p3, "pagePositionOnScreen"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    const/4 v2, 0x1

    .line 2012
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;

    invoke-direct {p0, p1, p2, v2, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getTappedAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;ILcom/google/android/apps/books/util/Holder;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 2014
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_0

    .line 2015
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempBooleanHolder:Lcom/google/android/apps/books/util/Holder;

    invoke-virtual {v1}, Lcom/google/android/apps/books/util/Holder;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->editAnnotation(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/annotations/Annotation;Z)V

    move v1, v2

    .line 2018
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleTapAsAnnotationSelection(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
    .locals 4
    .param p1, "touchEvent"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    .line 1988
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;

    iget-object v1, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 1989
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;

    iget-object v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v3, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/render/Renderer;->convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1991
    const/4 v0, 0x0

    .line 1994
    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempPoint:Landroid/graphics/Point;

    iget-object v2, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->handleTapAsAnnotationSelection(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z

    move-result v0

    goto :goto_0
.end method

.method private handleTapAsBookmarkToggle(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
    .locals 1
    .param p1, "touchEvent"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    .line 2579
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2580
    iget-object v0, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->toggleBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 2581
    const/4 v0, 0x1

    .line 2583
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 3961
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/BookmarkController;->getViewableBookmarks(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
    .locals 1
    .param p1, "touchEvent"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesView;->inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z

    move-result v0

    return v0
.end method

.method private static interpolate(Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 3
    .param p0, "p1"    # Landroid/graphics/Point;
    .param p1, "p2"    # Landroid/graphics/Point;
    .param p2, "out"    # Landroid/graphics/Point;

    .prologue
    .line 1834
    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->x:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Landroid/graphics/Point;->y:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 1835
    return-object p2
.end method

.method private invalidateAnnotationRects()V
    .locals 1

    .prologue
    .line 4369
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->clearData()V

    .line 4370
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->clearData()V

    .line 4371
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->clearAnnotationCaches()V

    .line 4372
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->loadNewNoteRects()V

    .line 4373
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 4374
    return-void
.end method

.method private invalidatePassages(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4383
    .local p1, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 4384
    .local v1, "passageIndex":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 4385
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->clearData(I)V

    .line 4386
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->clearData(I)V

    .line 4387
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->clearData(I)V

    goto :goto_0

    .line 4390
    .end local v1    # "passageIndex":Ljava/lang/Integer;
    :cond_1
    return-void
.end method

.method private invalidateState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1333
    iget v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSequenceNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSequenceNumber:I

    .line 1335
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->freeMemory()V

    .line 1337
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 1339
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    .line 1340
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    .line 1341
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 1343
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRectangles:Ljava/util/List;

    .line 1344
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedVisiblePages:Ljava/util/List;

    .line 1346
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->cancelPendingRequests()V

    .line 1347
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onCanceledRendererRequests()V

    .line 1348
    return-void
.end method

.method private invalidateVolumeAnnotationRects()V
    .locals 1

    .prologue
    .line 4377
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->clearData()V

    .line 4378
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->loadNewVolumeAnnotationRects()V

    .line 4379
    return-void
.end method

.method private isBusy()Z
    .locals 2

    .prologue
    .line 3062
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCurrentRequest(Lcom/google/android/apps/books/util/RenderRequestContext;)Z
    .locals 2
    .param p1, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;

    .prologue
    .line 1354
    iget v0, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->sequenceNumber:I

    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSequenceNumber:I

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->basePosition:Lcom/google/android/apps/books/common/Position;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPendingPageRequest(I)Z
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDbg:Z

    if-eqz v0, :cond_0

    .line 273
    const-string v0, "PagesViewHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPendingPageRequest("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")?:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRightToLeft()Z
    .locals 2

    .prologue
    .line 4362
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    sget-object v1, Lcom/google/android/apps/books/util/WritingDirection;->RIGHT_TO_LEFT:Lcom/google/android/apps/books/util/WritingDirection;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadNewVolumeAnnotationRects()V
    .locals 1

    .prologue
    .line 4404
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->clearRequestsAndCacheIndices()V

    .line 4405
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeLoadNearbyVolumeAnnotationRects()V

    .line 4406
    return-void
.end method

.method private loadNonSelectionRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;)I
    .locals 9
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1671
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move v1, p1

    move-object v2, p2

    move v5, v3

    move v6, v3

    move v7, v4

    move v8, v3

    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/books/render/Renderer;->loadRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;ZIIIIZ)I

    move-result v0

    return v0
.end method

.method private makeFullViewRenderResponseConsumer(ILcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    .locals 4
    .param p1, "devicePageOffset"    # I
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 1154
    new-instance v0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    new-instance v1, Lcom/google/android/apps/books/util/RenderRequestContext;

    iget v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSequenceNumber:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/apps/books/util/RenderRequestContext;-><init>(ILcom/google/android/apps/books/common/Position;I)V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p2, v2}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/widget/PagesViewController$1;)V

    .line 1157
    .local v0, "consumer":Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererLogEnabled:Z

    if-eqz v1, :cond_0

    .line 1158
    const-string v1, "RENDERERCALLS"

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "consumer":Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    .line 1161
    .restart local v0    # "consumer":Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    :cond_0
    return-object v0
.end method

.method private makeRenderPosition(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/RenderPosition;
    .locals 2
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;

    .prologue
    .line 1467
    new-instance v0, Lcom/google/android/apps/books/render/RenderPosition;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesView;->getBitmapConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;Landroid/graphics/Bitmap$Config;)V

    return-object v0
.end method

.method private makeRenderPosition(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/RenderPosition;
    .locals 2
    .param p1, "spreadPageIdentifier"    # Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .prologue
    .line 1471
    new-instance v0, Lcom/google/android/apps/books/render/RenderPosition;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesView;->getBitmapConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/books/render/RenderPosition;-><init>(Lcom/google/android/apps/books/render/SpreadPageIdentifier;Landroid/graphics/Bitmap$Config;)V

    return-object v0
.end method

.method private makeRenderPosition(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/render/RenderPosition;
    .locals 2
    .param p1, "renderedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 1462
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 1463
    .local v0, "pageIdentifier":Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v1

    return-object v1
.end method

.method private maybeDismissSelectionOverlay()V
    .locals 1

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-eqz v0, :cond_0

    .line 1974
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->removeFromParent()V

    .line 1975
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    .line 1976
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onDismissedSelection()V

    .line 1977
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V

    .line 1979
    :cond_0
    return-void
.end method

.method private maybeHighlightTtsRectangles(ILjava/lang/String;)Z
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "textPointsJson"    # Ljava/lang/String;

    .prologue
    .line 1761
    iget v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightBoundsRequestId:I

    if-ne p1, v6, :cond_1

    .line 1762
    new-instance v2, Lcom/google/android/apps/books/util/IntArrayParser;

    invoke-direct {v2}, Lcom/google/android/apps/books/util/IntArrayParser;-><init>()V

    .line 1763
    .local v2, "parser":Lcom/google/android/apps/books/util/IntArrayParser;
    invoke-virtual {v2, p2}, Lcom/google/android/apps/books/util/IntArrayParser;->init(Ljava/lang/String;)V

    .line 1764
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1765
    .local v3, "rectangles":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IntArrayParser;->hasMore()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1766
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IntArrayParser;->nextInt()I

    move-result v1

    .line 1767
    .local v1, "left":I
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IntArrayParser;->nextInt()I

    move-result v5

    .line 1768
    .local v5, "top":I
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IntArrayParser;->nextInt()I

    move-result v4

    .line 1769
    .local v4, "right":I
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IntArrayParser;->nextInt()I

    move-result v0

    .line 1770
    .local v0, "bottom":I
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v1, v5, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1772
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    .end local v4    # "right":I
    .end local v5    # "top":I
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->setTtsRectangles(Ljava/util/List;)V

    .line 1776
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->redrawPages()V

    .line 1778
    .end local v2    # "parser":Lcom/google/android/apps/books/util/IntArrayParser;
    .end local v3    # "rectangles":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    :cond_1
    const/4 v6, 0x0

    return v6
.end method

.method private maybeLoadNearbyAnnotationRects()V
    .locals 9

    .prologue
    .line 4420
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v6}, Lcom/google/android/apps/books/render/Renderer;->needsBackgroundAnnotationLoad()Z

    move-result v6

    if-nez v6, :cond_1

    .line 4435
    :cond_0
    return-void

    .line 4424
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v4, v0, v2

    .line 4425
    .local v4, "offset":I
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v5

    .line 4426
    .local v5, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v5, :cond_2

    .line 4428
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v7

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->maybeLoadDataForPassage(IZ)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4424
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4429
    :catch_0
    move-exception v1

    .line 4430
    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v6, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private maybeLoadNearbyVolumeAnnotationRects()V
    .locals 9

    .prologue
    .line 4438
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v6}, Lcom/google/android/apps/books/render/Renderer;->needsBackgroundAnnotationLoad()Z

    move-result v6

    if-nez v6, :cond_1

    .line 4453
    :cond_0
    return-void

    .line 4442
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v4, v0, v2

    .line 4443
    .local v4, "offset":I
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v5

    .line 4444
    .local v5, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v5, :cond_2

    .line 4446
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v7

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->maybeLoadDataForPassage(IZ)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4442
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 4448
    :catch_0
    move-exception v1

    .line 4449
    .local v1, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v6, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private maybePublishVisiblePages(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 3
    .param p1, "mainPageRendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 2835
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 2836
    .local v1, "visiblePages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2837
    iget v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 2838
    .local v0, "leftPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v0, :cond_0

    .line 2846
    .end local v0    # "leftPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :goto_0
    return-void

    .line 2841
    .restart local v0    # "leftPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2843
    .end local v0    # "leftPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_1
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2845
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->maybePublishVisiblePages(Ljava/util/List;)V

    goto :goto_0
.end method

.method private maybePublishVisiblePages(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2860
    .local p1, "visiblePages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedVisiblePages:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2861
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onVisibleDevicePagesChanged(Ljava/util/List;)V

    .line 2862
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedVisiblePages:Ljava/util/List;

    .line 2864
    :cond_0
    return-void
.end method

.method private maybeUpdateEobStatus()V
    .locals 2

    .prologue
    .line 2881
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingEndOfBookPage()Z

    move-result v0

    .line 2882
    .local v0, "currentEobStatus":Z
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedEobStatus:Z

    if-eq v1, v0, :cond_0

    .line 2883
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onEndOfBookPresenceChanged(Z)V

    .line 2884
    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedEobStatus:Z

    .line 2886
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateAccessiblePages(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 2888
    :cond_0
    return-void
.end method

.method private maybeUpdateHiddenTextViewPages()V
    .locals 10

    .prologue
    .line 2904
    iget-object v9, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

    invoke-interface {v9}, Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;->wantsPageRenderings()Z

    move-result v9

    if-nez v9, :cond_1

    .line 2916
    :cond_0
    return-void

    .line 2907
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v9

    if-eqz v9, :cond_2

    sget-object v8, Lcom/google/android/apps/books/widget/PagesViewController;->LANDSCAPE_VISIBLE_OFFSETS:[I

    .line 2910
    .local v8, "visibleOffsets":[I
    :goto_0
    const/4 v6, 0x0

    .line 2911
    .local v6, "viewIndex":I
    move-object v0, v8

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    move v7, v6

    .end local v6    # "viewIndex":I
    .local v7, "viewIndex":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget v4, v0, v1

    .line 2912
    .local v4, "offsetFromCurrentPage":I
    iget v9, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int v3, v9, v4

    .line 2913
    .local v3, "offsetFromBasePosition":I
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v5

    .line 2914
    .local v5, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    iget-object v9, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "viewIndex":I
    .restart local v6    # "viewIndex":I
    invoke-interface {v9, v7, v5}, Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;->setPageRendering(ILcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 2911
    add-int/lit8 v1, v1, 0x1

    move v7, v6

    .end local v6    # "viewIndex":I
    .restart local v7    # "viewIndex":I
    goto :goto_1

    .line 2907
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "offsetFromBasePosition":I
    .end local v4    # "offsetFromCurrentPage":I
    .end local v5    # "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    .end local v7    # "viewIndex":I
    .end local v8    # "visibleOffsets":[I
    :cond_2
    sget-object v8, Lcom/google/android/apps/books/widget/PagesViewController;->PORTRAIT_VISIBLE_OFFSETS:[I

    goto :goto_0
.end method

.method private maybeUpdateSearchBarNavigation()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 4669
    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z

    if-eqz v7, :cond_1

    .line 4724
    :cond_0
    :goto_0
    return-void

    .line 4672
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->searchNavigationEnabled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4673
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getStableSkimViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 4676
    .local v0, "currentSkimViewSpreadIdentifier":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v0, :cond_0

    .line 4680
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    if-eq v7, v0, :cond_0

    .line 4682
    :cond_2
    const/4 v6, 0x1

    .line 4683
    .local v6, "successfullyUpdated":Z
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getStableSkimViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->hasNextSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    move-result-object v1

    .line 4686
    .local v1, "enableNext":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    sget-object v7, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    if-ne v1, v7, :cond_3

    .line 4687
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7, v9}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->enableNextSearch(Z)V

    .line 4695
    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->hasPrevSearchMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    move-result-object v2

    .line 4697
    .local v2, "enablePrevious":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    sget-object v7, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    if-ne v2, v7, :cond_5

    .line 4698
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7, v9}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->enablePreviousSearch(Z)V

    .line 4706
    :goto_2
    new-instance v5, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;

    invoke-direct {v5}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;-><init>()V

    .line 4708
    .local v5, "positionWithinSearchResults":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v7, v0, v5}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->getMatchIndexForSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 4710
    const/4 v6, 0x0

    .line 4719
    :goto_3
    if-eqz v6, :cond_0

    .line 4720
    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    goto :goto_0

    .line 4688
    .end local v2    # "enablePrevious":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .end local v5    # "positionWithinSearchResults":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;
    :cond_3
    sget-object v7, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    if-ne v1, v7, :cond_4

    .line 4689
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7, v10}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->enableNextSearch(Z)V

    goto :goto_1

    .line 4692
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    .line 4699
    .restart local v2    # "enablePrevious":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    :cond_5
    sget-object v7, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    if-ne v2, v7, :cond_6

    .line 4700
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7, v10}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->enablePreviousSearch(Z)V

    goto :goto_2

    .line 4703
    :cond_6
    const/4 v6, 0x0

    goto :goto_2

    .line 4712
    .restart local v5    # "positionWithinSearchResults":Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;
    :cond_7
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->getNumMatches()I

    move-result v4

    .line 4713
    .local v4, "numMatches":I
    iget-boolean v3, v5, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->currentSpreadHasMatches:Z

    .line 4714
    .local v3, "enabled":Z
    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget v8, v5, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$PositionWithinSearchResults;->numMatchesBeforeSpread:I

    invoke-interface {v7, v8, v4, v3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->setSearchBarMatchText(IIZ)V

    goto :goto_3
.end method

.method private maybeUpdateSearchBarNavigationDelayed()V
    .locals 3

    .prologue
    .line 4649
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchBarUpdatePending:Z

    if-eqz v1, :cond_0

    .line 4666
    :goto_0
    return-void

    .line 4652
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchBarUpdatePending:Z

    .line 4653
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    .line 4654
    .local v0, "manager":Lcom/google/android/apps/books/util/UIThreadTaskManager;
    new-instance v1, Lcom/google/android/apps/books/widget/PagesViewController$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/widget/PagesViewController$7;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->post(Lcom/google/android/apps/books/util/UIThreadTaskManager$Task;I)V

    goto :goto_0
.end method

.method private onMissingPosition(Lcom/google/android/apps/books/common/Position;)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 1138
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    if-nez v0, :cond_1

    .line 1142
    :cond_0
    :goto_0
    return-void

    .line 1141
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onMissingPosition(Lcom/google/android/apps/books/common/Position;)V

    goto :goto_0
.end method

.method private onNonTurningDecorationsChanged()V
    .locals 1

    .prologue
    .line 1905
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-eqz v0, :cond_0

    .line 1906
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->invalidatePageContent()V

    .line 1908
    :cond_0
    return-void
.end method

.method private onOffsetChanged()V
    .locals 2

    .prologue
    .line 2868
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onPagesChanged()V

    .line 2869
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->updateCurrentPageRange()V

    .line 2870
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateEobStatus()V

    .line 2871
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BookView;->onPageOffsetChanged(I)V

    .line 2872
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PagesView;->onPageOffsetChanged(I)V

    .line 2873
    return-void
.end method

.method private onPagesChanged()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2764
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateHiddenTextViewPages()V

    .line 2771
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v1

    .line 2773
    .local v1, "mainPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v1, :cond_1

    .line 2778
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->maybePublishVisiblePages(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 2800
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v2

    .line 2801
    .local v2, "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2802
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-boolean v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSaveLastMove:Z

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastAction:Lcom/google/android/apps/books/app/MoveType;

    invoke-interface {v3, v2, v4, v5}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onPositionChanged(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 2803
    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastPublishedPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 2804
    iput-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastCenterPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 2805
    iput-boolean v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderedFirstMainPage:Z

    .line 2807
    .end local v2    # "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_0
    return-void

    .line 2779
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    if-eq v3, v4, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    if-ne v3, v4, :cond_0

    .line 2789
    :cond_3
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 2790
    .local v0, "leftPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v0, :cond_0

    .line 2794
    new-array v3, v6, [Lcom/google/android/apps/books/widget/DevicePageRendering;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->maybePublishVisiblePages(Ljava/util/List;)V

    .line 2795
    move-object v1, v0

    .line 2796
    goto :goto_0
.end method

.method private onSpecialState(Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 7
    .param p1, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p2, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x3

    .line 3248
    if-nez p1, :cond_1

    .line 3249
    const-string v3, "PagesViewHelper"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3250
    const-string v3, "PagesViewHelper"

    const-string v4, "Ignoring special state for null cookie"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3345
    :cond_0
    :goto_0
    return-void

    .line 3255
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->isCurrentRequest(Lcom/google/android/apps/books/util/RenderRequestContext;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 3256
    const-string v3, "PagesViewHelper"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3257
    const-string v3, "PagesViewHelper"

    const-string v4, "Ignoring special state for old base position"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3262
    :cond_2
    const-string v3, "PagesViewHelper"

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3263
    const-string v3, "PagesViewHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onSpecialState() for targetSlot="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3267
    :cond_3
    const/4 v2, 0x0

    .line 3269
    .local v2, "newPosition":Lcom/google/android/apps/books/render/SpreadIdentifier;
    const/4 v0, 0x0

    .line 3270
    .local v0, "displayType":Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    sget-object v3, Lcom/google/android/apps/books/widget/PagesViewController$8;->$SwitchMap$com$google$android$apps$books$render$SpecialPageIdentifier$SpecialPageType:[I

    iget-object v4, p2, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->type:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 3321
    :cond_4
    :goto_1
    if-eqz v0, :cond_5

    .line 3322
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCustomOffsets:Landroid/support/v4/util/SparseArrayCompat;

    iget v4, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    invoke-virtual {v3, v4, v0}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 3323
    iget v3, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    .line 3332
    :cond_5
    iget v3, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->removePendingPageRequest(I)V

    .line 3334
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateEobStatus()V

    .line 3335
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onPagesChanged()V

    .line 3337
    if-eqz v2, :cond_0

    .line 3343
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastAction:Lcom/google/android/apps/books/app/MoveType;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;)V

    goto :goto_0

    .line 3272
    :pswitch_0
    iget v3, p2, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    if-ne v3, v6, :cond_6

    .line 3274
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    iget v4, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/PagesViewUtils;->maxIgnoringNull(Ljava/lang/Integer;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageBeforeStartOfVolume:Ljava/lang/Integer;

    .line 3276
    sget-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    goto :goto_1

    .line 3277
    :cond_6
    iget v3, p2, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    if-ltz v3, :cond_4

    .line 3281
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 3285
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    iget v4, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    iget v5, p2, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/PagesViewUtils;->maxIgnoringNull(Ljava/lang/Integer;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    .line 3293
    :goto_2
    sget-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 3294
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->maybeLoadEndOfBookPage()V

    .line 3297
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 3298
    :cond_7
    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    .line 3303
    .local v1, "earliestCenterOffsetInReadingDirection":I
    :goto_3
    iget v3, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    if-ne v3, v1, :cond_4

    .line 3309
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/android/apps/books/render/SpreadIdentifier;->offsetBy(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v2

    goto :goto_1

    .line 3289
    .end local v1    # "earliestCenterOffsetInReadingDirection":I
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    iget v4, p1, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    iget v5, p2, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->margin:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/books/util/PagesViewUtils;->minIgnoringNull(Ljava/lang/Integer;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    goto :goto_2

    .line 3301
    :cond_9
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    add-int/lit8 v1, v3, -0x1

    .restart local v1    # "earliestCenterOffsetInReadingDirection":I
    goto :goto_3

    .line 3315
    .end local v1    # "earliestCenterOffsetInReadingDirection":I
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    goto/16 :goto_1

    .line 3270
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private paintHighlightRects(Lcom/google/android/apps/books/widget/Walker;Landroid/graphics/Canvas;Ljava/lang/String;Landroid/graphics/Rect;Z)V
    .locals 9
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "theme"    # Ljava/lang/String;
    .param p4, "clipRect"    # Landroid/graphics/Rect;
    .param p5, "coerceToAnnotationColor"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/widget/HighlightsSharingColor;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2482
    .local p1, "walker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    iget-object v8, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempHighlightsSharingColor:Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    .line 2483
    .local v8, "hsc":Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    :goto_0
    invoke-interface {p1, v8}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2484
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;

    iget v3, v8, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->color:I

    iget-object v4, v8, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->highlightRects:Lcom/google/android/apps/books/widget/Walker;

    iget-boolean v5, v8, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->isSelected:Z

    move-object v1, p2

    move-object v2, p3

    move-object v6, p4

    move v7, p5

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/books/render/PageCanvasPainter;->paintHighlightRects(Landroid/graphics/Canvas;Ljava/lang/String;ILcom/google/android/apps/books/widget/Walker;ZLandroid/graphics/Rect;Z)V

    goto :goto_0

    .line 2487
    :cond_0
    return-void
.end method

.method private paintHighlightRectsAndMarginNotes(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 19
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p3, "canvas"    # Landroid/graphics/Canvas;
    .param p4, "clipRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2395
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getRenderingTheme()Ljava/lang/String;

    move-result-object v5

    .line 2397
    .local v5, "theme":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->getHighlightRectsWalker(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v3

    .line 2399
    .local v3, "highlightsSharingColorWalker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    if-eqz v3, :cond_0

    .line 2401
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p3

    move-object/from16 v6, p4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/widget/PagesViewController;->paintHighlightRects(Lcom/google/android/apps/books/widget/Walker;Landroid/graphics/Canvas;Ljava/lang/String;Landroid/graphics/Rect;Z)V

    .line 2404
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mDrawAnnotations:Z

    if-eqz v2, :cond_2

    .line 2405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-interface {v2, v0, v4, v6}, Lcom/google/android/apps/books/render/Renderer;->getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v7

    .line 2407
    .local v7, "rects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/util/List;

    move-result-object v17

    .line 2408
    .local v17, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mMarginNoteIconTapSize:I

    const/4 v6, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-interface {v2, v0, v1, v4, v6}, Lcom/google/android/apps/books/render/Renderer;->getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v18

    .line 2414
    .local v18, "marginNoteIcons":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    if-eqz v18, :cond_2

    if-eqz v7, :cond_2

    .line 2417
    const/4 v11, 0x1

    move-object/from16 v6, p0

    move-object/from16 v8, p3

    move-object v9, v5

    move-object/from16 v10, p4

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/books/widget/PagesViewController;->paintHighlightRects(Lcom/google/android/apps/books/widget/Walker;Landroid/graphics/Canvas;Ljava/lang/String;Landroid/graphics/Rect;Z)V

    .line 2418
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->isVertical()Z

    move-result v10

    .line 2420
    .local v10, "isVertical":Z
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/widget/PagesViewController;->updateMinMarginNoteIconSize(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 2421
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 2422
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    iget-object v8, v2, Lcom/google/android/apps/books/render/MarginNote;->icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .line 2425
    .local v8, "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    if-eqz v8, :cond_1

    .line 2426
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mMinMarginNoteIconSize:Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v9, p1

    move-object/from16 v11, p2

    invoke-virtual/range {v8 .. v13}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->getPaintRect(Lcom/google/android/apps/books/widget/DevicePageRendering;ZLcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer;)Landroid/graphics/Rect;

    move-result-object v14

    .line 2428
    .local v14, "paintRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    iget v15, v2, Lcom/google/android/apps/books/render/MarginNote;->color:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempMarginNote:Lcom/google/android/apps/books/render/MarginNote;

    iget-object v2, v2, Lcom/google/android/apps/books/render/MarginNote;->annotationId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    move-object/from16 v12, p3

    move-object v13, v5

    invoke-interface/range {v11 .. v16}, Lcom/google/android/apps/books/render/PageCanvasPainter;->paintMarginNoteIcon(Landroid/graphics/Canvas;Ljava/lang/String;Landroid/graphics/Rect;IZ)V

    goto :goto_0

    .line 2435
    .end local v7    # "rects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    .end local v8    # "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    .end local v10    # "isVertical":Z
    .end local v14    # "paintRect":Landroid/graphics/Rect;
    .end local v17    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .end local v18    # "marginNoteIcons":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    :cond_2
    return-void
.end method

.method private paintTtsHighlight(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2498
    const-string v0, "PagesViewHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2499
    const-string v0, "PagesViewHelper"

    const-string v1, "Applying highlight rects to page"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2502
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTextHighlightRenderer:Lcom/google/android/apps/books/render/TextHighlightRenderer;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRectangles:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/render/TextHighlightRenderer;->paintHighlightRectangles(Landroid/graphics/Canvas;Ljava/util/List;)V

    .line 2503
    return-void
.end method

.method private processPageRenderingForAnyView(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 6
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 3205
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToRendering:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v4

    invoke-interface {v3, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3207
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v2

    .line 3213
    .local v2, "passageIndex":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v3}, Lcom/google/android/apps/books/render/Renderer;->needsBackgroundAnnotationLoad()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3215
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->maybeLoadDataForPassage(IZ)V

    .line 3216
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->maybeLoadDataForPassage(IZ)V

    .line 3217
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->maybeLoadDataForPassage(IZ)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3225
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 3226
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    if-eqz v3, :cond_1

    .line 3228
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v1

    .line 3229
    .local v1, "pageIndex":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, p1, v1, v5}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->maybeLoadDataForPage(Ljava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)V
    :try_end_1
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 3239
    .end local v1    # "pageIndex":I
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    if-eqz v3, :cond_2

    .line 3240
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v3, v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onPassageBecameVisible(I)V

    .line 3243
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigationDelayed()V

    .line 3244
    return-void

    .line 3218
    :catch_0
    move-exception v0

    .line 3219
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 3231
    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :catch_1
    move-exception v0

    .line 3232
    .restart local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v3, "PagesViewHelper"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3233
    const-string v3, "PagesViewHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad page id for loading highlights: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private processPageRenderingForFullView(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 8
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    const/4 v4, 0x3

    .line 3138
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z

    if-eqz v1, :cond_0

    .line 3197
    :goto_0
    return-void

    .line 3142
    :cond_0
    const-string v1, "PagesViewHelper"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3143
    const-string v1, "PagesViewHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processPageRenderingForFullView() for page="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cookie="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3146
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getFirstViewablePosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "PagesViewHelper"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3148
    const-string v1, "PagesViewHelper"

    const-string v2, "No reading positions on page"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3151
    :cond_2
    if-nez p2, :cond_4

    .line 3153
    const-string v1, "PagesViewHelper"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3154
    const-string v1, "PagesViewHelper"

    const-string v2, "Ignoring render result for unknown request"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3156
    :cond_3
    invoke-interface {p3}, Lcom/google/android/apps/books/render/PagePainter;->recycle()V

    goto :goto_0

    .line 3160
    :cond_4
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->isCurrentRequest(Lcom/google/android/apps/books/util/RenderRequestContext;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 3161
    const-string v1, "PagesViewHelper"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3162
    const-string v1, "PagesViewHelper"

    const-string v2, "Ignoring render result for old rendering context"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3164
    :cond_5
    invoke-interface {p3}, Lcom/google/android/apps/books/render/PagePainter;->recycle()V

    goto :goto_0

    .line 3168
    :cond_6
    iget v7, p2, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    .line 3170
    .local v7, "pageOffsetFromBasePage":I
    invoke-direct {p0, v7}, Lcom/google/android/apps/books/widget/PagesViewController;->removePendingPageRequest(I)V

    .line 3171
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDbg:Z

    if-eqz v1, :cond_7

    .line 3172
    const-string v1, "PagesViewHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RqstRemoved("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3181
    :cond_7
    new-instance v0, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;

    invoke-direct {p0, v7}, Lcom/google/android/apps/books/widget/PagesViewController;->getSideOfSpine(I)Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesViewController$BaseDecoratingPagePainter;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/Logger;)V

    .line 3184
    .local v0, "baseDecoratingPainter":Lcom/google/android/apps/books/render/PagePainter;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    new-instance v2, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;

    invoke-direct {v2, p1, p2, p3, v0}, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;-><init>(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;)V

    invoke-virtual {v1, v7, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 3188
    new-instance v6, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLogger:Lcom/google/android/apps/books/util/Logger;

    invoke-direct {v6, p0, p1, v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController$NonTurningPagePainter;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/Logger;)V

    .line 3192
    .local v6, "nonTurningPagePainter":Lcom/google/android/apps/books/render/PagePainter;
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onPagesChanged()V

    .line 3194
    invoke-direct {p0, p1, v0, v6, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->publishPageData(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/RenderRequestContext;)V

    .line 3196
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->processPageRenderingForAnyView(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    goto/16 :goto_0
.end method

.method private processPendingPage()Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 3546
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->isBusy()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/PageTurnController;->isTurning()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTimeStartedLastTurn:J

    const-wide/16 v6, 0x1f4

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 3555
    :cond_0
    const/4 v2, 0x1

    .line 3570
    :goto_0
    return v2

    .line 3558
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 3559
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;>;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3560
    const-string v2, "PagesViewHelper"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3561
    const-string v2, "PagesViewHelper"

    const-string v3, "Processing pending page"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3563
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3564
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 3565
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->sendPageUpdateToPagesView(Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;)V

    .line 3567
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;>;"
    :cond_3
    const-string v2, "PagesViewHelper"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3568
    const-string v2, "PagesViewHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pending pages left"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3570
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto :goto_0
.end method

.method private publishPageData(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/util/RenderRequestContext;)V
    .locals 20
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "baseDecoratingPainter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p3, "nonTurningPagePainter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;

    .prologue
    .line 3099
    move-object/from16 v0, p4

    iget v5, v0, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    .line 3101
    .local v5, "pageOffsetFromBasePage":I
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getCurrentRelevantOffsets()Ljava/util/Set;

    move-result-object v10

    .line 3103
    .local v10, "currentOffsets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/apps/books/widget/PagesViewController;->clearObsoletePendingPages(Ljava/util/Set;)V

    .line 3105
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v10, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3127
    :goto_0
    return-void

    .line 3109
    :cond_0
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v19

    .line 3110
    .local v19, "fittedSize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/PagesView;->getOnePageWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PagesView;->getOnePageHeight()I

    move-result v6

    move-object/from16 v0, v19

    invoke-static {v0, v4, v6}, Lcom/google/android/apps/books/util/PagesViewUtils;->fitInto(Landroid/graphics/Point;II)V

    .line 3112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastReceivedPageFittedSize:Landroid/graphics/Point;

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, v19

    iget v12, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v6, v12}, Landroid/graphics/Point;->set(II)V

    .line 3114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageOffsetToFullSizeInfo:Landroid/support/v4/util/SparseArrayCompat;

    new-instance v6, Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;-><init>(Lcom/google/android/apps/books/widget/PagesViewController;Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;)V

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 3117
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    move-result-object v7

    .line 3119
    .local v7, "bookmark":Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 3120
    .local v8, "now":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    move-object/from16 v6, p3

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/apps/books/widget/BookView;->setPageRendering(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V

    .line 3123
    new-instance v11, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;

    const/16 v18, 0x0

    move-object/from16 v12, p1

    move-object/from16 v13, p4

    move-object/from16 v14, p2

    move-object v15, v7

    move-wide/from16 v16, v8

    invoke-direct/range {v11 .. v18}, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;-><init>(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;JLcom/google/android/apps/books/widget/PagesViewController$1;)V

    .line 3125
    .local v11, "task":Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    invoke-interface {v4}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    goto :goto_0
.end method

.method private readingOffsetFromScreenOffset(I)I
    .locals 3
    .param p1, "screenOffset"    # I

    .prologue
    .line 1601
    sget-object v1, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/ScreenDirection;->fromReadingDirection(Lcom/google/android/apps/books/util/ReadingDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    .line 1603
    .local v0, "screenDirection":Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/ScreenDirection;->dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;I)I

    move-result v1

    return v1
.end method

.method private redrawPages()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1870
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, v0, v1

    .line 1871
    .local v3, "offset":I
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(I)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v4

    .line 1872
    .local v4, "position":Lcom/google/android/apps/books/render/RenderPosition;
    if-eqz v4, :cond_0

    .line 1873
    invoke-virtual {p0, v4, v3, v5, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;IZZ)V

    .line 1870
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1876
    .end local v3    # "offset":I
    .end local v4    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    :cond_1
    return-void
.end method

.method private removeCurrentBookmarkListener()V
    .locals 2

    .prologue
    .line 2551
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    if-eqz v0, :cond_0

    .line 2552
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkListener:Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/BookmarkController;->removeListener(Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;)V

    .line 2554
    :cond_0
    return-void
.end method

.method private removePendingPageRequest(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 259
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    .line 260
    .local v0, "oldConsumer":Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    if-eqz v0, :cond_0

    .line 261
    # invokes: Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->recycle()V
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;->access$100(Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)V

    .line 262
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 263
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    .line 265
    :cond_0
    return-void
.end method

.method private requestPage(Ljava/lang/Integer;Lcom/google/android/apps/books/render/RenderPosition;)V
    .locals 4
    .param p1, "offset"    # Ljava/lang/Integer;
    .param p2, "request"    # Lcom/google/android/apps/books/render/RenderPosition;

    .prologue
    .line 1295
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/RenderPosition;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->makeFullViewRenderResponseConsumer(ILcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;

    move-result-object v0

    .line 1297
    .local v0, "consumer":Lcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->addPendingPageRequest(ILcom/google/android/apps/books/widget/PagesViewController$FullViewRenderResponseConsumer;)V

    .line 1298
    iget-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDbg:Z

    if-eqz v1, :cond_0

    .line 1299
    const-string v1, "PagesViewHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RqstPage("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPageRequests:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1301
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v1, p2, v0}, Lcom/google/android/apps/books/render/Renderer;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    .line 1302
    return-void
.end method

.method private sendPageUpdateToPagesView(Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;)V
    .locals 6
    .param p1, "pending"    # Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;

    .prologue
    const/4 v2, 0x0

    .line 3594
    iget-object v0, p1, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->cookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    iget v1, v0, Lcom/google/android/apps/books/util/RenderRequestContext;->offsetFromBasePage:I

    .line 3596
    .local v1, "offset":I
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mExecutingInitialLoadTransition:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 3598
    if-nez v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlot0:Z

    if-eqz v0, :cond_1

    .line 3599
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlot0:Z

    .line 3610
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget-object v2, p1, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->painter:Lcom/google/android/apps/books/render/PagePainter;

    iget-object v3, p1, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    iget-wide v4, p1, Lcom/google/android/apps/books/widget/PagesViewController$PendingPage;->transitionStartTime:J

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesView;->setPageRendering(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V

    .line 3612
    :goto_1
    return-void

    .line 3600
    :cond_1
    const/4 v0, -0x1

    if-ne v1, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlotNeg1:Z

    if-eqz v0, :cond_2

    .line 3601
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlotNeg1:Z

    goto :goto_0

    .line 3605
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private setPageLoading(I)V
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 1809
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 1810
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->maybeLoadEndOfBookPage()V

    .line 1812
    :cond_0
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getInterpolatedPageSize(ILandroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 1813
    .local v0, "size":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/books/widget/PagesView;->setPageLoading(ILandroid/graphics/Point;)V

    .line 1814
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/widget/BookView;->setPageLoading(ILandroid/graphics/Point;)V

    .line 1815
    return-void
.end method

.method private setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V
    .locals 1
    .param p1, "specialPage"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p2, "targetOffset"    # I

    .prologue
    .line 1800
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getSpecialPageBitmap(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1801
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1802
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;ILandroid/graphics/Bitmap;)V

    .line 1806
    :goto_0
    return-void

    .line 1804
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->setPageLoading(I)V

    goto :goto_0
.end method

.method private setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;ILandroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "specialPage"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p2, "targetOffset"    # I
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1789
    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->displayAtFullSize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1790
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOnePageSize:Landroid/graphics/Point;

    .line 1794
    .local v5, "size":Landroid/graphics/Point;
    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 1795
    .local v6, "now":J
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-interface/range {v1 .. v7}, Lcom/google/android/apps/books/widget/PagesView;->setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;J)V

    .line 1796
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/books/widget/BookView;->setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;J)V

    .line 1797
    return-void

    .line 1792
    .end local v5    # "size":Landroid/graphics/Point;
    .end local v6    # "now":J
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->getInterpolatedPageSize(ILandroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v5

    .restart local v5    # "size":Landroid/graphics/Point;
    goto :goto_0
.end method

.method private setTtsRectangles(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "rectangles":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Rect;>;"
    const/4 v8, 0x3

    .line 1676
    const-string v5, "PagesViewHelper"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1677
    const-string v5, "PagesViewHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setTtsRectangles() called with rectangles "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1678
    const-string v5, "PagesViewHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Offset from base position is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1681
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRectangles:Ljava/util/List;

    .line 1683
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-nez v5, :cond_2

    .line 1746
    :cond_1
    :goto_0
    return-void

    .line 1689
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    instance-of v5, v5, Lcom/google/android/apps/books/render/ImageModeRenderer;

    if-nez v5, :cond_1

    .line 1695
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/render/Renderer;->getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v3

    .line 1697
    .local v3, "pages":Lcom/google/android/apps/books/util/PassagePages;
    invoke-static {p1}, Lcom/google/android/apps/books/util/MathUtils;->getBoundingRect(Ljava/util/List;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1698
    .local v0, "boundingRect":Landroid/graphics/Rect;
    if-eqz v0, :cond_1

    .line 1703
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->makePageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v2

    .line 1705
    .local v2, "page":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v2, :cond_3

    .line 1707
    const-string v5, "PagesViewHelper"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1708
    const-string v5, "PagesViewHelper"

    const-string v6, "Have TTS rectangles but no rendering for the current page"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1713
    :cond_3
    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 1716
    .local v4, "positionToSearchFor":I
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v6

    if-ne v5, v6, :cond_5

    .line 1717
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/util/PassagePages;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageBounds()Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;->intersects(Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1721
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v1

    .line 1730
    .local v1, "newPageIndex":I
    :goto_1
    const/4 v5, -0x1

    if-ne v1, v5, :cond_6

    .line 1734
    invoke-virtual {v3}, Lcom/google/android/apps/books/util/PassagePages;->getEndOfLastPage()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 1735
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0, v5, v6, v7}, Lcom/google/android/apps/books/widget/PagesViewController;->turnToPage(IILcom/google/android/apps/books/widget/PageTurnController;)V

    goto :goto_0

    .line 1723
    .end local v1    # "newPageIndex":I
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/books/util/PassagePages;->getPageIndexForX(ILjava/lang/Integer;)I

    move-result v1

    .restart local v1    # "newPageIndex":I
    goto :goto_1

    .line 1727
    .end local v1    # "newPageIndex":I
    :cond_5
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/books/util/PassagePages;->getPageIndexForX(ILjava/lang/Integer;)I

    move-result v1

    .restart local v1    # "newPageIndex":I
    goto :goto_1

    .line 1737
    :cond_6
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v6

    if-eq v5, v6, :cond_7

    .line 1738
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0, v5, v1, v6}, Lcom/google/android/apps/books/widget/PagesViewController;->turnToPage(IILcom/google/android/apps/books/widget/PageTurnController;)V

    goto/16 :goto_0

    .line 1739
    :cond_7
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v5

    if-eq v1, v5, :cond_1

    .line 1741
    const-string v5, "PagesViewHelper"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1742
    const-string v5, "PagesViewHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "turning to next page "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1744
    :cond_8
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0, v5, v1, v6}, Lcom/google/android/apps/books/widget/PagesViewController;->turnToPage(IILcom/google/android/apps/books/widget/PageTurnController;)V

    goto/16 :goto_0
.end method

.method private setupSelectionOverlay()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 3046
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getView()Landroid/view/View;

    move-result-object v2

    .line 3047
    .local v2, "pagesView":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/BookView;->getView()Landroid/view/View;

    move-result-object v0

    .line 3049
    .local v0, "baseView":Landroid/view/View;
    new-instance v4, Lcom/google/android/apps/books/widget/SelectionOverlay;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getRenderingTheme()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v7}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getSelectionPopup()Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/books/widget/SelectionOverlay;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/books/widget/SelectionPopup;)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    .line 3052
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 3053
    .local v3, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 3055
    .local v1, "baseViewIndex":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    add-int/lit8 v5, v1, 0x1

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v8, v8}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 3058
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->updatePageToViewMatrix()V

    .line 3059
    return-void
.end method

.method private shouldPaintTtsHighlight(Lcom/google/android/apps/books/widget/DevicePageRendering;)Z
    .locals 2
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 2492
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRectangles:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showBookView()V
    .locals 2

    .prologue
    .line 3940
    const-string v0, "PagesViewHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3941
    const-string v0, "PagesViewHelper"

    const-string v1, "Showing BookView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3943
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-nez v0, :cond_1

    .line 3950
    :goto_0
    return-void

    .line 3946
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BookView;->setVisibility(I)V

    .line 3949
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->requestLayout()V

    goto :goto_0
.end method

.method private showSkimView(Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    .locals 8
    .param p1, "centerSpread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 4874
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    .line 4875
    .local v0, "bookView":Lcom/google/android/apps/books/widget/BookView;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/widget/BookView;->setVisibility(I)V

    .line 4876
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v2

    .line 4877
    .local v2, "navView":Lcom/google/android/apps/books/navigation/BookNavView;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesViewCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$PagesViewCallbacks;

    invoke-virtual {v2, v4, v5, v6, p1}, Lcom/google/android/apps/books/navigation/BookNavView;->prepare(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    .line 4878
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v4}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 4879
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v4, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;

    .line 4880
    .local v3, "result":Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;
    iget-object v4, v3, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mDecorator:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v4, :cond_0

    .line 4881
    iget-object v5, v3, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPageInfo:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v6, v3, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mDecorator:Lcom/google/android/apps/books/render/PagePainter;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToBookmark:Ljava/util/Map;

    iget-object v7, v3, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPageInfo:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    invoke-virtual {v2, v5, v6, v4}, Lcom/google/android/apps/books/navigation/BookNavView;->importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 4878
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4885
    .end local v3    # "result":Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->showSkimView()V

    .line 4886
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v4, p1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->moveScrubberToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    .line 4887
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPreparedNavView:Z

    .line 4888
    return-void
.end method

.method private showingNavigationView()Z
    .locals 2

    .prologue
    .line 4894
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->getVisibleMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private squaredDistanceFromPointToIcon(Lcom/google/android/apps/books/render/MarginNote;Landroid/graphics/Point;Z)J
    .locals 6
    .param p1, "note"    # Lcom/google/android/apps/books/render/MarginNote;
    .param p2, "rendererTouchPoint"    # Landroid/graphics/Point;
    .param p3, "useTapArea"    # Z

    .prologue
    const-wide/16 v2, -0x1

    .line 2153
    iget-object v0, p1, Lcom/google/android/apps/books/render/MarginNote;->icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    .line 2154
    .local v0, "icon":Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;
    if-nez v0, :cond_1

    .line 2162
    :cond_0
    :goto_0
    return-wide v2

    .line 2155
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->getTapAreaRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 2156
    .local v1, "tapAreaRect":Landroid/graphics/Rect;
    if-eqz v1, :cond_0

    .line 2158
    if-eqz p3, :cond_2

    .line 2159
    iget v2, p2, Landroid/graphics/Point;->x:I

    iget v3, p2, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/MathUtils;->squaredDistanceFromPointToRect(IILandroid/graphics/Rect;)J

    move-result-wide v2

    goto :goto_0

    .line 2162
    :cond_2
    iget v2, p2, Landroid/graphics/Point;->x:I

    iget v3, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/books/util/MathUtils;->squaredDistanceBetweenPoints(IIII)J

    move-result-wide v2

    goto :goto_0
.end method

.method private toggleBookmark(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    const/4 v2, 0x0

    .line 2541
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/books/widget/DevicePageRendering;

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/BookmarkController;->toggleBookmarks(Ljava/util/List;Z)V

    .line 2542
    return-void
.end method

.method private turnToPage(IILcom/google/android/apps/books/widget/PageTurnController;)V
    .locals 3
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "pageTurnController"    # Lcom/google/android/apps/books/widget/PageTurnController;

    .prologue
    const/4 v2, 0x0

    .line 1754
    invoke-static {p1, p2, v2}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 1755
    .local v0, "pi":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->resetZoom()V

    .line 1756
    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastTurnFromUser:Z

    .line 1757
    invoke-virtual {p3, v0}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueTurnToPosition(Lcom/google/android/apps/books/render/PageIdentifier;)V

    .line 1758
    return-void
.end method

.method private updateCurrentPageRange()V
    .locals 7

    .prologue
    .line 4587
    iget v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->makePageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    .line 4588
    .local v4, "page":Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v4, :cond_0

    .line 4627
    :goto_0
    return-void

    .line 4593
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 4598
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 4600
    const/4 v2, -0x2

    .line 4601
    .local v2, "minOffset":I
    const/4 v1, 0x3

    .line 4612
    .local v1, "maxOffset":I
    :goto_1
    new-instance v5, Lcom/google/android/apps/books/render/Renderer$PageRange;

    invoke-direct {v5, v4, v2, v1}, Lcom/google/android/apps/books/render/Renderer$PageRange;-><init>(Lcom/google/android/apps/books/render/PageIdentifier;II)V

    iput-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

    .line 4613
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCurrentPageRange:Lcom/google/android/apps/books/render/Renderer$PageRange;

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/render/Renderer;->setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V

    .line 4619
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_2
    if-ltz v0, :cond_5

    .line 4620
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    sub-int v3, v5, v6

    .line 4621
    .local v3, "offset":I
    if-lt v3, v2, :cond_1

    if-le v3, v1, :cond_2

    .line 4622
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    .line 4619
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 4604
    .end local v0    # "i":I
    .end local v1    # "maxOffset":I
    .end local v2    # "minOffset":I
    .end local v3    # "offset":I
    :cond_3
    const/4 v2, -0x3

    .line 4605
    .restart local v2    # "minOffset":I
    const/4 v1, 0x2

    .restart local v1    # "maxOffset":I
    goto :goto_1

    .line 4608
    .end local v1    # "maxOffset":I
    .end local v2    # "minOffset":I
    :cond_4
    const/4 v2, -0x1

    .line 4609
    .restart local v2    # "minOffset":I
    const/4 v1, 0x1

    .restart local v1    # "maxOffset":I
    goto :goto_1

    .line 4626
    .restart local v0    # "i":I
    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigationDelayed()V

    goto :goto_0
.end method

.method private updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 3
    .param p1, "touchedPage"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "pagePositionOnScreen"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 2650
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererToViewTransform:Landroid/graphics/Matrix;

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/books/render/Renderer;->getPageToViewMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Matrix;)Z

    .line 2652
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    if-ne p2, v0, :cond_0

    .line 2653
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererToViewTransform:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2655
    :cond_0
    return-void
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;)V
    .locals 5
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x3

    .line 1657
    const-string v0, "PagesViewHelper"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1658
    const-string v1, "PagesViewHelper"

    const-string v2, "activateMediaElement(passageIndex=%d, pageOffset=%d, elementId=%s)"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x2

    if-nez p3, :cond_1

    const-string v0, "null"

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1662
    :cond_0
    iput p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    .line 1664
    iput p2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementPageOffset:I

    .line 1666
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/Renderer;->activateMediaElement(IILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMoElementHighlightRequestId:I

    .line 1668
    return-void

    :cond_1
    move-object v0, p3

    .line 1658
    goto :goto_0
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 4
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 4334
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 4336
    .local v0, "spread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/render/Renderer;->applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V

    .line 4337
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderSettings:Lcom/google/android/apps/books/render/ReaderSettings;

    .line 4339
    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/books/app/MoveType;->UNSPECIFIED:Lcom/google/android/apps/books/app/MoveType;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;)V

    .line 4341
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->invalidateAnnotationRects()V

    .line 4342
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->invalidateVolumeAnnotationRects()V

    .line 4343
    return-void
.end method

.method public clearHighlight()V
    .locals 1

    .prologue
    .line 1611
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightBoundsRequestId:I

    .line 1612
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRectangles:Ljava/util/List;

    .line 1613
    return-void
.end method

.method public clearSearchMatchHighlights()V
    .locals 1

    .prologue
    .line 1618
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    .line 1619
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->clearData()V

    .line 1620
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->clearRequestsAndCacheIndices()V

    .line 1621
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 1622
    return-void
.end method

.method public clearSearchMatchHighlights(I)V
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->clearData(I)V

    .line 1628
    return-void
.end method

.method public dismissTextSelection()V
    .locals 0

    .prologue
    .line 1969
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V

    .line 1970
    return-void
.end method

.method public endOfBookPageSequenceLength()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3519
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3527
    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int/2addr v1, v2

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    .line 3530
    .local v0, "evenOffsetFromRightPage":Z
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v1

    if-ne v1, v0, :cond_1

    const/4 v1, 0x2

    .line 3534
    .end local v0    # "evenOffsetFromRightPage":Z
    :goto_1
    return v1

    .line 3527
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3530
    .restart local v0    # "evenOffsetFromRightPage":Z
    :cond_1
    const/4 v1, 0x3

    goto :goto_1

    .end local v0    # "evenOffsetFromRightPage":Z
    :cond_2
    move v1, v0

    .line 3534
    goto :goto_1
.end method

.method public endTurn(Z)Z
    .locals 4
    .param p1, "canceled"    # Z

    .prologue
    .line 2664
    const-string v1, "PagesViewHelper"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2665
    const-string v1, "PagesViewHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "endTurn("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2668
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueEndTurn(Z)Z

    move-result v0

    .line 2670
    .local v0, "result":Z
    if-eqz v0, :cond_1

    .line 2672
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onUserSelectedNewPosition()V

    .line 2675
    :cond_1
    return v0
.end method

.method public enqueueEndTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZF)V
    .locals 3
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "flinging"    # Z
    .param p3, "velocity"    # F

    .prologue
    .line 2284
    const-string v0, "PagesViewHelper"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2285
    const-string v0, "PagesViewHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enqueueEndTurn("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2287
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueEndTurn(Lcom/google/android/apps/books/util/ScreenDirection;ZF)V

    .line 2288
    return-void
.end method

.method public enqueueStartTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 4
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p2, "finishAutomatically"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2252
    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->LEFT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/util/ScreenDirection;->RIGHT:Lcom/google/android/apps/books/util/ScreenDirection;

    if-ne p1, v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkState(Z)V

    .line 2254
    const-string v0, "PagesViewHelper"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2255
    const-string v0, "PagesViewHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enqueueStartTurn("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2258
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTimeStartedLastTurn:J

    .line 2260
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->resetZoom()V

    .line 2261
    iput-boolean v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastTurnFromUser:Z

    .line 2262
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueStartTurn(Lcom/google/android/apps/books/util/ScreenDirection;Z)V

    .line 2263
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V

    .line 2264
    return-void

    .line 2252
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enqueueUpdateTurn(F)V
    .locals 3
    .param p1, "dx"    # F

    .prologue
    .line 2272
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSwipeDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-interface {v1, p1, v2}, Lcom/google/android/apps/books/widget/PagesView;->dxToFraction(FLcom/google/android/apps/books/util/ScreenDirection;)F

    move-result v0

    .line 2273
    .local v0, "fraction":F
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/PageTurnController;->enqueueUpdateTurn(F)V

    .line 2274
    return-void
.end method

.method public findTappedTouchableItem(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/render/TouchableItem;
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "type"    # I

    .prologue
    const/4 v2, 0x0

    .line 1434
    iget v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->getTouchedPageEvent(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/widget/TouchedPageEvent;

    move-result-object v3

    .line 1435
    .local v3, "touchEvent":Lcom/google/android/apps/books/widget/TouchedPageEvent;
    iget-object v4, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-nez v4, :cond_1

    .line 1458
    :cond_0
    :goto_0
    return-object v2

    .line 1439
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    iget-object v4, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    invoke-direct {v1, v4}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    .line 1440
    .local v1, "rendererTouchPoint":Landroid/graphics/Point;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v5, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v6, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-interface {v4, v1, v5, v6}, Lcom/google/android/apps/books/render/Renderer;->convertScreenPointToRendererCoordinates(Landroid/graphics/Point;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1445
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v5, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-interface {v4, v5, v1, p2}, Lcom/google/android/apps/books/render/Renderer;->findTouchableItem(Lcom/google/android/apps/books/widget/DevicePageRendering;Landroid/graphics/Point;I)Lcom/google/android/apps/books/render/TouchableItem;

    move-result-object v2

    .line 1448
    .local v2, "tappedItem":Lcom/google/android/apps/books/render/TouchableItem;
    if-eqz v2, :cond_0

    .line 1450
    iget-object v4, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v5, v3, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/books/widget/PagesViewController;->updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 1451
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->updatePageToViewMatrix()V

    .line 1453
    new-instance v0, Landroid/graphics/RectF;

    iget-object v4, v2, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    invoke-direct {v0, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1454
    .local v0, "bounds":Landroid/graphics/RectF;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1455
    iget-object v4, v2, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iget v6, v0, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    iget v7, v0, Landroid/graphics/RectF;->right:F

    float-to-int v7, v7

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public getBookmarkToggleTargetPage(Landroid/view/MotionEvent;)Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1916
    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getTouchedPageEvent(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/widget/TouchedPageEvent;

    move-result-object v0

    .line 1917
    .local v0, "touchEvent":Lcom/google/android/apps/books/widget/TouchedPageEvent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->inBookmarkZone(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1918
    iget-object v1, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 1920
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;)Ljava/util/List;
    .locals 3
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4764
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    sget-object v2, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/books/render/Renderer;->getCandidateAnnotationsForPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 6

    .prologue
    .line 1550
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    if-nez v5, :cond_0

    .line 1551
    const/4 v5, 0x0

    .line 1565
    :goto_0
    return-object v5

    .line 1555
    :cond_0
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    .line 1557
    .local v3, "screenPageOffset":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->isRightToLeft()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1558
    add-int/lit8 v3, v3, 0x1

    .line 1560
    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->readingOffsetFromScreenOffset(I)I

    move-result v2

    .line 1561
    .local v2, "readingPageOffset":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getPagesPerSpread()I

    move-result v1

    .line 1563
    .local v1, "pagesPerSpread":I
    invoke-static {v2, v1}, Lcom/google/android/apps/books/util/MathUtils;->divideRoundingDown(II)I

    move-result v4

    .line 1564
    .local v4, "spreadOffset":I
    new-instance v0, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    invoke-direct {v0, v5, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 1565
    .local v0, "nonNormalized":Lcom/google/android/apps/books/render/SpreadIdentifier;
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-static {v5, v0}, Lcom/google/android/apps/books/render/RendererUtils;->normalizeSpreadIdentifier(Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v5

    goto :goto_0
.end method

.method public getMarginNoteScreenRect(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/render/MarginNote;)Landroid/graphics/Rect;
    .locals 12
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "pagePosition"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .param p3, "marginNote"    # Lcom/google/android/apps/books/render/MarginNote;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2373
    iget-object v0, p3, Lcom/google/android/apps/books/render/MarginNote;->icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    if-nez v0, :cond_0

    .line 2374
    const/4 v0, 0x0

    .line 2389
    :goto_0
    return-object v0

    .line 2379
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->updateMinMarginNoteIconSize(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    .line 2380
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->isVertical()Z

    move-result v2

    .line 2382
    .local v2, "isVertical":Z
    iget-object v0, p3, Lcom/google/android/apps/books/render/MarginNote;->icon:Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;

    invoke-virtual {p2}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMinMarginNoteIconSize:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PagesViewController$MarginNoteIcon;->getPaintRect(Lcom/google/android/apps/books/widget/DevicePageRendering;ZLcom/google/android/apps/books/render/Renderer$SideOfSpine;Landroid/graphics/Point;Lcom/google/android/apps/books/render/Renderer;)Landroid/graphics/Rect;

    move-result-object v6

    .line 2385
    .local v6, "paintRect":Landroid/graphics/Rect;
    const/4 v0, 0x4

    new-array v7, v0, [F

    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    aput v0, v7, v8

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    aput v0, v7, v9

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    aput v0, v7, v10

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    aput v0, v7, v11

    .line 2387
    .local v7, "pointF":[F
    invoke-virtual {p0, v7, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->pointsToScreen([FLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 2389
    new-instance v0, Landroid/graphics/Rect;

    aget v1, v7, v8

    float-to-int v1, v1

    aget v3, v7, v9

    float-to-int v3, v3

    aget v4, v7, v10

    float-to-int v4, v4

    aget v5, v7, v11

    float-to-int v5, v5

    invoke-direct {v0, v1, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public getPageFlipIncrementValue(Lcom/google/android/apps/books/util/ScreenDirection;)I
    .locals 2
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 2924
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    .line 2925
    .local v0, "increment":I
    :goto_0
    invoke-static {p1, v0}, Lcom/google/android/apps/books/util/ScreenDirection;->dotProduct(Lcom/google/android/apps/books/util/ScreenDirection;I)I

    move-result v1

    return v1

    .line 2924
    .end local v0    # "increment":I
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 1420
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageOffsetToFullSizeInfo:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;

    .line 1421
    .local v0, "info":Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;
    if-eqz v0, :cond_0

    .line 1422
    iget-object v1, v0, Lcom/google/android/apps/books/widget/PagesViewController$RenderedPageInfo;->rendering:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 1424
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSelectionBounds()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->getSelectionBounds()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;
    .locals 2

    .prologue
    .line 1575
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingNavigationView()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1576
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/BookNavView;->getStableSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 1577
    .local v0, "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v0, :cond_0

    .line 1578
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-static {v1, v0}, Lcom/google/android/apps/books/render/RendererUtils;->normalizeSpreadIdentifier(Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    .line 1582
    .end local v0    # "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_0
    return-object v1

    .line 1580
    .restart local v0    # "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1582
    .end local v0    # "result":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v1

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 4291
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public handleTapAsVolumeAnnotationSelection(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z
    .locals 6
    .param p1, "touchEvent"    # Lcom/google/android/apps/books/widget/TouchedPageEvent;

    .prologue
    const/4 v3, 0x0

    .line 2170
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getVolumeAnnotationRect(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Lcom/google/android/apps/books/render/LabeledRect;

    move-result-object v1

    .line 2172
    .local v1, "annotationRect":Lcom/google/android/apps/books/render/LabeledRect;
    if-nez v1, :cond_1

    .line 2184
    :cond_0
    :goto_0
    return v3

    .line 2176
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v5, p1, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getAnnotationsForPassage(I)Ljava/util/Iterator;

    move-result-object v2

    .line 2178
    .local v2, "annotations":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v4, v1, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->getAnnotationWithId(Ljava/util/Iterator;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 2179
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_0

    .line 2180
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;->GEO_ANNOTATION_TAPPED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logGeoAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$GeoAction;)V

    .line 2181
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v3, v0}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->showCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 2182
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public hideEndOfBook()V
    .locals 3

    .prologue
    .line 1282
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getEndOfBookView()Landroid/view/View;

    move-result-object v1

    .line 1283
    .local v1, "eobView":Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/BookView;->getView()Landroid/view/View;

    move-result-object v0

    .line 1284
    .local v0, "bookView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1285
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1286
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mShowingEndOfBookPage:Z

    .line 1287
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateEobStatus()V

    .line 1288
    return-void
.end method

.method public highlightSpokenText(ILcom/google/android/apps/books/annotations/TextLocationRange;)V
    .locals 4
    .param p1, "passageIndex"    # I
    .param p2, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;

    .prologue
    const/4 v3, 0x3

    .line 1631
    const-string v0, "missing highlightRange"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1633
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightRange:Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 1634
    iput p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReadableItemPassageIndex:I

    .line 1635
    const-string v0, "PagesViewHelper"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1636
    const-string v0, "PagesViewHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set TTS item to passage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", range="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1640
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->loadNonSelectionRangeData(ILcom/google/android/apps/books/annotations/TextLocationRange;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightBoundsRequestId:I

    .line 1641
    const-string v0, "PagesViewHelper"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1642
    const-string v0, "PagesViewHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Issued request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTtsHighlightBoundsRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for item rectangles"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    :cond_1
    return-void
.end method

.method public isTextSelected()Z
    .locals 1

    .prologue
    .line 1965
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTurning()Z
    .locals 1

    .prologue
    .line 4346
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->isTurning()Z

    move-result v0

    return v0
.end method

.method public loadNewNoteRects()V
    .locals 1

    .prologue
    .line 4396
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->clearRequestsAndCacheIndices()V

    .line 4397
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeLoadNearbyAnnotationRects()V

    .line 4398
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->canImmediatelyRedrawHighlights()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4399
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 4401
    :cond_0
    return-void
.end method

.method public lockSelection()V
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-eqz v0, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->lockSelection()V

    .line 1070
    :cond_0
    return-void
.end method

.method public makePageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 4
    .param p1, "screenOffset"    # I

    .prologue
    .line 1528
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v2

    .line 1530
    .local v2, "samePage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v2, :cond_0

    .line 1531
    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    .line 1540
    :goto_0
    return-object v3

    .line 1534
    :cond_0
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 1535
    .local v0, "centerPage":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v0, :cond_1

    .line 1536
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    sub-int v3, p1, v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->readingOffsetFromScreenOffset(I)I

    move-result v1

    .line 1538
    .local v1, "readingOffset":I
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->offsetBy(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    goto :goto_0

    .line 1540
    .end local v1    # "readingOffset":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public makeRenderPosition(I)Lcom/google/android/apps/books/render/RenderPosition;
    .locals 6
    .param p1, "screenOffset"    # I

    .prologue
    .line 1476
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->makePageIdentifier(I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v0

    .line 1477
    .local v0, "page":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v0, :cond_0

    .line 1478
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v2

    .line 1486
    :goto_0
    return-object v2

    .line 1480
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    if-nez v2, :cond_1

    .line 1481
    const/4 v2, 0x0

    goto :goto_0

    .line 1483
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadPageIndices:[I

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getSpreadPageIdentifierIndices(I[I)[I

    .line 1484
    new-instance v1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    new-instance v2, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadPageIndices:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mTempSpreadPageIndices:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/render/SpreadPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;I)V

    .line 1486
    .local v1, "spread":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v2

    goto :goto_0
.end method

.method public maybeDismissAnnotationSelection()V
    .locals 2

    .prologue
    .line 4460
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationPassageIndex:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->setSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;I)V

    .line 4461
    return-void
.end method

.method public maybeUpdateAccessiblePages(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 6
    .param p1, "uiMode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2891
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

    if-eqz v5, :cond_0

    .line 2892
    sget-object v5, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne p1, v5, :cond_1

    move v1, v3

    .line 2893
    .local v1, "inFullMode":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingEndOfBookPage()Z

    move-result v0

    .line 2894
    .local v0, "eobVisible":Z
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v5}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->isAnyAudioPlaying()Z

    move-result v2

    .line 2895
    .local v2, "isPlayingAudio":Z
    iget-object v5, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionAutomator:Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    if-nez v2, :cond_2

    :goto_1
    invoke-interface {v5, v3}, Lcom/google/android/apps/books/widget/PagesViewController$AccessiblePages;->setImportantForAccessibility(Z)V

    .line 2898
    .end local v0    # "eobVisible":Z
    .end local v1    # "inFullMode":Z
    .end local v2    # "isPlayingAudio":Z
    :cond_0
    return-void

    :cond_1
    move v1, v4

    .line 2892
    goto :goto_0

    .restart local v0    # "eobVisible":Z
    .restart local v1    # "inFullMode":Z
    .restart local v2    # "isPlayingAudio":Z
    :cond_2
    move v3, v4

    .line 2895
    goto :goto_1
.end method

.method public moveToNextMatch()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 4746
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getStableSkimViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 4749
    .local v0, "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v0, :cond_0

    .line 4750
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->moveToPrevOrNextMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    .line 4752
    :cond_0
    return-void
.end method

.method public moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;)V
    .locals 7
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "savePosition"    # Z
    .param p3, "action"    # Lcom/google/android/apps/books/app/MoveType;
    .param p4, "highlight"    # Ljava/lang/String;

    .prologue
    .line 1169
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V

    .line 1170
    return-void
.end method

.method public moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V
    .locals 11
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "savePosition"    # Z
    .param p3, "action"    # Lcom/google/android/apps/books/app/MoveType;
    .param p4, "highlight"    # Ljava/lang/String;
    .param p5, "uiMode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .param p6, "animate"    # Z

    .prologue
    .line 1179
    invoke-static {p3}, Lcom/google/android/apps/books/app/MoveType;->nonNull(Lcom/google/android/apps/books/app/MoveType;)Lcom/google/android/apps/books/app/MoveType;

    move-result-object p3

    .line 1181
    const-string v6, "PagesViewHelper"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    .line 1182
    .local v2, "isLoggable":Z
    if-eqz v2, :cond_0

    .line 1183
    const-string v6, "PagesViewHelper"

    const-string v7, "moveToPosition(%s, fromUser=%b, ...)"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1186
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V

    .line 1188
    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    .line 1190
    if-eqz p5, :cond_3

    move-object/from16 v4, p5

    .line 1192
    .local v4, "newUiMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    :goto_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateAccessiblePages(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 1194
    sget-object v6, Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;->FULL:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    if-ne v4, v6, :cond_7

    .line 1198
    if-eqz p1, :cond_4

    .line 1199
    move-object v3, p1

    .line 1207
    .local v3, "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_1
    if-eqz v3, :cond_1

    .line 1208
    iget v6, v3, Lcom/google/android/apps/books/render/SpreadIdentifier;->spreadOffset:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/widget/PagesViewController;->screenOffsetFromReadingOffset(I)I

    move-result v5

    .line 1210
    .local v5, "screenDirectionSpreadOffset":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v6

    if-eqz v6, :cond_6

    const/4 v6, 0x2

    :goto_2
    mul-int/2addr v6, v5

    iput v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    .line 1212
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastSearchBarUpdateSpreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 1213
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetOfPageAfterVolumeEnd:Ljava/lang/Integer;

    .line 1214
    iget-object v6, v3, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    iput-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBasePosition:Lcom/google/android/apps/books/common/Position;

    .line 1215
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastCenterPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 1216
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSaveLastMove:Z

    .line 1217
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastAction:Lcom/google/android/apps/books/app/MoveType;

    .line 1219
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->invalidateState()V

    .line 1226
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->clearRequests()V

    .line 1227
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->clearRequests()V

    .line 1228
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->clearRequests()V

    .line 1230
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageTurnController:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/PageTurnController;->clearPendingOperations()V

    .line 1234
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showBookView()V

    .line 1237
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/BookView;->onBasePositionChanged()V

    .line 1238
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PagesView;->onBasePositionChanged()V

    .line 1240
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onOffsetChanged()V

    .line 1243
    .end local v5    # "screenDirectionSpreadOffset":I
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingNavigationView()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1248
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->resetZoom()V

    .line 1250
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v6

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Lcom/google/android/apps/books/widget/BookView;->hideNavView(Z)V

    .line 1270
    :cond_2
    :goto_3
    return-void

    .line 1190
    .end local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v4    # "newUiMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v6}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v4

    goto :goto_0

    .line 1200
    .restart local v4    # "newUiMode":Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingNavigationView()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1201
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/navigation/BookNavView;->getStableSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    .restart local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    goto :goto_1

    .line 1203
    .end local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_5
    const/4 v3, 0x0

    .restart local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    goto :goto_1

    .line 1210
    .restart local v5    # "screenDirectionSpreadOffset":I
    :cond_6
    const/4 v6, 0x1

    goto :goto_2

    .line 1256
    .end local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v5    # "screenDirectionSpreadOffset":I
    :cond_7
    if-eqz p1, :cond_9

    move-object v3, p1

    .line 1259
    .restart local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_4
    iget-boolean v6, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPreparedNavView:Z

    if-eqz v6, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->showingNavigationView()Z

    move-result v6

    if-nez v6, :cond_a

    .line 1260
    :cond_8
    invoke-direct {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->showSkimView(Lcom/google/android/apps/books/render/SpreadIdentifier;)V

    .line 1261
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v1

    .line 1262
    .local v1, "bookView":Lcom/google/android/apps/books/widget/BookView;
    if-eqz p6, :cond_2

    .line 1263
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookView;->scaleSkimToFull()V

    .line 1264
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BookView;->animateSkimFromFull()V

    goto :goto_3

    .line 1256
    .end local v1    # "bookView":Lcom/google/android/apps/books/widget/BookView;
    .end local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getFullViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    goto :goto_4

    .line 1267
    .restart local v3    # "newSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v6

    invoke-virtual {p3}, Lcom/google/android/apps/books/app/MoveType;->isScrubberMove()Z

    move-result v7

    invoke-virtual {v6, v3, v7}, Lcom/google/android/apps/books/navigation/BookNavView;->scrollToLocation(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    goto :goto_3
.end method

.method public moveToPreviousMatch()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 4755
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getStableSkimViewSpreadIdentifier()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 4758
    .local v0, "currentSpread":Lcom/google/android/apps/books/render/SpreadIdentifier;
    if-eqz v0, :cond_0

    .line 4759
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->moveToPrevOrNextMatch(Lcom/google/android/apps/books/render/SpreadIdentifier;Z)V

    .line 4761
    :cond_0
    return-void
.end method

.method public moveToSearchResult(Lcom/google/android/apps/books/annotations/TextLocation;I)V
    .locals 3
    .param p1, "matchLocation"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 4737
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    new-instance v1, Lcom/google/android/apps/books/render/PendingSearchMatchData;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lcom/google/android/apps/books/render/PendingSearchMatchData;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;IZ)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->setPendingSearchMatch(Lcom/google/android/apps/books/render/PendingSearchMatchData;)V

    .line 4739
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSearchMatchRectsCache:Lcom/google/android/apps/books/widget/SearchMatchRectsCache;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache;->maybeMoveToPendingMatch()V

    .line 4742
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateSearchBarNavigationDelayed()V

    .line 4743
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2616
    iget v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getTouchedPageEvent(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/widget/TouchedPageEvent;

    move-result-object v0

    .line 2619
    .local v0, "touchEvent":Lcom/google/android/apps/books/widget/TouchedPageEvent;
    iget-object v1, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-nez v1, :cond_0

    .line 2638
    :goto_0
    return-void

    .line 2623
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    if-eqz v1, :cond_1

    .line 2625
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    .line 2626
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V

    .line 2630
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v2, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 2632
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    iget-object v2, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchLocation:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    iget-object v4, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    iget-object v5, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->pagePositionOnScreen:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/render/Renderer;->beginSelection(FFLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 4299
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView;->onPause()V

    .line 4302
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->endTurn(Z)Z

    .line 4303
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 4306
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PagesView;->onResume()V

    .line 4307
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 2588
    iget v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getTouchedPageEvent(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/widget/TouchedPageEvent;

    move-result-object v0

    .line 2589
    .local v0, "touchEvent":Lcom/google/android/apps/books/widget/TouchedPageEvent;
    iget-object v2, v0, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-eqz v2, :cond_2

    .line 2590
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->handleTapAsAnnotationSelection(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2610
    :cond_0
    :goto_0
    return v1

    .line 2593
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->handleTapAsVolumeAnnotationSelection(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2602
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->handleTapAsBookmarkToggle(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2606
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-eqz v2, :cond_3

    .line 2607
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeDismissSelectionOverlay()V

    goto :goto_0

    .line 2610
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/apps/books/app/SelectionState;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "selectionState"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1924
    iget v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mOffsetFromBasePosition:I

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->getTouchedPageEvent(Landroid/view/MotionEvent;I)Lcom/google/android/apps/books/widget/TouchedPageEvent;

    move-result-object v2

    .line 1926
    .local v2, "touchEvent":Lcom/google/android/apps/books/widget/TouchedPageEvent;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1927
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 1947
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-nez v3, :cond_2

    .line 1961
    :cond_1
    :goto_1
    return v1

    .line 1929
    :pswitch_1
    iput-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    .line 1930
    iget-object v3, v2, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-eqz v3, :cond_0

    .line 1931
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->getVolumeAnnotationRect(Lcom/google/android/apps/books/widget/TouchedPageEvent;)Lcom/google/android/apps/books/render/LabeledRect;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    .line 1933
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    if-eqz v3, :cond_0

    .line 1934
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V

    goto :goto_0

    .line 1940
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    if-eqz v3, :cond_0

    .line 1941
    iput-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightedVolumeAnnotationRect:Lcom/google/android/apps/books/render/LabeledRect;

    .line 1942
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V

    goto :goto_0

    .line 1951
    :cond_2
    iget-object v3, v2, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    if-eqz v3, :cond_1

    .line 1952
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    iget-object v4, v2, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPassageIndex()I

    move-result v4

    iget-object v5, v2, Lcom/google/android/apps/books/widget/TouchedPageEvent;->touchedPage:Lcom/google/android/apps/books/widget/DevicePageRendering;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIndex()I

    move-result v5

    invoke-virtual {v3, p1, v4, v5, p2}, Lcom/google/android/apps/books/widget/SelectionOverlay;->dragSelection(Landroid/view/MotionEvent;IILcom/google/android/apps/books/app/SelectionState;)Z

    move-result v1

    .line 1956
    .local v1, "selectionDrag":Z
    if-eqz v1, :cond_1

    .line 1957
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v3}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->onSelectionUpdate()V

    goto :goto_1

    .line 1927
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public pageToViewMatrixMapRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "boundsF"    # Landroid/graphics/RectF;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 190
    return-void
.end method

.method public pinchToSkim(F)V
    .locals 1
    .param p1, "detectorScale"    # F

    .prologue
    .line 4851
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/BookView;->pinchToSkim(F)V

    .line 4852
    return-void
.end method

.method public pointsToScreen([FLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 5
    .param p1, "pointF"    # [F
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p3, "pagePosition"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 1393
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/books/widget/PagesViewController;->updateRendererToViewTransform(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 1394
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererToViewTransform:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1395
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransform:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1397
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 1398
    .local v1, "viewLoc":[I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1400
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 1401
    aget v2, p1, v0

    const/4 v3, 0x0

    aget v3, v1, v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    aput v2, p1, v0

    .line 1402
    add-int/lit8 v2, v0, 0x1

    aget v3, p1, v2

    const/4 v4, 0x1

    aget v4, v1, v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    aput v3, p1, v2

    .line 1400
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 1404
    :cond_0
    return-void
.end method

.method protected refreshPageAtOffset(I)V
    .locals 3
    .param p1, "offset"    # I

    .prologue
    const/4 v2, 0x0

    .line 1894
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 1895
    .local v0, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v0, :cond_0

    .line 1896
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->makeRenderPosition(Lcom/google/android/apps/books/widget/DevicePageRendering;)Lcom/google/android/apps/books/render/RenderPosition;

    move-result-object v1

    invoke-virtual {p0, v1, p1, v2, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;IZZ)V

    .line 1898
    :cond_0
    return-void
.end method

.method public refreshRenderedPages()V
    .locals 5

    .prologue
    .line 1879
    iget-boolean v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z

    if-eqz v4, :cond_0

    .line 1888
    :goto_0
    return-void

    .line 1883
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getNearbyPageOffsets()[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget v3, v0, v1

    .line 1884
    .local v3, "offset":I
    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshPageAtOffset(I)V

    .line 1883
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1887
    .end local v3    # "offset":I
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v4}, Lcom/google/android/apps/books/widget/BookView;->getNavView()Lcom/google/android/apps/books/navigation/BookNavView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/books/navigation/BookNavView;->redraw()V

    goto :goto_0
.end method

.method public requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;IZZ)V
    .locals 5
    .param p1, "position"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "targetOffset"    # I
    .param p3, "setPageLoading"    # Z
    .param p4, "abortIfPending"    # Z

    .prologue
    .line 2691
    const-string v2, "PagesViewHelper"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2692
    const-string v2, "PagesViewHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestRenderPage("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2695
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCustomOffsets:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, p2}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 2696
    .local v1, "specialPage":Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    if-eqz v1, :cond_3

    .line 2697
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/PagesViewController;->getSpecialPageBitmap(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2698
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 2699
    invoke-direct {p0, v1, p2, v0}, Lcom/google/android/apps/books/widget/PagesViewController;->setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;ILandroid/graphics/Bitmap;)V

    .line 2715
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-void

    .line 2701
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    if-eqz p3, :cond_1

    .line 2702
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->setPageLoading(I)V

    goto :goto_0

    .line 2708
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    if-eqz p3, :cond_4

    .line 2709
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->setPageLoading(I)V

    .line 2711
    :cond_4
    if-eqz p4, :cond_5

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/widget/PagesViewController;->isPendingPageRequest(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2712
    :cond_5
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->requestPage(Ljava/lang/Integer;Lcom/google/android/apps/books/render/RenderPosition;)V

    goto :goto_0
.end method

.method public scaleAndScroll(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "scrollX"    # F
    .param p3, "scrollY"    # F

    .prologue
    .line 4311
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransformInverse:Landroid/graphics/Matrix;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/util/MathUtils;->setScaleAndScrollInverseMatrix(Landroid/graphics/Matrix;FFF)V

    .line 4313
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransform:Landroid/graphics/Matrix;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/books/util/MathUtils;->setScaleAndScrollMatrix(Landroid/graphics/Matrix;FFF)V

    .line 4315
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/BookView;->setZoom(FFF)V

    .line 4320
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->updatePageToViewMatrix()V

    .line 4321
    return-void
.end method

.method public scaleSkimToFull()V
    .locals 1

    .prologue
    .line 4866
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->getBookView()Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->scaleSkimToFull()V

    .line 4867
    return-void
.end method

.method public screenOffsetFromReadingOffset(I)I
    .locals 1
    .param p1, "readingOffset"    # I

    .prologue
    .line 1607
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mWritingDirection:Lcom/google/android/apps/books/util/WritingDirection;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/util/WritingDirection;->dotProduct(Lcom/google/android/apps/books/util/WritingDirection;I)I

    move-result v0

    return v0
.end method

.method public setBookmarkController(Lcom/google/android/apps/books/app/BookmarkController;)V
    .locals 1
    .param p1, "controller"    # Lcom/google/android/apps/books/app/BookmarkController;

    .prologue
    .line 2545
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->removeCurrentBookmarkListener()V

    .line 2546
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkListener:Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/app/BookmarkController;->addBookmarkListener(Lcom/google/android/apps/books/app/BookmarkController$BookmarkListener;)V

    .line 2547
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookmarkController:Lcom/google/android/apps/books/app/BookmarkController;

    .line 2548
    return-void
.end method

.method public setDrawAnnotations(Z)V
    .locals 1
    .param p1, "drawAnnotations"    # Z

    .prologue
    .line 2534
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDrawAnnotations:Z

    if-eq v0, p1, :cond_0

    .line 2535
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mDrawAnnotations:Z

    .line 2536
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 2538
    :cond_0
    return-void
.end method

.method public setExecutingInitialLoadTransition(Z)V
    .locals 2
    .param p1, "executing"    # Z

    .prologue
    .line 3574
    iput-boolean p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mExecutingInitialLoadTransition:Z

    .line 3577
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    instance-of v0, v0, Lcom/google/android/apps/books/render/ImageModeRenderer;

    if-nez v0, :cond_0

    .line 3579
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    .line 3580
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->displayTwoPages()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransShouldRenderSlotNeg1:Z

    .line 3584
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3587
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPendingPages:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 3588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mInitialTransitionDelayedPages:Ljava/util/Map;

    .line 3589
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    .line 3591
    :cond_1
    return-void
.end method

.method public setSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;I)V
    .locals 5
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "passageIndex"    # I

    .prologue
    .line 2216
    if-nez p1, :cond_2

    const/4 v1, 0x0

    .line 2217
    .local v1, "localId":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2218
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    .line 2220
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 2221
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationPassageIndex:I

    .line 2226
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mHighlightsRectsCache:Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    invoke-virtual {v3, p2}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;->getCachedValue(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/PassageHighlightRects;

    .line 2229
    .local v2, "passageRects":Lcom/google/android/apps/books/widget/PassageHighlightRects;
    if-eqz v2, :cond_0

    .line 2230
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    sget-object v4, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    invoke-interface {v3, p2, v4}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getPaintableAnnotations(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 2232
    .local v0, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationId:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/books/widget/PassageHighlightRects;->updateHighlightsAndSelections(Ljava/util/List;Ljava/lang/String;)V

    .line 2236
    .end local v0    # "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->onNonTurningDecorationsChanged()V

    .line 2240
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->refreshRenderedPages()V

    .line 2242
    .end local v2    # "passageRects":Lcom/google/android/apps/books/widget/PassageHighlightRects;
    :cond_1
    return-void

    .line 2216
    .end local v1    # "localId":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2223
    .restart local v1    # "localId":Ljava/lang/String;
    :cond_3
    iput p2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectedAnnotationPassageIndex:I

    goto :goto_1
.end method

.method public setSpecialPageBitmaps(Lcom/google/common/collect/ImmutableMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableMap",
            "<",
            "Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1783
    .local p1, "bitmaps":Lcom/google/common/collect/ImmutableMap;, "Lcom/google/common/collect/ImmutableMap<Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    .line 1784
    return-void
.end method

.method public setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V
    .locals 7
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .param p2, "animate"    # Z

    .prologue
    .line 4859
    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/books/app/MoveType;->UNSPECIFIED:Lcom/google/android/apps/books/app/MoveType;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mLastHighlightParams:Ljava/lang/String;

    move-object v0, p0

    move-object v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/books/widget/PagesViewController;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;Ljava/lang/String;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;Z)V

    .line 4860
    return-void
.end method

.method public showEndOfBook()V
    .locals 3

    .prologue
    .line 1273
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getEndOfBookView()Landroid/view/View;

    move-result-object v1

    .line 1274
    .local v1, "eobView":Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/BookView;->getView()Landroid/view/View;

    move-result-object v0

    .line 1275
    .local v0, "bookView":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1276
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 1277
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mShowingEndOfBookPage:Z

    .line 1278
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeUpdateEobStatus()V

    .line 1279
    return-void
.end method

.method public showingEndOfBookPage()Z
    .locals 1

    .prologue
    .line 2935
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mShowingEndOfBookPage:Z

    return v0
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2560
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mStopped:Z

    .line 2562
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    if-eqz v0, :cond_0

    .line 2563
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/BookView;->clear()V

    .line 2564
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mBookView:Lcom/google/android/apps/books/widget/BookView;

    .line 2569
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->removeCurrentBookmarkListener()V

    .line 2570
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mProcessPageScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->stop()V

    .line 2571
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->freeMemory()V

    .line 2573
    iput-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSpecialPageBitmaps:Lcom/google/common/collect/ImmutableMap;

    .line 2574
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mCachedRenderResults:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 2575
    return-void
.end method

.method public updateMinMarginNoteIconSize(Lcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 6
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 2453
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    sget-object v3, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererTransform:Landroid/graphics/Matrix;

    invoke-interface {v2, p1, v3, v4}, Lcom/google/android/apps/books/render/Renderer;->getPageToViewMatrix(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Matrix;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2458
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageCanvasPainter:Lcom/google/android/apps/books/render/PageCanvasPainter;

    invoke-interface {v2}, Lcom/google/android/apps/books/render/PageCanvasPainter;->minimumMarginNoteIconSize()Landroid/graphics/Point;

    move-result-object v0

    .line 2459
    .local v0, "minSize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererTransform:Landroid/graphics/Matrix;

    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v2

    div-float v1, v5, v2

    .line 2460
    .local v1, "scale":F
    iget-object v2, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mMinMarginNoteIconSize:Landroid/graphics/Point;

    iget v3, v0, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    mul-float/2addr v4, v1

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 2462
    .end local v0    # "minSize":Landroid/graphics/Point;
    .end local v1    # "scale":F
    :cond_0
    return-void
.end method

.method public updatePageToViewMatrix()V
    .locals 2

    .prologue
    .line 4324
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToViewMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mRendererToViewTransform:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 4325
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToViewMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageZoomTransform:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 4327
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    if-eqz v0, :cond_0

    .line 4328
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setPageToViewMatrix(Landroid/graphics/Matrix;)V

    .line 4329
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mSelectionOverlay:Lcom/google/android/apps/books/widget/SelectionOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->invalidate()V

    .line 4331
    :cond_0
    return-void
.end method

.method public volumeAnnotationsLoaded(ILjava/util/List;)V
    .locals 1
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4412
    .local p2, "annotations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PagesViewController;->mVolumeAnnotationRectsCache:Lcom/google/android/apps/books/widget/VolumeRectsCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->clearRangeDataRequestAndMarkIncomplete(I)V

    .line 4413
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/PagesViewController;->maybeLoadNearbyVolumeAnnotationRects()V

    .line 4414
    return-void
.end method
