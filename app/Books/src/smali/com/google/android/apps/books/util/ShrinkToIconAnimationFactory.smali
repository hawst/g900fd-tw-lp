.class public Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory;
.super Ljava/lang/Object;
.source "ShrinkToIconAnimationFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$1;,
        Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$CubicDecelerateInterpolator;
    }
.end annotation


# direct methods
.method public static create(Landroid/view/View;Landroid/view/View;Landroid/graphics/Rect;Z)Landroid/view/animation/AnimationSet;
    .locals 29
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visibleSubview"    # Landroid/view/View;
    .param p2, "iconRect"    # Landroid/graphics/Rect;
    .param p3, "toExpand"    # Z

    .prologue
    .line 42
    if-eqz p1, :cond_0

    .line 43
    invoke-static {}, Lcom/google/android/ublib/view/TranslationHelper;->get()Lcom/google/android/ublib/view/TranslationHelper;

    move-result-object v26

    .line 44
    .local v26, "translationHelper":Lcom/google/android/ublib/view/TranslationHelper;
    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/view/TranslationHelper;->getTranslationX(Landroid/view/View;)F

    move-result v27

    .line 45
    .local v27, "translationX":F
    const/4 v6, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/google/android/ublib/view/TranslationHelper;->setTranslationX(Landroid/view/View;F)V

    .line 50
    .end local v26    # "translationHelper":Lcom/google/android/ublib/view/TranslationHelper;
    :goto_0
    const/4 v6, 0x2

    new-array v0, v6, [I

    move-object/from16 v28, v0

    .line 51
    .local v28, "viewLoc":[I
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 52
    new-instance v22, Landroid/graphics/Rect;

    const/4 v6, 0x0

    aget v6, v28, v6

    const/4 v7, 0x1

    aget v7, v28, v7

    const/4 v8, 0x0

    aget v8, v28, v8

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v28, v9

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    move-object/from16 v0, v22

    invoke-direct {v0, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 57
    .local v22, "rect":Landroid/graphics/Rect;
    if-nez p1, :cond_1

    .line 58
    const/16 v23, 0x0

    .local v23, "rightMargin":I
    move/from16 v19, v23

    .line 67
    .local v19, "leftMargin":I
    :goto_1
    const v6, 0x3f8ccccd    # 1.1f

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->max(FF)F

    move-result v7

    mul-float v5, v6, v7

    .line 72
    .local v5, "scale":F
    const v6, 0x3f7d70a4    # 0.99f

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 76
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x40800000    # 4.0f

    div-float v20, v6, v7

    .line 78
    .local v20, "overlapX":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->centerX()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 79
    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    sub-float v6, v6, v20

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    move/from16 v0, v19

    int-to-float v7, v0

    mul-float/2addr v7, v5

    sub-float v17, v6, v7

    .line 88
    .local v17, "deltaX":F
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v0, v6

    move/from16 v18, v0

    .line 89
    .local v18, "deltaY":F
    const/high16 v21, 0x3f000000    # 0.5f

    .line 91
    .local v21, "pivotY":F
    new-instance v16, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 98
    .local v16, "animationSet":Landroid/view/animation/AnimationSet;
    if-eqz p3, :cond_3

    .line 99
    new-instance v4, Landroid/view/animation/ScaleAnimation;

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    sub-float v7, v17, v27

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v5

    div-float v10, v7, v10

    const/4 v11, 0x1

    const/high16 v12, 0x3f000000    # 0.5f

    move v7, v5

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 102
    .local v4, "scaleAnimation":Landroid/view/animation/ScaleAnimation;
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v6}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 103
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 105
    new-instance v25, Landroid/view/animation/TranslateAnimation;

    const/4 v6, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v27

    move/from16 v2, v27

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 107
    .local v25, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    new-instance v6, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 108
    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 110
    new-instance v15, Landroid/view/animation/AlphaAnimation;

    const v6, 0x3e99999a    # 0.3f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v15, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 111
    .local v15, "alpha":Landroid/view/animation/AlphaAnimation;
    new-instance v6, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v15, v6}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 112
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 133
    :goto_3
    return-object v16

    .line 47
    .end local v4    # "scaleAnimation":Landroid/view/animation/ScaleAnimation;
    .end local v5    # "scale":F
    .end local v15    # "alpha":Landroid/view/animation/AlphaAnimation;
    .end local v16    # "animationSet":Landroid/view/animation/AnimationSet;
    .end local v17    # "deltaX":F
    .end local v18    # "deltaY":F
    .end local v19    # "leftMargin":I
    .end local v20    # "overlapX":F
    .end local v21    # "pivotY":F
    .end local v22    # "rect":Landroid/graphics/Rect;
    .end local v23    # "rightMargin":I
    .end local v25    # "translateAnimation":Landroid/view/animation/TranslateAnimation;
    .end local v27    # "translationX":F
    .end local v28    # "viewLoc":[I
    :cond_0
    const/16 v27, 0x0

    .restart local v27    # "translationX":F
    goto/16 :goto_0

    .line 60
    .restart local v22    # "rect":Landroid/graphics/Rect;
    .restart local v28    # "viewLoc":[I
    :cond_1
    const/4 v6, 0x2

    new-array v0, v6, [I

    move-object/from16 v24, v0

    .line 61
    .local v24, "subviewLoc":[I
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 62
    const/4 v6, 0x0

    aget v6, v24, v6

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Rect;->left:I

    sub-int v19, v6, v7

    .line 63
    .restart local v19    # "leftMargin":I
    move-object/from16 v0, v22

    iget v6, v0, Landroid/graphics/Rect;->right:I

    const/4 v7, 0x0

    aget v7, v24, v7

    sub-int/2addr v6, v7

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v7

    sub-int v23, v6, v7

    .restart local v23    # "rightMargin":I
    goto/16 :goto_1

    .line 81
    .end local v24    # "subviewLoc":[I
    .restart local v5    # "scale":F
    .restart local v20    # "overlapX":F
    :cond_2
    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    add-float v6, v6, v20

    move-object/from16 v0, v22

    iget v7, v0, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    invoke-virtual/range {v22 .. v22}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int v8, v8, v23

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    sub-float v17, v6, v7

    .restart local v17    # "deltaX":F
    goto/16 :goto_2

    .line 114
    .restart local v16    # "animationSet":Landroid/view/animation/AnimationSet;
    .restart local v18    # "deltaY":F
    .restart local v21    # "pivotY":F
    :cond_3
    new-instance v4, Landroid/view/animation/ScaleAnimation;

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    sub-float v6, v17, v27

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v8, v5

    div-float v12, v6, v8

    const/4 v13, 0x1

    const/high16 v14, 0x3f000000    # 0.5f

    move-object v6, v4

    move v8, v5

    move v10, v5

    invoke-direct/range {v6 .. v14}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 118
    .restart local v4    # "scaleAnimation":Landroid/view/animation/ScaleAnimation;
    new-instance v6, Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$CubicDecelerateInterpolator;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$CubicDecelerateInterpolator;-><init>(Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory$1;)V

    invoke-virtual {v4, v6}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 119
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 121
    new-instance v25, Landroid/view/animation/TranslateAnimation;

    const/4 v6, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v27

    move/from16 v2, v27

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v6, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 123
    .restart local v25    # "translateAnimation":Landroid/view/animation/TranslateAnimation;
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 124
    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 126
    new-instance v15, Landroid/view/animation/AlphaAnimation;

    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3f19999a    # 0.6f

    invoke-direct {v15, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 127
    .restart local v15    # "alpha":Landroid/view/animation/AlphaAnimation;
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v15, v6}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 128
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 130
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto/16 :goto_3
.end method
