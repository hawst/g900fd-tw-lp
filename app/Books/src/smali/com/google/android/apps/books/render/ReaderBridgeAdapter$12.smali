.class Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;
.super Ljava/lang/Object;
.source "ReaderBridgeAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->onPassageTextReady(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

.field final synthetic val$finalAltMap:Lcom/google/android/apps/books/model/LabelMap;

.field final synthetic val$finalPositions:Lcom/google/android/apps/books/model/LabelMap;

.field final synthetic val$passageIndex:I

.field final synthetic val$requestId:I

.field final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    iput p2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$requestId:I

    iput p3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$passageIndex:I

    iput-object p4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$text:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$finalPositions:Lcom/google/android/apps/books/model/LabelMap;

    iput-object p6, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$finalAltMap:Lcom/google/android/apps/books/model/LabelMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->this$0:Lcom/google/android/apps/books/render/ReaderBridgeAdapter;

    # getter for: Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->mListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/ReaderBridgeAdapter;->access$000(Lcom/google/android/apps/books/render/ReaderBridgeAdapter;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$requestId:I

    iget v2, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$passageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$text:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$finalPositions:Lcom/google/android/apps/books/model/LabelMap;

    iget-object v5, p0, Lcom/google/android/apps/books/render/ReaderBridgeAdapter$12;->val$finalAltMap:Lcom/google/android/apps/books/model/LabelMap;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/ReaderListener;->onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 249
    return-void
.end method
