.class final Lcom/google/android/apps/books/app/HomeFragment$30;
.super Lcom/google/android/apps/books/util/LeakSafeCallback;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->makeRentalBadgesHandler(Lcom/google/android/apps/books/app/HomeFragment;)Landroid/os/Handler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/util/LeakSafeCallback",
        "<",
        "Lcom/google/android/apps/books/app/HomeFragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$fragment:Lcom/google/android/apps/books/app/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/app/HomeFragment;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/HomeFragment;

    .prologue
    .line 2736
    iput-object p2, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/LeakSafeCallback;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected handleMessage(Lcom/google/android/apps/books/app/HomeFragment;Landroid/os/Message;)Z
    .locals 1
    .param p1, "target"    # Lcom/google/android/apps/books/app/HomeFragment;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 2739
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/HomeFragment;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2740
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2741
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mReadNowHomeView:Lcom/google/android/apps/books/widget/ReadNowHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$2300(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/ReadNowHomeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->updateRentalBadges()V

    .line 2743
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2744
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mMyLibraryHomeView:Lcom/google/android/apps/books/widget/MyLibraryHomeView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$1000(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->updateRentalBadges()V

    .line 2746
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$30;->val$fragment:Lcom/google/android/apps/books/app/HomeFragment;

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->maybeQueueRentalBadgesUpdate()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$4700(Lcom/google/android/apps/books/app/HomeFragment;)V

    .line 2748
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic handleMessage(Ljava/lang/Object;Landroid/os/Message;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Landroid/os/Message;

    .prologue
    .line 2736
    check-cast p1, Lcom/google/android/apps/books/app/HomeFragment;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/HomeFragment$30;->handleMessage(Lcom/google/android/apps/books/app/HomeFragment;Landroid/os/Message;)Z

    move-result v0

    return v0
.end method
