.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PollMyEbooks"
.end annotation


# instance fields
.field private final mExitByTime:J

.field private final mOnboardHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

.field private final mScheduler:Lcom/google/android/ublib/utils/Scheduler;

.field private mTriesLeft:I

.field private final myEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Lcom/google/android/ublib/utils/Scheduler;JILcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 1
    .param p2, "scheduler"    # Lcom/google/android/ublib/utils/Scheduler;
    .param p3, "exitByTime"    # J
    .param p5, "triesLeft"    # I
    .param p6, "onboardHostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    new-instance v0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks$1;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)V

    iput-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->myEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 351
    iput-object p2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mScheduler:Lcom/google/android/ublib/utils/Scheduler;

    .line 352
    iput-wide p3, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mExitByTime:J

    .line 353
    iput p5, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I

    .line 354
    iput-object p6, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mOnboardHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    .line 355
    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    .prologue
    .line 343
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I

    return v0
.end method

.method static synthetic access$1406(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    .prologue
    .line 343
    iget v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I

    return v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)Lcom/google/android/ublib/utils/Scheduler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mScheduler:Lcom/google/android/ublib/utils/Scheduler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    .prologue
    .line 343
    iget-wide v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mExitByTime:J

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;)Lcom/google/android/play/onboard/OnboardHostControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mOnboardHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 424
    const-string v0, "OnboardSampleQuizPage"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    const-string v0, "OnboardSampleQuizPage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PollMyEbooks.run() tries left="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->mTriesLeft:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v0}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1900(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;->myEbooksConsumer:Lcom/google/android/ublib/utils/Consumer;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/books/data/BooksDataController$Priority;->HIGH:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/books/data/BooksDataController;->getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 429
    return-void
.end method
