.class public Lcom/google/android/apps/books/widget/SubdividedListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SubdividedListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/DescribingListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;,
        Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;
    }
.end annotation


# instance fields
.field private mCursorIndexWithinSubdivision:I

.field private mCursorPosition:I

.field private mCursorSubdivisionIndex:I

.field private mHidden:Z

.field private final mJoiner:Ljava/lang/String;

.field private final mSubdivisions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;",
            ">;"
        }
    .end annotation
.end field

.field private final mTitleRowCreator:Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;Landroid/content/Context;ZLjava/util/List;)V
    .locals 2
    .param p1, "titleCreator"    # Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "hidden"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p4, "subdivisions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;>;"
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f018d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;-><init>(Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;Ljava/lang/String;ZLjava/util/List;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;Ljava/lang/String;ZLjava/util/List;)V
    .locals 4
    .param p1, "titleCreator"    # Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;
    .param p2, "joiner"    # Ljava/lang/String;
    .param p3, "hidden"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    .local p4, "subdivisions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mTitleRowCreator:Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;

    .line 82
    iput-object p2, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mJoiner:Ljava/lang/String;

    .line 83
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mHidden:Z

    .line 84
    iput-object p4, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    .line 85
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    .line 86
    .local v1, "subdivision":Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    iget-object v2, v1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    new-instance v3, Lcom/google/android/apps/books/widget/SubdividedListAdapter$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter$1;-><init>(Lcom/google/android/apps/books/widget/SubdividedListAdapter;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    .line 93
    .end local v1    # "subdivision":Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->resetCursorToTop()V

    .line 94
    return-void
.end method

.method private biggestIndexWithinCursorSubdivision()I
    .locals 1

    .prologue
    .line 238
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->rowsForSubdivision(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private cursorIndexForSubdivisionAdapter()I
    .locals 1

    .prologue
    .line 286
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->hasTitleRowWhenNonEmpty(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    iget v0, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    add-int/lit8 v0, v0, -0x1

    .line 289
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    goto :goto_0
.end method

.method private cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    return-object v0
.end method

.method private getAdapterAtCursor()Lcom/google/android/apps/books/widget/DescribingListAdapter;
    .locals 1

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    return-object v0
.end method

.method private hasTitleRowWhenNonEmpty(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)Z
    .locals 5
    .param p1, "subdivision"    # Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 294
    iget-object v4, p1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->title:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 306
    :cond_0
    :goto_0
    return v3

    .line 297
    :cond_1
    iget-boolean v4, p1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->showTitleWhenOtherSubdivisionsAreEmpty:Z

    if-eqz v4, :cond_3

    .line 298
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v2, :cond_2

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    .line 300
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    .line 301
    .local v1, "otherSubdivision":Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    if-eq v1, p1, :cond_4

    iget-object v4, v1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    move v3, v2

    .line 303
    goto :goto_0
.end method

.method private isCursorOnTitleRow()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 278
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->hasTitleRowWhenNonEmpty(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 279
    iget v1, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 281
    :cond_0
    return v0
.end method

.method private resetCursorToTop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 111
    iput v0, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    .line 112
    iput v0, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    .line 113
    iput v0, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    .line 114
    return-void
.end method

.method private rowsForSubdivision(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)I
    .locals 3
    .param p1, "subdivision"    # Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    .prologue
    const/4 v1, 0x0

    .line 242
    iget-object v2, p1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getCount()I

    move-result v0

    .line 243
    .local v0, "subCount":I
    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->hasTitleRowWhenNonEmpty(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v1, v0

    goto :goto_0
.end method

.method private scanCursorTo(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 204
    :goto_0
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    if-lt v3, p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->biggestIndexWithinCursorSubdivision()I

    move-result v3

    if-gez v3, :cond_2

    .line 205
    :cond_0
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    sub-int v1, p1, v3

    .line 207
    .local v1, "itemsToMoveDown":I
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->biggestIndexWithinCursorSubdivision()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    sub-int v0, v3, v4

    .line 209
    .local v0, "itemsRemainingInSubdivision":I
    if-gt v1, v0, :cond_1

    .line 211
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    .line 212
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    goto :goto_0

    .line 215
    :cond_1
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    add-int/lit8 v4, v0, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    .line 216
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    .line 217
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    goto :goto_0

    .line 221
    .end local v0    # "itemsRemainingInSubdivision":I
    .end local v1    # "itemsToMoveDown":I
    :cond_2
    :goto_1
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    if-le v3, p1, :cond_4

    .line 222
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    sub-int v2, v3, p1

    .line 224
    .local v2, "itemsToMoveUp":I
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    if-gt v2, v3, :cond_3

    .line 226
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    sub-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    .line 227
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    sub-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    goto :goto_1

    .line 230
    :cond_3
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    iget v4, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    add-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorPosition:I

    .line 231
    iget v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    .line 232
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->biggestIndexWithinCursorSubdivision()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorIndexWithinSubdivision:I

    goto :goto_1

    .line 235
    .end local v2    # "itemsToMoveUp":I
    :cond_4
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 4

    .prologue
    .line 118
    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mHidden:Z

    if-eqz v3, :cond_1

    .line 119
    const/4 v0, 0x0

    .line 125
    :cond_0
    return v0

    .line 121
    :cond_1
    const/4 v0, 0x0

    .line 122
    .local v0, "count":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    .line 123
    .local v2, "subdivision":Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->rowsForSubdivision(Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;)I

    move-result v3

    add-int/2addr v0, v3

    .line 124
    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->scanCursorTo(I)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->isCursorOnTitleRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->getAdapterAtCursor()Lcom/google/android/apps/books/widget/DescribingListAdapter;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorIndexForSubdivisionAdapter()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 176
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->scanCursorTo(I)V

    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->isCursorOnTitleRow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152
    const/4 v2, 0x0

    .line 159
    :goto_0
    return v2

    .line 155
    :cond_0
    const/4 v1, 0x1

    .line 156
    .local v1, "lesserTypes":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mCursorSubdivisionIndex:I

    if-ge v0, v2, :cond_1

    .line 157
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    iget-object v2, v2, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getViewTypeCount()I

    move-result v2

    add-int/2addr v1, v2

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 159
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->getAdapterAtCursor()Lcom/google/android/apps/books/widget/DescribingListAdapter;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorIndexForSubdivisionAdapter()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getItemViewType(I)I

    move-result v2

    add-int/2addr v2, v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->scanCursorTo(I)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->isCursorOnTitleRow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->title:Ljava/lang/String;

    .line 184
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mTitleRowCreator:Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;->getTitleView(Ljava/lang/String;Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 186
    .end local v0    # "title":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->getAdapterAtCursor()Lcom/google/android/apps/books/widget/DescribingListAdapter;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorIndexForSubdivisionAdapter()I

    move-result v2

    invoke-interface {v1, v2, p2, p3}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 4

    .prologue
    .line 140
    const/4 v0, 0x1

    .line 141
    .local v0, "count":I
    iget-object v3, p0, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->mSubdivisions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    .line 142
    .local v2, "subdivision":Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    iget-object v3, v2, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->adapter:Lcom/google/android/apps/books/widget/DescribingListAdapter;

    invoke-interface {v3}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->getViewTypeCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 143
    goto :goto_0

    .line 144
    .end local v2    # "subdivision":Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    :cond_0
    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->scanCursorTo(I)V

    .line 167
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->isCursorOnTitleRow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorSubdivision()Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;->isTitleEnabled:Z

    .line 170
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->getAdapterAtCursor()Lcom/google/android/apps/books/widget/DescribingListAdapter;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->cursorIndexForSubdivisionAdapter()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/DescribingListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;->resetCursorToTop()V

    .line 107
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 108
    return-void
.end method
