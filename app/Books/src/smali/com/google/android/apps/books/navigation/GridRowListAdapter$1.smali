.class Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;
.super Ljava/lang/Object;
.source "GridRowListAdapter.java"

# interfaces
.implements Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/GridRowListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Lcom/google/android/ublib/widget/AbsWarpListView;III)V
    .locals 0
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 98
    return-void
.end method

.method public onScrollStateChanged(Lcom/google/android/ublib/widget/AbsWarpListView;I)V
    .locals 4
    .param p1, "listView"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    const v3, 0x7fffffff

    # setter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollLastVisible:I
    invoke-static {v2, v3}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$102(Lcom/google/android/apps/books/navigation/GridRowListAdapter;I)I

    move-result v2

    # setter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollFirstVisible:I
    invoke-static {v1, v2}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$002(Lcom/google/android/apps/books/navigation/GridRowListAdapter;I)I

    .line 83
    if-nez p2, :cond_3

    .line 84
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFirstVisiblePosition()I

    move-result v0

    .line 85
    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getLastVisiblePosition()I

    move-result v1

    if-gt v0, v1, :cond_2

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {p1, v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getView(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->setViewVisible(Landroid/view/View;IZ)V

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 88
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    # getter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
    invoke-static {v1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$200(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->invalidateLists()V

    goto :goto_0

    .line 89
    .end local v0    # "i":I
    :cond_3
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFinalFirstVisiblePosition()I

    move-result v2

    # setter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollFirstVisible:I
    invoke-static {v1, v2}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$002(Lcom/google/android/apps/books/navigation/GridRowListAdapter;I)I

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;->this$0:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getFinalLastVisiblePosition()I

    move-result v2

    # setter for: Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollLastVisible:I
    invoke-static {v1, v2}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->access$102(Lcom/google/android/apps/books/navigation/GridRowListAdapter;I)I

    goto :goto_0
.end method
