.class Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1$1;
.super Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;
.source "EndOfBookCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;->getHandler(Lcom/google/android/apps/books/playcards/BookDocument;)Lcom/google/android/ublib/cardlib/PlayCardMenuHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;

.field final synthetic val$doc:Lcom/google/android/apps/books/playcards/BookDocument;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;Landroid/content/Context;Lcom/google/android/apps/books/playcards/BookDocument;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1$1;->this$1:Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;

    iput-object p3, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1$1;->val$doc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-direct {p0, p2}, Lcom/google/android/ublib/cardlib/model/DocumentMenuHandler;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public addMenuEntries(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;>;"
    new-instance v0, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;

    iget-object v1, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1$1;->this$1:Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;

    iget-object v1, v1, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1;->this$0:Lcom/google/android/apps/books/eob/EndOfBookCardProvider;

    # getter for: Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/apps/books/eob/EndOfBookCardProvider;->access$100(Lcom/google/android/apps/books/eob/EndOfBookCardProvider;)Landroid/app/Activity;

    move-result-object v1

    const-string v2, "books_inapp_eob_submenu_buy_from_recommendation"

    iget-object v3, p0, Lcom/google/android/apps/books/eob/EndOfBookCardProvider$1$1;->val$doc:Lcom/google/android/apps/books/playcards/BookDocument;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/RecommendationMenuEntry;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/books/playcards/BookDocument;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method
