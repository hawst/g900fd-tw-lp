.class public final Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Blobs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/proto/Blobs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BlobInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;


# instance fields
.field private hasName:Z

.field private hasSize:Z

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/String;

.field private size_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 297
    new-instance v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;-><init>(Z)V

    sput-object v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 298
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs;->internalForceInit()V

    .line 299
    sget-object v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->initFields()V

    .line 300
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->name_:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->size_:I

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->memoizedSerializedSize:I

    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->initFields()V

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/annotations/proto/Blobs$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$1;

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1, "noInit"    # Z

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->name_:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->size_:I

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasName:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->name_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasSize:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .param p1, "x1"    # I

    .prologue
    .line 11
    iput p1, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->size_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->defaultInstance:Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public static newBuilder()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    .locals 1

    .prologue
    .line 144
    # invokes: Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->create()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;->access$100()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->memoizedSerializedSize:I

    .line 62
    .local v0, "size":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    .line 74
    .end local v0    # "size":I
    .local v1, "size":I
    :goto_0
    return v1

    .line 64
    .end local v1    # "size":I
    .restart local v0    # "size":I
    :cond_0
    const/4 v0, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasName()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasSize()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 70
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSize()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 73
    :cond_2
    iput v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->memoizedSerializedSize:I

    move v1, v0

    .line 74
    .end local v0    # "size":I
    .restart local v1    # "size":I
    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->size_:I

    return v0
.end method

.method public hasName()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasName:Z

    return v0
.end method

.method public hasSize()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasSize:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSize()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 57
    :cond_1
    return-void
.end method
