.class public interface abstract Lcom/google/android/apps/books/annotations/AnnotationServer;
.super Ljava/lang/Object;
.source "AnnotationServer.java"


# virtual methods
.method public abstract addAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract deleteAnnotation(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract editAnnotation(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getAnnotationData(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationData$Key;)Lcom/google/android/apps/books/annotations/AnnotationData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation
.end method

.method public abstract getAnnotationDatas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/Collection;II)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/annotations/AnnotationData$Key;",
            ">;II)",
            "Ljava/io/InputStream;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getCharacterQuotas(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation
.end method

.method public abstract getUserAnnotations(Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/ApiaryAnnotationServer$UserAnnotationRequest;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/annotations/LastSyncTooOldException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVolumeAnnotations(Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/apps/books/annotations/PaginatedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeAnnotationRequest;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/annotations/PaginatedResponse",
            "<",
            "Lcom/google/android/apps/books/annotations/data/JsonAnnotation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException;
        }
    .end annotation
.end method

.method public abstract getVolumeLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/VolumeVersion;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/annotations/AnnotationServerException;
        }
    .end annotation
.end method
