.class Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;
.super Ljava/lang/Object;
.source "BooksCardsAdapter.java"

# interfaces
.implements Lcom/google/android/ublib/cardlib/PlayCardImageProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/BooksCardsAdapter;->bindToUpload(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/upload/Upload;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

.field final synthetic val$res:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsAdapter;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;->val$res:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelFetch()V
    .locals 0

    .prologue
    .line 273
    return-void
.end method

.method public fetchImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)V
    .locals 4
    .param p1, "imageUri"    # Landroid/net/Uri;
    .param p2, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p3, "listener"    # Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .prologue
    .line 264
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;->val$res:Landroid/content/res/Resources;

    const v3, 0x7f020079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 265
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v2, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 266
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 267
    .local v1, "result":Landroid/graphics/Bitmap;
    invoke-interface {p3, v1}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;->onImage(Landroid/graphics/Bitmap;)V

    .line 268
    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsAdapter$3;->this$0:Lcom/google/android/apps/books/widget/BooksCardsAdapter;

    # invokes: Lcom/google/android/apps/books/widget/BooksCardsAdapter;->onFetchedCoverImage()V
    invoke-static {v2}, Lcom/google/android/apps/books/widget/BooksCardsAdapter;->access$100(Lcom/google/android/apps/books/widget/BooksCardsAdapter;)V

    .line 270
    .end local v1    # "result":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method
