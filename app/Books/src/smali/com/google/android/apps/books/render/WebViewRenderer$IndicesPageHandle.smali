.class Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;
.super Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;
.source "WebViewRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IndicesPageHandle"
.end annotation


# instance fields
.field private final mIndices:Lcom/google/android/apps/books/render/PageIndices;

.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRenderer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/render/PageIndices;)V
    .locals 1
    .param p2, "indices"    # Lcom/google/android/apps/books/render/PageIndices;

    .prologue
    .line 468
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    .line 469
    invoke-virtual {p1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/render/WebViewRenderer$BaseFlowingPageHandle;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;)V

    .line 470
    iput-object p2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    .line 471
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/render/PageIndices;Lcom/google/android/apps/books/render/WebViewRenderer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/render/WebViewRenderer;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/PageIndices;
    .param p3, "x2"    # Lcom/google/android/apps/books/render/WebViewRenderer$1;

    .prologue
    .line 464
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;-><init>(Lcom/google/android/apps/books/render/WebViewRenderer;Lcom/google/android/apps/books/render/PageIndices;)V

    return-void
.end method

.method private getPositionForChapterLookup()Lcom/google/android/apps/books/common/Position;
    .locals 5

    .prologue
    .line 474
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;
    invoke-static {v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v3, v3, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v4, v4, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v1

    .line 476
    .local v1, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v1, :cond_1

    .line 477
    const/4 v0, 0x0

    .line 485
    :cond_0
    :goto_0
    return-object v0

    .line 481
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getLastViewablePosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 482
    .local v0, "lastPosition":Lcom/google/android/apps/books/common/Position;
    if-nez v0, :cond_0

    .line 485
    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPrimaryPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected createPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 4

    .prologue
    .line 559
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v2, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v3, v3, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 561
    .local v0, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 564
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected createPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;
    .locals 4

    .prologue
    .line 544
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v2, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v3, v3, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 546
    .local v0, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v1

    .line 549
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected createSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    .locals 13

    .prologue
    .line 570
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->getPageIdentifier()Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v2

    .line 571
    .local v2, "pi":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v9, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;
    invoke-static {v9}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-result-object v9

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getInitialBlankPagesCount(I)I

    move-result v0

    .line 573
    .local v0, "initialBlankPagesCount":I
    iget-object v9, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # invokes: Lcom/google/android/apps/books/render/WebViewRenderer;->getPagesPerSpread()I
    invoke-static {v9}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$800(Lcom/google/android/apps/books/render/WebViewRenderer;)I

    move-result v1

    .line 576
    .local v1, "pagesPerSpread":I
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v9

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v10

    sub-int v3, v9, v10

    .line 577
    .local v3, "positionPassagePageIndex":I
    add-int v4, v3, v0

    .line 578
    .local v4, "positionScreenPageIndex":I
    div-int v5, v4, v1

    .line 580
    .local v5, "positionSpreadIndex":I
    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v9

    add-int v6, v9, v0

    .line 581
    .local v6, "screenPageIndex":I
    div-int v7, v6, v1

    .line 582
    .local v7, "spreadIndex":I
    rem-int v8, v6, v1

    .line 584
    .local v8, "spreadPageIndex":I
    new-instance v9, Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    new-instance v10, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v11

    sub-int v12, v7, v5

    invoke-direct {v10, v11, v12}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    invoke-direct {v9, v10, v8}, Lcom/google/android/apps/books/render/SpreadPageIdentifier;-><init>(Lcom/google/android/apps/books/render/SpreadIdentifier;I)V

    return-object v9
.end method

.method public getFirstBookPageIndex()I
    .locals 4

    .prologue
    .line 510
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mModel:Lcom/google/android/apps/books/render/WebViewRendererModel;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$000(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/WebViewRendererModel;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v2, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v3, v3, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v0

    .line 512
    .local v0, "rendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v0, :cond_0

    .line 513
    const/4 v1, -0x1

    .line 515
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getPrimaryPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/render/WebViewRenderer;->getFirstBookPageIndex(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$600(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/common/Position;)I

    move-result v1

    goto :goto_0
.end method

.method public getFirstChapterIndex()I
    .locals 4

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->getPositionForChapterLookup()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 495
    .local v0, "position":Lcom/google/android/apps/books/common/Position;
    if-eqz v0, :cond_0

    .line 496
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v2

    .line 505
    :goto_0
    return v2

    .line 502
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v3, v3, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getFirstSegmentForPassageIndex(I)Lcom/google/android/apps/books/model/Segment;

    move-result-object v1

    .line 505
    .local v1, "segment":Lcom/google/android/apps/books/model/Segment;
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Segment;->getStartPositionObject()Lcom/google/android/apps/books/common/Position;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getChapterIndexForPosition(Lcom/google/android/apps/books/common/Position;)I

    move-result v2

    goto :goto_0
.end method

.method public getGridRowIndex()I
    .locals 3

    .prologue
    .line 521
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    iget v2, v2, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageSegmentIndices(I)Ljava/util/List;

    move-result-object v0

    .line 523
    .local v0, "passageSegmentIndices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524
    const/4 v1, -0x1

    .line 526
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mGridRowIndexToStartSegmentIndex:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$700(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    move-result-object v2

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->rightIndexToLeftIndex(I)I

    move-result v1

    goto :goto_0
.end method

.method public getIndices()Lcom/google/android/apps/books/render/PageIndices;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    return-object v0
.end method

.method public getPosition()Lcom/google/android/apps/books/common/Position;
    .locals 2

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/android/apps/books/render/WebViewRenderer$IndicesPageHandle;->getPositionPageIdentifier()Lcom/google/android/apps/books/render/PositionPageIdentifier;

    move-result-object v0

    .line 535
    .local v0, "pageIdentifier":Lcom/google/android/apps/books/render/PositionPageIdentifier;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->getPageOffsetFromPosition()I

    move-result v1

    if-nez v1, :cond_0

    .line 536
    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PositionPageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 538
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
