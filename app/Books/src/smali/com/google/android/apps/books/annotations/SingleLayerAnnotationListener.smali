.class public abstract Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;
.super Lcom/google/android/apps/books/annotations/UnoptimizedAnnotationListener;
.source "SingleLayerAnnotationListener.java"


# instance fields
.field private final mLayerId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "layerId"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/UnoptimizedAnnotationListener;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;->mLayerId:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public final annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p2, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;->mLayerId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;->layerAnnotationsLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V

    .line 25
    :cond_0
    return-void
.end method

.method public characterQuotaLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "quota":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;->mLayerId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {p0, p2}, Lcom/google/android/apps/books/annotations/SingleLayerAnnotationListener;->layerQuotaLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V

    .line 32
    :cond_0
    return-void
.end method

.method public abstract layerAnnotationsLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract layerQuotaLoaded(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;",
            ">;)V"
        }
    .end annotation
.end method
