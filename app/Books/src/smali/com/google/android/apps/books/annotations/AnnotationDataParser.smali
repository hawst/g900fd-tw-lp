.class public Lcom/google/android/apps/books/annotations/AnnotationDataParser;
.super Ljava/lang/Object;
.source "AnnotationDataParser.java"


# static fields
.field private static final ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;",
            ">;"
        }
    .end annotation
.end field

.field private static final CONJUGATION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFINITION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Definition;",
            ">;"
        }
    .end annotation
.end field

.field private static final SENSE_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Sense;",
            ">;"
        }
    .end annotation
.end field

.field private static final WORD_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 215
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->WORD_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    .line 236
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser$2;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->SENSE_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    .line 257
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser$3;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$3;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->DEFINITION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    .line 268
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser$4;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$4;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    .line 287
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser$5;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$5;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->CONJUGATION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->SENSE_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    return-object v0
.end method

.method static synthetic access$100()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->ATTRIBUTED_TEXT_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    return-object v0
.end method

.method static synthetic access$200(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    .locals 1
    .param p0, "x0"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 36
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAttribution(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/codehaus/jackson/JsonNode;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->DEFINITION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    return-object v0
.end method

.method static synthetic access$500()Lcom/google/android/apps/books/util/JsonUtils$NodeParser;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->CONJUGATION_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    return-object v0
.end method

.method private static getCachePolicy(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .locals 3
    .param p0, "object"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 205
    const-string v2, "cachePolicy"

    invoke-static {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 207
    .local v1, "string":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 211
    :goto_0
    return-object v2

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNKNOWN:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    goto :goto_0

    .line 210
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v2, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNKNOWN:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    goto :goto_0
.end method

.method private static getFieldFloatValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)F
    .locals 4
    .param p0, "object"    # Lorg/codehaus/jackson/JsonNode;
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 189
    invoke-virtual {p0, p1}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    .line 190
    .local v0, "value":Lorg/codehaus/jackson/JsonNode;
    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonNode;->asDouble()D

    move-result-wide v2

    double-to-float v1, v2

    .line 193
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getFieldIntValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)I
    .locals 2
    .param p0, "object"    # Lorg/codehaus/jackson/JsonNode;
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 197
    invoke-virtual {p0, p1}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    .line 198
    .local v0, "value":Lorg/codehaus/jackson/JsonNode;
    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonNode;->asInt()I

    move-result v1

    .line 201
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "object"    # Lorg/codehaus/jackson/JsonNode;
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-virtual {p0, p1}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    .line 182
    .local v0, "value":Lorg/codehaus/jackson/JsonNode;
    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonNode;->getTextValue()Ljava/lang/String;

    move-result-object v1

    .line 185
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static parseAnnotationData(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationData;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "userLocale"    # Ljava/lang/String;
    .param p2, "entityLocale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/codehaus/jackson/JsonParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {p0}, Lcom/google/android/apps/books/util/JsonUtils;->parseFrom(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseAnnotationData(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationData;

    move-result-object v0

    return-object v0
.end method

.method public static parseAnnotationData(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/AnnotationData;
    .locals 8
    .param p0, "node"    # Lorg/codehaus/jackson/JsonNode;
    .param p1, "userLocale"    # Ljava/lang/String;
    .param p2, "entityLocale"    # Ljava/lang/String;

    .prologue
    .line 131
    const-string v6, "id"

    invoke-virtual {p0, v6}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v6

    invoke-virtual {v6}, Lorg/codehaus/jackson/JsonNode;->getTextValue()Ljava/lang/String;

    move-result-object v4

    .line 133
    .local v4, "id":Ljava/lang/String;
    const-string v6, "data"

    invoke-virtual {p0, v6}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v1

    .line 136
    .local v1, "data":Lorg/codehaus/jackson/JsonNode;
    if-eqz v1, :cond_1

    .line 137
    const-string v6, "common"

    invoke-virtual {v1, v6}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseCommon(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    move-result-object v0

    .line 138
    .local v0, "common":Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    const-string v6, "geo"

    invoke-virtual {v1, v6}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseGeo(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;

    move-result-object v3

    .line 139
    .local v3, "geo":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    const-string v6, "dict"

    invoke-virtual {v1, v6}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v7

    if-nez v0, :cond_0

    const/4 v6, 0x0

    :goto_0
    invoke-static {v7, v6}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->parseDictionary(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/DictionaryEntry;

    move-result-object v2

    .line 141
    .local v2, "dict":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    new-instance v5, Lcom/google/android/apps/books/annotations/GeoPayload;

    invoke-direct {v5, v0, v3, v2}, Lcom/google/android/apps/books/annotations/GeoPayload;-><init>(Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;Lcom/google/android/apps/books/annotations/DictionaryEntry;)V

    .line 146
    .end local v0    # "common":Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    .end local v2    # "dict":Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .end local v3    # "geo":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .local v5, "payload":Lcom/google/android/apps/books/annotations/AnnotationDataPayload;
    :goto_1
    new-instance v6, Lcom/google/android/apps/books/annotations/AnnotationData;

    new-instance v7, Lcom/google/android/apps/books/annotations/AnnotationData$Key;

    invoke-direct {v7, v4, p1, p2}, Lcom/google/android/apps/books/annotations/AnnotationData$Key;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v7, v5}, Lcom/google/android/apps/books/annotations/AnnotationData;-><init>(Lcom/google/android/apps/books/annotations/AnnotationData$Key;Lcom/google/android/apps/books/annotations/AnnotationDataPayload;)V

    return-object v6

    .line 139
    .end local v5    # "payload":Lcom/google/android/apps/books/annotations/AnnotationDataPayload;
    .restart local v0    # "common":Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    .restart local v3    # "geo":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    :cond_0
    iget-object v6, v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;->title:Ljava/lang/String;

    goto :goto_0

    .line 143
    .end local v0    # "common":Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    .end local v3    # "geo":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    :cond_1
    const/4 v5, 0x0

    .restart local v5    # "payload":Lcom/google/android/apps/books/annotations/AnnotationDataPayload;
    goto :goto_1
.end method

.method public static parseApiaryListResponse(Ljava/io/InputStream;)Ljava/util/List;
    .locals 6
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/codehaus/jackson/JsonNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/codehaus/jackson/JsonParseException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    invoke-static {p0}, Lcom/google/android/apps/books/util/JsonUtils;->parseFrom(Ljava/io/InputStream;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v3

    .line 113
    .local v3, "response":Lorg/codehaus/jackson/JsonNode;
    const-string v5, "items"

    invoke-virtual {v3, v5}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v2

    .line 114
    .local v2, "items":Lorg/codehaus/jackson/JsonNode;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 115
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lorg/codehaus/jackson/JsonNode;>;"
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonNode;->isArray()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonNode;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/codehaus/jackson/JsonNode;

    .line 117
    .local v1, "item":Lorg/codehaus/jackson/JsonNode;
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 120
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lorg/codehaus/jackson/JsonNode;
    :cond_0
    return-object v4
.end method

.method private static parseAttribution(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;
    .locals 3
    .param p0, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 278
    if-nez p0, :cond_0

    .line 279
    const/4 v2, 0x0

    .line 283
    :goto_0
    return-object v2

    .line 281
    :cond_0
    const-string v2, "attribution"

    invoke-static {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 282
    .local v0, "text":Ljava/lang/String;
    const-string v2, "url"

    invoke-static {p0, v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "url":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static parseCommon(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;
    .locals 6
    .param p0, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 154
    const-string v0, "lang"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    .local v1, "lang":Ljava/lang/String;
    const-string v0, "snippet"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "snippet":Ljava/lang/String;
    const-string v0, "snippetUrl"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 157
    .local v3, "snippetUrl":Ljava/lang/String;
    const-string v0, "previewImageUrl"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 158
    .local v4, "previewImageUrl":Ljava/lang/String;
    const-string v0, "title"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 159
    .local v5, "title":Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/annotations/data/CommonAnnotationPayload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static parseDictionary(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Lcom/google/android/apps/books/annotations/DictionaryEntry;
    .locals 4
    .param p0, "json"    # Lorg/codehaus/jackson/JsonNode;
    .param p1, "fallbackTitle"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 229
    if-nez p0, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-object v1

    .line 232
    :cond_1
    const-string v2, "words"

    invoke-virtual {p0, v2}, Lorg/codehaus/jackson/JsonNode;->get(Ljava/lang/String;)Lorg/codehaus/jackson/JsonNode;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->WORD_PARSER:Lcom/google/android/apps/books/util/JsonUtils$NodeParser;

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/JsonUtils;->parseList(Lorg/codehaus/jackson/JsonNode;Lcom/google/android/apps/books/util/JsonUtils$NodeParser;)Ljava/util/List;

    move-result-object v0

    .line 233
    .local v0, "words":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/DictionaryEntry$Word;>;"
    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/books/annotations/DictionaryEntry;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/books/annotations/DictionaryEntry;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method private static parseGeo(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
    .locals 7
    .param p0, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 166
    if-nez p0, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 176
    :goto_0
    return-object v0

    .line 170
    :cond_0
    const-string v0, "latitude"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldFloatValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)F

    move-result v1

    .line 171
    .local v1, "latitude":F
    const-string v0, "longitude"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldFloatValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)F

    move-result v2

    .line 172
    .local v2, "longitude":F
    const-string v0, "zoom"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldIntValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)I

    move-result v3

    .line 173
    .local v3, "zoom":I
    const-string v0, "countryCode"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 174
    .local v4, "countryCode":Ljava/lang/String;
    const-string v0, "resolution"

    invoke-static {p0, v0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 175
    .local v5, "resolution":Ljava/lang/String;
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getCachePolicy(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    move-result-object v6

    .line 176
    .local v6, "cachePolicy":Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    new-instance v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;-><init>(FFILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;)V

    goto :goto_0
.end method
