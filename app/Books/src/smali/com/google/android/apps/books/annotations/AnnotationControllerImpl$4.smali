.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/UserChangesEditor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->getForegroundAnnotationEditor(Lcom/google/android/apps/books/annotations/VolumeVersion;)Lcom/google/android/apps/books/annotations/UserChangesEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$cache:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$cache:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uiAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Updateable;)V
    .locals 4
    .param p1, "layer"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Updateable",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p2, "updateable":Lcom/google/android/apps/books/annotations/Updateable;, "Lcom/google/android/apps/books/annotations/Updateable<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$cache:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-interface {p2}, Lcom/google/android/apps/books/annotations/Updateable;->currentBestValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->add(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;

    invoke-direct {v3, p0, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Lcom/google/android/apps/books/annotations/Updateable;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V

    .line 249
    return-void
.end method

.method public uiEdit(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 5
    .param p1, "oldValue"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "newValue"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$cache:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->markEdited(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;

    invoke-direct {v3, p0, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Lcom/google/android/apps/books/annotations/Annotation;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V

    .line 283
    return-void
.end method

.method public uiRemove(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 6
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 253
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "layer":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, "localId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$cache:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->markDeleted(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 256
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v4}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;

    invoke-direct {v5, p0, v1, v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$2;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Ljava/lang/String;Ljava/lang/String;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V

    .line 269
    return-void
.end method

.method public uiUpdateLastUsedTimestampToNow(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 4
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 287
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 288
    .local v0, "timestamp":J
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$cache:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->updateLastUsedTimestamp(Lcom/google/android/apps/books/annotations/Annotation;J)V

    .line 289
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$400(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Lcom/google/android/apps/books/annotations/Annotation;J)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 294
    return-void
.end method
