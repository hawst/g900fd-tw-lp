.class public interface abstract Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioClipPlayerListener"
.end annotation


# virtual methods
.method public abstract onClipFinishedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
.end method

.method public abstract onClipStartedPlaying(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
.end method
