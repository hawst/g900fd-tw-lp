.class Lcom/google/android/apps/books/app/BaseBooksActivity$1;
.super Ljava/lang/Object;
.source "BaseBooksActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BaseBooksActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/BaseBooksActivity;)V
    .locals 0

    .prologue
    .line 139
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$1;, "Lcom/google/android/apps/books/app/BaseBooksActivity.1;"
    iput-object p1, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 143
    .local p0, "this":Lcom/google/android/apps/books/app/BaseBooksActivity$1;, "Lcom/google/android/apps/books/app/BaseBooksActivity.1;"
    invoke-static {p2}, Lcom/google/android/apps/books/preference/LocalPreferences;->isAccountKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/books/app/BaseBooksActivity$1;->this$0:Lcom/google/android/apps/books/app/BaseBooksActivity;

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getChangeBroadcaster(Landroid/content/Context;)Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 148
    :cond_0
    return-void
.end method
