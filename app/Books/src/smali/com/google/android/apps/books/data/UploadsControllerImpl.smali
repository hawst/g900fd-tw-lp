.class public Lcom/google/android/apps/books/data/UploadsControllerImpl;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/UploadsController;
.implements Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;
    }
.end annotation


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mClock:Lcom/google/android/apps/books/model/Clock;

.field private final mDataController:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

.field private final mForegroundExecutor:Ljava/util/concurrent/Executor;

.field private mLastPersist:J

.field private final mPersistInterval:J

.field private final mUpdateQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;",
            ">;"
        }
    .end annotation
.end field

.field private final mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

.field private final mUploadManager:Lcom/google/android/apps/books/upload/UploadManager;

.field private final mUploaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/upload/SingleBookUploader;",
            ">;"
        }
    .end annotation
.end field

.field private mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

.field private mUploadsFetched:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/upload/UploadDataStorage;Lcom/google/android/apps/books/model/Clock;Lcom/google/android/apps/books/upload/UploadManager;)V
    .locals 5
    .param p1, "controller"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;
    .param p2, "foregroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "backgroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "uploadDataStorage"    # Lcom/google/android/apps/books/upload/UploadDataStorage;
    .param p5, "clock"    # Lcom/google/android/apps/books/model/Clock;
    .param p6, "uploadManager"    # Lcom/google/android/apps/books/upload/UploadManager;

    .prologue
    const/4 v4, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;

    .line 59
    iput-object v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    .line 68
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUpdateQueue:Ljava/util/Queue;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsFetched:Z

    .line 99
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mDataController:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    .line 100
    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    .line 101
    iput-object p3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 102
    iput-object p4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

    .line 103
    iput-object p5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Clock;->currentTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mLastPersist:J

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/Clock;->timeUnitsPerSecond()J

    move-result-wide v0

    const-wide/16 v2, 0x5

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mPersistInterval:J

    .line 106
    iput-object p6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadManager:Lcom/google/android/apps/books/upload/UploadManager;

    .line 108
    invoke-direct {p0, v4}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->fetchUploads(Lcom/google/android/ublib/utils/Consumer;)V

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadManager:Lcom/google/android/apps/books/upload/UploadManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mDataController:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/ublib/utils/Updater;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;
    .param p1, "x1"    # Lcom/google/android/ublib/utils/Updater;
    .param p2, "x2"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->queueUpdate(Lcom/google/android/ublib/utils/Updater;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsFetched:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/books/data/UploadsControllerImpl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsFetched:Z

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;
    .param p1, "x1"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/UploadsControllerImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->applyUpdates()V

    return-void
.end method

.method private applyUpdates()V
    .locals 8

    .prologue
    .line 323
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    if-nez v3, :cond_1

    .line 324
    const-string v3, "UploadsController"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 325
    const-string v3, "UploadsController"

    const-string v4, "Uploads cache is null where it shouldn\'t be."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUpdateQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 332
    const/4 v1, 0x0

    .line 333
    .local v1, "shouldPersist":Z
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUpdateQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 334
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUpdateQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;

    .line 335
    .local v2, "updater":Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;->update(Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    .line 336
    invoke-virtual {v2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;->shouldPersist()Z

    move-result v3

    or-int/2addr v1, v3

    .line 337
    goto :goto_1

    .line 339
    .end local v2    # "updater":Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;
    :cond_2
    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Clock;->currentTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mLastPersist:J

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mPersistInterval:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 340
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->getCacheCopy()Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v0

    .line 342
    .local v0, "cacheCopy":Lcom/google/android/apps/books/upload/Upload$Uploads;
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/google/android/apps/books/data/UploadsControllerImpl$10;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl$10;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/apps/books/upload/Upload$Uploads;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 350
    iget-object v3, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Clock;->currentTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mLastPersist:J

    goto :goto_0
.end method

.method private deleteUploadOnlyBackground(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 280
    return-void
.end method

.method private fetchUploads(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/upload/Upload$Uploads;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 380
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/upload/Upload$Uploads;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 433
    return-void
.end method

.method private getCacheCopy()Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    if-nez v0, :cond_0

    .line 359
    const/4 v0, 0x0

    .line 361
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/upload/Upload$Uploads;

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/upload/Upload$Uploads;-><init>(Lcom/google/android/apps/books/upload/Upload$Uploads;)V

    goto :goto_0
.end method

.method public static isUploadIntent(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 442
    invoke-virtual {p0, p1}, Landroid/content/Intent;->resolveType(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 443
    .local v0, "type":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v1, "application/pdf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "application/epub+zip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private queueUpdate(Lcom/google/android/ublib/utils/Updater;Z)V
    .locals 2
    .param p2, "shouldPersist"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Updater",
            "<",
            "Lcom/google/android/apps/books/upload/Upload$Uploads;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 312
    .local p1, "updater":Lcom/google/android/ublib/utils/Updater;, "Lcom/google/android/ublib/utils/Updater<Lcom/google/android/apps/books/upload/Upload$Uploads;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUpdateQueue:Ljava/util/Queue;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;-><init>(Lcom/google/android/ublib/utils/Updater;Z)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->applyUpdates()V

    .line 316
    :cond_0
    return-void
.end method


# virtual methods
.method public abortUpload(Ljava/lang/String;Ljava/lang/Exception;Z)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "processingError"    # Z

    .prologue
    .line 169
    if-eqz p3, :cond_1

    .line 170
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SERVER_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 174
    :goto_0
    const-string v0, "UploadsController"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    const-string v0, "UploadsController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The upload failed due to the following exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_0
    return-void

    .line 172
    :cond_1
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ERROR:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    goto :goto_0
.end method

.method public addUpload(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 114
    .local v4, "id":Ljava/lang/String;
    new-instance v5, Lcom/google/android/apps/books/upload/Upload;

    invoke-direct {v5, v4, p2}, Lcom/google/android/apps/books/upload/Upload;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .local v5, "upload":Lcom/google/android/apps/books/upload/Upload;
    new-instance v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$1;

    invoke-direct {v0, p0, v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl$1;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/apps/books/upload/Upload;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->queueUpdate(Lcom/google/android/ublib/utils/Updater;Z)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mDataController:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$2;

    invoke-direct {v1, p0, v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl$2;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/apps/books/upload/Upload;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->notifyListeners(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;)V

    .line 131
    iget-object v6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl$3;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/upload/Upload;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 164
    return-object v4
.end method

.method public cancelUpload(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$12;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$12;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 463
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->deleteUpload(Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method public deleteUpload(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$9;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$9;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 305
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->deleteUploadOnlyBackground(Ljava/lang/String;)V

    .line 306
    return-void
.end method

.method public onNewListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsCache:Lcom/google/android/apps/books/upload/Upload$Uploads;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/model/BooksDataListener;->onUploads(Lcom/google/android/apps/books/upload/Upload$Uploads;)V

    .line 439
    :cond_0
    return-void
.end method

.method public pauseUpload(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$14;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$14;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 491
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_PAUSED:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 492
    return-void
.end method

.method public resumeUpload(Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$13;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$13;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 477
    sget-object v0, Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;->SCOTTY_ACTIVE:Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    .line 478
    return-void
.end method

.method public scheduleBackgroundTask(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 450
    return-void
.end method

.method public updateOneUpload(Ljava/lang/String;Lcom/google/android/ublib/utils/Updater;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Updater",
            "<",
            "Lcom/google/android/apps/books/upload/Upload;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 506
    .local p2, "updater":Lcom/google/android/ublib/utils/Updater;, "Lcom/google/android/ublib/utils/Updater<Lcom/google/android/apps/books/upload/Upload;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$16;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$16;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;Lcom/google/android/ublib/utils/Updater;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 521
    return-void
.end method

.method public updateUploadFilesize(Ljava/lang/String;I)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "fileSize"    # I

    .prologue
    .line 260
    new-instance v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$7;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$7;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;I)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateOneUpload(Ljava/lang/String;Lcom/google/android/ublib/utils/Updater;)V

    .line 267
    return-void
.end method

.method public updateUploadProgress(Ljava/lang/String;I)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "uploadPercentage"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$4;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 203
    return-void
.end method

.method public updateUploadStatus(Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "status"    # Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$5;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;Lcom/google/android/apps/books/upload/proto/UploadProto$Upload$UploadStatus;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 230
    return-void
.end method

.method public updateUploadTransferHandle(Ljava/lang/String;Lcom/google/uploader/client/ClientProto$TransferHandle;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "transferHandle"    # Lcom/google/uploader/client/ClientProto$TransferHandle;

    .prologue
    .line 496
    new-instance v0, Lcom/google/android/apps/books/data/UploadsControllerImpl$15;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$15;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/uploader/client/ClientProto$TransferHandle;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateOneUpload(Ljava/lang/String;Lcom/google/android/ublib/utils/Updater;)V

    .line 503
    return-void
.end method

.method public updateVolumeId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/UploadsControllerImpl$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/data/UploadsControllerImpl$6;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 257
    return-void
.end method
