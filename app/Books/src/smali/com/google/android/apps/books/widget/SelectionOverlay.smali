.class public Lcom/google/android/apps/books/widget/SelectionOverlay;
.super Landroid/view/View;
.source "SelectionOverlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/SelectionOverlay$1;,
        Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;,
        Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;
    }
.end annotation


# static fields
.field private static final mIdentityMatrix:Landroid/graphics/Matrix;


# instance fields
.field private final mDisplayFps:Z

.field private mDraggingFirstHandle:Z

.field private final mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

.field private final mFpsHelper:Lcom/google/android/apps/books/util/FpsHelper;

.field private final mHandlesSelectPaint:Landroid/graphics/Paint;

.field private mHasHandles:Z

.field private mHidingHandles:Z

.field private mIsSelectionLocked:Z

.field private mIsTapping:Z

.field private mLastRangeDataRequestId:I

.field private final mLastTouchPos:Landroid/graphics/Point;

.field private final mLeftHandleDrawable:Landroid/graphics/drawable/Drawable;

.field private final mPageToViewMatrix:Landroid/graphics/Matrix;

.field private final mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

.field private final mPreallocatedRect:Landroid/graphics/Rect;

.field private final mRectsPaint:Landroid/graphics/Paint;

.field private final mRegionDrawer:Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;

.field private final mRightHandleDrawable:Landroid/graphics/drawable/Drawable;

.field private final mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

.field private final mTapStart:Landroid/graphics/Point;

.field private mTextRectsValidForRequestId:I

.field private final mViewtoPageMatrix:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIdentityMatrix:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/books/widget/SelectionPopup;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "renderingTheme"    # Ljava/lang/String;
    .param p3, "popup"    # Lcom/google/android/apps/books/widget/SelectionPopup;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 111
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 67
    iput-boolean v8, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mDisplayFps:Z

    .line 68
    new-instance v4, Lcom/google/android/apps/books/util/FpsHelper;

    const v5, -0xffff01

    invoke-direct {v4, v5}, Lcom/google/android/apps/books/util/FpsHelper;-><init>(I)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mFpsHelper:Lcom/google/android/apps/books/util/FpsHelper;

    .line 70
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v6, v6}, Landroid/graphics/Point;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastTouchPos:Landroid/graphics/Point;

    .line 71
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v6, v6}, Landroid/graphics/Point;-><init>(II)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    .line 76
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHandlesSelectPaint:Landroid/graphics/Paint;

    .line 77
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRectsPaint:Landroid/graphics/Paint;

    .line 80
    new-instance v4, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    invoke-direct {v4, v7}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;-><init>(Lcom/google/android/apps/books/widget/SelectionOverlay$1;)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    .line 81
    new-instance v4, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    invoke-direct {v4, v7}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;-><init>(Lcom/google/android/apps/books/widget/SelectionOverlay$1;)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    .line 88
    iput-boolean v8, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsSelectionLocked:Z

    .line 100
    iput v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    .line 102
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    .line 103
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mViewtoPageMatrix:Landroid/graphics/Matrix;

    .line 105
    new-instance v4, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;

    invoke-direct {v4, v7}, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;-><init>(Lcom/google/android/apps/books/widget/SelectionOverlay$1;)V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRegionDrawer:Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;

    .line 106
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPreallocatedRect:Landroid/graphics/Rect;

    .line 318
    iput v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTextRectsValidForRequestId:I

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 114
    .local v2, "res":Landroid/content/res/Resources;
    invoke-static {p2}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedSelectionColorId(Ljava/lang/String;)I

    move-result v3

    .line 115
    .local v3, "selectionColorId":I
    invoke-static {p2}, Lcom/google/android/apps/books/util/ReaderUtils;->getThemedBlueForegroundColorId(Ljava/lang/String;)I

    move-result v1

    .line 116
    .local v1, "grabberColorId":I
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 118
    .local v0, "grabberColor":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHandlesSelectPaint:Landroid/graphics/Paint;

    const v5, -0xff0100

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 119
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHandlesSelectPaint:Landroid/graphics/Paint;

    const/16 v5, 0x26

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 121
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRectsPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    const v4, 0x7f020214

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLeftHandleDrawable:Landroid/graphics/drawable/Drawable;

    .line 124
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLeftHandleDrawable:Landroid/graphics/drawable/Drawable;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v0, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 126
    const v4, 0x7f020215

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRightHandleDrawable:Landroid/graphics/drawable/Drawable;

    .line 127
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRightHandleDrawable:Landroid/graphics/drawable/Drawable;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v0, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 129
    iput-object p3, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    .line 130
    return-void
.end method

.method static synthetic access$200()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIdentityMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private maybeShowPopup()V
    .locals 2

    .prologue
    .line 321
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHasHandles:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsSelectionLocked:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    if-nez v0, :cond_1

    .line 323
    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    iget v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTextRectsValidForRequestId:I

    if-ne v0, v1, :cond_1

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->setLocationReady()V

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->maybeShow()V

    .line 330
    :cond_1
    return-void
.end method

.method private selectHandle(FF)Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;
    .locals 5
    .param p1, "eventX"    # F
    .param p2, "eventY"    # F

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 298
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->hitTest(FF)I

    move-result v1

    .line 299
    .local v1, "startHit":I
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->hitTest(FF)I

    move-result v0

    .line 300
    .local v0, "endHit":I
    if-ne v0, v3, :cond_0

    .line 301
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    .line 308
    :goto_0
    return-object v2

    .line 302
    :cond_0
    if-ne v1, v3, :cond_1

    .line 303
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    goto :goto_0

    .line 304
    :cond_1
    if-ne v0, v4, :cond_2

    .line 305
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    goto :goto_0

    .line 306
    :cond_2
    if-ne v1, v4, :cond_3

    .line 307
    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    goto :goto_0

    .line 308
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setupHandle(Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;Landroid/graphics/drawable/Drawable;III)V
    .locals 1
    .param p1, "handle"    # Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "anchorX"    # I
    .param p4, "anchorY"    # I
    .param p5, "rotation"    # I

    .prologue
    .line 386
    iput-object p2, p1, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 387
    int-to-float v0, p5

    iput v0, p1, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotationAngle:F

    .line 388
    iget-object v0, p1, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mAnchorPoint:Landroid/graphics/Point;

    invoke-virtual {v0, p3, p4}, Landroid/graphics/Point;->set(II)V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLeftHandleDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p1, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mIsLeftDrawable:Z

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->applyTransform(Landroid/graphics/Matrix;)V

    .line 392
    return-void

    .line 389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public dragSelection(Landroid/view/MotionEvent;IILcom/google/android/apps/books/app/SelectionState;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "passageIndex"    # I
    .param p3, "pageIndex"    # I
    .param p4, "selectionState"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 222
    const/4 v0, 0x2

    new-array v11, v0, [F

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    aput v1, v11, v0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    aput v1, v11, v0

    .line 225
    .local v11, "point":[F
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mViewtoPageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 227
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    .line 228
    .local v7, "action":I
    const/4 v0, 0x0

    aget v0, v11, v0

    float-to-int v8, v0

    .line 229
    .local v8, "eventX":I
    const/4 v0, 0x1

    aget v0, v11, v0

    float-to-int v9, v0

    .line 230
    .local v9, "eventY":I
    const/4 v10, 0x0

    .line 232
    .local v10, "handled":Z
    packed-switch v7, :pswitch_data_0

    .line 285
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    if-eqz v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->hide()V

    .line 289
    :cond_1
    return v10

    .line 234
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    if-eqz v0, :cond_0

    .line 236
    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mDraggingFirstHandle:Z

    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sub-int v3, v8, v0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int v4, v9, v0

    const/4 v5, -0x1

    const/4 v6, 0x1

    move-object/from16 v0, p4

    move v1, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/app/SelectionState;->moveSelectionHandle(IZIIIZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    .line 240
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->maybeShowPopup()V

    .line 241
    const/4 v10, 0x1

    goto :goto_0

    .line 245
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    .line 246
    const/4 v10, 0x1

    .line 247
    goto :goto_0

    .line 254
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHidingHandles:Z

    if-nez v0, :cond_0

    .line 255
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    .line 256
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay;->selectHandle(FF)Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    move-result-object v12

    .line 257
    .local v12, "selected":Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    if-ne v0, v12, :cond_2

    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mDraggingFirstHandle:Z

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Point;->set(II)V

    .line 261
    const/4 v10, 0x1

    goto :goto_0

    .line 262
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    if-ne v0, v12, :cond_3

    .line 263
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mDraggingFirstHandle:Z

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Point;->set(II)V

    .line 266
    const/4 v10, 0x1

    goto :goto_0

    .line 268
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    .line 269
    const/4 v10, 0x0

    goto :goto_0

    .line 274
    .end local v12    # "selected":Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastTouchPos:Landroid/graphics/Point;

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Point;->equals(II)Z

    move-result v0

    if-nez v0, :cond_4

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastTouchPos:Landroid/graphics/Point;

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Point;->set(II)V

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sub-int v3, v8, v0

    .line 278
    .local v3, "deltaX":I
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTapStart:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int v4, v9, v0

    .line 279
    .local v4, "deltaY":I
    iget-boolean v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mDraggingFirstHandle:Z

    iget v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    const/4 v6, 0x0

    move-object/from16 v0, p4

    move v1, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/app/SelectionState;->moveSelectionHandle(IZIIIZ)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLastRangeDataRequestId:I

    .line 282
    .end local v3    # "deltaX":I
    .end local v4    # "deltaY":I
    :cond_4
    iget-boolean v10, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsTapping:Z

    goto/16 :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public getSelectionBounds()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 333
    new-instance v1, Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRegionDrawer:Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;

    invoke-virtual {v5}, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 334
    .local v1, "pageCoordBounds":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 335
    .local v4, "viewCoordBoundsF":Landroid/graphics/RectF;
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v4, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 336
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 337
    .local v3, "viewCoordBounds":Landroid/graphics/Rect;
    invoke-virtual {v4, v3}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 341
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v5, v5, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x9

    div-int/lit8 v0, v5, 0x4

    .line 343
    .local v0, "handleLongerAxis":I
    const/4 v5, 0x2

    new-array v2, v5, [F

    iget-object v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v5, v5, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mAnchorPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    aput v5, v2, v7

    iget-object v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v5, v5, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mAnchorPoint:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    aput v5, v2, v8

    .line 344
    .local v2, "points":[F
    iget-object v5, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 346
    new-instance v5, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    new-instance v6, Landroid/graphics/Point;

    aget v7, v2, v7

    float-to-int v7, v7

    aget v8, v2, v8

    float-to-int v8, v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {v5, v3, v0, v6}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;-><init>(Landroid/graphics/Rect;ILandroid/graphics/Point;)V

    return-object v5
.end method

.method public hideSelectionHandles()V
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHidingHandles:Z

    if-nez v0, :cond_0

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHidingHandles:Z

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->invalidate()V

    .line 200
    :cond_0
    return-void
.end method

.method public isSelectionLocked()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsSelectionLocked:Z

    return v0
.end method

.method public lockSelection()V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mIsSelectionLocked:Z

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->hideSelectionHandles()V

    .line 210
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 172
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRegionDrawer:Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRectsPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHasHandles:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHidingHandles:Z

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHandlesSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHandlesSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 188
    :cond_0
    return-void
.end method

.method public removeFromParent()V
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/SelectionPopup;->hide()V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 139
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 142
    :cond_0
    return-void
.end method

.method public setHandlesRects(Lcom/google/android/apps/books/widget/Walker;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "i":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    const/4 v13, 0x1

    const/16 v5, -0x5a

    const/4 v11, 0x0

    .line 351
    iget-object v12, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPreallocatedRect:Landroid/graphics/Rect;

    .line 352
    .local v12, "h":Landroid/graphics/Rect;
    invoke-interface {p1, v12}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 372
    :cond_0
    :goto_0
    return v11

    .line 355
    :cond_1
    iget v0, v12, Landroid/graphics/Rect;->top:I

    iget v1, v12, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_2

    .line 356
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRightHandleDrawable:Landroid/graphics/drawable/Drawable;

    iget v3, v12, Landroid/graphics/Rect;->right:I

    iget v4, v12, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setupHandle(Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;Landroid/graphics/drawable/Drawable;III)V

    .line 361
    :goto_1
    invoke-interface {p1, v12}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    iget v0, v12, Landroid/graphics/Rect;->top:I

    iget v1, v12, Landroid/graphics/Rect;->bottom:I

    if-ne v0, v1, :cond_3

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLeftHandleDrawable:Landroid/graphics/drawable/Drawable;

    iget v3, v12, Landroid/graphics/Rect;->right:I

    iget v4, v12, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setupHandle(Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;Landroid/graphics/drawable/Drawable;III)V

    .line 370
    :goto_2
    iput-boolean v13, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHasHandles:Z

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->invalidate()V

    move v11, v13

    .line 372
    goto :goto_0

    .line 358
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mLeftHandleDrawable:Landroid/graphics/drawable/Drawable;

    iget v9, v12, Landroid/graphics/Rect;->left:I

    iget v10, v12, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setupHandle(Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;Landroid/graphics/drawable/Drawable;III)V

    goto :goto_1

    .line 367
    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v8, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRightHandleDrawable:Landroid/graphics/drawable/Drawable;

    iget v9, v12, Landroid/graphics/Rect;->right:I

    iget v10, v12, Landroid/graphics/Rect;->bottom:I

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/books/widget/SelectionOverlay;->setupHandle(Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;Landroid/graphics/drawable/Drawable;III)V

    goto :goto_2
.end method

.method public setPageToViewMatrix(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "pageToView"    # Landroid/graphics/Matrix;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mViewtoPageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mViewtoPageMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 155
    const-string v0, "SelectionOverlay"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "SelectionOverlay"

    const-string v1, "setPageToViewMatrix can\'t inverse matrix"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHasHandles:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mHidingHandles:Z

    if-nez v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mStartHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->applyTransform(Landroid/graphics/Matrix;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mEndHandle:Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mPageToViewMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->applyTransform(Landroid/graphics/Matrix;)V

    .line 164
    :cond_1
    return-void
.end method

.method public setTextRects(Lcom/google/android/apps/books/widget/Walker;I)V
    .locals 1
    .param p2, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 312
    .local p1, "textPoints":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mRegionDrawer:Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/SelectionOverlay$RegionDrawer;->setRects(Lcom/google/android/apps/books/widget/Walker;)V

    .line 313
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->invalidate()V

    .line 315
    iput p2, p0, Lcom/google/android/apps/books/widget/SelectionOverlay;->mTextRectsValidForRequestId:I

    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay;->maybeShowPopup()V

    .line 317
    return-void
.end method
