.class Lcom/google/android/apps/books/data/UploadsControllerImpl$11;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->fetchUploads(Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 384
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsFetched:Z
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$600(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 385
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/books/upload/UploadDataStorage;->get()Lcom/google/android/apps/books/upload/Upload$Uploads;

    move-result-object v4

    .line 387
    .local v4, "uploads":Lcom/google/android/apps/books/upload/Upload$Uploads;
    invoke-virtual {v4}, Lcom/google/android/apps/books/upload/Upload$Uploads;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/upload/Upload;

    .line 389
    .local v2, "upload":Lcom/google/android/apps/books/upload/Upload;
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/apps/books/upload/Upload;->getFileSize()I

    move-result v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/upload/UploadDataStorage;->fileExists(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 391
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->deleteUpload(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 398
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    invoke-virtual {v2}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v0, v7}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->abortUpload(Ljava/lang/String;Ljava/lang/Exception;Z)V

    goto :goto_0

    .line 394
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadManager:Lcom/google/android/apps/books/upload/UploadManager;
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$100(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadManager;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v7, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;
    invoke-static {v7}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;

    move-result-object v7

    invoke-interface {v5, v6, v2, v7}, Lcom/google/android/apps/books/upload/UploadManager;->getUploader(Lcom/google/android/apps/books/upload/SingleBookUploader$Callbacks;Lcom/google/android/apps/books/upload/Upload;Lcom/google/android/apps/books/upload/UploadDataStorage;)Lcom/google/android/apps/books/upload/SingleBookUploader;

    move-result-object v3

    .line 396
    .local v3, "uploader":Lcom/google/android/apps/books/upload/SingleBookUploader;
    invoke-interface {v3}, Lcom/google/android/apps/books/upload/SingleBookUploader;->startUpload()V

    .line 397
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/apps/books/upload/Upload;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 402
    .end local v2    # "upload":Lcom/google/android/apps/books/upload/Upload;
    .end local v3    # "uploader":Lcom/google/android/apps/books/upload/SingleBookUploader;
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$400(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/concurrent/Executor;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;

    invoke-direct {v6, p0, v4}, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$1;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$11;Lcom/google/android/apps/books/upload/Upload$Uploads;)V

    invoke-interface {v5, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 420
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    const/4 v6, 0x1

    # setter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadsFetched:Z
    invoke-static {v5, v6}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$602(Lcom/google/android/apps/books/data/UploadsControllerImpl;Z)Z

    .line 431
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "uploads":Lcom/google/android/apps/books/upload/Upload$Uploads;
    :cond_3
    :goto_1
    return-void

    .line 421
    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    if-eqz v5, :cond_3

    .line 424
    iget-object v5, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$11;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v5}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$400(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/concurrent/Executor;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/data/UploadsControllerImpl$11$2;-><init>(Lcom/google/android/apps/books/data/UploadsControllerImpl$11;)V

    invoke-interface {v5, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
