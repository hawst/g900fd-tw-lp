.class public interface abstract Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;
.super Ljava/lang/Object;
.source "BooksSafeFetchController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/BooksSafeFetchController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EnsureOperation"
.end annotation


# virtual methods
.method public abstract ensure()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation
.end method
