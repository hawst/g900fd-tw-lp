.class Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadSpreadPageRequest"
.end annotation


# instance fields
.field final passageIndex:I

.field final position:Ljava/lang/String;

.field final spreadOffset:I

.field final spreadPageIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/lang/String;III)V
    .locals 6
    .param p2, "passageIndex"    # I
    .param p3, "position"    # Ljava/lang/String;
    .param p4, "spreadOffset"    # I
    .param p5, "spreadPageIndex"    # I
    .param p6, "requestId"    # I

    .prologue
    .line 1382
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1383
    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p6

    move v3, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1384
    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->passageIndex:I

    .line 1385
    iput-object p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->position:Ljava/lang/String;

    .line 1386
    iput p4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->spreadOffset:I

    .line 1387
    iput p5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->spreadPageIndex:I

    .line 1388
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1392
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1393
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Issuing LoadSpreadPageRequest("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->passageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->position:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->spreadOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->spreadPageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->mTaskRequestId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 1397
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->passageIndex:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->position:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->spreadOffset:I

    iget v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->spreadPageIndex:I

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadSpreadPageRequest;->mTaskRequestId:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/TextReader;->loadSpreadPage(ILjava/lang/String;III)V

    .line 1399
    return-void
.end method
