.class Lcom/google/android/apps/books/app/OnboardingController$1;
.super Ljava/lang/Object;
.source "OnboardingController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OnboardingController;->getSampleCategories(Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OnboardingController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/apps/books/app/OnboardingController$1;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    iput-object p2, p0, Lcom/google/android/apps/books/app/OnboardingController$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 70
    .local v0, "categories":Lcom/google/android/apps/books/api/data/SampleCategories;
    const/4 v2, 0x0

    .line 72
    .local v2, "exception":Ljava/io/IOException;
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/OnboardingController$1;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    # getter for: Lcom/google/android/apps/books/app/OnboardingController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {v4}, Lcom/google/android/apps/books/app/OnboardingController;->access$000(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/net/BooksServer;->getSampleCategories(Ljava/util/Locale;)Lcom/google/android/apps/books/api/data/SampleCategories;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 78
    :goto_0
    if-eqz v2, :cond_0

    .line 79
    invoke-static {v2}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    .line 84
    .local v3, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;>;>;"
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/OnboardingController$1;->this$0:Lcom/google/android/apps/books/app/OnboardingController;

    # getter for: Lcom/google/android/apps/books/app/OnboardingController;->mUiExecutor:Lcom/google/android/apps/books/util/HandlerExecutor;
    invoke-static {v4}, Lcom/google/android/apps/books/app/OnboardingController;->access$100(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/app/OnboardingController$1$1;

    invoke-direct {v5, p0, v3}, Lcom/google/android/apps/books/app/OnboardingController$1$1;-><init>(Lcom/google/android/apps/books/app/OnboardingController$1;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 100
    return-void

    .line 73
    .end local v3    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;>;>;"
    :catch_0
    move-exception v1

    .line 74
    .local v1, "e":Ljava/io/IOException;
    move-object v2, v1

    goto :goto_0

    .line 81
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    iget-object v4, v0, Lcom/google/android/apps/books/api/data/SampleCategories;->categories:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v3

    .restart local v3    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;>;>;"
    goto :goto_1
.end method
