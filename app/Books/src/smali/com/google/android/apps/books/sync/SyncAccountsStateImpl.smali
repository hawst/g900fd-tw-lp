.class public Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;
.super Ljava/lang/Object;
.source "SyncAccountsStateImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/sync/SyncAccountsState;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPrefFile:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    const-string v0, "SyncAccounts"

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefFile"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-string v0, "missing context"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v0, "missing prefFile"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    iput-object p1, p0, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->mPrefFile:Ljava/lang/String;

    .line 56
    return-void
.end method

.method private getMyEbooksTimeKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "myEbooksTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPrefs()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->mPrefFile:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private getSyncNagKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "syncNag:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSyncTimeKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "syncTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 141
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 142
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 143
    return-void
.end method

.method public getLastMyEbooksFetchTime(Ljava/lang/String;)J
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getMyEbooksTimeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastSyncTime(Ljava/lang/String;J)J
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "defaultIfNotExist"    # J

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getSyncTimeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public haveNagged(Ljava/lang/String;)Z
    .locals 3
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getSyncNagKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setHaveNagged(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 130
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getSyncNagKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 131
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 132
    return-void
.end method

.method public setLastMyEbooksFetchTime(Ljava/lang/String;J)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "fetchTime"    # J

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 88
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getMyEbooksTimeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 89
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 90
    return-void
.end method

.method public setLastSyncTime(Ljava/lang/String;J)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "syncTime"    # J

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getPrefs()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 107
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;->getSyncTimeKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 109
    return-void
.end method
