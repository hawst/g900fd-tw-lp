.class public interface abstract Lcom/google/android/apps/books/render/RendererListener;
.super Ljava/lang/Object;
.source "RendererListener.java"


# virtual methods
.method public abstract onActivatedMoElement(IILjava/lang/String;)V
.end method

.method public abstract onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
.end method

.method public abstract onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
.end method

.method public abstract onNewSelectionBegins()V
.end method

.method public abstract onPagesNeedRedraw()V
.end method

.method public abstract onPassageMoListReady(IILjava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
.end method

.method public abstract onPassagesPurged(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onRenderError(Ljava/lang/Exception;)V
.end method

.method public abstract onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
.end method
