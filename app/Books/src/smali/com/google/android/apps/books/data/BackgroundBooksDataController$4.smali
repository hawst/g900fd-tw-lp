.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$action:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$action:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$action:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$action:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-ne v0, v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$volumeId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->releaseVolumeLicense(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$500(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$600(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/annotations/AnnotationController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$volumeId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationController;->removeLocalVolumeAnnotationsForVolume(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$300(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->val$volumeId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/BooksDataStore;->deleteContent(Ljava/lang/String;)V

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$4;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$700(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;->notifyContentChanged()V

    .line 277
    const-string v0, "BgDataController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    const-string v0, "BgDataController"

    const-string v1, "notifyContentChanged: true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_1
    return-void
.end method
