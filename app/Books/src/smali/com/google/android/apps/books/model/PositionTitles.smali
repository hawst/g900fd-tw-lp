.class public interface abstract Lcom/google/android/apps/books/model/PositionTitles;
.super Ljava/lang/Object;
.source "PositionTitles.java"


# virtual methods
.method public abstract getChapterTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method

.method public abstract getPageTitle(Lcom/google/android/apps/books/common/Position;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation
.end method
