.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->uiUpdateLastUsedTimestampToNow(Lcom/google/android/apps/books/annotations/Annotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

.field final synthetic val$annotation:Lcom/google/android/apps/books/annotations/Annotation;

.field final synthetic val$timestamp:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Lcom/google/android/apps/books/annotations/Annotation;J)V
    .locals 1

    .prologue
    .line 289
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;->val$annotation:Lcom/google/android/apps/books/annotations/Annotation;

    iput-wide p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;->val$timestamp:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;->val$annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$4;->val$timestamp:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->setLastUsedTimestamp(Ljava/lang/String;J)V

    .line 292
    return-void
.end method
