.class Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FootnoteCardCallbacks"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 5557
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/ReaderFragment;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/ReaderFragment$1;

    .prologue
    .line 5557
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;)V

    return-void
.end method


# virtual methods
.method public disallowVerticalCardSwipes()V
    .locals 2

    .prologue
    .line 5575
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 5576
    return-void
.end method

.method public getCardsState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;
    .locals 1

    .prologue
    .line 5564
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->getCardsState()Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;

    move-result-object v0

    return-object v0
.end method

.method public showInBook(Lcom/google/android/apps/books/common/Position;)V
    .locals 1
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 5560
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$FootnoteCardCallbacks;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->navigateToLinkPosition(Lcom/google/android/apps/books/common/Position;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12700(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/common/Position;)V

    .line 5561
    return-void
.end method
