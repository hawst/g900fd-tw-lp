.class Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;
.super Lcom/google/android/apps/books/app/AppSettingsCheckableItem;
.source "AppSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AutoReadAloud"
.end annotation


# instance fields
.field private final mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

.field final synthetic this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/AppSettingsActivity;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;->this$0:Lcom/google/android/apps/books/app/AppSettingsActivity;

    .line 251
    const v0, 0x7f0f015a

    const v1, 0x7f0f015b

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/AppSettingsCheckableItem;-><init>(II)V

    .line 252
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    .line 253
    return-void
.end method


# virtual methods
.method protected getValue()Z
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getAutoReadAloud()Z

    move-result v0

    return v0
.end method

.method protected setValue(Z)V
    .locals 3
    .param p1, "autoReadAloud"    # Z

    .prologue
    .line 262
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;->AUTO_READ_ALOUD:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHomeSettingChoice(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$HomeSettingsOption;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/books/app/AppSettingsActivity$AutoReadAloud;->mPrefs:Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAutoReadAloud(Z)V

    .line 265
    return-void
.end method
