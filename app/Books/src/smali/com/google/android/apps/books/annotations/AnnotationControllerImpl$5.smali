.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->fetchLayers(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$toLoad:Ljava/util/List;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Lcom/google/android/apps/books/annotations/VolumeVersion;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->val$toLoad:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 323
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->val$toLoad:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 324
    .local v1, "layer":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    invoke-interface {v3, v4, v1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->load(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 325
    .local v2, "loaded":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    new-instance v5, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5$1;

    invoke-direct {v5, p0, v1, v2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$5;Ljava/lang/String;Ljava/util/List;)V

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->maybeWithForegroundCache(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$100(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;)V

    goto :goto_0

    .line 332
    .end local v1    # "layer":Ljava/lang/String;
    .end local v2    # "loaded":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    return-void
.end method
