.class Lcom/google/android/apps/books/app/UserSettingsController$2;
.super Ljava/lang/Object;
.source "UserSettingsController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/UserSettingsController;->updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/UserSettingsController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$newUserSettings:Lcom/google/android/apps/books/api/data/UserSettings;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/UserSettingsController;Lcom/google/android/apps/books/api/data/UserSettings;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/books/app/UserSettingsController$2;->this$0:Lcom/google/android/apps/books/app/UserSettingsController;

    iput-object p2, p0, Lcom/google/android/apps/books/app/UserSettingsController$2;->val$newUserSettings:Lcom/google/android/apps/books/api/data/UserSettings;

    iput-object p3, p0, Lcom/google/android/apps/books/app/UserSettingsController$2;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 82
    const/4 v3, 0x0

    .line 83
    .local v3, "userSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    const/4 v1, 0x0

    .line 85
    .local v1, "exception":Ljava/io/IOException;
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/UserSettingsController$2;->this$0:Lcom/google/android/apps/books/app/UserSettingsController;

    # getter for: Lcom/google/android/apps/books/app/UserSettingsController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;
    invoke-static {v4}, Lcom/google/android/apps/books/app/UserSettingsController;->access$000(Lcom/google/android/apps/books/app/UserSettingsController;)Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/UserSettingsController$2;->val$newUserSettings:Lcom/google/android/apps/books/api/data/UserSettings;

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/net/BooksServer;->updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;)Lcom/google/android/apps/books/api/data/UserSettings;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 90
    :goto_0
    if-eqz v1, :cond_0

    .line 91
    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .line 96
    .local v2, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;"
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/UserSettingsController$2;->this$0:Lcom/google/android/apps/books/app/UserSettingsController;

    # getter for: Lcom/google/android/apps/books/app/UserSettingsController;->mUiExecutor:Ljava/util/concurrent/Executor;
    invoke-static {v4}, Lcom/google/android/apps/books/app/UserSettingsController;->access$100(Lcom/google/android/apps/books/app/UserSettingsController;)Ljava/util/concurrent/Executor;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/app/UserSettingsController$2$1;

    invoke-direct {v5, p0, v2}, Lcom/google/android/apps/books/app/UserSettingsController$2$1;-><init>(Lcom/google/android/apps/books/app/UserSettingsController$2;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 102
    return-void

    .line 86
    .end local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;"
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/IOException;
    move-object v1, v0

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-static {v3}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v2

    .restart local v2    # "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;"
    goto :goto_1
.end method
