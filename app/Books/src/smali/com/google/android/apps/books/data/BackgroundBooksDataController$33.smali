.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->deliverExceptionOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$exception:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Lcom/google/android/ublib/utils/Consumer;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 1086
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;->val$exception:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 2
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$33;->val$exception:Ljava/lang/Exception;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeFailure(Ljava/lang/Exception;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 1090
    return-void
.end method
