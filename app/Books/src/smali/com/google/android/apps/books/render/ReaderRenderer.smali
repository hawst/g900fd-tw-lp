.class public abstract Lcom/google/android/apps/books/render/ReaderRenderer;
.super Ljava/lang/Object;
.source "ReaderRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/render/Renderer;
.implements Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;


# instance fields
.field private volatile mDestroyed:Z

.field private mListener:Lcom/google/android/apps/books/render/RendererListener;

.field private volatile mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

.field private volatile mSequenceNumber:I


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-boolean v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mDestroyed:Z

    .line 50
    iput v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    .line 56
    const-string v0, "missing metadata"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/VolumeMetadata;

    iput-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/ReaderRenderer;)Lcom/google/android/apps/books/render/RendererListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ReaderRenderer;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/ReaderRenderer;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ReaderRenderer;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/ReaderRenderer;->shouldHonorRenderRequest(I)Z

    move-result v0

    return v0
.end method

.method protected static paintDebugInfo(Landroid/graphics/Canvas;Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderPosition;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 14
    .param p0, "canvas"    # Landroid/graphics/Canvas;
    .param p1, "requested"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p2, "found"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p3, "cookie"    # Ljava/lang/Object;
    .param p4, "readerTheme"    # Ljava/lang/String;

    .prologue
    .line 215
    const/16 v7, 0x32

    .line 216
    .local v7, "TOP":I
    invoke-virtual {p0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v13

    .line 218
    .local v13, "width":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 219
    .local v6, "paint":Landroid/graphics/Paint;
    const v8, -0xbbbbbc

    .line 220
    .local v8, "color":I
    const/16 v1, 0xdf

    const v2, -0xbbbbbc

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    const v3, -0xbbbbbc

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    const v4, -0xbbbbbc

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 222
    const/4 v2, 0x0

    const/high16 v3, 0x42480000    # 50.0f

    int-to-float v4, v13

    const/high16 v5, 0x43020000    # 130.0f

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 224
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 225
    .local v12, "textPaint":Landroid/graphics/Paint;
    const/high16 v1, -0x10000

    invoke-virtual {v12, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 226
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v12, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 227
    const/4 v1, 0x1

    invoke-virtual {v12, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requested: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/RenderPosition;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 230
    .local v9, "line1":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/books/render/RenderPosition;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 231
    .local v10, "line2":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "consumer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 233
    .local v11, "line3":Ljava/lang/String;
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x428c0000    # 70.0f

    invoke-virtual {p0, v9, v1, v2, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 234
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x42be0000    # 95.0f

    invoke-virtual {p0, v10, v1, v2, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 235
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x42f00000    # 120.0f

    invoke-virtual {p0, v11, v1, v2, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 236
    return-void
.end method

.method private shouldHonorRenderRequest(I)Z
    .locals 1
    .param p1, "sequenceNumber"    # I

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/ReaderRenderer;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public beginSelection(FFLcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p4, "ppos"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 241
    return-void
.end method

.method public canFitWidth()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    return v0
.end method

.method public canImmediatelyRedrawHighlights()Z
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    return v0
.end method

.method public cancelPendingRequests()V
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    .line 251
    return-void
.end method

.method public clearAnnotationCaches()V
    .locals 0

    .prologue
    .line 291
    return-void
.end method

.method public createHighlightsRectsCache(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)Lcom/google/android/apps/books/widget/HighlightsRectsCache;
    .locals 6
    .param p2, "loader"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
    .param p3, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            ")",
            "Lcom/google/android/apps/books/widget/HighlightsRectsCache;"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/apps/books/widget/HighlightsRectsCache;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/HighlightsRectsCache;-><init>(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;Lcom/google/android/apps/books/widget/HighlightsRectsCache$RenderCallbacks;)V

    return-object v0
.end method

.method protected createSideLoadFilePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mDestroyed:Z

    .line 63
    return-void
.end method

.method protected dispatchListenerCallback(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 150
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    .line 151
    .local v0, "sequenceNumber":I
    new-instance v1, Lcom/google/android/apps/books/render/ReaderRenderer$4;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/books/render/ReaderRenderer$4;-><init>(Lcom/google/android/apps/books/render/ReaderRenderer;ILjava/lang/Runnable;)V

    invoke-static {v1}, Lcom/google/android/apps/books/util/UiThread;->run(Ljava/lang/Runnable;)V

    .line 162
    return-void
.end method

.method protected dispatchNewSelectionBegins()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/RendererListener;->onNewSelectionBegins()V

    .line 207
    :cond_0
    return-void
.end method

.method protected dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p2, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 173
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/books/render/RenderResponseConsumer;->isPurgeable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    .line 177
    .local v0, "sequenceNumber":I
    new-instance v1, Lcom/google/android/apps/books/render/ReaderRenderer$5;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/google/android/apps/books/render/ReaderRenderer$5;-><init>(Lcom/google/android/apps/books/render/ReaderRenderer;Lcom/google/android/apps/books/render/RenderResponseConsumer;ILjava/lang/Runnable;)V

    invoke-static {v1}, Lcom/google/android/apps/books/util/UiThread;->run(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected dispatchRenderError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/Exception;

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/apps/books/render/ReaderRenderer$3;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/render/ReaderRenderer$3;-><init>(Lcom/google/android/apps/books/render/ReaderRenderer;Ljava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/render/ReaderRenderer;->dispatchListenerCallback(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method protected dispatchRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 1
    .param p1, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p3, "painter"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/apps/books/render/ReaderRenderer$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/google/android/apps/books/render/ReaderRenderer$1;-><init>(Lcom/google/android/apps/books/render/ReaderRenderer;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/books/render/ReaderRenderer;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    .line 108
    return-void
.end method

.method protected dispatchSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V
    .locals 1
    .param p3, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "handleRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    .local p2, "textRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/RendererListener;->onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V

    .line 195
    :cond_0
    return-void
.end method

.method protected dispatchSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
    .locals 1
    .param p1, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/RendererListener;->onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V

    .line 201
    :cond_0
    return-void
.end method

.method protected dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    .locals 1
    .param p1, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p2, "specialPage"    # Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    .prologue
    .line 118
    new-instance v0, Lcom/google/android/apps/books/render/ReaderRenderer$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/books/render/ReaderRenderer$2;-><init>(Lcom/google/android/apps/books/render/ReaderRenderer;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/render/ReaderRenderer;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    .line 124
    return-void
.end method

.method protected getMetadata()Lcom/google/android/apps/books/model/VolumeMetadata;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    return-object v0
.end method

.method public getPaginationResultFor(I)Lcom/google/android/apps/books/util/PassagePages;
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSequenceNumber()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mSequenceNumber:I

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mDestroyed:Z

    return v0
.end method

.method public loadHighlightRectsForQuery(Ljava/lang/String;Lcom/google/android/apps/books/widget/DevicePageRendering;)I
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 311
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "ReaderRenderer#loadHighlightRectsForQuery"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public needsBackgroundAnnotationLoad()Z
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x1

    return v0
.end method

.method public onAccessibleSelectionChanged(IILcom/google/android/apps/books/widget/DevicePageRendering;)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "rendering"    # Lcom/google/android/apps/books/widget/DevicePageRendering;

    .prologue
    .line 246
    return-void
.end method

.method public onNewAnnotationRect(Lcom/google/android/apps/books/render/LabeledRect;)V
    .locals 0
    .param p1, "annotationRect"    # Lcom/google/android/apps/books/render/LabeledRect;

    .prologue
    .line 296
    return-void
.end method

.method public setCurrentPageRange(Lcom/google/android/apps/books/render/Renderer$PageRange;)V
    .locals 0
    .param p1, "range"    # Lcom/google/android/apps/books/render/Renderer$PageRange;

    .prologue
    .line 269
    return-void
.end method

.method public setHighlightsRectsCache(Lcom/google/android/apps/books/widget/PaintableRectsCache;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache",
            "<+",
            "Lcom/google/android/apps/books/widget/WalkableHighlightRects;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "highlightsRectsCache":Lcom/google/android/apps/books/widget/PaintableRectsCache;, "Lcom/google/android/apps/books/widget/PaintableRectsCache<+Lcom/google/android/apps/books/widget/WalkableHighlightRects;>;"
    return-void
.end method

.method public setRenderListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/apps/books/render/ReaderRenderer;->mListener:Lcom/google/android/apps/books/render/RendererListener;

    .line 77
    return-void
.end method

.method public shouldFitWidth()Z
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x0

    return v0
.end method

.method public weaklyAddWebLoadListener(Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .prologue
    .line 307
    return-void
.end method
