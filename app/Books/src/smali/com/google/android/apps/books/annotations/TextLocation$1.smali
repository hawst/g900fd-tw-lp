.class final Lcom/google/android/apps/books/annotations/TextLocation$1;
.super Ljava/lang/Object;
.source "TextLocation.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/TextLocation;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/annotations/TextLocation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$pageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/common/Position$PageOrdering;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/TextLocation$1;->val$pageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)I
    .locals 1
    .param p1, "lhs"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .param p2, "rhs"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/TextLocation$1;->val$pageOrdering:Lcom/google/android/apps/books/common/Position$PageOrdering;

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/books/annotations/TextLocation;->compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 30
    check-cast p1, Lcom/google/android/apps/books/annotations/TextLocation;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/annotations/TextLocation;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/TextLocation$1;->compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)I

    move-result v0

    return v0
.end method
