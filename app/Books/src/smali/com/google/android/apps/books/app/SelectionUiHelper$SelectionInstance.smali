.class public Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SelectionInstance"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;
    }
.end annotation


# instance fields
.field private mAlreadyTornDown:Z

.field private mDividerMarginPixels:I

.field private mDividerThicknessPixels:I

.field private mFirstSelectionSetup:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHasSelection:Z

.field private mLeaveSystemUiShowingOnTearDown:Z

.field private mNoteAnimationTime:I

.field private mNoteEditText:Landroid/widget/EditText;

.field private mNoteEditorContainer:Landroid/view/View;

.field private mNoteEditorDialog:Landroid/app/Dialog;

.field private mNoteEditorFlicker:Landroid/view/View;

.field private mNoteEditorShield:Landroid/view/View;

.field private mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

.field private mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

.field private mNoteQuoteContainer:Landroid/view/View;

.field private mNoteQuoteMenuButton:Landroid/view/View;

.field private mNoteQuoteText:Landroid/widget/TextView;

.field private mRemoveHighlightButton:Landroid/widget/ImageButton;

.field private mRemovingHighlight:Z

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

.field private mSelectionPopupButtonPaddingPixels:I

.field private mSelectionPopupButtonPixels:I

.field final synthetic this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper;Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;)V
    .locals 1
    .param p2, "info"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    .line 201
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHandler:Landroid/os/Handler;

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mFirstSelectionSetup:Z

    .line 215
    iput-object p2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    .line 216
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->animateNoteToIcon()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->activateNoteEditor()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    .param p1, "x1"    # Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/content/Context;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->createNoteMenu(Landroid/content/Context;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mAlreadyTornDown:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorShield:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->removeHighlightFromSelectedText()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->removeHighlightForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->copySelectedText()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->shareSelectedText()V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNote()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->translateSelectedText()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->searchInBookSelectedText()V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    .param p1, "x1"    # Z

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->showNoteText(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->animateNoteOut()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->onDismissingNoteEditor()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;
    .param p1, "x1"    # Z

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->animateNoteIn(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private activateNoteEditor()V
    .locals 3

    .prologue
    .line 425
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 427
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 430
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 433
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_1

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 436
    :cond_1
    return-void
.end method

.method private addNewAnnotation(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 899
    if-eqz p2, :cond_0

    .line 900
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getAnnotationContextSearch()Lcom/google/android/apps/books/annotations/AnnotationContextSearch;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/apps/books/annotations/Updateables;->forPlaceholder(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/AnnotationContextSearch;)Lcom/google/android/apps/books/annotations/Updateable;

    move-result-object v0

    .line 902
    .local v0, "updateable":Lcom/google/android/apps/books/annotations/Updateable;, "Lcom/google/android/apps/books/annotations/Updateable<Lcom/google/android/apps/books/annotations/Annotation;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiAdd(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Updateable;)V

    .line 904
    .end local v0    # "updateable":Lcom/google/android/apps/books/annotations/Updateable;, "Lcom/google/android/apps/books/annotations/Updateable<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    return-void
.end method

.method private animateNoteIn(Z)V
    .locals 11
    .param p1, "makeEditable"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 657
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mAlreadyTornDown:Z

    if-eqz v7, :cond_0

    .line 728
    :goto_0
    return-void

    .line 662
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .line 667
    .local v6, "view":Landroid/view/View;
    const/4 v2, 0x0

    .line 674
    .local v2, "iconRect":Landroid/graphics/Rect;
    if-nez v2, :cond_1

    .line 676
    invoke-static {v10, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 679
    .local v3, "measureSpec":I
    invoke-virtual {v6, v3, v3}, Landroid/view/View;->measure(II)V

    .line 681
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 683
    .local v4, "noteHeight":I
    new-instance v1, Landroid/view/animation/AnimationSet;

    invoke-direct {v1, v9}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 685
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    int-to-float v7, v4

    invoke-direct {v5, v8, v8, v7, v8}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 686
    .local v5, "translate":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v1, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 688
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v7, 0x3e800000    # 0.25f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v0, v7, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 689
    .local v0, "alpha":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 691
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 697
    .end local v0    # "alpha":Landroid/view/animation/AlphaAnimation;
    .end local v3    # "measureSpec":I
    .end local v4    # "noteHeight":I
    .end local v5    # "translate":Landroid/view/animation/TranslateAnimation;
    :goto_1
    iget v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteAnimationTime:I

    int-to-long v8, v7

    invoke-virtual {v1, v8, v9}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 699
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;

    invoke-direct {v7, p0, p1, v6}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$10;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;ZLandroid/view/View;)V

    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 724
    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 726
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    .line 727
    invoke-virtual {v6, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 693
    .end local v1    # "animationSet":Landroid/view/animation/AnimationSet;
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-static {v6, v7, v2, v9}, Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory;->create(Landroid/view/View;Landroid/view/View;Landroid/graphics/Rect;Z)Landroid/view/animation/AnimationSet;

    move-result-object v1

    .restart local v1    # "animationSet":Landroid/view/animation/AnimationSet;
    goto :goto_1
.end method

.method private animateNoteOut()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 731
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorShield:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 761
    :goto_0
    return-void

    .line 735
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorShield:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 737
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .line 739
    .local v3, "view":Landroid/view/View;
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 740
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    iget v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteAnimationTime:I

    int-to-long v4, v4

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 742
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v6, v6, v6, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 743
    .local v2, "translate":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 745
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3e800000    # 0.25f

    invoke-direct {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 746
    .local v0, "alpha":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 748
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 750
    new-instance v4, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$11;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$11;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v1, v4}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 759
    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 760
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private animateNoteToIcon()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 614
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorShield:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 646
    :goto_0
    return-void

    .line 618
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorShield:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 620
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorFlicker:Landroid/view/View;

    invoke-static {v4, v3}, Lcom/google/android/ublib/view/ViewCompat;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 622
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    if-nez v4, :cond_1

    move-object v1, v3

    .line 625
    .local v1, "iconRect":Landroid/graphics/Rect;
    :goto_1
    if-nez v1, :cond_2

    .line 626
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    .line 622
    .end local v1    # "iconRect":Landroid/graphics/Rect;
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getMarginNoteScreenRect()Landroid/graphics/Rect;

    move-result-object v1

    goto :goto_1

    .line 631
    .restart local v1    # "iconRect":Landroid/graphics/Rect;
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .line 633
    .local v2, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-static {v2, v4, v1, v6}, Lcom/google/android/apps/books/util/ShrinkToIconAnimationFactory;->create(Landroid/view/View;Landroid/view/View;Landroid/graphics/Rect;Z)Landroid/view/animation/AnimationSet;

    move-result-object v0

    .line 635
    .local v0, "animationSet":Landroid/view/animation/AnimationSet;
    iget v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteAnimationTime:I

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 637
    new-instance v4, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$9;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$9;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 644
    const/4 v4, 0x2

    invoke-virtual {v2, v4, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 645
    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private askToRemoveHighlightForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 6
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 922
    new-instance v2, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$16;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$16;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 929
    .local v2, "okListener":Landroid/content/DialogInterface$OnClickListener;
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 930
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0f00ca

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0f00cb

    invoke-virtual {v3, v4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0f00cc

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 934
    .local v1, "d":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 935
    return-void
.end method

.method private copySelectedText()V
    .locals 3

    .prologue
    .line 973
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/ublib/utils/ClipboardUtils;->sendTextToClipboard(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 976
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->isClipboardCopyingLimited()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 977
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->addNewClipboardCopy()V

    .line 980
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_COPY:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTextSelectionAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;)V

    .line 981
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->onSelectedTextCopiedToClipboard()V

    .line 983
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 984
    .local v0, "context":Landroid/content/Context;
    const v1, 0x7f0f007f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 987
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->endSelection()V

    .line 988
    return-void
.end method

.method private createNoteMenu(Landroid/content/Context;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 764
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 765
    .local v10, "res":Landroid/content/res/Resources;
    new-instance v0, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorContainer:Landroid/view/View;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZLcom/google/android/ublib/view/SystemUi;)V

    .line 768
    .local v0, "popupMenu":Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v6

    .line 770
    .local v6, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-nez v6, :cond_0

    .line 771
    new-instance v9, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v9, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 772
    .local v9, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    invoke-virtual {v9}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHighlightColor()I

    move-result v8

    .line 777
    .end local v9    # "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    .local v8, "initialPrefColorIndex":I
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$12;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$12;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-static {p1, v1, v4, v8}, Lcom/google/android/apps/books/app/HighlightColorSelector;->createColorSelector(Landroid/content/Context;Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)Landroid/view/View;

    move-result-object v7

    .line 786
    .local v7, "colorSelector":Landroid/view/View;
    const-string v1, ""

    new-instance v2, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$13;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$13;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v0, v1, v4, v7, v2}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLandroid/view/View;Lcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 794
    const v1, 0x7f0f0079

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$14;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$14;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 802
    const v1, 0x7f0f007a

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$15;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$15;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/ublib/cardlib/layout/PlayPopupMenu$OnActionSelectedListener;)V

    .line 810
    return-object v0

    .line 774
    .end local v7    # "colorSelector":Landroid/view/View;
    .end local v8    # "initialPrefColorIndex":I
    :cond_0
    invoke-virtual {v6}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getMatchingPrefColor(I)I

    move-result v8

    .restart local v8    # "initialPrefColorIndex":I
    goto :goto_0
.end method

.method private editMarginNote(Lcom/google/android/apps/books/annotations/Annotation;Ljava/lang/String;)V
    .locals 2
    .param p1, "oldAnnotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "finalText"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-virtual {p1, p2}, Lcom/google/android/apps/books/annotations/Annotation;->withNewNote(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 856
    .local v0, "newAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiEdit(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 857
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->onAnnotationChanged(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 858
    return-void
.end method

.method private firstTimeSelectionSetup(Landroid/content/res/Resources;Landroid/view/View;)V
    .locals 9
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x0

    .line 1002
    iget-boolean v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mFirstSelectionSetup:Z

    if-eqz v7, :cond_0

    .line 1003
    iput-boolean v8, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mFirstSelectionSetup:Z

    .line 1004
    const v7, 0x7f090163

    invoke-virtual {p1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionPopupButtonPixels:I

    .line 1005
    const v7, 0x7f090166

    invoke-virtual {p1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionPopupButtonPaddingPixels:I

    .line 1007
    const v7, 0x7f090164

    invoke-virtual {p1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerMarginPixels:I

    .line 1008
    const v7, 0x7f090165

    invoke-virtual {p1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerThicknessPixels:I

    .line 1011
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mScreenWidth:I

    .line 1012
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v7, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mScreenHeight:I

    .line 1014
    new-instance v5, Lcom/google/android/apps/books/widget/ToolTipListener;

    invoke-direct {v5, v8}, Lcom/google/android/apps/books/widget/ToolTipListener;-><init>(Z)V

    .line 1017
    .local v5, "toolTipListener":Lcom/google/android/apps/books/widget/ToolTipListener;
    const v7, 0x7f0e01cd

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1018
    .local v1, "copyItem":Landroid/view/View;
    invoke-virtual {v1, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1019
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$17;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$17;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1028
    const v7, 0x7f0e01cf

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1029
    .local v4, "shareItem":Landroid/view/View;
    invoke-virtual {v4, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1030
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$18;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$18;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1039
    const v7, 0x7f0e01cc

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1040
    .local v0, "addNoteItem":Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1041
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$19;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$19;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1048
    const v7, 0x7f0e01ce

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1049
    .local v6, "translateMenuItem":Landroid/view/View;
    invoke-virtual {v6, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1050
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$20;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$20;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1059
    const v7, 0x7f0e01d2

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1060
    .local v2, "removeHighlightButton":Landroid/view/View;
    invoke-virtual {v2, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1061
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$21;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$21;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1070
    const v7, 0x7f0e01d0

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1071
    .local v3, "searchInBookItem":Landroid/view/View;
    invoke-virtual {v3, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1072
    new-instance v7, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$22;

    invoke-direct {v7, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$22;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1081
    const v7, 0x7f0e012b

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1083
    const v7, 0x7f0e012c

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1085
    const v7, 0x7f0e012d

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1087
    const v7, 0x7f0e012e

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1090
    .end local v0    # "addNoteItem":Landroid/view/View;
    .end local v1    # "copyItem":Landroid/view/View;
    .end local v2    # "removeHighlightButton":Landroid/view/View;
    .end local v3    # "searchInBookItem":Landroid/view/View;
    .end local v4    # "shareItem":Landroid/view/View;
    .end local v5    # "toolTipListener":Lcom/google/android/apps/books/widget/ToolTipListener;
    .end local v6    # "translateMenuItem":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getAnnotationController()Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->getForegroundAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v0

    return-object v0
.end method

.method private getNoteEditorDrawingColor()I
    .locals 3

    .prologue
    .line 827
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    if-eqz v1, :cond_0

    .line 828
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 829
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_0

    .line 830
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDrawingColors:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$2000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;->getClosestMatch(I)I

    move-result v1

    .line 833
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper;->getCurrentAndroidColor()I
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$400(Lcom/google/android/apps/books/app/SelectionUiHelper;)I

    move-result v1

    goto :goto_0
.end method

.method private getNoteEditorText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private logHighlightAddOrEdit(ZI)V
    .locals 3
    .param p1, "isEdit"    # Z
    .param p2, "androidColor"    # I

    .prologue
    .line 874
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 875
    .local v0, "selectedText":Ljava/lang/CharSequence;
    invoke-static {p2}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getMatchingPrefColor(I)I

    move-result v2

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-static {p1, v2, v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHighlightAddOrEdit(ZII)V

    .line 878
    return-void

    .line 875
    :cond_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    goto :goto_0
.end method

.method private onDismissingNoteEditor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 814
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getNoteEditorText()Ljava/lang/String;

    move-result-object v0

    .line 815
    .local v0, "finalText":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 816
    iput-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    .line 817
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemovingHighlight:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    if-eqz v1, :cond_0

    .line 818
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->stopEditingNote(Ljava/lang/String;)V

    .line 820
    :cond_0
    return-void
.end method

.method private removeHighlightForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 1
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 911
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logHighlightDeletion(Z)V

    .line 912
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiRemove(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 916
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemovingHighlight:Z

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 918
    return-void
.end method

.method private removeHighlightFromSelectedText()V
    .locals 3

    .prologue
    .line 938
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 939
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v0, :cond_2

    .line 940
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 941
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->askToRemoveHighlightForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 943
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->removeHighlightForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    goto :goto_0

    .line 946
    :cond_2
    const-string v1, "SelectionUiHelper"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 947
    const-string v1, "SelectionUiHelper"

    const-string v2, "Attempting to remove highlight without annotation"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private searchInBookSelectedText()V
    .locals 2

    .prologue
    .line 991
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 994
    .local v0, "selectedText":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->onSearchInBookRequested(Ljava/lang/CharSequence;)V

    .line 995
    return-void
.end method

.method private setSelectionToAndroidColor(I)V
    .locals 2
    .param p1, "androidColor"    # I

    .prologue
    .line 861
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    if-nez v1, :cond_0

    .line 871
    :goto_0
    return-void

    .line 865
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 866
    .local v0, "isEdit":Z
    :goto_1
    if-eqz v0, :cond_2

    .line 867
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->updateHighlight(I)V

    goto :goto_0

    .line 865
    .end local v0    # "isEdit":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 869
    .restart local v0    # "isEdit":Z
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getNoteEditorText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->addNewHighlight(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private shareSelectedText()V
    .locals 6

    .prologue
    .line 953
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 955
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 956
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 957
    const-string v3, "android.intent.extra.SUBJECT"

    const v4, 0x7f0f010b

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 958
    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v4}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v5}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getShareTextForQuote(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 960
    const-string v3, "text/plain"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 961
    const/high16 v3, 0x80000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 962
    invoke-static {v2}, Lcom/google/android/apps/books/app/BaseBooksActivity;->markExternalIntent(Landroid/content/Intent;)V

    .line 964
    const v3, 0x7f0f0108

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 966
    .local v1, "dialogTitle":Ljava/lang/CharSequence;
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_SHARE_QUOTE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    invoke-static {v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTextSelectionAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;)V

    .line 967
    invoke-static {v2, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 969
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->endSelection()V

    .line 970
    return-void
.end method

.method private showNoteText(Z)V
    .locals 0
    .param p1, "startEditing"    # Z

    .prologue
    .line 385
    if-eqz p1, :cond_0

    .line 386
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNote()V

    .line 390
    :goto_0
    return-void

    .line 388
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startViewingNote()V

    goto :goto_0
.end method

.method private startEditingNote()V
    .locals 1

    .prologue
    .line 397
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNoteAtCharOffset(I)V

    .line 398
    return-void
.end method

.method private startEditingNoteAtCharOffset(I)V
    .locals 1
    .param p1, "charOffset"    # I

    .prologue
    .line 421
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNoteAtCharOffset(IZ)V

    .line 422
    return-void
.end method

.method private startEditingNoteAtCharOffset(IZ)V
    .locals 15
    .param p1, "charOffset"    # I
    .param p2, "makeEditable"    # Z

    .prologue
    .line 440
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    if-eqz v12, :cond_1

    .line 607
    .end local p1    # "charOffset":I
    :cond_0
    :goto_0
    return-void

    .line 446
    .restart local p1    # "charOffset":I
    :cond_1
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    if-eqz v12, :cond_0

    .line 450
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 451
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_0

    .line 454
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 458
    .local v10, "res":Landroid/content/res/Resources;
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v12

    if-nez v12, :cond_2

    .line 459
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper;->getCurrentAndroidColor()I
    invoke-static {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$400(Lcom/google/android/apps/books/app/SelectionUiHelper;)I

    move-result v2

    .line 460
    .local v2, "androidColor":I
    const-string v12, ""

    invoke-virtual {p0, v2, v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->addNewHighlight(ILjava/lang/String;)V

    .line 463
    .end local v2    # "androidColor":I
    :cond_2
    const/4 v12, 0x1

    new-array v3, v12, [I

    const/4 v12, 0x0

    const v13, 0x7f010183

    aput v13, v3, v12

    .line 464
    .local v3, "attributeIds":[I
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v12

    invoke-virtual {v12, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 465
    .local v1, "a":Landroid/content/res/TypedArray;
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    .line 466
    .local v6, "noteEditorDialogTheme":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 468
    const/high16 v12, 0x10e0000

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    iput v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteAnimationTime:I

    .line 470
    new-instance v12, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$1;

    invoke-direct {v12, p0, v4, v6}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$1;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/content/Context;I)V

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    .line 477
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$2;

    invoke-direct {v13, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$2;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 484
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    invoke-virtual {v12}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 485
    .local v5, "dialogWindow":Landroid/view/Window;
    const/16 v12, 0x50

    invoke-virtual {v5, v12}, Landroid/view/Window;->setGravity(I)V

    .line 486
    const/16 v12, 0x10

    invoke-virtual {v5, v12}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 488
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f04006c

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->setContentView(I)V

    .line 491
    const/4 v12, -0x1

    const/4 v13, -0x2

    invoke-virtual {v5, v12, v13}, Landroid/view/Window;->setLayout(II)V

    .line 496
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e013b

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorContainer:Landroid/view/View;

    .line 498
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e013c

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorFlicker:Landroid/view/View;

    .line 499
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e013d

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .line 501
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e013e

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;

    .line 502
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e0140

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;

    .line 503
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e013f

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteText:Landroid/widget/TextView;

    .line 504
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e0141

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    .line 505
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    const v13, 0x7f0e0142

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorShield:Landroid/view/View;

    .line 507
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;

    move/from16 v0, p2

    invoke-direct {v13, p0, v10, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$3;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/content/res/Resources;Z)V

    invoke-virtual {v12, v13}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 530
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorContainer:Landroid/view/View;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$4;

    invoke-direct {v13, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$4;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 538
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-virtual {v12}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->getSwipeHelper()Lcom/google/android/ublib/infocards/SwipeHelper;

    move-result-object v11

    .line 539
    .local v11, "swipeHelper":Lcom/google/android/ublib/infocards/SwipeHelper;
    const/high16 v12, 0x3f800000    # 1.0f

    invoke-virtual {v11, v12}, Lcom/google/android/ublib/infocards/SwipeHelper;->setMinAlpha(F)V

    .line 540
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/google/android/ublib/infocards/SwipeHelper;->setHasDismissAnimation(Z)V

    .line 542
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$5;

    invoke-direct {v13, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$5;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v12, v13}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->setDismissCallback(Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;)V

    .line 549
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$6;

    invoke-direct {v13, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$6;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 556
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$7;

    invoke-direct {v13, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$7;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v12, v13}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 572
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->updateNoteEditorColor()V

    .line 574
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v9

    .line 575
    .local v9, "quote":Ljava/lang/CharSequence;
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteText:Landroid/widget/TextView;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getInitialTextToEdit()Ljava/lang/String;

    move-result-object v7

    .line 578
    .local v7, "noteText":Ljava/lang/String;
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v12, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 581
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    const/4 v13, -0x1

    move/from16 v0, p1

    if-eq v0, v13, :cond_4

    .end local p1    # "charOffset":I
    :goto_1
    move/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 584
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->lockSelection()V

    .line 585
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->dismissInfoCards()V

    .line 587
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getPopup(Z)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v8

    .line 588
    .local v8, "popup":Lcom/google/android/apps/books/widget/SelectionPopup;
    if-eqz v8, :cond_3

    .line 589
    invoke-interface {v8}, Lcom/google/android/apps/books/widget/SelectionPopup;->hide()V

    .line 593
    :cond_3
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;

    new-instance v13, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;

    invoke-direct {v13, p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$8;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 604
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v12}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v12

    const/4 v13, 0x1

    invoke-interface {v12, v13}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->setSystemUiVisible(Z)V

    .line 606
    iget-object v12, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    invoke-virtual {v12}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 581
    .end local v8    # "popup":Lcom/google/android/apps/books/widget/SelectionPopup;
    .restart local p1    # "charOffset":I
    :cond_4
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result p1

    goto :goto_1
.end method

.method private startViewingNote()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 393
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->startEditingNoteAtCharOffset(IZ)V

    .line 394
    return-void
.end method

.method private tearDown()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 298
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mAlreadyTornDown:Z

    if-nez v2, :cond_9

    .line 299
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mAlreadyTornDown:Z

    .line 301
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getSelectionView()Landroid/view/View;

    move-result-object v1

    .line 302
    .local v1, "selectionView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 303
    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 306
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_1

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    .line 314
    .local v0, "noteEditorDialog":Landroid/app/Dialog;
    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 315
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->onDismissingNoteEditor()V

    .line 316
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 321
    .end local v0    # "noteEditorDialog":Landroid/app/Dialog;
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorContainer:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 322
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorContainer:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorContainer:Landroid/view/View;

    .line 326
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    if-eqz v2, :cond_3

    .line 327
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/widget/SwipeableLinearLayout;->setDismissCallback(Lcom/google/android/apps/books/widget/SwipeableLinearLayout$DismissCallback;)V

    .line 328
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorSwiper:Lcom/google/android/apps/books/widget/SwipeableLinearLayout;

    .line 331
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;

    if-eqz v2, :cond_4

    .line 332
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteContainer:Landroid/view/View;

    .line 336
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;

    if-eqz v2, :cond_5

    .line 337
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteMenuButton:Landroid/view/View;

    .line 341
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemoveHighlightButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_6

    .line 342
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemoveHighlightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemoveHighlightButton:Landroid/widget/ImageButton;

    .line 346
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->dismissActiveSelection()V

    .line 348
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$300(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 349
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mPopup:Lcom/google/android/apps/books/widget/SelectionPopup;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$300(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/widget/SelectionPopup;->hide()V

    .line 352
    :cond_7
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    .line 353
    iput-boolean v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemovingHighlight:Z

    .line 355
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    if-eqz v2, :cond_8

    .line 356
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    invoke-virtual {v2}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->dismiss()V

    .line 357
    iput-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    .line 360
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mLeaveSystemUiShowingOnTearDown:Z

    if-nez v2, :cond_9

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->setSystemUiVisible(Z)V

    .line 364
    .end local v1    # "selectionView":Landroid/view/View;
    :cond_9
    return-void
.end method

.method private translateSelectedText()V
    .locals 2

    .prologue
    .line 369
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mLeaveSystemUiShowingOnTearDown:Z

    .line 373
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 374
    .local v0, "text":Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->endSelection()V

    .line 376
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;->SELECTION_TRANSLATE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTextSelectionAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TextSelectionAction;)V

    .line 377
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->translateText(Ljava/lang/CharSequence;)V

    .line 378
    return-void
.end method

.method private updateHighlight(I)V
    .locals 3
    .param p1, "newAndroidColor"    # I

    .prologue
    .line 881
    const/4 v2, 0x1

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->logHighlightAddOrEdit(ZI)V

    .line 882
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    .line 883
    .local v1, "oldAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/annotations/Annotation;->withNewColor(I)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 884
    .local v0, "newAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiEdit(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 885
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v2, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->onAnnotationChanged(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 886
    return-void
.end method

.method private updateNoteEditorColor()V
    .locals 6

    .prologue
    .line 401
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    if-eqz v4, :cond_0

    .line 402
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->getNoteEditorDrawingColor()I

    move-result v0

    .line 405
    .local v0, "drawingColor":I
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getCurrentTextColor()I

    move-result v2

    .line 406
    .local v2, "textColor":I
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v4

    const/16 v5, 0x7f

    if-le v4, v5, :cond_1

    .line 407
    const v1, 0x3f333333    # 0.7f

    .line 408
    .local v1, "quoteFract":F
    const v3, 0x3dcccccd    # 0.1f

    .line 413
    .local v3, "typedFract":F
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteQuoteText:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 415
    iget-object v4, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditText:Landroid/widget/EditText;

    invoke-static {v0, v3}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->adjustColor(IF)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 418
    .end local v0    # "drawingColor":I
    .end local v1    # "quoteFract":F
    .end local v2    # "textColor":I
    .end local v3    # "typedFract":F
    :cond_0
    return-void

    .line 410
    .restart local v0    # "drawingColor":I
    .restart local v2    # "textColor":I
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    .line 411
    .restart local v1    # "quoteFract":F
    const v3, 0x3feccccd    # 1.85f

    .restart local v3    # "typedFract":F
    goto :goto_0
.end method

.method private visibleOrGone(Z)I
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 998
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public addNewClipboardCopy()V
    .locals 2

    .prologue
    .line 895
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->COPY_LAYER_ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->addNewAnnotation(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 896
    return-void
.end method

.method public addNewHighlight(ILjava/lang/String;)V
    .locals 2
    .param p1, "androidColor"    # I
    .param p2, "marginNoteText"    # Ljava/lang/String;

    .prologue
    .line 889
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->logHighlightAddOrEdit(ZI)V

    .line 890
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->NOTES_LAYER_ID:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->addNewAnnotation(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 892
    return-void
.end method

.method beginSelection(Z)V
    .locals 4
    .param p1, "isNote"    # Z

    .prologue
    const/4 v3, 0x1

    .line 223
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHasSelection:Z

    if-eqz v2, :cond_0

    .line 247
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getSelectionView()Landroid/view/View;

    move-result-object v1

    .line 228
    .local v1, "selectionView":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 229
    invoke-virtual {v1, v3}, Landroid/view/View;->setSelected(Z)V

    .line 233
    :cond_1
    if-nez p1, :cond_3

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getPopup(Z)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v0

    .line 238
    .local v0, "popup":Lcom/google/android/apps/books/widget/SelectionPopup;
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 239
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->setLocationReady()V

    .line 241
    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->setContentReady()V

    .line 242
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->maybeShow()V

    .line 245
    .end local v0    # "popup":Lcom/google/android/apps/books/widget/SelectionPopup;
    :cond_3
    iput-boolean v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHasSelection:Z

    .line 246
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemovingHighlight:Z

    goto :goto_0
.end method

.method endSelection()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper;->discardPopup()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$100(Lcom/google/android/apps/books/app/SelectionUiHelper;)V

    .line 252
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mHasSelection:Z

    if-eqz v0, :cond_0

    .line 253
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mRemovingHighlight:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNoteEditorDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->animateNoteOut()V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->tearDown()V

    goto :goto_0
.end method

.method getInitialTextToEdit()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 272
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :goto_0
    if-nez v0, :cond_1

    .line 273
    const-string v1, ""

    .line 275
    :goto_1
    return-object v1

    .line 270
    .end local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 275
    .restart local v0    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getNote()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public getSelectionInfo()Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    return-object v0
.end method

.method highlightSelectionFromPrefColorId(IZ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "savePref"    # Z

    .prologue
    .line 284
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 285
    .local v1, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    .line 286
    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getHighlightColor()I

    move-result p1

    .line 290
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper;->getAndroidColor(I)I
    invoke-static {v2, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$200(Lcom/google/android/apps/books/app/SelectionUiHelper;I)I

    move-result v0

    .line 291
    .local v0, "androidColor":I
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->setSelectionToAndroidColor(I)V

    .line 293
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->updateNoteEditorColor()V

    .line 294
    return-void

    .line 287
    .end local v0    # "androidColor":I
    :cond_1
    if-eqz p2, :cond_0

    .line 288
    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setHighlightColor(I)V

    goto :goto_0
.end method

.method public setupPopup(Landroid/view/View;)V
    .locals 36
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1094
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-object/from16 v32, v0

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 1098
    .local v25, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->firstTimeSelectionSetup(Landroid/content/res/Resources;Landroid/view/View;)V

    .line 1100
    const/16 v19, 0x0

    .line 1102
    .local v19, "nonHighlightButtonCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getNormalizedSelectedText()Ljava/lang/CharSequence;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Ljava/lang/CharSequence;->length()I

    move-result v21

    .line 1105
    .local v21, "normalizedSelectedTextLength":I
    const v32, 0x7f0e01cd

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 1107
    .local v9, "copyItem":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->isClipboardCopyingPermitted(I)Z

    move-result v8

    .line 1109
    .local v8, "copyButtonEnabled":Z
    if-eqz v8, :cond_0

    .line 1110
    add-int/lit8 v19, v19, 0x1

    .line 1112
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->visibleOrGone(Z)I

    move-result v32

    move/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1114
    const v32, 0x7f0e01cf

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    .line 1115
    .local v28, "shareItem":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->isQuoteSharingPermitted()Z

    move-result v27

    .line 1116
    .local v27, "shareButtonEnabled":Z
    if-eqz v27, :cond_1

    .line 1117
    add-int/lit8 v19, v19, 0x1

    .line 1119
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->visibleOrGone(Z)I

    move-result v32

    move-object/from16 v0, v28

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1121
    const v32, 0x7f0e01d0

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .line 1122
    .local v26, "searchInBookItem":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->isSearchingPermitted()Z

    move-result v13

    .line 1123
    .local v13, "enableSearchButton":Z
    if-eqz v13, :cond_2

    .line 1124
    add-int/lit8 v19, v19, 0x1

    .line 1126
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->visibleOrGone(Z)I

    move-result v32

    move-object/from16 v0, v26

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1128
    if-eqz v13, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-object/from16 v32, v0

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v32

    if-eqz v32, :cond_5

    const/4 v14, 0x1

    .line 1131
    .local v14, "enableSearchWebButton":Z
    :goto_0
    const v32, 0x7f0e01cc

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1132
    .local v5, "addNoteItem":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->isEditable()Z

    move-result v16

    .line 1133
    .local v16, "isEditable":Z
    if-eqz v16, :cond_3

    .line 1134
    add-int/lit8 v19, v19, 0x1

    .line 1136
    :cond_3
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->visibleOrGone(Z)I

    move-result v32

    move/from16 v0, v32

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1138
    const v32, 0x7f0e01ce

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .line 1139
    .local v30, "translateMenuItem":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->isTranslationPermitted()Z

    move-result v32

    if-eqz v32, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-object/from16 v32, v0

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->getContext()Landroid/content/Context;

    move-result-object v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v32

    if-eqz v32, :cond_6

    const/4 v15, 0x1

    .line 1142
    .local v15, "enableTranslation":Z
    :goto_1
    if-eqz v15, :cond_4

    .line 1143
    add-int/lit8 v19, v19, 0x1

    .line 1145
    :cond_4
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->visibleOrGone(Z)I

    move-result v32

    move-object/from16 v0, v30

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v6

    .line 1148
    .local v6, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v6, :cond_7

    const/4 v12, 0x1

    .line 1149
    .local v12, "enableRemoveHighlight":Z
    :goto_2
    const v32, 0x7f0e01d2

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .line 1151
    .local v24, "removeHighlightButton":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->visibleOrGone(Z)I

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1154
    if-eqz v6, :cond_8

    .line 1155
    invoke-virtual {v6}, Lcom/google/android/apps/books/annotations/Annotation;->getColor()I

    move-result v32

    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->getMatchingPrefColor(I)I

    move-result v10

    .line 1160
    .local v10, "currentPrefColor":I
    :goto_3
    new-instance v32, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$ColorSelectionListenerImpl;-><init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;Landroid/view/View;)V

    const/16 v33, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    move/from16 v2, v33

    invoke-static {v0, v1, v2, v10}, Lcom/google/android/apps/books/app/HighlightColorSelector;->setupColorSelector(Landroid/view/View;Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;ZI)V

    .line 1163
    const v32, 0x7f0e01ca

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    check-cast v29, Landroid/widget/LinearLayout;

    .line 1166
    .local v29, "topView":Landroid/widget/LinearLayout;
    const v32, 0x7f0e01cb

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout;

    .line 1169
    .local v20, "noncolorLayout":Landroid/widget/LinearLayout;
    const v32, 0x7f0e01d1

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 1172
    .local v7, "colorLayout":Landroid/widget/LinearLayout;
    const v32, 0x7f0e00a4

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1175
    .local v11, "divider":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-object/from16 v32, v0

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->isSample()Z

    move-result v32

    if-nez v32, :cond_9

    const/16 v17, 0x1

    .line 1176
    .local v17, "isHighlightable":Z
    :goto_4
    if-eqz v17, :cond_a

    .line 1177
    add-int/lit8 v22, v19, 0x5

    .line 1185
    .local v22, "numberOfActiveButtons":I
    :goto_5
    if-nez v22, :cond_b

    .line 1186
    const/16 v32, 0x8

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1226
    :goto_6
    return-void

    .line 1128
    .end local v5    # "addNoteItem":Landroid/view/View;
    .end local v6    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .end local v7    # "colorLayout":Landroid/widget/LinearLayout;
    .end local v10    # "currentPrefColor":I
    .end local v11    # "divider":Landroid/view/View;
    .end local v12    # "enableRemoveHighlight":Z
    .end local v14    # "enableSearchWebButton":Z
    .end local v15    # "enableTranslation":Z
    .end local v16    # "isEditable":Z
    .end local v17    # "isHighlightable":Z
    .end local v20    # "noncolorLayout":Landroid/widget/LinearLayout;
    .end local v22    # "numberOfActiveButtons":I
    .end local v24    # "removeHighlightButton":Landroid/view/View;
    .end local v29    # "topView":Landroid/widget/LinearLayout;
    .end local v30    # "translateMenuItem":Landroid/view/View;
    :cond_5
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 1139
    .restart local v5    # "addNoteItem":Landroid/view/View;
    .restart local v14    # "enableSearchWebButton":Z
    .restart local v16    # "isEditable":Z
    .restart local v30    # "translateMenuItem":Landroid/view/View;
    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 1148
    .restart local v6    # "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    .restart local v15    # "enableTranslation":Z
    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 1157
    .restart local v12    # "enableRemoveHighlight":Z
    .restart local v24    # "removeHighlightButton":Landroid/view/View;
    :cond_8
    const/4 v10, -0x1

    .restart local v10    # "currentPrefColor":I
    goto :goto_3

    .line 1175
    .restart local v7    # "colorLayout":Landroid/widget/LinearLayout;
    .restart local v11    # "divider":Landroid/view/View;
    .restart local v20    # "noncolorLayout":Landroid/widget/LinearLayout;
    .restart local v29    # "topView":Landroid/widget/LinearLayout;
    :cond_9
    const/16 v17, 0x0

    goto :goto_4

    .line 1180
    .restart local v17    # "isHighlightable":Z
    :cond_a
    move/from16 v22, v19

    .line 1181
    .restart local v22    # "numberOfActiveButtons":I
    const/16 v32, 0x8

    move/from16 v0, v32

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1182
    const/16 v32, 0x8

    move/from16 v0, v32

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 1189
    :cond_b
    const/16 v32, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1192
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionPopupButtonPixels:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionPopupButtonPaddingPixels:I

    move/from16 v33, v0

    mul-int/lit8 v33, v33, 0x2

    add-int v32, v32, v33

    mul-int v32, v32, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionPopupButtonPaddingPixels:I

    move/from16 v33, v0

    add-int v23, v32, v33

    .line 1197
    .local v23, "oneRowPixels":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-object/from16 v32, v0

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper;->mDelegate:Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;
    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$000(Lcom/google/android/apps/books/app/SelectionUiHelper;)Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Lcom/google/android/apps/books/app/SelectionUiHelper$Delegate;->isVertical()Z

    move-result v31

    .line 1198
    .local v31, "vertical":Z
    if-eqz v31, :cond_e

    .line 1199
    const/16 v32, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1200
    const/16 v32, 0x1

    move/from16 v0, v32

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1209
    :goto_7
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout$LayoutParams;

    .line 1212
    .local v18, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    if-eqz v17, :cond_d

    if-eqz v31, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mScreenHeight:I

    move/from16 v32, v0

    move/from16 v0, v23

    move/from16 v1, v32

    if-ge v0, v1, :cond_d

    :cond_c
    if-nez v31, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mScreenWidth:I

    move/from16 v32, v0

    move/from16 v0, v23

    move/from16 v1, v32

    if-ge v0, v1, :cond_f

    .line 1214
    :cond_d
    const/16 v32, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1216
    const/16 v32, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerMarginPixels:I

    move/from16 v33, v0

    const/16 v34, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerMarginPixels:I

    move/from16 v35, v0

    move-object/from16 v0, v18

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1217
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerThicknessPixels:I

    move/from16 v32, v0

    move/from16 v0, v32

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1218
    const/16 v32, -0x1

    move/from16 v0, v32

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_6

    .line 1202
    .end local v18    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_e
    const/16 v32, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1203
    const/16 v32, 0x0

    move/from16 v0, v32

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_7

    .line 1220
    .restart local v18    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_f
    const/16 v32, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1222
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerMarginPixels:I

    move/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerMarginPixels:I

    move/from16 v34, v0

    const/16 v35, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1223
    const/16 v32, -0x1

    move/from16 v0, v32

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 1224
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mDividerThicknessPixels:I

    move/from16 v32, v0

    move/from16 v0, v32

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_6
.end method

.method stopEditingNote(Ljava/lang/String;)V
    .locals 4
    .param p1, "finalText"    # Ljava/lang/String;

    .prologue
    .line 837
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mSelectionInfo:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;

    invoke-interface {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v2

    .line 838
    .local v2, "oldAnnotation":Lcom/google/android/apps/books/annotations/Annotation;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x1

    .line 841
    .local v1, "logAsEdit":Z
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logNoteAddOrEdit(ZI)V

    .line 842
    if-nez v2, :cond_2

    .line 843
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 844
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    # invokes: Lcom/google/android/apps/books/app/SelectionUiHelper;->getCurrentAndroidColor()I
    invoke-static {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->access$400(Lcom/google/android/apps/books/app/SelectionUiHelper;)I

    move-result v0

    .line 845
    .local v0, "defaultColor":I
    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->addNewHighlight(ILjava/lang/String;)V

    .line 847
    .end local v0    # "defaultColor":I
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 851
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 852
    return-void

    .line 838
    .end local v1    # "logAsEdit":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 849
    .restart local v1    # "logAsEdit":Z
    :cond_2
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->editMarginNote(Lcom/google/android/apps/books/annotations/Annotation;Ljava/lang/String;)V

    goto :goto_1
.end method

.method textSelectionChanged()V
    .locals 3

    .prologue
    .line 262
    iget-object v1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->this$0:Lcom/google/android/apps/books/app/SelectionUiHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getPopup(Z)Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v0

    .line 263
    .local v0, "popup":Lcom/google/android/apps/books/widget/SelectionPopup;
    if-eqz v0, :cond_0

    .line 264
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->setContentReady()V

    .line 265
    invoke-interface {v0}, Lcom/google/android/apps/books/widget/SelectionPopup;->maybeShow()V

    .line 267
    :cond_0
    return-void
.end method
