.class Lcom/google/android/apps/books/app/TextModeSelectionState$1;
.super Ljava/lang/Object;
.source "TextModeSelectionState.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/TextModeSelectionState;->loadMatchingAnnotation(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/TextModeSelectionState;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/TextModeSelectionState;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->this$0:Lcom/google/android/apps/books/app/TextModeSelectionState;

    iput-object p2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;)V
    .locals 4
    .param p1, "t"    # Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;

    .prologue
    .line 103
    if-eqz p1, :cond_1

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->this$0:Lcom/google/android/apps/books/app/TextModeSelectionState;

    # getter for: Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-static {v2}, Lcom/google/android/apps/books/app/TextModeSelectionState;->access$000(Lcom/google/android/apps/books/app/TextModeSelectionState;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;->getAnnotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Iterator;

    move-result-object v0

    .line 105
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 106
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/Annotation;

    .line 107
    .local v1, "next":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->this$0:Lcom/google/android/apps/books/app/TextModeSelectionState;

    # getter for: Lcom/google/android/apps/books/app/TextModeSelectionState;->mSelectedTextRange:Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-static {v2}, Lcom/google/android/apps/books/app/TextModeSelectionState;->access$000(Lcom/google/android/apps/books/app/TextModeSelectionState;)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/Annotation;->getPositionRange()Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v2, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 114
    .end local v0    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/annotations/Annotation;>;"
    .end local v1    # "next":Lcom/google/android/apps/books/annotations/Annotation;
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 100
    check-cast p1, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/TextModeSelectionState$1;->take(Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;)V

    return-void
.end method
