.class Lcom/google/android/apps/books/navigation/BookNavView$3;
.super Ljava/lang/Object;
.source "BookNavView.java"

# interfaces
.implements Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/navigation/BookNavView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDownTime:J

.field private mGridInitialized:Z

.field private mInProgress:Z

.field final synthetic this$0:Lcom/google/android/apps/books/navigation/BookNavView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/navigation/BookNavView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 207
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mInProgress:Z

    .line 209
    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mGridInitialized:Z

    return-void
.end method

.method private updateVelocity(Landroid/view/View;I)V
    .locals 12
    .param p1, "grabView"    # Landroid/view/View;
    .param p2, "action"    # I

    .prologue
    .line 217
    invoke-static {p1}, Lcom/google/android/apps/books/util/ViewUtils;->getLocationOnScreen(Landroid/view/View;)J

    move-result-wide v10

    .line 219
    .local v10, "location":J
    iget-wide v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mDownTime:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {v10, v11}, Lcom/google/android/apps/books/util/LongPoint;->getX(J)I

    move-result v4

    int-to-float v5, v4

    invoke-static {v10, v11}, Lcom/google/android/apps/books/util/LongPoint;->getY(J)I

    move-result v4

    int-to-float v6, v4

    const/4 v7, 0x0

    move v4, p2

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 222
    .local v8, "event":Landroid/view/MotionEvent;
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$800(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/view/VelocityTracker;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 223
    invoke-virtual {v8}, Landroid/view/MotionEvent;->recycle()V

    .line 224
    return-void
.end method


# virtual methods
.method public onGesture(Lcom/google/android/apps/books/util/MultiTouchGestureDetector;)Z
    .locals 24
    .param p1, "detector"    # Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .prologue
    .line 228
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mInProgress:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    :cond_0
    const/4 v2, 0x1

    .line 307
    :goto_0
    return v2

    .line 232
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getFirstVisiblePosition()I

    move-result v13

    .line 233
    .local v13, "firstVisible":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getLastVisiblePosition()I

    move-result v18

    .line 235
    .local v18, "lastVisible":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1000(Lcom/google/android/apps/books/navigation/BookNavView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getView(I)Landroid/view/View;

    move-result-object v14

    .line 237
    .local v14, "grabView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScaleEventTime:J
    invoke-static {v2, v6, v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$002(Lcom/google/android/apps/books/navigation/BookNavView;J)J

    .line 239
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->getScaleFactor()F

    move-result v2

    invoke-virtual {v14}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float v21, v2, v3

    .line 240
    .local v21, "scale":F
    sget-object v2, Lcom/google/android/apps/books/util/ConfigValue;->GRID_TOC:Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-virtual {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v9

    .line 242
    .local v9, "allowGrid":Z
    if-nez v9, :cond_4

    .line 247
    const v2, 0x3f7ae148    # 0.98f

    move/from16 v0, v21

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v21

    .line 266
    :cond_2
    :goto_1
    const v2, 0x3f866666    # 1.05f

    cmpl-float v2, v21, v2

    if-gez v2, :cond_3

    const v2, 0x3f733333    # 0.95f

    cmpg-float v2, v21, v2

    if-gtz v2, :cond_6

    .line 267
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-static {v14}, Lcom/google/android/apps/books/util/ViewUtils;->getLocationOnScreen(Landroid/view/View;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/LongPoint;->getY(J)I

    move-result v3

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPinchEndY:I
    invoke-static {v2, v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1802(Lcom/google/android/apps/books/navigation/BookNavView;I)I

    .line 268
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mInProgress:Z

    .line 269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    const-wide/16 v6, 0x0

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScaleEventTime:J
    invoke-static {v2, v6, v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$002(Lcom/google/android/apps/books/navigation/BookNavView;J)J

    .line 270
    const v2, 0x3f866666    # 1.05f

    cmpl-float v2, v21, v2

    if-ltz v2, :cond_5

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPinchedToFull:Z
    invoke-static {v2, v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1902(Lcom/google/android/apps/books/navigation/BookNavView;Z)Z

    .line 272
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1100(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookScrollController;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1000(Lcom/google/android/apps/books/navigation/BookNavView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/navigation/BookScrollController;->showSpreadInFull(I)V

    .line 276
    :goto_2
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 248
    :cond_4
    const v2, 0x3f7ae148    # 0.98f

    cmpg-float v2, v21, v2

    if-gtz v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mGridInitialized:Z

    if-nez v2, :cond_2

    .line 255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1100(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookScrollController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/BookScrollController;->getPageRangeLoader()Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-result-object v20

    .line 256
    .local v20, "pageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->setPaused(Z)V

    .line 258
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1000(Lcom/google/android/apps/books/navigation/BookNavView;)I

    move-result v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/navigation/PageRangeLoader;->getSpreadIdentifier(I)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v5

    .line 261
    .local v5, "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookGridView:Lcom/google/android/apps/books/navigation/BookGridView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1600(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookGridView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1200(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v4}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1300(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;
    invoke-static {v6}, Lcom/google/android/apps/books/navigation/BookNavView;->access$500(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/BookView$Callbacks;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/BitmapCache;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mFullScreenSpreadSize:Landroid/graphics/Point;
    invoke-static {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/Point;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/books/navigation/BookGridView;->prepare(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Landroid/graphics/Point;)V

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPageGrid:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1700(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_1

    .line 274
    .end local v5    # "spreadId":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v20    # "pageLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->animateToGrid()V
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2000(Lcom/google/android/apps/books/navigation/BookNavView;)V

    goto :goto_2

    .line 281
    :cond_6
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v21, v2

    if-ltz v2, :cond_8

    .line 282
    const/high16 v23, 0x3f800000    # 1.0f

    .line 283
    .local v23, "viewAlpha":F
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v21, v2

    const v3, 0x3d4cccc0    # 0.049999952f

    div-float v12, v2, v3

    .line 284
    .local v12, "doneFraction":F
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 290
    .local v17, "labelAlpha":F
    :goto_3
    move v15, v13

    .local v15, "i":I
    :goto_4
    move/from16 v0, v18

    if-gt v15, v0, :cond_9

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getView(I)Landroid/view/View;

    move-result-object v11

    .line 292
    .local v11, "child":Landroid/view/View;
    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/view/View;->setScaleX(F)V

    .line 293
    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/view/View;->setScaleY(F)V

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1000(Lcom/google/android/apps/books/navigation/BookNavView;)I

    move-result v2

    if-eq v15, v2, :cond_7

    .line 296
    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/view/View;->setAlpha(F)V

    .line 290
    :cond_7
    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    .line 286
    .end local v11    # "child":Landroid/view/View;
    .end local v12    # "doneFraction":F
    .end local v15    # "i":I
    .end local v17    # "labelAlpha":F
    .end local v23    # "viewAlpha":F
    :cond_8
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v2, v21

    const v3, 0x3d4cccd0    # 0.050000012f

    mul-float v12, v2, v3

    .line 287
    .restart local v12    # "doneFraction":F
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .restart local v17    # "labelAlpha":F
    move/from16 v23, v17

    .restart local v23    # "viewAlpha":F
    goto :goto_3

    .line 300
    .restart local v15    # "i":I
    :cond_9
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, Lcom/google/android/apps/books/navigation/BookNavView$3;->updateVelocity(Landroid/view/View;I)V

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mFadeViews:[Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2100(Lcom/google/android/apps/books/navigation/BookNavView;)[Landroid/view/View;

    move-result-object v10

    .local v10, "arr$":[Landroid/view/View;
    array-length v0, v10

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_5
    move/from16 v0, v16

    move/from16 v1, v19

    if-ge v0, v1, :cond_a

    aget-object v22, v10, v16

    .line 303
    .local v22, "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;
    invoke-static {v2}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2200(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/MultiViewAnimator;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/util/MultiViewAnimator;->getSavedViewState(Landroid/view/View;)Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .line 304
    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 302
    add-int/lit8 v16, v16, 0x1

    goto :goto_5

    .line 307
    .end local v22    # "view":Landroid/view/View;
    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public onGestureBegin(Lcom/google/android/apps/books/util/MultiTouchGestureDetector;)Z
    .locals 10
    .param p1, "detector"    # Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 312
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mSnapAnimator:Landroid/animation/Animator;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2300(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/animation/Animator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/animation/Animator;->isRunning()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPageNumberEditor:Lcom/google/android/apps/books/widget/KeyboardAwareEditText;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2400(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/widget/KeyboardAwareEditText;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/widget/KeyboardAwareEditText;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    .line 358
    :cond_0
    :goto_0
    return v5

    .line 317
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScaleEventTime:J
    invoke-static {v7, v8, v9}, Lcom/google/android/apps/books/navigation/BookNavView;->access$002(Lcom/google/android/apps/books/navigation/BookNavView;J)J

    .line 318
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2200(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/MultiViewAnimator;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/util/MultiViewAnimator;->clear()V

    .line 319
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->getFocusX()F

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->getFocusY()F

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/PointF;->set(FF)V

    .line 320
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;
    invoke-static {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;
    invoke-static {v9}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/books/util/ViewUtils;->isParentPointInChildBounds(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 325
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getCenterPosition()I

    move-result v8

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mGrabbedSpreadIndex:I
    invoke-static {v7, v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1002(Lcom/google/android/apps/books/navigation/BookNavView;I)I

    .line 326
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getCenterPosition()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getView(I)Landroid/view/View;

    move-result-object v0

    .line 329
    .local v0, "centerSpread":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;
    invoke-static {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2600(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v8

    invoke-static {v0, v7, v8}, Lcom/google/android/apps/books/util/ViewUtils;->isParentPointInChildBounds(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 334
    iput-boolean v6, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mInProgress:Z

    .line 335
    iput-boolean v5, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mGridInitialized:Z

    .line 337
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 339
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getFirstVisiblePosition()I

    move-result v1

    .line 340
    .local v1, "firstVisible":I
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getLastVisiblePosition()I

    move-result v3

    .line 342
    .local v3, "lastVisible":I
    move v2, v1

    .local v2, "i":I
    :goto_1
    if-gt v2, v3, :cond_2

    .line 343
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getView(I)Landroid/view/View;

    move-result-object v4

    .line 344
    .local v4, "view":Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewPointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2500(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;
    invoke-static {v8}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2600(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v8

    invoke-static {v4, v7, v8}, Lcom/google/android/apps/books/util/ViewUtils;->isParentPointInChildBounds(Landroid/view/View;Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    .line 346
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mViewAnimator:Lcom/google/android/apps/books/util/MultiViewAnimator;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2200(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/util/MultiViewAnimator;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/google/android/apps/books/util/MultiViewAnimator;->getSavedViewState(Landroid/view/View;)Lcom/google/android/apps/books/util/MultiViewAnimator$ViewAnimationState;

    .line 347
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2600(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->x:F

    invoke-virtual {v4, v7}, Landroid/view/View;->setPivotX(F)V

    .line 348
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPagePointF:Landroid/graphics/PointF;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2600(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/graphics/PointF;

    move-result-object v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v7}, Landroid/view/View;->setPivotY(F)V

    .line 342
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 351
    .end local v4    # "view":Landroid/view/View;
    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScrollController:Lcom/google/android/apps/books/navigation/BookScrollController;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$1100(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/apps/books/navigation/BookScrollController;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/navigation/BookScrollController;->redraw()V

    .line 352
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mBookSkimView:Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$900(Lcom/google/android/apps/books/navigation/BookNavView;)Lcom/google/android/ublib/widget/HorizontalWarpListView;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setStasis(Z)V

    .line 354
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mDownTime:J

    .line 355
    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # getter for: Lcom/google/android/apps/books/navigation/BookNavView;->mPinchVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static {v7}, Lcom/google/android/apps/books/navigation/BookNavView;->access$800(Lcom/google/android/apps/books/navigation/BookNavView;)Landroid/view/VelocityTracker;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/VelocityTracker;->clear()V

    .line 356
    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/books/navigation/BookNavView$3;->updateVelocity(Landroid/view/View;I)V

    move v5, v6

    .line 358
    goto/16 :goto_0
.end method

.method public onGestureEnd(Lcom/google/android/apps/books/util/MultiTouchGestureDetector;)V
    .locals 4
    .param p1, "detector"    # Lcom/google/android/apps/books/util/MultiTouchGestureDetector;

    .prologue
    .line 363
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mInProgress:Z

    if-nez v0, :cond_0

    .line 370
    :goto_0
    return-void

    .line 367
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->mInProgress:Z

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    const-wide/16 v2, 0x0

    # setter for: Lcom/google/android/apps/books/navigation/BookNavView;->mScaleEventTime:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/navigation/BookNavView;->access$002(Lcom/google/android/apps/books/navigation/BookNavView;J)J

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookNavView$3;->this$0:Lcom/google/android/apps/books/navigation/BookNavView;

    # invokes: Lcom/google/android/apps/books/navigation/BookNavView;->snapback()V
    invoke-static {v0}, Lcom/google/android/apps/books/navigation/BookNavView;->access$2700(Lcom/google/android/apps/books/navigation/BookNavView;)V

    goto :goto_0
.end method
