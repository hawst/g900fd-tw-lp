.class Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;
.super Lcom/google/android/apps/books/data/BasePendingAction;
.source "PageStructureSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$result:Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;Lcom/google/android/apps/books/sync/VolumeContentId;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$result:Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-direct {p0}, Lcom/google/android/apps/books/data/BasePendingAction;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 285
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$result:Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;

    iget-object v1, v1, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    invoke-interface {v1}, Lcom/google/android/apps/books/data/DataControllerBlob;->save()V

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$800(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    sget-object v3, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishResult(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$900(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    :goto_0
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;
    invoke-static {v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$800(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerMap;->publishFailure(Ljava/lang/Object;Ljava/lang/Exception;Lcom/google/android/ublib/utils/Consumer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$900(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    # getter for: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->mContentIdToResult:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$900(Lcom/google/android/apps/books/data/PageStructureSubcontroller;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$8;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v1
.end method
