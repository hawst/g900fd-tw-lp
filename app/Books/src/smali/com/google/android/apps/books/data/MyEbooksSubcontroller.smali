.class public Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
.super Ljava/lang/Object;
.source "MyEbooksSubcontroller.java"


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

.field private final mMyEbooksConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerList",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;"
        }
    .end annotation
.end field

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerList",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lcom/google/android/apps/books/sync/SyncAccountsState;Lcom/google/android/apps/books/annotations/AnnotationController;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "syncState"    # Lcom/google/android/apps/books/sync/SyncAccountsState;
    .param p3, "annotationController"    # Lcom/google/android/apps/books/annotations/AnnotationController;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->createExceptionOrList()Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mMyEbooksConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    .line 50
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->createExceptionOrList()Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAccount:Landroid/accounts/Account;

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 59
    iput-object p3, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Ljava/lang/Runnable;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailure(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Ljava/lang/Runnable;
    .param p3, "x3"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p4, "x4"    # Lcom/google/android/apps/books/model/LocalSessionKey;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;)Lcom/google/android/apps/books/annotations/AnnotationController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAnnotationController:Lcom/google/android/apps/books/annotations/AnnotationController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Exception;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x3"    # Ljava/lang/Exception;
    .param p4, "x4"    # Ljava/lang/Runnable;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Exception;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    .param p3, "x3"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/MyEbooksSubcontroller;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    return-object v0
.end method

.method private createResultFromServerResponse(Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Lcom/google/android/apps/books/data/ControlTaskServices;)Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .locals 14
    .param p1, "licenseResponse"    # Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    .param p2, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 289
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v4

    .line 290
    .local v4, "resultLocalVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 291
    .local v3, "resultDownloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    const/4 v5, 0x0

    .line 292
    .local v5, "resultVolumeDataList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    if-eqz p1, :cond_3

    .line 297
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v7

    .line 298
    .local v7, "storedLocalVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v6

    .line 300
    .local v6, "storedDownloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    :try_start_0
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v11

    const/4 v12, -0x1

    const/4 v13, 0x0

    invoke-interface {v11, v12, v13, v7, v6}, Lcom/google/android/apps/books/model/BooksDataStore;->getMyEbooksVolumesRange(ILjava/util/List;Ljava/util/Map;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->getVolumes()Ljava/util/List;

    move-result-object v5

    .line 309
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/model/VolumeData;

    .line 310
    .local v8, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v8}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v10

    .line 311
    .local v10, "volumeId":Ljava/lang/String;
    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 312
    .local v2, "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    if-nez v2, :cond_1

    .line 314
    sget-object v11, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->DEFAULT:Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withTimestamp(Lcom/google/android/apps/books/model/LocalVolumeData;J)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v2

    .line 317
    :cond_1
    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 319
    .local v9, "volumeDownloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    if-nez v9, :cond_2

    .line 320
    sget-object v9, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->NO_PROGRESS:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 322
    :cond_2
    invoke-interface {v4, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    invoke-interface {v3, v10, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 302
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    .end local v8    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    .end local v9    # "volumeDownloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .end local v10    # "volumeId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/io/IOException;
    const-string v11, "MESC"

    const/4 v12, 0x6

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 304
    const-string v11, "MESC"

    const-string v12, "error retrieving current library"

    invoke-static {v11, v12, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 326
    .end local v0    # "e":Ljava/io/IOException;
    .end local v6    # "storedDownloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    .end local v7    # "storedLocalVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    :cond_3
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 328
    :cond_4
    new-instance v11, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    invoke-direct {v11, v5, v4, v3}, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;-><init>(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V

    return-object v11
.end method

.method private getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 137
    new-instance v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V

    .line 148
    return-void
.end method

.method private getEbooksOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Ljava/util/List;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p3, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/Runnable;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 169
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p4, "volumeIdsToRenew":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p4

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/util/List;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 215
    return-void
.end method

.method private handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Exception;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p3, "e"    # Ljava/lang/Exception;
    .param p4, "continuation"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/NetworkTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/lang/Exception;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 220
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    new-instance v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;

    invoke-direct {v0, p0, p2, p4, p3}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 231
    return-void
.end method

.method private onFoundFetchKey(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 6
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "expiredKeyContinuation"    # Ljava/lang/Runnable;
    .param p3, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/lang/Runnable;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p4, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore;->getVolumeIdsForLicenseRenewal()Ljava/util/List;

    move-result-object v4

    .local v4, "volumeIdsToRenew":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v5, p3

    .line 159
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->getEbooksOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Ljava/util/List;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 160
    return-void
.end method

.method private publishFailure(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mMyEbooksConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishFailure(Ljava/lang/Exception;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishFailure(Ljava/lang/Exception;)V

    .line 345
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$7;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$7;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 340
    return-void
.end method

.method private publishResult(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Z)V
    .locals 5
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "licenseResponse"    # Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;
    .param p3, "save"    # Z

    .prologue
    const/4 v4, 0x0

    .line 245
    invoke-direct {p0, p2, p1}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->createResultFromServerResponse(Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Lcom/google/android/apps/books/data/ControlTaskServices;)Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    move-result-object v0

    .line 247
    .local v0, "myEbooksVolumes":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    const-string v1, "MESC"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    const-string v1, "MESC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "My Ebooks Volumes from server "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_0
    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->publishResult(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;Lcom/google/android/ublib/utils/Consumer;)V

    .line 252
    if-eqz p3, :cond_1

    .line 253
    new-instance v1, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;

    invoke-direct {v1, p0, p2, v0}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$6;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V

    invoke-interface {p1, v1}, Lcom/google/android/apps/books/data/ControlTaskServices;->scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V

    .line 270
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    sget-object v2, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method private publishResult(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "myEbooksVolumes"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p2, "myEbooksConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mMyEbooksConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mMyEbooksConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishSuccess(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 278
    :cond_1
    return-void
.end method

.method private publishServerResponseOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "licenseResponse"    # Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

    .prologue
    .line 235
    new-instance v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$5;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$5;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 241
    return-void
.end method


# virtual methods
.method public getMyEbooks(Lcom/google/android/apps/books/data/ControlTaskServices;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 18
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "fromServer"    # Z
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 66
    .local p3, "myEbooksConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mAccount:Landroid/accounts/Account;

    iget-object v14, v14, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v13, v14}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastMyEbooksFetchTime(Ljava/lang/String;)J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-nez v13, :cond_5

    const/4 v10, 0x1

    .line 68
    .local v10, "neverBeenSynced":Z
    :goto_0
    if-nez p2, :cond_0

    if-eqz v10, :cond_6

    :cond_0
    const/4 v9, 0x1

    .line 72
    .local v9, "mustUseServer":Z
    :goto_1
    if-nez v9, :cond_7

    .line 74
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v8

    .line 75
    .local v8, "localVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v4

    .line 77
    .local v4, "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    const-string v13, "MESC"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 78
    const-string v13, "MESC"

    const-string v14, "getMyEbooks bypassing server"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v13

    invoke-interface {v13, v8, v4}, Lcom/google/android/apps/books/model/BooksDataStore;->getMyEbooksVolumes(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v12

    .line 82
    .local v12, "volumeData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    new-instance v11, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    invoke-direct {v11, v12, v8, v4}, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;-><init>(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V

    .line 86
    .local v11, "result":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    if-eqz p3, :cond_2

    .line 87
    invoke-static {v11}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 89
    :cond_2
    if-eqz p4, :cond_3

    .line 90
    sget-object v13, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    move-object/from16 v0, p4

    invoke-interface {v0, v13}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 92
    :cond_3
    const-string v13, "MESC"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 93
    const-string v13, "MESC"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "My Ebooks List from DB: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " vols"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v4    # "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    .end local v8    # "localVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    .end local v11    # "result":Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .end local v12    # "volumeData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    :cond_4
    :goto_2
    return-void

    .line 66
    .end local v9    # "mustUseServer":Z
    .end local v10    # "neverBeenSynced":Z
    :cond_5
    const/4 v10, 0x0

    goto :goto_0

    .line 68
    .restart local v10    # "neverBeenSynced":Z
    :cond_6
    const/4 v9, 0x0

    goto :goto_1

    .line 96
    .restart local v9    # "mustUseServer":Z
    :catch_0
    move-exception v5

    .line 98
    .local v5, "e":Ljava/io/IOException;
    const-string v13, "MESC"

    const/4 v14, 0x6

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 99
    const-string v13, "MESC"

    const-string v14, "Database load of getMyEbooks failed"

    invoke-static {v13, v14, v5}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    .end local v5    # "e":Ljava/io/IOException;
    :cond_7
    const/4 v7, 0x0

    .line 107
    .local v7, "hadExistingConsumers":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mMyEbooksConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->addConsumer(Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v13

    if-nez v13, :cond_8

    const/4 v13, 0x1

    :goto_3
    or-int/2addr v7, v13

    .line 108
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-object/from16 v0, p4

    invoke-virtual {v13, v0}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->addConsumer(Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v13

    if-nez v13, :cond_9

    const/4 v13, 0x1

    :goto_4
    or-int/2addr v7, v13

    .line 109
    if-eqz v7, :cond_a

    .line 110
    const-string v13, "MESC"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 111
    const-string v13, "MESC"

    const-string v14, "getMyEbooks using server, piggybacking"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 107
    :cond_8
    const/4 v13, 0x0

    goto :goto_3

    .line 108
    :cond_9
    const/4 v13, 0x0

    goto :goto_4

    .line 116
    :cond_a
    const-string v13, "MESC"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 117
    const-string v13, "MESC"

    const-string v14, "getMyEbooks using server"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_b
    new-instance v6, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/MyEbooksSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 126
    .local v6, "expiredKeyContinuation":Ljava/lang/Runnable;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/books/data/MyEbooksSubcontroller;->getDefaultKeyToFetchContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/Runnable;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    goto :goto_2
.end method
