.class Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;
.super Ljava/lang/Object;
.source "OnboardSampleQuizPage.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getDoneButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

.field final synthetic val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Lcom/google/android/play/onboard/OnboardHostControl;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iput-object p2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 307
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # invokes: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getSelectedItemIds()Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1000(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Ljava/util/Set;

    move-result-object v10

    .line 309
    .local v10, "selectedVolumeIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 310
    .local v11, "volumeId":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    iget-object v1, v1, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mController:Lcom/google/android/apps/books/app/OnboardingController;

    invoke-virtual {v1, v11}, Lcom/google/android/apps/books/app/OnboardingController;->addBookToMyLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 312
    .end local v11    # "volumeId":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-interface {v1}, Lcom/google/android/play/onboard/OnboardHostControl;->finishOnboardFlow()V

    .line 316
    new-instance v9, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    invoke-virtual {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v9, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 317
    .local v9, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    const/4 v1, 0x1

    invoke-virtual {v9, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setSuppressOnboardingCard(Z)V

    .line 319
    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-static {v1}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1100(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;)Lcom/google/android/play/onboard/OnboardHostControl;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getSelectedGenres()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    .line 321
    .local v8, "numGenres":I
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->REQUESTED_GENRES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    new-instance v2, Ljava/lang/Long;

    int-to-long v6, v8

    invoke-direct {v2, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logOnboardingAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;Ljava/lang/Long;)V

    .line 324
    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->REQUESTED_SAMPLES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    new-instance v2, Ljava/lang/Long;

    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v6

    int-to-long v6, v6

    invoke-direct {v2, v6, v7}, Ljava/lang/Long;-><init>(J)V

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logOnboardingAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;Ljava/lang/Long;)V

    .line 330
    new-instance v3, Lcom/google/android/ublib/utils/HandlerScheduler;

    invoke-direct {v3}, Lcom/google/android/ublib/utils/HandlerScheduler;-><init>()V

    .line 331
    .local v3, "scheduler":Lcom/google/android/ublib/utils/HandlerScheduler;
    invoke-virtual {v3}, Lcom/google/android/ublib/utils/HandlerScheduler;->getTime()J

    move-result-wide v6

    # getter for: Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->MAX_SAMPLE_WAIT_TIME_MS:J
    invoke-static {}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;->access$1200()J

    move-result-wide v12

    add-long v4, v6, v12

    .line 332
    .local v4, "exitByTime":J
    const-string v1, "OnboardSampleQuizPage"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 333
    const-string v1, "OnboardSampleQuizPage"

    const-string v2, "Scheduling PollMyEbooks"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_1
    new-instance v1, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->this$0:Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$3;->val$hostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage$PollMyEbooks;-><init>(Lcom/google/android/apps/books/onboard/OnboardSampleQuizPage;Lcom/google/android/ublib/utils/Scheduler;JILcom/google/android/play/onboard/OnboardHostControl;)V

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v1, v6, v7}, Lcom/google/android/ublib/utils/HandlerScheduler;->schedule(Ljava/lang/Runnable;J)V

    .line 337
    return-void
.end method
