.class public Lcom/google/android/apps/books/net/BooksAccountManagerImpl;
.super Ljava/lang/Object;
.source "BooksAccountManagerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/net/BooksAccountManager;


# instance fields
.field private final mAccountManager:Landroid/accounts/AccountManager;


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;)V
    .locals 0
    .param p1, "accountManager"    # Landroid/accounts/AccountManager;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/books/net/BooksAccountManagerImpl;->mAccountManager:Landroid/accounts/AccountManager;

    .line 27
    return-void
.end method

.method private apiGetAuthToken(Landroid/accounts/Account;)Landroid/accounts/AccountManagerFuture;
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            ")",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/net/BooksAccountManagerImpl;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "print"

    const/4 v4, 0x1

    move-object v1, p1

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x6

    .line 32
    if-eqz p1, :cond_0

    .line 33
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/net/BooksAccountManagerImpl;->apiGetAuthToken(Landroid/accounts/Account;)Landroid/accounts/AccountManagerFuture;

    move-result-object v1

    .line 34
    .local v1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    invoke-interface {v1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 36
    .local v2, "result":Landroid/os/Bundle;
    if-nez v2, :cond_1

    .line 37
    new-instance v3, Ljava/io/IOException;

    const-string v4, "getResult returned null"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1

    .line 45
    .end local v1    # "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .end local v2    # "result":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    const-string v3, "BooksAccountManager"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    const-string v3, "BooksAccountManager"

    const-string v4, "Authentication error"

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    .end local v0    # "e":Landroid/accounts/AuthenticatorException;
    :cond_0
    :goto_0
    const/4 v3, 0x0

    :goto_1
    return-object v3

    .line 39
    .restart local v1    # "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .restart local v2    # "result":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    const-string v3, "authtoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 40
    const-string v3, "authtoken"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 42
    :cond_2
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Unauthorized"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1

    .line 49
    .end local v1    # "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    .end local v2    # "result":Landroid/os/Bundle;
    :catch_1
    move-exception v0

    .line 50
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v3, "BooksAccountManager"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    const-string v3, "BooksAccountManager"

    const-string v4, "Authentication canceled"

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public invalidateAuthToken(Ljava/lang/String;)V
    .locals 2
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 64
    if-eqz p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/books/net/BooksAccountManagerImpl;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    return-void
.end method
