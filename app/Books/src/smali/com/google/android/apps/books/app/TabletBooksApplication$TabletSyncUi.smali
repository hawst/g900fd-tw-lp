.class Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;
.super Ljava/lang/Object;
.source "TabletBooksApplication.java"

# interfaces
.implements Lcom/google/android/apps/books/service/SyncService$SyncUi;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/TabletBooksApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabletSyncUi"
.end annotation


# instance fields
.field private mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

.field private mDownloadedVolumeId:Ljava/lang/String;

.field private mDownloadingVolumeId:Ljava/lang/String;

.field private mFirstAddedVolumeId:Ljava/lang/String;

.field private mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

.field final synthetic this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/app/TabletBooksApplication;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/app/TabletBooksApplication;Lcom/google/android/apps/books/app/TabletBooksApplication$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication;
    .param p2, "x1"    # Lcom/google/android/apps/books/app/TabletBooksApplication$1;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;-><init>(Lcom/google/android/apps/books/app/TabletBooksApplication;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadingVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mFirstAddedVolumeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;)Landroid/app/NotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    return-object v0
.end method

.method private cancelNotification()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 122
    iput-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 123
    iput-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadedVolumeId:Ljava/lang/String;

    .line 124
    iput-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadingVolumeId:Ljava/lang/String;

    .line 125
    return-void
.end method

.method private getBookDownloadNotificationTitle(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "volumeCount"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "author"    # Ljava/lang/String;
    .param p5, "alwaysPluralize"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 340
    if-nez p5, :cond_1

    if-ne p2, v2, :cond_1

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p3    # "title":Ljava/lang/String;
    :goto_0
    return-object p3

    .restart local p3    # "title":Ljava/lang/String;
    :cond_0
    const v0, 0x7f0f0180

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v3

    aput-object p4, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_1
    const v0, 0x7f110002

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, p2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_0
.end method

.method private getNotificationManager()Landroid/app/NotificationManager;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/TabletBooksApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method private updateDownloadNotification()V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v1, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 235
    return-void
.end method


# virtual methods
.method public addedVolumes(Landroid/accounts/Account;Ljava/util/List;)V
    .locals 34
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 352
    .local p2, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    move-object/from16 v17, v0

    .line 353
    .local v17, "context":Landroid/content/Context;
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 354
    .local v5, "res":Landroid/content/res/Resources;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v6

    .line 356
    .local v6, "volumeCount":I
    if-nez v6, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/android/apps/books/model/VolumeData;

    .line 361
    .local v18, "firstVolume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v19

    .line 362
    .local v19, "firstVolumeId":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mFirstAddedVolumeId:Ljava/lang/String;

    .line 365
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    # invokes: Lcom/google/android/apps/books/app/TabletBooksApplication;->isAnyActivityResumed()Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/TabletBooksApplication;->access$300(Lcom/google/android/apps/books/app/TabletBooksApplication;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 366
    const-string v4, "SyncUI"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 367
    const-string v4, "SyncUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addedVolumes("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") skipped due to resumed activities"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 372
    :cond_2
    new-instance v26, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 374
    .local v26, "publicNotificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v22, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 376
    .local v22, "notificationBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    const v4, 0x7f110003

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v16

    .line 379
    .local v16, "booksAddedText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    # invokes: Lcom/google/android/apps/books/app/TabletBooksApplication;->getBookAddedNotificationTicker(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/books/app/TabletBooksApplication;->access$400(Lcom/google/android/apps/books/app/TabletBooksApplication;Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v32

    .line 381
    .local v32, "tickerText":Ljava/lang/CharSequence;
    move-object/from16 v33, v32

    .line 382
    .local v33, "titleText":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    # invokes: Lcom/google/android/apps/books/app/TabletBooksApplication;->getBookAddedNotificationTicker(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static/range {v4 .. v9}, Lcom/google/android/apps/books/app/TabletBooksApplication;->access$400(Lcom/google/android/apps/books/app/TabletBooksApplication;Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v27

    .line 386
    .local v27, "publicTitleText":Ljava/lang/CharSequence;
    const/4 v4, 0x1

    if-ne v6, v4, :cond_4

    .line 387
    const-string v4, "SyncUI"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 388
    const-string v4, "SyncUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addedVolumes("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") notified"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_3
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    .line 424
    .local v13, "addedVolumesBooksIntent":Landroid/content/Intent;
    :goto_1
    const/4 v4, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-static {v0, v4, v13, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v25

    .line 427
    .local v25, "pendingIntent":Landroid/app/PendingIntent;
    const v4, 0x7f0b00be

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    .line 428
    .local v14, "appColor":I
    const v4, 0x7f020202

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setNumber(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const-string v7, "status"

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, -0x2

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 441
    const v4, 0x7f020202

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setNumber(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    const-string v7, "status"

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v21

    .line 457
    .local v21, "notification":Landroid/app/Notification;
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v4

    const/4 v7, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v4, v7, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 460
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v15

    .line 463
    .local v15, "bdc":Lcom/google/android/apps/books/data/BooksDataController;
    new-instance v31, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v13}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$2;-><init>(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;Ljava/lang/String;Landroid/support/v4/app/NotificationCompat$Builder;Landroid/content/Intent;)V

    .line 511
    .local v31, "thumbnailConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-static {v15, v0, v5, v1}, Lcom/google/android/apps/books/data/DataControllerUtils;->getNotificationThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/res/Resources;Lcom/google/android/ublib/utils/Consumer;)V

    goto/16 :goto_0

    .line 395
    .end local v13    # "addedVolumesBooksIntent":Landroid/content/Intent;
    .end local v14    # "appColor":I
    .end local v15    # "bdc":Lcom/google/android/apps/books/data/BooksDataController;
    .end local v21    # "notification":Landroid/app/Notification;
    .end local v25    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v31    # "thumbnailConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    :cond_4
    const-string v4, "SyncUI"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 396
    const-string v4, "SyncUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addedVolumes("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") + "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " volumes notified"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    :cond_5
    new-instance v4, Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;-><init>(Landroid/support/v4/app/NotificationCompat$Builder;)V

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-result-object v28

    .line 403
    .local v28, "style":Landroid/support/v4/app/NotificationCompat$InboxStyle;
    const/4 v4, 0x5

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v23

    .line 404
    .local v23, "numVolumesToShow":I
    const/16 v20, 0x0

    .local v20, "index":I
    :goto_2
    move/from16 v0, v20

    move/from16 v1, v23

    if-ge v0, v1, :cond_6

    .line 405
    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/android/apps/books/model/VolumeData;

    .line 407
    .local v30, "thisVolume":Lcom/google/android/apps/books/model/VolumeData;
    const/4 v9, 0x1

    invoke-interface/range {v30 .. v30}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {v30 .. v30}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    move-object/from16 v7, p0

    move-object v8, v5

    invoke-direct/range {v7 .. v12}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getBookDownloadNotificationTitle(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    .line 404
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 411
    .end local v30    # "thisVolume":Lcom/google/android/apps/books/model/VolumeData;
    :cond_6
    move/from16 v0, v23

    if-ge v0, v6, :cond_7

    .line 413
    sub-int v24, v6, v23

    .line 414
    .local v24, "numVolumesUnshown":I
    const v4, 0x7f110005

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    move/from16 v0, v24

    invoke-virtual {v5, v4, v0, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    .line 419
    .end local v24    # "numVolumesUnshown":I
    .local v29, "summaryText":Ljava/lang/String;
    :goto_3
    invoke-virtual/range {v28 .. v29}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    .line 421
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalHomeIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    .restart local v13    # "addedVolumesBooksIntent":Landroid/content/Intent;
    goto/16 :goto_1

    .line 417
    .end local v13    # "addedVolumesBooksIntent":Landroid/content/Intent;
    .end local v29    # "summaryText":Ljava/lang/String;
    :cond_7
    move-object/from16 v29, v16

    .restart local v29    # "summaryText":Ljava/lang/String;
    goto :goto_3
.end method

.method public cancelDownloadNotification(Ljava/lang/String;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadedVolumeId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->cancelNotification()V

    .line 118
    :cond_0
    return-void
.end method

.method public finishedAllVolumeDownloads()V
    .locals 2

    .prologue
    .line 326
    const-string v0, "SyncUI"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const-string v0, "SyncUI"

    const-string v1, "finishedAllVolumeDownloads"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_0
    return-void
.end method

.method public finishedVolumeDownload(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "fullyDownloaded"    # Z

    .prologue
    .line 271
    const-string v5, "SyncUI"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 272
    const-string v5, "SyncUI"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "finishedVolumeDownload("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), fullDL="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-nez v5, :cond_1

    .line 322
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    .line 278
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 281
    .local v4, "res":Landroid/content/res/Resources;
    if-eqz p4, :cond_2

    .line 282
    const v5, 0x7f0f0186

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "contentText":Ljava/lang/String;
    const-string v3, ""

    .line 284
    .local v3, "publicContentText":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const v6, 0x7f0f0184

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const v6, 0x7f02010e

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 287
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const v6, 0x7f02010e

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const v6, 0x7f0f0183

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p3, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 307
    :goto_1
    iput-object p2, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadedVolumeId:Ljava/lang/String;

    .line 309
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 315
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->updateDownloadNotification()V

    goto :goto_0

    .line 291
    .end local v1    # "contentText":Ljava/lang/String;
    .end local v3    # "publicContentText":Ljava/lang/String;
    :cond_2
    const v5, 0x7f0f0187

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 292
    .restart local v1    # "contentText":Ljava/lang/String;
    move-object v3, v1

    .line 293
    .restart local v3    # "publicContentText":Ljava/lang/String;
    const/4 v5, 0x0

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalHomeIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v2, v5, v6, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 297
    .local v0, "clickIntent":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const v6, 0x7f02010d

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 301
    iget-object v5, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const v6, 0x7f02010d

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    const v6, 0x7f0f0185

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p3, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    goto/16 :goto_1
.end method

.method public shouldNotifyByDefault()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public startingVolumeDownload(Lcom/google/android/apps/books/model/VolumeData;IILandroid/accounts/Account;)V
    .locals 20
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p2, "volumeNumber"    # I
    .param p3, "volumeCount"    # I
    .param p4, "account"    # Landroid/accounts/Account;

    .prologue
    .line 135
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    .line 136
    .local v12, "context":Landroid/content/Context;
    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 137
    .local v3, "res":Landroid/content/res/Resources;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v16

    .line 138
    .local v16, "volumeId":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeData;->getAuthor()Ljava/lang/String;

    move-result-object v6

    .line 139
    .local v6, "author":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, "title":Ljava/lang/String;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadedVolumeId:Ljava/lang/String;

    .line 141
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadingVolumeId:Ljava/lang/String;

    .line 143
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move/from16 v4, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->getBookDownloadNotificationTitle(Landroid/content/res/Resources;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v15

    .line 145
    .local v15, "titleText":Ljava/lang/CharSequence;
    const v2, 0x7f0f0181

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 148
    .local v13, "publicTitleText":Ljava/lang/CharSequence;
    const-string v2, "SyncUI"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    const-string v2, "SyncUI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startingVolumeDownload("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v12, v0, v2}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 153
    .local v10, "booksIntent":Landroid/content/Intent;
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static {v12, v2, v10, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    .line 155
    .local v11, "clickIntent":Landroid/app/PendingIntent;
    const v2, 0x7f0b00be

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 157
    .local v8, "appColor":I
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/TabletBooksApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v4, 0x1080081

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const-string v4, "progress"

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 168
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    invoke-virtual {v4}, Lcom/google/android/apps/books/app/TabletBooksApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v4, 0x1080081

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const v4, 0x7f0f017f

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v17, 0x0

    aput-object v5, v7, v17

    invoke-virtual {v3, v4, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const-string v4, "progress"

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v9

    .line 184
    .local v9, "bdc":Lcom/google/android/apps/books/data/BooksDataController;
    new-instance v14, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v14, v0, v1}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi$1;-><init>(Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;Ljava/lang/String;)V

    .line 225
    .local v14, "thumbnailConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    move-object/from16 v0, p1

    invoke-static {v9, v0, v3, v14}, Lcom/google/android/apps/books/data/DataControllerUtils;->getNotificationThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Landroid/content/res/Resources;Lcom/google/android/ublib/utils/Consumer;)V

    .line 227
    const/4 v2, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->updateVolumeDownload(I)V

    .line 228
    return-void
.end method

.method public updateVolumeDownload(I)V
    .locals 10
    .param p1, "progressPercent"    # I

    .prologue
    const/16 v5, 0x32

    const/4 v4, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x64

    .line 242
    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-nez v3, :cond_0

    .line 266
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->this$0:Lcom/google/android/apps/books/app/TabletBooksApplication;

    .line 247
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 249
    .local v2, "res":Landroid/content/res/Resources;
    if-ltz p1, :cond_2

    .line 250
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v3

    int-to-double v4, p1

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 252
    .local v1, "percentDownloaded":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v3, v8, p1, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 254
    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v3, v8, p1, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 257
    const-string v3, "SyncUI"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 258
    const-string v3, "SyncUI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateVolumeDownload("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    .end local v1    # "percentDownloaded":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->updateDownloadNotification()V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v3, v8, v5, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 262
    iget-object v3, p0, Lcom/google/android/apps/books/app/TabletBooksApplication$TabletSyncUi;->mPublicDownloadNotificationBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v3, v8, v5, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1
.end method
