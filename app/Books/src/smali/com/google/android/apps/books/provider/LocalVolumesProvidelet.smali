.class Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;
.super Lcom/google/android/apps/books/provider/NotifyingProvidelet;
.source "LocalVolumesProvidelet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/provider/LocalVolumesProvidelet$Query;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LocalVolumesProvidelet"

.field private static sNativeVolumeColumns:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sUpdateCounter:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sUpdateCounter:Ljava/util/Map;

    .line 68
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sNativeVolumeColumns:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V
    .locals 0
    .param p1, "notifier"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
    .param p2, "database"    # Lcom/google/android/apps/books/provider/database/BooksDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;",
            "Lcom/google/android/apps/books/provider/database/BooksDatabase;",
            "Lcom/google/android/apps/books/util/pool/Pool",
            "<",
            "Lcom/google/android/apps/books/util/SelectionBuilder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p3, "builderPool":Lcom/google/android/apps/books/util/pool/Pool;, "Lcom/google/android/apps/books/util/pool/Pool<Lcom/google/android/apps/books/util/SelectionBuilder;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/provider/NotifyingProvidelet;-><init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;Lcom/google/android/apps/books/provider/database/BooksDatabase;Lcom/google/android/apps/books/util/pool/Pool;)V

    .line 66
    return-void
.end method

.method private augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)V
    .locals 5
    .param p1, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257
    sparse-switch p2, :sswitch_data_0

    .line 268
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad match "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :sswitch_0
    const-string v0, "account_name=?"

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getAccountName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 270
    :goto_0
    :sswitch_1
    return-void

    .line 264
    :sswitch_2
    const-string v0, "account_name=?"

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getAccountName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 265
    const-string v0, "volume_id=?"

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getVolumeId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->where(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    goto :goto_0

    .line 257
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x6e -> :sswitch_0
        0x78 -> :sswitch_2
    .end sparse-switch
.end method

.method private deleteVolumes(Lcom/google/android/apps/books/util/SelectionBuilder;Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 1
    .param p1, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 237
    const-string v0, "volumes"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/util/SelectionBuilder;->delete(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    return v0
.end method

.method private static declared-synchronized getNativeVolumeColumns()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    const-class v2, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sNativeVolumeColumns:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 76
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->builder()Lcom/google/common/collect/ImmutableSet$Builder;

    move-result-object v0

    .line 77
    .local v0, "nativeVolumeColumns":Lcom/google/common/collect/ImmutableSet$Builder;, "Lcom/google/common/collect/ImmutableSet$Builder<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/books/provider/database/BooksDatabase;->getVolumeColumnToClass()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->addAll(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableSet$Builder;

    .line 78
    const-string v1, "_count"

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableSet$Builder;->add(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet$Builder;

    .line 79
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet$Builder;->build()Lcom/google/common/collect/ImmutableSet;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sNativeVolumeColumns:Ljava/util/Set;

    .line 81
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sNativeVolumeColumns:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v1

    .line 75
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method static getUpdateCounter(Ljava/lang/String;)I
    .locals 2
    .param p0, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 277
    sget-object v1, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sUpdateCounter:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 278
    .local v0, "count":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private declared-synchronized incrementCounterForVolumesIn(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "oldCursor"    # Landroid/database/Cursor;

    .prologue
    .line 201
    monitor-enter p0

    const/4 v2, -0x1

    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 202
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "volumeId":Ljava/lang/String;
    sget-object v2, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sUpdateCounter:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 205
    .local v0, "currCount":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 206
    sget-object v2, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sUpdateCounter:Ljava/util/Map;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 201
    .end local v0    # "currCount":Ljava/lang/Integer;
    .end local v1    # "volumeId":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 208
    .restart local v0    # "currCount":Ljava/lang/Integer;
    .restart local v1    # "volumeId":Ljava/lang/String;
    :cond_0
    :try_start_1
    sget-object v2, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->sUpdateCounter:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 211
    .end local v0    # "currCount":Ljava/lang/Integer;
    .end local v1    # "volumeId":Ljava/lang/String;
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private updateInTransaction(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 175
    const-string v2, "volumes"

    invoke-virtual {p3, v2}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet$Query;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Lcom/google/android/apps/books/util/SelectionBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 178
    .local v1, "oldCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 180
    const-string v2, "LocalVolumesProvidelet"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    const-string v2, "LocalVolumesProvidelet"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_0
    const-string v2, "volumes"

    invoke-virtual {p3, v2}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/books/util/SelectionBuilder;->update(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)I

    move-result v0

    .line 187
    .local v0, "count":I
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ne v2, v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Bad update count"

    invoke-static {v2, v3}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 188
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->incrementCounterForVolumesIn(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    .line 187
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 192
    .end local v0    # "count":I
    :catchall_0
    move-exception v2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v2
.end method


# virtual methods
.method public deleteWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 224
    invoke-direct {p0, p4, p2, p3}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)V

    .line 225
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 228
    :try_start_0
    invoke-direct {p0, p4, p1}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->deleteVolumes(Lcom/google/android/apps/books/util/SelectionBuilder;Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 229
    .local v0, "count":I
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return v0

    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public insertWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 126
    const/16 v3, 0x64

    if-eq p2, v3, :cond_0

    .line 127
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad match "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for URI "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 131
    :cond_0
    const-string v3, "price_amount"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 132
    const-string v3, "price_currency"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 133
    const-string v3, "suggested_price_amount"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 134
    const-string v3, "suggested_price_currency"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 137
    const-string v3, "account_name"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "accountName":Ljava/lang/String;
    const-string v3, "volume_id"

    invoke-virtual {p4, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 139
    .local v2, "volumeId":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildVolumeUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 141
    .local v1, "newUri":Landroid/net/Uri;
    const-string v3, "volumes"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, p4}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 142
    return-object v1
.end method

.method public openFile(ILandroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p1, "match"    # I
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mode"    # Ljava/lang/String;

    .prologue
    .line 247
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public queryWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/util/SelectionBuilder;)Landroid/database/Cursor;
    .locals 18
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "projection"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 98
    const/16 v16, 0x0

    .line 99
    .local v16, "projectionContainsJoinColumn":Z
    if-eqz p4, :cond_0

    .line 100
    invoke-static {}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->getNativeVolumeColumns()Ljava/util/Set;

    move-result-object v15

    .line 101
    .local v15, "nativeVolumeColumns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v11, p4

    .local v11, "arr$":[Ljava/lang/String;
    array-length v14, v11

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_0
    if-ge v13, v14, :cond_0

    aget-object v12, v11, v13

    .line 102
    .local v12, "column":Ljava/lang/String;
    invoke-interface {v15, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 103
    const/16 v16, 0x1

    .line 108
    .end local v11    # "arr$":[Ljava/lang/String;
    .end local v12    # "column":Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "nativeVolumeColumns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    if-eqz v16, :cond_2

    const-string v17, "view_volumes"

    .line 111
    .local v17, "table":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)V

    .line 113
    const-string v4, "_count"

    const-string v5, "COUNT(*)"

    move-object/from16 v0, p6

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/books/util/SelectionBuilder;->map(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    .line 115
    move-object/from16 v0, p6

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/SelectionBuilder;->table(Ljava/lang/String;)Lcom/google/android/apps/books/util/SelectionBuilder;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p1

    move-object/from16 v6, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/apps/books/util/SelectionBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    return-object v4

    .line 101
    .end local v17    # "table":Ljava/lang/String;
    .restart local v11    # "arr$":[Ljava/lang/String;
    .restart local v12    # "column":Ljava/lang/String;
    .restart local v13    # "i$":I
    .restart local v14    # "len$":I
    .restart local v15    # "nativeVolumeColumns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 108
    .end local v11    # "arr$":[Ljava/lang/String;
    .end local v12    # "column":Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "nativeVolumeColumns":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    const-string v17, "volumes"

    goto :goto_1
.end method

.method public updateWithoutNotify(Landroid/database/sqlite/SQLiteDatabase;ILandroid/net/Uri;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "match"    # I
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "values"    # Landroid/content/ContentValues;
    .param p5, "builder"    # Lcom/google/android/apps/books/util/SelectionBuilder;

    .prologue
    .line 156
    invoke-direct {p0, p5, p2, p3}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->augmentSelectionBuilder(Lcom/google/android/apps/books/util/SelectionBuilder;ILandroid/net/Uri;)V

    .line 158
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 160
    :try_start_0
    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/apps/books/provider/LocalVolumesProvidelet;->updateInTransaction(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Lcom/google/android/apps/books/util/SelectionBuilder;)I

    move-result v0

    .line 161
    .local v0, "count":I
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return v0

    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method
