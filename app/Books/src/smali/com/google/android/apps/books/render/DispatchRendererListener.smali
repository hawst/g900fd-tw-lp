.class public Lcom/google/android/apps/books/render/DispatchRendererListener;
.super Ljava/lang/Object;
.source "DispatchRendererListener.java"

# interfaces
.implements Lcom/google/android/apps/books/render/RendererListener;


# instance fields
.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/render/RendererListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 67
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/apps/books/render/RendererListener;->onActivatedMoElement(IILjava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 8
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "textPointsJson"    # Ljava/lang/String;
    .param p4, "handlesPointsJson"    # Ljava/lang/String;
    .param p5, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 50
    iget-object v1, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RendererListener;

    .local v0, "listener":Lcom/google/android/apps/books/render/RendererListener;
    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 51
    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/render/RendererListener;->onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V

    goto :goto_0

    .line 54
    .end local v0    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 7
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RendererListener;

    .local v0, "listener":Lcom/google/android/apps/books/render/RendererListener;
    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    .line 60
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/RendererListener;->onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 62
    .end local v0    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onNewSelectionBegins()V
    .locals 3

    .prologue
    .line 82
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 83
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1}, Lcom/google/android/apps/books/render/RendererListener;->onNewSelectionBegins()V

    goto :goto_0

    .line 85
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onPagesNeedRedraw()V
    .locals 3

    .prologue
    .line 111
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 112
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1}, Lcom/google/android/apps/books/render/RendererListener;->onPagesNeedRedraw()V

    goto :goto_0

    .line 114
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 76
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/apps/books/render/RendererListener;->onPassageMoListReady(IILjava/util/Map;)V

    goto :goto_0

    .line 78
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 40
    iget-object v1, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RendererListener;

    .local v0, "listener":Lcom/google/android/apps/books/render/RendererListener;
    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 41
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/RendererListener;->onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    goto :goto_0

    .line 44
    .end local v0    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onPassagesPurged(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 105
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/books/render/RendererListener;->onPassagesPurged(Ljava/util/Collection;)V

    goto :goto_0

    .line 107
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onRenderError(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/Exception;

    .prologue
    .line 32
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 33
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/books/render/RendererListener;->onRenderError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 35
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V
    .locals 3
    .param p3, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Landroid/graphics/Rect;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "handleRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    .local p2, "textRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Landroid/graphics/Rect;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 91
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/apps/books/render/RendererListener;->onSelectionAppearanceChanged(Lcom/google/android/apps/books/widget/Walker;Lcom/google/android/apps/books/widget/Walker;I)V

    goto :goto_0

    .line 93
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V
    .locals 3
    .param p1, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 97
    iget-object v2, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/render/RendererListener;

    .line 98
    .local v1, "listener":Lcom/google/android/apps/books/render/RendererListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/books/render/RendererListener;->onSelectionStateChanged(Lcom/google/android/apps/books/app/SelectionState;)V

    goto :goto_0

    .line 100
    .end local v1    # "listener":Lcom/google/android/apps/books/render/RendererListener;
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public weaklyAddListener(Lcom/google/android/apps/books/render/RendererListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/render/RendererListener;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/books/render/DispatchRendererListener;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method
