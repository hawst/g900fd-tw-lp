.class Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;
.super Lcom/google/android/apps/books/annotations/StubAnnotationListener;
.source "ListeningAnnotationSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;-><init>(Lcom/google/android/apps/books/common/Position$PageOrdering;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/StubAnnotationListener;-><init>()V

    return-void
.end method

.method private notifyListeners(Ljava/lang/String;)V
    .locals 3
    .param p1, "layerId"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    # getter for: Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mListeners:Lcom/google/common/collect/Multimap;
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->access$100(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)Lcom/google/common/collect/Multimap;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/common/collect/Multimap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;

    .line 53
    .local v1, "listener":Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;
    invoke-interface {v1}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;->annotationSetChanged()V

    goto :goto_0

    .line 55
    .end local v1    # "listener":Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$AnnotationSetChangeListener;
    :cond_0
    return-void
.end method


# virtual methods
.method public annotationAdded(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p1, "layerId"    # Ljava/lang/String;
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p3, "newFullSet":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    # getter for: Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mLoadedLayers:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->access$000(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->addAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->notifyListeners(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public annotationsLoaded(Ljava/lang/String;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .param p1, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "annotations":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/common/collect/ImmutableList<Lcom/google/android/apps/books/annotations/Annotation;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    # getter for: Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->mLoadedLayers:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->access$000(Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    invoke-virtual {p2}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->this$0:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    invoke-virtual {p2}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->setAnnotationsForLayer(Ljava/lang/String;Ljava/util/List;)V

    .line 40
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet$1;->notifyListeners(Ljava/lang/String;)V

    .line 41
    return-void
.end method
