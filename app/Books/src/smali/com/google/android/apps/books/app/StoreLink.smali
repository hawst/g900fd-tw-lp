.class public Lcom/google/android/apps/books/app/StoreLink;
.super Ljava/lang/Object;
.source "StoreLink.java"


# instance fields
.field private final mCampaignId:Ljava/lang/String;

.field private final mCollectionId:Ljava/lang/String;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "collectionId"    # Ljava/lang/String;
    .param p3, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/app/StoreLink;->mVolumeId:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/books/app/StoreLink;->mCollectionId:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/google/android/apps/books/app/StoreLink;->mCampaignId:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static forBook(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/app/StoreLink;
    .locals 2
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "campaignId"    # Ljava/lang/String;

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/books/app/StoreLink;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/books/app/StoreLink;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getUri(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;
    .locals 2
    .param p1, "config"    # Lcom/google/android/apps/books/util/Config;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/app/StoreLink;->mVolumeId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/app/StoreLink;->mVolumeId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/app/StoreLink;->mCampaignId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/util/OceanUris;->getBuyTheBookUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/StoreLink;->mCollectionId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/app/StoreLink;->mCampaignId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/util/OceanUris;->getStoreCollectionUrl(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public viewStorePage(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .param p3, "useDirectPurchaseApi"    # Z

    .prologue
    .line 35
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/StoreLink;->getUri(Lcom/google/android/apps/books/util/Config;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "uri":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/app/StoreLink;->mVolumeId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 37
    iget-object v1, p0, Lcom/google/android/apps/books/app/StoreLink;->mVolumeId:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {p2, v1, v0, p3, v2}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->startBuyVolume(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-interface {p2, v0}, Lcom/google/android/apps/books/app/BooksFragmentCallbacks;->startViewCollection(Ljava/lang/String;)V

    goto :goto_0
.end method
