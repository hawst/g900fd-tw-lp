.class public Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
.super Ljava/lang/Object;
.source "SentenceSearchTextProcessor.java"

# interfaces
.implements Lcom/google/android/apps/books/model/LocalContentSearchXmlHandler$SearchTextProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;,
        Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;,
        Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;,
        Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;,
        Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;,
        Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;
    }
.end annotation


# instance fields
.field private mEndOfDocument:Z

.field private final mLocale:Ljava/util/Locale;

.field private final mNumOverlapChars:I

.field private final mPattern:Ljava/util/regex/Pattern;

.field private final mProgressListener:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;

.field private final mSentencePolicy:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;

.field private final mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

.field private final mTracker:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

.field private final mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;


# direct methods
.method public constructor <init>(Ljava/util/regex/Pattern;ILcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;Ljava/util/Locale;Lcom/google/android/apps/books/model/Snippet$Builder;ZLcom/google/android/apps/books/util/Logging$PerformanceTracker;)V
    .locals 2
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "patternSize"    # I
    .param p3, "progressListener"    # Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;
    .param p4, "locale"    # Ljava/util/Locale;
    .param p5, "snippetBuilder"    # Lcom/google/android/apps/books/model/Snippet$Builder;
    .param p6, "includeSentenceBeforeAndAfter"    # Z
    .param p7, "tracker"    # Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .prologue
    const/4 v1, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mEndOfDocument:Z

    .line 251
    iput-object p1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mPattern:Ljava/util/regex/Pattern;

    .line 252
    iput-object p3, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mProgressListener:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;

    .line 253
    iput-object p5, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

    .line 254
    if-eqz p6, :cond_0

    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceBeforeAndAfter;-><init>(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mSentencePolicy:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;

    .line 256
    add-int/lit8 v0, p2, -0x1

    iput v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mNumOverlapChars:I

    .line 257
    new-instance v0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    .line 259
    iput-object p4, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mLocale:Ljava/util/Locale;

    .line 260
    iput-object p7, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTracker:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 261
    return-void

    .line 254
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$OnlyMatchingSentence;-><init>(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$1;)V

    goto :goto_0
.end method

.method private recentTextMatches(I)Z
    .locals 2
    .param p1, "newChunkLength"    # I

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    iget v1, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mNumOverlapChars:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->getUnprocessed(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->recentTextMatches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private recentTextMatches(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public handleEndDocument(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 313
    .local p1, "queue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mEndOfDocument:Z

    .line 314
    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->handleNewText(Ljava/lang/String;Ljava/util/Collection;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mProgressListener:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;->onSearchedUpTo(Lcom/google/android/apps/books/annotations/TextLocation;Ljava/util/Collection;)V

    .line 316
    return-void
.end method

.method public handleNewReadingPosition(Ljava/lang/String;)V
    .locals 1
    .param p1, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->addPosition(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public handleNewText(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 12
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 270
    .local p2, "queue":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/SearchResult;>;"
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-virtual {v9, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->addText(Ljava/lang/String;)V

    .line 272
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mSentencePolicy:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;

    invoke-interface {v9}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;->needsMoreSentencesToFinishCurrentMatch()Z

    move-result v4

    .line 274
    .local v4, "needMoreSentences":Z
    if-nez v4, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->recentTextMatches(I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 275
    :cond_0
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-virtual {v9}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->getUnprocessed()Ljava/lang/String;

    move-result-object v2

    .line 276
    .local v2, "matchText":Ljava/lang/String;
    new-instance v7, Lcom/google/android/apps/books/tts/SentenceSegmentation;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mLocale:Ljava/util/Locale;

    iget-object v11, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mLocale:Ljava/util/Locale;

    invoke-static {v11}, Lcom/google/android/apps/books/model/BreakIteratorManager;->getSentenceIterator(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v11

    invoke-direct {v7, v2, v9, v10, v11}, Lcom/google/android/apps/books/tts/SentenceSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;Ljava/text/BreakIterator;)V

    .line 278
    .local v7, "segmentation":Lcom/google/android/apps/books/tts/SentenceSegmentation;
    invoke-virtual {v7}, Lcom/google/android/apps/books/tts/SentenceSegmentation;->getItemCount()I

    move-result v5

    .line 279
    .local v5, "numSentences":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_5

    .line 280
    invoke-virtual {v7, v1}, Lcom/google/android/apps/books/tts/SentenceSegmentation;->getItemText(I)Ljava/lang/String;

    move-result-object v8

    .line 285
    .local v8, "sentence":Ljava/lang/String;
    add-int/lit8 v9, v5, -0x1

    if-ne v1, v9, :cond_3

    const/4 v0, 0x1

    .line 286
    .local v0, "afterLastCompleteSentence":Z
    :goto_1
    if-eqz v0, :cond_4

    iget-boolean v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mEndOfDocument:Z

    if-nez v9, :cond_4

    const/4 v3, 0x1

    .line 287
    .local v3, "mayBeIncomplete":Z
    :goto_2
    invoke-virtual {p0, v8, v3}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->scanSentence(Ljava/lang/String;Z)Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    move-result-object v6

    .line 289
    .local v6, "result":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->hasMatch()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->isComplete()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 290
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mProgressListener:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->matchStartLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v10

    invoke-interface {v9, v10, p2}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;->onSearchedUpTo(Lcom/google/android/apps/books/annotations/TextLocation;Ljava/util/Collection;)V

    .line 291
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTracker:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Found: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->getSnippet()Lcom/google/android/apps/books/model/Snippet;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 294
    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mSentencePolicy:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;

    iget-boolean v10, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mEndOfDocument:Z

    invoke-interface {v9, v6, p2, v10}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentencePolicy;->onSentence(Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;Ljava/util/Collection;Z)V

    .line 296
    if-nez v0, :cond_2

    .line 297
    iget-object v9, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->onProcessedText(I)V

    .line 279
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 285
    .end local v0    # "afterLastCompleteSentence":Z
    .end local v3    # "mayBeIncomplete":Z
    .end local v6    # "result":Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 286
    .restart local v0    # "afterLastCompleteSentence":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    .line 301
    .end local v0    # "afterLastCompleteSentence":Z
    .end local v1    # "i":I
    .end local v2    # "matchText":Ljava/lang/String;
    .end local v5    # "numSentences":I
    .end local v7    # "segmentation":Lcom/google/android/apps/books/tts/SentenceSegmentation;
    .end local v8    # "sentence":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method public scanSentence(Ljava/lang/String;Z)Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;
    .locals 10
    .param p1, "sentence"    # Ljava/lang/String;
    .param p2, "mayBeContinued"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 319
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v1

    .line 320
    .local v1, "compressedSentenceOffsetToPosition":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v7, v1, v8}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->buildCompressedSentenceAndOffsetToPositionMap(Ljava/util/TreeMap;I)Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "compressed":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 324
    .local v2, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 325
    if-eqz p2, :cond_0

    .line 326
    sget-object v5, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->INCOMPLETE_MATCHING:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    .line 339
    :goto_0
    return-object v5

    .line 328
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 329
    .local v3, "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    iget-object v7, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mTrailingText:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    invoke-virtual {v7, v0, v2, v3, v1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->textRangesWithinSentence(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/util/List;Ljava/util/TreeMap;)V

    .line 331
    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->find(I)Z

    .line 332
    iget-object v6, p0, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;->mSnippetBuilder:Lcom/google/android/apps/books/model/Snippet$Builder;

    invoke-interface {v6, v0, v2}, Lcom/google/android/apps/books/model/Snippet$Builder;->build(Ljava/lang/String;Ljava/util/regex/Matcher;)Lcom/google/android/apps/books/model/Snippet;

    move-result-object v4

    .line 333
    .local v4, "snippet":Lcom/google/android/apps/books/model/Snippet;
    new-instance v6, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    invoke-direct {v6, v3, v4, v5}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;-><init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V

    move-object v5, v6

    goto :goto_0

    .line 336
    .end local v3    # "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    .end local v4    # "snippet":Lcom/google/android/apps/books/model/Snippet;
    :cond_1
    if-eqz p2, :cond_2

    .line 337
    sget-object v5, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;->INCOMPLETE_NOT_MATCHING:Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    goto :goto_0

    .line 339
    :cond_2
    new-instance v7, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;

    const/4 v8, 0x0

    new-instance v9, Lcom/google/android/apps/books/model/Snippet;

    invoke-direct {v9, v0}, Lcom/google/android/apps/books/model/Snippet;-><init>(Ljava/lang/CharSequence;)V

    if-nez p2, :cond_3

    :goto_1
    invoke-direct {v7, v8, v9, v5}, Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceScanResult;-><init>(Ljava/util/List;Lcom/google/android/apps/books/model/Snippet;Z)V

    move-object v5, v7

    goto :goto_0

    :cond_3
    move v5, v6

    goto :goto_1
.end method
