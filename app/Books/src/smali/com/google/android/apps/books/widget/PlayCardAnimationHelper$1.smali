.class Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;
.super Ljava/lang/Object;
.source "PlayCardAnimationHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->playAnimators(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 161
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->recordLocations()V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mIsAnimating:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->access$002(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;Z)Z

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$1;->this$0:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;

    # getter for: Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->mCardGroup:Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;->access$100(Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;)Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;->onAnimationEnd()V

    .line 157
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 165
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 150
    return-void
.end method
