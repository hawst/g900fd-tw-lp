.class public Lcom/google/android/apps/books/render/HoneycombResourceContentStore;
.super Ljava/lang/Object;
.source "HoneycombResourceContentStore.java"

# interfaces
.implements Lcom/google/android/apps/books/render/ResourceContentStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/HoneycombResourceContentStore$ResourceNotFoundException;
    }
.end annotation


# static fields
.field private static final sFontResponseHeadersMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sReasonPhrase:Ljava/lang/String;


# instance fields
.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mInputStreams:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/util/StoppableInputStream;",
            ">;"
        }
    .end annotation
.end field

.field private mShutDown:Z

.field private final mStreamCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "OK"

    sput-object v0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sReasonPhrase:Ljava/lang/String;

    .line 63
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sFontResponseHeadersMap:Ljava/util/LinkedHashMap;

    .line 64
    sget-object v0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sFontResponseHeadersMap:Ljava/util/LinkedHashMap;

    const-string v1, "Access-Control-Allow-Origin"

    const-string v2, "*"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 1
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mInputStreams:Ljava/util/Set;

    .line 71
    new-instance v0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$1;-><init>(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mStreamCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 84
    return-void
.end method

.method private WebResourceResponseWithHeaders(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Ljava/io/InputStream;)Landroid/webkit/WebResourceResponse;
    .locals 7
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "encoding"    # Ljava/lang/String;
    .param p3, "statusCode"    # I
    .param p4, "reasonPhrase"    # Ljava/lang/String;
    .param p6, "data"    # Ljava/io/InputStream;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/InputStream;",
            ")",
            "Landroid/webkit/WebResourceResponse;"
        }
    .end annotation

    .prologue
    .line 341
    .local p5, "responseHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "HCResourceStore"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    const-string v0, "HCResourceStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "webresponse "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_0
    new-instance v0, Landroid/webkit/WebResourceResponse;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Ljava/io/InputStream;)V

    return-object v0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mInputStreams:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HoneycombResourceContentStore;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mShutDown:Z

    return v0
.end method

.method static synthetic access$200()Landroid/webkit/WebResourceResponse;
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildNotFoundResponse()Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/webkit/WebResourceResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HoneycombResourceContentStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/net/Uri;
    .param p4, "x4"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildSectionContentResponse(Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/base/Function;)Landroid/webkit/WebResourceResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HoneycombResourceContentStore;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/net/Uri;
    .param p4, "x4"    # Lcom/google/common/base/Function;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildResourceContentResponse(Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/base/Function;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/HoneycombResourceContentStore;
    .param p1, "x1"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->buildSharedResourceContentResponse(Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    return-object v0
.end method

.method private static buildNotFoundResponse()Landroid/webkit/WebResourceResponse;
    .locals 4

    .prologue
    .line 225
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v1, "text/plain"

    const-string v2, "UTF-8"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    return-object v0
.end method

.method private buildResourceContentResponse(Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/base/Function;)Landroid/webkit/WebResourceResponse;
    .locals 13
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "resourceContentUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)",
            "Landroid/webkit/WebResourceResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    .local p4, "resourceIdToResource":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->getResourceId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    .line 269
    .local v10, "resId":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-interface {v0, v10}, Lcom/google/common/base/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/model/Resource;

    .line 270
    .local v11, "resource":Lcom/google/android/apps/books/model/Resource;
    if-nez v11, :cond_0

    .line 271
    new-instance v1, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$ResourceNotFoundException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No such resource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$ResourceNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 273
    :cond_0
    invoke-direct {p0, p2, v11}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->getContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;

    move-result-object v8

    .line 274
    .local v8, "content":Ljava/io/InputStream;
    invoke-interface {v11}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v12

    .line 276
    .local v12, "retrievedMimeType":Ljava/lang/String;
    const-string v2, "application/octet-stream"

    .line 277
    .local v2, "mimeType":Ljava/lang/String;
    const-string v3, "UTF-8"

    .line 278
    .local v3, "encoding":Ljava/lang/String;
    if-eqz v12, :cond_2

    .line 279
    invoke-direct {p0, v12}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->hasAsciiEncoding(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    const-string v3, "ASCII"

    .line 282
    :cond_1
    move-object v2, v12

    .line 288
    :cond_2
    const-string v9, ""

    .line 290
    .local v9, "debugString":Ljava/lang/String;
    new-instance v7, Lcom/google/android/apps/books/util/StoppableInputStream;

    iget-object v1, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mStreamCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

    const-string v4, ""

    invoke-direct {v7, v8, v1, v4}, Lcom/google/android/apps/books/util/StoppableInputStream;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;Ljava/lang/String;)V

    .line 292
    .local v7, "sis":Lcom/google/android/apps/books/util/StoppableInputStream;
    iget-object v1, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mInputStreams:Ljava/util/Set;

    invoke-interface {v1, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 294
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v2}, Lcom/google/android/apps/books/model/ResourceUtils;->isFontResourceType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 302
    const/16 v4, 0xc8

    sget-object v5, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sReasonPhrase:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sFontResponseHeadersMap:Ljava/util/LinkedHashMap;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->WebResourceResponseWithHeaders(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Ljava/io/InputStream;)Landroid/webkit/WebResourceResponse;

    move-result-object v1

    .line 305
    :goto_0
    return-object v1

    :cond_3
    new-instance v1, Landroid/webkit/WebResourceResponse;

    invoke-direct {v1, v2, v3, v7}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method private buildSectionContentResponse(Landroid/accounts/Account;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/webkit/WebResourceResponse;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "segmentContentUri"    # Landroid/net/Uri;
    .param p4, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    invoke-static {p3}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->getSectionId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 237
    .local v4, "segmentId":Ljava/lang/String;
    invoke-interface {p4}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getValueWithId(Ljava/lang/String;)Lcom/google/android/apps/books/util/Identifiable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    .line 238
    .local v3, "segment":Lcom/google/android/apps/books/model/Segment;
    if-nez v3, :cond_0

    .line 239
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No segment for ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 244
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {v5, p2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->getSegmentContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 249
    .local v0, "content":Ljava/lang/String;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 250
    .local v2, "input":Ljava/io/InputStream;
    new-instance v5, Landroid/webkit/WebResourceResponse;

    const-string v6, "application/octet-stream"

    const-string v7, "UTF-8"

    invoke-direct {v5, v6, v7, v2}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    return-object v5

    .line 245
    .end local v0    # "content":Ljava/lang/String;
    .end local v2    # "input":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 246
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v5

    throw v5
.end method

.method private buildSharedResourceContentResponse(Landroid/net/Uri;)Landroid/webkit/WebResourceResponse;
    .locals 12
    .param p1, "resourceContentUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    invoke-static {p1}, Lcom/google/android/apps/books/provider/BooksContract$SharedResources;->getSharedResourceId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v11

    .line 321
    .local v11, "resId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v0, v11}, Lcom/google/android/apps/books/data/BooksDataController;->getSharedResourceContent(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    .line 322
    .local v7, "content":Ljava/io/InputStream;
    const-string v10, "application/vnd.ms-opentype"

    .line 323
    .local v10, "mimeType":Ljava/lang/String;
    const-string v9, "UTF-8"

    .line 325
    .local v9, "encoding":Ljava/lang/String;
    const-string v8, ""

    .line 326
    .local v8, "debugString":Ljava/lang/String;
    new-instance v6, Lcom/google/android/apps/books/util/StoppableInputStream;

    iget-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mStreamCallbacks:Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;

    const-string v1, ""

    invoke-direct {v6, v7, v0, v1}, Lcom/google/android/apps/books/util/StoppableInputStream;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/books/util/StoppableInputStream$Callbacks;Ljava/lang/String;)V

    .line 328
    .local v6, "sis":Lcom/google/android/apps/books/util/StoppableInputStream;
    iget-object v0, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mInputStreams:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 330
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    const-string v1, "application/vnd.ms-opentype"

    const-string v2, "UTF-8"

    const/16 v3, 0xc8

    sget-object v4, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sReasonPhrase:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->sFontResponseHeadersMap:Ljava/util/LinkedHashMap;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->WebResourceResponseWithHeaders(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;Ljava/io/InputStream;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    .line 334
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v1, "application/vnd.ms-opentype"

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2, v6}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method private getContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/books/data/DataControllerUtils;->getResourceContent(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v1

    throw v1
.end method

.method private hasAsciiEncoding(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 361
    const-string v0, "image/svg+xml"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public configureWebView(Landroid/accounts/Account;Ljava/lang/String;Landroid/webkit/WebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Landroid/webkit/WebViewClient;)V
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "webView"    # Landroid/webkit/WebView;
    .param p5, "callback"    # Lcom/google/android/apps/books/render/ReaderListener;
    .param p6, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .param p8, "clientDelegate"    # Landroid/webkit/WebViewClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Landroid/webkit/WebView;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Exception;",
            ">;",
            "Lcom/google/android/apps/books/render/ReaderListener;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;",
            "Landroid/webkit/WebViewClient;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    .local p4, "exceptionHandler":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Exception;>;"
    .local p7, "resourceIdToResource":Lcom/google/common/base/Function;, "Lcom/google/common/base/Function<Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;>;"
    new-instance v0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;

    move-object v1, p0

    move-object/from16 v2, p8

    move-object v3, p1

    move-object v4, p2

    move-object v5, p6

    move-object v6, p7

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/books/render/HoneycombResourceContentStore$2;-><init>(Lcom/google/android/apps/books/render/HoneycombResourceContentStore;Landroid/webkit/WebViewClient;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Lcom/google/android/ublib/utils/Consumer;)V

    .line 216
    .local v0, "webViewClient":Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;
    invoke-virtual {v0, p5}, Lcom/google/android/apps/books/render/ContentEnsuringWebViewClient;->setCallback(Lcom/google/android/apps/books/render/ReaderListener;)V

    .line 217
    invoke-virtual {p3, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 218
    return-void
.end method

.method public getResourceContentUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "resource"    # Lcom/google/android/apps/books/model/Resource;

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/apps/books/data/ResourcePolicies;->HONEYCOMB:Lcom/google/android/apps/books/data/ResourcePolicy;

    invoke-interface {p3}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/books/data/ResourcePolicy;->getResourceContentUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public shutDown()V
    .locals 3

    .prologue
    .line 90
    iget-object v2, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mInputStreams:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/util/StoppableInputStream;

    .line 91
    .local v1, "s":Lcom/google/android/apps/books/util/StoppableInputStream;
    invoke-virtual {v1}, Lcom/google/android/apps/books/util/StoppableInputStream;->stop()V

    goto :goto_0

    .line 93
    .end local v1    # "s":Lcom/google/android/apps/books/util/StoppableInputStream;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mInputStreams:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 94
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/books/render/HoneycombResourceContentStore;->mShutDown:Z

    .line 95
    return-void
.end method
