.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OnboardingAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

.field public static final enum BACK_FROM_GENRE_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

.field public static final enum BACK_FROM_TUTORIAL_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

.field public static final enum REQUESTED_GENRES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

.field public static final enum REQUESTED_SAMPLES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

.field public static final enum SKIPPED_FROM_GENRES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

.field public static final enum SKIPPED_FROM_SAMPLES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 517
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const-string v1, "SKIPPED_FROM_GENRES_PAGE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->SKIPPED_FROM_GENRES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 522
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const-string v1, "BACK_FROM_GENRE_PAGE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->BACK_FROM_GENRE_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 527
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const-string v1, "BACK_FROM_TUTORIAL_PAGE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->BACK_FROM_TUTORIAL_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 532
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const-string v1, "SKIPPED_FROM_SAMPLES_PAGE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->SKIPPED_FROM_SAMPLES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 537
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const-string v1, "REQUESTED_GENRES"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->REQUESTED_GENRES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 542
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    const-string v1, "REQUESTED_SAMPLES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->REQUESTED_SAMPLES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    .line 513
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->SKIPPED_FROM_GENRES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->BACK_FROM_GENRE_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->BACK_FROM_TUTORIAL_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->SKIPPED_FROM_SAMPLES_PAGE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->REQUESTED_GENRES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->REQUESTED_SAMPLES:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 513
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;
    .locals 1

    .prologue
    .line 513
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$OnboardingAction;

    return-object v0
.end method
