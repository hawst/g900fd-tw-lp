.class public Lcom/google/android/apps/books/navigation/GridRowListAdapter;
.super Landroid/widget/BaseAdapter;
.source "GridRowListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Destroyable;
.implements Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;


# instance fields
.field private final mAdapters:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/navigation/PageListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

.field private final mCollapseDrawable:Landroid/graphics/drawable/Drawable;

.field private final mCollapsedChapters:Landroid/util/SparseBooleanArray;

.field private final mCurrentPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

.field private final mCurrentRow:I

.field private final mExpandDrawable:Landroid/graphics/drawable/Drawable;

.field private mFullScreenSpreadSize:Landroid/graphics/Point;

.field private mHideCurrentSpread:Z

.field private mIsDestroyed:Z

.field private final mRenderer:Lcom/google/android/apps/books/render/Renderer;

.field private final mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

.field private final mRowHeight:I

.field private final mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

.field private mScrollFirstVisible:I

.field private mScrollLastVisible:I

.field private final mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

.field private final mView:Lcom/google/android/ublib/widget/AbsWarpListView;

.field private final mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/widget/AbsWarpListView;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;Landroid/graphics/Point;)V
    .locals 9
    .param p1, "view"    # Lcom/google/android/ublib/widget/AbsWarpListView;
    .param p2, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p4, "currentPosition"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p5, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p7, "rowStateManager"    # Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
    .param p8, "fullScreenSpreadSize"    # Landroid/graphics/Point;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/widget/AbsWarpListView;",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            "Lcom/google/android/apps/books/render/Renderer;",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/widget/BookView$Callbacks;",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;",
            "Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 117
    .local p6, "fallback":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<Lcom/google/android/apps/books/navigation/NavPageKey;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mAdapters:Ljava/util/Set;

    .line 68
    new-instance v6, Landroid/util/SparseBooleanArray;

    invoke-direct {v6}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapsedChapters:Landroid/util/SparseBooleanArray;

    .line 72
    const v6, 0x7fffffff

    iput v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollFirstVisible:I

    const v6, 0x7fffffff

    iput v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollLastVisible:I

    .line 74
    new-instance v6, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter$1;-><init>(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)V

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    .line 101
    new-instance v6, Lcom/google/android/apps/books/navigation/GridRowListAdapter$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter$2;-><init>(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)V

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

    .line 118
    iput-object p1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 119
    iput-object p2, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    .line 120
    iput-object p3, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    .line 121
    iput-object p5, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    .line 122
    iput-object p4, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 123
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mFullScreenSpreadSize:Landroid/graphics/Point;

    .line 125
    new-instance v5, Lcom/google/android/apps/books/render/SpreadItems;

    iget-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v6}, Lcom/google/android/apps/books/render/Renderer;->displayTwoPages()Z

    move-result v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    .line 126
    .local v5, "spreadPages":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/render/PageHandle;>;"
    invoke-interface {p3, p4, v5}, Lcom/google/android/apps/books/render/Renderer;->getSpreadPageHandles(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadItems;)Lcom/google/android/apps/books/render/SpreadItems;

    .line 128
    const/4 v4, -0x1

    .line 130
    .local v4, "row":I
    invoke-virtual {v5}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v6

    add-int/lit8 v2, v6, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 131
    invoke-virtual {v5, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/render/PageHandle;

    invoke-interface {v6}, Lcom/google/android/apps/books/render/PageHandle;->getGridRowIndex()I

    move-result v4

    .line 132
    const/4 v6, -0x1

    if-eq v4, v6, :cond_1

    .line 137
    :cond_0
    iput v4, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentRow:I

    .line 139
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 141
    .local v1, "context":Landroid/content/Context;
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v6}, Lcom/google/android/ublib/widget/AbsWarpListView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x4

    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901b0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowHeight:I

    .line 144
    new-instance v6, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;

    const/4 v7, 0x1

    invoke-direct {v6, v7, p6}, Lcom/google/android/apps/books/util/NativeHeapBitmapCache;-><init>(ZLcom/google/android/apps/books/util/BitmapCache;)V

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    .line 146
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .line 147
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v6, v7}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->addRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 148
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v6}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 152
    .local v3, "res":Landroid/content/res/Resources;
    const v6, 0x7f02021b

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mExpandDrawable:Landroid/graphics/drawable/Drawable;

    .line 153
    const v6, 0x7f020218

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapseDrawable:Landroid/graphics/drawable/Drawable;

    .line 154
    return-void

    .line 130
    .end local v1    # "context":Landroid/content/Context;
    .end local v3    # "res":Landroid/content/res/Resources;
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/navigation/GridRowListAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollFirstVisible:I

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/navigation/GridRowListAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollLastVisible:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)Landroid/util/SparseBooleanArray;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapsedChapters:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/navigation/GridRowListAdapter;)Lcom/google/android/ublib/widget/AbsWarpListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    return-object v0
.end method

.method private getRowStartPosition(I)Lcom/google/android/apps/books/common/Position;
    .locals 1
    .param p1, "row"    # I

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 301
    const/4 v0, 0x0

    .line 303
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/Renderer;->getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    goto :goto_0
.end method

.method private removeAdapter(Lcom/google/android/ublib/widget/AbsWarpListView;)V
    .locals 2
    .param p1, "pagesView"    # Lcom/google/android/ublib/widget/AbsWarpListView;

    .prologue
    .line 206
    invoke-virtual {p1}, Lcom/google/android/ublib/widget/AbsWarpListView;->getAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 207
    .local v0, "oldAdapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    if-eqz v0, :cond_0

    .line 208
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 209
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->destroy()V

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->removeListView(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mAdapters:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 213
    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mIsDestroyed:Z

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRendererListener:Lcom/google/android/apps/books/render/RendererListener;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BookView$Callbacks;->removeRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->destroy()V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/BitmapCache;->clear()V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWeakOnScrollListeners()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollListener:Lcom/google/android/ublib/widget/AbsWarpListView$OnScrollListener;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 167
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/Renderer;->getGridRowCount()I

    move-result v0

    return v0
.end method

.method public getCurrentRow()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentRow:I

    return v0
.end method

.method public getCurrentSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 4

    .prologue
    .line 364
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mAdapters:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 365
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getCurrentSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v1

    .line 366
    .local v1, "currentSpreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    if-eqz v1, :cond_0

    .line 370
    .end local v0    # "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    .end local v1    # "currentSpreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItem(I)Lcom/google/android/apps/books/common/Position;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getItem(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 196
    int-to-long v0, p1

    return-wide v0
.end method

.method public getNegativeCount()I
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 4

    .prologue
    .line 374
    iget-object v3, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mAdapters:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/PageListAdapter;

    .line 375
    .local v0, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/PageListAdapter;->getSelectedView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v2

    .line 376
    .local v2, "selectedSpreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    if-eqz v2, :cond_0

    .line 380
    .end local v0    # "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    .end local v2    # "selectedSpreadView":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 23
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 222
    const-class v21, Landroid/view/ViewGroup;

    const v22, 0x7f040077

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/util/ViewUtils;->getChildView(Ljava/lang/Class;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup;

    .line 225
    .local v18, "rowView":Landroid/view/ViewGroup;
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v20

    .line 226
    .local v20, "startPosition":Lcom/google/android/apps/books/common/Position;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v12

    .line 229
    .local v12, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v12}, Lcom/google/android/apps/books/render/PageHandle;->getFirstChapter()Lcom/google/android/apps/books/model/Chapter;

    move-result-object v4

    .line 231
    .local v4, "chapter":Lcom/google/android/apps/books/model/Chapter;
    const v21, 0x7f0e00c4

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 237
    .local v5, "chapterTitleView":Landroid/widget/TextView;
    add-int/lit8 v16, p1, -0x1

    .line 238
    .local v16, "previousRowIndex":I
    if-ltz v16, :cond_0

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/Renderer;->getGridRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v17

    .line 241
    .local v17, "previousRowStartPosition":Lcom/google/android/apps/books/common/Position;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lcom/google/android/apps/books/render/Renderer;->getPageHandle(Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v7

    .line 243
    .local v7, "firstPageInPreviousRow":Lcom/google/android/apps/books/render/PageHandle;
    invoke-interface {v7}, Lcom/google/android/apps/books/render/PageHandle;->getFirstChapter()Lcom/google/android/apps/books/model/Chapter;

    move-result-object v15

    .line 248
    .end local v7    # "firstPageInPreviousRow":Lcom/google/android/apps/books/render/PageHandle;
    .end local v17    # "previousRowStartPosition":Lcom/google/android/apps/books/common/Position;
    .local v15, "previousRowChapter":Lcom/google/android/apps/books/model/Chapter;
    :goto_0
    if-eq v4, v15, :cond_1

    .line 249
    invoke-interface {v4}, Lcom/google/android/apps/books/model/Chapter;->getTitle()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    :goto_1
    const v21, 0x7f0e0158

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 256
    .local v19, "startPage":Landroid/widget/TextView;
    invoke-interface {v12}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPage()Lcom/google/android/apps/books/model/Page;

    move-result-object v11

    .line 258
    .local v11, "page":Lcom/google/android/apps/books/model/Page;
    invoke-interface {v11}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    const v21, 0x7f0e015a

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/ublib/widget/HorizontalWarpListView;

    .line 263
    .local v13, "pagesView":Lcom/google/android/ublib/widget/HorizontalWarpListView;
    invoke-virtual {v13}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 264
    .local v9, "listParams":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 265
    invoke-virtual {v13, v9}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapsedChapters:Landroid/util/SparseBooleanArray;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v8

    .line 269
    .local v8, "isCollapsed":Z
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->removeAdapter(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 271
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setCenterLockPosition(I)V

    .line 273
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 276
    .local v14, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v21, v0

    mul-int/lit8 v21, v21, 0x8

    div-int/lit8 v10, v21, 0xa

    .line 278
    .local v10, "margin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setRTL(Z)V

    .line 279
    const/high16 v21, 0x3f800000    # 1.0f

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setViewAlignment(F)V

    .line 280
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setSnapMode(I)V

    .line 281
    invoke-virtual {v13, v10, v10}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setGaps(II)V

    .line 283
    if-eqz v8, :cond_2

    const/16 v21, 0x8

    :goto_2
    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/google/android/ublib/widget/HorizontalWarpListView;->setVisibility(I)V

    .line 285
    const v21, 0x7f0e0159

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 286
    .local v6, "collapseButton":Landroid/widget/ImageButton;
    if-eqz v8, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mExpandDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v21, v0

    :goto_3
    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 288
    new-instance v21, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/apps/books/navigation/GridRowListAdapter$3;-><init>(Lcom/google/android/apps/books/navigation/GridRowListAdapter;IZ)V

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    return-object v18

    .line 245
    .end local v6    # "collapseButton":Landroid/widget/ImageButton;
    .end local v8    # "isCollapsed":Z
    .end local v9    # "listParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v10    # "margin":I
    .end local v11    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v13    # "pagesView":Lcom/google/android/ublib/widget/HorizontalWarpListView;
    .end local v14    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v15    # "previousRowChapter":Lcom/google/android/apps/books/model/Chapter;
    .end local v19    # "startPage":Landroid/widget/TextView;
    :cond_0
    const/4 v15, 0x0

    .restart local v15    # "previousRowChapter":Lcom/google/android/apps/books/model/Chapter;
    goto/16 :goto_0

    .line 252
    :cond_1
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 283
    .restart local v8    # "isCollapsed":Z
    .restart local v9    # "listParams":Landroid/view/ViewGroup$LayoutParams;
    .restart local v10    # "margin":I
    .restart local v11    # "page":Lcom/google/android/apps/books/model/Page;
    .restart local v13    # "pagesView":Lcom/google/android/ublib/widget/HorizontalWarpListView;
    .restart local v14    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v19    # "startPage":Landroid/widget/TextView;
    :cond_2
    const/16 v21, 0x0

    goto :goto_2

    .line 286
    .restart local v6    # "collapseButton":Landroid/widget/ImageButton;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapseDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v21, v0

    goto :goto_3
.end method

.method public getViewSize(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 217
    const/4 v0, -0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mIsDestroyed:Z

    return v0
.end method

.method public isViewPinned(Landroid/view/View;I)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 356
    iget v0, p0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentRow:I

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public preload(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 187
    return-void
.end method

.method public setViewVisible(Landroid/view/View;IZ)V
    .locals 26
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "isVisible"    # Z

    .prologue
    .line 308
    const v3, 0x7f0e015a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 309
    .local v14, "pagesView":Lcom/google/android/ublib/widget/AbsWarpListView;
    invoke-virtual {v14}, Lcom/google/android/ublib/widget/AbsWarpListView;->getAdapter()Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;

    move-result-object v3

    if-eqz v3, :cond_1

    const/16 v24, 0x1

    .line 311
    .local v24, "currentVisible":Z
    :goto_0
    move/from16 v0, p3

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 352
    :cond_0
    :goto_1
    return-void

    .line 309
    .end local v24    # "currentVisible":Z
    :cond_1
    const/16 v24, 0x0

    goto :goto_0

    .line 315
    .restart local v24    # "currentVisible":Z
    :cond_2
    if-nez p3, :cond_4

    .line 316
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollFirstVisible:I

    move/from16 v0, p2

    if-lt v0, v3, :cond_3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollLastVisible:I

    move/from16 v0, p2

    if-gt v0, v3, :cond_3

    const/16 v25, 0x1

    .line 318
    .local v25, "isScrollTarget":Z
    :goto_2
    invoke-virtual/range {p0 .. p2}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->isViewPinned(Landroid/view/View;I)Z

    move-result v3

    if-nez v3, :cond_0

    if-nez v25, :cond_0

    .line 319
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->removeAdapter(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    goto :goto_1

    .line 316
    .end local v25    # "isScrollTarget":Z
    :cond_3
    const/16 v25, 0x0

    goto :goto_2

    .line 321
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getScrollState()I

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollFirstVisible:I

    move/from16 v0, p2

    if-lt v0, v3, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mScrollLastVisible:I

    move/from16 v0, p2

    if-gt v0, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCollapsedChapters:Landroid/util/SparseBooleanArray;

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 325
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v7

    .line 326
    .local v7, "startPosition":Lcom/google/android/apps/books/common/Position;
    add-int/lit8 v3, p2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getRowStartPosition(I)Lcom/google/android/apps/books/common/Position;

    move-result-object v8

    .line 328
    .local v8, "endPosition":Lcom/google/android/apps/books/common/Position;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentRow:I

    move/from16 v0, p2

    if-ne v0, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 331
    .local v9, "basePosition":Lcom/google/android/apps/books/render/SpreadIdentifier;
    :goto_3
    new-instance v2, Lcom/google/android/apps/books/navigation/PageRangeLoader;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mBitmapCache:Lcom/google/android/apps/books/util/BitmapCache;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRenderer:Lcom/google/android/apps/books/render/Renderer;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    sget-object v10, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    const/4 v11, 0x4

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/books/navigation/PageRangeLoader;-><init>(Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/SpreadIdentifier;Landroid/graphics/Bitmap$Config;I)V

    .line 335
    .local v2, "pageRangeLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mView:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v3}, Lcom/google/android/ublib/widget/AbsWarpListView;->getWidth()I

    move-result v3

    div-int/lit8 v16, v3, 0x2

    .line 337
    .local v16, "maxThumbnailWidth":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v3, v14}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->weaklyAddListView(Lcom/google/android/ublib/widget/AbsWarpListView;)V

    .line 339
    new-instance v10, Lcom/google/android/apps/books/navigation/PageListAdapter;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCallbacks:Lcom/google/android/apps/books/widget/BookView$Callbacks;

    const/4 v15, 0x1

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->isRightToLeft()Z

    move-result v18

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mFullScreenSpreadSize:Landroid/graphics/Point;

    move-object/from16 v20, v0

    const v21, 0x7f02021e

    const v22, 0x7f02021f

    const/16 v23, 0x0

    move-object/from16 v11, p0

    move-object v13, v2

    invoke-direct/range {v10 .. v23}, Lcom/google/android/apps/books/navigation/PageListAdapter;-><init>(Lcom/google/android/apps/books/util/Destroyable;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/navigation/PageRangeLoader;Lcom/google/android/ublib/widget/AbsWarpListView;IIZZZLandroid/graphics/Point;IIZ)V

    .line 346
    .local v10, "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mCurrentRow:I

    move/from16 v0, p2

    if-ne v0, v3, :cond_7

    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v10, v3}, Lcom/google/android/apps/books/navigation/PageListAdapter;->setHighlightCurrentSpread(Z)V

    .line 348
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mHideCurrentSpread:Z

    invoke-virtual {v10, v3}, Lcom/google/android/apps/books/navigation/PageListAdapter;->setHideCurrentSpread(Z)V

    .line 349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->mAdapters:Ljava/util/Set;

    invoke-interface {v3, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 350
    invoke-virtual {v14, v10}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    goto/16 :goto_1

    .line 328
    .end local v2    # "pageRangeLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .end local v9    # "basePosition":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .end local v10    # "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    .end local v16    # "maxThumbnailWidth":I
    :cond_6
    const/4 v9, 0x0

    goto :goto_3

    .line 346
    .restart local v2    # "pageRangeLoader":Lcom/google/android/apps/books/navigation/PageRangeLoader;
    .restart local v9    # "basePosition":Lcom/google/android/apps/books/render/SpreadIdentifier;
    .restart local v10    # "adapter":Lcom/google/android/apps/books/navigation/PageListAdapter;
    .restart local v16    # "maxThumbnailWidth":I
    :cond_7
    const/4 v3, 0x0

    goto :goto_4
.end method

.method public transformView(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 361
    return-void
.end method
