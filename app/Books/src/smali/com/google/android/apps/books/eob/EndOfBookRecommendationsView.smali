.class public Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
.super Landroid/widget/LinearLayout;
.source "EndOfBookRecommendationsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;,
        Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;,
        Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    }
.end annotation


# instance fields
.field private mCampaignId:Ljava/lang/String;

.field private mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

.field private mRecommendationAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
    .param p1, "x1"    # Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mRecommendationAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    return-object v0
.end method

.method public static find(Landroid/view/View;)Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
    .locals 1
    .param p0, "parent"    # Landroid/view/View;

    .prologue
    .line 66
    const v0, 0x7f0e01b6

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;

    return-object v0
.end method

.method private getImageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/ImageManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    invoke-static {p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v0

    .line 172
    .local v0, "booksApplication":Lcom/google/android/apps/books/app/BooksApplication;
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/BooksApplication;->getImageManager()Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v1

    return-object v1
.end method

.method private recommendedSubdivision(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Ljava/util/List;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Z)Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "callbacks"    # Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    .param p4, "handlerFactory"    # Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;
    .param p5, "showReasons"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/books/app/BooksFragmentCallbacks;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;",
            "Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;",
            "Z)",
            "Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;"
        }
    .end annotation

    .prologue
    .line 136
    .local p3, "recommendations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->getImageManager(Landroid/content/Context;)Lcom/google/android/apps/books/common/ImageManager;

    move-result-object v2

    .line 138
    .local v2, "imageManager":Lcom/google/android/apps/books/common/ImageManager;
    invoke-virtual {p0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->maxRecommendationsToShow()I

    move-result v3

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 141
    .local v11, "recommendationsToShow":I
    iget-object v3, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    if-eqz v3, :cond_0

    new-instance v6, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;

    invoke-direct {v6, p0, v11}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;-><init>(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;I)V

    .line 144
    .local v6, "imageListener":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;
    :goto_0
    new-instance v1, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;

    iget-object v3, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mCampaignId:Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;

    move-object/from16 v0, p2

    invoke-direct {v4, p0, p1, v0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$1;-><init>(Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;)V

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/widget/RecommendationViewPopulator;-><init>(Lcom/google/android/apps/books/common/ImageManager;Ljava/lang/String;Lcom/google/android/apps/books/widget/RecommendationViewPopulator$OnStoreLinkClickListener;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Lcom/google/android/apps/books/widget/RemoteImageView$RemoteImageViewListener;)V

    .line 155
    .local v1, "populator":Lcom/google/android/apps/books/widget/RecommendationViewPopulator;
    new-instance v7, Lcom/google/android/apps/books/widget/RecommendedAdapter;

    invoke-static {p1}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v10

    move-object v8, p1

    move-object v9, v1

    move/from16 v12, p5

    move-object/from16 v13, p2

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/books/widget/RecommendedAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/widget/RecommendationViewPopulator;ZIZLcom/google/android/apps/books/app/BooksFragmentCallbacks;)V

    .line 159
    .local v7, "recommendedAdapter":Lcom/google/android/apps/books/widget/RecommendedAdapter;
    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/widget/RecommendedAdapter;->setRecommendations(Ljava/util/List;)V

    .line 161
    new-instance v3, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v7}, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;)V

    return-object v3

    .line 141
    .end local v1    # "populator":Lcom/google/android/apps/books/widget/RecommendationViewPopulator;
    .end local v6    # "imageListener":Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsImageListener;
    .end local v7    # "recommendedAdapter":Lcom/google/android/apps/books/widget/RecommendedAdapter;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public maxRecommendationsToShow()I
    .locals 1

    .prologue
    .line 165
    invoke-static {}, Lcom/google/android/apps/books/util/ReaderUtils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setup(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "bookClickCampaignId"    # Ljava/lang/String;
    .param p4, "headingText"    # Ljava/lang/String;
    .param p5, "handlerFactory"    # Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;
    .param p6, "shopHandler"    # Landroid/view/View$OnClickListener;
    .param p7, "showReasons"    # Z
    .param p8, "listener"    # Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;
    .param p9, "recommendationAction"    # Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;",
            "Landroid/view/View$OnClickListener;",
            "Z",
            "Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;",
            "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    .local p2, "recommendations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iput-object p3, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mCampaignId:Ljava/lang/String;

    .line 88
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mReadyListener:Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$RecommendationsReadyListener;

    .line 89
    invoke-static {p1}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getFragmentCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksFragmentCallbacks;

    move-result-object v3

    .line 91
    .local v3, "callbacks":Lcom/google/android/apps/books/app/BooksFragmentCallbacks;
    const-string v1, "RecommendationsView"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    const-string v1, "RecommendationsView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Setup received "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " recommendations"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .local v10, "subdivisions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;>;"
    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object/from16 v5, p5

    move/from16 v6, p7

    .line 96
    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->recommendedSubdivision(Landroid/content/Context;Lcom/google/android/apps/books/app/BooksFragmentCallbacks;Ljava/util/List;Lcom/google/android/apps/books/widget/PlayCardMenuHandlerFactory;Z)Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v1, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;

    const/4 v2, 0x0

    new-instance v4, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;

    move-object/from16 v0, p6

    invoke-direct {v4, p1, v0}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V

    invoke-direct {v1, v2, v4}, Lcom/google/android/apps/books/widget/SubdividedListAdapter$Subdivision;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/widget/DescribingListAdapter;)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    new-instance v7, Lcom/google/android/apps/books/widget/SubdividedListAdapter;

    new-instance v1, Lcom/google/android/apps/books/widget/BooksTitleRowCreator;

    invoke-direct {v1}, Lcom/google/android/apps/books/widget/BooksTitleRowCreator;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v7, v1, p1, v2, v10}, Lcom/google/android/apps/books/widget/SubdividedListAdapter;-><init>(Lcom/google/android/apps/books/widget/SubdividedListAdapter$TitleRowCreator;Landroid/content/Context;ZLjava/util/List;)V

    .line 104
    .local v7, "adapter":Lcom/google/android/apps/books/widget/SubdividedListAdapter;
    const v1, 0x7f0e01b7

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 105
    .local v8, "headingView":Landroid/widget/TextView;
    invoke-virtual {v8, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    const v1, 0x7f0e01b8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/app/ShortWideListView;

    .line 109
    .local v9, "listView":Lcom/google/android/apps/books/app/ShortWideListView;
    invoke-virtual {v9, v7}, Lcom/google/android/apps/books/app/ShortWideListView;->setAdapter(Landroid/widget/Adapter;)V

    .line 110
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;->mRecommendationAction:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$StoreAction;

    .line 111
    return-void
.end method
