.class Lcom/google/android/apps/books/eob/TextureEndOfBookView$3;
.super Ljava/lang/Object;
.source "TextureEndOfBookView.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/eob/TextureEndOfBookView;->setup(Lcom/google/android/apps/books/model/VolumeMetadata;Landroid/app/Activity;Lcom/google/android/apps/books/eob/TextureEndOfBookView$EndOfBookListener;Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;Landroid/accounts/Account;Lcom/google/android/apps/books/app/SystemBarManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$3;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 190
    if-eqz p1, :cond_1

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$3;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # getter for: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->mCoverView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$200(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/books/eob/TextureEndOfBookView$3;->this$0:Lcom/google/android/apps/books/eob/TextureEndOfBookView;

    # invokes: Lcom/google/android/apps/books/eob/TextureEndOfBookView;->notifyChange()V
    invoke-static {v0}, Lcom/google/android/apps/books/eob/TextureEndOfBookView;->access$300(Lcom/google/android/apps/books/eob/TextureEndOfBookView;)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    const-string v0, "TextureEndOfBookView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    const-string v0, "TextureEndOfBookView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not load EOB bitmap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
