.class public Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;
.super Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;
.source "DataControllerUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BlockingExceptionOrConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 423
    .local p0, "this":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<TT;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;-><init>()V

    .line 424
    return-void
.end method

.method public static createExceptionOrConsumer()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 429
    new-instance v0, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getOrThrow()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 433
    .local p0, "this":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingExceptionOrConsumer;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v0}, Lcom/google/android/apps/books/util/ExceptionOr;->getValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
