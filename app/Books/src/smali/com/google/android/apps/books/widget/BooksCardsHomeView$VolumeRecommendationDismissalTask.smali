.class public Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;
.super Landroid/os/AsyncTask;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VolumeRecommendationDismissalTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mActionType:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

.field private final mContext:Landroid/content/Context;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "volumeId"    # Ljava/lang/String;
    .param p4, "actionType"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .prologue
    .line 638
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 639
    const-string v0, "missing context"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mContext:Landroid/content/Context;

    .line 640
    const-string v0, "missing account"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mAccount:Landroid/accounts/Account;

    .line 641
    const-string v0, "missing volumeId"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mVolumeId:Ljava/lang/String;

    .line 642
    const-string v0, "missing actionType"

    invoke-static {p4, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    iput-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mActionType:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    .line 643
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 631
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 647
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksApplication;->getApiaryClient(Landroid/content/Context;)Lcom/google/android/apps/books/api/ApiaryClient;

    move-result-object v0

    .line 648
    .local v0, "client":Lcom/google/android/apps/books/api/ApiaryClient;
    iget-object v4, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksApplication;->getConfig(Landroid/content/Context;)Lcom/google/android/apps/books/util/Config;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mVolumeId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mActionType:Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/books/api/OceanApiaryUrls;->forDismissRecommendation(Lcom/google/android/apps/books/util/Config;Ljava/lang/String;Lcom/google/android/apps/books/widget/BooksCardsHomeView$DismissRecommendationAction;)Lcom/google/api/client/http/GenericUrl;

    move-result-object v3

    .line 650
    .local v3, "url":Lcom/google/api/client/http/GenericUrl;
    sget-object v4, Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;->DISMISS_RECOMMENDATION:Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;

    invoke-static {v4}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->setThreadFlag(Lcom/google/android/apps/books/net/TrafficStatsUtils$TrafficFlag;)V

    .line 652
    :try_start_0
    sget-object v4, Lcom/google/android/apps/books/api/ApiaryClient;->NO_POST_DATA:Ljava/lang/Object;

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/books/api/ApiaryClient;->makePostRequest(Lcom/google/api/client/http/GenericUrl;Ljava/lang/Object;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v2

    .line 653
    .local v2, "request":Lcom/google/api/client/http/HttpRequest;
    const-class v4, Lcom/google/android/apps/books/api/ApiaryClient$NoReturnValue;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeRecommendationDismissalTask;->mAccount:Landroid/accounts/Account;

    const/4 v6, 0x0

    new-array v6, v6, [I

    invoke-interface {v0, v2, v4, v5, v6}, Lcom/google/android/apps/books/api/ApiaryClient;->execute(Lcom/google/api/client/http/HttpRequest;Ljava/lang/Class;Landroid/accounts/Account;[I)Ljava/lang/Object;

    .line 654
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->incrementOperationCount()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 660
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    .line 662
    .end local v2    # "request":Lcom/google/api/client/http/HttpRequest;
    :goto_0
    const/4 v4, 0x0

    return-object v4

    .line 655
    :catch_0
    move-exception v1

    .line 656
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    const-string v4, "BooksCardsHomeView"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 657
    const-string v4, "BooksCardsHomeView"

    const-string v5, "/recommendationDismissal failed "

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    goto :goto_0

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    invoke-static {}, Lcom/google/android/apps/books/net/TrafficStatsUtils;->clearThreadFlags()V

    throw v4
.end method
