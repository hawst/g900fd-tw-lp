.class Lcom/google/android/apps/books/util/BookCoverImageProvider$1;
.super Ljava/lang/Object;
.source "BookCoverImageProvider.java"

# interfaces
.implements Lcom/google/android/apps/books/common/ImageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/BookCoverImageProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/BookCoverImageProvider;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;->this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onImage(Landroid/graphics/Bitmap;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    const/4 v1, 0x0

    .line 25
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;->this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;

    # getter for: Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    invoke-static {v0}, Lcom/google/android/apps/books/util/BookCoverImageProvider;->access$000(Lcom/google/android/apps/books/util/BookCoverImageProvider;)Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    if-eqz p1, :cond_1

    .line 27
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;->this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;

    # getter for: Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    invoke-static {v0}, Lcom/google/android/apps/books/util/BookCoverImageProvider;->access$000(Lcom/google/android/apps/books/util/BookCoverImageProvider;)Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;->onImage(Landroid/graphics/Bitmap;)V

    .line 32
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;->this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;

    # setter for: Lcom/google/android/apps/books/util/BookCoverImageProvider;->mFuture:Lcom/google/android/apps/books/common/ImageFuture;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/BookCoverImageProvider;->access$102(Lcom/google/android/apps/books/util/BookCoverImageProvider;Lcom/google/android/apps/books/common/ImageFuture;)Lcom/google/android/apps/books/common/ImageFuture;

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;->this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;

    # setter for: Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/BookCoverImageProvider;->access$002(Lcom/google/android/apps/books/util/BookCoverImageProvider;Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;)Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    .line 34
    return-void

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/util/BookCoverImageProvider$1;->this$0:Lcom/google/android/apps/books/util/BookCoverImageProvider;

    # getter for: Lcom/google/android/apps/books/util/BookCoverImageProvider;->mListener:Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;
    invoke-static {v0}, Lcom/google/android/apps/books/util/BookCoverImageProvider;->access$000(Lcom/google/android/apps/books/util/BookCoverImageProvider;)Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/ublib/cardlib/PlayCardImageProvider$OnImageFetchedListener;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
