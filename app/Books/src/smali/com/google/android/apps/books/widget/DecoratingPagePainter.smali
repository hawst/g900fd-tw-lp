.class public abstract Lcom/google/android/apps/books/widget/DecoratingPagePainter;
.super Ljava/lang/Object;
.source "DecoratingPagePainter.java"

# interfaces
.implements Lcom/google/android/apps/books/render/PagePainter;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDelegate:Lcom/google/android/apps/books/render/PagePainter;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mOkToMutate:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/PagePainter;ZLcom/google/android/apps/books/util/Logger;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p2, "okToMutate"    # Z
    .param p3, "logger"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    .line 37
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mOkToMutate:Z

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 39
    return-void
.end method

.method private createBitmap()V
    .locals 10

    .prologue
    .line 50
    iget-boolean v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mOkToMutate:Z

    if-nez v7, :cond_0

    .line 51
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Asked to create bitmap but not allowed to mutate it"

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 54
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v8, "decorating page"

    invoke-static {v7, v8}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v5

    .line 56
    .local v5, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :try_start_0
    const-string v1, "DecoratingPagePainter#createBitmap"

    .line 57
    .local v1, "debugString":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/render/PagePainter;->getOwnedBitmap(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 58
    .local v4, "source":Landroid/graphics/Bitmap;
    const/4 v0, 0x0

    .line 59
    .local v0, "canvas":Landroid/graphics/Canvas;
    if-nez v4, :cond_6

    .line 61
    const-string v7, "DecoratingPagePainter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 62
    const-string v7, "DecoratingPagePainter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " creating bitmap from delegate draw"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8}, Landroid/graphics/Point;-><init>()V

    invoke-interface {v7, v8}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v3

    .line 66
    .local v3, "size":Landroid/graphics/Point;
    iget v7, v3, Landroid/graphics/Point;->x:I

    if-eqz v7, :cond_2

    iget v7, v3, Landroid/graphics/Point;->y:I

    if-nez v7, :cond_3

    :cond_2
    const-string v7, "DecoratingPagePainter"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 71
    const-string v7, "DecoratingPagePainter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Invalid PagePainter size ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_3
    const/4 v7, 0x1

    iget v8, v3, Landroid/graphics/Point;->x:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 74
    .local v6, "width":I
    const/4 v7, 0x1

    iget v8, v3, Landroid/graphics/Point;->y:I

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 75
    .local v2, "height":I
    const-string v7, "DecoratingPagePainter#createBitmap"

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v6, v2, v8}, Lcom/google/android/apps/books/util/BitmapUtils;->createBitmapInReader(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    .line 77
    new-instance v0, Landroid/graphics/Canvas;

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 78
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v7, v0}, Lcom/google/android/apps/books/render/PagePainter;->draw(Landroid/graphics/Canvas;)V

    .line 79
    const-string v7, "creating bitmap"

    invoke-interface {v5, v7}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 105
    .end local v2    # "height":I
    .end local v3    # "size":Landroid/graphics/Point;
    .end local v6    # "width":I
    :goto_0
    if-nez v0, :cond_4

    .line 106
    new-instance v0, Landroid/graphics/Canvas;

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 108
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    :cond_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->decorate(Landroid/graphics/Canvas;)V

    .line 115
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v7}, Lcom/google/android/apps/books/render/PagePainter;->isUpdatable()Z

    move-result v7

    if-nez v7, :cond_5

    .line 116
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_5
    invoke-interface {v5}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 121
    return-void

    .line 80
    :cond_6
    :try_start_1
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v7

    if-nez v7, :cond_8

    .line 82
    const-string v7, "DecoratingPagePainter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 83
    const-string v7, "DecoratingPagePainter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " copying immutable bitmap"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_7
    const-string v7, "DecoratingPagePainter#createBitmap"

    invoke-static {v4, v7}, Lcom/google/android/apps/books/util/PagesViewUtils;->createMutableBitmapOfSameSize(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    .line 88
    new-instance v0, Landroid/graphics/Canvas;

    .end local v0    # "canvas":Landroid/graphics/Canvas;
    iget-object v7, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 89
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0, v4, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 94
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 96
    const-string v7, "creating mutable bitmap copy"

    invoke-interface {v5, v7}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 119
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "debugString":Ljava/lang/String;
    .end local v4    # "source":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v7

    invoke-interface {v5}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    throw v7

    .line 98
    .restart local v0    # "canvas":Landroid/graphics/Canvas;
    .restart local v1    # "debugString":Ljava/lang/String;
    .restart local v4    # "source":Landroid/graphics/Bitmap;
    :cond_8
    :try_start_2
    const-string v7, "DecoratingPagePainter"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 99
    const-string v7, "DecoratingPagePainter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " modifying delegate bitmap"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_9
    iput-object v4, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public canStillDraw()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 216
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v1}, Lcom/google/android/apps/books/render/PagePainter;->canStillDraw()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract decorate(Landroid/graphics/Canvas;)V
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mOkToMutate:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PagePainter;->isMutableBitmap()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    const-string v0, "DecoratingPagePainter"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const-string v0, "DecoratingPagePainter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " eagerly stealing delegate bitmap"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->createBitmap()V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v3, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 174
    :goto_0
    return-void

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_3

    .line 160
    const-string v0, "DecoratingPagePainter"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 161
    const-string v0, "DecoratingPagePainter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " drawing from delegate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/PagePainter;->draw(Landroid/graphics/Canvas;)V

    .line 164
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->decorate(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 169
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 170
    const-string v0, "DecoratingPagePainter"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 171
    const-string v0, "DecoratingPagePainter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " drawing from our bitmap"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v3, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 177
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No delegate or bitmap"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOwnedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public getSharedBitmap(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "create"    # Z

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->createBitmap()V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 130
    :goto_0
    return-object p1

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/render/PagePainter;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    goto :goto_0
.end method

.method public getViewLayerType()I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PagePainter;->getViewLayerType()I

    move-result v0

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMutableBitmap()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    .line 190
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PagePainter;->isMutableBitmap()Z

    move-result v0

    goto :goto_0
.end method

.method public isUpdatable()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PagePainter;->isUpdatable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public recycle()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    invoke-interface {v0}, Lcom/google/android/apps/books/render/PagePainter;->recycle()V

    .line 206
    iput-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mDelegate:Lcom/google/android/apps/books/render/PagePainter;

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 210
    iput-object v1, p0, Lcom/google/android/apps/books/widget/DecoratingPagePainter;->mBitmap:Landroid/graphics/Bitmap;

    .line 212
    :cond_1
    return-void
.end method
