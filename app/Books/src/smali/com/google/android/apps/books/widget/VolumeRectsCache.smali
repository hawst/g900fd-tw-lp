.class Lcom/google/android/apps/books/widget/VolumeRectsCache;
.super Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;
.source "VolumeRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/apps/books/render/LabeledRect;",
        ">;>;"
    }
.end annotation


# instance fields
.field protected final mLayerIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V
    .locals 0
    .param p2, "loader"    # Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;
    .param p3, "readerDelegate"    # Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;",
            "Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;",
            "Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/books/widget/PaintableRectsCacheImpl;-><init>(Lcom/google/android/apps/books/widget/PagesViewController$WebLoader;Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;)V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/widget/VolumeRectsCache;->mLayerIds:Ljava/util/Set;

    .line 19
    return-void
.end method


# virtual methods
.method protected finishProcessRequestResult()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/books/widget/VolumeRectsCache;->mCallbacks:Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/PaintableRectsCache$Callbacks;->onCacheUpdated()V

    .line 30
    return-void
.end method

.method protected getPaintables(I)Ljava/util/List;
    .locals 2
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/widget/VolumeRectsCache;->mReaderDelegate:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/VolumeRectsCache;->mLayerIds:Ljava/util/Set;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;->getPaintableAnnotations(ILjava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic makeCachedData(ILjava/util/List;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/widget/VolumeRectsCache;->makeCachedData(ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected makeCachedData(ILjava/util/List;)Ljava/util/List;
    .locals 0
    .param p1, "cachedItemIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    .local p2, "rects":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/LabeledRect;>;"
    return-object p2
.end method
