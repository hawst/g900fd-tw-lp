.class Lcom/google/android/apps/books/widget/MyLibraryHomeView$1;
.super Lcom/google/android/apps/books/model/StubBooksDataListener;
.source "MyLibraryHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/MyLibraryHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/MyLibraryHomeView;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$1;->this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/StubBooksDataListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onUploadProgressUpdate(Ljava/lang/String;I)V
    .locals 4
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "uploadPercentage"    # I

    .prologue
    const/4 v3, 0x0

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/books/widget/MyLibraryHomeView$1;->this$0:Lcom/google/android/apps/books/widget/MyLibraryHomeView;

    # invokes: Lcom/google/android/apps/books/widget/MyLibraryHomeView;->findUploadView(Ljava/lang/String;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    invoke-static {v2, p1}, Lcom/google/android/apps/books/widget/MyLibraryHomeView;->access$000(Lcom/google/android/apps/books/widget/MyLibraryHomeView;Ljava/lang/String;)Lcom/google/android/ublib/cardlib/layout/PlayCardView;

    move-result-object v0

    .line 145
    .local v0, "cardView":Lcom/google/android/ublib/cardlib/layout/PlayCardView;
    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 149
    :cond_0
    const v2, 0x7f0e00e3

    invoke-virtual {v0, v2}, Lcom/google/android/ublib/cardlib/layout/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 151
    .local v1, "progressBar":Landroid/widget/ProgressBar;
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 152
    invoke-virtual {v1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 153
    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
