.class public Lcom/google/android/apps/books/app/RecommendationsUtil;
.super Ljava/lang/Object;
.source "RecommendationsUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;
    }
.end annotation


# static fields
.field private static sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/util/Eventual",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic access$200()Lcom/google/android/apps/books/util/Eventual;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/apps/books/app/RecommendationsUtil;->sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;

    return-object v0
.end method

.method private static allowedByGservicesPolicy(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/books/util/ConfigValue;->getBoolean(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static hasDeviceOwnerJBMR2(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 157
    const-string v3, "device_policy"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 159
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    if-nez v0, :cond_0

    .line 166
    :goto_0
    return v2

    .line 163
    :cond_0
    :try_start_0
    const-string v3, "com.google.android.apps.enterprise.dmagent"

    invoke-virtual {v0, v3}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 164
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Ljava/lang/Throwable;
    const-string v3, "RecommendationsUtil"

    const-string v4, "error when checking device ownership"

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static loadIsSchoolOwned(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "c":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    const/4 v1, 0x0

    .line 112
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnOrAfter(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 133
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/app/RecommendationsUtil;->hasDeviceOwnerJBMR2(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 123
    :cond_1
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnOrAfter(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    invoke-static {p0, p1}, Lcom/google/android/apps/books/app/RecommendationsUtil;->loadSchoolOwnershipKitkat(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 132
    :cond_2
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static loadSchoolOwnershipKitkat(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "c":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    new-instance v0, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;

    const/4 v2, 0x0

    invoke-direct {v0, p1, p0, v2}, Lcom/google/android/apps/books/app/RecommendationsUtil$KitkatSchoolOwnershipServiceConnection;-><init>(Lcom/google/android/ublib/utils/Consumer;Landroid/content/Context;Lcom/google/android/apps/books/app/RecommendationsUtil$1;)V

    .line 141
    .local v0, "conn":Landroid/content/ServiceConnection;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.nfcprovision.IOwnedService.BIND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 142
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.google.android.nfcprovision"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 147
    :cond_0
    return-void
.end method

.method public static loadShouldShowRecommendations(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "c":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/lang/Boolean;>;"
    sget-object v0, Lcom/google/android/apps/books/app/RecommendationsUtil;->sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;

    if-nez v0, :cond_0

    .line 87
    invoke-static {}, Lcom/google/android/apps/books/util/Eventual;->create()Lcom/google/android/apps/books/util/Eventual;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/RecommendationsUtil;->sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;

    .line 88
    invoke-static {p0}, Lcom/google/android/apps/books/app/RecommendationsUtil;->allowedByGservicesPolicy(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    sget-object v0, Lcom/google/android/apps/books/app/RecommendationsUtil;->sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/Eventual;->onLoad(Ljava/lang/Object;)V

    .line 99
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/apps/books/app/RecommendationsUtil;->sEventualRecommendationsEnabled:Lcom/google/android/apps/books/util/Eventual;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/Eventual;->whenLoaded(Lcom/google/android/ublib/utils/Consumer;)V

    .line 100
    return-void

    .line 91
    :cond_1
    new-instance v0, Lcom/google/android/apps/books/app/RecommendationsUtil$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/RecommendationsUtil$1;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/books/app/BooksApplication;->withIsSchoolOwned(Landroid/content/Context;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method
