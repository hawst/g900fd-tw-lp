.class Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$1;
.super Ljava/lang/Object;
.source "FootnoteCardProvider.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$Callbacks;Lcom/google/android/apps/books/render/ResourceContentStore;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/render/ReaderDataSource;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Exception;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$1;->this$0:Lcom/google/android/apps/books/footnotes/FootnoteCardProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Exception;

    .prologue
    .line 87
    const-string v0, "FootnoteCardProvider"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "FootnoteCardProvider"

    const-string v1, "Failed to load resource."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 84
    check-cast p1, Ljava/lang/Exception;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/footnotes/FootnoteCardProvider$1;->take(Ljava/lang/Exception;)V

    return-void
.end method
