.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;
.super Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadMenuEntry"
.end annotation


# instance fields
.field private final mAction:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

.field private final mUploadId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;Ljava/lang/String;)V
    .locals 4
    .param p2, "actionType"    # Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;
    .param p3, "uploadId"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .line 184
    iget-object v0, p1, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, -0x1

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->mUiStringResourceId:I
    invoke-static {p2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->access$300(Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;)I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/ublib/cardlib/PlayCardMenuHandler$MenuEntry;-><init>(Landroid/content/res/Resources;IIZ)V

    .line 185
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->mAction:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    .line 186
    iput-object p3, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->mUploadId:Ljava/lang/String;

    .line 187
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->getUploadsController()Lcom/google/android/apps/books/data/UploadsController;

    move-result-object v0

    .line 192
    .local v0, "controller":Lcom/google/android/apps/books/data/UploadsController;
    sget-object v1, Lcom/google/android/apps/books/widget/BooksCardsHomeView$4;->$SwitchMap$com$google$android$apps$books$widget$BooksCardsHomeView$UploadAction:[I

    iget-object v2, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->mAction:Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 203
    :goto_0
    return-void

    .line 194
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->mUploadId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/UploadsController;->cancelUpload(Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->mUploadId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/UploadsController;->pauseUpload(Ljava/lang/String;)V

    goto :goto_0

    .line 200
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$UploadMenuEntry;->mUploadId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/UploadsController;->resumeUpload(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
