.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnnotationImageController"
.end annotation


# instance fields
.field private final mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mBlobStore:Lcom/google/android/apps/books/annotations/BlobStore;

.field private final mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

.field private final mUiThreadExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/annotations/BlobStore;Lcom/google/android/apps/books/net/ResponseGetter;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1, "blobStore"    # Lcom/google/android/apps/books/annotations/BlobStore;
    .param p2, "responseGetter"    # Lcom/google/android/apps/books/net/ResponseGetter;
    .param p3, "uiThreadExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 563
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBlobStore:Lcom/google/android/apps/books/annotations/BlobStore;

    .line 564
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    .line 565
    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    .line 566
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 567
    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Lcom/google/android/apps/books/annotations/BlobStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBlobStore:Lcom/google/android/apps/books/annotations/BlobStore;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Lcom/google/android/apps/books/net/ResponseGetter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mResponseGetter:Lcom/google/android/apps/books/net/ResponseGetter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mUiThreadExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public fetchImage(Ljava/lang/String;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "cachePolicy"    # Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 571
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;->mBackgroundExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController$1;-><init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$AnnotationImageController;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 634
    return-void
.end method
