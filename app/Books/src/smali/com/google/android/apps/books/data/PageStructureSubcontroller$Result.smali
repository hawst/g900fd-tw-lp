.class Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;
.super Ljava/lang/Object;
.source "PageStructureSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/PageStructureSubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Result"
.end annotation


# instance fields
.field final blob:Lcom/google/android/apps/books/data/DataControllerBlob;

.field final sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 0
    .param p1, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/DataControllerBlob;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    .line 48
    iput-object p2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 49
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/data/PageStructureSubcontroller$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x2"    # Lcom/google/android/apps/books/data/PageStructureSubcontroller$1;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/PageStructureSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    return-void
.end method
