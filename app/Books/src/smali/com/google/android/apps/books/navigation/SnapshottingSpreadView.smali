.class public Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
.super Landroid/widget/RelativeLayout;
.source "SnapshottingSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$2;
    }
.end annotation


# static fields
.field private static sPageNumberHeight:I


# instance fields
.field private mDistanceFromCenter:I

.field private mLeftNumber:Landroid/widget/TextView;

.field private mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

.field private mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

.field private mPageContents:Lcom/google/android/apps/books/render/SpreadItems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;"
        }
    .end annotation
.end field

.field private mPageViewCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

.field private final mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

.field private mRightNumber:Landroid/widget/TextView;

.field private mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

.field private mShadowId:I

.field private mTempPageSize:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 280
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->sPageNumberHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mShadowId:I

    .line 48
    new-instance v0, Lcom/google/android/apps/books/render/SpreadItems;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/render/SpreadItems;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageContents:Lcom/google/android/apps/books/render/SpreadItems;

    .line 135
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    .line 265
    new-instance v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$1;-><init>(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViewCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;)Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    return-object v0
.end method

.method private getCurrentPageDescription()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 328
    .local v3, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getPage()Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v0

    .line 329
    .local v0, "content":Lcom/google/android/apps/books/navigation/PageViewContent;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageHandle()Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v2

    .line 330
    .local v2, "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :goto_0
    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPage()Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    .line 331
    .local v1, "page":Lcom/google/android/apps/books/model/Page;
    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 333
    .local v4, "title":Ljava/lang/String;
    :cond_0
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 334
    const v5, 0x7f0f0121

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 336
    :goto_2
    return-object v5

    .end local v1    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v2    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    move-object v2, v4

    .line 329
    goto :goto_0

    .restart local v2    # "pageHandle":Lcom/google/android/apps/books/render/PageHandle;
    :cond_2
    move-object v1, v4

    .line 330
    goto :goto_1

    .line 336
    .restart local v1    # "page":Lcom/google/android/apps/books/model/Page;
    .restart local v4    # "title":Ljava/lang/String;
    :cond_3
    const v5, 0x7f0f0122

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method private static getPageName(Lcom/google/android/apps/books/navigation/PageViewContent;)Ljava/lang/String;
    .locals 2
    .param p0, "pageViewContent"    # Lcom/google/android/apps/books/navigation/PageViewContent;

    .prologue
    .line 138
    invoke-interface {p0}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPageHandle()Lcom/google/android/apps/books/render/PageHandle;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/render/PageHandle;->getFirstBookPage()Lcom/google/android/apps/books/model/Page;

    move-result-object v0

    .line 139
    .local v0, "page":Lcom/google/android/apps/books/model/Page;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getPageNumberHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 304
    sget v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->sPageNumberHeight:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 305
    invoke-static {p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->initPageNumberHeight(Landroid/content/Context;)V

    .line 307
    :cond_0
    sget v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->sPageNumberHeight:I

    return v0
.end method

.method private static initPageNumberHeight(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 283
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 284
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f0400c2

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    .line 289
    .local v3, "view":Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    iget-object v4, v3, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightNumber:Landroid/widget/TextView;

    const-string v5, "0123456789IVLMyi"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    const/16 v1, 0x400

    .line 293
    .local v1, "size":I
    const/16 v4, 0x400

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 294
    .local v2, "sizeSpec":I
    iget-object v4, v3, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightNumber:Landroid/widget/TextView;

    invoke-virtual {v4, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 296
    iget-object v4, v3, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightNumber:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    sput v4, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->sPageNumberHeight:I

    .line 297
    return-void
.end method

.method private setMargins(Lcom/google/android/apps/books/navigation/SnapshottingPageView;IIII)V
    .locals 1
    .param p1, "view"    # Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 235
    invoke-virtual {p1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 236
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 237
    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    return-void
.end method


# virtual methods
.method public getCenterAsFractionOfScreenWidth()F
    .locals 10

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 111
    const/4 v5, 0x0

    .line 112
    .local v5, "spreadPlacement":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .local v0, "arr$":[Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v6, v0, v2

    .line 113
    .local v6, "view":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    invoke-virtual {v6}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getPage()Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v1

    .line 114
    .local v1, "content":Lcom/google/android/apps/books/navigation/PageViewContent;
    if-eqz v1, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getVisibility()I

    move-result v8

    if-nez v8, :cond_0

    .line 116
    invoke-interface {v1}, Lcom/google/android/apps/books/navigation/PageViewContent;->getPagePositionOnScreen()Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    move-result-object v4

    .line 117
    .local v4, "pos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    if-nez v5, :cond_1

    .line 118
    move-object v5, v4

    .line 112
    .end local v4    # "pos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 120
    .restart local v4    # "pos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_1
    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_1

    .line 124
    .end local v1    # "content":Lcom/google/android/apps/books/navigation/PageViewContent;
    .end local v4    # "pos":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .end local v6    # "view":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    :cond_2
    if-nez v5, :cond_3

    .line 131
    :goto_2
    return v7

    .line 126
    :cond_3
    sget-object v8, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView$2;->$SwitchMap$com$google$android$apps$books$render$Renderer$PagePositionOnScreen:[I

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    goto :goto_2

    .line 127
    :pswitch_0
    const/high16 v7, 0x3e800000    # 0.25f

    goto :goto_2

    .line 128
    :pswitch_1
    const/high16 v7, 0x3f400000    # 0.75f

    goto :goto_2

    .line 126
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getNonEmptyPagesCount()I
    .locals 6

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 94
    .local v1, "count":I
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .local v0, "arr$":[Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 95
    .local v4, "view":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    invoke-virtual {v4}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 96
    add-int/lit8 v1, v1, 0x1

    .line 94
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    .end local v4    # "view":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    :cond_1
    return v1
.end method

.method public getPageBorder()Landroid/view/View;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    return-object v0
.end method

.method public hasDestroyedPage()Z
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->hasDestroyedPage()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->hasDestroyedPage()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public init(Lcom/google/android/apps/books/app/BookmarkController;FII)V
    .locals 9
    .param p1, "bookmarkController"    # Lcom/google/android/apps/books/app/BookmarkController;
    .param p2, "scale"    # F
    .param p3, "loadingColor"    # I
    .param p4, "letterboxColor"    # I

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->from(Landroid/content/res/Resources;F)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v2

    .line 80
    .local v2, "measurements":Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    iget-object v6, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .local v6, "arr$":[Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v0, v6, v7

    .local v0, "pageView":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 81
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->init(Lcom/google/android/apps/books/app/BookmarkController;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;FII)V

    .line 80
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 83
    .end local v0    # "pageView":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    :cond_0
    return-void
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 245
    invoke-super {p0}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->invalidate()V

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->invalidate()V

    .line 252
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 66
    const v0, 0x7f0e01d9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .line 67
    const v0, 0x7f0e01db

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    aput-object v2, v0, v1

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    aput-object v2, v0, v1

    .line 71
    const v0, 0x7f0e01da

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftNumber:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0e01d8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightNumber:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0e01d7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    .line 75
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 342
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 346
    iget v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mDistanceFromCenter:I

    packed-switch v1, :pswitch_data_0

    .line 357
    const-string v0, ""

    .line 361
    .local v0, "description":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 362
    return-void

    .line 348
    .end local v0    # "description":Ljava/lang/String;
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getCurrentPageDescription()Ljava/lang/String;

    move-result-object v0

    .line 349
    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0

    .line 351
    .end local v0    # "description":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f011f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 352
    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0

    .line 354
    .end local v0    # "description":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f011e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 355
    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0

    .line 346
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public prepare(Lcom/google/android/apps/books/widget/OnPageTouchListener;)V
    .locals 5
    .param p1, "onPageTouchListener"    # Lcom/google/android/apps/books/widget/OnPageTouchListener;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViews:[Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    .local v0, "arr$":[Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 87
    .local v3, "pageView":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    iget-object v4, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageViewCallbacks:Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;

    invoke-virtual {v3, p1, v4}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->prepare(Lcom/google/android/apps/books/widget/OnPageTouchListener;Lcom/google/android/apps/books/navigation/SnapshottingPageView$Callbacks;)V

    .line 86
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v3    # "pageView":Lcom/google/android/apps/books/navigation/SnapshottingPageView;
    :cond_0
    return-void
.end method

.method public setDistanceFromCenter(I)V
    .locals 2
    .param p1, "distance"    # I

    .prologue
    const/4 v0, 0x1

    .line 319
    iget v1, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mDistanceFromCenter:I

    if-eq v1, p1, :cond_0

    .line 320
    iput p1, p0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mDistanceFromCenter:I

    .line 321
    const/4 v1, -0x1

    if-gt v1, p1, :cond_1

    if-gt p1, v0, :cond_1

    :goto_0
    invoke-static {p0, v0}, Lcom/google/android/ublib/utils/AccessibilityUtils;->setImportantForAccessibility(Landroid/view/View;Z)V

    .line 323
    :cond_0
    return-void

    .line 321
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPages(Lcom/google/android/apps/books/render/SpreadItems;IZZ)V
    .locals 23
    .param p2, "shadowId"    # I
    .param p3, "showPageNumbers"    # Z
    .param p4, "isRTL"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/SpreadItems",
            "<",
            "Lcom/google/android/apps/books/navigation/PageViewContent;",
            ">;IZZ)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "spreadItems":Lcom/google/android/apps/books/render/SpreadItems;, "Lcom/google/android/apps/books/render/SpreadItems<Lcom/google/android/apps/books/navigation/PageViewContent;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageContents:Lcom/google/android/apps/books/render/SpreadItems;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/render/SpreadItems;->set(Lcom/google/android/apps/books/render/SpreadItems;)V

    .line 158
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/render/SpreadItems;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 159
    if-eqz p4, :cond_0

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 160
    .local v15, "leftPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    if-eqz p4, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 161
    .local v21, "rightPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    sget-object v14, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 162
    .local v14, "leftPPOS":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    sget-object v20, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 170
    .local v20, "rightPPOS":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getPage()Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    if-ne v15, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->getPage()Lcom/google/android/apps/books/navigation/PageViewContent;

    move-result-object v2

    move-object/from16 v0, v21

    if-ne v0, v2, :cond_3

    .line 232
    :goto_3
    return-void

    .line 159
    .end local v14    # "leftPPOS":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .end local v15    # "leftPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    .end local v20    # "rightPPOS":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .end local v21    # "rightPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 160
    .restart local v15    # "leftPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    .line 164
    .end local v15    # "leftPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    :cond_2
    const/4 v15, 0x0

    .line 165
    .restart local v15    # "leftPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/render/SpreadItems;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/books/navigation/PageViewContent;

    .line 166
    .restart local v21    # "rightPage":Lcom/google/android/apps/books/navigation/PageViewContent;
    const/4 v14, 0x0

    .line 167
    .restart local v14    # "leftPPOS":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    sget-object v20, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .restart local v20    # "rightPPOS":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    goto :goto_2

    .line 174
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    invoke-virtual {v2, v15, v14}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->setPage(Lcom/google/android/apps/books/navigation/PageViewContent;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->setPage(Lcom/google/android/apps/books/navigation/PageViewContent;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    const/4 v3, 0x1

    const/4 v8, 0x1

    invoke-virtual {v2, v3, v8}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->setLoadingSides(ZZ)V

    .line 179
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mShadowId:I

    move/from16 v0, p2

    if-eq v0, v2, :cond_4

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->setBackgroundResource(I)V

    .line 181
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mShadowId:I

    .line 184
    :cond_4
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageName(Lcom/google/android/apps/books/navigation/PageViewContent;)Ljava/lang/String;

    move-result-object v22

    .line 186
    .local v22, "rightPageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightNumber:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    move-object/from16 v0, v21

    invoke-interface {v0, v2}, Lcom/google/android/apps/books/navigation/PageViewContent;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    iget v0, v2, Landroid/graphics/Point;->x:I

    move/from16 v18, v0

    .line 189
    .local v18, "pagesWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    iget v0, v2, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    .line 193
    .local v17, "pagesHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getPaddingLeft()I

    move-result v4

    .line 194
    .local v4, "leftBackgroundInset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getPaddingTop()I

    move-result v5

    .line 195
    .local v5, "topBackgroundInset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getPaddingRight()I

    move-result v6

    .line 196
    .local v6, "rightBackgroundInset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mPageBorder:Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;

    invoke-virtual {v2}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getPaddingBottom()I

    move-result v7

    .line 198
    .local v7, "bottomBackgroundInset":I
    if-nez v15, :cond_6

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->setVisibility(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setMargins(Lcom/google/android/apps/books/navigation/SnapshottingPageView;IIII)V

    .line 218
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftNumber:Landroid/widget/TextView;

    if-eqz p3, :cond_7

    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightNumber:Landroid/widget/TextView;

    if-eqz p3, :cond_8

    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    .line 223
    .local v19, "params":Landroid/view/ViewGroup$LayoutParams;
    add-int v2, v18, v4

    add-int/2addr v2, v6

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 224
    add-int v2, v17, v5

    add-int/2addr v2, v7

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 226
    if-eqz p3, :cond_5

    .line 227
    move-object/from16 v0, v19

    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageNumberHeight(Landroid/content/Context;)I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 230
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 231
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->invalidate()V

    goto/16 :goto_3

    .line 203
    .end local v19    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/navigation/SnapshottingPageView;->setVisibility(I)V

    .line 205
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/4 v12, 0x0

    move-object/from16 v8, p0

    move v10, v4

    move v11, v5

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setMargins(Lcom/google/android/apps/books/navigation/SnapshottingPageView;IIII)V

    .line 207
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mRightPageView:Lcom/google/android/apps/books/navigation/SnapshottingPageView;

    const/4 v10, 0x0

    move-object/from16 v8, p0

    move v11, v5

    move v12, v6

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->setMargins(Lcom/google/android/apps/books/navigation/SnapshottingPageView;IIII)V

    .line 210
    invoke-static {v15}, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->getPageName(Lcom/google/android/apps/books/navigation/PageViewContent;)Ljava/lang/String;

    move-result-object v16

    .line 211
    .local v16, "leftPageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mLeftNumber:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    invoke-interface {v15, v2}, Lcom/google/android/apps/books/navigation/PageViewContent;->getSize(Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    add-int v18, v18, v2

    .line 215
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;->mTempPageSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    move/from16 v0, v17

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v17

    goto/16 :goto_4

    .line 218
    .end local v16    # "leftPageName":Ljava/lang/String;
    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_5

    .line 219
    :cond_8
    const/16 v2, 0x8

    goto/16 :goto_6
.end method
