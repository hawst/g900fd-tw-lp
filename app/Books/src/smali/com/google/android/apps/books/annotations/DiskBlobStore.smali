.class public Lcom/google/android/apps/books/annotations/DiskBlobStore;
.super Ljava/lang/Object;
.source "DiskBlobStore.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/BlobStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;
    }
.end annotation


# instance fields
.field private final mBlobsDir:Ljava/io/File;

.field private mCurrentSize:I

.field private final mEntryInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mIndexFile:Ljava/io/File;

.field private final mMaxSize:I


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;I)V
    .locals 4
    .param p1, "indexFile"    # Ljava/io/File;
    .param p2, "blobsDir"    # Ljava/io/File;
    .param p3, "maxSize"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mIndexFile:Ljava/io/File;

    .line 58
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mBlobsDir:Ljava/io/File;

    .line 59
    iput p3, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I

    .line 61
    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0xa

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mIndexFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mBlobsDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->loadInfos()V

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->deleteGarbage()V

    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->enforceMaxSize()V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/annotations/DiskBlobStore;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->digestForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/annotations/DiskBlobStore;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->pathForDigest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/annotations/DiskBlobStore;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/annotations/DiskBlobStore;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->removeEntry(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/annotations/DiskBlobStore;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    return v0
.end method

.method static synthetic access$412(Lcom/google/android/apps/books/annotations/DiskBlobStore;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;
    .param p1, "x1"    # I

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/annotations/DiskBlobStore;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;

    .prologue
    .line 37
    iget v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/annotations/DiskBlobStore;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/annotations/DiskBlobStore;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->enforceMaxSizeWithPendingSize(I)V

    return-void
.end method

.method private deleteGarbage()V
    .locals 9

    .prologue
    .line 108
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mBlobsDir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "filenames":[Ljava/lang/String;
    if-nez v3, :cond_1

    .line 122
    :cond_0
    return-void

    .line 113
    :cond_1
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 114
    .local v2, "filename":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 115
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mBlobsDir:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 117
    const-string v6, "BlobStore"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 118
    const-string v6, "BlobStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Deleting unknown blob "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    .end local v1    # "f":Ljava/io/File;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private digestForKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "digest":Ljava/security/MessageDigest;
    const-string v2, "SHA-1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 167
    const-string v2, "utf-8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 168
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    .line 169
    .local v1, "digestBytes":[B
    invoke-static {v1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->filenameForDigest([B)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private enforceMaxSize()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->enforceMaxSizeWithPendingSize(I)V

    .line 143
    return-void
.end method

.method private enforceMaxSizeWithPendingSize(I)V
    .locals 5
    .param p1, "pendingBytes"    # I

    .prologue
    .line 129
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 130
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I

    if-le v2, v3, :cond_1

    .line 131
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 132
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;"
    const-string v2, "BlobStore"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const-string v3, "BlobStore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expunging blob "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->pathForDigest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 136
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->removeEntry(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)V

    .line 137
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 139
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;"
    :cond_1
    return-void
.end method

.method private static filenameForDigest([B)Ljava/lang/String;
    .locals 2
    .param p0, "bytes"    # [B

    .prologue
    .line 159
    const/16 v0, 0xb

    .line 160
    .local v0, "flags":I
    const/16 v1, 0xb

    invoke-static {p0, v1}, Lcom/google/android/apps/books/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getProtoPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mIndexFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadInfos()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    .line 82
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->getProtoPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 83
    .local v4, "is":Ljava/io/InputStream;
    invoke-static {v4}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v3

    .line 84
    .local v3, "infos":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    invoke-virtual {v3}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->getInfoList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 85
    .local v2, "info":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSize()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 92
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    .end local v3    # "infos":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .end local v4    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v5, "BlobStore"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 95
    const-string v5, "BlobStore"

    const-string v6, "Index file not found"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_1
    return-void

    .line 88
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "infos":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .restart local v4    # "is":Ljava/io/InputStream;
    :cond_1
    :try_start_1
    const-string v5, "BlobStore"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    const-string v5, "BlobStore"

    const-string v6, "Loaded blob index with size %d/%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 97
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "infos":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;
    .end local v4    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "BlobStore"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 99
    const-string v5, "BlobStore"

    const-string v6, "IO error loading index"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private pathForDigest(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "digest"    # Ljava/lang/String;

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mBlobsDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private removeEntry(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .prologue
    .line 125
    iget v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSize()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    .line 126
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 280
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->digestForKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 282
    .local v0, "digest":Ljava/lang/String;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->pathForDigest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 286
    .local v2, "result":Ljava/io/InputStream;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    .line 295
    .end local v0    # "digest":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 288
    .end local v2    # "result":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/io/FileNotFoundException;
    const/4 v2, 0x0

    .line 294
    .restart local v2    # "result":Ljava/io/InputStream;
    goto :goto_0

    .line 290
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v2    # "result":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    .line 291
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const/4 v2, 0x0

    .line 294
    .restart local v2    # "result":Ljava/io/InputStream;
    goto :goto_0

    .line 292
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v2    # "result":Ljava/io/InputStream;
    :catch_2
    move-exception v1

    .line 293
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v2, 0x0

    .restart local v2    # "result":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public save()V
    .locals 11

    .prologue
    const/4 v10, 0x6

    .line 175
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->newBuilder()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;

    move-result-object v0

    .line 176
    .local v0, "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    iget-object v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 177
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 178
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 179
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->addInfo(Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;)Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 188
    .end local v0    # "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;"
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;>;"
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v5, "BlobStore"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 190
    const-string v5, "BlobStore"

    const-string v6, "File not found error saving index"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_1
    return-void

    .line 181
    .restart local v0    # "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;>;"
    :cond_1
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/DiskBlobStore;->getProtoPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 182
    .local v4, "os":Ljava/io/OutputStream;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;->build()Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList;->writeTo(Ljava/io/OutputStream;)V

    .line 183
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 184
    const-string v5, "BlobStore"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 185
    const-string v5, "BlobStore"

    const-string v6, "Saved blobstore index; space consumed %d/%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 192
    .end local v0    # "builder":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfoList$Builder;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;>;"
    .end local v4    # "os":Ljava/io/OutputStream;
    :catch_1
    move-exception v1

    .line 193
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "BlobStore"

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 194
    const-string v5, "BlobStore"

    const-string v6, "IO error saving index"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public set(Ljava/lang/String;)Ljava/io/OutputStream;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    const-string v1, "blob"

    const-string v2, "tmp"

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mBlobsDir:Ljava/io/File;

    invoke-static {v1, v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 273
    .local v0, "tempFile":Ljava/io/File;
    new-instance v1, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/apps/books/annotations/DiskBlobStore$BlobOutputStream;-><init>(Lcom/google/android/apps/books/annotations/DiskBlobStore;Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 300
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v3

    const-string v4, "currentSize"

    iget v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mCurrentSize:I

    invoke-virtual {v3, v4, v5}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v3

    const-string v4, "maxSize"

    iget v5, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mMaxSize:I

    invoke-virtual {v3, v4, v5}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v1

    .line 302
    .local v1, "helper":Lcom/google/common/base/Objects$ToStringHelper;
    iget-object v3, p0, Lcom/google/android/apps/books/annotations/DiskBlobStore;->mEntryInfos:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 303
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 304
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;

    .line 305
    .local v0, "blob":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;->getSize()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "bytes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    goto :goto_0

    .line 307
    .end local v0    # "blob":Lcom/google/android/apps/books/annotations/proto/Blobs$BlobInfo;
    :cond_0
    invoke-virtual {v1}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
