.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;
.super Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BuyBookMenuEntry"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;)V
    .locals 2
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 618
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .line 619
    const v0, 0x7f0f00c3

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZ)V

    .line 620
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 4

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$BuyBookMenuEntry;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "books_inapp_home_longpress_buy_from_sample"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/widget/BooksHomeController;->startBuyBook(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V

    .line 627
    return-void
.end method
