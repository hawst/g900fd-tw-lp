.class Lcom/google/android/apps/books/view/pages/PageMesh;
.super Ljava/lang/Object;
.source "PageMesh.java"


# instance fields
.field private final mMeshNormals:Ljava/nio/FloatBuffer;

.field private final mMeshTexCoord:Ljava/nio/FloatBuffer;

.field private final mMeshVertices:Ljava/nio/FloatBuffer;

.field private mPreviousFraction:F

.field private mPreviousLiftingPageFromLeft:Z

.field private mPreviousPortraitRtl:Z

.field private final mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

.field private nbX:I

.field private nbY:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnScene;)V
    .locals 6
    .param p1, "scene"    # Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .prologue
    const/16 v5, 0x978

    const/4 v3, 0x0

    const v4, 0x3c23d70a    # 0.01f

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousFraction:F

    .line 120
    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousPortraitRtl:Z

    .line 121
    iput-boolean v3, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousLiftingPageFromLeft:Z

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    .line 37
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    .line 39
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    .line 41
    const/16 v2, 0x650

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    .line 45
    const v1, 0x3c23d70a    # 0.01f

    .line 46
    .local v1, "stepX":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x64

    if-gt v0, v2, :cond_0

    .line 48
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    int-to-float v3, v0

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 49
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 51
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    int-to-float v3, v0

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 52
    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method


# virtual methods
.method protected draw(Z)V
    .locals 10
    .param p1, "spineIsToRightOfPage"    # Z

    .prologue
    const/16 v2, 0x1406

    const/4 v0, 0x0

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v9, v1, Lcom/google/android/apps/books/view/pages/PageTurnScene;->mRenderer:Lcom/google/android/apps/books/view/pages/PageTurnRenderer;

    .line 142
    .local v9, "renderer":Lcom/google/android/apps/books/view/pages/PageTurnRenderer;
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 143
    const/4 v1, 0x3

    iget-object v5, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    move v3, v0

    move v4, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    invoke-virtual {v9, v1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setNormalPointer(Ljava/nio/FloatBuffer;)V

    .line 148
    invoke-virtual {v9, p1}, Lcom/google/android/apps/books/view/pages/PageTurnRenderer;->setBackFacing(Z)V

    .line 150
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 151
    const/4 v3, 0x1

    const/4 v4, 0x2

    iget-object v8, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshTexCoord:Ljava/nio/FloatBuffer;

    move v5, v2

    move v6, v0

    move v7, v0

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 154
    if-eqz p1, :cond_0

    .line 155
    const/16 v1, 0x404

    invoke-static {v1}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 158
    :cond_0
    const/4 v1, 0x5

    const/16 v2, 0xca

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 160
    const/16 v0, 0x405

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    .line 161
    return-void
.end method

.method protected isSameAsLastUpdate(F)Z
    .locals 3
    .param p1, "fractionDraggedFromRight"    # F

    .prologue
    .line 123
    iget v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousFraction:F

    sub-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3a83126f    # 0.001f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousPortraitRtl:Z

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v2

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousLiftingPageFromLeft:Z

    iget-object v2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v2}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingPageFromLeft()Z

    move-result v2

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 126
    .local v0, "isSame":Z
    :goto_0
    if-nez v0, :cond_0

    .line 127
    iput p1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousFraction:F

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousPortraitRtl:Z

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingPageFromLeft()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mPreviousLiftingPageFromLeft:Z

    .line 131
    :cond_0
    return v0

    .line 123
    .end local v0    # "isSame":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setSampling(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->nbX:I

    .line 58
    iput p2, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->nbY:I

    .line 59
    return-void
.end method

.method updateMesh(F)V
    .locals 15
    .param p1, "fractionDraggedFromRight"    # F

    .prologue
    .line 72
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/books/view/pages/PageMesh;->isSameAsLastUpdate(F)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 116
    :cond_0
    return-void

    .line 76
    :cond_1
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    iget-object v4, v12, Lcom/google/android/apps/books/view/pages/PageTurnScene;->pageAnimator:Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;

    .line 80
    .local v4, "pageAnimator":Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->isRtlOneUp()Z

    move-result v12

    if-eqz v12, :cond_3

    const/high16 v8, 0x3f800000    # 1.0f

    .line 81
    .local v8, "spineX":F
    :goto_0
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingPageFromLeft()Z

    move-result v12

    if-eqz v12, :cond_4

    const/high16 v9, -0x40000000    # -2.0f

    .line 82
    .local v9, "xVectorFromSpineToLiftedEdge":F
    :goto_1
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;

    invoke-virtual {v12}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->liftingPageFromLeft()Z

    move-result v12

    if-eqz v12, :cond_5

    const/high16 v12, 0x3f800000    # 1.0f

    sub-float v0, v12, p1

    .line 85
    .local v0, "fractionLifted":F
    :goto_2
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v12}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 86
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    invoke-virtual {v12}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 88
    const/high16 v12, 0x3f800000    # 1.0f

    iget v13, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->nbX:I

    int-to-float v13, v13

    div-float v10, v12, v13

    .line 89
    .local v10, "xinterval":F
    const/high16 v12, 0x3f800000    # 1.0f

    iget v13, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->nbY:I

    int-to-float v13, v13

    div-float v11, v12, v13

    .line 90
    .local v11, "yinterval":F
    new-instance v5, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v5}, Lcom/google/android/opengl/common/Float3;-><init>()V

    .line 91
    .local v5, "pos":Lcom/google/android/opengl/common/Float3;
    new-instance v3, Lcom/google/android/opengl/common/Float3;

    invoke-direct {v3}, Lcom/google/android/opengl/common/Float3;-><init>()V

    .line 92
    .local v3, "normal":Lcom/google/android/opengl/common/Float3;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    const/16 v12, 0x64

    if-gt v1, v12, :cond_0

    .line 93
    int-to-float v12, v1

    mul-float v6, v12, v10

    .line 95
    .local v6, "rx":F
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_4
    iget v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->nbY:I

    if-gt v2, v12, :cond_6

    .line 96
    int-to-float v12, v2

    mul-float v7, v12, v11

    .line 98
    .local v7, "ry":F
    invoke-virtual {v4, v6, v7, v0, v3}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->normal(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 99
    invoke-virtual {v4, v6, v7, v0, v5}, Lcom/google/android/apps/books/view/pages/KeyFramePageAnimator;->pos(FFFLcom/google/android/opengl/common/Float3;)Lcom/google/android/opengl/common/Float3;

    .line 100
    iget v12, v5, Lcom/google/android/opengl/common/Float3;->x:F

    mul-float/2addr v12, v9

    add-float/2addr v12, v8

    iput v12, v5, Lcom/google/android/opengl/common/Float3;->x:F

    .line 101
    const/high16 v12, -0x40800000    # -1.0f

    const/high16 v13, 0x40000000    # 2.0f

    iget v14, v5, Lcom/google/android/opengl/common/Float3;->y:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    iput v12, v5, Lcom/google/android/opengl/common/Float3;->y:F

    .line 103
    const/4 v12, 0x0

    cmpg-float v12, v9, v12

    if-gez v12, :cond_2

    .line 104
    invoke-virtual {v3}, Lcom/google/android/opengl/common/Float3;->negate()V

    .line 107
    :cond_2
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    iget v13, v5, Lcom/google/android/opengl/common/Float3;->x:F

    invoke-virtual {v12, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 108
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    iget v13, v5, Lcom/google/android/opengl/common/Float3;->y:F

    invoke-virtual {v12, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 109
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshVertices:Ljava/nio/FloatBuffer;

    iget v13, v5, Lcom/google/android/opengl/common/Float3;->z:F

    invoke-virtual {v12, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 111
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    iget v13, v3, Lcom/google/android/opengl/common/Float3;->x:F

    invoke-virtual {v12, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 112
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    iget v13, v3, Lcom/google/android/opengl/common/Float3;->y:F

    invoke-virtual {v12, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 113
    iget-object v12, p0, Lcom/google/android/apps/books/view/pages/PageMesh;->mMeshNormals:Ljava/nio/FloatBuffer;

    iget v13, v3, Lcom/google/android/opengl/common/Float3;->z:F

    invoke-virtual {v12, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 80
    .end local v0    # "fractionLifted":F
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "normal":Lcom/google/android/opengl/common/Float3;
    .end local v5    # "pos":Lcom/google/android/opengl/common/Float3;
    .end local v6    # "rx":F
    .end local v7    # "ry":F
    .end local v8    # "spineX":F
    .end local v9    # "xVectorFromSpineToLiftedEdge":F
    .end local v10    # "xinterval":F
    .end local v11    # "yinterval":F
    :cond_3
    const/high16 v8, -0x40800000    # -1.0f

    goto/16 :goto_0

    .line 81
    .restart local v8    # "spineX":F
    :cond_4
    const/high16 v9, 0x40000000    # 2.0f

    goto/16 :goto_1

    .restart local v9    # "xVectorFromSpineToLiftedEdge":F
    :cond_5
    move/from16 v0, p1

    .line 82
    goto/16 :goto_2

    .line 92
    .restart local v0    # "fractionLifted":F
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "normal":Lcom/google/android/opengl/common/Float3;
    .restart local v5    # "pos":Lcom/google/android/opengl/common/Float3;
    .restart local v6    # "rx":F
    .restart local v10    # "xinterval":F
    .restart local v11    # "yinterval":F
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method
