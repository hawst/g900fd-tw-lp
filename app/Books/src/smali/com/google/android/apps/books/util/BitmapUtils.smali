.class public Lcom/google/android/apps/books/util/BitmapUtils;
.super Ljava/lang/Object;
.source "BitmapUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/BitmapUtils$1;
    }
.end annotation


# direct methods
.method public static createBitmapInReader(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "debugString"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 32
    :try_start_0
    invoke-static {p1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logVolumeOom(Ljava/lang/String;)V

    .line 35
    throw v0
.end method

.method public static decodeStreamInReader(Ljava/lang/String;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "debugString"    # Ljava/lang/String;
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "outPadding"    # Landroid/graphics/Rect;
    .param p3, "options"    # Landroid/graphics/BitmapFactory$Options;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/BitmapDecodeException;
        }
    .end annotation

    .prologue
    .line 47
    :try_start_0
    invoke-static {p1, p2, p3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 48
    .local v1, "result":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 49
    new-instance v2, Lcom/google/android/apps/books/util/BitmapDecodeException;

    invoke-direct {v2}, Lcom/google/android/apps/books/util/BitmapDecodeException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    .end local v1    # "result":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logVolumeOom(Ljava/lang/String;)V

    .line 54
    throw v0

    .line 51
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "result":Landroid/graphics/Bitmap;
    :cond_0
    return-object v1
.end method

.method public static getBitmapConfigSize(Landroid/graphics/Bitmap$Config;)I
    .locals 2
    .param p0, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/apps/books/util/BitmapUtils$1;->$SwitchMap$android$graphics$Bitmap$Config:[I

    invoke-virtual {p0}, Landroid/graphics/Bitmap$Config;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 68
    const/4 v0, 0x2

    :goto_0
    return v0

    .line 62
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 64
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static reconfigure(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 99
    :cond_1
    :goto_0
    return v0

    .line 94
    :cond_2
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, p1, :cond_3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v2, p2, :cond_3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/graphics/Bitmap$Config;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 99
    :cond_3
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnKitKatOrLater()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/books/util/BitmapUtils;->reconfigureKitKat(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private static reconfigureKitKat(Landroid/graphics/Bitmap;IILandroid/graphics/Bitmap$Config;)Z
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 75
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/graphics/Bitmap;->reconfigure(IILandroid/graphics/Bitmap$Config;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    const/4 v1, 0x1

    .line 80
    :goto_0
    return v1

    .line 77
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static shrinkToConstraints(Landroid/graphics/Bitmap;Lcom/google/android/ublib/util/ImageConstraints;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "constraints"    # Lcom/google/android/ublib/util/ImageConstraints;

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 110
    iget-object v6, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    if-eqz v6, :cond_3

    .line 111
    iget-object v6, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float v5, v6, v7

    .line 116
    .local v5, "widthRatio":F
    :goto_0
    iget-object v6, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    if-eqz v6, :cond_4

    .line 117
    iget-object v6, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float v2, v6, v7

    .line 121
    .local v2, "heightRatio":F
    :goto_1
    const-string v6, "BitmapUtils"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 122
    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 123
    .local v3, "ratio":F
    const-string v7, "BitmapUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    cmpg-float v6, v3, v9

    if-gez v6, :cond_5

    const-string v6, "Shrinking"

    :goto_2
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " image to "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "x original size"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v3    # "ratio":F
    :cond_0
    cmpg-float v6, v5, v9

    if-ltz v6, :cond_1

    cmpg-float v6, v2, v9

    if-gez v6, :cond_2

    .line 130
    :cond_1
    cmpg-float v6, v5, v2

    if-gez v6, :cond_6

    .line 131
    iget-object v6, p1, Lcom/google/android/ublib/util/ImageConstraints;->width:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 132
    .local v1, "destWidth":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 137
    .local v0, "destHeight":I
    :goto_3
    const/4 v6, 0x1

    invoke-static {p0, v1, v0, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 139
    .local v4, "smaller":Landroid/graphics/Bitmap;
    if-eq v4, p0, :cond_2

    .line 140
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    move-object p0, v4

    .line 145
    .end local v0    # "destHeight":I
    .end local v1    # "destWidth":I
    .end local v4    # "smaller":Landroid/graphics/Bitmap;
    :cond_2
    return-object p0

    .line 113
    .end local v2    # "heightRatio":F
    .end local v5    # "widthRatio":F
    :cond_3
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    .restart local v5    # "widthRatio":F
    goto :goto_0

    .line 119
    :cond_4
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .restart local v2    # "heightRatio":F
    goto :goto_1

    .line 123
    .restart local v3    # "ratio":F
    :cond_5
    const-string v6, "(Not) expanding"

    goto :goto_2

    .line 134
    .end local v3    # "ratio":F
    :cond_6
    iget-object v6, p1, Lcom/google/android/ublib/util/ImageConstraints;->height:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 135
    .restart local v0    # "destHeight":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v1

    .restart local v1    # "destWidth":I
    goto :goto_3
.end method
