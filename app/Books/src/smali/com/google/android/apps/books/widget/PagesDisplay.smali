.class public interface abstract Lcom/google/android/apps/books/widget/PagesDisplay;
.super Ljava/lang/Object;
.source "PagesDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/PagesDisplay$Callbacks;
    }
.end annotation


# virtual methods
.method public abstract getView()Landroid/view/View;
.end method

.method public abstract getWidth()I
.end method

.method public abstract onBasePositionChanged()V
.end method

.method public abstract onBookmarkChanged(IZ)V
.end method

.method public abstract onPageOffsetChanged(I)V
.end method

.method public abstract setPageLoading(ILandroid/graphics/Point;)V
.end method

.method public abstract setPageRendering(ILcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;J)V
.end method

.method public abstract setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;J)V
.end method
