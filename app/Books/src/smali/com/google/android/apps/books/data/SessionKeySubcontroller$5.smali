.class final Lcom/google/android/apps/books/data/SessionKeySubcontroller$5;
.super Ljava/lang/Object;
.source "SessionKeySubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/SessionKeySubcontroller$KeyProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public process(Lcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/apps/books/model/LocalSessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 203
    .local p2, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    const/4 v0, 0x0

    .line 205
    .local v0, "accountKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->saveAccountSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    :goto_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataControllerStore()Lcom/google/android/apps/books/model/DataControllerStore;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->maybeInitializeUserKey(Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/model/LocalSessionKey;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$600(Lcom/google/android/apps/books/model/DataControllerStore;Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 216
    return-object v0

    .line 207
    :catch_0
    move-exception v1

    .line 213
    .local v1, "e":Landroid/database/sqlite/SQLiteConstraintException;
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getAccountSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    goto :goto_0
.end method
