.class public interface abstract Lcom/google/android/apps/books/app/mo/MediaOverlaysController$AbstractAudioClipPlayer;
.super Ljava/lang/Object;
.source "MediaOverlaysController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AbstractAudioClipPlayer"
.end annotation


# virtual methods
.method public abstract addClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
.end method

.method public abstract close()V
.end method

.method public abstract reset()V
.end method

.method public abstract setListener(Lcom/google/android/apps/books/app/AudioClipPlayer$AudioClipPlayerListener;)V
.end method
