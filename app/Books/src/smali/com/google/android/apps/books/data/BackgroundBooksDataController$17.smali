.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$localOnly:Z

.field final synthetic val$reloadLibrary:Z

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-boolean p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$localOnly:Z

    iput-boolean p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$reloadLibrary:Z

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mOffersSubcontroller:Lcom/google/android/apps/books/data/OffersSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2800(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/OffersSubcontroller;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$localOnly:Z

    iget-boolean v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$reloadLibrary:Z

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v3}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$17;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getOffers(ZZLcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 791
    return-void
.end method
