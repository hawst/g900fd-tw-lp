.class Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)V
    .locals 0

    .prologue
    .line 1259
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(II)V
    .locals 6
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I

    .prologue
    .line 1262
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    iget-object v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPassageIndex:I
    invoke-static {v2}, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->access$1900(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPageOffset:I
    invoke-static {v3}, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->access$2000(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mElementId:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->access$2100(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    iget v5, v5, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mTaskRequestId:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IILjava/lang/String;I)V

    .line 1265
    .local v0, "newRequest":Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    iget-object v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$500(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/PriorityQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 1266
    return-void
.end method
