.class Lcom/google/android/apps/books/widget/ReadNowHomeView$6;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$6;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$6;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    # getter for: Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCurrentlyShowingOfferId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->access$100(Lcom/google/android/apps/books/widget/ReadNowHomeView;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$6;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->getHomeController()Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$6;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    # getter for: Lcom/google/android/apps/books/widget/ReadNowHomeView;->mCurrentlyShowingOfferId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/widget/ReadNowHomeView;->access$100(Lcom/google/android/apps/books/widget/ReadNowHomeView;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->dismissOffer(Ljava/lang/String;)V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    const-string v0, "ReadNowHomeView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "ReadNowHomeView"

    const-string v1, "Trying to dismiss an empty offer id"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
