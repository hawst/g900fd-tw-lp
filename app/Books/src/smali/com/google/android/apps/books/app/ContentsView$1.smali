.class Lcom/google/android/apps/books/app/ContentsView$1;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "ContentsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ContentsView;->setup(Lcom/google/android/apps/books/app/ContentsView$Arguments;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ContentsView;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ContentsView;Landroid/content/Context;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/books/app/ContentsView$1;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    iput-object p3, p0, Lcom/google/android/apps/books/app/ContentsView$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 127
    const v3, 0x7f0400d1

    invoke-virtual {p1, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    .line 129
    .local v1, "pager":Landroid/support/v4/view/ViewPager;
    iget-object v3, p0, Lcom/google/android/apps/books/app/ContentsView$1;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # invokes: Lcom/google/android/apps/books/app/ContentsView;->createTabFactories()Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ContentsView;->access$000(Lcom/google/android/apps/books/app/ContentsView;)Ljava/util/List;

    move-result-object v2

    .line 131
    .local v2, "tabs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/ublib/widget/TabFactory;>;"
    new-instance v3, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ContentsView$1;->val$context:Landroid/content/Context;

    invoke-direct {v3, v2, v4}, Lcom/google/android/ublib/widget/TabFactoryPagerAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 134
    new-instance v3, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ContentsView$1;->val$context:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/google/android/apps/books/preference/LocalPreferences;->getLastOpenedTocTab()I

    move-result v0

    .line 135
    .local v0, "lastOpenedTocTab":I
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v5, v3}, Lcom/google/android/apps/books/util/MathUtils;->constrain(III)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 139
    iget-object v3, p0, Lcom/google/android/apps/books/app/ContentsView$1;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    new-instance v4, Lcom/google/android/apps/books/app/ContentsView$1$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/books/app/ContentsView$1$1;-><init>(Lcom/google/android/apps/books/app/ContentsView$1;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/app/ContentsView;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 153
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 155
    sget-object v3, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->SHOW_TOC_VIEW:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logTocAction(Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;Ljava/lang/Long;)V

    .line 156
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method protected getCustomTabStrip(Landroid/content/Context;Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 194
    invoke-super {p0, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getCustomTabStrip(Landroid/content/Context;Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    move-result-object v0

    return-object v0
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$1;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ContentsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ContentsView$1;->getHeaderBottomMargin()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getHeaderMode()I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x3

    return v0
.end method

.method protected getTabAccessibilityMode()I
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x3

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method protected getTabPaddingMode()I
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x1

    return v0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 213
    const v0, 0x7f0e01f9

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x1

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x1

    return v0
.end method
