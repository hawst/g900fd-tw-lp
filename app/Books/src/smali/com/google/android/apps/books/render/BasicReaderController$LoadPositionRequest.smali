.class Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadPositionRequest"
.end annotation


# instance fields
.field final fallbackChapterIndex:Ljava/lang/Integer;

.field final offset:Ljava/lang/Integer;

.field final passage:I

.field final position:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;IILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 6
    .param p2, "requestId"    # I
    .param p3, "passage"    # I
    .param p4, "position"    # Ljava/lang/String;
    .param p5, "offset"    # Ljava/lang/Integer;
    .param p6, "fallbackChapterIndex"    # Ljava/lang/Integer;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 435
    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 436
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->passage:I

    .line 437
    iput-object p5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->offset:Ljava/lang/Integer;

    .line 438
    iput-object p4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->position:Ljava/lang/String;

    .line 439
    iput-object p6, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->fallbackChapterIndex:Ljava/lang/Integer;

    .line 440
    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x3

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->position:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->createPositionOrNull(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->offset:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/PageIdentifier;->withPosition(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v6

    .line 446
    .local v6, "identifier":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/apps/books/render/ReaderDataModelUtils;->getBestEffortPageIndices(Lcom/google/android/apps/books/render/ReaderDataModel;Lcom/google/android/apps/books/render/PageIdentifier;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v7

    .line 449
    .local v7, "indices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v7, :cond_4

    .line 450
    const-string v0, "ReaderController"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    const-string v0, "ReaderController"

    const-string v1, "Converting loadPosition(%d, %s, %d, %d, %d) ..."

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->passage:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->position:Ljava/lang/String;

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->offset:Ljava/lang/Integer;

    aput-object v3, v2, v9

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->fallbackChapterIndex:Ljava/lang/Integer;

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->mTaskRequestId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/google/android/apps/books/render/ReaderDataModel;->isLoaded(Lcom/google/android/apps/books/render/PageIndices;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 461
    const-string v0, "ReaderController"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 462
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "... to bypass JS for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v7, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v7, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget v1, v7, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v2, v7, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->mTaskRequestId:I

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/books/render/ReaderListener;->onPageLoaded(III)V

    .line 494
    :goto_0
    return-void

    .line 468
    :cond_2
    const-string v0, "ReaderController"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 469
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "... to a LoadPageRequest "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v7, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v7, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v5}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, v7, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v2, v7, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->mTaskRequestId:I

    invoke-interface {v0, v1, v8, v2, v3}, Lcom/google/android/apps/books/render/TextReader;->loadPage(IIII)V

    goto :goto_0

    .line 484
    :cond_4
    const-string v0, "ReaderController"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 485
    const-string v0, "ReaderController"

    const-string v1, "Issuing loadPosition(%d, %s, %d, %d, %d)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->passage:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->position:Ljava/lang/String;

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->offset:Ljava/lang/Integer;

    aput-object v3, v2, v9

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->fallbackChapterIndex:Ljava/lang/Integer;

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->mTaskRequestId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/StringUtils;->machineFormat(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v5}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->passage:I

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->position:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->offset:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->fallbackChapterIndex:Ljava/lang/Integer;

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$LoadPositionRequest;->mTaskRequestId:I

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/TextReader;->loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;I)V

    goto/16 :goto_0
.end method
