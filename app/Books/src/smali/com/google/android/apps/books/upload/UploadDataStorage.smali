.class public interface abstract Lcom/google/android/apps/books/upload/UploadDataStorage;
.super Ljava/lang/Object;
.source "UploadDataStorage.java"


# virtual methods
.method public abstract copyToTempDir(Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
        }
    .end annotation
.end method

.method public abstract deleteFile(Ljava/lang/String;)V
.end method

.method public abstract fileExists(Ljava/lang/String;)Z
.end method

.method public abstract get()Lcom/google/android/apps/books/upload/Upload$Uploads;
.end method

.method public abstract getFileInputStream(Ljava/lang/String;)Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation
.end method

.method public abstract set(Lcom/google/android/apps/books/upload/Upload$Uploads;)V
.end method
