.class public interface abstract Lcom/google/android/apps/books/app/InfoCardsHelper$Callbacks;
.super Ljava/lang/Object;
.source "InfoCardsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/InfoCardsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
.end method

.method public abstract hideCardsLayout()Z
.end method

.method public abstract onAllCardsDismissed()V
.end method

.method public abstract onCardsAdded(Lcom/google/android/apps/books/widget/TransientInfoCardsLayout$CardsState;)V
.end method
