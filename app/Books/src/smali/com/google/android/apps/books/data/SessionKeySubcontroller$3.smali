.class Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;
.super Lcom/google/android/apps/books/data/NetworkTask;
.source "SessionKeySubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/SessionKeySubcontroller;->fetchNewAccountKeyOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/NetworkTask;-><init>(Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    .locals 4
    .param p1, "callbacks"    # Lcom/google/android/apps/books/data/NetworkTaskServices;

    .prologue
    .line 161
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/NetworkTaskServices;->getServer()Lcom/google/android/apps/books/net/BooksServer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getNewSessionKey(Ljava/util/List;)Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;

    move-result-object v2

    iget-object v1, v2, Lcom/google/android/apps/books/net/BooksServer$SessionKeyResponse;->newKey:Lcom/google/android/apps/books/model/SessionKey;

    .line 162
    .local v1, "newKey":Lcom/google/android/apps/books/model/SessionKey;
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->handleNewAccountKeyOnControlThread(Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v2, v1, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$300(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKey;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 168
    .end local v1    # "newKey":Lcom/google/android/apps/books/model/SessionKey;
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;
    invoke-static {v3}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$000(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v2, v3, v0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$400(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    goto :goto_0

    .line 165
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 166
    .local v0, "e":Ljava/security/GeneralSecurityException;
    iget-object v2, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SessionKeySubcontroller$3;->this$0:Lcom/google/android/apps/books/data/SessionKeySubcontroller;

    # getter for: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->mAccountSessionKeyId:Lcom/google/android/apps/books/model/AccountSessionKeyId;
    invoke-static {v3}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$000(Lcom/google/android/apps/books/data/SessionKeySubcontroller;)Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/data/SessionKeySubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V
    invoke-static {v2, v3, v0, p1}, Lcom/google/android/apps/books/data/SessionKeySubcontroller;->access$400(Lcom/google/android/apps/books/data/SessionKeySubcontroller;Lcom/google/android/apps/books/model/SessionKeyId;Ljava/lang/Exception;Lcom/google/android/apps/books/data/NetworkTaskServices;)V

    goto :goto_0
.end method
