.class public interface abstract Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
.super Ljava/lang/Object;
.source "ReaderSettingsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderSettingsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReaderSettingsListener"
.end annotation


# virtual methods
.method public abstract done()V
.end method

.method public abstract onBrightnessChanged(IZ)V
.end method

.method public abstract onJustificationChanged(Z)V
.end method

.method public abstract onLineHeightSettingChanged(FZ)V
.end method

.method public abstract onPageLayoutChanged(ZZ)V
.end method

.method public abstract onReadingModeChanged(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Z)V
.end method

.method public abstract onShowingSettings()V
.end method

.method public abstract onTextZoomSettingChanged(FZ)V
.end method

.method public abstract onThemeChanged(Z)V
.end method

.method public abstract onTypefaceChanged(Z)V
.end method
