.class Lcom/google/android/apps/books/app/FreeTextSetting$1;
.super Ljava/lang/Object;
.source "FreeTextSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/FreeTextSetting;->createValidView(Landroid/app/Activity;Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/FreeTextSetting;

.field final synthetic val$edit:Landroid/widget/EditText;

.field final synthetic val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/FreeTextSetting;Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/apps/books/app/FreeTextSetting$1;->this$0:Lcom/google/android/apps/books/app/FreeTextSetting;

    iput-object p2, p0, Lcom/google/android/apps/books/app/FreeTextSetting$1;->val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;

    iput-object p3, p0, Lcom/google/android/apps/books/app/FreeTextSetting$1;->val$edit:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/books/app/FreeTextSetting$1;->val$listener:Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/app/FreeTextSetting$1;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/AbstractSetting$OptionValueListener;->onOptionValuesChanged([Ljava/lang/String;)V

    .line 35
    return-void
.end method
