.class public Lcom/google/android/apps/books/widget/SinglePageView;
.super Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;
.source "SinglePageView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final mPageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/widget/BookmarkedFrameLayout;-><init>(Landroid/content/Context;ZLcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    .line 30
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SinglePageView;->mPageView:Landroid/widget/ImageView;

    .line 31
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SinglePageView;->mPageView:Landroid/widget/ImageView;

    invoke-static {}, Lcom/google/android/apps/books/util/ViewUtils;->newFillParentLayout()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/widget/SinglePageView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    return-void
.end method


# virtual methods
.method public releaseBitmap()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SinglePageView;->mPageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 41
    return-void
.end method

.method public setPageBitmap(Landroid/graphics/Bitmap;Landroid/widget/ImageView$ScaleType;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SinglePageView;->mPageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SinglePageView;->mPageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 37
    return-void
.end method
