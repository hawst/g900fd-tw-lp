.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->ensureSharedResourcesOnControlThread(Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/model/VolumeManifest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

.field final synthetic val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->this$1:Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;

    iget-object v0, v0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mSharedResourceSubcontroller:Lcom/google/android/apps/books/data/SharedResourceSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$2400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/SharedResourceSubcontroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->val$manifest:Lcom/google/android/apps/books/model/VolumeManifest;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/util/IdentifiableCollection;->getValues()Ljava/util/Collection;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$14$2;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/apps/books/data/SharedResourceSubcontroller;->ensureSharedResources(Lcom/google/android/apps/books/data/BooksDataController$Priority;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/Collection;Lcom/google/android/ublib/utils/Consumer;)V

    .line 619
    return-void
.end method
