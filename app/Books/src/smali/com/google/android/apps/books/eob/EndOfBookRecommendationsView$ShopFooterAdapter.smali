.class Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;
.super Landroid/widget/BaseAdapter;
.source "EndOfBookRecommendationsView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/DescribingListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShopFooterAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mMoreHandler:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "moreHandler"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 180
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 181
    iput-object p1, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;->mContext:Landroid/content/Context;

    .line 182
    iput-object p2, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;->mMoreHandler:Landroid/view/View$OnClickListener;

    .line 183
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 197
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 202
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400b7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 204
    .local v0, "inflated":Landroid/view/View;
    const v1, 0x7f0e01b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/eob/EndOfBookRecommendationsView$ShopFooterAdapter;->mMoreHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    return-object v0
.end method
