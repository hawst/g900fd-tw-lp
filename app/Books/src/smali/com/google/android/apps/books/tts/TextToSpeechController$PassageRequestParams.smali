.class Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
.super Ljava/lang/Object;
.source "TextToSpeechController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/tts/TextToSpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PassageRequestParams"
.end annotation


# instance fields
.field final passage:I

.field final position:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "passage"    # I
    .param p2, "position"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->passage:I

    .line 140
    iput-object p2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->position:Ljava/lang/String;

    .line 141
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 144
    instance-of v1, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 145
    check-cast v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;

    .line 146
    .local v0, "o":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    iget v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->passage:I

    iget v2, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->passage:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->position:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->position:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 148
    .end local v0    # "o":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    :goto_0
    return v1

    .line 146
    .restart local v0    # "o":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 148
    .end local v0    # "o":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->passage:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->position:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
