.class Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;
.super Landroid/view/ViewGroup;
.source "ImageSpreadView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/ImageSpreadView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyViewGroup"
.end annotation


# instance fields
.field private final mTempPoint:Landroid/graphics/Point;

.field private final mTempRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ImageSpreadView;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    .line 110
    invoke-direct {p0, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 155
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempPoint:Landroid/graphics/Point;

    .line 156
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    .line 111
    return-void
.end method

.method private layoutPage(Landroid/view/View;ILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "pagePosition"    # Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempPoint:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/ImageSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/ImageSpreadView;->access$000(Lcom/google/android/apps/books/widget/ImageSpreadView;)Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempPoint:Landroid/graphics/Point;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    const/4 v3, 0x0

    invoke-static {p3, v0, v1, v2, v3}, Lcom/google/android/apps/books/util/PagesViewUtils;->getPageBounds(Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Landroid/graphics/Point;Landroid/graphics/Point;Landroid/graphics/Rect;Lcom/google/android/apps/books/util/Holder;)Landroid/graphics/Rect;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v0, v0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 172
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v6, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getMeasuredWidth()I

    move-result v3

    .line 135
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getMeasuredHeight()I

    move-result v1

    .line 136
    .local v1, "height":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ImageSpreadView;->mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    invoke-virtual {v4, v3, v1}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setContainerSize(II)V

    .line 138
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/ImageSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/ImageSpreadView;->access$000(Lcom/google/android/apps/books/widget/ImageSpreadView;)Landroid/graphics/Point;

    move-result-object v4

    iput v1, v4, Landroid/graphics/Point;->y:I

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getChildCount()I

    move-result v0

    .line 141
    .local v0, "childCount":I
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/ImageSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/ImageSpreadView;->access$000(Lcom/google/android/apps/books/widget/ImageSpreadView;)Landroid/graphics/Point;

    move-result-object v4

    div-int v5, v3, v0

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 143
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    .line 144
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/ImageSpreadView;->mDisplayTwoPages:Z
    invoke-static {v4}, Lcom/google/android/apps/books/widget/ImageSpreadView;->access$100(Lcom/google/android/apps/books/widget/ImageSpreadView;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v2, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 146
    .local v2, "pagePosition":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v4, v6, v2}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->layoutPage(Landroid/view/View;ILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 147
    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    .line 148
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    # getter for: Lcom/google/android/apps/books/widget/ImageSpreadView;->mPageCellSize:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/google/android/apps/books/widget/ImageSpreadView;->access$000(Lcom/google/android/apps/books/widget/ImageSpreadView;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->x:I

    sget-object v6, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->layoutPage(Landroid/view/View;ILcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;)V

    .line 151
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v4, v4, Lcom/google/android/apps/books/widget/ImageSpreadView;->mZoomHelper:Lcom/google/android/apps/books/util/ConstrainedScaleScroll;

    iget-object v5, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v5, v5, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v6, v6, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v7, v7, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->this$0:Lcom/google/android/apps/books/widget/ImageSpreadView;

    iget-object v8, v8, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/google/android/apps/books/util/ConstrainedScaleScroll;->setContentRect(IIII)V

    .line 153
    return-void

    .line 144
    .end local v2    # "pagePosition":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_1
    sget-object v2, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthSpec"    # I
    .param p2, "heightSpec"    # I

    .prologue
    const/high16 v7, -0x80000000

    .line 115
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 116
    .local v6, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 118
    .local v3, "height":I
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getChildCount()I

    move-result v0

    .line 119
    .local v0, "childCount":I
    div-int v5, v6, v0

    .line 120
    .local v5, "pageCellWidth":I
    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 122
    .local v2, "childWidthSpec":I
    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 125
    .local v1, "childHeightSpec":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_0

    .line 126
    invoke-virtual {p0, v4}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2, v1}, Landroid/view/View;->measure(II)V

    .line 125
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 129
    :cond_0
    invoke-virtual {p0, v6, v3}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setMeasuredDimension(II)V

    .line 130
    return-void
.end method
