.class Lcom/google/android/apps/books/service/SyncAdapter$2;
.super Ljava/lang/Object;
.source "SyncAdapter.java"

# interfaces
.implements Lcom/google/android/apps/books/service/BooksSafeFetchController$EnsureOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/service/SyncAdapter;->enqueueVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/service/BooksSafeFetchController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/service/SyncAdapter;

.field final synthetic val$dataController:Lcom/google/android/apps/books/data/BooksDataController;

.field final synthetic val$volume:Lcom/google/android/apps/books/model/VolumeData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/service/SyncAdapter;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->this$0:Lcom/google/android/apps/books/service/SyncAdapter;

    iput-object p2, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->val$dataController:Lcom/google/android/apps/books/data/BooksDataController;

    iput-object p3, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ensure()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 662
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->val$dataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 664
    iget-object v1, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->val$dataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v2, p0, Lcom/google/android/apps/books/service/SyncAdapter$2;->val$volume:Lcom/google/android/apps/books/model/VolumeData;

    sget-object v3, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeThumbnail(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    return-void

    .line 666
    :catch_0
    move-exception v0

    .line 667
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/android/ublib/utils/WrappedIoException;->maybeWrap(Ljava/lang/Exception;)Ljava/io/IOException;

    move-result-object v1

    throw v1
.end method
