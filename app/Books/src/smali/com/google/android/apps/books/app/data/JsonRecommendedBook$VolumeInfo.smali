.class public Lcom/google/android/apps/books/app/data/JsonRecommendedBook$VolumeInfo;
.super Ljava/lang/Object;
.source "JsonRecommendedBook.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/data/JsonRecommendedBook;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VolumeInfo"
.end annotation


# instance fields
.field public authors:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "authors"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public averageRating:F
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "averageRating"
    .end annotation
.end field

.field public canonicalVolumeLink:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "canonicalVolumeLink"
    .end annotation
.end field

.field public imageLinks:Lcom/google/android/apps/books/app/data/JsonRecommendedBook$ImageLinks;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "imageLinks"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "title"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
