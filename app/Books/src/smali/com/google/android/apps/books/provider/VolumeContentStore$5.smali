.class Lcom/google/android/apps/books/provider/VolumeContentStore$5;
.super Ljava/lang/Object;
.source "VolumeContentStore.java"

# interfaces
.implements Lcom/google/android/apps/books/data/InternalVolumeContentFile$TempFileSource;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/provider/VolumeContentStore;->dictionaryFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/provider/VolumeContentStore;

.field final synthetic val$dictionaryName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/provider/VolumeContentStore;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$5;->this$0:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iput-object p2, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$5;->val$dictionaryName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createTempFile()Ljava/io/File;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$5;->this$0:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/provider/VolumeContentStore$5;->val$dictionaryName:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/provider/VolumeContentStore;->createTempDictionaryFile(Ljava/lang/String;)Ljava/io/File;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->access$200(Lcom/google/android/apps/books/provider/VolumeContentStore;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
