.class public Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;
.super Ljava/lang/Object;
.source "MapBasedComparatorNullsEqual.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U::",
        "Ljava/lang/Comparable",
        "<TU;>;>",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;TU;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<TT;TU;>;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;, "Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual<TT;TU;>;"
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<TT;TU;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;->mMap:Ljava/util/Map;

    .line 18
    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;, "Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual<TT;TU;>;"
    .local p1, "lhs":Ljava/lang/Object;, "TT;"
    .local p2, "rhs":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;->mMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 23
    .local v0, "leftIndex":Ljava/lang/Comparable;, "TU;"
    iget-object v2, p0, Lcom/google/android/apps/books/util/MapBasedComparatorNullsEqual;->mMap:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    .line 24
    .local v1, "rightIndex":Ljava/lang/Comparable;, "TU;"
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 25
    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    .line 27
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
