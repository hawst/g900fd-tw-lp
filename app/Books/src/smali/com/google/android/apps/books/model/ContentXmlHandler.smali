.class public abstract Lcom/google/android/apps/books/model/ContentXmlHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "ContentXmlHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;,
        Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;
    }
.end annotation


# instance fields
.field private mScriptOpen:Z

.field private mStyleOpen:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 54
    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ContentXmlHandler;->mScriptOpen:Z

    .line 59
    iput-boolean v0, p0, Lcom/google/android/apps/books/model/ContentXmlHandler;->mStyleOpen:Z

    return-void
.end method

.method private static cleanContentApi(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 36
    const/16 v0, 0xa

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getReadingPosition(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;
    .locals 2
    .param p0, "qName"    # Ljava/lang/String;
    .param p1, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 40
    const-string v1, "a"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    const-string v1, "id"

    invoke-interface {p1, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "position":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "GBS."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    .end local v0    # "position":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private newXmlReader()Lorg/xml/sax/XMLReader;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
        }
    .end annotation

    .prologue
    .line 107
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    .line 108
    .local v1, "reader":Lorg/xml/sax/XMLReader;
    const-string v2, "http://xml.org/sax/features/namespaces"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V

    .line 109
    const-string v2, "http://xml.org/sax/features/namespace-prefixes"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 110
    return-object v1

    .line 111
    .end local v1    # "reader":Lorg/xml/sax/XMLReader;
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Lorg/xml/sax/SAXException;
    new-instance v2, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 113
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    :catch_1
    move-exception v0

    .line 114
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    new-instance v2, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private updateOpenTags(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "qName"    # Ljava/lang/String;
    .param p2, "opened"    # Z

    .prologue
    .line 84
    const-string v0, "script"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iput-boolean p2, p0, Lcom/google/android/apps/books/model/ContentXmlHandler;->mScriptOpen:Z

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const-string v0, "style"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iput-boolean p2, p0, Lcom/google/android/apps/books/model/ContentXmlHandler;->mStyleOpen:Z

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;
        }
    .end annotation

    .prologue
    .line 63
    iget-boolean v1, p0, Lcom/google/android/apps/books/model/ContentXmlHandler;->mScriptOpen:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/books/model/ContentXmlHandler;->mStyleOpen:Z

    if-eqz v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v1}, Lcom/google/android/apps/books/model/ContentXmlHandler;->cleanContentApi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler;->onText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler;->updateOpenTags(Ljava/lang/String;Z)V

    .line 78
    return-void
.end method

.method protected abstract onText(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException;
        }
    .end annotation
.end method

.method public parse(Ljava/lang/String;)V
    .locals 4
    .param p1, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/apps/books/model/ContentXmlHandler;->newXmlReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    .line 93
    .local v1, "reader":Lorg/xml/sax/XMLReader;
    invoke-interface {v1, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 95
    :try_start_0
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v1, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/ContentXmlHandler$StopParsingException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Lorg/xml/sax/SAXException;
    new-instance v2, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 100
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    :catch_1
    move-exception v0

    .line 101
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;

    invoke-direct {v2, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler$ContentParseException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 96
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    goto :goto_0
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 72
    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/books/model/ContentXmlHandler;->updateOpenTags(Ljava/lang/String;Z)V

    .line 73
    return-void
.end method
