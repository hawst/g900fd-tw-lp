.class public interface abstract Lcom/google/android/apps/books/navigation/PageViewContent;
.super Ljava/lang/Object;
.source "PageViewContent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/navigation/PageViewContent$Listener;
    }
.end annotation


# virtual methods
.method public abstract detachListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)V
.end method

.method public abstract draw(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
.end method

.method public abstract getBookmark()Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
.end method

.method public abstract getPageHandle()Lcom/google/android/apps/books/render/PageHandle;
.end method

.method public abstract getPagePositionOnScreen()Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
.end method

.method public abstract getPageRect(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
.end method

.method public abstract getPageRendering()Lcom/google/android/apps/books/widget/DevicePageRendering;
.end method

.method public abstract getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
.end method

.method public abstract isDestroyed()Z
.end method

.method public abstract isReadyToDraw(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)Z
.end method

.method public abstract onBookmarksChanged()V
.end method

.method public abstract setListener(Lcom/google/android/apps/books/navigation/PageViewContent$Listener;)V
.end method
