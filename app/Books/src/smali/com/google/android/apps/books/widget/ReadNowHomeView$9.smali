.class Lcom/google/android/apps/books/widget/ReadNowHomeView$9;
.super Ljava/lang/Object;
.source "ReadNowHomeView.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/widget/ReadNowHomeView;->withShouldShowOnboardingCard(Lcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

.field final synthetic val$showOnboardingCardConsumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/ReadNowHomeView;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 347
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iput-object p2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;->val$showOnboardingCardConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "isSchoolOwned"    # Ljava/lang/Boolean;

    .prologue
    const/4 v1, 0x0

    .line 350
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;->val$showOnboardingCardConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;->this$0:Lcom/google/android/apps/books/widget/ReadNowHomeView;

    iget-object v2, v2, Lcom/google/android/apps/books/widget/ReadNowHomeView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    .line 356
    .local v0, "prefs":Lcom/google/android/apps/books/preference/LocalPreferences;
    iget-object v2, p0, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;->val$showOnboardingCardConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/preference/LocalPreferences;->getSuppressOnboardingCard()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 347
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/ReadNowHomeView$9;->take(Ljava/lang/Boolean;)V

    return-void
.end method
