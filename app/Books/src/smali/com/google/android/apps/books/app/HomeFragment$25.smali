.class Lcom/google/android/apps/books/app/HomeFragment$25;
.super Ljava/lang/Object;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/HomeFragment;->updatePin(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/HomeFragment;

.field final synthetic val$pinned:Z

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/HomeFragment;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2304
    iput-object p1, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iput-boolean p2, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->val$pinned:Z

    iput-object p3, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 3
    .param p1, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 2307
    const-string v0, "HomeFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2308
    const-string v0, "HomeFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting pinned="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->val$pinned:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2310
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    iget-object v1, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->val$volumeId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->val$pinned:Z

    # invokes: Lcom/google/android/apps/books/app/HomeFragment;->updatePinInDataController(Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Z)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/books/app/HomeFragment;->access$4000(Lcom/google/android/apps/books/app/HomeFragment;Lcom/google/android/apps/books/data/BooksDataController;Ljava/lang/String;Z)V

    .line 2311
    iget-boolean v0, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->val$pinned:Z

    if-eqz v0, :cond_1

    .line 2313
    iget-object v0, p0, Lcom/google/android/apps/books/app/HomeFragment$25;->this$0:Lcom/google/android/apps/books/app/HomeFragment;

    # getter for: Lcom/google/android/apps/books/app/HomeFragment;->mLoadVolumesScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;
    invoke-static {v0}, Lcom/google/android/apps/books/app/HomeFragment;->access$2600(Lcom/google/android/apps/books/app/HomeFragment;)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    .line 2315
    :cond_1
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2304
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/HomeFragment$25;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
