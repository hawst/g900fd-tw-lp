.class public Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SqliteLocalAnnotationDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AnnotationDatabaseHelper"
.end annotation


# static fields
.field public static final DATABASE_VERSION:I = 0x1b


# instance fields
.field private mPathToBeEnsured:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 225
    const/16 v0, 0x1b

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "databaseVersion"    # I

    .prologue
    .line 230
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 231
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->mPathToBeEnsured:Ljava/lang/String;

    .line 232
    return-void
.end method

.method private static addLayerInvalidationFields(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 349
    const-string v3, "AnnotationsDB"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 350
    const-string v3, "AnnotationsDB"

    const-string v4, "Migrating annotations table from 23 to 24"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_0
    const-string v1, "local_id, layer_id, type, volume_id, content_version, start_position, start_offset, end_position, end_offset, before_selected_text, selected_text, after_selected_text, server_id, server_timestamp, should_delete_on_server, segment_index, data_id, color, notes"

    .line 359
    .local v1, "selectColumns":Ljava/lang/String;
    const-string v0, "local_id, layer_id, type, volume_id, content_version, start_position, start_offset, end_position, end_offset, before_selected_text, selected_text, after_selected_text, server_id, server_timestamp, should_delete_on_server, segment_index, data_id, color, notes, layer_version"

    .line 361
    .local v0, "insertColumns":Ljava/lang/String;
    const-string v2, "annotations__TEMP"

    .line 362
    .local v2, "tempTableName":Ljava/lang/String;
    const-string v3, "ALTER TABLE annotations RENAME TO annotations__TEMP"

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 363
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createAnnotationTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 364
    const-string v3, "INSERT INTO annotations (local_id, layer_id, type, volume_id, content_version, start_position, start_offset, end_position, end_offset, before_selected_text, selected_text, after_selected_text, server_id, server_timestamp, should_delete_on_server, segment_index, data_id, color, notes, layer_version) SELECT local_id, layer_id, type, volume_id, content_version, start_position, start_offset, end_position, end_offset, before_selected_text, selected_text, after_selected_text, server_id, server_timestamp, should_delete_on_server, segment_index, data_id, color, notes, \'\' FROM annotations__TEMP"

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 366
    const-string v3, "DROP TABLE IF EXISTS annotations__TEMP"

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 367
    return-void
.end method

.method private static createAnnotationTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 393
    const-string v0, "CREATE TABLE annotations (_id INTEGER PRIMARY KEY, local_id TEXT UNIQUE, layer_id TEXT, layer_version TEXT NOT NULL DEFAULT \'\', type TEXT, volume_id TEXT, content_version TEXT, start_position TEXT, start_offset INTEGER, end_position TEXT, end_offset INTEGER, before_selected_text TEXT, selected_text TEXT, after_selected_text TEXT, server_id TEXT, server_timestamp LONG, color LONG, notes TEXT, should_delete_on_server INTEGER, segment_index INTEGER, data_id TEXT, image_start_cfi TEXT, image_end_cfi TEXT, last_used_timestamp LONG NOT NULL DEFAULT -1, UNIQUE(content_version, layer_version, server_id));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 394
    return-void
.end method

.method private static createSummaryTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 389
    invoke-static {}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->getCreationSql()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 390
    return-void
.end method

.method private static createVolumeLayersTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 397
    invoke-static {}, Lcom/google/android/apps/books/annotations/LayersTable;->getCreationSql()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method private maybeEnsureParent()V
    .locals 4

    .prologue
    .line 249
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->mPathToBeEnsured:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 250
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->mPathToBeEnsured:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 251
    .local v0, "parentFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 252
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    const-string v1, "AnnotationsDB"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    const-string v1, "AnnotationsDB"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tried to create parent directory but failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->mPathToBeEnsured:Ljava/lang/String;

    .line 259
    .end local v0    # "parentFile":Ljava/io/File;
    :cond_1
    return-void
.end method

.method private recreateAnnotationsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 373
    const-string v0, "AnnotationsDB"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "AnnotationsDB"

    const-string v1, "Recreating annotations table"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :cond_0
    const-string v0, "DROP TABLE IF EXISTS annotations"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 377
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createAnnotationTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 378
    return-void
.end method

.method private static recreateSummaryTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 381
    const-string v0, "AnnotationsDB"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    const-string v0, "AnnotationsDB"

    const-string v1, "Recreating segment volume annotations table"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/annotations/SegmentVolumeAnnotationsSummaryTable;->getDropSql()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 385
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createSummaryTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 386
    return-void
.end method


# virtual methods
.method public getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->maybeEnsureParent()V

    .line 237
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->maybeEnsureParent()V

    .line 243
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 263
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createAnnotationTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 264
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createSummaryTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 265
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createVolumeLayersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 266
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/16 v3, 0x16

    .line 270
    move v2, p2

    .line 273
    .local v2, "versionDuringUpgrade":I
    const/4 v0, 0x0

    .line 274
    .local v0, "updatedAnnotationsTable":Z
    const/4 v1, 0x0

    .line 276
    .local v1, "updatedLayersTable":Z
    if-ge v2, v3, :cond_0

    .line 283
    const/16 v2, 0x16

    .line 286
    :cond_0
    if-ne v2, v3, :cond_1

    .line 291
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->recreateAnnotationsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 292
    const/4 v0, 0x1

    .line 293
    const/16 v2, 0x17

    .line 296
    :cond_1
    const/16 v3, 0x17

    if-ne v2, v3, :cond_4

    .line 297
    if-nez v0, :cond_2

    .line 300
    const-string v3, "DELETE FROM annotations where segment_index NOT NULL"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 304
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->addLayerInvalidationFields(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 305
    const/4 v0, 0x1

    .line 307
    :cond_2
    const-string v3, "AnnotationsDB"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 308
    const-string v3, "AnnotationsDB"

    const-string v4, "Creating volume layers table"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :cond_3
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->recreateSummaryTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 311
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/SqliteLocalAnnotationDatabase$AnnotationDatabaseHelper;->createVolumeLayersTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 312
    const/4 v1, 0x1

    .line 313
    const/16 v2, 0x18

    .line 316
    :cond_4
    const/16 v3, 0x18

    if-ne v2, v3, :cond_6

    .line 317
    if-nez v0, :cond_5

    .line 318
    const-string v3, "ALTER TABLE annotations ADD COLUMN image_start_cfi TEXT"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 320
    const-string v3, "ALTER TABLE annotations ADD COLUMN image_end_cfi TEXT"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 323
    :cond_5
    const/16 v2, 0x19

    .line 326
    :cond_6
    const/16 v3, 0x19

    if-ne v2, v3, :cond_8

    .line 327
    if-nez v1, :cond_7

    .line 328
    invoke-static {p1}, Lcom/google/android/apps/books/annotations/LayersTable;->addCharacterQuotaFields(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 329
    const/4 v1, 0x1

    .line 331
    :cond_7
    const/16 v2, 0x1a

    .line 334
    :cond_8
    const/16 v3, 0x1a

    if-ne v2, v3, :cond_a

    .line 335
    if-nez v0, :cond_9

    .line 336
    const-string v3, "ALTER TABLE annotations ADD COLUMN last_used_timestamp LONG NOT NULL DEFAULT -1"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 339
    :cond_9
    const/16 v2, 0x1b

    .line 341
    :cond_a
    return-void
.end method
