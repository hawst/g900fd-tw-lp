.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DownloadFailureAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

.field public static final enum NO_LICENSE_WHEN_OPENING:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

.field public static final enum NO_LICENSE_WHEN_PINNED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

.field public static final enum SYNC_FAILURE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 499
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    const-string v1, "NO_LICENSE_WHEN_OPENING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->NO_LICENSE_WHEN_OPENING:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    .line 504
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    const-string v1, "NO_LICENSE_WHEN_PINNED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->NO_LICENSE_WHEN_PINNED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    .line 510
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    const-string v1, "SYNC_FAILURE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->SYNC_FAILURE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    .line 495
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->NO_LICENSE_WHEN_OPENING:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->NO_LICENSE_WHEN_PINNED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->SYNC_FAILURE:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 495
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 495
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;
    .locals 1

    .prologue
    .line 495
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DownloadFailureAction;

    return-object v0
.end method
