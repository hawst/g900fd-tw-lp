.class Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;
.super Ljava/lang/Object;
.source "NotifyingProvidelet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "State"
.end annotation


# instance fields
.field mBatchSuccessful:Z

.field final mPendingUris:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;->mPendingUris:Ljava/util/HashSet;

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;->mBatchSuccessful:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/provider/NotifyingProvidelet$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/provider/NotifyingProvidelet$1;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/books/provider/NotifyingProvidelet$Notifier$State;-><init>()V

    return-void
.end method
