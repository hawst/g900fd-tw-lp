.class public Lcom/google/android/apps/books/tts/AltSegmentedText;
.super Lcom/google/android/apps/books/tts/SegmentedText;
.source "AltSegmentedText.java"


# instance fields
.field private final mAltStringsLengths:[I

.field private final mAltStringsOffsets:[I

.field private final mAltTextMapOrig:Lcom/google/android/apps/books/model/LabelMap;

.field private final mVisibleText:Lcom/google/android/apps/books/tts/SegmentedText;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;[I[ILcom/google/android/apps/books/tts/SegmentedText;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "altStringsOffsets"    # [I
    .param p4, "altStringsLengths"    # [I
    .param p5, "visibleText"    # Lcom/google/android/apps/books/tts/SegmentedText;
    .param p6, "altTextMap"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 101
    invoke-virtual {p5}, Lcom/google/android/apps/books/tts/SegmentedText;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/tts/SegmentedText;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V

    .line 103
    iput-object p3, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltStringsOffsets:[I

    .line 104
    iput-object p4, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltStringsLengths:[I

    .line 105
    iput-object p5, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mVisibleText:Lcom/google/android/apps/books/tts/SegmentedText;

    .line 106
    iput-object p6, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltTextMapOrig:Lcom/google/android/apps/books/model/LabelMap;

    .line 107
    return-void
.end method

.method public static create(Lcom/google/android/apps/books/tts/SegmentedText;Lcom/google/android/apps/books/model/LabelMap;)Lcom/google/android/apps/books/tts/AltSegmentedText;
    .locals 21
    .param p0, "visibleText"    # Lcom/google/android/apps/books/tts/SegmentedText;
    .param p1, "altTextMap"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/tts/SegmentedText;->getText()Ljava/lang/String;

    move-result-object v20

    .line 47
    .local v20, "text":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/tts/SegmentedText;->getPositionOffsets()Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/LabelMap;->cloneOffsets()[I

    move-result-object v17

    .line 48
    .local v17, "posOffsetsInMergedText":[I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/model/LabelMap;->cloneOffsets()[I

    move-result-object v5

    .line 49
    .local v5, "altOffsetsInMergedText":[I
    array-length v2, v5

    new-array v6, v2, [I

    .line 51
    .local v6, "altStringsLengths":[I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .local v19, "sb":Ljava/lang/StringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/tts/SegmentedText;->getNumberOfPositions()I

    move-result v16

    .line 54
    .local v16, "posCount":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/model/LabelMap;->getNumberOfLabels()I

    move-result v9

    .line 55
    .local v9, "altCount":I
    const/4 v14, 0x0

    .line 56
    .local v14, "nextPosIndex":I
    const/4 v12, 0x0

    .line 57
    .local v12, "nextAltIndex":I
    const/16 v18, 0x0

    .line 58
    .local v18, "prevAltOffset":I
    const/4 v11, 0x0

    .line 60
    .local v11, "insertedLength":I
    :cond_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_2

    aget v15, v17, v14

    .line 62
    .local v15, "nextPosOffset":I
    :goto_0
    if-ge v12, v9, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/books/model/LabelMap;->getCharacterOffsetAt(I)I

    move-result v13

    .line 64
    .local v13, "nextAltOffset":I
    :goto_1
    if-gt v15, v13, :cond_4

    .line 66
    aget v2, v17, v14

    add-int/2addr v2, v11

    aput v2, v17, v14

    .line 67
    add-int/lit8 v14, v14, 0x1

    .line 85
    :goto_2
    move/from16 v0, v16

    if-lt v14, v0, :cond_0

    if-lt v12, v9, :cond_0

    .line 87
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_1

    .line 88
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_1
    new-instance v4, Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/tts/SegmentedText;->getPositionOffsets()Lcom/google/android/apps/books/model/LabelMap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/LabelMap;->cloneLabels()[Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-direct {v4, v0, v2}, Lcom/google/android/apps/books/model/LabelMap;-><init>([I[Ljava/lang/String;)V

    .line 93
    .local v4, "positionOffsets":Lcom/google/android/apps/books/model/LabelMap;
    new-instance v2, Lcom/google/android/apps/books/tts/AltSegmentedText;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/books/tts/AltSegmentedText;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;[I[ILcom/google/android/apps/books/tts/SegmentedText;Lcom/google/android/apps/books/model/LabelMap;)V

    return-object v2

    .line 60
    .end local v4    # "positionOffsets":Lcom/google/android/apps/books/model/LabelMap;
    .end local v13    # "nextAltOffset":I
    .end local v15    # "nextPosOffset":I
    :cond_2
    const v15, 0x7fffffff

    goto :goto_0

    .line 62
    .restart local v15    # "nextPosOffset":I
    :cond_3
    const v13, 0x7fffffff

    goto :goto_1

    .line 69
    .restart local v13    # "nextAltOffset":I
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/books/model/LabelMap;->getLabelAt(I)Ljava/lang/String;

    move-result-object v10

    .line 72
    .local v10, "altText":Ljava/lang/String;
    aget v2, v5, v12

    add-int/2addr v2, v11

    aput v2, v5, v12

    .line 73
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    aput v2, v6, v12

    .line 76
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    move/from16 v18, v13

    .line 79
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v11, v2

    .line 82
    add-int/lit8 v12, v12, 0x1

    goto :goto_2
.end method

.method private mapToOriginalOffset(IZ)I
    .locals 7
    .param p1, "offsetInCombinedText"    # I
    .param p2, "isEnd"    # Z

    .prologue
    const/4 v4, -0x1

    .line 134
    iget-object v5, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltStringsOffsets:[I

    invoke-static {v5, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v3

    .line 135
    .local v3, "searchRes":I
    if-ltz v3, :cond_1

    .line 136
    iget-object v5, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltStringsOffsets:[I

    aget v5, v5, v3

    iget-object v6, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltTextMapOrig:Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/books/model/LabelMap;->getCharacterOffsetAt(I)I

    move-result v6

    sub-int v1, v5, v6

    .line 139
    .local v1, "insertedLength":I
    if-eqz p2, :cond_0

    sub-int v4, p1, v1

    .line 152
    .end local v1    # "insertedLength":I
    :cond_0
    :goto_0
    return v4

    .line 140
    :cond_1
    if-ne v3, v4, :cond_2

    move v4, p1

    .line 142
    goto :goto_0

    .line 144
    :cond_2
    neg-int v5, v3

    add-int/lit8 v2, v5, -0x2

    .line 145
    .local v2, "prevAltNdx":I
    iget-object v5, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltStringsOffsets:[I

    aget v5, v5, v2

    iget-object v6, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltStringsLengths:[I

    aget v6, v6, v2

    add-int v0, v5, v6

    .line 146
    .local v0, "altEnd":I
    if-lt p1, v0, :cond_0

    if-eqz p2, :cond_3

    if-eq p1, v0, :cond_0

    .line 150
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mAltTextMapOrig:Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/books/model/LabelMap;->getCharacterOffsetAt(I)I

    move-result v4

    sub-int v1, v0, v4

    .line 152
    .restart local v1    # "insertedLength":I
    sub-int v4, p1, v1

    goto :goto_0
.end method


# virtual methods
.method public getNormalizedRange(Lcom/google/android/apps/books/util/Range;I)Lcom/google/android/apps/books/annotations/TextLocationRange;
    .locals 6
    .param p1, "phraseRange"    # Lcom/google/android/apps/books/util/Range;
    .param p2, "passageIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 114
    iget v4, p1, Lcom/google/android/apps/books/util/Range;->start:I

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/books/tts/AltSegmentedText;->mapToOriginalOffset(IZ)I

    move-result v1

    .line 115
    .local v1, "origStart":I
    iget v4, p1, Lcom/google/android/apps/books/util/Range;->end:I

    const/4 v5, 0x1

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/books/tts/AltSegmentedText;->mapToOriginalOffset(IZ)I

    move-result v0

    .line 116
    .local v0, "origEnd":I
    if-ltz v1, :cond_0

    if-ltz v0, :cond_0

    .line 117
    iget-object v4, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mVisibleText:Lcom/google/android/apps/books/tts/SegmentedText;

    invoke-virtual {v4}, Lcom/google/android/apps/books/tts/SegmentedText;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 120
    .local v2, "textToSpeak":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 124
    .end local v2    # "textToSpeak":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 120
    .restart local v2    # "textToSpeak":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/tts/AltSegmentedText;->mVisibleText:Lcom/google/android/apps/books/tts/SegmentedText;

    new-instance v4, Lcom/google/android/apps/books/util/Range;

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/books/util/Range;-><init>(II)V

    invoke-virtual {v3, v4, p2}, Lcom/google/android/apps/books/tts/SegmentedText;->getNormalizedRange(Lcom/google/android/apps/books/util/Range;I)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v3

    goto :goto_0
.end method
