.class public interface abstract Lcom/google/android/apps/books/widget/PlayCardAnimationHelper$AnimatableCardGroup;
.super Ljava/lang/Object;
.source "PlayCardAnimationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PlayCardAnimationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AnimatableCardGroup"
.end annotation


# virtual methods
.method public abstract getChildAt(I)Landroid/view/View;
.end method

.method public abstract getChildCount()I
.end method

.method public abstract isAnimationEnabled()Z
.end method

.method public abstract onAnimationEnd()V
.end method

.method public abstract removeView(Landroid/view/View;)V
.end method

.method public abstract setupCard(Lcom/google/android/ublib/cardlib/layout/PlayCardView;Lcom/google/android/apps/books/playcards/BookDocument;Z)V
.end method
