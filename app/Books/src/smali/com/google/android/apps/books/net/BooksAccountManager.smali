.class public interface abstract Lcom/google/android/apps/books/net/BooksAccountManager;
.super Ljava/lang/Object;
.source "BooksAccountManager.java"


# virtual methods
.method public abstract getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract invalidateAuthToken(Ljava/lang/String;)V
.end method
