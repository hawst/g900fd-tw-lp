.class public Lcom/google/android/apps/books/widget/WalkerFromCollection;
.super Ljava/lang/Object;
.source "WalkerFromCollection.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/Walker;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/apps/books/widget/Assignable",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/widget/Walker",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final mCollection:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation
.end field

.field mIterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/apps/books/widget/WalkerFromCollection;, "Lcom/google/android/apps/books/widget/WalkerFromCollection<TT;>;"
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mCollection:Ljava/util/Collection;

    .line 15
    iget-object v0, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mCollection:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mIterator:Ljava/util/Iterator;

    .line 16
    return-void
.end method


# virtual methods
.method public next(Lcom/google/android/apps/books/widget/Assignable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/apps/books/widget/WalkerFromCollection;, "Lcom/google/android/apps/books/widget/WalkerFromCollection<TT;>;"
    .local p1, "walkedObj":Lcom/google/android/apps/books/widget/Assignable;, "TT;"
    iget-object v1, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    iget-object v1, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mIterator:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/Assignable;

    .line 26
    .local v0, "currentObj":Lcom/google/android/apps/books/widget/Assignable;, "TT;"
    invoke-interface {p1, v0}, Lcom/google/android/apps/books/widget/Assignable;->assign(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const/4 v1, 0x1

    .line 29
    .end local v0    # "currentObj":Lcom/google/android/apps/books/widget/Assignable;, "TT;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 9
    .local p0, "this":Lcom/google/android/apps/books/widget/WalkerFromCollection;, "Lcom/google/android/apps/books/widget/WalkerFromCollection<TT;>;"
    check-cast p1, Lcom/google/android/apps/books/widget/Assignable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/widget/WalkerFromCollection;->next(Lcom/google/android/apps/books/widget/Assignable;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/apps/books/widget/WalkerFromCollection;, "Lcom/google/android/apps/books/widget/WalkerFromCollection<TT;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mCollection:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/widget/WalkerFromCollection;->mIterator:Ljava/util/Iterator;

    .line 20
    return-void
.end method
