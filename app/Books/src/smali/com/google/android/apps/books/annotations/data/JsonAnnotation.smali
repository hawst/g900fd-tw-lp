.class public Lcom/google/android/apps/books/annotations/data/JsonAnnotation;
.super Ljava/lang/Object;
.source "JsonAnnotation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/data/JsonAnnotation$BridgeContentRange;,
        Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;,
        Lcom/google/android/apps/books/annotations/data/JsonAnnotation$ImageCfiRange;,
        Lcom/google/android/apps/books/annotations/data/JsonAnnotation$GbTextRange;,
        Lcom/google/android/apps/books/annotations/data/JsonAnnotation$Annotations;
    }
.end annotation


# instance fields
.field public afterSelectedText:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "afterSelectedText"
    .end annotation
.end field

.field public annotationType:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "annotationType"
    .end annotation
.end field

.field public beforeSelectedText:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "beforeSelectedText"
    .end annotation
.end field

.field public clientVersionRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "clientVersionRanges"
    .end annotation
.end field

.field public contentRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "contentRanges"
    .end annotation
.end field

.field public currentVersionRanges:Lcom/google/android/apps/books/annotations/data/JsonAnnotation$VersionRanges;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "currentVersionRanges"
    .end annotation
.end field

.field public data:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "data"
    .end annotation
.end field

.field public dataId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "annotationDataId"
    .end annotation
.end field

.field public dataLink:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "annotationDataLink"
    .end annotation
.end field

.field public highlightedStyle:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "highlightStyle"
    .end annotation
.end field

.field public isDeleted:Z
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "deleted"
    .end annotation
.end field

.field public layerId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "layerId"
    .end annotation
.end field

.field public layerSummary:Lcom/google/android/apps/books/annotations/data/JsonLayer;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "layerSummary"
    .end annotation
.end field

.field public localId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "localId"
    .end annotation
.end field

.field public pageIds:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "pageIds"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public selectedText:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "selectedText"
    .end annotation
.end field

.field public serverId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "id"
    .end annotation
.end field

.field public type:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "type"
    .end annotation
.end field

.field public updated:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "updated"
    .end annotation
.end field

.field public volumeId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "volumeId"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method
