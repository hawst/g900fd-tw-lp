.class Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MaybePurgePassagesRequest"
.end annotation


# instance fields
.field final mPassageIndices:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;ILjava/util/Collection;)V
    .locals 6
    .param p2, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1357
    .local p3, "passages":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1358
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1359
    iput-object p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;->mPassageIndices:Ljava/util/Collection;

    .line 1360
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1364
    const-string v0, "ReaderController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1365
    const-string v0, "ReaderController"

    const-string v1, "Issuing maybePurgePassages"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 1368
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;->mPassageIndices:Ljava/util/Collection;

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$MaybePurgePassagesRequest;->mTaskRequestId:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/render/TextReader;->maybePurgePassages(Ljava/util/Collection;I)V

    .line 1369
    return-void
.end method
