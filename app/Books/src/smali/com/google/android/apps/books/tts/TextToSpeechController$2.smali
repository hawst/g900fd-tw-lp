.class Lcom/google/android/apps/books/tts/TextToSpeechController$2;
.super Ljava/lang/Object;
.source "TextToSpeechController.java"

# interfaces
.implements Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/tts/TextToSpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/tts/TextToSpeechController;)V
    .locals 0

    .prologue
    .line 1121
    iput-object p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onUnexpectedPhrase(Ljava/lang/String;)V
    .locals 6
    .param p1, "phraseId"    # Ljava/lang/String;

    .prologue
    .line 1152
    const-string v3, "BooksTTS"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1153
    const-string v3, "BooksTTS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected phrase "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ; restarting queue"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$300(Lcom/google/android/apps/books/tts/TextToSpeechController;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1159
    .local v1, "queuedPhraseId":Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1160
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$300(Lcom/google/android/apps/books/tts/TextToSpeechController;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    iget-object v2, v3, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 1162
    .local v2, "startPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    invoke-virtual {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->stopSpeaking()V

    .line 1163
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    iget-object v4, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z
    invoke-static {v4}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$400(Lcom/google/android/apps/books/tts/TextToSpeechController;)Z

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/books/tts/TextToSpeechController;->startSpeakingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Z)V

    .line 1167
    .end local v1    # "queuedPhraseId":Ljava/lang/String;
    .end local v2    # "startPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    :cond_2
    return-void
.end method


# virtual methods
.method public onInitializationError()V
    .locals 2

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->tearDownSynthesizer()V
    invoke-static {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$1000(Lcom/google/android/apps/books/tts/TextToSpeechController;)V

    .line 1224
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    sget-object v1, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->OTHER_ERROR:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$500(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 1225
    return-void
.end method

.method public onPhraseFinished(Ljava/lang/String;)V
    .locals 7
    .param p1, "phraseId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    .line 1177
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$300(Lcom/google/android/apps/books/tts/TextToSpeechController;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1180
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;>;"
    const-string v3, "BooksTTS"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1181
    const-string v3, "BooksTTS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received completion callback for phrase "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->checkPing(Ljava/lang/String;)Z
    invoke-static {v3, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$200(Lcom/google/android/apps/books/tts/TextToSpeechController;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1219
    :cond_1
    :goto_0
    return-void

    .line 1188
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1189
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1190
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1191
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1192
    const-string v3, "BooksTTS"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1193
    const-string v3, "BooksTTS"

    const-string v4, "Matched first queued phrase"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$400(Lcom/google/android/apps/books/tts/TextToSpeechController;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1196
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    sget-object v4, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->COMPLETED_ITEM:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$500(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 1206
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;"
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1207
    const-string v3, "BooksTTS"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1208
    const-string v3, "BooksTTS"

    const-string v4, "Highlighting next enqueued phrase"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1210
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    iget-object v2, v3, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 1211
    .local v2, "sentence":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->focusOnPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V
    invoke-static {v3, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$600(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    .line 1214
    .end local v2    # "sentence":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$700(Lcom/google/android/apps/books/tts/TextToSpeechController;)Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 1215
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    iget-object v4, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;
    invoke-static {v4}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$800(Lcom/google/android/apps/books/tts/TextToSpeechController;)Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    move-result-object v4

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$500(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    goto :goto_0

    .line 1199
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;"
    :cond_7
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->onUnexpectedPhrase(Ljava/lang/String;)V

    goto :goto_0

    .line 1216
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;"
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$400(Lcom/google/android/apps/books/tts/TextToSpeechController;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1217
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueue()V
    invoke-static {v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$900(Lcom/google/android/apps/books/tts/TextToSpeechController;)V

    goto/16 :goto_0
.end method

.method public onPhraseStarted(Ljava/lang/String;)V
    .locals 5
    .param p1, "phraseId"    # Ljava/lang/String;

    .prologue
    .line 1125
    const-string v2, "BooksTTS"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1126
    const-string v2, "BooksTTS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received start callback for phrase "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # invokes: Lcom/google/android/apps/books/tts/TextToSpeechController;->checkPing(Ljava/lang/String;)Z
    invoke-static {v2, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$200(Lcom/google/android/apps/books/tts/TextToSpeechController;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1142
    :cond_1
    :goto_0
    return-void

    .line 1133
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->this$0:Lcom/google/android/apps/books/tts/TextToSpeechController;

    # getter for: Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->access$300(Lcom/google/android/apps/books/tts/TextToSpeechController;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1136
    .local v1, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;>;"
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1137
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1138
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1139
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController$2;->onUnexpectedPhrase(Ljava/lang/String;)V

    goto :goto_0
.end method
