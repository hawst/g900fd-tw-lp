.class public Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;
.super Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.source "SelectLicenseTypeDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;-><init>()V

    .line 90
    return-void
.end method

.method private onClicked(Z)V
    .locals 7
    .param p1, "offlineLicenseRequested"    # Z

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 43
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 45
    .local v2, "args":Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 46
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v5

    .line 47
    .local v5, "volumeId":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 49
    new-instance v3, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;

    invoke-direct {v3, p0, v0, v5, v1}, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;-><init>(Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;Landroid/accounts/Account;Ljava/lang/String;Landroid/app/Activity;)V

    .line 51
    .local v3, "callbacks":Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/books/net/OfflineLicenseManager;->requestOfflineLicense(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController;)V

    .line 65
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "args":Landroid/os/Bundle;
    .end local v3    # "callbacks":Lcom/google/android/apps/books/app/SelectLicenseTypeDialog$OpenOfflineCallbacks;
    .end local v5    # "volumeId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 57
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v2    # "args":Landroid/os/Bundle;
    .restart local v5    # "volumeId":Ljava/lang/String;
    :cond_1
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v4

    .line 59
    .local v4, "dataController":Lcom/google/android/apps/books/data/BooksDataController;
    if-eqz v4, :cond_0

    .line 60
    sget-object v6, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->READ:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/books/data/BooksDataController;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 82
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 83
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReadingActivity;->getReaderCallbacks(Landroid/content/Context;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v1

    .line 84
    .local v1, "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-interface {v1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->closeBook()V

    .line 88
    .end local v1    # "callbacks":Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    :cond_0
    return-void
.end method

.method protected onCancelClicked()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;->onClicked(Z)V

    .line 77
    return-void
.end method

.method protected onOkClicked(Z)V
    .locals 1
    .param p1, "checkboxChecked"    # Z

    .prologue
    .line 70
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/SelectLicenseTypeDialog;->onClicked(Z)V

    .line 71
    return-void
.end method
