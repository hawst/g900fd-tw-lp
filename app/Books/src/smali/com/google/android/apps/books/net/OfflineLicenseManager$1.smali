.class final Lcom/google/android/apps/books/net/OfflineLicenseManager$1;
.super Ljava/lang/Object;
.source "OfflineLicenseManager.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/net/OfflineLicenseManager;->requestOfflineLicense(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$callbacks:Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/apps/books/net/OfflineLicenseManager$1;->val$callbacks:Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;"
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerExecutor;->getUiThreadExecutor()Lcom/google/android/apps/books/util/HandlerExecutor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/net/OfflineLicenseManager$1$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/net/OfflineLicenseManager$1$1;-><init>(Lcom/google/android/apps/books/net/OfflineLicenseManager$1;Lcom/google/android/apps/books/util/ExceptionOr;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/HandlerExecutor;->execute(Ljava/lang/Runnable;)V

    .line 77
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 68
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/net/OfflineLicenseManager$1;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
