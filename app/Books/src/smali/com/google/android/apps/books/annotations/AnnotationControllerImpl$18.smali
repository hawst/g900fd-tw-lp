.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$ForegroundOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->locallyApplyAddFromServer(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$annotation:Lcom/google/android/apps/books/annotations/ServerAnnotation;

.field final synthetic val$layer:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/String;Lcom/google/android/apps/books/annotations/ServerAnnotation;)V
    .locals 0

    .prologue
    .line 1000
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;->val$layer:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;->val$annotation:Lcom/google/android/apps/books/annotations/ServerAnnotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V
    .locals 2
    .param p1, "cache"    # Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;->val$layer:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$18;->val$annotation:Lcom/google/android/apps/books/annotations/ServerAnnotation;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/ServerAnnotation;->annotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->add(Ljava/lang/String;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 1004
    return-void
.end method
