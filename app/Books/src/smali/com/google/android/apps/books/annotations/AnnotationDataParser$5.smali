.class final Lcom/google/android/apps/books/annotations/AnnotationDataParser$5;
.super Ljava/lang/Object;
.source "AnnotationDataParser.java"

# interfaces
.implements Lcom/google/android/apps/books/util/JsonUtils$NodeParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/AnnotationDataParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/books/util/JsonUtils$NodeParser",
        "<",
        "Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;
    .locals 3
    .param p1, "json"    # Lorg/codehaus/jackson/JsonNode;

    .prologue
    .line 290
    const-string v2, "text"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "text":Ljava/lang/String;
    const-string v2, "type"

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationDataParser;->getFieldStringValue(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p1, v2}, Lcom/google/android/apps/books/annotations/AnnotationDataParser;->access$300(Lorg/codehaus/jackson/JsonNode;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 292
    .local v1, "type":Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 287
    check-cast p1, Lorg/codehaus/jackson/JsonNode;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/annotations/AnnotationDataParser$5;->apply(Lorg/codehaus/jackson/JsonNode;)Lcom/google/android/apps/books/annotations/DictionaryEntry$Conjugation;

    move-result-object v0

    return-object v0
.end method
