.class public Lcom/google/android/apps/books/annotations/TextLocation;
.super Ljava/lang/Object;
.source "TextLocation.java"


# instance fields
.field public final offset:I

.field public final position:Lcom/google/android/apps/books/common/Position;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/common/Position;I)V
    .locals 0
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "offset"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    .line 70
    iput p2, p0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    .line 71
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "position"    # Ljava/lang/String;
    .param p2, "offset"    # I

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-direct {v0, p1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 66
    return-void
.end method

.method public static comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;
    .locals 1
    .param p0, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/common/Position$PageOrdering;",
            ")",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocation$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/annotations/TextLocation$1;-><init>(Lcom/google/android/apps/books/common/Position$PageOrdering;)V

    return-object v0
.end method

.method public static compare(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/common/Position$PageOrdering;)I
    .locals 3
    .param p0, "lhs"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .param p1, "rhs"    # Lcom/google/android/apps/books/annotations/TextLocation;
    .param p2, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;

    .prologue
    .line 40
    if-nez p0, :cond_1

    .line 41
    if-nez p1, :cond_0

    const/4 v1, 0x0

    .line 50
    :goto_0
    return v1

    .line 41
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 43
    :cond_1
    if-nez p1, :cond_2

    .line 44
    const/4 v1, -0x1

    goto :goto_0

    .line 46
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-static {v1, v2, p2}, Lcom/google/android/apps/books/common/Position;->compare(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v0

    .line 47
    .local v0, "positionComparison":I
    if-eqz v0, :cond_3

    move v1, v0

    .line 48
    goto :goto_0

    .line 50
    :cond_3
    iget v1, p0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    iget v2, p1, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    sub-int/2addr v1, v2

    goto :goto_0
.end method

.method public static createOrNull(Ljava/lang/String;I)Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 1
    .param p0, "pos"    # Ljava/lang/String;
    .param p1, "offset"    # I

    .prologue
    .line 54
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 75
    instance-of v2, p1, Lcom/google/android/apps/books/annotations/TextLocation;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 76
    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 77
    .local v0, "location":Lcom/google/android/apps/books/annotations/TextLocation;
    iget-object v2, v0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/common/Position;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    iget v3, p0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 79
    .end local v0    # "location":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 89
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "position"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "offset"

    iget v2, p0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
