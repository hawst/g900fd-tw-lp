.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->removeLocalVolumeAnnotationsForVolume(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;->val$volumeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$10;->val$volumeId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->removeVolumeAnnotationsForVolume(Ljava/lang/String;)V

    .line 468
    return-void
.end method
