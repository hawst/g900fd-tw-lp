.class Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$1;
.super Lcom/google/android/apps/books/model/ContentXmlHandler;
.source "ReadAlongDataSourceFromStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->getXmlHandler()Lcom/google/android/apps/books/model/ContentXmlHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/ContentXmlHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected onText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 295
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "localName"    # Ljava/lang/String;
    .param p3, "qName"    # Ljava/lang/String;
    .param p4, "attributes"    # Lorg/xml/sax/Attributes;

    .prologue
    .line 300
    const-string v2, "id"

    invoke-interface {p4, v2}, Lorg/xml/sax/Attributes;->getIndex(Ljava/lang/String;)I

    move-result v1

    .line 301
    .local v1, "idIndex":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 302
    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mIdSet:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->access$1400(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval$1;->this$1:Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;

    # getter for: Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->mFoundElements:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;->access$1500(Lcom/google/android/apps/books/app/ReadAlongDataSourceFromStorage$MediaOverlayItemRetrieval;)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    .end local v0    # "id":Ljava/lang/String;
    :cond_0
    return-void
.end method
