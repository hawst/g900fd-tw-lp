.class public Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;
.super Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;
.source "OnboardGenreQuizPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;,
        Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 130
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/graphics/Bitmap;
    .param p4, "x4"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method private createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    .locals 4
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "icon"    # Landroid/graphics/Bitmap;
    .param p4, "serverIndex"    # I

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    .line 150
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    const v2, 0x7f0e002f

    invoke-virtual {v0, v2, p1}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 151
    const v2, 0x7f0e0033

    invoke-virtual {v0, v2, p2}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 152
    const v2, 0x7f0e0031

    invoke-virtual {v0, v2, p3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mSelectedItemIds:Ljava/util/Set;

    if-eqz v2, :cond_1

    .line 156
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mSelectedItemIds:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 161
    .local v1, "selected":Z
    :goto_0
    const v2, 0x7f0e0035

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 163
    const v2, 0x7f0e0036

    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getItemClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 164
    sget v2, Lcom/google/android/libraries/bind/data/BindAdapter;->DK_VIEW_RES_ID:I

    const v3, 0x7f040072

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 165
    if-nez p3, :cond_0

    .line 166
    const v2, 0x7f0e0034

    invoke-static {p2}, Lcom/google/android/apps/books/util/StringUtils;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 169
    :cond_0
    const v2, 0x7f0e0032

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 170
    const v2, 0x7f0e0030

    const v3, 0x7f02009a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 171
    const v2, 0x7f0e0039

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/data/Data;->put(ILjava/lang/Object;)V

    .line 172
    return-object v0

    .line 158
    .end local v1    # "selected":Z
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "selected":Z
    goto :goto_0
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 187
    const/4 v0, 0x1

    return v0
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0028

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    const v1, 0x7f020089

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$1;-><init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Lcom/google/android/play/onboard/OnboardHostControl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getBooksHostControl()Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->getShowFullSequence()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getHeaderText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f020a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNumColumns()I
    .locals 1

    .prologue
    .line 177
    const v0, 0x7f0900fb

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->columnsPerRow(I)I

    move-result v0

    return v0
.end method

.method public getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 211
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f020d

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$2;-><init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I

    move-result v0

    if-lez v0, :cond_0

    .line 236
    invoke-super {p0, p1}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->onBackPressed(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    .line 237
    const/4 v0, 0x1

    .line 239
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onExitPage(Z)V
    .locals 2
    .param p1, "movingTowardsEnd"    # Z

    .prologue
    .line 226
    if-eqz p1, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mHostControl:Lcom/google/android/play/onboard/OnboardHostControl;

    check-cast v0, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;

    iget-object v1, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mSelectedItemIds:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/onboard/BooksOnboardHostFragment;->setSelectedGenres(Ljava/util/Set;)V

    .line 229
    :cond_0
    return-void
.end method

.method public saveOnboardState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 140
    invoke-super {p0, p1}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->saveOnboardState(Landroid/os/Bundle;)V

    .line 142
    const-string v1, "onboard_genres_selected"

    iget-object v0, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mSelectedItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 144
    return-void

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setController(Lcom/google/android/apps/books/app/OnboardingController;)V
    .locals 2
    .param p1, "controller"    # Lcom/google/android/apps/books/app/OnboardingController;

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/google/android/apps/books/onboard/BaseBooksQuizPage;->setController(Lcom/google/android/apps/books/app/OnboardingController;)V

    .line 135
    new-instance v0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$CategoryConsumer;-><init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$1;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/app/OnboardingController;->getSampleCategories(Lcom/google/android/ublib/utils/Consumer;)V

    .line 136
    return-void
.end method
