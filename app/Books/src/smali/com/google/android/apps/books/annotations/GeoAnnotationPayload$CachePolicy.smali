.class public final enum Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
.super Ljava/lang/Enum;
.source "GeoAnnotationPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/GeoAnnotationPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CachePolicy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

.field public static final enum NEVER:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

.field public static final enum RESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

.field public static final enum UNKNOWN:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

.field public static final enum UNRESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNKNOWN:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .line 27
    new-instance v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    const-string v1, "UNRESTRICTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNRESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .line 32
    new-instance v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->NEVER:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .line 40
    new-instance v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    const-string v1, "RESTRICTED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->RESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    sget-object v1, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNKNOWN:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->UNRESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->NEVER:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->RESTRICTED:Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->$VALUES:[Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->$VALUES:[Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/annotations/GeoAnnotationPayload$CachePolicy;

    return-object v0
.end method
