.class public Lcom/google/android/apps/books/data/SmallBlobFromServer;
.super Ljava/lang/Object;
.source "SmallBlobFromServer.java"

# interfaces
.implements Lcom/google/android/apps/books/data/DataControllerBlob;


# instance fields
.field private final mBytes:[B

.field private mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

.field private final mSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

.field private final mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;


# direct methods
.method public constructor <init>([BLcom/google/android/apps/books/model/BooksDataStore$ContentSaver;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "saver"    # Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .param p3, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mBytes:[B

    .line 32
    iput-object p2, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    .line 34
    return-void
.end method

.method private saveTemp()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    if-nez v1, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/SmallBlobFromServer;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 51
    .local v0, "input":Ljava/io/InputStream;
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mSaver:Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    new-instance v2, Lcom/google/android/apps/books/model/EncryptedContentImpl;

    iget-object v3, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mSessionKeyId:Lcom/google/android/apps/books/model/SessionKeyId;

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/books/model/EncryptedContentImpl;-><init>(Lcom/google/android/apps/books/model/SessionKeyId;Ljava/io/InputStream;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;->saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 56
    .end local v0    # "input":Ljava/io/InputStream;
    :cond_0
    return-void

    .line 53
    .restart local v0    # "input":Ljava/io/InputStream;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method


# virtual methods
.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/books/data/SmallBlobFromServer;->saveTemp()V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mBytes:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public save()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/books/data/SmallBlobFromServer;->saveTemp()V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/data/SmallBlobFromServer;->mCommitter:Lcom/google/android/apps/books/model/BooksDataStore$Committer;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$Committer;->commit()V

    .line 62
    return-void
.end method
