.class public Lcom/google/android/apps/books/annotations/AnnotationUtils;
.super Ljava/lang/Object;
.source "AnnotationUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;,
        Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;
    }
.end annotation


# static fields
.field public static DRAWING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

.field public static final IDENTITY_PALETTE:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

.field private static NUM_COLORS:I

.field public static SERVING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

.field private static TAG:Ljava/lang/String;

.field private static final WHITESPACE:Ljava/util/regex/Pattern;

.field private static sDrawingColors:[I

.field private static sInitialized:Z

.field private static sMatchingColors:[I

.field private static sServingColors:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationUtils$1;

    invoke-direct {v0}, Lcom/google/android/apps/books/annotations/AnnotationUtils$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->IDENTITY_PALETTE:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    .line 22
    const-string v0, "AnnotationUtils"

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->TAG:Ljava/lang/String;

    .line 24
    const-string v0, "\\r|\\n|\\s+|\\xA0+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->WHITESPACE:Ljava/util/regex/Pattern;

    .line 47
    const/4 v0, 0x4

    sput v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    .line 48
    sget v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    .line 49
    sget v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    .line 50
    sget v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sMatchingColors:[I

    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sInitialized:Z

    .line 158
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;

    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;-><init>([I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->DRAWING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    .line 163
    new-instance v0, Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;

    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/annotations/AnnotationUtils$ArrayMatchingPalette;-><init>([I)V

    sput-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->SERVING_COLORS:Lcom/google/android/apps/books/annotations/AnnotationUtils$Palette;

    return-void
.end method

.method static synthetic access$000(I)I
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 11
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->matchColor(I)I

    move-result v0

    return v0
.end method

.method public static adjustColor(IF)I
    .locals 7
    .param p0, "color"    # I
    .param p1, "adjustFraction"    # F

    .prologue
    const/16 v6, 0xff

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/high16 v5, 0x437f0000    # 255.0f

    .line 184
    cmpg-float v2, p1, v4

    if-gez v2, :cond_1

    .line 185
    invoke-static {v3, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 186
    .local v0, "darkFraction":F
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v6, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    .line 197
    .end local v0    # "darkFraction":F
    .end local p0    # "color":I
    :cond_0
    :goto_0
    return p0

    .line 190
    .restart local p0    # "color":I
    :cond_1
    cmpl-float v2, p1, v4

    if-lez v2, :cond_0

    .line 191
    const/high16 v2, 0x40000000    # 2.0f

    sub-float/2addr v2, p1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 192
    .local v1, "lightFraction":F
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    rsub-int v2, v2, 0xff

    int-to-float v2, v2

    mul-float/2addr v2, v1

    sub-float v2, v5, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    rsub-int v3, v3, 0xff

    int-to-float v3, v3

    mul-float/2addr v3, v1

    sub-float v3, v5, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    rsub-int v4, v4, 0xff

    int-to-float v4, v4

    mul-float/2addr v4, v1

    sub-float v4, v5, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v6, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    goto :goto_0
.end method

.method private static blendColors(IIF)I
    .locals 7
    .param p0, "color1"    # I
    .param p1, "color2"    # I
    .param p2, "fraction"    # F

    .prologue
    .line 209
    const/high16 v2, 0x43800000    # 256.0f

    mul-float/2addr v2, p2

    float-to-int v0, v2

    .line 210
    .local v0, "frac1":I
    rsub-int v1, v0, 0x100

    .line 211
    .local v1, "frac2":I
    const/16 v2, 0xff

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    mul-int/2addr v3, v0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v4

    mul-int/2addr v4, v1

    add-int/2addr v3, v4

    shr-int/lit8 v3, v3, 0x8

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    mul-int/2addr v4, v0

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v5

    mul-int/2addr v5, v1

    add-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x8

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    mul-int/2addr v5, v0

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    mul-int/2addr v6, v1

    add-int/2addr v5, v6

    shr-int/lit8 v5, v5, 0x8

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    return v2
.end method

.method public static collapseWhitespace(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->WHITESPACE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static colorDistance(II)I
    .locals 3
    .param p0, "color1"    # I
    .param p1, "color2"    # I

    .prologue
    .line 202
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static getDefaultDrawingColor()I
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->lazyInitializeHighlightColors()V

    .line 174
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public static getDrawingColorFromPrefColor(I)I
    .locals 2
    .param p0, "prefColor"    # I

    .prologue
    .line 167
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->lazyInitializeHighlightColors()V

    .line 168
    sget-object v0, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    invoke-static {p0}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->ordinalFromPrefColorIndex(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method public static getMatchingPrefColor(I)I
    .locals 1
    .param p0, "color"    # I

    .prologue
    .line 129
    invoke-static {p0}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->matchColor(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->prefColorIndexFromOrdinal(I)I

    move-result v0

    return v0
.end method

.method private static lazyInitializeHighlightColors()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 54
    sget-boolean v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sInitialized:Z

    if-eqz v1, :cond_0

    .line 74
    .local v0, "k":I
    :goto_0
    return-void

    .line 58
    .end local v0    # "k":I
    :cond_0
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    const/16 v2, -0x1976

    aput v2, v1, v3

    .line 59
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    const v2, -0x450067

    aput v2, v1, v5

    .line 60
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    const/16 v2, -0x4a45

    aput v2, v1, v4

    .line 61
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    const v2, -0x65181e

    aput v2, v1, v6

    .line 64
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    const v2, -0x43fd3

    aput v2, v1, v3

    .line 65
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    const v2, -0x743cb6

    aput v2, v1, v5

    .line 66
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    const v2, -0x8fbd

    aput v2, v1, v4

    .line 67
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    const v2, -0xd93926

    aput v2, v1, v6

    .line 70
    const/4 v0, 0x0

    .restart local v0    # "k":I
    :goto_1
    sget v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    if-ge v0, v1, :cond_1

    .line 71
    sget-object v1, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sMatchingColors:[I

    sget-object v2, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    aget v2, v2, v0

    sget-object v3, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sDrawingColors:[I

    aget v3, v3, v0

    const v4, 0x3f666666    # 0.9f

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->blendColors(IIF)I

    move-result v2

    aput v2, v1, v0

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 73
    :cond_1
    sput-boolean v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sInitialized:Z

    goto :goto_0
.end method

.method private static matchColor(I)I
    .locals 8
    .param p0, "color"    # I

    .prologue
    .line 92
    invoke-static {}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->lazyInitializeHighlightColors()V

    .line 93
    sget-object v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->TAG:Ljava/lang/String;

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    .line 96
    .local v2, "debug":Z
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_0
    sget v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    if-ge v4, v5, :cond_2

    .line 97
    sget-object v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    aget v5, v5, v4

    if-ne p0, v5, :cond_1

    .line 98
    if-eqz v2, :cond_0

    .line 99
    sget-object v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "known color: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ordinal: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    .end local v4    # "k":I
    :cond_0
    :goto_1
    return v4

    .line 96
    .restart local v4    # "k":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 106
    :cond_2
    const v0, 0x7fffffff

    .line 107
    .local v0, "bestDiff":I
    const/4 v1, 0x0

    .line 108
    .local v1, "bestIndex":I
    const/4 v4, 0x0

    :goto_2
    sget v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->NUM_COLORS:I

    if-ge v4, v5, :cond_5

    if-lez v0, :cond_5

    .line 109
    sget-object v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sMatchingColors:[I

    aget v5, v5, v4

    invoke-static {p0, v5}, Lcom/google/android/apps/books/annotations/AnnotationUtils;->colorDistance(II)I

    move-result v3

    .line 110
    .local v3, "diff":I
    if-eqz v2, :cond_3

    .line 111
    sget-object v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sMatchingColors:[I

    aget v7, v7, v4

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " diff: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_3
    if-lt v0, v3, :cond_4

    .line 115
    move v0, v3

    .line 116
    move v1, v4

    .line 108
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 119
    .end local v3    # "diff":I
    :cond_5
    if-eqz v2, :cond_6

    .line 120
    sget-object v5, Lcom/google/android/apps/books/annotations/AnnotationUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "matched color: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ordinal: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bestDiff: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " closest: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/books/annotations/AnnotationUtils;->sServingColors:[I

    aget v7, v7, v1

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v4, v1

    .line 124
    goto/16 :goto_1
.end method

.method private static ordinalFromPrefColorIndex(I)I
    .locals 1
    .param p0, "prefColor"    # I

    .prologue
    .line 83
    add-int/lit8 v0, p0, -0x1

    return v0
.end method

.method private static prefColorIndexFromOrdinal(I)I
    .locals 1
    .param p0, "ordinal"    # I

    .prologue
    .line 78
    add-int/lit8 v0, p0, 0x1

    return v0
.end method
