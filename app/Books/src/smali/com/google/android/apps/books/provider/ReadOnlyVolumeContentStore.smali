.class public interface abstract Lcom/google/android/apps/books/provider/ReadOnlyVolumeContentStore;
.super Ljava/lang/Object;
.source "ReadOnlyVolumeContentStore.java"


# virtual methods
.method public abstract getPageContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.end method

.method public abstract getPageStructureContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.end method

.method public abstract getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.end method

.method public abstract getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.end method

.method public abstract getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.end method

.method public abstract getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ReadOnlyVolumeContentFile;
.end method
