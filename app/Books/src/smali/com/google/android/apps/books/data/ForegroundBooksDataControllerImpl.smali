.class public Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ForegroundBooksDataController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;
    }
.end annotation


# instance fields
.field private final mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mBackgroundExecutor:Ljava/util/concurrent/Executor;

.field private final mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

.field private final mClock:Lcom/google/android/apps/books/model/Clock;

.field private final mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

.field private final mForegroundExecutor:Ljava/util/concurrent/Executor;

.field private final mListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentSearches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRecs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;"
        }
    .end annotation
.end field

.field private final mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

.field private final mUploadsController:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field private mVolumeIdForRecentSearches:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/upload/UploadDataStorage;Lcom/google/android/apps/books/model/Clock;Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/upload/UploadManager;)V
    .locals 7
    .param p1, "foregroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "backgroundExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "uploadDataStorage"    # Lcom/google/android/apps/books/upload/UploadDataStorage;
    .param p4, "clock"    # Lcom/google/android/apps/books/model/Clock;
    .param p5, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p6, "backgroundDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p7, "uploadManager"    # Lcom/google/android/apps/books/upload/UploadManager;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mListeners:Ljava/util/Set;

    .line 75
    new-instance v0, Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-direct {v0}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    .line 92
    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    .line 93
    iput-object p3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

    .line 94
    iput-object p4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    .line 95
    const-string v0, "missing data store"

    invoke-static {p5, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/BooksDataStore;

    iput-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    .line 96
    const-string v0, "missing background data controller"

    invoke-static {p6, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/data/BooksDataController;

    iput-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 98
    new-instance v0, Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;

    iget-object v5, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    move-object v1, p0

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/UploadsControllerImpl;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/books/upload/UploadDataStorage;Lcom/google/android/apps/books/model/Clock;Lcom/google/android/apps/books/upload/UploadManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mUploadsController:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/LocalVolumeDataCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->maybeExpireDismissedRecommendations()V

    return-void
.end method

.method static synthetic access$502(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    return-object p1
.end method

.method private copyListeners()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mListeners:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private static getDefaultLocalVolumeData()Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->DEFAULT:Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    return-object v0
.end method

.method private getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 179
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v1, v1, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 180
    .local v0, "existing":Lcom/google/android/apps/books/model/LocalVolumeData;
    if-eqz v0, :cond_0

    .line 183
    .end local v0    # "existing":Lcom/google/android/apps/books/model/LocalVolumeData;
    :goto_0
    return-object v0

    .restart local v0    # "existing":Lcom/google/android/apps/books/model/LocalVolumeData;
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getDefaultLocalVolumeData()Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    goto :goto_0
.end method

.method private maybeExpireDismissedRecommendations()V
    .locals 6

    .prologue
    .line 390
    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v4, v4, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    const/16 v5, 0x14

    if-le v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecs:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 392
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 393
    .local v1, "recommendations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecs:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;

    .line 394
    .local v2, "recommendedBook":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    iget-object v4, v2, Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;->volumeId:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 398
    .end local v2    # "recommendedBook":Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v4, v4, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-static {v4}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v3

    .line 399
    .local v3, "staleDismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 400
    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v4, v4, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 402
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "recommendations":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v3    # "staleDismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->saveDismissedRecommendations()V

    .line 406
    return-void
.end method

.method private publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "data"    # Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    .prologue
    .line 310
    invoke-static {p1, p2}, Lcom/google/common/collect/ImmutableMap;->of(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableMap;

    move-result-object v2

    .line 311
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 312
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onLocalVolumeData(Ljava/util/Map;)V

    goto :goto_0

    .line 314
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method private saveDismissedRecommendations()V
    .locals 3

    .prologue
    .line 409
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v1, v1, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-static {v1}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    .line 410
    .local v0, "dismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/Set;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 422
    return-void
.end method


# virtual methods
.method public acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 455
    .local p2, "volumeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "onResult":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/data/BooksDataController;->acceptOffer(Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    .line 456
    return-void
.end method

.method public addDismissedRecommendation(Ljava/lang/String;)V
    .locals 3
    .param p1, "dismissedRec"    # Ljava/lang/String;

    .prologue
    .line 372
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 373
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->saveDismissedRecommendations()V

    .line 375
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 376
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onDismissedRecommendations(Ljava/util/Set;)V

    goto :goto_0

    .line 378
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method public dismissOffer(Ljava/lang/String;)V
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/BooksDataController;->dismissOffer(Ljava/lang/String;)V

    .line 450
    return-void
.end method

.method public finishedOpeningBook(Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/BooksDataController;->finishedOpeningBook(Ljava/lang/Object;)V

    .line 529
    return-void
.end method

.method public getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 3
    .param p1, "fromServer"    # Z
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 535
    .local p2, "myEbooksConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/MyEbooksVolumesResults;>;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {p2}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-static {p3}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2, p4}, Lcom/google/android/apps/books/data/BooksDataController;->getMyEbooks(ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 537
    return-void
.end method

.method public getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p1, "localOnly"    # Z
    .param p2, "reloadLibrary"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 442
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p3, v1}, Lcom/google/android/ublib/utils/Consumers;->deliverOnThreadOrNull(Lcom/google/android/ublib/utils/Consumer;Ljava/util/concurrent/Executor;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mForegroundExecutor:Ljava/util/concurrent/Executor;

    invoke-static {p4, v2}, Lcom/google/android/ublib/utils/Consumers;->deliverOnThreadOrNull(Lcom/google/android/ublib/utils/Consumer;Ljava/util/concurrent/Executor;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 445
    return-void
.end method

.method public getPageContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/CcBox;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 487
    .local p3, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "ccBoxConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/CcBox;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/data/BooksDataController;->getPageContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 489
    return-void
.end method

.method public getPageStructure(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Page;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 503
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/ocean/frontend/javascript/proto/PageInfo$JsonPage;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/data/BooksDataController;->getPageStructure(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 505
    return-void
.end method

.method public getRequestedDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 667
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {p1}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->getRequestedDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V

    .line 669
    return-void
.end method

.method public getResourceContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resource"    # Lcom/google/android/apps/books/model/Resource;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p7, "onlyIfLocal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 472
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getResourceContent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .param p7, "onlyIfLocal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 496
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p4, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSegmentContent(Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;ZLcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p6, "ignoreResources"    # Z
    .param p7, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Segment;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;>;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;Z",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 464
    .local p3, "contentConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/lang/String;>;>;"
    .local p4, "resourcesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getServerDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 673
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Ljava/util/List<Lcom/google/android/apps/books/dictionary/DictionaryMetadata;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {p1}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->getServerDictionaryMetadataList(Lcom/google/android/ublib/utils/Consumer;)V

    .line 675
    return-void
.end method

.method public getSharedResourceContent(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1
    .param p1, "resId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/BooksDataController;->getSharedResourceContent(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getUploadsController()Lcom/google/android/apps/books/data/UploadsController;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mUploadsController:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    return-object v0
.end method

.method public getUploadsControllerImpl()Lcom/google/android/apps/books/data/UploadsControllerImpl;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mUploadsController:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    return-object v0
.end method

.method public getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "licenseType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 546
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVolumeCover(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 511
    .local p2, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVolumeData(Ljava/lang/String;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fromServer"    # Z
    .param p5, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 428
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/model/VolumeData;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVolumeManifest(Ljava/lang/String;Ljava/util/Set;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p3, "fromServer"    # Z
    .param p6, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 435
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$ManifestResponse;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getVolumeSearches(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 631
    .local p2, "searchesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Ljava/lang/String;>;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mVolumeIdForRecentSearches:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 633
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mVolumeIdForRecentSearches:Ljava/lang/String;

    .line 634
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    .line 636
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    if-eqz v1, :cond_2

    .line 637
    if-eqz p2, :cond_1

    .line 638
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/books/util/ExceptionOr;->makeSuccess(Ljava/lang/Object;)Lcom/google/android/apps/books/util/ExceptionOr;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 656
    :cond_1
    :goto_0
    return-void

    .line 641
    :cond_2
    new-instance v0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$8;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Lcom/google/android/ublib/utils/Consumer;)V

    .line 653
    .local v0, "cachedSearchesConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Ljava/lang/String;>;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {v0}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeSearches(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0
.end method

.method public getVolumeThumbnail(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V
    .locals 2
    .param p1, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;
    .param p4, "priority"    # Lcom/google/android/apps/books/data/BooksDataController$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeData;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/InputStreamSource;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;",
            "Lcom/google/android/apps/books/data/BooksDataController$Priority;",
            ")V"
        }
    .end annotation

    .prologue
    .line 518
    .local p2, "imageConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/InputStreamSource;>;>;"
    .local p3, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public loadDismissedRecommendations()V
    .locals 4

    .prologue
    .line 340
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 342
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->dismissedRecs:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onDismissedRecommendations(Ljava/util/Set;)V

    goto :goto_0

    .line 345
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$6;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 368
    :cond_1
    return-void
.end method

.method public loadDownloadProgress(Ljava/lang/String;)V
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$5;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 336
    return-void
.end method

.method public loadedLocalVolumeData(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;+Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v3, v3, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-static {v3, p1}, Lcom/google/android/apps/books/util/MapUtils;->addValues(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 165
    .local v2, "valuesToPublish":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    const-string v3, "FgDataController"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 166
    const-string v3, "FgDataController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "adding localvolumedata: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_0
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 169
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 170
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onLocalVolumeData(Ljava/util/Map;)V

    goto :goto_0

    .line 173
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_1
    return-void
.end method

.method public loadedVolumeDownloadProgress(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v3, v3, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->volumeDownloadProgress:Ljava/util/Map;

    invoke-static {v3, p1}, Lcom/google/android/apps/books/util/MapUtils;->addOrReplaceValues(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 154
    .local v2, "valuesToPublish":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 155
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 156
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onVolumeDownloadProgress(Ljava/util/Map;)V

    goto :goto_0

    .line 159
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method public lookup(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "term"    # Ljava/lang/String;
    .param p2, "languageId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/annotations/DictionaryEntry;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 661
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/annotations/DictionaryEntry;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {p3}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/books/data/BooksDataController;->lookup(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 663
    return-void
.end method

.method protected notifyListeners(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;)V
    .locals 3
    .param p1, "listenerNotifier"    # Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->copyListeners()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 131
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {p1, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$ListenerNotifier;->take(Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method

.method public setFitWidth(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fitWidth"    # Z

    .prologue
    .line 586
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 587
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withFitWidth(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 589
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 590
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 591
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->setFitWidth(Ljava/lang/String;Z)V

    .line 592
    return-void
.end method

.method public setForceDownload(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "forceDownload"    # Z

    .prologue
    .line 244
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 245
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getForceDownload()Z

    move-result v2

    if-ne v2, p2, :cond_0

    .line 260
    :goto_0
    return-void

    .line 248
    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withForceDownload(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 250
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 254
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$3;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;Z)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setHasOfflineLicense(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "hasOfflineLicense"    # Z

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 292
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v2

    if-ne v2, p2, :cond_0

    .line 307
    :goto_0
    return-void

    .line 295
    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withOfflineLicense(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 297
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 301
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$4;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$4;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;Z)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setLastVolumeSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lastSearch"    # Ljava/lang/String;

    .prologue
    .line 608
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->setLastVolumeSearch(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 611
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    invoke-static {v2, p2}, Lcom/google/android/apps/books/model/RecentSearchesUtil;->setLastVolumeSearch(Ljava/util/List;Ljava/lang/String;)V

    .line 612
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 613
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecentSearches:Ljava/util/List;

    invoke-interface {v1, p1, v2}, Lcom/google/android/apps/books/model/BooksDataListener;->onRecentSearchesChanged(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 616
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method public setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .prologue
    .line 264
    const-string v4, "missing license action"

    invoke-static {p2, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v3

    .line 266
    .local v3, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v3}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v2

    .line 274
    .local v2, "oldAction":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    invoke-static {p2, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 287
    :goto_0
    return-void

    .line 277
    :cond_0
    invoke-static {v3, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withLicenseAction(Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 279
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    sget-object v4, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    if-ne p2, v4, :cond_1

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withOfflineLicense(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v1

    .line 282
    .local v1, "newerData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v4, v4, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v4, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 286
    iget-object v4, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v4, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V

    goto :goto_0

    .end local v1    # "newerData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    :cond_1
    move-object v1, v0

    .line 279
    goto :goto_1
.end method

.method public setLineHeight(Ljava/lang/String;F)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lineHeight"    # F

    .prologue
    .line 566
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 567
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withLineHeight(Lcom/google/android/apps/books/model/LocalVolumeData;F)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 569
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 571
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->setLineHeight(Ljava/lang/String;F)V

    .line 572
    return-void
.end method

.method public setPinned(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 189
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    if-ne v2, p2, :cond_0

    .line 198
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withPinned(Lcom/google/android/apps/books/model/LocalVolumeData;Z)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 194
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->setPinned(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z
    .param p3, "hasOfflineLicense"    # Z

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 224
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getPinned()Z

    move-result v2

    if-ne v2, p2, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->hasOfflineLicense()Z

    move-result v2

    if-ne v2, p2, :cond_0

    .line 240
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withPinnedAndOfflineLicense(Lcom/google/android/apps/books/model/LocalVolumeData;ZZ)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 230
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 234
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$2;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;ZZ)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "lastAccess"    # J
    .param p5, "lastAction"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .param p6, "notifyContentChanged"    # Z

    .prologue
    .line 555
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v8

    .line 556
    .local v8, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-static {v8, p3, p4}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withLastLocalAccess(Lcom/google/android/apps/books/model/LocalVolumeData;J)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 558
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v1, v1, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 560
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-interface/range {v1 .. v7}, Lcom/google/android/apps/books/data/BooksDataController;->setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    .line 562
    return-void
.end method

.method public setRecommendations(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 382
    .local p1, "newRecs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mRecs:Ljava/util/List;

    .line 383
    invoke-direct {p0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->maybeExpireDismissedRecommendations()V

    .line 384
    return-void
.end method

.method public setRequestedDictionaryLanguages(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 679
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/data/BooksDataController;->setRequestedDictionaryLanguages(Ljava/util/List;)V

    .line 680
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 681
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v1, p1}, Lcom/google/android/apps/books/model/BooksDataListener;->onRequestedDictionaryLanguages(Ljava/util/List;)V

    goto :goto_0

    .line 683
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method public setTextZoom(Ljava/lang/String;F)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "textZoom"    # F

    .prologue
    .line 576
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 577
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withTextZoom(Lcom/google/android/apps/books/model/LocalVolumeData;F)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 579
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 580
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 581
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->setTextZoom(Ljava/lang/String;F)V

    .line 582
    return-void
.end method

.method public setUserSelectedMode(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 203
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLastMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v2

    if-ne v2, p2, :cond_0

    .line 218
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-static {v1, p2}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withLastMode(Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 208
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$1;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$1;-><init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public startedOpeningBook(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/data/BooksDataController;->startedOpeningBook(Ljava/lang/Object;Ljava/lang/String;)V

    .line 524
    return-void
.end method

.method public syncDictionaryMetadata(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 687
    .local p1, "finishConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {p1}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->syncDictionaryMetadata(Lcom/google/android/ublib/utils/Consumer;)V

    .line 689
    return-void
.end method

.method public syncRequestedDictionaries(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 693
    .local p1, "finishConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-static {p1}, Lcom/google/android/ublib/utils/Consumers;->deliverOnUiThreadOrNull(Lcom/google/android/ublib/utils/Consumer;)Lcom/google/android/ublib/utils/Consumer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->syncRequestedDictionaries(Lcom/google/android/ublib/utils/Consumer;)V

    .line 695
    return-void
.end method

.method public updateLastLocalAccess(Ljava/lang/String;JZ)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lastLocalAccess"    # J
    .param p4, "notifyContentChanged"    # Z

    .prologue
    .line 597
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v1

    .line 598
    .local v1, "oldData":Lcom/google/android/apps/books/model/LocalVolumeData;
    invoke-static {v1, p2, p3}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->withLastLocalAccess(Lcom/google/android/apps/books/model/LocalVolumeData;J)Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-result-object v0

    .line 600
    .local v0, "newData":Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    iget-object v2, v2, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->localVolumeData:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->publishLocalVolumeData(Ljava/lang/String;Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;)V

    .line 602
    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mBackgroundDataController:Lcom/google/android/apps/books/data/BooksDataController;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/google/android/apps/books/data/BooksDataController;->updateLastLocalAccess(Ljava/lang/String;JZ)V

    .line 604
    return-void
.end method

.method public uploadCollectionChanges(JLcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 540
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public weaklyAddListener(Lcom/google/android/apps/books/model/BooksDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/apps/books/model/BooksDataListener;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mUploadsController:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->onNewListener(Lcom/google/android/apps/books/model/BooksDataListener;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mCache:Lcom/google/android/apps/books/model/LocalVolumeDataCache;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/LocalVolumeDataCache;->fullListenerUpdate(Lcom/google/android/apps/books/model/BooksDataListener;)V

    .line 109
    return-void
.end method
