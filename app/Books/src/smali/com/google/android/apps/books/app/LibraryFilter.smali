.class public abstract enum Lcom/google/android/apps/books/app/LibraryFilter;
.super Ljava/lang/Enum;
.source "LibraryFilter.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/LibraryFilter$LibraryFilterSource;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/LibraryFilter;",
        ">;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/books/widget/CardData;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/LibraryFilter;

.field public static final enum ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

.field public static final enum FREE_AND_PURCHASED:Lcom/google/android/apps/books/app/LibraryFilter;

.field public static final enum RENTALS:Lcom/google/android/apps/books/app/LibraryFilter;

.field public static final enum SAMPLES:Lcom/google/android/apps/books/app/LibraryFilter;

.field public static final enum UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;


# instance fields
.field private mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

.field private final mResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Lcom/google/android/apps/books/app/LibraryFilter$1;

    const-string v1, "ALL_BOOKS"

    const v2, 0x7f0f0193

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/books/app/LibraryFilter$1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 36
    new-instance v0, Lcom/google/android/apps/books/app/LibraryFilter$2;

    const-string v1, "UPLOADED"

    const v2, 0x7f0f0194

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/books/app/LibraryFilter$2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 74
    new-instance v0, Lcom/google/android/apps/books/app/LibraryFilter$3;

    const-string v1, "FREE_AND_PURCHASED"

    const v2, 0x7f0f0195

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/books/app/LibraryFilter$3;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->FREE_AND_PURCHASED:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 81
    new-instance v0, Lcom/google/android/apps/books/app/LibraryFilter$4;

    const-string v1, "RENTALS"

    const v2, 0x7f0f0197

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/books/app/LibraryFilter$4;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->RENTALS:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 93
    new-instance v0, Lcom/google/android/apps/books/app/LibraryFilter$5;

    const-string v1, "SAMPLES"

    const v2, 0x7f0f0196

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/books/app/LibraryFilter$5;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->SAMPLES:Lcom/google/android/apps/books/app/LibraryFilter;

    .line 24
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/books/app/LibraryFilter;

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->ALL_BOOKS:Lcom/google/android/apps/books/app/LibraryFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->UPLOADED:Lcom/google/android/apps/books/app/LibraryFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->FREE_AND_PURCHASED:Lcom/google/android/apps/books/app/LibraryFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->RENTALS:Lcom/google/android/apps/books/app/LibraryFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/LibraryFilter;->SAMPLES:Lcom/google/android/apps/books/app/LibraryFilter;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->$VALUES:[Lcom/google/android/apps/books/app/LibraryFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "resId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 155
    iput p3, p0, Lcom/google/android/apps/books/app/LibraryFilter;->mResId:I

    .line 156
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILcom/google/android/apps/books/app/LibraryFilter$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # I
    .param p4, "x3"    # Lcom/google/android/apps/books/app/LibraryFilter$1;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/app/LibraryFilter;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/LibraryFilter;Ljava/util/List;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/LibraryFilter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/LibraryFilter;->isNonEmpty(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method private isNonEmpty(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/CardData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 202
    .local p1, "cardsData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/CardData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/CardData;

    .line 203
    .local v0, "cardData":Lcom/google/android/apps/books/widget/CardData;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/LibraryFilter;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    const/4 v2, 0x1

    .line 207
    .end local v0    # "cardData":Lcom/google/android/apps/books/widget/CardData;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/LibraryFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/LibraryFilter;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/LibraryFilter;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/books/app/LibraryFilter;->$VALUES:[Lcom/google/android/apps/books/app/LibraryFilter;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/LibraryFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/LibraryFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/apps/books/app/LibraryFilter;->mResId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public passesCurrentDownloadFilter(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/books/app/LibraryFilter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/LibraryFilter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v0}, Lcom/google/android/apps/books/widget/BooksHomeController;->getDownloadedOnlyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/LibraryFilter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/widget/BooksHomeController;->isFullyDownloaded(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBooksHomeController(Lcom/google/android/apps/books/widget/BooksHomeController;)V
    .locals 0
    .param p1, "homeController"    # Lcom/google/android/apps/books/widget/BooksHomeController;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/apps/books/app/LibraryFilter;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;

    .line 169
    return-void
.end method

.method public shouldShowWhenEmpty(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method
