.class public Lcom/google/android/apps/books/common/Position;
.super Ljava/lang/Object;
.source "Position.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/common/Position$PageOrdering;
    }
.end annotation


# static fields
.field private static final NO_INT_TOKENS:[I


# instance fields
.field private mCachedPageId:Ljava/lang/String;

.field private mCachedPageIndex:I

.field private final mPosition:Ljava/lang/String;

.field private mPostPageIdTokens:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/books/common/Position;->NO_INT_TOKENS:[I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "position"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageId:Ljava/lang/String;

    .line 80
    iput-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPostPageIdTokens:[I

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageIndex:I

    .line 89
    invoke-static {p1}, Lcom/google/android/apps/books/common/Position;->isNormalized(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iput-object p1, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    .line 98
    :goto_0
    return-void

    .line 93
    :cond_0
    const-string v0, "Position"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    const-string v0, "Position"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating position from unnormalized string: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/books/common/Position;->addNormalisationPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    goto :goto_0
.end method

.method private static addNormalisationPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "position"    # Ljava/lang/String;

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GBS."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;)Ljava/util/Comparator;
    .locals 1
    .param p0, "ordering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/common/Position$PageOrdering;",
            ")",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/books/common/Position;->comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;Z)Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public static comparator(Lcom/google/android/apps/books/common/Position$PageOrdering;Z)Ljava/util/Comparator;
    .locals 1
    .param p0, "pageMap"    # Lcom/google/android/apps/books/common/Position$PageOrdering;
    .param p1, "safeFallback"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/common/Position$PageOrdering;",
            "Z)",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/apps/books/common/Position$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/common/Position$1;-><init>(Lcom/google/android/apps/books/common/Position$PageOrdering;)V

    return-object v0
.end method

.method public static compare(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position$PageOrdering;)I
    .locals 5
    .param p0, "lhs"    # Lcom/google/android/apps/books/common/Position;
    .param p1, "rhs"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "pageOrdering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;

    .prologue
    .line 194
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/common/Position;->getPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v0

    .line 195
    .local v0, "lPage":I
    invoke-direct {p1, p2}, Lcom/google/android/apps/books/common/Position;->getPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v2

    .line 196
    .local v2, "rPage":I
    sub-int v1, v0, v2

    .line 197
    .local v1, "pageDiff":I
    if-eqz v1, :cond_0

    .line 200
    .end local v1    # "pageDiff":I
    :goto_0
    return v1

    .restart local v1    # "pageDiff":I
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/common/Position;->intTokensAfterPageId()[I

    move-result-object v3

    invoke-direct {p1}, Lcom/google/android/apps/books/common/Position;->intTokensAfterPageId()[I

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/books/common/Position;->compareIntTokens([I[I)I

    move-result v1

    goto :goto_0
.end method

.method private static compareIntTokens([I[I)I
    .locals 4
    .param p0, "ltokens"    # [I
    .param p1, "rtokens"    # [I

    .prologue
    .line 204
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    .line 205
    array-length v2, p1

    if-lt v1, v2, :cond_1

    .line 206
    const/4 v0, 0x1

    .line 216
    :cond_0
    :goto_1
    return v0

    .line 208
    :cond_1
    aget v2, p0, v1

    aget v3, p1, v1

    sub-int v0, v2, v3

    .line 209
    .local v0, "diff":I
    if-nez v0, :cond_0

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 213
    .end local v0    # "diff":I
    :cond_2
    array-length v2, p1

    array-length v3, p0

    if-le v2, v3, :cond_3

    .line 214
    const/4 v0, -0x1

    goto :goto_1

    .line 216
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private computePostPageIdTokens()[I
    .locals 5

    .prologue
    .line 175
    iget-object v3, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    const-string v4, "[.]"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 176
    .local v2, "stringTokens":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x3

    if-gt v3, v4, :cond_1

    .line 177
    sget-object v1, Lcom/google/android/apps/books/common/Position;->NO_INT_TOKENS:[I

    .line 184
    :cond_0
    return-object v1

    .line 180
    :cond_1
    array-length v3, v2

    add-int/lit8 v3, v3, -0x3

    new-array v1, v3, [I

    .line 181
    .local v1, "intTokens":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 182
    add-int/lit8 v3, v0, 0x3

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v0

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static createPositionOrNull(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;
    .locals 1
    .param p0, "position"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-static {p0}, Lcom/google/android/apps/books/util/BooksTextUtils;->isNullOrWhitespace(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    .line 116
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static extractPageId(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "position"    # Ljava/lang/String;

    .prologue
    .line 130
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing/empty position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkIsGraphic(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 131
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, "."

    invoke-direct {v1, p0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .local v1, "tokenizer":Ljava/util/StringTokenizer;
    :cond_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v2

    if-lez v2, :cond_1

    .line 133
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "token":Ljava/lang/String;
    const-string v2, "GBS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    .end local v0    # "token":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private getPageIndex(Lcom/google/android/apps/books/common/Position$PageOrdering;)I
    .locals 2
    .param p1, "ordering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;

    .prologue
    .line 252
    iget v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 253
    invoke-static {p0, p1}, Lcom/google/android/apps/books/common/Position;->getPageIndex(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position$PageOrdering;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageIndex:I

    .line 255
    :cond_0
    iget v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageIndex:I

    return v0
.end method

.method private static getPageIndex(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/common/Position$PageOrdering;)I
    .locals 4
    .param p0, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p1, "ordering"    # Lcom/google/android/apps/books/common/Position$PageOrdering;

    .prologue
    .line 242
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/android/apps/books/common/Position$PageOrdering;->getPageIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 247
    :goto_0
    return v1

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, "Position"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    const-string v1, "Position"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t find position in metadata: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private intTokensAfterPageId()[I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPostPageIdTokens:[I

    if-nez v0, :cond_0

    .line 169
    invoke-direct {p0}, Lcom/google/android/apps/books/common/Position;->computePostPageIdTokens()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPostPageIdTokens:[I

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPostPageIdTokens:[I

    return-object v0
.end method

.method private static isNormalized(Ljava/lang/String;)Z
    .locals 1
    .param p0, "position"    # Ljava/lang/String;

    .prologue
    .line 71
    const-string v0, "GBS."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "position"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/google/android/apps/books/common/Position;->isNormalized(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .end local p0    # "position":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "position":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/books/common/Position;->addNormalisationPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;
    .locals 2
    .param p0, "pageId"    # Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-static {p0}, Lcom/google/android/apps/books/common/Position;->addNormalisationPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 155
    instance-of v0, p1, Lcom/google/android/apps/books/common/Position;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    check-cast p1, Lcom/google/android/apps/books/common/Position;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 158
    :goto_0
    return v0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPageId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/books/common/Position;->extractPageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageId:Ljava/lang/String;

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mCachedPageId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/books/common/Position;->mPosition:Ljava/lang/String;

    return-object v0
.end method
