.class public Lcom/google/android/apps/books/util/JsPerformanceMetrics;
.super Ljava/lang/Object;
.source "JsPerformanceMetrics.java"


# instance fields
.field private final mFirstPageTime:I

.field private final mFontsTime:I

.field private final mLastPageTime:I

.field private final mTotalPaginationTime:I

.field private final mWasBackgrounded:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "jsonMetrics"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 27
    .local v1, "metricsObject":Lorg/json/JSONObject;
    const-string v2, "firstPage"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFirstPageTime:I

    .line 28
    const-string v2, "lastPage"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mLastPageTime:I

    .line 29
    const-string v2, "fontLoad"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFontsTime:I

    .line 30
    const-string v2, "total"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mTotalPaginationTime:I

    .line 31
    const-string v2, "background"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mWasBackgrounded:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    return-void

    .line 32
    .end local v1    # "metricsObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "problem getting json metrics"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public firstPageTime()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFirstPageTime:I

    return v0
.end method

.method public fontsTime()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFontsTime:I

    return v0
.end method

.method public hasFonts()Z
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFontsTime:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mTotalPaginationTime:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastPageTime()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mLastPageTime:I

    return v0
.end method

.method public overHeadTime()I
    .locals 3

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mTotalPaginationTime:I

    iget v1, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mLastPageTime:I

    iget v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFontsTime:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 94
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "firstPageTime"

    iget v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFirstPageTime:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastPageTime"

    iget v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mLastPageTime:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "fontsTime"

    iget v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mFontsTime:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "overHeadTime"

    invoke-virtual {p0}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->overHeadTime()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "background"

    iget-boolean v2, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mWasBackgrounded:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Z)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public wasBackgrounded()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->mWasBackgrounded:Z

    return v0
.end method
