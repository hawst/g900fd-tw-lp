.class public interface abstract Lcom/google/android/apps/books/api/TranslationServer;
.super Ljava/lang/Object;
.source "TranslationServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;
    }
.end annotation


# virtual methods
.method public abstract discoverLanguages()Lcom/google/common/collect/BiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/BiMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract translateText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/google/android/apps/books/api/TranslationServer$TranslatedTextResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
