.class public Lcom/google/android/apps/books/render/BasicTextReader;
.super Ljava/lang/Object;
.source "BasicTextReader.java"

# interfaces
.implements Lcom/google/android/apps/books/render/TextReader;


# instance fields
.field private final mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/JavaScriptExecutor;)V
    .locals 0
    .param p1, "engine"    # Lcom/google/android/apps/books/render/JavaScriptExecutor;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    .line 51
    return-void
.end method

.method public static buildRangeJson(Ljava/util/Map;)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p0, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    const/16 v13, 0x2c

    .line 27
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .local v4, "rangeBuilder":Ljava/lang/StringBuilder;
    const-string v7, "["

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    const/4 v5, 0x0

    .line 30
    .local v5, "rangeIndex":I
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 31
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    if-lez v5, :cond_0

    .line 32
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 34
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 35
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/annotations/TextLocationRange;

    .line 36
    .local v3, "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    invoke-virtual {v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 37
    .local v6, "start":Lcom/google/android/apps/books/annotations/TextLocation;
    invoke-virtual {v3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/TextLocation;

    .line 38
    .local v0, "end":Lcom/google/android/apps/books/annotations/TextLocation;
    invoke-static {v13}, Lcom/google/common/base/Joiner;->on(C)Lcom/google/common/base/Joiner;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/books/render/BasicTextReader;->quotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v9, v6, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v9}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/books/render/BasicTextReader;->quotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v6, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, v0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v12}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/google/android/apps/books/render/BasicTextReader;->quotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    iget v12, v0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v7, v9, v10}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 44
    .end local v0    # "end":Lcom/google/android/apps/books/annotations/TextLocation;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    .end local v3    # "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    .end local v6    # "start":Lcom/google/android/apps/books/annotations/TextLocation;
    :cond_1
    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private static quotedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public activateMediaElement(IILjava/lang/String;I)V
    .locals 5
    .param p1, "passageIndex"    # I
    .param p2, "pageOffset"    # I
    .param p3, "elementId"    # Ljava/lang/String;
    .param p4, "taskRequestId"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.activateMediaElement"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public applySettings(Lcom/google/android/apps/books/render/ReaderSettings;)V
    .locals 5
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.applyPreferences"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->fontFamily:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/ReaderSettings;->getTextZoom()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/google/android/apps/books/render/ReaderSettings;->getLineHeight()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p1, Lcom/google/android/apps/books/render/ReaderSettings;->textAlign:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public initializeJavascript(Lcom/google/android/apps/books/util/JsConfiguration;)V
    .locals 5
    .param p1, "configData"    # Lcom/google/android/apps/books/util/JsConfiguration;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.initializeReader"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/JsConfiguration;->toJson()Lorg/json/JSONObject;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public loadNearbyText(IIFFI)V
    .locals 5
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "radius"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.loadNearbyText"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public loadNotNormalizedRangeData(IIIII)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "pageIndex"    # I
    .param p4, "start"    # I
    .param p5, "end"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.loadNotNormalizedRangeData"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public loadPage(IIII)V
    .locals 5
    .param p1, "chapter"    # I
    .param p2, "page"    # I
    .param p3, "offset"    # I
    .param p4, "requestId"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.loadPage"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public loadPosition(ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;I)V
    .locals 5
    .param p1, "chapter"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offset"    # Ljava/lang/Integer;
    .param p4, "fallbackChapterIndex"    # Ljava/lang/Integer;
    .param p5, "requestId"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.loadPosition"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p3, v2, v3

    const/4 v3, 0x4

    aput-object p4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public loadRangeData(IILcom/google/android/apps/books/annotations/TextLocationRange;ZIII)V
    .locals 9
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "range"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p4, "needSelectionData"    # Z
    .param p5, "handle"    # I
    .param p6, "deltaX"    # I
    .param p7, "deltaY"    # I

    .prologue
    .line 117
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v4, v4, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v4}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v3

    .line 118
    .local v3, "startPosition":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getStart()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/TextLocation;

    iget v2, v4, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    .line 119
    .local v2, "startOffset":I
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/TextLocation;

    iget-object v4, v4, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v4}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "endPosition":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/google/android/apps/books/annotations/TextLocationRange;->getEnd()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/annotations/TextLocation;

    iget v0, v4, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    .line 121
    .local v0, "endOffset":I
    iget-object v4, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v5, "engine.loadRangeData"

    const/16 v6, 0xa

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    aput-object v3, v6, v7

    const/4 v7, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    aput-object v1, v6, v7

    const/4 v7, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x7

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x8

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/16 v7, 0x9

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public loadRangeDataBulk(IILjava/util/Map;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 140
    .local p3, "ranges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    invoke-static {p3}, Lcom/google/android/apps/books/render/BasicTextReader;->buildRangeJson(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "rangeParam":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v2, "engine.loadRangeDataBulk"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public loadSpreadPage(ILjava/lang/String;III)V
    .locals 5
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "spreadOffset"    # I
    .param p4, "spreadPageIndex"    # I
    .param p5, "requestId"    # I

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.loadSpreadPage"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public loadSpreadPageFromEob(III)V
    .locals 5
    .param p1, "spreadOffset"    # I
    .param p2, "spreadPageIndex"    # I
    .param p3, "requestId"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.loadSpreadPageFromEob"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public maybePurgePassages(Ljava/util/Collection;I)V
    .locals 6
    .param p2, "taskRequestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 153
    .local v0, "passagesJson":Lorg/json/JSONArray;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v2, "engine.maybePurgePassages"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public preloadPassage(II)V
    .locals 5
    .param p1, "passageIndex"    # I
    .param p2, "requestId"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.preloadPassage"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;I)V
    .locals 8
    .param p1, "request"    # Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    .param p2, "requestId"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 91
    instance-of v1, p1, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 92
    check-cast v0, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;

    .line 93
    .local v0, "moRequest":Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v2, "engine.requestMoElements"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->getPassage()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    new-instance v4, Lorg/json/JSONArray;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;->getElementIds()Ljava/util/List;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 99
    .end local v0    # "moRequest":Lcom/google/android/apps/books/app/mo/MediaOverlaysController$MoReadableItemsRequestData;
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v2, "engine.requestPassageText"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p1}, Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;->getPassage()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setOverlayActiveClass(Ljava/lang/String;)V
    .locals 4
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicTextReader;->mEngine:Lcom/google/android/apps/books/render/JavaScriptExecutor;

    const-string v1, "engine.setOverlayActiveClass"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/books/util/JSUtils;->buildMethodInvoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/render/JavaScriptExecutor;->run(Ljava/lang/String;)V

    .line 82
    return-void
.end method
