.class public Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TappedAnnotationCandidate"
.end annotation


# instance fields
.field public final annotationId:Ljava/lang/String;

.field public final isMarginIcon:Z

.field public final mainDistance:I

.field public final secondaryDistance:I


# direct methods
.method private constructor <init>(IILjava/lang/String;Z)V
    .locals 0
    .param p1, "mainDistance"    # I
    .param p2, "secondaryDistance"    # I
    .param p3, "annotationId"    # Ljava/lang/String;
    .param p4, "isMarginIcon"    # Z

    .prologue
    .line 2042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043
    iput p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->mainDistance:I

    .line 2044
    iput p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->secondaryDistance:I

    .line 2045
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->annotationId:Ljava/lang/String;

    .line 2046
    iput-boolean p4, p0, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;->isMarginIcon:Z

    .line 2047
    return-void
.end method

.method synthetic constructor <init>(IILjava/lang/String;ZLcom/google/android/apps/books/widget/PagesViewController$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Z
    .param p5, "x4"    # Lcom/google/android/apps/books/widget/PagesViewController$1;

    .prologue
    .line 2034
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/widget/PagesViewController$TappedAnnotationCandidate;-><init>(IILjava/lang/String;Z)V

    return-void
.end method
