.class Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;
.super Landroid/os/AsyncTask;
.source "TokenAuthAuthenticationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->getUberToken(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mException:Ljava/io/IOException;

.field final synthetic this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

.field final synthetic val$httpClient:Lcom/google/android/apps/books/net/ResponseGetter;

.field final synthetic val$lsid:Ljava/lang/String;

.field final synthetic val$sid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/net/ResponseGetter;)V
    .locals 1

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    iput-object p2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$sid:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$lsid:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$httpClient:Lcom/google/android/apps/books/net/ResponseGetter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->mException:Ljava/io/IOException;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 170
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 11
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 175
    # getter for: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->ISSUE_AUTH_TOKEN_URL:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$200()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v9, "SID"

    iget-object v10, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$sid:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v9, "LSID"

    iget-object v10, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$lsid:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 179
    .local v6, "url":Ljava/lang/String;
    :try_start_0
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v3, v6}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 180
    .local v3, "request":Lorg/apache/http/client/methods/HttpUriRequest;
    iget-object v7, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$httpClient:Lcom/google/android/apps/books/net/ResponseGetter;

    const/4 v9, 0x0

    const/4 v10, 0x3

    new-array v10, v10, [I

    fill-array-data v10, :array_0

    invoke-interface {v7, v3, v9, v10}, Lcom/google/android/apps/books/net/ResponseGetter;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Landroid/accounts/Account;[I)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 183
    .local v4, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    .line 184
    .local v5, "statusLine":Lorg/apache/http/StatusLine;
    if-eqz v5, :cond_1

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    const/16 v9, 0x193

    if-ne v7, v9, :cond_1

    .line 186
    iget-object v7, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    iget-object v7, v7, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    check-cast v7, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;

    invoke-interface {v7}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 187
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # getter for: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mInvalidatedTokens:Z
    invoke-static {v7}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$300(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 188
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 189
    .local v1, "am":Landroid/accounts/AccountManager;
    const-string v7, "com.google"

    iget-object v9, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$lsid:Ljava/lang/String;

    invoke-virtual {v1, v7, v9}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v7, "com.google"

    iget-object v9, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->val$sid:Ljava/lang/String;

    invoke-virtual {v1, v7, v9}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v7, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    const/4 v9, 0x1

    # setter for: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mInvalidatedTokens:Z
    invoke-static {v7, v9}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$302(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Z)Z

    .end local v1    # "am":Landroid/accounts/AccountManager;
    :cond_0
    move-object v7, v8

    .line 202
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v3    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    .end local v5    # "statusLine":Lorg/apache/http/StatusLine;
    :goto_0
    return-object v7

    .line 195
    .restart local v3    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    .restart local v4    # "response":Lorg/apache/http/HttpResponse;
    .restart local v5    # "statusLine":Lorg/apache/http/StatusLine;
    :cond_1
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    const-string v9, "UTF-8"

    invoke-static {v7, v9}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_0

    .line 197
    .end local v3    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v4    # "response":Lorg/apache/http/HttpResponse;
    .end local v5    # "statusLine":Lorg/apache/http/StatusLine;
    :catch_0
    move-exception v2

    .line 198
    .local v2, "e":Ljava/io/IOException;
    iput-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->mException:Ljava/io/IOException;

    .line 199
    const-string v7, "TokenAuthHelper"

    const/4 v9, 0x6

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 200
    const-string v7, "TokenAuthHelper"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IO error while acquiring token: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v7, v8

    .line 202
    goto :goto_0

    .line 180
    :array_0
    .array-data 4
        0xc8
        0xc9
        0x193
    .end array-data
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 170
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 208
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->uriForToken(Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v1, p1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$400(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 209
    .local v0, "uri":Landroid/net/Uri;
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    iget-object v2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$1;->mException:Ljava/io/IOException;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->deliverResult(Landroid/net/Uri;Ljava/lang/Exception;)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$500(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Landroid/net/Uri;Ljava/lang/Exception;)V

    .line 210
    return-void

    .line 208
    .end local v0    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
