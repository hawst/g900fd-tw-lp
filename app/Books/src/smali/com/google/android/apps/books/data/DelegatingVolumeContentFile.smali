.class public Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;
.super Ljava/lang/Object;
.source "DelegatingVolumeContentFile.java"

# interfaces
.implements Lcom/google/android/apps/books/data/VolumeContentFile;


# instance fields
.field private final mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/data/VolumeContentFile;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/apps/books/data/VolumeContentFile;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    .line 16
    return-void
.end method


# virtual methods
.method public createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->createSaver()Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;

    move-result-object v0

    return-object v0
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->exists()Z

    move-result v0

    return v0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->getLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->openInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public openParcelFileDescriptor(I)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .param p1, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/books/data/DelegatingVolumeContentFile;->mDelegate:Lcom/google/android/apps/books/data/VolumeContentFile;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/data/VolumeContentFile;->openParcelFileDescriptor(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method
