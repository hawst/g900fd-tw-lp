.class public Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;
.super Lcom/google/android/apps/books/render/StubReaderListener;
.source "WebViewRendererModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/WebViewRendererModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WebViewReaderListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/WebViewRendererModel;)V
    .locals 0

    .prologue
    .line 579
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-direct {p0}, Lcom/google/android/apps/books/render/StubReaderListener;-><init>()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;
    .param p1, "x1"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "x2"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 579
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v0

    return v0
.end method

.method private computeFirstPositionOnNextPage(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/common/Position;
    .locals 4
    .param p1, "firstPositionOnNextPageInThisChapter"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "passageIndex"    # I

    .prologue
    const/4 v2, 0x0

    .line 970
    if-eqz p1, :cond_0

    .line 981
    .end local p1    # "firstPositionOnNextPageInThisChapter":Lcom/google/android/apps/books/common/Position;
    :goto_0
    return-object p1

    .line 974
    .restart local p1    # "firstPositionOnNextPageInThisChapter":Lcom/google/android/apps/books/common/Position;
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mVolumeMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3, p2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionAfter(I)Ljava/lang/String;

    move-result-object v1

    .line 975
    .local v1, "positionString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 976
    new-instance p1, Lcom/google/android/apps/books/common/Position;

    .end local p1    # "firstPositionOnNextPageInThisChapter":Lcom/google/android/apps/books/common/Position;
    invoke-direct {p1, v1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 978
    .end local v1    # "positionString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    move-object p1, v2

    .line 979
    goto :goto_0

    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .restart local v1    # "positionString":Ljava/lang/String;
    .restart local p1    # "firstPositionOnNextPageInThisChapter":Lcom/google/android/apps/books/common/Position;
    :cond_1
    move-object p1, v2

    .line 981
    goto :goto_0
.end method

.method private maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z
    .locals 9
    .param p1, "pageIdentifier"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "consumer"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p3, "passageIndex"    # I
    .param p4, "pageIndex"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 914
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v6

    if-eqz v6, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v4, v5

    .line 950
    :cond_1
    :goto_0
    return v4

    .line 918
    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/PaginationState;

    move-result-object v6

    invoke-virtual {v6, p3, p4, v4}, Lcom/google/android/apps/books/render/PaginationState;->normalizePageIndices(IIZ)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 921
    .local v1, "normalized":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v1, :cond_1

    .line 925
    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v6

    if-eqz v6, :cond_3

    .line 927
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v6

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->getOutOfBoundsMargin(I)I
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1500(I)I

    move-result v6

    invoke-static {v6}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v6

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    invoke-static {v4, p2, v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$600(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    move v4, v5

    .line 929
    goto :goto_0

    .line 932
    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/books/render/PageIdentifier;->getIndices()Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v0

    .line 933
    .local v0, "indices":Lcom/google/android/apps/books/render/PageIndices;
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget v7, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v8, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getDevicePageRendering(II)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v2

    .line 935
    .local v2, "pageRendering":Lcom/google/android/apps/books/widget/DevicePageRendering;
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget v7, v0, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v3

    .line 937
    .local v3, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    iget v6, v0, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/PassagePages;->getLastReadyPageIndex()I

    move-result v7

    if-le v6, v7, :cond_4

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/PassagePages;->isPassageReady()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 941
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V
    invoke-static {v4, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1200(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/util/PassagePages;)V

    .line 943
    const-string v4, "WebViewRendererModel"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 944
    const-string v4, "WebViewRendererModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "**** Rendering Chap.Page: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->sendOnRendered(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V
    invoke-static {v4, v2, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1600(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    move v4, v5

    .line 948
    goto/16 :goto_0
.end method

.method private normalizeSpreadPageIdentifier(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/PageIdentifier;
    .locals 5
    .param p1, "spi"    # Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .prologue
    .line 959
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    iget-object v3, p1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v4, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mTempPageIdentifiers:[Lcom/google/android/apps/books/render/PageIdentifier;
    invoke-static {v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1700(Lcom/google/android/apps/books/render/WebViewRendererModel;)[Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getSpreadPageIdentifiers(Lcom/google/android/apps/books/render/SpreadIdentifier;[Lcom/google/android/apps/books/render/PageIdentifier;)[Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v1

    .line 961
    .local v1, "pages":[Lcom/google/android/apps/books/render/PageIdentifier;
    if-nez v1, :cond_0

    .line 962
    const/4 v2, 0x0

    .line 965
    :goto_0
    return-object v2

    .line 964
    :cond_0
    iget v2, p1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->pageIndex:I

    aget-object v0, v1, v2

    .line 965
    .local v0, "page":Lcom/google/android/apps/books/render/PageIdentifier;
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->normalizePageIdentifier(Lcom/google/android/apps/books/render/PageIdentifier;Z)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v2

    goto :goto_0
.end method

.method private publishReadyPages()V
    .locals 15

    .prologue
    .line 722
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 723
    .local v10, "requestsToRemove":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$700(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 724
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/render/RenderPosition;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/books/render/RenderPosition;

    .line 725
    .local v8, "position":Lcom/google/android/apps/books/render/RenderPosition;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 726
    .local v9, "requestId":I
    iget-object v5, v8, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 727
    .local v5, "page":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v5, :cond_2

    .line 728
    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidIndices()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 729
    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v12

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v13

    add-int v6, v12, v13

    .line 730
    .local v6, "pageIndex":I
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/google/android/apps/books/util/WeakSparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 731
    .local v0, "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v12

    invoke-direct {p0, v5, v0, v12, v6}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 732
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    .line 733
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 735
    .end local v0    # "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .end local v6    # "pageIndex":I
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->hasValidPosition()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 736
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/PaginationState;

    move-result-object v12

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->getPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/apps/books/render/PaginationState;->getPageIndices(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/render/PageIndices;

    move-result-object v7

    .line 738
    .local v7, "pageIndices":Lcom/google/android/apps/books/render/PageIndices;
    if-eqz v7, :cond_0

    .line 739
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/google/android/apps/books/util/WeakSparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 741
    .restart local v0    # "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    iget v12, v7, Lcom/google/android/apps/books/render/PageIndices;->passageIndex:I

    iget v13, v7, Lcom/google/android/apps/books/render/PageIndices;->pageIndex:I

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromPosition()I

    move-result v14

    add-int/2addr v13, v14

    invoke-direct {p0, v5, v0, v12, v13}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 743
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    .line 744
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 749
    .end local v0    # "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .end local v7    # "pageIndices":Lcom/google/android/apps/books/render/PageIndices;
    :cond_2
    iget-object v11, v8, Lcom/google/android/apps/books/render/RenderPosition;->spreadPageIdentifier:Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    .line 750
    .local v11, "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    if-eqz v11, :cond_0

    .line 751
    invoke-direct {p0, v11}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->normalizeSpreadPageIdentifier(Lcom/google/android/apps/books/render/SpreadPageIdentifier;)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v4

    .line 752
    .local v4, "normalized":Lcom/google/android/apps/books/render/PageIdentifier;
    if-eqz v4, :cond_0

    .line 753
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/google/android/apps/books/util/WeakSparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 755
    .restart local v0    # "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPageIndex()I

    move-result v12

    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getOffsetFromIndices()I

    move-result v13

    add-int v6, v12, v13

    .line 757
    .restart local v6    # "pageIndex":I
    invoke-virtual {v4}, Lcom/google/android/apps/books/render/PageIdentifier;->getPassageIndex()I

    move-result v12

    invoke-direct {p0, v4, v0, v12, v6}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 759
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v12

    invoke-virtual {v12, v9}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    .line 760
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 766
    .end local v0    # "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/render/RenderPosition;>;"
    .end local v4    # "normalized":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v5    # "page":Lcom/google/android/apps/books/render/PageIdentifier;
    .end local v6    # "pageIndex":I
    .end local v8    # "position":Lcom/google/android/apps/books/render/RenderPosition;
    .end local v9    # "requestId":I
    .end local v11    # "spi":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    :cond_3
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 767
    .local v1, "currentRequestId":Ljava/lang/Integer;
    iget-object v12, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;
    invoke-static {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$700(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v12

    invoke-interface {v12, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 769
    .end local v1    # "currentRequestId":Ljava/lang/Integer;
    :cond_4
    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1046
    const-string v0, "BooksJS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1047
    const-string v0, "BooksJS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JS DEBUG: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1053
    const-string v0, "BooksJS"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054
    const-string v0, "BooksJS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JS ERROR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    :cond_0
    return-void
.end method

.method public onActivatedMoElement(IILjava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "elementId"    # Ljava/lang/String;

    .prologue
    .line 999
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    :cond_0
    :goto_0
    return-void

    .line 1003
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/RendererListener;->onActivatedMoElement(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public onChapterReady(ILcom/google/android/apps/books/util/JsPerformanceMetrics;)V
    .locals 4
    .param p1, "passageIndex"    # I
    .param p2, "performanceMetrics"    # Lcom/google/android/apps/books/util/JsPerformanceMetrics;

    .prologue
    .line 675
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 697
    :goto_0
    return-void

    .line 679
    :cond_0
    const-string v1, "WebViewRendererModel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 680
    const-string v1, "WebViewRendererModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onChapterReady: passage="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    invoke-virtual {p2}, Lcom/google/android/apps/books/util/JsPerformanceMetrics;->isValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 682
    const-string v1, "WebViewRendererModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "JS metrics: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 687
    .local v0, "passage":Lcom/google/android/apps/books/util/PassagePages;
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->markReadyAndSizeKnown()V

    .line 688
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->markPassageAsUsed(Lcom/google/android/apps/books/util/PassagePages;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1200(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/util/PassagePages;)V

    .line 692
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->publishReadyPages()V

    .line 694
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->purgeLruPassagesAndRequests()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1300(Lcom/google/android/apps/books/render/WebViewRendererModel;)V

    .line 696
    invoke-static {p2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->logJsPerformance(Lcom/google/android/apps/books/util/JsPerformanceMetrics;)V

    goto :goto_0
.end method

.method public onChapterUnloaded(I)V
    .locals 4
    .param p1, "passageIndex"    # I

    .prologue
    .line 630
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 636
    .local v0, "passage":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v0, :cond_2

    .line 637
    invoke-virtual {v0}, Lcom/google/android/apps/books/util/PassagePages;->markPurged()V

    .line 639
    :cond_2
    const-string v1, "WebViewRendererModel"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 640
    const-string v1, "WebViewRendererModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "**** onChapterUnloaded Chap: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 1060
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1071
    :goto_0
    return-void

    .line 1065
    :cond_0
    instance-of v1, p1, Ljava/lang/Exception;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 1066
    check-cast v0, Ljava/lang/Exception;

    .line 1070
    .local v0, "exception":Ljava/lang/Exception;
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->dispatchRenderError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1068
    .end local v0    # "exception":Ljava/lang/Exception;
    :cond_1
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .restart local v0    # "exception":Ljava/lang/Exception;
    goto :goto_1
.end method

.method public onInvalidPosition(II)V
    .locals 3
    .param p1, "margin"    # I
    .param p2, "requestId"    # I

    .prologue
    .line 595
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 601
    :goto_0
    return-void

    .line 599
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 600
    .local v0, "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-static {p1}, Lcom/google/android/apps/books/render/SpecialPageIdentifier;->makeOutOfBounds(I)Lcom/google/android/apps/books/render/SpecialPageIdentifier;

    move-result-object v2

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->dispatchSpecialState(Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$600(Lcom/google/android/apps/books/render/WebViewRendererModel;Lcom/google/android/apps/books/render/RenderResponseConsumer;Lcom/google/android/apps/books/render/SpecialPageIdentifier;)V

    goto :goto_0
.end method

.method public onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "textPointsJson"    # Ljava/lang/String;
    .param p4, "handlesPointsJson"    # Ljava/lang/String;
    .param p5, "textRange"    # Lcom/google/android/apps/books/annotations/TextLocationRange;
    .param p6, "textContext"    # Lcom/google/android/apps/books/annotations/AnnotationTextualContext;

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1032
    :cond_0
    :goto_0
    return-void

    .line 1028
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/books/render/RendererListener;->onLoadedRangeData(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/annotations/TextLocationRange;Lcom/google/android/apps/books/annotations/AnnotationTextualContext;)V

    goto :goto_0
.end method

.method public onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V
    .locals 3
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/common/collect/Multimap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1036
    .local p2, "idToRects":Lcom/google/common/collect/Multimap;, "Lcom/google/common/collect/Multimap<Ljava/lang/String;Landroid/graphics/Rect;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1042
    :cond_0
    return-void

    .line 1039
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mOnLoadedRangeDataBulkListenersWeakSet:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1900(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;

    .line 1040
    .local v1, "listener":Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;
    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/books/widget/PagesViewController$WebLoadListener;->onLoadedRangeDataBulk(ILcom/google/common/collect/Multimap;)V

    goto :goto_0
.end method

.method public onMissingPosition(I)V
    .locals 3
    .param p1, "requestId"    # I

    .prologue
    .line 605
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 617
    :goto_0
    return-void

    .line 609
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 610
    .local v0, "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$700(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener$1;-><init>(Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;Lcom/google/android/apps/books/render/RenderResponseConsumer;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->dispatchRenderCallback(Lcom/google/android/apps/books/render/RenderResponseConsumer;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 6
    .param p1, "chapterIndex"    # I
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "offsetFromPosition"    # I
    .param p4, "str"    # Ljava/lang/String;
    .param p5, "offset"    # I

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/RendererListener;->onNearbyTextLoaded(ILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method public onPageData(IILjava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;Lcom/google/android/apps/books/common/Position;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "readingPosition"    # Ljava/lang/String;
    .param p6, "bounds"    # Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;
    .param p7, "firstPositionOnNextPageInPassage"    # Lcom/google/android/apps/books/common/Position;
    .param p8, "pageText"    # Ljava/lang/String;
    .param p9, "debugText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;",
            "Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 825
    .local p4, "allPositionsOnThisPage":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/common/Position;>;"
    .local p5, "touchableItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    iget-object v11, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v11}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 901
    :cond_0
    :goto_0
    return-void

    .line 829
    :cond_1
    const-string v11, "WebViewRendererModel"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 830
    const-string v11, "WebViewRendererModel"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onPageData("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    :cond_2
    move-object/from16 v0, p7

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->computeFirstPositionOnNextPage(Lcom/google/android/apps/books/common/Position;I)Lcom/google/android/apps/books/common/Position;

    move-result-object v2

    .line 836
    .local v2, "firstPositionOnNextPage":Lcom/google/android/apps/books/common/Position;
    iget-object v11, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v11, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v9

    .line 839
    .local v9, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    if-lez p2, :cond_3

    invoke-virtual {v9}, Lcom/google/android/apps/books/util/PassagePages;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    .line 845
    :cond_3
    new-instance v6, Lcom/google/android/apps/books/render/PageIndices;

    move/from16 v0, p2

    invoke-direct {v6, p1, v0}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    .line 847
    .local v6, "pageIndices":Lcom/google/android/apps/books/render/PageIndices;
    const/4 v1, 0x0

    .line 848
    .local v1, "fallbackPosition":Lcom/google/android/apps/books/common/Position;
    const/4 v7, 0x0

    .line 851
    .local v7, "pageOffsetFromFallbackPosition":I
    if-eqz p4, :cond_4

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_9

    .line 852
    :cond_4
    add-int/lit8 v5, p2, -0x1

    .line 853
    .local v5, "pageIndexWithFallback":I
    :goto_1
    if-ltz v5, :cond_5

    .line 854
    invoke-virtual {v9, v5}, Lcom/google/android/apps/books/util/PassagePages;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v8

    .line 857
    .local v8, "pageWithFallback":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-eqz v8, :cond_7

    .line 858
    invoke-virtual {v8}, Lcom/google/android/apps/books/widget/DevicePageRendering;->getLastViewablePosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v1

    .line 859
    if-eqz v1, :cond_8

    .line 860
    sub-int v7, p2, v5

    .line 870
    .end local v8    # "pageWithFallback":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_5
    :goto_2
    if-nez v1, :cond_9

    .line 872
    const-string v11, "WebViewRendererModel"

    const/4 v12, 0x6

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 873
    const-string v11, "WebViewRendererModel"

    const-string v12, "Couldn\'t find a fallback position."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :cond_6
    iget-object v11, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mCallbacks:Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;
    invoke-static {v11}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$800(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;

    move-result-object v11

    new-instance v12, Lcom/google/android/apps/books/render/WebViewRendererModel$NoPositionInPassageException;

    invoke-direct {v12}, Lcom/google/android/apps/books/render/WebViewRendererModel$NoPositionInPassageException;-><init>()V

    invoke-interface {v11, v12}, Lcom/google/android/apps/books/render/WebViewRendererModel$Callbacks;->dispatchRenderError(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 864
    .restart local v8    # "pageWithFallback":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_7
    const-string v11, "WebViewRendererModel"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to find fallback position. Missing page "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " after receiving page "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 853
    :cond_8
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    .line 881
    .end local v5    # "pageIndexWithFallback":I
    .end local v8    # "pageWithFallback":Lcom/google/android/apps/books/widget/DevicePageRendering;
    :cond_9
    invoke-static {}, Lcom/google/android/apps/books/widget/DevicePageRendering;->newBuilder()Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    invoke-virtual {v11, v6}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setIndices(Lcom/google/android/apps/books/render/PageIndices;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-virtual {v11, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setViewablePositions(Ljava/util/List;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    invoke-virtual {v11, v2}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setFirstPositionOnNextPage(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    move-object/from16 v0, p5

    invoke-virtual {v11, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setTouchables(Ljava/util/List;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v11, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setPageBounds(Lcom/google/android/apps/books/widget/DevicePageRendering$PageBounds;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    move-object/from16 v0, p8

    invoke-virtual {v11, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setPageText(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    move-object/from16 v0, p9

    invoke-virtual {v11, v0}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setDebugText(Ljava/lang/String;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setFallbackPosition(Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    invoke-virtual {v11, v7}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->setPageOffset(I)Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/books/widget/DevicePageRendering$Builder;->build()Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v4

    .line 896
    .local v4, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/common/Position;

    .line 897
    .local v10, "position":Lcom/google/android/apps/books/common/Position;
    iget-object v11, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mPaginationState:Lcom/google/android/apps/books/render/PaginationState;
    invoke-static {v11}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/PaginationState;

    move-result-object v11

    invoke-virtual {v11, v10, v6}, Lcom/google/android/apps/books/render/PaginationState;->setPageIndices(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/render/PageIndices;)V

    goto :goto_3

    .line 900
    .end local v10    # "position":Lcom/google/android/apps/books/common/Position;
    :cond_a
    invoke-virtual {v9, v4}, Lcom/google/android/apps/books/util/PassagePages;->add(Lcom/google/android/apps/books/widget/DevicePageRendering;)V

    goto/16 :goto_0
.end method

.method public onPageLoaded(III)V
    .locals 10
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I
    .param p3, "requestId"    # I

    .prologue
    const/4 v9, 0x6

    .line 773
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 816
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v6

    invoke-virtual {v6, p3}, Lcom/google/android/apps/books/util/WeakSparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    .line 780
    .local v0, "consumer":Lcom/google/android/apps/books/render/RenderResponseConsumer;
    if-eqz v0, :cond_0

    .line 784
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$700(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/render/RenderPosition;

    .line 786
    .local v5, "requestedPosition":Lcom/google/android/apps/books/render/RenderPosition;
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v4

    .line 787
    .local v4, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/books/util/PassagePages;->getLastReadyPageIndex()I

    move-result v6

    if-le p2, v6, :cond_3

    .line 788
    :cond_2
    const-string v6, "WebViewRendererModel"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 789
    const-string v6, "WebViewRendererModel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPageLoaded called for ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") which is not in a ready page range"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 795
    :cond_3
    new-instance v1, Lcom/google/android/apps/books/render/PageIndices;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    .line 796
    .local v1, "indices":Lcom/google/android/apps/books/render/PageIndices;
    const-string v6, "WebViewRendererModel"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 797
    const-string v6, "WebViewRendererModel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPageLoaded: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    :cond_4
    invoke-virtual {v4, p2}, Lcom/google/android/apps/books/util/PassagePages;->getPageRendering(I)Lcom/google/android/apps/books/widget/DevicePageRendering;

    move-result-object v2

    .line 801
    .local v2, "pageData":Lcom/google/android/apps/books/widget/DevicePageRendering;
    if-nez v2, :cond_5

    .line 802
    const-string v6, "WebViewRendererModel"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 803
    const-string v6, "WebViewRendererModel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPageLoaded called for indices with null page data. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 808
    :cond_5
    iget-object v6, v5, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    if-eqz v6, :cond_6

    iget-object v3, v5, Lcom/google/android/apps/books/render/RenderPosition;->pageIdentifier:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 812
    .local v3, "pageIdentifierToRender":Lcom/google/android/apps/books/render/PageIdentifier;
    :goto_1
    invoke-direct {p0, v3, v0, p1, p2}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->maybeRenderPage(Lcom/google/android/apps/books/render/PageIdentifier;Lcom/google/android/apps/books/render/RenderResponseConsumer;II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 813
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToConsumer:Lcom/google/android/apps/books/util/WeakSparseArray;
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$500(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/util/WeakSparseArray;

    move-result-object v6

    invoke-virtual {v6, p3}, Lcom/google/android/apps/books/util/WeakSparseArray;->remove(I)Ljava/lang/Object;

    .line 814
    iget-object v6, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mRequestIdToPosition:Ljava/util/Map;
    invoke-static {v6}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$700(Lcom/google/android/apps/books/render/WebViewRendererModel;)Ljava/util/Map;

    move-result-object v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 808
    .end local v3    # "pageIdentifierToRender":Lcom/google/android/apps/books/render/PageIdentifier;
    :cond_6
    const/4 v6, 0x0

    invoke-static {p1, p2, v6}, Lcom/google/android/apps/books/render/PageIdentifier;->withIndices(III)Lcom/google/android/apps/books/render/PageIdentifier;

    move-result-object v3

    goto :goto_1
.end method

.method public onPageRangeReady(II)V
    .locals 4
    .param p1, "passageIndex"    # I
    .param p2, "lastReadyPageIndex"    # I

    .prologue
    .line 701
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 716
    :goto_0
    return-void

    .line 705
    :cond_0
    const-string v1, "WebViewRendererModel"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 706
    const-string v1, "WebViewRendererModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPageRangeReady("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getOrAddPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v0

    .line 709
    .local v0, "passagePages":Lcom/google/android/apps/books/util/PassagePages;
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/util/PassagePages;->setLastReadyPageIndex(I)V

    .line 713
    invoke-direct {p0}, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->publishReadyPages()V

    .line 715
    iget-object v1, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # invokes: Lcom/google/android/apps/books/render/WebViewRendererModel;->purgeLruPassagesAndRequests()V
    invoke-static {v1}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1300(Lcom/google/android/apps/books/render/WebViewRendererModel;)V

    goto :goto_0
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1011
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 1015
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1016
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/books/render/RendererListener;->onPassageMoListReady(IILjava/util/Map;)V

    goto :goto_0
.end method

.method public onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 987
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 995
    :cond_0
    :goto_0
    return-void

    .line 991
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/RendererListener;->onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    goto :goto_0
.end method

.method public onPassagesPurged(ILjava/util/Collection;)V
    .locals 6
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 646
    .local p2, "passageIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mPurgeInProgress:Z
    invoke-static {v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1002(Lcom/google/android/apps/books/render/WebViewRendererModel;Z)Z

    .line 652
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;
    invoke-static {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1100(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/LruPassagePurger;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 656
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 657
    .local v1, "index":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/render/WebViewRendererModel;->getPassagePages(I)Lcom/google/android/apps/books/util/PassagePages;

    move-result-object v2

    .line 658
    .local v2, "passage":Lcom/google/android/apps/books/util/PassagePages;
    if-eqz v2, :cond_3

    .line 659
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mPassagePurger:Lcom/google/android/apps/books/render/LruPassagePurger;
    invoke-static {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$1100(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/LruPassagePurger;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/apps/books/render/LruPassagePurger;->remove(Lcom/google/android/apps/books/util/PassagePages;)V

    .line 660
    invoke-virtual {v2}, Lcom/google/android/apps/books/util/PassagePages;->markPurged()V

    .line 663
    :cond_3
    const-string v3, "WebViewRendererModel"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 664
    const-string v3, "WebViewRendererModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPassagesPurged: passageIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 668
    .end local v1    # "index":Ljava/lang/Integer;
    .end local v2    # "passage":Lcom/google/android/apps/books/util/PassagePages;
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 669
    iget-object v3, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mListener:Lcom/google/android/apps/books/render/RendererListener;
    invoke-static {v3}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$400(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/RendererListener;

    move-result-object v3

    invoke-interface {v3, p2}, Lcom/google/android/apps/books/render/RendererListener;->onPassagesPurged(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public onReaderReady()V
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    :goto_0
    return-void

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/WebViewRendererModel$WebViewReaderListener;->this$0:Lcom/google/android/apps/books/render/WebViewRendererModel;

    # getter for: Lcom/google/android/apps/books/render/WebViewRendererModel;->mReader:Lcom/google/android/apps/books/render/ReaderController;
    invoke-static {v0}, Lcom/google/android/apps/books/render/WebViewRendererModel;->access$900(Lcom/google/android/apps/books/render/WebViewRendererModel;)Lcom/google/android/apps/books/render/ReaderController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/render/ReaderController;->transitionToReaderReady()V

    goto :goto_0
.end method
