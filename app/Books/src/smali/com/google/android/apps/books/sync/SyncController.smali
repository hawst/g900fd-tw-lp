.class public interface abstract Lcom/google/android/apps/books/sync/SyncController;
.super Ljava/lang/Object;
.source "SyncController.java"


# virtual methods
.method public abstract getSyncAutomatically()Z
.end method

.method public abstract requestManualSync()V
.end method

.method public abstract requestManualSync(Z)V
.end method

.method public abstract requestManualUploadOnlySync()V
.end method

.method public varargs abstract requestManualVolumeContentSync(Z[Ljava/lang/String;)V
.end method

.method public varargs abstract requestManualVolumeContentSyncAsync(Z[Ljava/lang/String;)V
.end method

.method public abstract setIsSyncable(Z)V
.end method

.method public abstract setSyncAutomatically(Z)V
.end method

.method public abstract updateSyncSettings(Lcom/google/android/apps/books/util/Config;)V
.end method
