.class Lcom/google/android/apps/books/app/ReaderFragment$41;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mMissingPositions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 1

    .prologue
    .line 8539
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9074
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->mMissingPositions:Ljava/util/Set;

    return-void
.end method

.method private moveToFallbackPosition(Lcom/google/android/apps/books/common/Position;)Z
    .locals 5
    .param p1, "fallbackPosition"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 9132
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->mMissingPositions:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 9137
    :goto_0
    return v0

    .line 9135
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    new-instance v3, Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-direct {v3, p1, v0}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    sget-object v4, Lcom/google/android/apps/books/app/MoveType;->FALLBACK_TO_DEFAULT_POSITION:Lcom/google/android/apps/books/app/MoveType;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V
    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V

    move v0, v1

    .line 9137
    goto :goto_0
.end method

.method private onSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 9189
    if-eqz p1, :cond_0

    .line 9190
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotationController:Lcom/google/android/apps/books/annotations/VolumeAnnotationController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/VolumeAnnotationController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/VolumeAnnotationController;->getForegroundAnnotationEditor()Lcom/google/android/apps/books/annotations/UserChangesEditor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/annotations/UserChangesEditor;->uiUpdateLastUsedTimestampToNow(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 9192
    sget-object v0, Lcom/google/android/apps/books/annotations/Annotation;->BOOKMARK_LAYER_ID:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 9193
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->updateQuickBookmarks()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 9196
    :cond_0
    return-void
.end method


# virtual methods
.method public editHighlight(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 3
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 8730
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 8731
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 8732
    new-instance v0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 8734
    .local v0, "selectionInfo":Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->hasNote()Z

    move-result v1

    if-nez v1, :cond_0

    .line 8735
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->startTextSelection(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;)V

    .line 8739
    :goto_0
    return-void

    .line 8737
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->startEditingNote(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;Z)V

    goto :goto_0
.end method

.method public editNote(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 3
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 8743
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 8744
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 8745
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/Annotation;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/app/SelectionUiHelper;->startEditingNote(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;Z)V

    .line 8746
    return-void
.end method

.method public enableNextSearch(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 8691
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v0

    if-nez v0, :cond_1

    .line 8692
    const-string v0, "ReaderFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8693
    const-string v0, "ReaderFragment"

    const-string v1, "enableNextSearch with mSearchScrubBar null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 8698
    :cond_0
    :goto_0
    return-void

    .line 8697
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setNextButtonEnabled(Z)V

    goto :goto_0
.end method

.method public enablePreviousSearch(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 8680
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v0

    if-nez v0, :cond_1

    .line 8681
    const-string v0, "ReaderFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8682
    const-string v0, "ReaderFragment"

    const-string v1, "enablePreviousSearch with mSearchScrubBar null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 8687
    :cond_0
    :goto_0
    return-void

    .line 8686
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setPreviousButtonEnabled(Z)V

    goto :goto_0
.end method

.method public getAnnotationsForImagePage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "layerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9157
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->getAnnotationsForImagePage(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotationsForPassage(I)Ljava/util/Iterator;
    .locals 5
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 8850
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 8852
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionRangeForPassage(I)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->getAnnotationsInRangeIterator(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Iterator;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 8861
    :cond_0
    :goto_0
    return-object v1

    .line 8854
    :catch_0
    move-exception v0

    .line 8855
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v2, "ReaderFragment"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8856
    const-string v2, "ReaderFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to find annotations for passage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAnnotationsForPassage(ILjava/util/Set;)Ljava/util/List;
    .locals 4
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9142
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 9144
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAnnotations:Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPositionRangeForPassage(I)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/google/android/apps/books/annotations/ListeningAnnotationSet;->getAnnotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;Ljava/util/Set;)Lcom/google/common/collect/ImmutableList;
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 9152
    :goto_0
    return-object v1

    .line 9146
    :catch_0
    move-exception v0

    .line 9147
    .local v0, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v1, "ReaderFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9148
    const-string v1, "ReaderFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to find annotations for passage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 9152
    .end local v0    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->of()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    goto :goto_0
.end method

.method public getEndOfBookView()Landroid/view/View;
    .locals 2

    .prologue
    .line 8948
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getBookView()Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$300(Lcom/google/android/apps/books/app/ReaderFragment;)Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0e0113

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 8765
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getLocale()Ljava/util/Locale;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16200(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getPaintableAnnotations(ILjava/util/Set;)Ljava/util/List;
    .locals 2
    .param p1, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/PaintableTextRange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8843
    .local p2, "layerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 8844
    .local v0, "resultSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/PaintableTextRange;>;"
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment$41;->getAnnotationsForPassage(ILjava/util/Set;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 8845
    return-object v0
.end method

.method public getRenderingTheme(Lcom/google/android/apps/books/render/ReaderSettings;)Ljava/lang/String;
    .locals 1
    .param p1, "settings"    # Lcom/google/android/apps/books/render/ReaderSettings;

    .prologue
    .line 8771
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isAppleFixedLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8772
    const-string v0, "0"

    .line 8781
    :goto_0
    return-object v0

    .line 8775
    :cond_0
    if-nez p1, :cond_1

    .line 8779
    const-string v0, "0"

    goto :goto_0

    .line 8781
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/books/render/ReaderSettings;->readerTheme:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSearchResultMap()Lcom/google/android/apps/books/app/SearchResultMap;
    .locals 1

    .prologue
    .line 8953
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchResultMap:Lcom/google/android/apps/books/app/SearchResultMap;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20600(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SearchResultMap;

    move-result-object v0

    return-object v0
.end method

.method public getSelectionPopup()Lcom/google/android/apps/books/widget/SelectionPopup;
    .locals 1

    .prologue
    .line 8991
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->getPopup()Lcom/google/android/apps/books/widget/SelectionPopup;

    move-result-object v0

    return-object v0
.end method

.method public getTextLocationComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 8866
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8867
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getTextLocationComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 8869
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUiMode()Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    .locals 1

    .prologue
    .line 9068
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentUiMode:Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$21200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    move-result-object v0

    return-object v0
.end method

.method public getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    .locals 1

    .prologue
    .line 8755
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v0

    return-object v0
.end method

.method public isAnyAudioPlaying()Z
    .locals 1

    .prologue
    .line 9200
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isAnyAudioPlaying()Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$17600(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    return v0
.end method

.method public isScrubbing()Z
    .locals 1

    .prologue
    .line 9015
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/ScrubBarController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ScrubBarController;->isScrubbing()Z

    move-result v0

    return v0
.end method

.method public isValidPassageIndex(I)Z
    .locals 1
    .param p1, "passageIndex"    # I

    .prologue
    .line 8886
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPassageCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVertical()Z
    .locals 1

    .prologue
    .line 8760
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->isVertical()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybeLoadEndOfBookPage()V
    .locals 1

    .prologue
    .line 8914
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeLoadEndOfBookPage()V

    .line 8915
    return-void
.end method

.method public moveScrubberToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)V
    .locals 1
    .param p1, "spreadIdentifier"    # Lcom/google/android/apps/books/render/SpreadIdentifier;

    .prologue
    .line 9025
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/ScrubBarController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/ScrubBarController;->moveToSpread(Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 9026
    return-void
.end method

.method public moveToImageModeSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
    .locals 6
    .param p1, "matchLocation"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    const/4 v4, 0x0

    .line 8908
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    iget-object v2, p1, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    invoke-direct {v1, v2, v4}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchQuery:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7700(Lcom/google/android/apps/books/app/ReaderFragment;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/books/app/MoveType;->MOVE_TO_SEARCH_RESULT:Lcom/google/android/apps/books/app/MoveType;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;ZLjava/lang/String;ZLcom/google/android/apps/books/app/MoveType;)V

    .line 8910
    return-void
.end method

.method public moveToPosition(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    const/4 v3, 0x1

    .line 8901
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    new-instance v1, Lcom/google/android/apps/books/render/SpreadIdentifier;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/books/render/SpreadIdentifier;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V
    invoke-static {v0, v1, v3, v3, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20300(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;ZZLcom/google/android/apps/books/app/MoveType;)V

    .line 8904
    return-void
.end method

.method public moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 1
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "moveType"    # Lcom/google/android/apps/books/app/MoveType;
    .param p3, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 9163
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onUserSelectedNewPosition()V

    .line 9164
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->moveToPosition(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;)V

    .line 9165
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 9166
    return-void
.end method

.method public moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
    .locals 1
    .param p1, "matchLocation"    # Lcom/google/android/apps/books/annotations/TextLocation;

    .prologue
    .line 8896
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->moveToSearchLocation(Lcom/google/android/apps/books/annotations/TextLocation;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4200(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 8897
    return-void
.end method

.method public onCanceledRendererRequests()V
    .locals 3

    .prologue
    .line 8786
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v0

    .line 8787
    .local v0, "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-eqz v0, :cond_0

    .line 8788
    invoke-virtual {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->onPendingRequestsCanceled()V

    .line 8790
    :cond_0
    return-void
.end method

.method public onCompletedPageTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    .locals 6
    .param p1, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;

    .prologue
    .line 9030
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isTtsSpeaking()Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isMoSpeaking()Z
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 9043
    :cond_0
    :goto_0
    return-void

    .line 9035
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->getWritingDirection()Lcom/google/android/apps/books/util/WritingDirection;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/google/android/apps/books/util/ReadingDirection;->fromScreenDirection(Lcom/google/android/apps/books/util/ScreenDirection;Lcom/google/android/apps/books/util/WritingDirection;)Lcom/google/android/apps/books/util/ReadingDirection;

    move-result-object v2

    .line 9037
    .local v2, "readingDirection":Lcom/google/android/apps/books/util/ReadingDirection;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesView:Lcom/google/android/apps/books/widget/PagesView;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesView;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/widget/PagesView;->getView()Landroid/view/View;

    move-result-object v3

    .line 9038
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 9039
    .local v0, "context":Landroid/content/Context;
    sget-object v4, Lcom/google/android/apps/books/util/ReadingDirection;->FORWARD:Lcom/google/android/apps/books/util/ReadingDirection;

    if-ne v2, v4, :cond_2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f011c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 9042
    .local v1, "pageTurnText":Ljava/lang/String;
    :goto_1
    invoke-static {v0, v3, v1}, Lcom/google/android/apps/books/util/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 9039
    .end local v1    # "pageTurnText":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f011d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public onDismissedSelection()V
    .locals 1

    .prologue
    .line 8816
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->textSelectionHasEnded()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20000(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8819
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8820
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUnPauseMo()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19600(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8821
    return-void
.end method

.method public onEndOfBookPresenceChanged(Z)V
    .locals 4
    .param p1, "showingEndOfBookPage"    # Z

    .prologue
    const/4 v0, 0x1

    .line 8927
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8928
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p1, :cond_2

    .line 8929
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onTurnedToEndOfBookPage()V

    .line 8933
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v1, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2502(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8936
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 8942
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/app/ReaderMenu$Item;->BOOKMARK:Lcom/google/android/apps/books/app/ReaderMenu$Item;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/books/app/ReaderMenu;->setItemVisible(Lcom/google/android/apps/books/app/ReaderMenu$Item;Z)V

    .line 8944
    :cond_1
    return-void

    .line 8930
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mShowingEobPage:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 8931
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowEOBBRecommendations(Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18600(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 8942
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 8675
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onFatalException(Ljava/lang/Exception;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11100(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/Exception;)V

    .line 8676
    return-void
.end method

.method public onFinishedTransitionToMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    .line 9063
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setBarVisibilityFromMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$21100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V

    .line 9064
    return-void
.end method

.method public onIsTurningChanged(ZZ)V
    .locals 1
    .param p1, "isTurning"    # Z
    .param p2, "fromUser"    # Z

    .prologue
    .line 8716
    if-eqz p1, :cond_0

    .line 8717
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLastStartedTurnFromUser:Z
    invoke-static {v0, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19502(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8719
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUnPauseMo()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19600(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8720
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8721
    return-void
.end method

.method public onMissingPosition(Lcom/google/android/apps/books/common/Position;)V
    .locals 6
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    .line 9078
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 9126
    :cond_0
    :goto_0
    return-void

    .line 9082
    :cond_1
    if-nez p1, :cond_2

    .line 9083
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Unexpected null missing position"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 9087
    :cond_2
    const-string v3, "ReaderFragment"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 9088
    const-string v3, "ReaderFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Missing position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 9091
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->mMissingPositions:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 9095
    invoke-virtual {p1}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v1

    .line 9096
    .local v1, "pageId":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/api/client/util/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 9097
    invoke-static {v1}, Lcom/google/android/apps/books/common/Position;->withPageId(Ljava/lang/String;)Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 9098
    .local v0, "fallbackPosition":Lcom/google/android/apps/books/common/Position;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->moveToFallbackPosition(Lcom/google/android/apps/books/common/Position;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 9104
    .end local v0    # "fallbackPosition":Lcom/google/android/apps/books/common/Position;
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v2

    .line 9105
    .local v2, "savedPosition":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 9106
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    .line 9107
    .restart local v0    # "fallbackPosition":Lcom/google/android/apps/books/common/Position;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->moveToFallbackPosition(Lcom/google/android/apps/books/common/Position;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 9113
    .end local v0    # "fallbackPosition":Lcom/google/android/apps/books/common/Position;
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getDefaultPosition()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 9114
    .restart local v0    # "fallbackPosition":Lcom/google/android/apps/books/common/Position;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->moveToFallbackPosition(Lcom/google/android/apps/books/common/Position;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 9119
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getSegments()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Segment;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Segment;->getStartPositionObject()Lcom/google/android/apps/books/common/Position;

    move-result-object v0

    .line 9120
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderFragment$41;->moveToFallbackPosition(Lcom/google/android/apps/books/common/Position;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 9125
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "No fallback position found"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/app/ReaderFragment$41;->onError(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method public onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V
    .locals 4
    .param p1, "handle"    # Lcom/google/android/apps/books/render/PageHandle;

    .prologue
    .line 9001
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onInternalUserInteraction()V
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 9003
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/ScrubBarController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/ScrubBarController;->getCurrentSpread()Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v0

    .line 9004
    .local v0, "currentPosition":Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-interface {p1}, Lcom/google/android/apps/books/render/PageHandle;->getSpreadPageIdentifier()Lcom/google/android/apps/books/render/SpreadPageIdentifier;

    move-result-object v1

    .line 9007
    .local v1, "spreadPageId":Lcom/google/android/apps/books/render/SpreadPageIdentifier;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/books/render/SpreadPageIdentifier;->spreadIdentifier:Lcom/google/android/apps/books/render/SpreadIdentifier;

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/books/render/Renderer;->getScreenSpreadDifference(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/render/SpreadIdentifier;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 9010
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/ScrubBarController;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/widget/ScrubBarController;->onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V

    .line 9012
    :cond_1
    return-void
.end method

.method public onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V
    .locals 1
    .param p1, "endHandle"    # Lcom/google/android/apps/books/render/PageHandle;
    .param p2, "progress"    # F

    .prologue
    .line 9020
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mScrubBarController:Lcom/google/android/apps/books/widget/ScrubBarController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20800(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/ScrubBarController;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/books/widget/ScrubBarController;->onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V

    .line 9021
    return-void
.end method

.method public onPassageBecameVisible(I)V
    .locals 2
    .param p1, "passageIndex"    # I

    .prologue
    .line 8665
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mGeoLayerEnabled:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19300(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8667
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->GEO:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getLayerAnnotationLoader(Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2700(Lcom/google/android/apps/books/app/ReaderFragment;Ljava/lang/String;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->load(I)V

    .line 8670
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->maybeHideTransitionCover()Z

    .line 8671
    return-void
.end method

.method public onPassageMoListReady(IILjava/util/Map;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 9181
    .local p3, "elementIdsToPages":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getMoController(Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/app/mo/MediaOverlaysController;

    move-result-object v0

    .line 9183
    .local v0, "moController":Lcom/google/android/apps/books/app/mo/MediaOverlaysController;
    if-eqz v0, :cond_0

    .line 9184
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/app/mo/MediaOverlaysController;->onPassageMoListReady(IILjava/util/Map;)V

    .line 9186
    :cond_0
    return-void
.end method

.method public onPositionChanged(Lcom/google/android/apps/books/render/SpreadIdentifier;ZLcom/google/android/apps/books/app/MoveType;)V
    .locals 18
    .param p1, "spread"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p2, "savePosition"    # Z
    .param p3, "moveType"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 8544
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->isDestroyed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8657
    :cond_0
    :goto_0
    return-void

    .line 8551
    :cond_1
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/books/app/MoveType;->nonNull(Lcom/google/android/apps/books/app/MoveType;)Lcom/google/android/apps/books/app/MoveType;

    move-result-object p3

    .line 8553
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/books/app/MoveType;->shouldClearUndoJumpBit()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 8554
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mJustUndidJump:Z
    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18302(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8557
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    move-object/from16 v0, p3

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeClearJumpBit(Lcom/google/android/apps/books/app/MoveType;)V
    invoke-static {v3, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18400(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/MoveType;)V

    .line 8559
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    if-eqz v3, :cond_3

    const/4 v15, 0x1

    .line 8560
    .local v15, "validMetadata":Z
    :goto_1
    if-nez v15, :cond_4

    .line 8561
    const-string v3, "ReaderFragment"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 8562
    const-string v3, "ReaderFragment"

    const-string v4, "validMetadata missing, dropping position update on floor"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 8559
    .end local v15    # "validMetadata":Z
    :cond_3
    const/4 v15, 0x0

    goto :goto_1

    .line 8567
    .restart local v15    # "validMetadata":Z
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8569
    if-eqz p1, :cond_0

    .line 8573
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/books/app/MoveType;->isPageTurn()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, p1

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeShowEOBBRecommendations(Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V
    invoke-static {v3, v0, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18600(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;Ljava/lang/Boolean;)V

    .line 8577
    if-nez p3, :cond_5

    if-nez p2, :cond_c

    :cond_5
    const/4 v3, 0x1

    :goto_2
    const-string v4, "missing lastAction"

    invoke-static {v3, v4}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 8579
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/util/Logger;->shouldLog(Lcom/google/android/apps/books/util/Logger$Category;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 8580
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$15300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/books/util/Logger$Category;->PERFORMANCE:Lcom/google/android/apps/books/util/Logger$Category;

    const-string v8, "position changed"

    invoke-interface {v3, v4, v8}, Lcom/google/android/apps/books/util/Logger;->log(Lcom/google/android/apps/books/util/Logger$Category;Ljava/lang/String;)V

    .line 8583
    :cond_6
    const-string v3, "ReaderFragment"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 8584
    const-string v4, "ReaderFragment"

    const-string v8, "onPositionChanged(%s, fromUser=%b, lastAction=%s)"

    const/4 v3, 0x3

    new-array v9, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/render/SpreadIdentifier;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v9, v3

    const/4 v3, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    aput-object v16, v9, v3

    const/16 v16, 0x2

    if-nez p3, :cond_d

    const-string v3, "null"

    :goto_3
    aput-object v3, v9, v16

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 8592
    :cond_7
    if-eqz p2, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->isViewingSearchResult()Z
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18700(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 8593
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v3}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v5

    .line 8595
    .local v5, "positionString":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 8596
    .local v6, "lastAccess":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/books/app/MoveType;->getServerAction()Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lcom/google/android/apps/books/data/BooksDataController;->setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V

    .line 8598
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v4, 0x1

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mNeedReadingPositionSync:Z
    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8601
    .end local v5    # "positionString":Ljava/lang/String;
    .end local v6    # "lastAccess":J
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    move-result-object v3

    if-nez v3, :cond_e

    const/4 v12, 0x1

    .line 8603
    .local v12, "isFirstVisiblePosition":Z
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    move-object/from16 v0, p1

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mLastKnownPosition:Lcom/google/android/apps/books/render/SpreadIdentifier;
    invoke-static {v3, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/render/SpreadIdentifier;)Lcom/google/android/apps/books/render/SpreadIdentifier;

    .line 8605
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 8606
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMenu:Lcom/google/android/apps/books/app/ReaderMenu;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1700(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderMenu;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/app/ReaderMenu;->setPosition(Lcom/google/android/apps/books/common/Position;)V

    .line 8609
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateMenu()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$8300(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8611
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 8612
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 8613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mFirstPositionRenderTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-static {v3, v4}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19002(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/util/Logging$PerformanceTracker;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    .line 8616
    :cond_a
    if-eqz v12, :cond_b

    .line 8625
    const-wide/16 v10, 0x7d0

    .line 8627
    .local v10, "DELAY_MILLIS":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->getView()Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/books/app/ReaderFragment$41$1;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/books/app/ReaderFragment$41$1;-><init>(Lcom/google/android/apps/books/app/ReaderFragment$41;)V

    const-wide/16 v8, 0x7d0

    invoke-virtual {v3, v4, v8, v9}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 8639
    .end local v10    # "DELAY_MILLIS":J
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeRequestVolumeDownload()V
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19200(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8645
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/books/render/SpreadIdentifier;->position:Lcom/google/android/apps/books/common/Position;

    invoke-virtual {v3}, Lcom/google/android/apps/books/common/Position;->getPageId()Ljava/lang/String;

    move-result-object v13

    .line 8647
    .local v13, "pageId":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3, v13}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageIndex(Ljava/lang/String;)I

    move-result v14

    .line 8648
    .local v14, "pageIndex":I
    int-to-float v3, v14

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v8}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/apps/books/model/VolumeMetadata;->getPageCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-double v8, v3

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    cmpl-double v3, v8, v16

    if-lez v3, :cond_0

    .line 8649
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/books/app/HomeFragment;->sClosedBookInSecondHalf:Z
    :try_end_0
    .catch Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 8651
    .end local v14    # "pageIndex":I
    :catch_0
    move-exception v2

    .line 8652
    .local v2, "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    const-string v3, "ReaderFragment"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 8653
    const-string v3, "ReaderFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Couldn\'t find page index for page ID "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 8577
    .end local v2    # "e":Lcom/google/android/apps/books/model/VolumeMetadata$BadContentException;
    .end local v12    # "isFirstVisiblePosition":Z
    .end local v13    # "pageId":Ljava/lang/String;
    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 8584
    :cond_d
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/books/app/MoveType;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    .line 8601
    :cond_e
    const/4 v12, 0x0

    goto/16 :goto_4
.end method

.method public onSelectionChanged(ILcom/google/android/apps/books/app/SelectionState;)V
    .locals 2
    .param p1, "passageIndex"    # I
    .param p2, "state"    # Lcom/google/android/apps/books/app/SelectionState;

    .prologue
    .line 8804
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v0, p2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11902(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/SelectionState;)Lcom/google/android/apps/books/app/SelectionState;

    .line 8806
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->textSelectionChanged()V

    .line 8808
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/InfoCardsHelper;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/app/InfoCardsHelper;->setTextSelection(Lcom/google/android/apps/books/app/SelectionState;)V

    .line 8809
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onShowedCards(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19900(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 8811
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->firstAnnotationInSelection()Lcom/google/android/apps/books/annotations/Annotation;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/books/widget/PagesViewController;->setSelectedAnnotation(Lcom/google/android/apps/books/annotations/Annotation;I)V

    .line 8812
    return-void
.end method

.method public onSelectionUpdate()V
    .locals 1

    .prologue
    .line 8986
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8987
    return-void
.end method

.method public onStartedPageTurn()V
    .locals 2

    .prologue
    .line 8702
    const-string v0, "ReaderFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8703
    const-string v0, "ReaderFragment"

    const-string v1, "onStartedPageTurn()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 8705
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 8706
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMediaPlaybackCanceled:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12802(Lcom/google/android/apps/books/app/ReaderFragment;Z)Z

    .line 8708
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->clearMediaViews()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19400(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8710
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeExpireRental()Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$000(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    .line 8711
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeResetInfoCards()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$18500(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8712
    return-void
.end method

.method public onStartedSelection()V
    .locals 4

    .prologue
    .line 8794
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 8795
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;-><init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/Annotation;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper;->startTextSelection(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;)V

    .line 8798
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybeUpdateTtsController()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$3900(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8799
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->maybePauseMo(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19800(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 8800
    return-void
.end method

.method public onTurnedToEndOfBookPage()V
    .locals 3

    .prologue
    .line 8825
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    if-nez v1, :cond_1

    .line 8838
    :cond_0
    :goto_0
    return-void

    .line 8829
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->showEndOfBookCards(Z)V
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20100(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 8831
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mAccessibilityEnabled:Z
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$16800(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8832
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0f00b2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 8836
    .local v0, "eobMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/AccessibilityUtils;->announceText(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 8832
    .end local v0    # "eobMessage":Ljava/lang/String;
    :cond_2
    const v1, 0x7f0f00b1

    goto :goto_1
.end method

.method public onUserSelectedNewPosition()V
    .locals 1

    .prologue
    .line 8750
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onUserSelectedNewPosition()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11000(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8751
    return-void
.end method

.method public onVisibleDevicePagesChanged(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/DevicePageRendering;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 8725
    .local p1, "devicePages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/UnloadableEventual;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/util/UnloadableEventual;->onLoad(Ljava/lang/Object;)V

    .line 8726
    return-void
.end method

.method public prepareBookView(Lcom/google/android/apps/books/widget/BookView$Callbacks;Z)Lcom/google/android/apps/books/widget/BookView;
    .locals 6
    .param p1, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p2, "displayTwoPages"    # Z

    .prologue
    .line 8958
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 8959
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mReadingView:Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;

    move-result-object v0

    const v2, 0x7f0e00d5

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/widget/TransientInfoCardsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/BookView;

    # setter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9502(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/widget/BookView;)Lcom/google/android/apps/books/widget/BookView;

    .line 8960
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->createPagesViewOnHoverListener(Z)Landroid/view/View$OnHoverListener;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$6000(Lcom/google/android/apps/books/app/ReaderFragment;Z)Landroid/view/View$OnHoverListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/BookView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 8963
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0101a3

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getThemeColor(Landroid/content/Context;I)I

    move-result v5

    .line 8965
    .local v5, "navViewBgColor":I
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentMode:Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBackgroundColor:I
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20700(Lcom/google/android/apps/books/app/ReaderFragment;)I

    move-result v4

    move-object v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/widget/BookView;->prepare(Lcom/google/android/apps/books/model/VolumeManifest$Mode;Lcom/google/android/apps/books/widget/BookView$Callbacks;ZII)V

    .line 8967
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mBookView:Lcom/google/android/apps/books/widget/BookView;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/BookView;

    move-result-object v0

    return-object v0
.end method

.method public resetZoom()V
    .locals 1

    .prologue
    .line 8881
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->resetZoom()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$20200(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8882
    return-void
.end method

.method public searchNavigationEnabled()Z
    .locals 1

    .prologue
    .line 8891
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchModeActive:Z
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7900(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v0

    return v0
.end method

.method public setActionBarElevation(F)V
    .locals 1
    .param p1, "elevation"    # F

    .prologue
    .line 9058
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getCallbacks()Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$5900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$Callbacks;->setActionBarElevation(F)V

    .line 9059
    return-void
.end method

.method public setSearchBarMatchText(IIZ)V
    .locals 1
    .param p1, "numMatchesBeforeSpread"    # I
    .param p2, "numMatches"    # I
    .param p3, "enabled"    # Z

    .prologue
    .line 9048
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSearchScrubBar:Lcom/google/android/apps/books/widget/SearchScrubBar;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/SearchScrubBar;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/books/widget/SearchScrubBar;->setSearchBarMatchText(IIZ)V

    .line 9049
    return-void
.end method

.method public setSystemBarsVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 9053
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setSystemBarsVisible(Z)V
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$21000(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 9054
    return-void
.end method

.method public setTtsPassageText(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 6
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 9172
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v2, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->getTtsController(Z)Lcom/google/android/apps/books/tts/TextToSpeechController;
    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19700(Lcom/google/android/apps/books/app/ReaderFragment;Z)Lcom/google/android/apps/books/tts/TextToSpeechController;

    move-result-object v0

    .local v0, "tts":Lcom/google/android/apps/books/tts/TextToSpeechController;
    if-eqz v0, :cond_0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 9173
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/tts/TextToSpeechController;->setPassageText(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    .line 9176
    :cond_0
    return-void
.end method

.method public setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;

    .prologue
    const/4 v1, 0x0

    .line 8996
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->setUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V
    invoke-static {v0, p1, v1, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$4100(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;ZZ)V

    .line 8997
    return-void
.end method

.method public showCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 2
    .param p1, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 8874
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionUiHelper:Lcom/google/android/apps/books/app/SelectionUiHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$14100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionUiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper;->endActiveSelection()Z

    .line 8875
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mInfoCardsHelper:Lcom/google/android/apps/books/app/InfoCardsHelper;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1100(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/InfoCardsHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/InfoCardsHelper;->showCardsForAnnotation(Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 8876
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onShowedCards(Z)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$19900(Lcom/google/android/apps/books/app/ReaderFragment;Z)V

    .line 8877
    return-void
.end method

.method public showTableOfContents(I)V
    .locals 2
    .param p1, "tabIndex"    # I

    .prologue
    .line 8976
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TableOfContents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 8977
    new-instance v0, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LocalPreferences;->setLastOpenedTocTab(I)V

    .line 8978
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mTableOfContents:Lcom/google/android/apps/books/widget/TableOfContents;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$9000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/TableOfContents;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/TableOfContents;->show(Landroid/view/View;)V

    .line 8981
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$41;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->onInternalUserInteraction()V
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$10800(Lcom/google/android/apps/books/app/ReaderFragment;)V

    .line 8982
    return-void
.end method
