.class Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;
.super Ljava/lang/Object;
.source "PagesViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PagesViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CachedRenderResult"
.end annotation


# instance fields
.field final mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

.field mDecorator:Lcom/google/android/apps/books/render/PagePainter;

.field final mPageInfo:Lcom/google/android/apps/books/widget/DevicePageRendering;

.field final mPainter:Lcom/google/android/apps/books/render/PagePainter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/util/RenderRequestContext;Lcom/google/android/apps/books/render/PagePainter;Lcom/google/android/apps/books/render/PagePainter;)V
    .locals 0
    .param p1, "pageInfo"    # Lcom/google/android/apps/books/widget/DevicePageRendering;
    .param p2, "cookie"    # Lcom/google/android/apps/books/util/RenderRequestContext;
    .param p3, "painter"    # Lcom/google/android/apps/books/render/PagePainter;
    .param p4, "decorator"    # Lcom/google/android/apps/books/render/PagePainter;

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mCookie:Lcom/google/android/apps/books/util/RenderRequestContext;

    .line 393
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPageInfo:Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 394
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mPainter:Lcom/google/android/apps/books/render/PagePainter;

    .line 395
    iput-object p4, p0, Lcom/google/android/apps/books/widget/PagesViewController$CachedRenderResult;->mDecorator:Lcom/google/android/apps/books/render/PagePainter;

    .line 396
    return-void
.end method
