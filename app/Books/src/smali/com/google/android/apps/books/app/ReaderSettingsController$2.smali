.class Lcom/google/android/apps/books/app/ReaderSettingsController$2;
.super Ljava/lang/Object;
.source "ReaderSettingsController.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference$BrightnessChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderSettingsController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$2;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBrightnessChange(I)V
    .locals 2
    .param p1, "brightness"    # I

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$2;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onBrightnessChanged(IZ)V

    .line 412
    return-void
.end method

.method public onChange(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 402
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$2;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 403
    new-instance v1, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$2;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$100(Lcom/google/android/apps/books/app/ReaderSettingsController;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/apps/books/preference/LocalPreferences;->getBrightness()I

    move-result v0

    .line 405
    .local v0, "brightness":I
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController$2;->this$0:Lcom/google/android/apps/books/app/ReaderSettingsController;

    # getter for: Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onBrightnessChanged(IZ)V

    .line 407
    .end local v0    # "brightness":I
    :cond_0
    return-void
.end method
