.class public Lcom/google/android/apps/books/render/Renderer$PageRange;
.super Ljava/lang/Object;
.source "Renderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/Renderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PageRange"
.end annotation


# instance fields
.field public final centerPosition:Lcom/google/android/apps/books/render/PageIdentifier;

.field public final firstPageOffset:I

.field public final lastPageOffset:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/PageIdentifier;II)V
    .locals 2
    .param p1, "centerPosition"    # Lcom/google/android/apps/books/render/PageIdentifier;
    .param p2, "firstPageOffset"    # I
    .param p3, "lastPageOffset"    # I

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    iput-object p1, p0, Lcom/google/android/apps/books/render/Renderer$PageRange;->centerPosition:Lcom/google/android/apps/books/render/PageIdentifier;

    .line 300
    if-gt p2, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid page range"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 301
    iput p2, p0, Lcom/google/android/apps/books/render/Renderer$PageRange;->firstPageOffset:I

    .line 302
    iput p3, p0, Lcom/google/android/apps/books/render/Renderer$PageRange;->lastPageOffset:I

    .line 303
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 307
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "centerPosition"

    iget-object v2, p0, Lcom/google/android/apps/books/render/Renderer$PageRange;->centerPosition:Lcom/google/android/apps/books/render/PageIdentifier;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "firstPageOffset"

    iget v2, p0, Lcom/google/android/apps/books/render/Renderer$PageRange;->firstPageOffset:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "lastPageOffset"

    iget v2, p0, Lcom/google/android/apps/books/render/Renderer$PageRange;->lastPageOffset:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
