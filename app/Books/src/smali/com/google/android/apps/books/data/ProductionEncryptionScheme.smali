.class public Lcom/google/android/apps/books/data/ProductionEncryptionScheme;
.super Ljava/lang/Object;
.source "ProductionEncryptionScheme.java"

# interfaces
.implements Lcom/google/android/apps/books/data/EncryptionScheme;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decrypt(Ljava/io/InputStream;Lcom/google/android/apps/books/model/SessionKey;)Ljava/io/InputStream;
    .locals 2
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/books/util/EncryptionUtils$WrongRootKeyException;,
            Ljava/security/GeneralSecurityException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p2, Lcom/google/android/apps/books/model/SessionKey;->encryptedKey:[B

    iget-object v1, p2, Lcom/google/android/apps/books/model/SessionKey;->version:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/util/EncryptionUtils;->D_s(Ljava/io/InputStream;[BLjava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method
