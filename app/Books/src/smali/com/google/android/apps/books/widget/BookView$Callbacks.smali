.class public interface abstract Lcom/google/android/apps/books/widget/BookView$Callbacks;
.super Ljava/lang/Object;
.source "BookView.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/OnPageTouchListener;
.implements Lcom/google/android/apps/books/widget/PagesDisplay$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BookView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callbacks"
.end annotation


# virtual methods
.method public abstract addRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V
.end method

.method public abstract getBookmarkController()Lcom/google/android/apps/books/app/BookmarkController;
.end method

.method public abstract getTheme()Ljava/lang/String;
.end method

.method public abstract importPage(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/PagePainter;)V
.end method

.method public abstract isScrubbing()Z
.end method

.method public abstract onBeganTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
.end method

.method public abstract onFinishedTransitionToUiMode(Lcom/google/android/apps/books/app/ReaderFragment$ReadingUiMode;)V
.end method

.method public abstract onNavScroll(Lcom/google/android/apps/books/render/PageHandle;)V
.end method

.method public abstract onNavScrollProgress(Lcom/google/android/apps/books/render/PageHandle;F)V
.end method

.method public abstract onScrollStateChanged(I)V
.end method

.method public abstract removeRendererListener(Lcom/google/android/apps/books/render/RendererListener;)V
.end method

.method public abstract requestRenderPage(Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;Lcom/google/android/apps/books/widget/PagesViewController$PvcRenderResponseConsumer;)V
.end method

.method public abstract setActionBarElevation(F)V
.end method

.method public abstract setSystemBarsVisible(Z)V
.end method

.method public abstract showSpreadInFullView(Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/app/MoveType;Z)V
.end method

.method public abstract showTableOfContents(I)V
.end method
