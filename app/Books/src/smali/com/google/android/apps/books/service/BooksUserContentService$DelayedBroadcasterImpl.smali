.class public Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;
.super Ljava/lang/Object;
.source "BooksUserContentService.java"

# interfaces
.implements Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/BooksUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DelayedBroadcasterImpl"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mHasNotifications:Z

.field private final mNotificationBlocks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379
    invoke-static {}, Lcom/google/android/apps/books/util/CollectionUtils;->newWeakSet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mNotificationBlocks:Ljava/util/Set;

    .line 382
    iput-object p1, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mContext:Landroid/content/Context;

    .line 383
    return-void
.end method

.method private declared-synchronized maybeNotifyContentChanged(Z)V
    .locals 1
    .param p1, "overrideBlocks"    # Z

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mHasNotifications:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mNotificationBlocks:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 424
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->sendNotification()V

    .line 425
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mHasNotifications:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    :cond_1
    monitor-exit p0

    return-void

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized addNotificationBlock(Ljava/lang/Object;)V
    .locals 3
    .param p1, "block"    # Ljava/lang/Object;

    .prologue
    .line 398
    monitor-enter p0

    :try_start_0
    const-string v0, "BooksUserContent"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    const-string v0, "BooksUserContent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addNotificationBlock: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mNotificationBlocks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    monitor-exit p0

    return-void

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized flushNotifications()V
    .locals 2

    .prologue
    .line 416
    monitor-enter p0

    :try_start_0
    const-string v0, "BooksUserContent"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "BooksUserContent"

    const-string v1, "flushNotifications"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->maybeNotifyContentChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    monitor-exit p0

    return-void

    .line 416
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized notifyContentChanged()V
    .locals 2

    .prologue
    .line 387
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mHasNotifications:Z

    .line 389
    const-string v0, "BooksUserContent"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    const-string v0, "BooksUserContent"

    const-string v1, "notifyContentChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->maybeNotifyContentChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    monitor-exit p0

    return-void

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeNotificationBlock(Ljava/lang/Object;)V
    .locals 3
    .param p1, "block"    # Ljava/lang/Object;

    .prologue
    .line 406
    monitor-enter p0

    :try_start_0
    const-string v0, "BooksUserContent"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const-string v0, "BooksUserContent"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeNotificationBlock: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mNotificationBlocks:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 411
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->maybeNotifyContentChanged(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    monitor-exit p0

    return-void

    .line 406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected sendNotification()V
    .locals 3

    .prologue
    .line 430
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.play.CONTENT_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 431
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "Play.DataType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 433
    const-string v1, "Play.BackendId"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 435
    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcasterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 437
    const-string v1, "BooksUserContent"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    const-string v1, "BooksUserContent"

    const-string v2, "Sending notification to widget"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_0
    return-void
.end method
