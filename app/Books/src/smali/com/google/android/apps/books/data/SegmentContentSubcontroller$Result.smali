.class Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;
.super Ljava/lang/Object;
.source "SegmentContentSubcontroller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/SegmentContentSubcontroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Result"
.end annotation


# instance fields
.field final blob:Lcom/google/android/apps/books/data/DataControllerBlob;

.field final resources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field final sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/List;)V
    .locals 0
    .param p1, "blob"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/DataControllerBlob;",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "sessionKey":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<*>;"
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->blob:Lcom/google/android/apps/books/data/DataControllerBlob;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    .line 67
    iput-object p3, p0, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;->resources:Ljava/util/List;

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/List;Lcom/google/android/apps/books/data/SegmentContentSubcontroller$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/data/DataControllerBlob;
    .param p2, "x1"    # Lcom/google/android/apps/books/model/LocalSessionKey;
    .param p3, "x2"    # Ljava/util/List;
    .param p4, "x3"    # Lcom/google/android/apps/books/data/SegmentContentSubcontroller$1;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/data/SegmentContentSubcontroller$Result;-><init>(Lcom/google/android/apps/books/data/DataControllerBlob;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/util/List;)V

    return-void
.end method
