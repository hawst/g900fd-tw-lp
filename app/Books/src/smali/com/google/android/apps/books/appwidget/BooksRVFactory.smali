.class Lcom/google/android/apps/books/appwidget/BooksRVFactory;
.super Ljava/lang/Object;
.source "BooksAppWidgetService.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private final mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mAppContext:Landroid/content/Context;

.field private final mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private final mCollectionObserver:Landroid/database/ContentObserver;

.field private final mMediaMountedReceiver:Landroid/content/BroadcastReceiver;

.field private final mResolver:Landroid/content/ContentResolver;

.field private mTargetHeight:I

.field private mVolumeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    .line 74
    new-instance v0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory$1;-><init>(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mMediaMountedReceiver:Landroid/content/BroadcastReceiver;

    .line 86
    new-instance v0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/appwidget/BooksRVFactory$2;-><init>(Lcom/google/android/apps/books/appwidget/BooksRVFactory;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mCollectionObserver:Landroid/database/ContentObserver;

    .line 129
    new-instance v0, Lcom/google/android/apps/books/appwidget/BooksRVFactory$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory$3;-><init>(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 350
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mTargetHeight:I

    .line 159
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mResolver:Landroid/content/ContentResolver;

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->update()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/appwidget/BooksRVFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadVolumeList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/appwidget/BooksRVFactory;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/appwidget/BooksRVFactory;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadAccountFromPreferences()V

    return-void
.end method

.method private forceEmptyToLoading()V
    .locals 4

    .prologue
    const v3, 0x7f0e00b5

    .line 227
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040029

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 229
    .local v0, "rv":Landroid/widget/RemoteViews;
    const v1, 0x7f0e00b7

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    .line 230
    const v1, 0x7f0e00b6

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 231
    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 232
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getAppWidgetIds()[I

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;->updateWidgets([ILandroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 233
    return-void
.end method

.method private forceEmptyToStatic(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/CharSequence;

    .prologue
    const v3, 0x7f0e00b6

    .line 240
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040029

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 242
    .local v0, "rv":Landroid/widget/RemoteViews;
    const v1, 0x7f0e00b7

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    .line 243
    const v1, 0x1020014

    invoke-virtual {v0, v1, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 244
    const v1, 0x7f0e00b5

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 245
    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 246
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getAppWidgetIds()[I

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;->updateWidgets([ILandroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 247
    return-void
.end method

.method private getAppWidgetIds()[I
    .locals 4

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/books/appwidget/BooksAppWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    return-object v0
.end method

.method private getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    return-object v0
.end method

.method private getTargetHeight()I
    .locals 8

    .prologue
    .line 370
    iget v4, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mTargetHeight:I

    if-nez v4, :cond_0

    .line 371
    iget-object v4, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 372
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    .line 373
    .local v0, "density":F
    const v4, 0x7f090122

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 374
    .local v2, "pixelHeight":F
    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    .line 377
    float-to-double v4, v0

    const-wide v6, 0x3fe428f5c28f5c29L    # 0.63

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    .line 378
    .local v1, "factor":F
    mul-float v4, v2, v1

    div-float/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mTargetHeight:I

    .line 383
    .end local v1    # "factor":F
    :goto_0
    const-string v4, "BooksAppWidget"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 384
    const-string v4, "BooksAppWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Widget Target Height "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mTargetHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    .end local v0    # "density":F
    .end local v2    # "pixelHeight":F
    .end local v3    # "res":Landroid/content/res/Resources;
    :cond_0
    iget v4, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mTargetHeight:I

    return v4

    .line 381
    .restart local v0    # "density":F
    .restart local v2    # "pixelHeight":F
    .restart local v3    # "res":Landroid/content/res/Resources;
    :cond_1
    float-to-int v4, v2

    iput v4, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mTargetHeight:I

    goto :goto_0
.end method

.method private getViewAtInternal(I)Landroid/widget/RemoteViews;
    .locals 14
    .param p1, "position"    # I

    .prologue
    .line 391
    const-string v11, "BooksAppWidget"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 392
    const-string v11, "BooksAppWidget"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getViewAtInternal() at position="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :cond_0
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lt p1, v11, :cond_3

    .line 398
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getLoadingView()Landroid/widget/RemoteViews;

    move-result-object v6

    .line 478
    :cond_2
    :goto_0
    return-object v6

    .line 401
    :cond_3
    const/4 v5, 0x0

    .line 402
    .local v5, "inStream":Ljava/io/InputStream;
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    invoke-interface {v11, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/model/VolumeData;

    .line 406
    .local v9, "volume":Lcom/google/android/apps/books/model/VolumeData;
    :try_start_0
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 407
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    iget-object v12, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-static {v11, v12}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    .line 409
    .local v1, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    sget-object v11, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v1, v9, v11}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 413
    .end local v1    # "dc":Lcom/google/android/apps/books/data/BooksDataController;
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v11

    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/google/android/apps/books/model/BooksDataStore;->isVolumeCoverLocal(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getLoadingView()Landroid/widget/RemoteViews;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 468
    if-eqz v5, :cond_2

    .line 469
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 471
    :catch_0
    move-exception v11

    goto :goto_0

    .line 418
    :cond_5
    :try_start_2
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 419
    .local v3, "factory":Landroid/graphics/BitmapFactory$Options;
    sget-object v11, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v11, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 421
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildCoverUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 422
    .local v10, "volumeCoverUri":Landroid/net/Uri;
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v11, v10}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    .line 423
    const/4 v11, 0x0

    invoke-static {v5, v11, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 424
    .local v0, "coverBitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_6

    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getLoadingView()Landroid/widget/RemoteViews;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 468
    if-eqz v5, :cond_2

    .line 469
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 471
    :catch_1
    move-exception v11

    goto :goto_0

    .line 429
    :cond_6
    :try_start_4
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f040028

    invoke-direct {v6, v11, v12}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 431
    .local v6, "rv":Landroid/widget/RemoteViews;
    const v12, 0x7f0e00b4

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->isSample(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v11, 0x0

    :goto_1
    invoke-virtual {v6, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 433
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getTargetHeight()I

    move-result v8

    .line 434
    .local v8, "targetHeight":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-le v11, v8, :cond_9

    .line 435
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    mul-int/2addr v11, v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    div-int/2addr v11, v12

    const/4 v12, 0x1

    invoke-static {v0, v11, v8, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 438
    .local v7, "smallerBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 439
    const v11, 0x7f0e00b3

    invoke-virtual {v6, v11, v7}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 443
    .end local v7    # "smallerBitmap":Landroid/graphics/Bitmap;
    :goto_2
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnIcsMR1OrLater()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 451
    const v11, 0x7f0e00b3

    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v11, v12}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 453
    :cond_7
    iget-object v11, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-interface {v9}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v11, v12, v13}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 455
    .local v4, "fillInIntent":Landroid/content/Intent;
    const v11, 0x7f0e00b2

    invoke-virtual {v6, v11, v4}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 468
    if-eqz v5, :cond_2

    .line 469
    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 471
    :catch_2
    move-exception v11

    goto/16 :goto_0

    .line 431
    .end local v4    # "fillInIntent":Landroid/content/Intent;
    .end local v8    # "targetHeight":I
    :cond_8
    const/16 v11, 0x8

    goto :goto_1

    .line 441
    .restart local v8    # "targetHeight":I
    :cond_9
    const v11, 0x7f0e00b3

    :try_start_6
    invoke-virtual {v6, v11, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 458
    .end local v0    # "coverBitmap":Landroid/graphics/Bitmap;
    .end local v3    # "factory":Landroid/graphics/BitmapFactory$Options;
    .end local v6    # "rv":Landroid/widget/RemoteViews;
    .end local v8    # "targetHeight":I
    .end local v10    # "volumeCoverUri":Landroid/net/Uri;
    :catch_3
    move-exception v2

    .line 459
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_7
    const-string v11, "BooksAppWidget"

    const/4 v12, 0x5

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 460
    const-string v11, "BooksAppWidget"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "problem finding cover: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 468
    :cond_a
    if-eqz v5, :cond_b

    .line 469
    :try_start_8
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 478
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :cond_b
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getLoadingView()Landroid/widget/RemoteViews;

    move-result-object v6

    goto/16 :goto_0

    .line 462
    :catch_4
    move-exception v2

    .line 463
    .local v2, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v11, "BooksAppWidget"

    const/4 v12, 0x5

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 464
    const-string v11, "BooksAppWidget"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "problem reading cover: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 468
    :cond_c
    if-eqz v5, :cond_b

    .line 469
    :try_start_a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    goto :goto_3

    .line 471
    :catch_5
    move-exception v11

    goto :goto_3

    .line 467
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    .line 468
    if-eqz v5, :cond_d

    .line 469
    :try_start_b
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 473
    :cond_d
    :goto_4
    throw v11

    .line 471
    .local v2, "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v11

    goto :goto_3

    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_7
    move-exception v12

    goto :goto_4
.end method

.method private isSample(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 5
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 482
    if-nez p1, :cond_0

    .line 483
    const/4 v4, 0x0

    .line 490
    :goto_0
    return v4

    .line 486
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getViewability()Ljava/lang/String;

    move-result-object v3

    .line 487
    .local v3, "viewability":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getOldStyleOpenAccessValue()Ljava/lang/String;

    move-result-object v2

    .line 488
    .local v2, "openAccess":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getBuyUrl()Ljava/lang/String;

    move-result-object v1

    .line 489
    .local v1, "buyUrl":Ljava/lang/String;
    invoke-static {v3, v2, v1}, Lcom/google/android/apps/books/model/VolumeData$Access;->getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v0

    .line 490
    .local v0, "access":Lcom/google/android/apps/books/model/VolumeData$Access;
    sget-object v4, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_0
.end method

.method private loadAccountFromPreferences()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 254
    iget-object v4, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    .line 256
    .local v4, "originalAccount":Landroid/accounts/Account;
    const/4 v3, 0x1

    .line 257
    .local v3, "defaultToAny":Z
    iget-object v8, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    const/4 v9, 0x0

    invoke-static {v8, v9, v6}, Lcom/google/android/apps/books/util/AccountUtils;->findIntentAccount(Landroid/content/Context;Landroid/accounts/Account;Z)Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;

    move-result-object v1

    .line 260
    .local v1, "accountResult":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    iget-object v8, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    iput-object v8, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    .line 264
    iget-boolean v8, v1, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    if-nez v8, :cond_0

    .line 265
    new-instance v8, Lcom/google/android/apps/books/preference/LocalPreferences;

    iget-object v9, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    iget-object v9, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 269
    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mResolver:Landroid/content/ContentResolver;

    iget-object v9, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mCollectionObserver:Landroid/database/ContentObserver;

    invoke-virtual {v8, v9}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 271
    iget-object v8, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-static {v8, v4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    move v0, v6

    .line 272
    .local v0, "accountChanged":Z
    :goto_0
    iget-object v8, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    if-eqz v8, :cond_4

    move v5, v6

    .line 273
    .local v5, "validAccount":Z
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    .line 274
    const-string v7, "BooksAppWidget"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 275
    const-string v7, "BooksAppWidget"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadAccountFromPreferences() found new account "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->myEBooksDirUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 281
    .local v2, "bookCollectionUri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mResolver:Landroid/content/ContentResolver;

    iget-object v8, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mCollectionObserver:Landroid/database/ContentObserver;

    invoke-virtual {v7, v2, v6, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 283
    .end local v2    # "bookCollectionUri":Landroid/net/Uri;
    :cond_2
    return-void

    .end local v0    # "accountChanged":Z
    .end local v5    # "validAccount":Z
    :cond_3
    move v0, v7

    .line 271
    goto :goto_0

    .restart local v0    # "accountChanged":Z
    :cond_4
    move v5, v7

    .line 272
    goto :goto_1
.end method

.method private loadVolumeList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 290
    .local v0, "token":J
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadVolumeListInternal()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 292
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method private loadVolumeListInternal()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 297
    const-string v1, "BooksAppWidget"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    const-string v1, "BooksAppWidget"

    const-string v2, "loadVolumeListInternal()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    const/16 v2, 0x14

    invoke-interface {v1, v2, v3, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getMyEbooksVolumesRange(ILjava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 303
    .local v0, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    return-object v0
.end method

.method private update()V
    .locals 3

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getAppWidgetIds()[I

    move-result-object v1

    const v2, 0x7f0e00b7

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 156
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 308
    const-string v0, "BooksAppWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    const-string v0, "BooksAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCount() found size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 316
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 331
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f040028

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 333
    .local v0, "rv":Landroid/widget/RemoteViews;
    const v1, 0x7f0e00b3

    const v2, 0x7f0b00c7

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 334
    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 341
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 343
    .local v0, "token":J
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->getViewAtInternal(I)Landroid/widget/RemoteViews;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 345
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 321
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 326
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 168
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadAccountFromPreferences()V

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, v2}, Lcom/google/android/apps/books/app/BooksApplication;->getSyncController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/SyncController;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/sync/SyncController;->requestManualSync()V

    .line 176
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 177
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mMediaMountedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 179
    return-void
.end method

.method public onDataSetChanged()V
    .locals 3

    .prologue
    .line 194
    const-string v1, "BooksAppWidget"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    const-string v1, "BooksAppWidget"

    const-string v2, "onDataSetChanged()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_3

    .line 200
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->forceEmptyToLoading()V

    .line 202
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->loadVolumeList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    const v2, 0x7f0f0058

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->forceEmptyToStatic(Ljava/lang/CharSequence;)V

    .line 219
    :cond_2
    :goto_1
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BooksAppWidget"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205
    const-string v1, "BooksAppWidget"

    const-string v2, "Exception loading volume ids"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 216
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mVolumeList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    const v2, 0x7f0f0104

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->forceEmptyToStatic(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAccountListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mCollectionObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/books/appwidget/BooksRVFactory;->mMediaMountedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 190
    return-void
.end method
