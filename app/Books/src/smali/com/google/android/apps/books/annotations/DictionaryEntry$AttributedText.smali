.class public Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;
.super Ljava/lang/Object;
.source "DictionaryEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/DictionaryEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AttributedText"
.end annotation


# instance fields
.field public final attribution:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

.field public final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attribution"    # Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;->text:Ljava/lang/String;

    .line 96
    iput-object p2, p0, Lcom/google/android/apps/books/annotations/DictionaryEntry$AttributedText;->attribution:Lcom/google/android/apps/books/annotations/DictionaryEntry$Attribution;

    .line 97
    return-void
.end method
