.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->uiEdit(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

.field final synthetic val$newValue:Lcom/google/android/apps/books/annotations/Annotation;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;->val$newValue:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v0, v0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    # getter for: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->mAnnotationTable:Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;
    invoke-static {v0}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$200(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;)Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;->this$1:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;

    iget-object v1, v1, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$4$3;->val$newValue:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/annotations/LocalAnnotationDatabase;->markEditedOnClient(Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/Annotation;)V

    .line 281
    return-void
.end method
