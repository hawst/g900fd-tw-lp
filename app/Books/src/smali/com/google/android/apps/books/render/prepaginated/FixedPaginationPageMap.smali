.class public Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
.super Ljava/lang/Object;
.source "FixedPaginationPageMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;
    }
.end annotation


# instance fields
.field private final mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/render/prepaginated/SequenceBimap",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            "Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;",
            ">;"
        }
    .end annotation
.end field

.field private final mLastViewableBookPageIndex:I

.field private final mTwoPageSpreads:Z


# direct methods
.method public constructor <init>(Ljava/util/List;ZZ)V
    .locals 9
    .param p2, "twoPageSpreads"    # Z
    .param p3, "coalesceGaps"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .local p1, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v5, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-direct {v5}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    .line 38
    iput-boolean p2, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mTwoPageSpreads:Z

    .line 40
    if-eqz p2, :cond_0

    .line 41
    sget-object v5, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-direct {p0, v7, v5, v6}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    .line 44
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 45
    .local v0, "bookPageCount":I
    if-eqz p3, :cond_9

    .line 48
    const/4 v2, 0x0

    .line 50
    .local v2, "currentSpanSize":I
    const/4 v3, 0x0

    .line 51
    .local v3, "inGap":Ljava/lang/Boolean;
    const/4 v1, 0x0

    .local v1, "bookPageIndex":I
    :goto_0
    if-ge v1, v0, :cond_6

    .line 52
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/model/Page;

    invoke-interface {v5}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v4

    .line 53
    .local v4, "thisPageViewable":Z
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-ne v4, v5, :cond_5

    .line 55
    :cond_1
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v3, v5, :cond_3

    .line 56
    sget-object v5, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->GAP:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-direct {p0, v2, v5, v6}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    .line 60
    :cond_2
    :goto_1
    if-nez v4, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 61
    const/4 v2, 0x1

    .line 51
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_3
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v3, v5, :cond_2

    .line 58
    invoke-direct {p0, v2, v8, v2}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    goto :goto_1

    :cond_4
    move v5, v7

    .line 60
    goto :goto_2

    .line 63
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 68
    .end local v4    # "thisPageViewable":Z
    :cond_6
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne v3, v5, :cond_7

    .line 69
    invoke-direct {p0, v2, v8, v2}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    .line 75
    .end local v1    # "bookPageIndex":I
    .end local v2    # "currentSpanSize":I
    .end local v3    # "inGap":Ljava/lang/Boolean;
    :cond_7
    :goto_4
    iget-object v5, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getLeftSize()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mLastViewableBookPageIndex:I

    .line 77
    if-eqz p2, :cond_8

    iget-object v5, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v5}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getRightSize()I

    move-result v5

    rem-int/lit8 v5, v5, 0x2

    if-ne v5, v6, :cond_8

    .line 79
    sget-object v5, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;->BLANK:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    invoke-direct {p0, v7, v5, v6}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    .line 81
    :cond_8
    return-void

    .line 72
    :cond_9
    invoke-direct {p0, v0, v8, v0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V

    goto :goto_4
.end method

.method private addSpan(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;I)V
    .locals 2
    .param p1, "bookPageLength"    # I
    .param p2, "specialPageDisplayType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p3, "screenPageLength"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    sget-object v1, Lcom/google/android/apps/books/util/Nothing;->NOTHING:Lcom/google/android/apps/books/util/Nothing;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->addSpan(Ljava/lang/Object;ILjava/lang/Object;I)V

    .line 87
    return-void
.end method


# virtual methods
.method public bookPageIndexToContent(IILcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;)Z
    .locals 3
    .param p1, "bookPageIndex"    # I
    .param p2, "screenPageOffset"    # I
    .param p3, "outContent"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    .prologue
    .line 152
    iget-object v2, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v2}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getLeftSize()I

    move-result v2

    if-lt p1, v2, :cond_0

    .line 153
    const/4 v2, 0x0

    .line 157
    :goto_0
    return v2

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v1

    .line 156
    .local v1, "screenPageIndex":I
    add-int v0, v1, p2

    .line 157
    .local v0, "offsetScreenPageIndex":I
    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->screenPageIndexToContent(ILcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;)Z

    move-result v2

    goto :goto_0
.end method

.method public bookPageIndexToScreenPageIndex(I)I
    .locals 1
    .param p1, "bookPageIndex"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v0

    return v0
.end method

.method public getLastViewableBookPageIndex()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mLastViewableBookPageIndex:I

    return v0
.end method

.method public getPagesPerSpread()I
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mTwoPageSpreads:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScreenPageIndexOfFirstBookPage()I
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v0

    return v0
.end method

.method public getScreenPageIndexOfLastViewableBookPage()I
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->getLastViewableBookPageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->leftIndexToRightIndex(I)I

    move-result v0

    return v0
.end method

.method public screenPageIndexToContent(ILcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;)Z
    .locals 2
    .param p1, "screenPageIndex"    # I
    .param p2, "outContent"    # Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    .prologue
    .line 131
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->getRightSize()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 132
    :cond_0
    const/4 v1, 0x0

    .line 140
    :goto_0
    return v1

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->rightIndexToValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 135
    .local v0, "specialPage":Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    if-eqz v0, :cond_2

    .line 136
    invoke-virtual {p2, v0}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->set(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    .line 140
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 138
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;->mBookToScreenMap:Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/render/prepaginated/SequenceBimap;->rightIndexToLeftIndex(I)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;->set(I)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap$ScreenPageContent;

    goto :goto_1
.end method
