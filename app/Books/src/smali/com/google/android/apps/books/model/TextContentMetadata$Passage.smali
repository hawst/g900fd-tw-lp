.class Lcom/google/android/apps/books/model/TextContentMetadata$Passage;
.super Ljava/lang/Object;
.source "TextContentMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/TextContentMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Passage"
.end annotation


# instance fields
.field final mCssIndices:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mSegmentIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final mStartPosition:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;)V
    .locals 0
    .param p3, "startPosition"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "segmentIndices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p2, "cssIndices":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->mSegmentIndices:Ljava/util/List;

    .line 133
    iput-object p2, p0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->mCssIndices:Ljava/util/Collection;

    .line 134
    iput-object p3, p0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->mStartPosition:Ljava/lang/String;

    .line 135
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;Lcom/google/android/apps/books/model/TextContentMetadata$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Ljava/util/Collection;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lcom/google/android/apps/books/model/TextContentMetadata$1;

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;-><init>(Ljava/util/List;Ljava/util/Collection;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->isViewable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getSegmentIndices()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getCssIndices()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/model/TextContentMetadata$Passage;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/TextContentMetadata$Passage;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getStartPosition()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCssIndices()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->mCssIndices:Ljava/util/Collection;

    return-object v0
.end method

.method private getSegmentIndices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->mSegmentIndices:Ljava/util/List;

    return-object v0
.end method

.method private getStartPosition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->mStartPosition:Ljava/lang/String;

    return-object v0
.end method

.method private isViewable()Z
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/android/apps/books/model/TextContentMetadata$Passage;->getSegmentIndices()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
