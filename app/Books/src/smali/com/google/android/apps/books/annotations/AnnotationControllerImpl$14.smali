.class Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;
.super Ljava/lang/Object;
.source "AnnotationControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->inBackgroundFollowedBySync(Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

.field final synthetic val$layersToSync:Ljava/util/List;

.field final synthetic val$runnable:Ljava/lang/Runnable;

.field final synthetic val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/lang/Runnable;Lcom/google/android/apps/books/annotations/VolumeVersion;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 739
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$runnable:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    iput-object p4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$layersToSync:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 742
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$runnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 743
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$runnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 746
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->this$0:Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/books/annotations/VolumeVersion;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$vv:Lcom/google/android/apps/books/annotations/VolumeVersion;

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl$14;->val$layersToSync:Ljava/util/List;

    # invokes: Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->syncOnBackgroundThread(Ljava/util/List;Ljava/util/Collection;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;->access$000(Lcom/google/android/apps/books/annotations/AnnotationControllerImpl;Ljava/util/List;Ljava/util/Collection;)V
    :try_end_0
    .catch Lcom/google/android/apps/books/annotations/AnnotationServerErrorResponseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 752
    :cond_1
    :goto_0
    return-void

    .line 747
    :catch_0
    move-exception v0

    .line 748
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const-string v1, "AnnotationC"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 749
    const-string v1, "AnnotationC"

    const-string v2, "Error during sync"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 747
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method
