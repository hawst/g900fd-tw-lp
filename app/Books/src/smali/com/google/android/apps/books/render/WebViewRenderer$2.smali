.class Lcom/google/android/apps/books/render/WebViewRenderer$2;
.super Lcom/google/android/apps/books/render/FetchingDataSource;
.source "WebViewRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/WebViewRenderer;-><init>(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/ReaderSettings;Lcom/google/android/apps/books/util/Logger;Landroid/graphics/Point;ZILandroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/render/WebViewRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/WebViewRenderer;Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/util/Logger;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Landroid/accounts/Account;
    .param p4, "x2"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p5, "x3"    # Lcom/google/android/apps/books/util/Logger;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/apps/books/render/WebViewRenderer$2;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/apps/books/render/FetchingDataSource;-><init>(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/util/Logger;)V

    return-void
.end method


# virtual methods
.method public getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "sectionId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation

    .prologue
    .line 209
    iget-object v2, p0, Lcom/google/android/apps/books/render/WebViewRenderer$2;->this$0:Lcom/google/android/apps/books/render/WebViewRenderer;

    # getter for: Lcom/google/android/apps/books/render/WebViewRenderer;->mLogger:Lcom/google/android/apps/books/util/Logger;
    invoke-static {v2}, Lcom/google/android/apps/books/render/WebViewRenderer;->access$200(Lcom/google/android/apps/books/render/WebViewRenderer;)Lcom/google/android/apps/books/util/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "load segment "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v1

    .line 211
    .local v1, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/books/render/FetchingDataSource;->getSegmentContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "result":Ljava/lang/String;
    invoke-interface {v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 213
    return-object v0
.end method
