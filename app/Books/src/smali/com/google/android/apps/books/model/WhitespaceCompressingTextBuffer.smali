.class public Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;
.super Ljava/lang/Object;
.source "WhitespaceCompressingTextBuffer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$1;,
        Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;
    }
.end annotation


# instance fields
.field private final mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

.field private mHeadOffset:I

.field private mLastCalculatedOffset:I

.field private mLastCalculatedOffsetNormalizedOffset:I

.field private final mOffsetToPosition:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mProcessedOffset:I

.field private mTailOffset:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;-><init>(Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$1;)V

    iput-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    .line 87
    invoke-static {}, Lcom/google/common/collect/Maps;->newTreeMap()Ljava/util/TreeMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    .line 90
    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mHeadOffset:I

    .line 97
    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    .line 100
    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mTailOffset:I

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffset:I

    .line 130
    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffsetNormalizedOffset:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mHeadOffset:I

    return v0
.end method


# virtual methods
.method public addPosition(Ljava/lang/String;)V
    .locals 2
    .param p1, "readingPosition"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    iget v1, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mTailOffset:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method

.method public addText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mTailOffset:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mTailOffset:I

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->append(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public buildCompressedSentenceAndOffsetToPositionMap(Ljava/util/TreeMap;I)Ljava/lang/String;
    .locals 9
    .param p2, "uncompressedLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 265
    .local p1, "map":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    .local v2, "compressedString":Ljava/lang/StringBuilder;
    const/4 v5, 0x1

    .line 269
    .local v5, "shouldTrimNextSpace":Z
    iget-object v7, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    iget v8, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/TreeMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v4

    .line 271
    .local v4, "nextOffsetAndPosition":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 272
    .local v1, "compressedIdx":I
    iget v6, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    .line 273
    .local v6, "uncompressedIdx":I
    :goto_0
    iget v7, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    add-int/2addr v7, p2

    if-ge v6, v7, :cond_4

    .line 274
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 276
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object v7, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    add-int/lit8 v8, v6, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/TreeMap;->ceilingEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v4

    .line 280
    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    invoke-virtual {v7, v6}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->charAt(I)C

    move-result v0

    .line 281
    .local v0, "c":C
    invoke-static {v0}, Lcom/google/android/apps/books/util/ReaderUtils;->isWhiteSpace(C)Z

    move-result v3

    .line 282
    .local v3, "isWhitespace":Z
    if-eqz v3, :cond_1

    if-nez v5, :cond_3

    .line 284
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 285
    if-eqz v3, :cond_2

    const/16 v0, 0x20

    .end local v0    # "c":C
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 287
    :cond_3
    move v5, v3

    .line 273
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 289
    .end local v3    # "isWhitespace":Z
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public getProcessedTextOffsetTextLocation()Lcom/google/android/apps/books/annotations/TextLocation;
    .locals 6

    .prologue
    .line 134
    iget-object v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    iget v5, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    .line 136
    .local v3, "precedingPositionEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 137
    .local v1, "lastPositionOffset":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 141
    .local v2, "normalizedOffset":I
    iget v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffset:I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 142
    iget v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 143
    iget v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffsetNormalizedOffset:I

    .line 146
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    if-ge v0, v4, :cond_2

    .line 147
    iget-object v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->charAt(I)C

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/ReaderUtils;->isWhiteSpace(C)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    :goto_1
    add-int/2addr v2, v4

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 147
    :cond_1
    const/4 v4, 0x1

    goto :goto_1

    .line 150
    :cond_2
    iget v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    iput v4, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffset:I

    .line 151
    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mLastCalculatedOffsetNormalizedOffset:I

    .line 153
    new-instance v5, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v5, v4, v2}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Ljava/lang/String;I)V

    return-object v5
.end method

.method public getUnprocessed()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    iget v1, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnprocessed(I)Ljava/lang/String;
    .locals 3
    .param p1, "maxChars"    # I

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    iget v1, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    iget v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mTailOffset:I

    sub-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onProcessedText(I)V
    .locals 5
    .param p1, "length"    # I

    .prologue
    .line 165
    iget v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    .line 166
    iget-object v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    iget v3, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mProcessedOffset:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 169
    .local v1, "lowestNecessaryPositionIndex":Ljava/lang/Integer;
    iget v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mHeadOffset:I

    iget-object v3, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mBuffer:Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer$SlidingWindow;->delete(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mHeadOffset:I

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 173
    .local v0, "agedOutPosition":Ljava/lang/Integer;
    :goto_0
    if-eqz v0, :cond_0

    .line 174
    iget-object v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    invoke-virtual {v2, v0}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->mOffsetToPosition:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->floorKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "agedOutPosition":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .restart local v0    # "agedOutPosition":Ljava/lang/Integer;
    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method public textRangesWithinSentence(Ljava/lang/String;Ljava/util/regex/Matcher;Ljava/util/List;Ljava/util/TreeMap;)V
    .locals 20
    .param p1, "compressedSentence"    # Ljava/lang/String;
    .param p2, "matcher"    # Ljava/util/regex/Matcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/regex/Matcher;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p3, "matchRanges":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/TextLocationRange;>;"
    .local p4, "compressedSentenceOffsetToPosition":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/4 v7, -0x1

    .line 195
    .local v7, "lastCountStartCompressed":I
    const/4 v9, 0x0

    .line 196
    .local v9, "lastEndNormalized":I
    const/4 v8, 0x0

    .line 199
    .local v8, "lastEndCompressed":I
    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 200
    invoke-virtual/range {p2 .. p2}, Ljava/util/regex/Matcher;->start()I

    move-result v16

    .line 201
    .local v16, "startCompressed":I
    invoke-virtual/range {p2 .. p2}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    .line 207
    .local v5, "endCompressed":I
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, p4

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v17

    .line 212
    .local v17, "startEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-nez v17, :cond_1

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/model/WhitespaceCompressingTextBuffer;->getProcessedTextOffsetTextLocation()Lcom/google/android/apps/books/annotations/TextLocation;

    move-result-object v18

    .line 214
    .local v18, "textLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    move-object/from16 v0, v18

    iget v15, v0, Lcom/google/android/apps/books/annotations/TextLocation;->offset:I

    .line 215
    .local v15, "rangeStartNormalized":I
    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/google/android/apps/books/annotations/TextLocation;->position:Lcom/google/android/apps/books/common/Position;

    .line 216
    .local v10, "position":Lcom/google/android/apps/books/common/Position;
    const/4 v4, 0x0

    .line 225
    .end local v18    # "textLocation":Lcom/google/android/apps/books/annotations/TextLocation;
    .local v4, "countStartCompressed":I
    :goto_1
    if-ne v7, v4, :cond_2

    .line 226
    move v15, v9

    .line 227
    move v4, v8

    .line 232
    :goto_2
    move v13, v15

    .line 233
    .local v13, "rangeEndNormalized":I
    move v6, v4

    .local v6, "i":I
    :goto_3
    if-ge v6, v5, :cond_4

    .line 234
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 235
    .local v3, "c":C
    invoke-static {v3}, Lcom/google/android/apps/books/util/ReaderUtils;->isWhiteSpace(C)Z

    move-result v19

    if-eqz v19, :cond_3

    const/4 v2, 0x0

    .line 236
    .local v2, "amountToAdd":I
    :goto_4
    move/from16 v0, v16

    if-ge v6, v0, :cond_0

    .line 237
    add-int/2addr v15, v2

    .line 239
    :cond_0
    add-int/2addr v13, v2

    .line 233
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 218
    .end local v2    # "amountToAdd":I
    .end local v3    # "c":C
    .end local v4    # "countStartCompressed":I
    .end local v6    # "i":I
    .end local v10    # "position":Lcom/google/android/apps/books/common/Position;
    .end local v13    # "rangeEndNormalized":I
    .end local v15    # "rangeStartNormalized":I
    :cond_1
    const/4 v15, 0x0

    .line 219
    .restart local v15    # "rangeStartNormalized":I
    new-instance v10, Lcom/google/android/apps/books/common/Position;

    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v10, v0}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    .line 220
    .restart local v10    # "position":Lcom/google/android/apps/books/common/Position;
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .restart local v4    # "countStartCompressed":I
    goto :goto_1

    .line 229
    :cond_2
    move v7, v4

    goto :goto_2

    .line 235
    .restart local v3    # "c":C
    .restart local v6    # "i":I
    .restart local v13    # "rangeEndNormalized":I
    :cond_3
    const/4 v2, 0x1

    goto :goto_4

    .line 242
    .end local v3    # "c":C
    :cond_4
    new-instance v14, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v14, v10, v15}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 243
    .local v14, "rangeStart":Lcom/google/android/apps/books/annotations/TextLocation;
    new-instance v12, Lcom/google/android/apps/books/annotations/TextLocation;

    invoke-direct {v12, v10, v13}, Lcom/google/android/apps/books/annotations/TextLocation;-><init>(Lcom/google/android/apps/books/common/Position;I)V

    .line 244
    .local v12, "rangeEnd":Lcom/google/android/apps/books/annotations/TextLocation;
    new-instance v11, Lcom/google/android/apps/books/annotations/TextLocationRange;

    invoke-direct {v11, v14, v12}, Lcom/google/android/apps/books/annotations/TextLocationRange;-><init>(Lcom/google/android/apps/books/annotations/TextLocation;Lcom/google/android/apps/books/annotations/TextLocation;)V

    .line 245
    .local v11, "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    move v8, v5

    .line 248
    move v9, v13

    .line 249
    goto :goto_0

    .line 250
    .end local v4    # "countStartCompressed":I
    .end local v5    # "endCompressed":I
    .end local v6    # "i":I
    .end local v10    # "position":Lcom/google/android/apps/books/common/Position;
    .end local v11    # "range":Lcom/google/android/apps/books/annotations/TextLocationRange;
    .end local v12    # "rangeEnd":Lcom/google/android/apps/books/annotations/TextLocation;
    .end local v13    # "rangeEndNormalized":I
    .end local v14    # "rangeStart":Lcom/google/android/apps/books/annotations/TextLocation;
    .end local v15    # "rangeStartNormalized":I
    .end local v16    # "startCompressed":I
    .end local v17    # "startEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/String;>;"
    :cond_5
    return-void
.end method
