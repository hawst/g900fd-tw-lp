.class Lcom/google/android/apps/books/data/UploadsControllerImpl$8;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->deleteUploadOnlyBackground(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$id:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->val$id:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->val$id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->val$id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/SingleBookUploader;

    invoke-interface {v0}, Lcom/google/android/apps/books/upload/SingleBookUploader;->processingCompleted()V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->val$id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploadDataStorage:Lcom/google/android/apps/books/upload/UploadDataStorage;
    invoke-static {v0}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$000(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Lcom/google/android/apps/books/upload/UploadDataStorage;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$8;->val$id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/upload/UploadDataStorage;->deleteFile(Ljava/lang/String;)V

    .line 278
    return-void
.end method
