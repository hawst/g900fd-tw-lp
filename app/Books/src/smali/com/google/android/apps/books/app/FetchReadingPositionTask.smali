.class public abstract Lcom/google/android/apps/books/app/FetchReadingPositionTask;
.super Landroid/os/AsyncTask;
.source "FetchReadingPositionTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/apps/books/model/VolumeData;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private mException:Ljava/lang/Exception;

.field private final mServer:Lcom/google/android/apps/books/net/BooksServer;

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/net/BooksServer;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 1
    .param p1, "server"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "account"    # Landroid/accounts/Account;

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 34
    const-string v0, "missing server"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/net/BooksServer;

    iput-object v0, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mServer:Lcom/google/android/apps/books/net/BooksServer;

    .line 35
    const-string v0, "missing volume ID"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mVolumeId:Ljava/lang/String;

    .line 36
    const-string v0, "missing account"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mAccount:Landroid/accounts/Account;

    .line 37
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/model/VolumeData;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 42
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mServer:Lcom/google/android/apps/books/net/BooksServer;

    iget-object v3, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mVolumeId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/books/net/BooksServer;->getVolumeOverview(Ljava/lang/String;)Lcom/google/android/apps/books/api/data/ApiaryVolume;

    move-result-object v0

    .line 43
    .local v0, "apiaryVolume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    new-instance v2, Lcom/google/android/apps/books/model/JsonVolumeData;

    iget-object v3, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mAccount:Landroid/accounts/Account;

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/books/model/JsonVolumeData;-><init>(Lcom/google/android/apps/books/api/data/ApiaryVolume;Landroid/accounts/Account;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .end local v0    # "apiaryVolume":Lcom/google/android/apps/books/api/data/ApiaryVolume;
    :goto_0
    return-object v2

    .line 44
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Ljava/io/IOException;
    iput-object v1, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mException:Ljava/lang/Exception;

    .line 46
    const-string v2, "FetchPositionTask"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    const-string v2, "FetchPositionTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error fetching volume overview: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->doInBackground([Ljava/lang/Void;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    return-object v0
.end method

.method protected abstract onFinished(Lcom/google/android/apps/books/model/VolumeData;Ljava/lang/Exception;)V
.end method

.method protected onPostExecute(Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 1
    .param p1, "result"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->mException:Ljava/lang/Exception;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->onFinished(Lcom/google/android/apps/books/model/VolumeData;Ljava/lang/Exception;)V

    .line 57
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/apps/books/model/VolumeData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/FetchReadingPositionTask;->onPostExecute(Lcom/google/android/apps/books/model/VolumeData;)V

    return-void
.end method
