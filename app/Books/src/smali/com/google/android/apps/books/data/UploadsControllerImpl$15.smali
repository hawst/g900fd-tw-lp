.class Lcom/google/android/apps/books/data/UploadsControllerImpl$15;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Updater;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->updateUploadTransferHandle(Ljava/lang/String;Lcom/google/uploader/client/ClientProto$TransferHandle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Updater",
        "<",
        "Lcom/google/android/apps/books/upload/Upload;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$transferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Lcom/google/uploader/client/ClientProto$TransferHandle;)V
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$15;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$15;->val$transferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;
    .locals 1
    .param p1, "upload"    # Lcom/google/android/apps/books/upload/Upload;

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$15;->val$transferHandle:Lcom/google/uploader/client/ClientProto$TransferHandle;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/upload/Upload;->setTransferHandle(Lcom/google/uploader/client/ClientProto$TransferHandle;)V

    .line 500
    return-object p1
.end method

.method public bridge synthetic update(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 496
    check-cast p1, Lcom/google/android/apps/books/upload/Upload;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/data/UploadsControllerImpl$15;->update(Lcom/google/android/apps/books/upload/Upload;)Lcom/google/android/apps/books/upload/Upload;

    move-result-object v0

    return-object v0
.end method
