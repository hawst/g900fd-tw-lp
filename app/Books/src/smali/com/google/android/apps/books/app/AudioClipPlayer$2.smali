.class Lcom/google/android/apps/books/app/AudioClipPlayer$2;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"

# interfaces
.implements Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer;->createMediaPlayerListener()Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/AudioClipPlayer;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    .prologue
    .line 588
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    const-string v0, "AudioClipPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPrepared, seek to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    invoke-static {v2}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1200(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mMediaPlayer:Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
    invoke-static {v0}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1500(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1200(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;->seekTo(I)V

    .line 592
    return-void
.end method

.method public onSeekComplete(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;

    .prologue
    .line 576
    const-string v0, "AudioClipPlayer"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    const-string v0, "AudioClipPlayer"

    const-string v1, "onSeekComplete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1200(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;->startTime:I

    int-to-long v2, v1

    # setter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mClipStartAtLastSeek:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1102(Lcom/google/android/apps/books/app/AudioClipPlayer;J)J

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mTimeAtLastSeek:J
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1302(Lcom/google/android/apps/books/app/AudioClipPlayer;J)J

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    iget-object v1, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    # getter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mCurrentClip:Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;
    invoke-static {v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1200(Lcom/google/android/apps/books/app/AudioClipPlayer;)Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;

    move-result-object v1

    # invokes: Lcom/google/android/apps/books/app/AudioClipPlayer;->startedNextClip(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$1400(Lcom/google/android/apps/books/app/AudioClipPlayer;Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;)V

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/books/app/AudioClipPlayer$2;->this$0:Lcom/google/android/apps/books/app/AudioClipPlayer;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/books/app/AudioClipPlayer;->mPreparingOrSeeking:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/AudioClipPlayer;->access$302(Lcom/google/android/apps/books/app/AudioClipPlayer;Z)Z

    .line 584
    return-void
.end method
