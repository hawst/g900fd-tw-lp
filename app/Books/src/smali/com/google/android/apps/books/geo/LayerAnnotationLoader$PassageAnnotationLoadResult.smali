.class public interface abstract Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;
.super Ljava/lang/Object;
.source "LayerAnnotationLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/geo/LayerAnnotationLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PassageAnnotationLoadResult"
.end annotation


# virtual methods
.method public abstract getAnnotationsInRange(Lcom/google/android/apps/books/annotations/TextLocationRange;)Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocationRange;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/books/annotations/Annotation;",
            ">;"
        }
    .end annotation
.end method
