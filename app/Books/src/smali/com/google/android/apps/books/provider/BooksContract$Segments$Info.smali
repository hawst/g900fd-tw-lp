.class public Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;
.super Ljava/lang/Object;
.source "BooksContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/provider/BooksContract$Segments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# instance fields
.field public final accountName:Ljava/lang/String;

.field public final sectionId:Ljava/lang/String;

.field public final volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "acct"    # Ljava/lang/String;
    .param p2, "vol"    # Ljava/lang/String;
    .param p3, "sec"    # Ljava/lang/String;

    .prologue
    .line 2093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2094
    invoke-static {p1}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->accountName:Ljava/lang/String;

    .line 2095
    invoke-static {p2}, Lcom/google/api/client/repackaged/com/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->volumeId:Ljava/lang/String;

    .line 2096
    iput-object p3, p0, Lcom/google/android/apps/books/provider/BooksContract$Segments$Info;->sectionId:Ljava/lang/String;

    .line 2097
    return-void
.end method
