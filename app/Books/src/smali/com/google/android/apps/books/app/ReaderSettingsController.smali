.class public Lcom/google/android/apps/books/app/ReaderSettingsController;
.super Ljava/lang/Object;
.source "ReaderSettingsController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReaderSettingsController$9;,
        Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;,
        Lcom/google/android/apps/books/app/ReaderSettingsController$DisplayOptionsDialog;
    }
.end annotation


# instance fields
.field private final mBackgroundPadding:Landroid/graphics/Rect;

.field private final mBrightnessListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private final mContainer:Landroid/view/ViewGroup;

.field private mCurrentSettingsView:Landroid/view/View;

.field private final mDisplayFrame:Landroid/graphics/Rect;

.field private final mFixedLayoutSettings:Landroid/view/View;

.field private final mFlowingTextSettings:Landroid/view/View;

.field private final mImageModeSettings:Landroid/view/View;

.field private final mJustificationListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mLineHeight:Lcom/google/android/apps/books/preference/LineHeightPreference;

.field private final mLineHeightSettingListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

.field private mPageLayoutFL:Landroid/view/View;

.field private mPageLayoutIM:Landroid/view/View;

.field private final mPageLayoutListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private final mPopupDialog:Landroid/app/Dialog;

.field private final mPrefs:Landroid/content/SharedPreferences;

.field private mTextSize:Lcom/google/android/apps/books/preference/TextZoomPreference;

.field private final mTextZoomSettingListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private final mThemeListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mTypeFace:Lcom/google/android/apps/books/preference/SpinnerPreference;

.field private final mTypefaceListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private final mWindow:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/view/Window;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mDisplayFrame:Landroid/graphics/Rect;

    .line 73
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mBackgroundPadding:Landroid/graphics/Rect;

    .line 399
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$2;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mBrightnessListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 415
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$3;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mThemeListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 424
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$4;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mJustificationListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 433
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$5;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$5;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTypefaceListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 442
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$6;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$6;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextZoomSettingListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 452
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$7;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$7;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mLineHeightSettingListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 462
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$8;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$8;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 99
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mWindow:Landroid/view/Window;

    .line 101
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 102
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPrefs:Landroid/content/SharedPreferences;

    .line 104
    new-instance v2, Lcom/google/android/apps/books/app/ReaderSettingsController$DisplayOptionsDialog;

    const v3, 0x7f0a01ba

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/apps/books/app/ReaderSettingsController$DisplayOptionsDialog;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    const v3, 0x7f04004b

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(I)V

    .line 107
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x35

    invoke-virtual {v2, v3}, Landroid/view/Window;->setGravity(I)V

    .line 109
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 111
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    new-instance v3, Lcom/google/android/apps/books/app/ReaderSettingsController$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/app/ReaderSettingsController$1;-><init>(Lcom/google/android/apps/books/app/ReaderSettingsController;)V

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 120
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    const v3, 0x7f0e00f7

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    .line 122
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 123
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 124
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mBackgroundPadding:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 133
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->createImageModeSettingsView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mImageModeSettings:Landroid/view/View;

    .line 136
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->createFlowingTextModeSettingsView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFlowingTextSettings:Landroid/view/View;

    .line 139
    invoke-direct {p0, v1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->createFixedLayoutSettingsView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFixedLayoutSettings:Landroid/view/View;

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderSettingsController;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ReaderSettingsController;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderSettingsController;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/preference/TextZoomPreference;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderSettingsController;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextSize:Lcom/google/android/apps/books/preference/TextZoomPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/app/ReaderSettingsController;)Lcom/google/android/apps/books/preference/LineHeightPreference;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReaderSettingsController;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mLineHeight:Lcom/google/android/apps/books/preference/LineHeightPreference;

    return-object v0
.end method

.method private addBrightness(Landroid/widget/LinearLayout;)V
    .locals 4
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 322
    const v2, 0x7f0400a2

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v1

    .line 323
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0e0196

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mBrightnessListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/preference/BrightnessPreference;

    .line 325
    .local v0, "brightness":Lcom/google/android/apps/books/preference/BrightnessPreference;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mWindow:Landroid/view/Window;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/preference/BrightnessPreference;->setWindow(Landroid/view/Window;)V

    .line 326
    return-void
.end method

.method private addJustification(Landroid/widget/LinearLayout;)V
    .locals 3
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 335
    const v1, 0x7f0400a4

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v0

    .line 336
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e019a

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mJustificationListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    .line 337
    return-void
.end method

.method private addLineHeight(Landroid/widget/LinearLayout;)V
    .locals 3
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 329
    const v1, 0x7f0400a5

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v0

    .line 330
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e019d

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mLineHeightSettingListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/LineHeightPreference;

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mLineHeight:Lcom/google/android/apps/books/preference/LineHeightPreference;

    .line 332
    return-void
.end method

.method private addPageLayout(Landroid/widget/LinearLayout;)Landroid/view/View;
    .locals 3
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 357
    const v1, 0x7f0400a6

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v0

    .line 358
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e01a1

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    .line 359
    return-object v0
.end method

.method private addTextSize(Landroid/widget/LinearLayout;)V
    .locals 3
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 340
    const v1, 0x7f0400a9

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v0

    .line 341
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e01a2

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextZoomSettingListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/TextZoomPreference;

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextSize:Lcom/google/android/apps/books/preference/TextZoomPreference;

    .line 343
    return-void
.end method

.method private addTheme(Landroid/widget/LinearLayout;)V
    .locals 3
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 346
    const v1, 0x7f0400aa

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v0

    .line 347
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e01a3

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mThemeListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    .line 348
    return-void
.end method

.method private addTypeFace(Landroid/widget/LinearLayout;)V
    .locals 3
    .param p1, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 351
    const v1, 0x7f0400ab

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/app/ReaderSettingsController;->getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v0

    .line 352
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0e01a7

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTypefaceListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/books/app/ReaderSettingsController;->setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/SpinnerPreference;

    iput-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTypeFace:Lcom/google/android/apps/books/preference/SpinnerPreference;

    .line 354
    return-void
.end method

.method private createFixedLayoutSettingsView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 389
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04004c

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 391
    .local v1, "displayOptionsView":Landroid/view/View;
    const v2, 0x7f0e00f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 393
    .local v0, "container":Landroid/widget/LinearLayout;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addTheme(Landroid/widget/LinearLayout;)V

    .line 394
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addBrightness(Landroid/widget/LinearLayout;)V

    .line 395
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addPageLayout(Landroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutFL:Landroid/view/View;

    .line 396
    return-object v1
.end method

.method private createFlowingTextModeSettingsView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 363
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04004c

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 365
    .local v1, "displayOptionsView":Landroid/view/View;
    const v2, 0x7f0e00f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 367
    .local v0, "container":Landroid/widget/LinearLayout;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addTheme(Landroid/widget/LinearLayout;)V

    .line 368
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addBrightness(Landroid/widget/LinearLayout;)V

    .line 369
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addTypeFace(Landroid/widget/LinearLayout;)V

    .line 370
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addJustification(Landroid/widget/LinearLayout;)V

    .line 371
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addTextSize(Landroid/widget/LinearLayout;)V

    .line 372
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addLineHeight(Landroid/widget/LinearLayout;)V

    .line 373
    return-object v1
.end method

.method private createImageModeSettingsView(Landroid/content/Context;)Landroid/view/View;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 377
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04004c

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 379
    .local v1, "displayOptionsView":Landroid/view/View;
    const v2, 0x7f0e00f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 381
    .local v0, "container":Landroid/widget/LinearLayout;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addTheme(Landroid/widget/LinearLayout;)V

    .line 382
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addBrightness(Landroid/widget/LinearLayout;)V

    .line 383
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/app/ReaderSettingsController;->addPageLayout(Landroid/widget/LinearLayout;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutIM:Landroid/view/View;

    .line 384
    return-object v1
.end method

.method private getPreferenceItem(ILandroid/widget/LinearLayout;)Landroid/view/View;
    .locals 5
    .param p1, "prefLayout"    # I
    .param p2, "container"    # Landroid/widget/LinearLayout;

    .prologue
    .line 307
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 308
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f0400a3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 309
    .local v2, "prefItem":Landroid/view/View;
    const v3, 0x7f0e0199

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 310
    .local v1, "prefContainer":Landroid/widget/FrameLayout;
    const/4 v3, 0x1

    invoke-virtual {v0, p1, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 311
    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 312
    return-object v2
.end method

.method private selectBackground(I)V
    .locals 2
    .param p1, "backgroundIndex"    # I

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 217
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 220
    :cond_0
    return-void
.end method

.method private setUpPreference(Landroid/view/View;ILcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)Lcom/google/android/apps/books/preference/LightweightPreference;
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "prefId"    # I
    .param p3, "listener"    # Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .prologue
    .line 316
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/preference/LightweightPreference;

    .line 317
    .local v0, "preference":Lcom/google/android/apps/books/preference/LightweightPreference;
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1, p3}, Lcom/google/android/apps/books/preference/LightweightPreference;->setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V

    .line 318
    return-object v0
.end method


# virtual methods
.method public getVisible()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 224
    return-void
.end method

.method public setFitWidth(ZZ)V
    .locals 6
    .param p1, "visible"    # Z
    .param p2, "fitWidth"    # Z

    .prologue
    const v5, 0x7f0e01a1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 157
    if-eqz p2, :cond_0

    const-string v0, "1up"

    .line 162
    .local v0, "pageLayout":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutFL:Landroid/view/View;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutFL:Landroid/view/View;

    if-eqz p1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutFL:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    check-cast v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->setValue(Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutIM:Landroid/view/View;

    invoke-static {v1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutIM:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPageLayoutIM:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    check-cast v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->setValue(Ljava/lang/String;)V

    .line 171
    return-void

    .line 157
    .end local v0    # "pageLayout":Ljava/lang/String;
    :cond_0
    const-string v0, "2up"

    goto :goto_0

    .restart local v0    # "pageLayout":Ljava/lang/String;
    :cond_1
    move v1, v3

    .line 163
    goto :goto_1

    :cond_2
    move v2, v3

    .line 168
    goto :goto_2
.end method

.method public setLineHeight(F)V
    .locals 1
    .param p1, "lineHeight"    # F

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mLineHeight:Lcom/google/android/apps/books/preference/LineHeightPreference;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mLineHeight:Lcom/google/android/apps/books/preference/LineHeightPreference;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/LineHeightPreference;->setValue(F)V

    .line 154
    return-void
.end method

.method public setListener(Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    .line 144
    return-void
.end method

.method public setMinimalFontMenu(Z)V
    .locals 5
    .param p1, "minimal"    # Z

    .prologue
    .line 278
    if-eqz p1, :cond_0

    const v1, 0x7f100002

    .line 279
    .local v1, "entryId":I
    :goto_0
    if-eqz p1, :cond_1

    const v2, 0x7f100003

    .line 280
    .local v2, "valueId":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 281
    .local v0, "entries":[Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v3

    .line 282
    .local v3, "values":[Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTypeFace:Lcom/google/android/apps/books/preference/SpinnerPreference;

    invoke-virtual {v4, v0, v3}, Lcom/google/android/apps/books/preference/SpinnerPreference;->setEntries([Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)V

    .line 283
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTypeFace:Lcom/google/android/apps/books/preference/SpinnerPreference;

    invoke-virtual {v4}, Lcom/google/android/apps/books/preference/SpinnerPreference;->bindPreference()V

    .line 284
    return-void

    .line 278
    .end local v0    # "entries":[Ljava/lang/CharSequence;
    .end local v1    # "entryId":I
    .end local v2    # "valueId":I
    .end local v3    # "values":[Ljava/lang/CharSequence;
    :cond_0
    const/high16 v1, 0x7f100000

    goto :goto_0

    .line 279
    .restart local v1    # "entryId":I
    :cond_1
    const v2, 0x7f100001

    goto :goto_1
.end method

.method public setReadingMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 7
    .param p1, "readingMode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    const v6, 0x7f0e01a3

    const v5, 0x7f0e0196

    .line 231
    const-string v2, "ReaderSettings"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    const-string v2, "ReaderSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setReadingMode("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 236
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    .line 241
    sget-object v2, Lcom/google/android/apps/books/app/ReaderSettingsController$9;->$SwitchMap$com$google$android$apps$books$model$VolumeManifest$Mode:[I

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 257
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mImageModeSettings:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/preference/BrightnessPreference;

    .line 259
    .local v0, "brightness":Lcom/google/android/apps/books/preference/BrightnessPreference;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mImageModeSettings:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    .line 260
    .local v1, "theme":Lcom/google/android/apps/books/preference/ButtonGroupPreference;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mImageModeSettings:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    .line 263
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 266
    if-eqz v0, :cond_1

    .line 267
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mBrightnessListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/books/preference/BrightnessPreference;->setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V

    .line 269
    :cond_1
    if-eqz v1, :cond_2

    .line 270
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mThemeListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/books/preference/ButtonGroupPreference;->setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V

    .line 272
    :cond_2
    return-void

    .line 243
    .end local v0    # "brightness":Lcom/google/android/apps/books/preference/BrightnessPreference;
    .end local v1    # "theme":Lcom/google/android/apps/books/preference/ButtonGroupPreference;
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFlowingTextSettings:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/preference/BrightnessPreference;

    .line 245
    .restart local v0    # "brightness":Lcom/google/android/apps/books/preference/BrightnessPreference;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFlowingTextSettings:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/preference/ButtonGroupPreference;

    .line 246
    .restart local v1    # "theme":Lcom/google/android/apps/books/preference/ButtonGroupPreference;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFlowingTextSettings:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    goto :goto_0

    .line 249
    .end local v0    # "brightness":Lcom/google/android/apps/books/preference/BrightnessPreference;
    .end local v1    # "theme":Lcom/google/android/apps/books/preference/ButtonGroupPreference;
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFixedLayoutSettings:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/preference/BrightnessPreference;

    .line 252
    .restart local v0    # "brightness":Lcom/google/android/apps/books/preference/BrightnessPreference;
    const/4 v1, 0x0

    .line 253
    .restart local v1    # "theme":Lcom/google/android/apps/books/preference/ButtonGroupPreference;
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mFixedLayoutSettings:Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    goto :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setTextZoom(F)V
    .locals 1
    .param p1, "textZoom"    # F

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextSize:Lcom/google/android/apps/books/preference/TextZoomPreference;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mTextSize:Lcom/google/android/apps/books/preference/TextZoomPreference;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/preference/TextZoomPreference;->setValue(F)V

    .line 149
    return-void
.end method

.method public show()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 177
    const/16 v2, 0x35

    .line 178
    .local v2, "gravity":I
    const v0, 0x7f0a000e

    .line 180
    .local v0, "animationStyle":I
    invoke-direct {p0, v8}, Lcom/google/android/apps/books/app/ReaderSettingsController;->selectBackground(I)V

    .line 182
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x35

    invoke-virtual {v5, v6}, Landroid/view/Window;->setGravity(I)V

    .line 184
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 185
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    const v5, 0x7f0a000e

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 193
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mDisplayFrame:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 194
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mDisplayFrame:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 195
    .local v1, "availableHeight":I
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewGroup;->measure(II)V

    .line 198
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v4

    .line 199
    .local v4, "popupHeight":I
    if-le v4, v1, :cond_0

    .line 200
    iput v1, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 205
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    if-eqz v5, :cond_1

    .line 206
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mCurrentSettingsView:Landroid/view/View;

    invoke-virtual {v5, v8, v8}, Landroid/view/View;->scrollTo(II)V

    .line 208
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    if-eqz v5, :cond_2

    .line 209
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mListener:Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;

    invoke-interface {v5}, Lcom/google/android/apps/books/app/ReaderSettingsController$ReaderSettingsListener;->onShowingSettings()V

    .line 212
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/app/ReaderSettingsController;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 213
    return-void
.end method
