.class public interface abstract Lcom/google/android/apps/books/data/ControlTaskServices;
.super Ljava/lang/Object;
.source "ControlTaskServices.java"


# virtual methods
.method public abstract copyListeners()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataListener;",
            ">;"
        }
    .end annotation
.end method

.method public abstract executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V
.end method

.method public abstract getDataControllerStore()Lcom/google/android/apps/books/model/DataControllerStore;
.end method

.method public abstract getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;
.end method

.method public abstract getValidAccountSessionKey(Lcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;)V"
        }
    .end annotation
.end method

.method public abstract getValidSessionKey(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/ublib/utils/Consumer;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKeyId;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;>;>;)V"
        }
    .end annotation
.end method

.method public abstract removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<*>;)V"
        }
    .end annotation
.end method

.method public abstract scheduleDeferrableTask(Lcom/google/android/apps/books/data/BackgroundBooksDataController$PendingAction;)V
.end method
