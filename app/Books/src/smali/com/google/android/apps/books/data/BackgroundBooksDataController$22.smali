.class Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;
.super Ljava/lang/Object;
.source "BackgroundBooksDataController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getResourceContent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

.field final synthetic val$contentConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$onlyIfLocal:Z

.field final synthetic val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

.field final synthetic val$resourceId:Ljava/lang/String;

.field final synthetic val$resourcesConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V
    .locals 0

    .prologue
    .line 852
    iput-object p1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iput-object p2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$resourceId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$contentConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$resourcesConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iput-object p7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    iput-boolean p8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$onlyIfLocal:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 855
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$resourceId:Ljava/lang/String;

    # invokes: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->getResource(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3100(Lcom/google/android/apps/books/data/BackgroundBooksDataController;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;

    move-result-object v3

    .line 856
    .local v3, "resource":Lcom/google/android/apps/books/model/Resource;
    iget-object v0, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mResourceContentSubcontroller:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;
    invoke-static {v0}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$3000(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->this$0:Lcom/google/android/apps/books/data/BackgroundBooksDataController;

    # getter for: Lcom/google/android/apps/books/data/BackgroundBooksDataController;->mDispatcher:Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;
    invoke-static {v1}, Lcom/google/android/apps/books/data/BackgroundBooksDataController;->access$400(Lcom/google/android/apps/books/data/BackgroundBooksDataController;)Lcom/google/android/apps/books/data/DataControllerTaskDispatcher;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$volumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$contentConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v5, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$resourcesConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v6, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$saveConsumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v7, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$priority:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    iget-boolean v8, p0, Lcom/google/android/apps/books/data/BackgroundBooksDataController$22;->val$onlyIfLocal:Z

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->getResourceContent(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/data/BooksDataController$Priority;Z)V

    .line 858
    return-void
.end method
