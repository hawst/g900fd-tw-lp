.class public Lcom/google/android/apps/books/util/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;,
        Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;,
        Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
    }
.end annotation


# static fields
.field public static final DONT_LISTEN:Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;

    invoke-direct {v0}, Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/IOUtils;->DONT_LISTEN:Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;

    return-void
.end method

.method public static close(Ljava/io/Closeable;)V
    .locals 4
    .param p0, "closeable"    # Ljava/io/Closeable;

    .prologue
    .line 217
    if-nez p0, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "IOUtils"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    const-string v1, "IOUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in close(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static varargs close([Ljava/io/Closeable;)V
    .locals 4
    .param p0, "closeables"    # [Ljava/io/Closeable;

    .prologue
    .line 233
    if-nez p0, :cond_1

    .line 239
    :cond_0
    return-void

    .line 236
    :cond_1
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 237
    .local v1, "closeable":Ljava/io/Closeable;
    invoke-static {v1}, Lcom/google/android/apps/books/util/IOUtils;->close(Ljava/io/Closeable;)V

    .line 236
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "output"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/apps/books/util/IOUtils;->DONT_LISTEN:Lcom/google/android/apps/books/util/IOUtils$StubStreamProgressListener;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/books/util/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)I

    move-result v0

    return v0
.end method

.method public static copy(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)I
    .locals 4
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "output"    # Ljava/io/OutputStream;
    .param p2, "listener"    # Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/books/util/IOUtils;->copyLarge(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)J

    move-result-wide v0

    .line 152
    .local v0, "count":J
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 153
    const/4 v2, -0x1

    .line 155
    :goto_0
    return v2

    :cond_0
    long-to-int v2, v0

    goto :goto_0
.end method

.method public static copyLarge(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)J
    .locals 6
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "output"    # Ljava/io/OutputStream;
    .param p2, "listener"    # Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    const/16 v4, 0x1000

    new-array v0, v4, [B

    .line 180
    .local v0, "buffer":[B
    const-wide/16 v2, 0x0

    .line 181
    .local v2, "count":J
    const/4 v1, 0x0

    .line 182
    .local v1, "n":I
    :goto_0
    const/4 v4, -0x1

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-eq v4, v1, :cond_0

    .line 183
    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 184
    int-to-long v4, v1

    add-long/2addr v2, v4

    .line 185
    invoke-interface {p2, v2, v3}, Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;->bytesTransferred(J)V

    goto :goto_0

    .line 187
    :cond_0
    return-wide v2
.end method

.method public static copyWithLimit(Ljava/io/InputStream;Ljava/io/OutputStream;J)J
    .locals 6
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "output"    # Ljava/io/OutputStream;
    .param p2, "limit"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;
        }
    .end annotation

    .prologue
    .line 198
    const/16 v4, 0x1000

    new-array v0, v4, [B

    .line 199
    .local v0, "buffer":[B
    const-wide/16 v2, 0x0

    .line 200
    .local v2, "count":J
    const/4 v1, 0x0

    .line 201
    .local v1, "n":I
    :goto_0
    const/4 v4, -0x1

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-eq v4, v1, :cond_1

    .line 202
    int-to-long v4, v1

    add-long/2addr v2, v4

    .line 203
    cmp-long v4, v2, p2

    if-lez v4, :cond_0

    .line 204
    new-instance v4, Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;

    invoke-direct {v4}, Lcom/google/android/apps/books/util/IOUtils$InputTooLargeException;-><init>()V

    throw v4

    .line 206
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 208
    :cond_1
    return-wide v2
.end method

.method public static toByteArray(Ljava/io/InputStream;)[B
    .locals 2
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 116
    .local v0, "output":Ljava/io/ByteArrayOutputStream;
    invoke-static {p0, v0}, Lcom/google/android/apps/books/util/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 117
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method
