.class public interface abstract Lcom/google/android/apps/books/app/ReadAlongController$DataSource;
.super Ljava/lang/Object;
.source "ReadAlongController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReadAlongController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DataSource"
.end annotation


# virtual methods
.method public abstract getLocale()Ljava/util/Locale;
.end method

.method public abstract isPassageForbidden(I)Z
.end method

.method public abstract requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I
.end method

.method public abstract shutDown()V
.end method
