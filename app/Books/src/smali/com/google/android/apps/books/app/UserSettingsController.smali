.class public Lcom/google/android/apps/books/app/UserSettingsController;
.super Ljava/lang/Object;
.source "UserSettingsController.java"


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mBooksServer:Lcom/google/android/apps/books/net/BooksServer;

.field private final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/net/BooksServer;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "booksServer"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p2, "backgroundExecutor"    # Ljava/util/concurrent/ExecutorService;
    .param p3, "uiExecutor"    # Ljava/util/concurrent/Executor;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    .line 29
    iput-object p3, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/UserSettingsController;)Lcom/google/android/apps/books/net/BooksServer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/UserSettingsController;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/UserSettingsController;)Ljava/util/concurrent/Executor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/UserSettingsController;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mUiExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method private updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .param p1, "newUserSettings"    # Lcom/google/android/apps/books/api/data/UserSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/api/data/UserSettings;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/UserSettings;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/app/UserSettingsController$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/books/app/UserSettingsController$2;-><init>(Lcom/google/android/apps/books/app/UserSettingsController;Lcom/google/android/apps/books/api/data/UserSettings;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 104
    return-void
.end method


# virtual methods
.method public getUserSettings(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/UserSettings;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/UserSettingsController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/app/UserSettingsController$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/UserSettingsController$1;-><init>(Lcom/google/android/apps/books/app/UserSettingsController;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 61
    return-void
.end method

.method public updateSaveNotes(ZLjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "isEnabled"    # Z
    .param p2, "folderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/UserSettings;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/UserSettings;>;>;"
    invoke-static {p1, p2}, Lcom/google/android/apps/books/api/data/UserSettings;->createUserSettingsForNotesExport(ZLjava/lang/String;)Lcom/google/android/apps/books/api/data/UserSettings;

    move-result-object v0

    .line 70
    .local v0, "newSettings":Lcom/google/android/apps/books/api/data/UserSettings;
    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/books/app/UserSettingsController;->updateUserSettings(Lcom/google/android/apps/books/api/data/UserSettings;Lcom/google/android/ublib/utils/Consumer;)V

    .line 71
    return-void
.end method
