.class Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;
.super Ljava/lang/Object;
.source "PageTurnController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EndTurnInertial"
.end annotation


# instance fields
.field private final mCanceled:Z

.field private final mSequenceNumber:I

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PageTurnController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PageTurnController;ZI)V
    .locals 0
    .param p2, "canceled"    # Z
    .param p3, "sequenceNumber"    # I

    .prologue
    .line 489
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->mCanceled:Z

    .line 491
    iput p3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->mSequenceNumber:I

    .line 492
    return-void
.end method


# virtual methods
.method public canBeCanceled()Z
    .locals 2

    .prologue
    .line 538
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->mSequenceNumber:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$200(Lcom/google/android/apps/books/widget/PageTurnController;)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canRun()Z
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$200(Lcom/google/android/apps/books/widget/PageTurnController;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->mSequenceNumber:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDirection()Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    return-object v0
.end method

.method public run()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 502
    const-string v3, "PageTurnController"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 503
    const-string v3, "PageTurnController"

    const-string v4, "EndTurnInertial.run()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->mSequenceNumber:I

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mEndedTurnSequenceNumber:I
    invoke-static {v3, v4}, Lcom/google/android/apps/books/widget/PageTurnController;->access$702(Lcom/google/android/apps/books/widget/PageTurnController;I)I

    .line 508
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v3

    if-nez v3, :cond_2

    .line 510
    const-string v1, "PageTurnController"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 511
    const-string v1, "PageTurnController"

    const-string v2, "EndTurnInertial.run() early exit: Not turning"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 528
    :goto_0
    return-object v1

    .line 517
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mGestureFraction:F
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PageTurnController;->access$500(Lcom/google/android/apps/books/widget/PageTurnController;)F

    move-result v3

    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_3

    iget-boolean v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->mCanceled:Z

    if-eqz v3, :cond_4

    .line 519
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/ScreenDirection;->oppositeOf(Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v4

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v3, v4}, Lcom/google/android/apps/books/widget/PageTurnController;->access$102(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;)Lcom/google/android/apps/books/util/ScreenDirection;

    .line 520
    const/4 v0, 0x1

    .line 525
    .local v0, "changingDirection":Z
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mWaitingForFinishAnimation:Z
    invoke-static {v3, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$802(Lcom/google/android/apps/books/widget/PageTurnController;Z)Z

    .line 526
    iget-object v3, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    invoke-static {v3}, Lcom/google/android/apps/books/widget/PageTurnController;->access$300(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnInertial;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v4}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v4

    const/4 v5, 0x0

    if-nez v0, :cond_5

    :goto_2
    invoke-interface {v3, v4, v2, v5, v1}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->finishTurnAnimation(Lcom/google/android/apps/books/util/ScreenDirection;ZFZ)V

    .line 528
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 522
    .end local v0    # "changingDirection":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "changingDirection":Z
    goto :goto_1

    :cond_5
    move v1, v2

    .line 526
    goto :goto_2
.end method
