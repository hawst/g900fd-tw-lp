.class public Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
.super Ljava/lang/Object;
.source "Layer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/annotations/Layer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CharacterQuota"
.end annotation


# static fields
.field private static final DEFAULT_LIMITED_QUOTA:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

.field private static final DEFAULT_UNLIMITED_QUOTA:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;


# instance fields
.field public final allowedCharacters:I

.field public final limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

.field public final remainingCharacters:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 100
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$LimitType;->LIMITED:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;-><init>(Lcom/google/android/apps/books/annotations/Layer$LimitType;II)V

    sput-object v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->DEFAULT_LIMITED_QUOTA:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .line 102
    new-instance v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$LimitType;->UNLIMITED:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;-><init>(Lcom/google/android/apps/books/annotations/Layer$LimitType;II)V

    sput-object v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->DEFAULT_UNLIMITED_QUOTA:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/annotations/Layer$LimitType;II)V
    .locals 1
    .param p1, "limitType"    # Lcom/google/android/apps/books/annotations/Layer$LimitType;
    .param p2, "remainingCharacters"    # I
    .param p3, "allowedCharacters"    # I

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$LimitType;

    iput-object v0, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    .line 134
    iput p2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    .line 135
    iput p3, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->allowedCharacters:I

    .line 136
    return-void
.end method

.method public static defaultFromLayerId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .locals 1
    .param p0, "layerId"    # Ljava/lang/String;

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/apps/books/annotations/LayerId;->COPY:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->DEFAULT_LIMITED_QUOTA:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->DEFAULT_UNLIMITED_QUOTA:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    goto :goto_0
.end method

.method public static fromJson(Lcom/google/android/apps/books/annotations/data/JsonLayer;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .locals 4
    .param p0, "json"    # Lcom/google/android/apps/books/annotations/data/JsonLayer;

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonLayer;->limitType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonLayer;->layerId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->defaultFromLayerId(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    move-result-object v1

    .line 124
    :goto_0
    return-object v1

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/annotations/data/JsonLayer;->limitType:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/books/annotations/Layer$LimitType;->valueOfCaseInsensitive(Ljava/lang/String;)Lcom/google/android/apps/books/annotations/Layer$LimitType;

    move-result-object v0

    .line 124
    .local v0, "limitType":Lcom/google/android/apps/books/annotations/Layer$LimitType;
    new-instance v1, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    iget v2, p0, Lcom/google/android/apps/books/annotations/data/JsonLayer;->remainingCharacterCount:I

    iget v3, p0, Lcom/google/android/apps/books/annotations/data/JsonLayer;->allowedCharacterCount:I

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;-><init>(Lcom/google/android/apps/books/annotations/Layer$LimitType;II)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 165
    instance-of v2, p1, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 166
    check-cast v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .line 167
    .local v0, "otherQuota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    iget-object v3, v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->allowedCharacters:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, v0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->allowedCharacters:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 171
    .end local v0    # "otherQuota":Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    :cond_0
    return v1
.end method

.method public getAllowedCharacterCount()I
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->hasLimit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->allowedCharacters:I

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public getRemainingCharacterCount()I
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->hasLimit()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public hasLimit()Z
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    sget-object v1, Lcom/google/android/apps/books/annotations/Layer$LimitType;->LIMITED:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 160
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->allowedCharacters:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 152
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "limitType"

    iget-object v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->limitType:Lcom/google/android/apps/books/annotations/Layer$LimitType;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "remainingCharacters"

    iget v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "allowedCharacters"

    iget v2, p0, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->allowedCharacters:I

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;I)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
