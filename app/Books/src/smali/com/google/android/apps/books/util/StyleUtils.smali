.class public Lcom/google/android/apps/books/util/StyleUtils;
.super Ljava/lang/Object;
.source "StyleUtils.java"


# direct methods
.method public static configureFlatBlueActionBar(Landroid/content/Context;Landroid/support/v7/app/ActionBar;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/support/v7/app/ActionBar;

    .prologue
    const/16 v1, 0xe

    .line 44
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/StyleUtils;->setThemedActionBarDrawable(Landroid/content/Context;Landroid/support/v7/app/ActionBar;)V

    .line 45
    const/16 v0, 0xe

    .line 47
    .local v0, "displayOptions":I
    invoke-virtual {p1, v1, v1}, Landroid/support/v7/app/ActionBar;->setDisplayOptions(II)V

    .line 48
    return-void
.end method

.method public static flattenToolbar(Landroid/support/v7/widget/Toolbar;)V
    .locals 1
    .param p0, "toolbar"    # Landroid/support/v7/widget/Toolbar;

    .prologue
    .line 63
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/Toolbar;->setElevation(F)V

    .line 66
    :cond_0
    return-void
.end method

.method public static setActionBarElevation(Landroid/support/v7/app/ActionBar;F)V
    .locals 1
    .param p0, "actionBar"    # Landroid/support/v7/app/ActionBar;
    .param p1, "elevation"    # F

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLollipopOrLater()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0, p1}, Landroid/support/v7/app/ActionBar;->setElevation(F)V

    .line 60
    :cond_0
    return-void
.end method

.method public static setThemedActionBarDrawable(Landroid/content/Context;Landroid/support/v7/app/ActionBar;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "actionBar"    # Landroid/support/v7/app/ActionBar;

    .prologue
    const/4 v4, 0x0

    .line 30
    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    invoke-virtual {p0, v2, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 31
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 33
    .local v1, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 34
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 35
    return-void
.end method
