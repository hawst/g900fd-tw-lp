.class Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;
.super Ljava/lang/Object;
.source "SelectionOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SelectionOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HandleData"
.end annotation


# instance fields
.field final mAnchorPoint:Landroid/graphics/Point;

.field mDrawable:Landroid/graphics/drawable/Drawable;

.field final mHandleRect:Landroid/graphics/Rect;

.field mIsLeftDrawable:Z

.field final mRotation:Landroid/graphics/Matrix;

.field mRotationAngle:F

.field final mRotationInverse:Landroid/graphics/Matrix;

.field final mSelectRect:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mAnchorPoint:Landroid/graphics/Point;

    .line 408
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    .line 409
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mSelectRect:Landroid/graphics/Rect;

    .line 411
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotation:Landroid/graphics/Matrix;

    .line 412
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotationInverse:Landroid/graphics/Matrix;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/books/widget/SelectionOverlay$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/apps/books/widget/SelectionOverlay$1;

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;-><init>()V

    return-void
.end method


# virtual methods
.method applyTransform(Landroid/graphics/Matrix;)V
    .locals 10
    .param p1, "pageToViewMatrix"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 449
    const/4 v6, 0x2

    new-array v3, v6, [F

    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mAnchorPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    aput v6, v3, v4

    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mAnchorPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    aput v6, v3, v7

    .line 452
    .local v3, "point":[F
    invoke-virtual {p1, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 454
    aget v6, v3, v4

    float-to-int v0, v6

    .line 455
    .local v0, "anchorX":I
    aget v6, v3, v7

    float-to-int v1, v6

    .line 457
    .local v1, "anchorY":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotation:Landroid/graphics/Matrix;

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotationAngle:F

    int-to-float v8, v0

    int-to-float v9, v1

    invoke-virtual {v6, v7, v8, v9}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 458
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotationInverse:Landroid/graphics/Matrix;

    iget v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotationAngle:F

    neg-float v7, v7

    int-to-float v8, v0

    int-to-float v9, v1

    invoke-virtual {v6, v7, v8, v9}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 461
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 462
    .local v5, "width":I
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 463
    .local v2, "height":I
    iget-boolean v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mIsLeftDrawable:Z

    if-eqz v6, :cond_0

    move v4, v5

    .line 464
    .local v4, "tipOffsetX":I
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    iput v1, v6, Landroid/graphics/Rect;->top:I

    .line 465
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    sub-int v7, v0, v4

    iput v7, v6, Landroid/graphics/Rect;->left:I

    .line 466
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v5

    iput v7, v6, Landroid/graphics/Rect;->right:I

    .line 467
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v2

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    .line 468
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 471
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mSelectRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 472
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mSelectRect:Landroid/graphics/Rect;

    mul-int/lit8 v7, v5, 0x5

    div-int/lit8 v7, v7, 0x8

    neg-int v7, v7

    mul-int/lit8 v8, v2, 0x5

    div-int/lit8 v8, v8, 0x8

    neg-int v8, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->inset(II)V

    .line 473
    iget-object v6, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mSelectRect:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v2

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    .line 474
    return-void
.end method

.method draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "debugPaint"    # Landroid/graphics/Paint;

    .prologue
    .line 416
    # getter for: Lcom/google/android/apps/books/widget/SelectionOverlay;->mIdentityMatrix:Landroid/graphics/Matrix;
    invoke-static {}, Lcom/google/android/apps/books/widget/SelectionOverlay;->access$200()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotation:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 424
    return-void
.end method

.method hitTest(FF)I
    .locals 7
    .param p1, "pageX"    # F
    .param p2, "pageY"    # F

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 433
    new-array v0, v2, [F

    aput p1, v0, v3

    aput p2, v0, v1

    .line 436
    .local v0, "point":[F
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mRotationInverse:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 438
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mHandleRect:Landroid/graphics/Rect;

    aget v5, v0, v3

    float-to-int v5, v5

    aget v6, v0, v1

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 442
    :goto_0
    return v1

    .line 440
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/widget/SelectionOverlay$HandleData;->mSelectRect:Landroid/graphics/Rect;

    aget v5, v0, v3

    float-to-int v5, v5

    aget v1, v0, v1

    float-to-int v1, v1

    invoke-virtual {v4, v5, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    .line 441
    goto :goto_0

    :cond_1
    move v1, v3

    .line 442
    goto :goto_0
.end method
