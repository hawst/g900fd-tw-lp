.class Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader$1;
.super Ljava/lang/Object;
.source "StaggeredCoverView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader$1;->this$1:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 133
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader$1;->this$1:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    iget-object v2, v2, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mViewsNeedingCovers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/BookCoverContainerView;

    .line 134
    .local v1, "view":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/BookCoverContainerView;->loadCoverView(Z)V

    goto :goto_0

    .line 136
    .end local v1    # "view":Lcom/google/android/apps/books/widget/BookCoverContainerView;
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader$1;->this$1:Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;

    iget-object v2, v2, Lcom/google/android/apps/books/widget/StaggeredCoverView$CoverViewLoader;->mViewsNeedingCovers:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 137
    return-void
.end method
