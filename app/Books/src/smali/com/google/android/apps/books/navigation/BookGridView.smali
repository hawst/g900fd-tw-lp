.class public Lcom/google/android/apps/books/navigation/BookGridView;
.super Landroid/widget/FrameLayout;
.source "BookGridView.java"


# instance fields
.field private final mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

.field private mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

.field private mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

.field private mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 44
    new-instance v0, Lcom/google/android/apps/books/navigation/BookGridView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookGridView$1;-><init>(Lcom/google/android/apps/books/navigation/BookGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    new-instance v0, Lcom/google/android/apps/books/navigation/BookGridView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookGridView$1;-><init>(Lcom/google/android/apps/books/navigation/BookGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Lcom/google/android/apps/books/navigation/BookGridView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/navigation/BookGridView$1;-><init>(Lcom/google/android/apps/books/navigation/BookGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/navigation/BookGridView;)Lcom/google/android/ublib/widget/AbsWarpListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/navigation/BookGridView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    if-nez v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->destroy()V

    .line 91
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;->destroy()V

    .line 93
    iput-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .line 94
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->removeBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    goto :goto_0
.end method

.method public getCurrentSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getCurrentSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getSelectedSpreadView()Lcom/google/android/apps/books/navigation/SnapshottingSpreadView;

    move-result-object v0

    goto :goto_0
.end method

.method public onBookmarksChanged()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    const v0, 0x7f0e00c3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/navigation/BookGridView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/ublib/widget/AbsWarpListView;

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setSnapMode(I)V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/ublib/widget/AbsWarpListView;->setGaps(II)V

    .line 42
    return-void
.end method

.method public prepare(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Landroid/graphics/Point;)V
    .locals 9
    .param p1, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p2, "renderer"    # Lcom/google/android/apps/books/render/Renderer;
    .param p3, "currentPosition"    # Lcom/google/android/apps/books/render/SpreadIdentifier;
    .param p4, "callbacks"    # Lcom/google/android/apps/books/widget/BookView$Callbacks;
    .param p6, "fullScreenSpreadSize"    # Landroid/graphics/Point;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeMetadata;",
            "Lcom/google/android/apps/books/render/Renderer;",
            "Lcom/google/android/apps/books/render/SpreadIdentifier;",
            "Lcom/google/android/apps/books/widget/BookView$Callbacks;",
            "Lcom/google/android/apps/books/util/BitmapCache",
            "<",
            "Lcom/google/android/apps/books/navigation/NavPageKey;",
            ">;",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p5, "fallback":Lcom/google/android/apps/books/util/BitmapCache;, "Lcom/google/android/apps/books/util/BitmapCache<Lcom/google/android/apps/books/navigation/NavPageKey;>;"
    new-instance v0, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    invoke-direct {v0}, Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    .line 60
    new-instance v0, Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v7, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mRowStateManager:Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;-><init>(Lcom/google/android/ublib/widget/AbsWarpListView;Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/Renderer;Lcom/google/android/apps/books/render/SpreadIdentifier;Lcom/google/android/apps/books/widget/BookView$Callbacks;Lcom/google/android/apps/books/util/BitmapCache;Lcom/google/android/apps/books/navigation/GridRowInvalidationManager;Landroid/graphics/Point;)V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/books/navigation/GridRowListAdapter;->getCurrentRow()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setCenterLockPosition(I)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mChapterGrid:Lcom/google/android/ublib/widget/AbsWarpListView;

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mGridRowListAdapter:Lcom/google/android/apps/books/navigation/GridRowListAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/ublib/widget/AbsWarpListView;->setAdapter(Lcom/google/android/ublib/widget/AbsWarpListView$Adapter;)V

    .line 66
    invoke-static {}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->getManager()Lcom/google/android/apps/books/util/UIThreadTaskManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/navigation/BookGridView;->mBusyProvider:Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->weaklyAddBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 67
    return-void
.end method
