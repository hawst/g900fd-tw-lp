.class public Lcom/google/android/apps/books/app/OnboardingController;
.super Ljava/lang/Object;
.source "OnboardingController.java"


# instance fields
.field private final mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mBooksServer:Lcom/google/android/apps/books/net/BooksServer;

.field private final mImageManager:Lcom/google/android/apps/books/common/ImageManager;

.field private mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

.field private final mUiExecutor:Lcom/google/android/apps/books/util/HandlerExecutor;

.field private final mVolumeImageConstraints:Lcom/google/android/ublib/util/ImageConstraints;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/net/BooksServer;Lcom/google/android/apps/books/common/ImageManager;Ljava/util/concurrent/ExecutorService;Lcom/google/android/apps/books/util/HandlerExecutor;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/model/RemoteFileCache;)V
    .locals 0
    .param p1, "booksServer"    # Lcom/google/android/apps/books/net/BooksServer;
    .param p2, "booksImageManager"    # Lcom/google/android/apps/books/common/ImageManager;
    .param p3, "backgroundExecutor"    # Ljava/util/concurrent/ExecutorService;
    .param p4, "uiExecutor"    # Lcom/google/android/apps/books/util/HandlerExecutor;
    .param p5, "volumeImageConstraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .param p6, "imageStore"    # Lcom/google/android/apps/books/model/RemoteFileCache;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/books/app/OnboardingController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/books/app/OnboardingController;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/books/app/OnboardingController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/books/app/OnboardingController;->mUiExecutor:Lcom/google/android/apps/books/util/HandlerExecutor;

    .line 57
    iput-object p5, p0, Lcom/google/android/apps/books/app/OnboardingController;->mVolumeImageConstraints:Lcom/google/android/ublib/util/ImageConstraints;

    .line 58
    iput-object p6, p0, Lcom/google/android/apps/books/app/OnboardingController;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/net/BooksServer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OnboardingController;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/app/OnboardingController;->mBooksServer:Lcom/google/android/apps/books/net/BooksServer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/OnboardingController;)Lcom/google/android/apps/books/util/HandlerExecutor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/OnboardingController;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/books/app/OnboardingController;->mUiExecutor:Lcom/google/android/apps/books/util/HandlerExecutor;

    return-object v0
.end method

.method private getImage(Ljava/lang/String;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 6
    .param p1, "imageUrl"    # Ljava/lang/String;
    .param p2, "imageConstraints"    # Lcom/google/android/ublib/util/ImageConstraints;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/util/ImageConstraints;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    new-instance v4, Lcom/google/android/apps/books/app/OnboardingController$3;

    invoke-direct {v4, p0, p3}, Lcom/google/android/apps/books/app/OnboardingController$3;-><init>(Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/ublib/utils/Consumer;)V

    .line 201
    .local v4, "imageCallback":Lcom/google/android/apps/books/common/ImageCallback;
    iget-object v0, p0, Lcom/google/android/apps/books/app/OnboardingController;->mImageManager:Lcom/google/android/apps/books/common/ImageManager;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/books/app/OnboardingController;->mImageStore:Lcom/google/android/apps/books/model/RemoteFileCache;

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/common/ImageManager;->getImage(Landroid/net/Uri;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/apps/books/common/ImageManager$Ensurer;Lcom/google/android/apps/books/common/ImageCallback;Lcom/google/android/apps/books/model/RemoteFileCache;)Lcom/google/android/apps/books/common/ImageFuture;

    .line 203
    return-void
.end method


# virtual methods
.method public addBookToMyLibrary(Ljava/lang/String;)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 206
    const-string v0, "OnboardingController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    const-string v0, "OnboardingController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addBookToMyLibrary: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/OnboardingController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/app/OnboardingController$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/OnboardingController$4;-><init>(Lcom/google/android/apps/books/app/OnboardingController;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 223
    return-void
.end method

.method public getCategoryImage(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "imageUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 178
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/app/OnboardingController;->getImage(Ljava/lang/String;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/utils/Consumer;)V

    .line 179
    return-void
.end method

.method public getSampleCategories(Lcom/google/android/ublib/utils/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;>;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/OnboardingController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/books/app/OnboardingController$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/app/OnboardingController$1;-><init>(Lcom/google/android/apps/books/app/OnboardingController;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method public getSampleVolumes(Ljava/util/List;ILjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 7
    .param p2, "pageSize"    # I
    .param p3, "nextPageToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/SampleVolumes;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/SampleVolumes;>;>;"
    const-string v0, "OnboardingController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "OnboardingController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requesting samples nextPageToken="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pageSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/books/app/OnboardingController;->mBackgroundExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/google/android/apps/books/app/OnboardingController$2;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/app/OnboardingController$2;-><init>(Lcom/google/android/apps/books/app/OnboardingController;Ljava/util/List;ILjava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 159
    return-void
.end method

.method public getVolumeImage(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .param p1, "imageUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/app/OnboardingController;->mVolumeImageConstraints:Lcom/google/android/ublib/util/ImageConstraints;

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/app/OnboardingController;->getImage(Ljava/lang/String;Lcom/google/android/ublib/util/ImageConstraints;Lcom/google/android/ublib/utils/Consumer;)V

    .line 169
    return-void
.end method
