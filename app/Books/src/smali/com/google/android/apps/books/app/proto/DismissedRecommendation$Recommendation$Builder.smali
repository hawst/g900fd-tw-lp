.class public final Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "DismissedRecommendation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;",
        "Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 1

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 3

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;-><init>()V

    .line 147
    .local v0, "builder":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    new-instance v1, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;-><init>(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$1;)V

    iput-object v1, v0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .line 148
    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    invoke-static {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->buildPartial()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .locals 3

    .prologue
    .line 192
    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    if-nez v1, :cond_0

    .line 193
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "build() has already been called on this Builder."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .line 197
    .local v0, "returnMe":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .line 198
    return-object v0
.end method

.method public clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 2

    .prologue
    .line 165
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->create()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->mergeFrom(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->clone()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public mergeFrom(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 1
    .param p1, "other"    # Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .prologue
    .line 202
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->getDefaultInstance()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-object p0

    .line 203
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->hasVolumeId()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    invoke-virtual {p1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->getVolumeId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    goto :goto_0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "extensionRegistry"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 215
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 219
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 220
    :sswitch_0
    return-object p0

    .line 225
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    goto :goto_0

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/CodedInputStream;
    .param p2, "x1"    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 241
    if-nez p1, :cond_0

    .line 242
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->hasVolumeId:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->access$302(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;Z)Z

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->result:Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    # setter for: Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->volumeId_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->access$402(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;Ljava/lang/String;)Ljava/lang/String;

    .line 246
    return-object p0
.end method
