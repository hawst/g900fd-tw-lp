.class Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;
.super Ljava/lang/Object;
.source "RecommendationsClusterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/RecommendationsClusterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RecommendationInfo"
.end annotation


# instance fields
.field private final mColumns:I

.field private final mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

.field private final mRows:I


# direct methods
.method constructor <init>(Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;II)V
    .locals 0
    .param p1, "metadata"    # Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .param p2, "columns"    # I
    .param p3, "rows"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    .line 42
    iput p2, p0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->mColumns:I

    .line 43
    iput p3, p0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->mRows:I

    .line 44
    return-void
.end method


# virtual methods
.method public getCardType()Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->mMetadata:Lcom/google/android/ublib/cardlib/layout/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method public getColumns()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->mColumns:I

    return v0
.end method

.method public getRows()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/apps/books/app/RecommendationsClusterBuilder$RecommendationInfo;->mRows:I

    return v0
.end method
