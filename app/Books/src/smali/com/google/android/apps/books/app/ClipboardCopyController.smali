.class public Lcom/google/android/apps/books/app/ClipboardCopyController;
.super Ljava/lang/Object;
.source "ClipboardCopyController.java"


# instance fields
.field final mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

.field private mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

.field private mUnsyncedCopiesCharacterCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/apps/books/app/ClipboardCopyController$1;

    sget-object v1, Lcom/google/android/apps/books/annotations/LayerId;->COPY:Lcom/google/android/apps/books/annotations/LayerId;

    invoke-virtual {v1}, Lcom/google/android/apps/books/annotations/LayerId;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/books/app/ClipboardCopyController$1;-><init>(Lcom/google/android/apps/books/app/ClipboardCopyController;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/books/app/ClipboardCopyController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ClipboardCopyController;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I

    return p1
.end method

.method static synthetic access$012(Lcom/google/android/apps/books/app/ClipboardCopyController;I)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ClipboardCopyController;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iget v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/books/app/ClipboardCopyController;Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;)Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ClipboardCopyController;
    .param p1, "x1"    # Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    return-object p1
.end method


# virtual methods
.method public getAllowedCharacterCount()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->getAllowedCharacterCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAnnotationListener()Lcom/google/android/apps/books/annotations/AnnotationListener;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mAnnotationListener:Lcom/google/android/apps/books/annotations/AnnotationListener;

    return-object v0
.end method

.method public getRemainingCharacterCount()I
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->getRemainingCharacterCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClipboardCopyingPermitted(I)Z
    .locals 4
    .param p1, "textLength"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    if-nez v2, :cond_1

    move v0, v1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v2}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->hasLimit()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    iget-object v2, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    iget v2, v2, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->remainingCharacters:I

    iget v3, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mUnsyncedCopiesCharacterCount:I

    sub-int/2addr v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isCopyingLimited()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ClipboardCopyController;->mCopyQuota:Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Layer$CharacterQuota;->hasLimit()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
