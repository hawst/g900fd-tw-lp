.class Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;
.super Ljava/lang/Object;
.source "LayerAnnotationLoader.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->load(ILcom/google/android/ublib/utils/Consumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Lcom/google/android/apps/books/util/Nothing;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

.field final synthetic val$consumer:Lcom/google/android/ublib/utils/Consumer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    iput-object p2, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "t":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    iget-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    # getter for: Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mPassageAnnotationLoadResult:Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;
    invoke-static {v1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->access$200(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)Lcom/google/android/apps/books/geo/LayerAnnotationLoader$PassageAnnotationLoadResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;->val$consumer:Lcom/google/android/ublib/utils/Consumer;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 101
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$4;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
