.class Lcom/google/android/apps/books/widget/BookView$PageDrawingData;
.super Ljava/lang/Object;
.source "BookView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BookView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PageDrawingData"
.end annotation


# instance fields
.field bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

.field content:Lcom/google/android/apps/books/util/SimpleDrawable;

.field specialPageDisplaySize:Landroid/graphics/Point;

.field specialPageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V
    .locals 0
    .param p1, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p2, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;
    .param p3, "specialPageType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p4, "specialPageDisplaySize"    # Landroid/graphics/Point;

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->content:Lcom/google/android/apps/books/util/SimpleDrawable;

    .line 200
    iput-object p3, p0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPageType:Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;

    .line 201
    iput-object p2, p0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->bookmark:Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .line 202
    iput-object p4, p0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;->specialPageDisplaySize:Landroid/graphics/Point;

    .line 203
    return-void
.end method

.method static loading(Landroid/graphics/Point;)Lcom/google/android/apps/books/widget/BookView$PageDrawingData;
    .locals 2
    .param p0, "displaySize"    # Landroid/graphics/Point;

    .prologue
    const/4 v1, 0x0

    .line 206
    new-instance v0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    invoke-direct {v0, v1, v1, v1, p0}, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V

    return-object v0
.end method

.method static normalPage(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)Lcom/google/android/apps/books/widget/BookView$PageDrawingData;
    .locals 2
    .param p0, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p1, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    const/4 v1, 0x0

    .line 210
    new-instance v0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V

    return-object v0
.end method

.method static specialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Bitmap;Landroid/graphics/Point;)Lcom/google/android/apps/books/widget/BookView$PageDrawingData;
    .locals 3
    .param p0, "type"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 215
    new-instance v0, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;

    new-instance v1, Lcom/google/android/apps/books/util/BitmapSimpleDrawable;

    invoke-direct {v1, p1}, Lcom/google/android/apps/books/util/BitmapSimpleDrawable;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0, p2}, Lcom/google/android/apps/books/widget/BookView$PageDrawingData;-><init>(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Landroid/graphics/Point;)V

    return-object v0
.end method
