.class public interface abstract Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer;
.super Ljava/lang/Object;
.source "AudioClipPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/AudioClipPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IMediaPlayer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;
    }
.end annotation


# virtual methods
.method public abstract isPlaying()Z
.end method

.method public abstract release()V
.end method

.method public abstract reset()V
.end method

.method public abstract seekTo(I)V
.end method

.method public abstract setDataSourceAndPreparePlayer(Lcom/google/android/apps/books/app/AudioClipPlayer$Clip;I)V
.end method

.method public abstract setListener(Lcom/google/android/apps/books/app/AudioClipPlayer$IMediaPlayer$Listener;)V
.end method

.method public abstract start()V
.end method
