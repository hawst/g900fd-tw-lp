.class Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;
.super Ljava/lang/Object;
.source "TokenAuthAuthenticationHelper.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SidTokenHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field final synthetic this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Landroid/accounts/Account;)V
    .locals 0
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p2, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->mAccount:Landroid/accounts/Account;

    .line 109
    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "value":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    const-string v2, "authtoken"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 114
    .local v8, "sid":Ljava/lang/String;
    new-instance v5, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;

    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    invoke-direct {v5, v1, v8}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;-><init>(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;Ljava/lang/String;)V

    .line 115
    .local v5, "callback":Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    iget-object v1, v1, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    iget-object v1, v1, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->mClient:Lcom/google/android/apps/books/util/BrowserAuthenticationHelper$Client;

    check-cast v1, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;

    invoke-interface {v1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$TokenAuthClient;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 117
    .local v4, "activity":Landroid/app/Activity;
    if-eqz v4, :cond_0

    .line 118
    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 119
    .local v0, "am":Landroid/accounts/AccountManager;
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->mAccount:Landroid/accounts/Account;

    const-string v2, "LSID"

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 134
    .end local v0    # "am":Landroid/accounts/AccountManager;
    .end local v4    # "activity":Landroid/app/Activity;
    .end local v5    # "callback":Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$LsidTokenHandler;
    .end local v8    # "sid":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v7

    .line 123
    .local v7, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "TokenAuthHelper"

    const-string v2, "cancelled while acquiring token"

    invoke-static {v1, v2, v7}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V
    invoke-static {v1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V

    goto :goto_0

    .line 125
    .end local v7    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v7

    .line 126
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    const-string v1, "TokenAuthHelper"

    const-string v2, "authentication error while acquiring token"

    invoke-static {v1, v2, v7}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V
    invoke-static {v1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V

    goto :goto_0

    .line 128
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v7

    .line 129
    .local v7, "e":Ljava/io/IOException;
    const-string v1, "TokenAuthHelper"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 130
    const-string v1, "TokenAuthHelper"

    const-string v2, "IO error while acquiring token"

    invoke-static {v1, v2, v7}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper$SidTokenHandler;->this$0:Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;

    # invokes: Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->abort()V
    invoke-static {v1}, Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;->access$000(Lcom/google/android/apps/books/util/TokenAuthAuthenticationHelper;)V

    goto :goto_0
.end method
