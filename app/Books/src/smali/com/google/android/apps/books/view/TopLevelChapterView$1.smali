.class Lcom/google/android/apps/books/view/TopLevelChapterView$1;
.super Ljava/lang/Object;
.source "TopLevelChapterView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/view/TopLevelChapterView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/view/TopLevelChapterView;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    # getter for: Lcom/google/android/apps/books/view/TopLevelChapterView;->mView:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->access$100(Lcom/google/android/apps/books/view/TopLevelChapterView;)Landroid/widget/ExpandableListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    # getter for: Lcom/google/android/apps/books/view/TopLevelChapterView;->mGroupIndex:I
    invoke-static {v1}, Lcom/google/android/apps/books/view/TopLevelChapterView;->access$000(Lcom/google/android/apps/books/view/TopLevelChapterView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    # getter for: Lcom/google/android/apps/books/view/TopLevelChapterView;->mView:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->access$100(Lcom/google/android/apps/books/view/TopLevelChapterView;)Landroid/widget/ExpandableListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    # getter for: Lcom/google/android/apps/books/view/TopLevelChapterView;->mGroupIndex:I
    invoke-static {v1}, Lcom/google/android/apps/books/view/TopLevelChapterView;->access$000(Lcom/google/android/apps/books/view/TopLevelChapterView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    # getter for: Lcom/google/android/apps/books/view/TopLevelChapterView;->mView:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lcom/google/android/apps/books/view/TopLevelChapterView;->access$100(Lcom/google/android/apps/books/view/TopLevelChapterView;)Landroid/widget/ExpandableListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/view/TopLevelChapterView$1;->this$0:Lcom/google/android/apps/books/view/TopLevelChapterView;

    # getter for: Lcom/google/android/apps/books/view/TopLevelChapterView;->mGroupIndex:I
    invoke-static {v1}, Lcom/google/android/apps/books/view/TopLevelChapterView;->access$000(Lcom/google/android/apps/books/view/TopLevelChapterView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_0
.end method
