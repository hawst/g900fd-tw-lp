.class Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateOperation"
.end annotation


# instance fields
.field private final mShouldPersist:Z

.field private final mUpdater:Lcom/google/android/ublib/utils/Updater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Updater",
            "<",
            "Lcom/google/android/apps/books/upload/Upload$Uploads;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/ublib/utils/Updater;Z)V
    .locals 0
    .param p2, "shouldPersist"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/ublib/utils/Updater",
            "<",
            "Lcom/google/android/apps/books/upload/Upload$Uploads;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "updater":Lcom/google/android/ublib/utils/Updater;, "Lcom/google/android/ublib/utils/Updater<Lcom/google/android/apps/books/upload/Upload$Uploads;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;->mUpdater:Lcom/google/android/ublib/utils/Updater;

    .line 75
    iput-boolean p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;->mShouldPersist:Z

    .line 76
    return-void
.end method


# virtual methods
.method public shouldPersist()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;->mShouldPersist:Z

    return v0
.end method

.method public update(Lcom/google/android/apps/books/upload/Upload$Uploads;)Lcom/google/android/apps/books/upload/Upload$Uploads;
    .locals 1
    .param p1, "uploads"    # Lcom/google/android/apps/books/upload/Upload$Uploads;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$UpdateOperation;->mUpdater:Lcom/google/android/ublib/utils/Updater;

    invoke-interface {v0, p1}, Lcom/google/android/ublib/utils/Updater;->update(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/Upload$Uploads;

    return-object v0
.end method
