.class public Lcom/google/android/apps/books/app/NotesExportDialogFragment;
.super Lcom/google/android/apps/books/app/BooksDialogFragment;
.source "NotesExportDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;
    }
.end annotation


# instance fields
.field private mCheckbox:Landroid/widget/CheckBox;

.field private mFolderEdit:Landroid/widget/EditText;

.field mListener:Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BooksDialogFragment;-><init>()V

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/app/NotesExportDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->currentCheckbox()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/NotesExportDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->currentFolder()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private currentCheckbox()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method private currentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mFolderEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(ZLjava/lang/String;)Lcom/google/android/apps/books/app/NotesExportDialogFragment;
    .locals 3
    .param p0, "isEnabled"    # Z
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 55
    new-instance v1, Lcom/google/android/apps/books/app/NotesExportDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;-><init>()V

    .line 56
    .local v1, "dialog":Lcom/google/android/apps/books/app/NotesExportDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "ne_enabled"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    const-string v2, "ne_folder"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->setRetainInstance(Z)V

    .line 61
    return-object v1
.end method


# virtual methods
.method protected getEnabled()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ne_enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected getFolder()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ne_folder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/BooksDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 67
    check-cast p1, Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mListener:Lcom/google/android/apps/books/app/NotesExportDialogFragment$NotesExportDialogListener;

    .line 68
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 74
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04006d

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 75
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0e0088

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    .line 76
    const v3, 0x7f0e0144

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mFolderEdit:Landroid/widget/EditText;

    .line 78
    iget-object v3, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->getEnabled()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 79
    iget-object v3, p0, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->mFolderEdit:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment;->getFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v1, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/app/NotesExportDialogFragment$1;-><init>(Lcom/google/android/apps/books/app/NotesExportDialogFragment;)V

    .line 90
    .local v1, "okListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0f0160

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    invoke-virtual {v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method
