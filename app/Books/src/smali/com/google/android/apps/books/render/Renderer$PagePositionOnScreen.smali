.class public final enum Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
.super Ljava/lang/Enum;
.source "Renderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/Renderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PagePositionOnScreen"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field public static final enum FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field public static final enum LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

.field public static final enum RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;


# instance fields
.field private final mLeftLetterboxFraction:F

.field private final mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 60
    new-instance v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    const-string v1, "FULL_SCREEN"

    sget-object v2, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/render/Renderer$SideOfSpine;F)V

    sput-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 61
    new-instance v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    const-string v1, "LEFT_PAGE_OF_TWO"

    sget-object v2, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/render/Renderer$SideOfSpine;F)V

    sput-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 62
    new-instance v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    const-string v1, "RIGHT_PAGE_OF_TWO"

    sget-object v2, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;-><init>(Ljava/lang/String;ILcom/google/android/apps/books/render/Renderer$SideOfSpine;F)V

    sput-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 59
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->$VALUES:[Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/books/render/Renderer$SideOfSpine;F)V
    .locals 0
    .param p3, "displayStyle"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .param p4, "leftLetterboxFraction"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/render/Renderer$SideOfSpine;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 68
    iput-object p3, p0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 69
    iput p4, p0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->mLeftLetterboxFraction:F

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    const-class v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->$VALUES:[Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    return-object v0
.end method


# virtual methods
.method public getLeftLetterboxFraction()F
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->mLeftLetterboxFraction:F

    return v0
.end method

.method public getSideOfSpine()Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->mSideOfSpine:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    return-object v0
.end method
