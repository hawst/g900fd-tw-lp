.class Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;
.super Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;
.source "BooksCardsHomeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/BooksCardsHomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AboutThisBookMenuEntry"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;)V
    .locals 2
    .param p2, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    .line 586
    const v0, 0x7f0f00c4

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$VolumeMenuEntry;-><init>(Lcom/google/android/apps/books/widget/BooksCardsHomeView;Ljava/lang/String;IZ)V

    .line 587
    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 2

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;->this$0:Lcom/google/android/apps/books/widget/BooksCardsHomeView;

    # getter for: Lcom/google/android/apps/books/widget/BooksCardsHomeView;->mHomeController:Lcom/google/android/apps/books/widget/BooksHomeController;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView;->access$100(Lcom/google/android/apps/books/widget/BooksCardsHomeView;)Lcom/google/android/apps/books/widget/BooksHomeController;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/BooksCardsHomeView$AboutThisBookMenuEntry;->getVolumeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/BooksHomeController;->viewAboutThisBook(Ljava/lang/String;)V

    .line 592
    return-void
.end method
