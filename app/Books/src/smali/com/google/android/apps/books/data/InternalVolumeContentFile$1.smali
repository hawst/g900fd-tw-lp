.class Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;
.super Ljava/lang/Object;
.source "InternalVolumeContentFile.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/InternalVolumeContentFile;->createSaver(Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)Lcom/google/android/apps/books/model/BooksDataStore$ContentSaver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

.field final synthetic val$progressListener:Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;

.field final synthetic val$saveListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

.field final synthetic val$tempFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    iput-object p2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$tempFile:Ljava/io/File;

    iput-object p3, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$progressListener:Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;

    iput-object p4, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$saveListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public saveTemp(Lcom/google/android/apps/books/model/EncryptedContent;)Lcom/google/android/apps/books/model/BooksDataStore$Committer;
    .locals 6
    .param p1, "content"    # Lcom/google/android/apps/books/model/EncryptedContent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    if-eqz p1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    invoke-interface {p1}, Lcom/google/android/apps/books/model/EncryptedContent;->getContent()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$tempFile:Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$progressListener:Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;

    # invokes: Lcom/google/android/apps/books/data/InternalVolumeContentFile;->writeToContentFile(Ljava/io/InputStream;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->access$400(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Ljava/io/InputStream;Ljava/io/File;Lcom/google/android/apps/books/util/IOUtils$StreamProgressListener;)V

    .line 147
    iget-object v4, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$tempFile:Ljava/io/File;

    .line 152
    .local v4, "tempFileToCommit":Ljava/io/File;
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;

    iget-object v1, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->this$0:Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    iget-object v2, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$saveListener:Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;

    const/4 v5, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/data/InternalVolumeContentFile$VolumeContentCommitter;-><init>(Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;Lcom/google/android/apps/books/model/EncryptedContent;Ljava/io/File;Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;)V

    return-object v0

    .line 149
    .end local v4    # "tempFileToCommit":Ljava/io/File;
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/InternalVolumeContentFile$1;->val$tempFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 150
    const/4 v4, 0x0

    .restart local v4    # "tempFileToCommit":Ljava/io/File;
    goto :goto_0
.end method
