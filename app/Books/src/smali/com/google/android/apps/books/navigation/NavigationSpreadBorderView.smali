.class public Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;
.super Landroid/view/View;
.source "NavigationSpreadBorderView.java"


# instance fields
.field private mLeftSideLoading:Z

.field private final mLoadingColor:I

.field private final mLoadingRegion:Landroid/graphics/Rect;

.field private mRightSideLoading:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 25
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0101a3

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getThemeColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingColor:I

    .line 21
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0101a3

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getThemeColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingColor:I

    .line 21
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x1

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0101a3

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getThemeColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingColor:I

    .line 21
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I

    .prologue
    const/4 v2, 0x1

    .line 38
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0101a3

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/ViewUtils;->getThemeColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingColor:I

    .line 21
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    .line 22
    iput-boolean v2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    .line 39
    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x0

    .line 58
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    .line 60
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/Rect;->union(IIII)V

    .line 63
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    if-eqz v1, :cond_1

    .line 64
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->getHeight()I

    move-result v4

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/graphics/Rect;->union(IIII)V

    .line 68
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    move-result v0

    .line 69
    .local v0, "checkpoint":I
    iget-object v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingRegion:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 70
    iget v1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLoadingColor:I

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 71
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 72
    return-void
.end method

.method public setLoadingSides(ZZ)V
    .locals 1
    .param p1, "left"    # Z
    .param p2, "right"    # Z

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    if-ne p2, v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mLeftSideLoading:Z

    .line 50
    iput-boolean p2, p0, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->mRightSideLoading:Z

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/books/navigation/NavigationSpreadBorderView;->invalidate()V

    goto :goto_0
.end method
