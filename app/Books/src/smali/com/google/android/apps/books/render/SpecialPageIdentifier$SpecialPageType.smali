.class public final enum Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;
.super Ljava/lang/Enum;
.source "SpecialPageIdentifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/SpecialPageIdentifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpecialPageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

.field public static final enum OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

.field public static final enum PREVIEW_GAP:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    const-string v1, "PREVIEW_GAP"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->PREVIEW_GAP:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    .line 19
    new-instance v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    const-string v1, "OUT_OF_BOUNDS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    .line 10
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    sget-object v1, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->PREVIEW_GAP:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->OUT_OF_BOUNDS:Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->$VALUES:[Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->$VALUES:[Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/render/SpecialPageIdentifier$SpecialPageType;

    return-object v0
.end method
