.class public Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;
.super Lcom/google/android/apps/books/app/InfoCardProvider;
.source "DictionaryCardProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;
    }
.end annotation


# instance fields
.field private final mAnnotationServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

.field private final mBookLocale:Ljava/util/Locale;

.field private final mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mContext:Landroid/content/Context;

.field private final mOfflineDictionaryEnabled:Z

.field private final mUserLocale:Ljava/util/Locale;

.field private final mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/annotations/VolumeVersion;Lcom/google/android/apps/books/annotations/AnnotationServer;Ljava/util/Locale;Ljava/util/Locale;Lcom/google/android/apps/books/data/BooksDataController;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "volumeVersion"    # Lcom/google/android/apps/books/annotations/VolumeVersion;
    .param p3, "annotationServer"    # Lcom/google/android/apps/books/annotations/AnnotationServer;
    .param p4, "userLocale"    # Ljava/util/Locale;
    .param p5, "bookLocale"    # Ljava/util/Locale;
    .param p6, "booksDataController"    # Lcom/google/android/apps/books/data/BooksDataController;
    .param p7, "offlineDictionaryEnabled"    # Z

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/books/app/InfoCardProvider;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mContext:Landroid/content/Context;

    .line 73
    iput-object p2, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    .line 74
    iput-object p3, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mAnnotationServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    .line 75
    invoke-static {p4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iput-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mUserLocale:Ljava/util/Locale;

    .line 76
    iput-object p5, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBookLocale:Ljava/util/Locale;

    .line 77
    iput-object p6, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 78
    iput-boolean p7, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mOfflineDictionaryEnabled:Z

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBookLocale:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Ljava/util/Locale;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mUserLocale:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Lcom/google/android/apps/books/annotations/VolumeVersion;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mVolumeVersion:Lcom/google/android/apps/books/annotations/VolumeVersion;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Lcom/google/android/apps/books/annotations/AnnotationServer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mAnnotationServer:Lcom/google/android/apps/books/annotations/AnnotationServer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mOfflineDictionaryEnabled:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;)Lcom/google/android/apps/books/data/BooksDataController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mBooksDataController:Lcom/google/android/apps/books/data/BooksDataController;

    return-object v0
.end method

.method public static wordToDefine(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 7
    .param p0, "selection"    # Ljava/lang/String;
    .param p1, "textLocale"    # Ljava/util/Locale;

    .prologue
    .line 90
    invoke-static {p0, p1}, Lcom/google/android/apps/books/util/TextSegmentation;->findWords(Ljava/lang/String;Ljava/util/Locale;)Lcom/google/android/apps/books/util/TextSegmentation;

    move-result-object v4

    .line 91
    .local v4, "segmentation":Lcom/google/android/apps/books/util/TextSegmentation;
    const/4 v3, 0x0

    .line 92
    .local v3, "result":Ljava/lang/String;
    const/4 v1, 0x0

    .line 93
    .local v1, "lastOffset":I
    iget-object v6, v4, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 94
    .local v2, "offset":I
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "value":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isGraphic(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 96
    if-eqz v3, :cond_1

    .line 98
    const/4 v3, 0x0

    .line 104
    .end local v2    # "offset":I
    .end local v3    # "result":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_0
    return-object v3

    .line 100
    .restart local v2    # "offset":I
    .restart local v3    # "result":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_1
    move-object v3, v5

    .line 102
    :cond_2
    move v1, v2

    .line 103
    goto :goto_0
.end method


# virtual methods
.method public loadCards(Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 3
    .param p1, "selection"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Ljava/util/List<Landroid/view/View;>;>;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->mUserLocale:Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;->wordToDefine(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "word":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 111
    new-instance v1, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/apps/books/dictionary/DictionaryCardProvider$LookupTask;-><init>(Lcom/google/android/apps/books/dictionary/DictionaryCardProvider;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v1, v2}, Lcom/google/android/ublib/utils/SystemUtils;->executeTaskOnThreadPool(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 113
    :cond_0
    return-void
.end method
