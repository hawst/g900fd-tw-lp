.class public Lcom/google/android/apps/books/data/OffersSubcontroller;
.super Ljava/lang/Object;
.source "OffersSubcontroller.java"


# instance fields
.field private mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

.field private final mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerList",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;

.field private mDismissedOffers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOwnedVolumes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/books/data/ExceptionOrConsumerList",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/DeviceInfo;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/google/android/apps/books/app/DeviceInfo;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->createExceptionOrList()Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    .line 57
    invoke-static {}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->createExceptionOrList()Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    .line 61
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->saveDismissedOfferOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/app/DeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->publishFailure(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/data/OffersSubcontroller;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDismissedOffers:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getFilteredCachedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "x2"    # Ljava/util/List;
    .param p3, "x3"    # Z
    .param p4, "x4"    # J

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/data/OffersSubcontroller;->notifyListeners(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Lcom/google/android/apps/books/api/data/ApiaryOffers;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->saveServerOffersOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/util/List;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/google/android/ublib/utils/Consumer;
    .param p4, "x4"    # Lcom/google/android/ublib/utils/Consumer;
    .param p5, "x5"    # J

    .prologue
    .line 37
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/data/OffersSubcontroller;->publishResult(Ljava/util/List;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;J)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/data/OffersSubcontroller;)Lcom/google/android/apps/books/data/ExceptionOrConsumerList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/data/OffersSubcontroller;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    return-object v0
.end method

.method private cachedOffersStillFresh()Z
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;->getLastModifiedMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fetchCachedOffersFromDataStore(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    .locals 1
    .param p1, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDeviceInfo:Lcom/google/android/apps/books/app/DeviceInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/DeviceInfo;->useFakeOffersData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getMockTestingOffersData()Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/BooksDataStore;->getCachedOffers()Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    move-result-object v0

    goto :goto_0
.end method

.method private getDismissedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/Set;
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDismissedOffers:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 251
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getDismissedOffers()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDismissedOffers:Ljava/util/Set;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mDismissedOffers:Ljava/util/Set;

    return-object v1

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "OffersSubcontroller"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    const-string v1, "OffersSubcontroller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error fetching dismissed offers: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getFilteredCachedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    if-nez v0, :cond_0

    .line 222
    const/4 v0, 0x0

    .line 224
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;->getOffers()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getFilteredOffers(Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private getFilteredOffers(Ljava/util/List;Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;
    .locals 7
    .param p2, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    .local p1, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getDismissedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/Set;

    move-result-object v0

    .line 233
    .local v0, "dismissedOffers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getOwnedVolumes(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/Set;

    move-result-object v5

    .line 234
    .local v5, "ownedVolumes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 235
    .local v2, "filteredOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/OfferData;

    .line 236
    .local v4, "offer":Lcom/google/android/apps/books/model/OfferData;
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/OfferData;->getOfferId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->isDismissedOffer(Ljava/lang/String;Ljava/util/Set;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 237
    invoke-static {v4, v5}, Lcom/google/android/apps/books/model/OfferData;->fromOfferDataWithFiltering(Lcom/google/android/apps/books/model/OfferData;Ljava/util/Set;)Lcom/google/android/apps/books/model/OfferData;

    move-result-object v1

    .line 239
    .local v1, "filteredOffer":Lcom/google/android/apps/books/model/OfferData;
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/OfferData;->getOfferedBooks()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 240
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 244
    .end local v1    # "filteredOffer":Lcom/google/android/apps/books/model/OfferData;
    .end local v4    # "offer":Lcom/google/android/apps/books/model/OfferData;
    :cond_1
    return-object v2
.end method

.method private getMockTestingOffersData()Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 131
    new-instance v2, Ljava/io/File;

    sget-object v4, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    const-string v6, "offers.json"

    invoke-direct {v2, v4, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 135
    .local v2, "file":Ljava/io/File;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 136
    .local v3, "is":Ljava/io/InputStream;
    new-instance v4, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v4}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-virtual {v4, v3}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonParser(Ljava/io/InputStream;)Lcom/google/api/client/json/JsonParser;

    move-result-object v4

    const-class v6, Lcom/google/android/apps/books/api/data/ApiaryOffers;

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/api/client/json/JsonParser;->parse(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/ApiaryOffers;

    .line 139
    .local v0, "apiaryOffers":Lcom/google/android/apps/books/api/data/ApiaryOffers;
    new-instance v4, Lcom/google/android/apps/books/model/CachedOffersImpl;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v4, v0, v6, v7}, Lcom/google/android/apps/books/model/CachedOffersImpl;-><init>(Lcom/google/android/apps/books/api/data/ApiaryOffers;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    .end local v0    # "apiaryOffers":Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .end local v3    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v4

    .line 140
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "OffersSubcontroller"

    const/4 v6, 0x6

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    const-string v4, "OffersSubcontroller"

    const-string v6, "Failed to load \'offers.json\'"

    invoke-static {v4, v6, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    const-string v4, "OffersSubcontroller"

    const-string v6, "Did you run \'adb push tablet/assets_for_testing/offers.json /sdcard/Download/\'?"

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v4, v5

    .line 147
    goto :goto_0
.end method

.method private getOffersOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 262
    new-instance v0, Lcom/google/android/apps/books/data/OffersSubcontroller$4;

    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const-string v2, ""

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/data/OffersSubcontroller$4;-><init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 273
    return-void
.end method

.method private getOwnedVolumes(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/Set;
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mOwnedVolumes:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 112
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore;->getOwnedVolumeIds()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mOwnedVolumes:Ljava/util/Set;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mOwnedVolumes:Ljava/util/Set;

    return-object v1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "OffersSubcontroller"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    const-string v1, "OffersSubcontroller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error loading volume data from data store: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isDismissedOffer(Ljava/lang/String;Ljava/util/Set;)Z
    .locals 1
    .param p1, "offerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 217
    .local p2, "dismissedOffers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyListeners(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/util/List;ZJ)V
    .locals 4
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p3, "fromServer"    # Z
    .param p4, "lastUpdateTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;ZJ)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p2, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->copyListeners()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/BooksDataListener;

    .line 308
    .local v1, "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    invoke-interface {v1, p2, p3, p4, p5}, Lcom/google/android/apps/books/model/BooksDataListener;->onOffersData(Ljava/util/List;ZJ)V

    goto :goto_0

    .line 310
    .end local v1    # "listener":Lcom/google/android/apps/books/model/BooksDataListener;
    :cond_0
    return-void
.end method

.method private publishFailure(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishFailure(Ljava/lang/Exception;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishFailure(Ljava/lang/Exception;)V

    .line 337
    return-void
.end method

.method private publishFailureOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 326
    new-instance v0, Lcom/google/android/apps/books/data/OffersSubcontroller$6;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller$6;-><init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 332
    return-void
.end method

.method private publishResult(Ljava/util/List;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;J)V
    .locals 3
    .param p2, "fromServer"    # Z
    .param p5, "lastUpdateTimeMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/OfferData;",
            ">;Z",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;J)V"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    .local p3, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;>;"
    .local p4, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;

    invoke-direct {v0, p1, p2, p5, p6}, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;-><init>(Ljava/util/List;ZJ)V

    .line 318
    .local v0, "response":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v1, v0, p3}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishSuccess(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 319
    if-nez p2, :cond_0

    .line 320
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    sget-object v2, Lcom/google/android/apps/books/util/ExceptionOr;->OPAQUE_SUCCESS:Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-virtual {v1, v2, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->publishResult(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V

    .line 322
    :cond_0
    return-void
.end method

.method private saveDismissedOfferOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Ljava/lang/String;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "offerId"    # Ljava/lang/String;

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/apps/books/data/OffersSubcontroller$3;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller$3;-><init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 214
    return-void
.end method

.method private saveServerOffersOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    .locals 1
    .param p1, "services"    # Lcom/google/android/apps/books/data/NetworkTaskServices;
    .param p2, "apiaryOffers"    # Lcom/google/android/apps/books/api/data/ApiaryOffers;

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/apps/books/data/OffersSubcontroller$5;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller$5;-><init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/api/data/ApiaryOffers;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/NetworkTaskServices;->executeControlTask(Lcom/google/android/apps/books/data/ControlTask;)V

    .line 300
    return-void
.end method


# virtual methods
.method public acceptOffer(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 7
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "offerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p3, "volumeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "onResult":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/ApiaryOfferRedemptionResponse;>;>;"
    new-instance v0, Lcom/google/android/apps/books/data/OffersSubcontroller$2;

    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const-string v3, ""

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/data/OffersSubcontroller$2;-><init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/ublib/utils/Consumer;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 195
    return-void
.end method

.method public dismissOffer(Lcom/google/android/apps/books/data/ControlTaskServices;Ljava/lang/String;)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .param p2, "offerId"    # Ljava/lang/String;

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/apps/books/data/OffersSubcontroller$1;

    sget-object v1, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    const-string v2, ""

    invoke-direct {v0, p0, v1, v2, p2}, Lcom/google/android/apps/books/data/OffersSubcontroller$1;-><init>(Lcom/google/android/apps/books/data/OffersSubcontroller;Lcom/google/android/apps/books/data/BooksDataController$Priority;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->executeNetworkTask(Lcom/google/android/apps/books/data/NetworkTask;)V

    .line 175
    return-void
.end method

.method public getOffers(ZZLcom/google/android/apps/books/data/ControlTaskServices;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 10
    .param p1, "localOnly"    # Z
    .param p2, "reloadLibrary"    # Z
    .param p3, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;",
            ">;>;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/util/Nothing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p4, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;>;"
    .local p5, "saveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    const/4 v5, 0x0

    .line 68
    if-eqz p2, :cond_0

    .line 69
    iput-object v5, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mOwnedVolumes:Ljava/util/Set;

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    if-nez v1, :cond_1

    .line 73
    :try_start_0
    invoke-interface {p3}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/books/data/OffersSubcontroller;->fetchCachedOffersFromDataStore(Lcom/google/android/apps/books/model/BooksDataStore;)Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/books/data/OffersSubcontroller;->cachedOffersStillFresh()Z

    move-result v8

    .line 81
    .local v8, "freshOffers":Z
    if-nez v8, :cond_2

    if-eqz p1, :cond_7

    .line 83
    :cond_2
    if-eqz v8, :cond_3

    move-object v5, p5

    .line 85
    .local v5, "localSaveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :cond_3
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getFilteredCachedOffers(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mCachedOffers:Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;

    invoke-interface {v1}, Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;->getLastModifiedMillis()J

    move-result-wide v6

    :goto_1
    move-object v1, p0

    move-object v4, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/data/OffersSubcontroller;->publishResult(Ljava/util/List;ZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;J)V

    .line 87
    if-nez v8, :cond_4

    if-nez p5, :cond_6

    .line 104
    .end local v5    # "localSaveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :cond_4
    :goto_2
    return-void

    .line 74
    .end local v8    # "freshOffers":Z
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "OffersSubcontroller"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    const-string v1, "OffersSubcontroller"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error loading local offers data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v5    # "localSaveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    .restart local v8    # "freshOffers":Z
    :cond_5
    const-wide/high16 v6, -0x8000000000000000L

    goto :goto_1

    .line 91
    :cond_6
    if-eqz p1, :cond_7

    .line 93
    const/4 p4, 0x0

    .line 97
    .end local v5    # "localSaveConsumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/util/Nothing;>;>;"
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v1, p4}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->addConsumer(Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v9

    .line 98
    .local v9, "startNewTask":Z
    iget-object v1, p0, Lcom/google/android/apps/books/data/OffersSubcontroller;->mSaveConsumers:Lcom/google/android/apps/books/data/ExceptionOrConsumerList;

    invoke-virtual {v1, p5}, Lcom/google/android/apps/books/data/ExceptionOrConsumerList;->addConsumer(Lcom/google/android/ublib/utils/Consumer;)Z

    move-result v1

    or-int/2addr v9, v1

    .line 99
    if-eqz v9, :cond_4

    .line 103
    invoke-direct {p0, p3}, Lcom/google/android/apps/books/data/OffersSubcontroller;->getOffersOnNetworkThread(Lcom/google/android/apps/books/data/ControlTaskServices;)V

    goto :goto_2
.end method
