.class public interface abstract Lcom/google/android/apps/books/render/ResourceContentStore;
.super Ljava/lang/Object;
.source "ResourceContentStore.java"


# virtual methods
.method public abstract configureWebView(Landroid/accounts/Account;Ljava/lang/String;Landroid/webkit/WebView;Lcom/google/android/ublib/utils/Consumer;Lcom/google/android/apps/books/render/ReaderListener;Lcom/google/android/apps/books/model/VolumeManifest;Lcom/google/common/base/Function;Landroid/webkit/WebViewClient;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Landroid/webkit/WebView;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Ljava/lang/Exception;",
            ">;",
            "Lcom/google/android/apps/books/render/ReaderListener;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Lcom/google/common/base/Function",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;",
            "Landroid/webkit/WebViewClient;",
            ")V"
        }
    .end annotation
.end method

.method public abstract getResourceContentUri(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/books/util/BlockedContentReason$BlockedContentException;
        }
    .end annotation
.end method

.method public abstract shutDown()V
.end method
