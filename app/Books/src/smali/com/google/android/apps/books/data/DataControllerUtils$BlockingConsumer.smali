.class public Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;
.super Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;
.source "DataControllerUtils.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/data/DataControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BlockingConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private mGetCalled:Z

.field private mResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 390
    .local p0, "this":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<TT;>;"
    invoke-direct {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$Waiter;-><init>()V

    .line 391
    return-void
.end method

.method public static create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 394
    new-instance v0, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    invoke-direct {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 401
    .local p0, "this":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->mGetCalled:Z

    if-eqz v0, :cond_0

    .line 402
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "get() called more than once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 404
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->mGetCalled:Z

    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->block()V

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->mResult:Ljava/lang/Object;

    return-object v0
.end method

.method public take(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 411
    .local p0, "this":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<TT;>;"
    .local p1, "t":Ljava/lang/Object;, "TT;"
    iput-object p1, p0, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->mResult:Ljava/lang/Object;

    .line 412
    invoke-virtual {p0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->unblock()V

    .line 413
    return-void
.end method
