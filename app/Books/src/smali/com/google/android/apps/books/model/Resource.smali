.class public interface abstract Lcom/google/android/apps/books/model/Resource;
.super Ljava/lang/Object;
.source "Resource.java"

# interfaces
.implements Lcom/google/android/apps/books/util/Identifiable;


# virtual methods
.method public abstract getIsDefault()Z
.end method

.method public abstract getIsShared()Z
.end method

.method public abstract getLanguage()Ljava/lang/String;
.end method

.method public abstract getMd5Hash()Ljava/lang/String;
.end method

.method public abstract getMimeType()Ljava/lang/String;
.end method

.method public abstract getOverlay()Ljava/lang/String;
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method
