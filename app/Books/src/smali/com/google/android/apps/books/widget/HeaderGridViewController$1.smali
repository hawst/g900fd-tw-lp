.class Lcom/google/android/apps/books/widget/HeaderGridViewController$1;
.super Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;
.source "HeaderGridViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/HeaderGridViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/widget/HeaderGridViewController;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$400(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->notifyDataSetChanged()V

    .line 111
    return-void
.end method

.method public onItemRangeInserted(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$400(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->notifyItemRangeInserted(II)V

    .line 123
    return-void
.end method

.method public onItemRangeRemoved(II)V
    .locals 2
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # getter for: Lcom/google/android/apps/books/widget/HeaderGridViewController;->mHeaderAddingAdapter:Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$400(Lcom/google/android/apps/books/widget/HeaderGridViewController;)Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/HeaderGridViewController$1;->this$0:Lcom/google/android/apps/books/widget/HeaderGridViewController;

    # invokes: Lcom/google/android/apps/books/widget/HeaderGridViewController;->getHeaderCount()I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/HeaderGridViewController;->access$200(Lcom/google/android/apps/books/widget/HeaderGridViewController;)I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/books/widget/HeaderGridViewController$HeaderAddingAdapter;->notifyItemRangeRemoved(II)V

    .line 129
    return-void
.end method
