.class Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;
.super Lcom/google/android/play/IUserContentService$Stub;
.source "BooksUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/BooksUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BooksUserContentBinder"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/play/IUserContentService$Stub;-><init>()V

    .line 97
    iput-object p1, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    .line 98
    new-instance v0, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;

    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/sync/SyncAccountsStateImpl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 99
    return-void
.end method

.method private getMyEbooksVolumes(Landroid/accounts/Account;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    .local p2, "localData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    .local p3, "downloadProgress":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Lcom/google/android/apps/books/model/BooksDataStore;->getMyEbooksVolumes(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 274
    :goto_0
    return-object v1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "BooksUserContent"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    const-string v1, "BooksUserContent"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "problem loading volume list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getOffers(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    .locals 8
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v5, 0x0

    .line 280
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/books/app/BooksApplication;->getBooksApplication(Landroid/content/Context;)Lcom/google/android/apps/books/app/BooksApplication;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v1

    .line 282
    .local v1, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    invoke-static {}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->create()Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;

    move-result-object v0

    .line 284
    .local v0, "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;>;"
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v1, v4, v6, v0, v7}, Lcom/google/android/apps/books/data/BooksDataController;->getOffers(ZZLcom/google/android/ublib/utils/Consumer;Lcom/google/android/ublib/utils/Consumer;)V

    .line 285
    invoke-virtual {v0}, Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/util/ExceptionOr;

    invoke-static {v4}, Lcom/google/android/apps/books/util/ExceptionOr;->getValueOrThrow(Lcom/google/android/apps/books/util/ExceptionOr;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    .end local v0    # "consumer":Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer;, "Lcom/google/android/apps/books/data/DataControllerUtils$BlockingConsumer<Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;>;>;"
    .end local v1    # "dc":Lcom/google/android/apps/books/data/BooksDataController;
    :goto_0
    return-object v3

    .line 287
    :catch_0
    move-exception v2

    .line 288
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "BooksUserContent"

    const/4 v6, 0x5

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 289
    const-string v4, "BooksUserContent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "problem loading offers: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v3, v5

    .line 291
    goto :goto_0
.end method

.method private getPageTitleForPosition(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "readingPosition"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 297
    if-eqz p3, :cond_0

    .line 298
    invoke-static {p3}, Lcom/google/android/apps/books/common/Position;->extractPageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 300
    .local v2, "pageId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v6, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getDataStore(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    .line 301
    .local v0, "bds":Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-interface {v0, p2, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getPage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;

    move-result-object v1

    .line 302
    .local v1, "page":Lcom/google/android/apps/books/model/Page;
    if-eqz v1, :cond_0

    .line 303
    iget-object v6, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 304
    .local v4, "res":Landroid/content/res/Resources;
    invoke-interface {v1}, Lcom/google/android/apps/books/model/Page;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 305
    .local v3, "pageTitle":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v4, v3, v5, v6}, Lcom/google/android/apps/books/util/ReaderUtils;->formatPageTitle(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/Long;Z)Ljava/lang/CharSequence;

    move-result-object v5

    .line 311
    .end local v0    # "bds":Lcom/google/android/apps/books/model/BooksDataStore;
    .end local v1    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v2    # "pageId":Ljava/lang/String;
    .end local v3    # "pageTitle":Ljava/lang/String;
    .end local v4    # "res":Landroid/content/res/Resources;
    :cond_0
    return-object v5
.end method

.method private getWhatsNext(I)Ljava/util/List;
    .locals 34
    .param p1, "numItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    new-instance v29, Ljava/util/ArrayList;

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 132
    .local v29, "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const/4 v9, 0x1

    .line 133
    .local v9, "defaultToAny":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const/16 v32, 0x1

    invoke-static/range {v30 .. v32}, Lcom/google/android/apps/books/util/AccountUtils;->findIntentAccount(Landroid/content/Context;Landroid/accounts/Account;Z)Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;

    move-result-object v5

    .line 135
    .local v5, "accountResult":Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;
    iget-object v0, v5, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    move-object/from16 v30, v0

    if-eqz v30, :cond_0

    iget-object v0, v5, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_2

    .line 136
    :cond_0
    const-string v30, "BooksUserContent"

    const/16 v31, 0x5

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 137
    const-string v30, "BooksUserContent"

    const-string v31, "No account in getWhatsNext. Not returning any recents"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    .end local v29    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_1
    :goto_0
    return-object v29

    .line 142
    .restart local v29    # "whatsNext":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_2
    iget-object v4, v5, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->account:Landroid/accounts/Account;

    .line 146
    .local v4, "account":Landroid/accounts/Account;
    iget-boolean v0, v5, Lcom/google/android/apps/books/util/AccountUtils$FindAccountResult;->isFromPreferences:Z

    move/from16 v30, v0

    if-nez v30, :cond_3

    .line 147
    new-instance v30, Lcom/google/android/apps/books/preference/LocalPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    invoke-direct/range {v30 .. v31}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/preference/LocalPreferences;->setAccount(Landroid/accounts/Account;)V

    .line 151
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->getOffers(Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;

    move-result-object v19

    .line 152
    .local v19, "offersResponse":Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;
    if-eqz v19, :cond_5

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    move-object/from16 v30, v0

    if-eqz v30, :cond_5

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    move-object/from16 v30, v0

    invoke-interface/range {v30 .. v30}, Ljava/util/List;->isEmpty()Z

    move-result v30

    if-nez v30, :cond_5

    .line 154
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 155
    .local v7, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    .line 156
    .local v22, "res":Landroid/content/res/Resources;
    const-string v30, "Play.Reason"

    const v31, 0x7f0f01be

    move-object/from16 v0, v22

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->offers:Ljava/util/List;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-interface/range {v30 .. v31}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/books/model/OfferData;

    .line 159
    .local v17, "offer":Lcom/google/android/apps/books/model/OfferData;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/books/model/OfferData;->getArtUri()Landroid/net/Uri;

    move-result-object v6

    .line 160
    .local v6, "artUri":Landroid/net/Uri;
    const-string v30, "Play.ImageUri"

    move-object/from16 v0, v30

    invoke-virtual {v7, v0, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalOffersIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v18

    .line 165
    .local v18, "offersIntent":Landroid/content/Intent;
    const-string v30, "Play.ViewIntent"

    move-object/from16 v0, v30

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 167
    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->lastUpdateTimeMillis:J

    move-wide/from16 v30, v0

    const-wide/high16 v32, -0x8000000000000000L

    cmp-long v30, v30, v32

    if-lez v30, :cond_4

    .line 168
    const-string v30, "Play.LastUpdateTimeMillis"

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/apps/books/data/BooksDataController$OffersResponse;->lastUpdateTimeMillis:J

    move-wide/from16 v32, v0

    move-object/from16 v0, v30

    move-wide/from16 v1, v32

    invoke-virtual {v7, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 172
    :cond_4
    move-object/from16 v0, v29

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 178
    .end local v6    # "artUri":Landroid/net/Uri;
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v17    # "offer":Lcom/google/android/apps/books/model/OfferData;
    .end local v18    # "offersIntent":Landroid/content/Intent;
    .end local v22    # "res":Landroid/content/res/Resources;
    :cond_5
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v13

    .line 179
    .local v13, "localDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v10

    .line 180
    .local v10, "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v13, v10}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->getMyEbooksVolumes(Landroid/accounts/Account;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v28

    .line 182
    .local v28, "volumes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    if-eqz v28, :cond_6

    invoke-interface/range {v28 .. v28}, Ljava/util/List;->isEmpty()Z

    move-result v30

    if-eqz v30, :cond_8

    .line 184
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-object/from16 v30, v0

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-interface/range {v30 .. v31}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastMyEbooksFetchTime(Ljava/lang/String;)J

    move-result-wide v30

    const-wide/16 v32, 0x0

    cmp-long v30, v30, v32

    if-nez v30, :cond_1

    .line 187
    const-string v30, "BooksUserContent"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 188
    const-string v30, "BooksUserContent"

    const-string v31, "Never synced this account, returning null."

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_7
    const/16 v29, 0x0

    goto/16 :goto_0

    .line 196
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-object/from16 v30, v0

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    const-wide v32, 0x7fffffffffffffffL

    invoke-interface/range {v30 .. v33}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastSyncTime(Ljava/lang/String;J)J

    move-result-wide v14

    .line 198
    .local v14, "lastSyncTime":J
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 199
    .local v12, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/apps/books/model/VolumeData;>;"
    :cond_9
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_1

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v30

    move/from16 v0, v30

    move/from16 v1, p1

    if-ge v0, v1, :cond_1

    .line 200
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/books/model/VolumeData;

    .line 201
    .local v23, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v26

    .line 202
    .local v26, "volumeId":Ljava/lang/String;
    move-object/from16 v0, v26

    invoke-interface {v13, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 204
    .local v16, "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    move-object/from16 v0, v26

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2, v14, v15}, Lcom/google/android/apps/books/model/VolumeDataUtils;->isReadNowVolume(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;Lcom/google/android/apps/books/model/VolumeDownloadProgress;J)Z

    move-result v11

    .line 207
    .local v11, "isReadNow":Z
    if-eqz v11, :cond_9

    .line 212
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 215
    .restart local v7    # "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    move-object/from16 v30, v0

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    move-object/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksApplication;->buildExternalReadIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v21

    .line 217
    .local v21, "readIntent":Landroid/content/Intent;
    const-string v30, "books:warnOnSample"

    const/16 v31, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 218
    const-string v30, "Play.ViewIntent"

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 221
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/books/model/VolumeDataUtils;->getLastInteraction(Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/model/LocalVolumeData;)J

    move-result-wide v24

    .line 222
    .local v24, "updatedDate":J
    const-string v30, "Play.LastUpdateTimeMillis"

    move-object/from16 v0, v30

    move-wide/from16 v1, v24

    invoke-virtual {v7, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 225
    const-string v30, "Play.FinskyDocId"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "book-"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->getCoverUri(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)Landroid/net/Uri;

    move-result-object v8

    .line 229
    .local v8, "coverUri":Landroid/net/Uri;
    if-eqz v8, :cond_a

    .line 230
    const-string v30, "Play.ImageUri"

    invoke-static {v8}, Lcom/google/android/apps/books/provider/BooksProvider;->toPublicUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 234
    :cond_a
    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/VolumeData;->getReadingPosition()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    move-object/from16 v2, v30

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->getPageTitleForPosition(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v20

    .line 236
    .local v20, "pageTitle":Ljava/lang/CharSequence;
    if-eqz v20, :cond_b

    .line 237
    const-string v30, "Play.Reason"

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_b
    move-object/from16 v0, v29

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    const-string v30, "BooksUserContent"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 243
    invoke-interface/range {v23 .. v23}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v27

    .line 244
    .local v27, "volumeTitle":Ljava/lang/String;
    const-string v30, "BooksUserContent"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Added bundle: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " for "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method


# virtual methods
.method protected getCoverUri(Landroid/accounts/Account;Lcom/google/android/apps/books/model/VolumeData;)Landroid/net/Uri;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 253
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/app/BooksApplication;->getBackgroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/BooksDataController;

    move-result-object v0

    .line 255
    .local v0, "dc":Lcom/google/android/apps/books/data/BooksDataController;
    sget-object v2, Lcom/google/android/apps/books/data/BooksDataController$Priority;->BACKGROUND:Lcom/google/android/apps/books/data/BooksDataController$Priority;

    invoke-static {v0, p2, v2}, Lcom/google/android/apps/books/data/DataControllerUtils;->ensureVolumeCover(Lcom/google/android/apps/books/data/BooksDataController;Lcom/google/android/apps/books/model/VolumeData;Lcom/google/android/apps/books/data/BooksDataController$Priority;)V

    .line 256
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildCoverUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 261
    .end local v0    # "dc":Lcom/google/android/apps/books/data/BooksDataController;
    :goto_0
    return-object v2

    .line 257
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "BooksUserContent"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 259
    const-string v2, "BooksUserContent"

    const-string v3, "Failed to load cover image"

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/books/util/LogUtil;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 261
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDocuments(II)Ljava/util/List;
    .locals 6
    .param p1, "dataTypeToFetch"    # I
    .param p2, "numItemsToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v1, p0, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/books/util/SecurityUtils;->checkCallerSignature(Landroid/content/Context;)V

    .line 105
    packed-switch p1, :pswitch_data_0

    .line 117
    const-string v1, "BooksUserContent"

    const/4 v4, 0x6

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    const-string v1, "BooksUserContent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown dataTypeToFetch: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 108
    :pswitch_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 111
    .local v2, "savedIdentity":J
    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/books/service/BooksUserContentService$BooksUserContentBinder;->getWhatsNext(I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 113
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .end local v0    # "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :catchall_0
    move-exception v1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v1

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
