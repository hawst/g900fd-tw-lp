.class public interface abstract Lcom/google/android/apps/books/model/SentenceSearchTextProcessor$SentenceSearchProgressListener;
.super Ljava/lang/Object;
.source "SentenceSearchTextProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/model/SentenceSearchTextProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SentenceSearchProgressListener"
.end annotation


# virtual methods
.method public abstract onSearchedUpTo(Lcom/google/android/apps/books/annotations/TextLocation;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/annotations/TextLocation;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SearchResult;",
            ">;)V"
        }
    .end annotation
.end method
