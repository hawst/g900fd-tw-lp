.class public interface abstract Lcom/google/android/apps/books/widget/BooksHomeController;
.super Ljava/lang/Object;
.source "BooksHomeController.java"


# virtual methods
.method public abstract alreadyFetchedMyEbooks()Z
.end method

.method public abstract dismissOffer(Ljava/lang/String;)V
.end method

.method public abstract getAccount()Landroid/accounts/Account;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract getCoverLoadEnsurer(Lcom/google/android/apps/books/model/VolumeData;)Lcom/google/android/apps/books/common/ImageManager$Ensurer;
.end method

.method public abstract getDataCache()Lcom/google/android/apps/books/model/LocalVolumeDataCache;
.end method

.method public abstract getDownloadedOnlyMode()Z
.end method

.method public abstract getLibrarySortOrder()Lcom/google/android/apps/books/app/LibraryComparator;
.end method

.method public abstract getMyLibraryFilter()Lcom/google/android/apps/books/app/LibraryFilter;
.end method

.method public abstract getUploadsController()Lcom/google/android/apps/books/data/UploadsController;
.end method

.method public abstract getVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;
.end method

.method public abstract isAvailableForReading(Ljava/lang/String;)Z
.end method

.method public abstract isFullyDownloaded(Ljava/lang/String;)Z
.end method

.method public abstract isPartiallyOrFullyDownloaded(Ljava/lang/String;)Z
.end method

.method public abstract isPinned(Ljava/lang/String;)Z
.end method

.method public abstract isReadNowVolume(Lcom/google/android/apps/books/model/VolumeData;)Z
.end method

.method public abstract onBookSelected(Ljava/lang/String;Landroid/view/View;)V
.end method

.method public abstract onCardOverflowMenuShown()V
.end method

.method public abstract onPinSelected(Ljava/lang/String;)V
.end method

.method public abstract removeFromLibrary(Ljava/lang/String;)V
.end method

.method public abstract removeFromRecommendations(Ljava/lang/String;)V
.end method

.method public abstract startBuyBook(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/app/PurchaseInfo;)V
.end method

.method public abstract startOnboarding(I)V
.end method

.method public abstract viewAboutThisBook(Ljava/lang/String;)V
.end method

.method public abstract viewBookStore(Ljava/lang/String;)V
.end method

.method public abstract viewOffers()V
.end method

.method public abstract viewRecommendedSample(Ljava/lang/String;)V
.end method

.method public abstract viewStoreLinkForRecommendedBook(Ljava/lang/String;)V
.end method
