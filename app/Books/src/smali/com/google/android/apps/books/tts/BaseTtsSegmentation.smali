.class public abstract Lcom/google/android/apps/books/tts/BaseTtsSegmentation;
.super Ljava/lang/Object;
.source "BaseTtsSegmentation.java"

# interfaces
.implements Lcom/google/android/apps/books/tts/Segmentation;


# static fields
.field private static final nonWhitespacePattern:Ljava/util/regex/Pattern;


# instance fields
.field final mPositionMap:Lcom/google/android/apps/books/model/LabelMap;

.field final mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "\\S"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->nonWhitespacePattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionMap"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->mText:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->mPositionMap:Lcom/google/android/apps/books/model/LabelMap;

    .line 26
    return-void
.end method


# virtual methods
.method public getNearestItemIndex(I)I
    .locals 3
    .param p1, "characterIndex"    # I

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->getItemCount()I

    move-result v0

    .line 47
    .local v0, "itemCount":I
    const/4 v1, 0x0

    .local v1, "itemIndex":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 48
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->getItemRange(I)Lcom/google/android/apps/books/util/Range;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/books/util/Range;->start:I

    if-le v2, p1, :cond_1

    .line 49
    if-lez v1, :cond_0

    .line 50
    add-int/lit8 v1, v1, -0x1

    :cond_0
    move v2, v1

    .line 55
    :goto_1
    return v2

    .line 47
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    :cond_2
    add-int/lit8 v2, v0, -0x1

    goto :goto_1
.end method

.method public getNearestItemIndex(Lcom/google/android/apps/books/common/Position;)Landroid/util/Pair;
    .locals 4
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/common/Position;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 33
    invoke-virtual {p1}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v1

    .line 34
    .local v1, "positionString":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->mPositionMap:Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/model/LabelMap;->getCharacterOffsetForLabel(Ljava/lang/String;)I

    move-result v0

    .line 36
    .local v0, "positionOffset":I
    if-ne v0, v3, :cond_0

    .line 37
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 40
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->getNearestItemIndex(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0
.end method

.method protected final shouldIncludeItem(Ljava/lang/String;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 66
    sget-object v1, Lcom/google/android/apps/books/tts/BaseTtsSegmentation;->nonWhitespacePattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 67
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    return v1
.end method
