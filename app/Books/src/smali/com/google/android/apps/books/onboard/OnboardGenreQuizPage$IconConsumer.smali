.class Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;
.super Ljava/lang/Object;
.source "OnboardGenreQuizPage.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IconConsumer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/util/ExceptionOr",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mCategory:Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;

.field private mServerOrderIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;I)V
    .locals 2
    .param p2, "category"    # Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;
    .param p3, "serverOrderIndex"    # I

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p3, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->mServerOrderIndex:I

    .line 44
    iget-object v0, p2, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->categoryId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "IconConsumer needs non-null categoryId"

    invoke-static {v0, v1}, Lcom/google/android/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->mCategory:Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;

    .line 47
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "bitmapExceptionOr":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Landroid/graphics/Bitmap;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    const-string v2, "OnboardGenreQuizPage"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    const-string v2, "OnboardGenreQuizPage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "genre icon fetch failed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getException()Ljava/lang/Exception;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    const/4 v0, 0x0

    .line 62
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    new-instance v1, Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    iget-object v2, v2, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->mQuizItemDataList:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/DataList;->getSnapshot()Lcom/google/android/libraries/bind/data/Snapshot;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/bind/data/Snapshot;->list:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 65
    .local v1, "snapshotList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/libraries/bind/data/Data;>;"
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    iget-object v3, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->mCategory:Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;

    iget-object v3, v3, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->categoryId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->mCategory:Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;

    iget-object v4, v4, Lcom/google/android/apps/books/api/data/SampleCategories$SampleCategory;->name:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->mServerOrderIndex:I

    # invokes: Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->createData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;
    invoke-static {v2, v3, v4, v0, v5}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->access$000(Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v2, p0, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->this$0:Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage;->serverOrderSortAndUpdateSnapshot(Ljava/util/List;)V

    .line 69
    return-void

    .line 60
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "snapshotList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/google/android/libraries/bind/data/Data;>;"
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 35
    check-cast p1, Lcom/google/android/apps/books/util/ExceptionOr;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/onboard/OnboardGenreQuizPage$IconConsumer;->take(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method
