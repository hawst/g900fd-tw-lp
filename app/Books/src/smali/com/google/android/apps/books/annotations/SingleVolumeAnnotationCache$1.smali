.class Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache$1;
.super Ljava/lang/Object;
.source "SingleVolumeAnnotationCache.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;->getRecentAnnotationsForLayer(Ljava/lang/String;I)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/books/annotations/Annotation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache$1;->this$0:Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)I
    .locals 5
    .param p1, "a"    # Lcom/google/android/apps/books/annotations/Annotation;
    .param p2, "b"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 306
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLastUsedTimestamp()J

    move-result-wide v0

    .line 307
    .local v0, "atime":J
    invoke-virtual {p2}, Lcom/google/android/apps/books/annotations/Annotation;->getLastUsedTimestamp()J

    move-result-wide v2

    .line 308
    .local v2, "btime":J
    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 309
    const/4 v4, 0x0

    .line 313
    :goto_0
    return v4

    .line 310
    :cond_0
    cmp-long v4, v2, v0

    if-gez v4, :cond_1

    .line 311
    const/4 v4, -0x1

    goto :goto_0

    .line 313
    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 303
    check-cast p1, Lcom/google/android/apps/books/annotations/Annotation;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/apps/books/annotations/Annotation;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/books/annotations/SingleVolumeAnnotationCache$1;->compare(Lcom/google/android/apps/books/annotations/Annotation;Lcom/google/android/apps/books/annotations/Annotation;)I

    move-result v0

    return v0
.end method
