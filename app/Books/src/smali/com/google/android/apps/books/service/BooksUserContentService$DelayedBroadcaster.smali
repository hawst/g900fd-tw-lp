.class public interface abstract Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
.super Ljava/lang/Object;
.source "BooksUserContentService.java"

# interfaces
.implements Lcom/google/android/apps/books/service/BooksUserContentService$Broadcaster;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/service/BooksUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DelayedBroadcaster"
.end annotation


# virtual methods
.method public abstract addNotificationBlock(Ljava/lang/Object;)V
.end method

.method public abstract flushNotifications()V
.end method

.method public abstract removeNotificationBlock(Ljava/lang/Object;)V
.end method
