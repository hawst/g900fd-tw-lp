.class public Lcom/google/android/apps/books/annotations/BlobKeys;
.super Ljava/lang/Object;
.source "BlobKeys.java"


# static fields
.field private static final RESERVED_CHARS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "|"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/annotations/BlobKeys;->RESERVED_CHARS:[Ljava/lang/String;

    return-void
.end method

.method private static escape(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "character"    # Ljava/lang/String;

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\\"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static forUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "url"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p0, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/books/annotations/BlobKeys;->makeKey([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFieldValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "field"    # Ljava/lang/String;

    .prologue
    .line 69
    if-nez p0, :cond_1

    .line 70
    const-string v4, "_"

    .line 76
    :cond_0
    return-object v4

    .line 72
    :cond_1
    move-object v4, p0

    .line 73
    .local v4, "result":Ljava/lang/String;
    sget-object v0, Lcom/google/android/apps/books/annotations/BlobKeys;->RESERVED_CHARS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 74
    .local v3, "reserved":Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/google/android/apps/books/annotations/BlobKeys;->escape(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static varargs makeKey([Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "fields"    # [Ljava/lang/String;

    .prologue
    .line 54
    new-instance v1, Ljava/util/ArrayList;

    array-length v5, p0

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    .local v1, "escapedFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    .line 56
    .local v2, "field":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/books/annotations/BlobKeys;->getFieldValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 58
    .end local v2    # "field":Ljava/lang/String;
    :cond_0
    const-string v5, "|"

    invoke-static {v5}, Lcom/google/common/base/Joiner;->on(Ljava/lang/String;)Lcom/google/common/base/Joiner;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/common/base/Joiner;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
