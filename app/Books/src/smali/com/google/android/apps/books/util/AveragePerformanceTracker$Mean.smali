.class Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;
.super Ljava/lang/Object;
.source "AveragePerformanceTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/AveragePerformanceTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Mean"
.end annotation


# instance fields
.field private mNumberOfSamples:I

.field private mSampleSumation:J

.field final synthetic this$0:Lcom/google/android/apps/books/util/AveragePerformanceTracker;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/AveragePerformanceTracker;)V
    .locals 2

    .prologue
    .line 18
    iput-object p1, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->this$0:Lcom/google/android/apps/books/util/AveragePerformanceTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mSampleSumation:J

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mNumberOfSamples:I

    return-void
.end method


# virtual methods
.method public addValue(J)V
    .locals 3
    .param p1, "newValue"    # J

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mSampleSumation:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mSampleSumation:J

    .line 27
    iget v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mNumberOfSamples:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mNumberOfSamples:I

    .line 28
    return-void
.end method

.method public getMean()F
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mSampleSumation:J

    long-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/books/util/AveragePerformanceTracker$Mean;->mNumberOfSamples:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method
