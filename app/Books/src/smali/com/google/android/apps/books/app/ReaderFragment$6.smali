.class Lcom/google/android/apps/books/app/ReaderFragment$6;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ReaderFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/model/VolumeMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;)V
    .locals 0

    .prologue
    .line 1084
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$6;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/model/VolumeMetadata;)V
    .locals 2
    .param p1, "t"    # Lcom/google/android/apps/books/model/VolumeMetadata;

    .prologue
    .line 1087
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$6;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mNotesFirstLoadTimer:Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$2000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v0

    const-string v1, "#metadataLoaded"

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->checkpoint(Ljava/lang/String;)V

    .line 1088
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1084
    check-cast p1, Lcom/google/android/apps/books/model/VolumeMetadata;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/ReaderFragment$6;->take(Lcom/google/android/apps/books/model/VolumeMetadata;)V

    return-void
.end method
