.class public abstract Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;
.super Landroid/app/Activity;
.source "BaseTestingOptionsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;,
        Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;,
        Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;,
        Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    }
.end annotation


# static fields
.field private static final BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

.field private static final COMPILE_JS_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

.field private static final ENVIRONMENT_KEYS:[Lcom/google/android/apps/books/util/ConfigValue;

.field private static final STATIC_SETTINGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/books/app/AbstractSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDirty:Z

.field private final mResources:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 60
    new-array v0, v8, [Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->APIARY:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->CONTENT_API:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/util/ConfigValue;->UPLOAD_URL:Lcom/google/android/apps/books/util/ConfigValue;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->ENVIRONMENT_KEYS:[Lcom/google/android/apps/books/util/ConfigValue;

    .line 63
    const-string v0, "true"

    const-string v1, "false"

    invoke-static {v0, v1}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->booleanChoices(Ljava/lang/String;Ljava/lang/String;)[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .line 65
    new-array v0, v8, [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    const-string v2, "Compiled"

    new-array v3, v6, [Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_COMPILED_CONFIG_VALUE:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    const-string v2, "Debug"

    new-array v3, v6, [Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_DEBUG_CONFIG_VALUE:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    const-string v2, "Side Loaded"

    new-array v3, v6, [Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue$Constants;->JS_SIDELOADED_CONFIG_VALUE:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->COMPILE_JS_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .line 71
    const/16 v0, 0x1c

    new-array v0, v0, [Lcom/google/android/apps/books/app/AbstractSetting;

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v2, "Log to GA"

    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->LOG_TO_GOOGLE_ANALYTICS:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v2, "Geo layer"

    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->GEO_LAYER:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v2, "Performance logging"

    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->PERFORMANCE_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v2, "Always force full annotation refresh"

    sget-object v3, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_FORCE_ANNOTATION_REFRESH:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v1, v0, v8

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Show recommendations"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Legacy play logging"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->LEGACY_PLAY_LOGGING:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "WebView hardware rendering"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->WEBVIEW_HARDWARE_RENDERING:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Mock recommendations for testing"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_RECOMMENDATIONS:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Mock offers for testing"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->TESTING_OFFERS:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Emulate Offers Device"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->EMULATE_OFFERS_DEVICE:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Show debug word boxes"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->SHOW_DEBUG_WORD_BOXES:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Load page text for UI Automator"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->LOAD_TEXT_FOR_UI_AUTOMATOR:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Compiled JS"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->COMPILE_JS:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->COMPILE_JS_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Always show onboard card in empty library"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_ONBOARDING_CARD:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Search Uploaded PDFs"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_SEARCH_ON_UPLOADED_PDF:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "TTS content from storage"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->USE_TTS_CONTENT_FROM_STORAGE:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Grid TOC"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->GRID_TOC:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Include sentence before and after match in search results"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->SENTENCE_BEFORE_AND_AFTER:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Enable nasty proxy server"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_PROXY_SERVER:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Pretend Offers always succeed (requires proxy server)"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->PRETEND_OFFERS_ALWAYS_SUCCEED:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Enable offline dictionary"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_OFFLINE_DICTIONARY:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Enable stub offline dictionary metadata"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_STUB_DICTIONARY_METADATA:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lcom/google/android/apps/books/app/FreeTextSetting;

    const-string v3, "Proxy server denies download license for volumeIds containing [blank for none]: "

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->NASTY_DENY_DOWNLOAD_LICENSE:Lcom/google/android/apps/books/util/ConfigValue;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/books/app/FreeTextSetting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Always offer HaTS survey"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_SHOW_HATS_SURVEYS:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Enable onboarding"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Enable onboard of existing users"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ENABLE_ONBOARD_EXISTING:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Always auto-start onboarding"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->ALWAYS_AUTO_START_ONBOARDING:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v3, "Fake slow is-EDU-device"

    sget-object v4, Lcom/google/android/apps/books/util/ConfigValue;->FAKE_SLOW_IS_SCHOOL_OWNED:Lcom/google/android/apps/books/util/ConfigValue;

    sget-object v5, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->BOOLEAN_CHOICES:[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->STATIC_SETTINGS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->createResources()Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->mResources:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;

    return-void
.end method

.method private static booleanChoices(Ljava/lang/String;Ljava/lang/String;)[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .locals 6
    .param p0, "trueValue"    # Ljava/lang/String;
    .param p1, "falseValue"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 131
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    const-string v2, "Enabled"

    new-array v3, v5, [Ljava/lang/String;

    aput-object p0, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    const-string v2, "Disabled"

    new-array v3, v5, [Ljava/lang/String;

    aput-object p1, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    aput-object v1, v0, v5

    return-object v0
.end method


# virtual methods
.method protected abstract createResources()Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 19
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 286
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 287
    if-eqz p1, :cond_0

    .line 288
    const-string v14, "dirty"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->mDirty:Z

    .line 290
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->mResources:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;

    iget v14, v14, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->contentViewLayoutId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->setContentView(I)V

    .line 292
    sget-object v14, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->STATIC_SETTINGS:Ljava/util/List;

    invoke-static {v14}, Lcom/google/common/collect/Lists;->newArrayList(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v12

    .line 298
    .local v12, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/app/AbstractSetting;>;"
    new-instance v6, Ljava/io/File;

    sget-object v14, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v14}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v14

    const-string v15, "books-environments.json"

    invoke-direct {v6, v14, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 302
    .local v6, "environmentsFile":Ljava/io/File;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 303
    .local v8, "is":Ljava/io/InputStream;
    new-instance v14, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v14}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-virtual {v14, v8}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonParser(Ljava/io/InputStream;)Lcom/google/api/client/json/JsonParser;

    move-result-object v14

    const-class v15, Ljava/util/ArrayList;

    const-class v16, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;

    const/16 v17, 0x0

    invoke-virtual/range {v14 .. v17}, Lcom/google/api/client/json/JsonParser;->parseArray(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/util/Collection;

    move-result-object v9

    .line 305
    .local v9, "json":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 306
    .local v5, "environmentChoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;>;"
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;

    .line 307
    .local v4, "environment":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;
    iget-object v14, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->name:Ljava/lang/String;

    if-eqz v14, :cond_2

    iget-object v14, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->apiary:Ljava/lang/String;

    if-eqz v14, :cond_2

    iget-object v14, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->contentApi:Ljava/lang/String;

    if-eqz v14, :cond_2

    iget-object v14, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->uploadsURL:Ljava/lang/String;

    if-eqz v14, :cond_2

    .line 309
    new-instance v14, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    iget-object v15, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->name:Ljava/lang/String;

    const/16 v16, 0x3

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    iget-object v0, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->apiary:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x1

    iget-object v0, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->contentApi:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x2

    iget-object v0, v4, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;->uploadsURL:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    invoke-direct/range {v14 .. v16}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {v5, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 323
    .end local v4    # "environment":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;
    .end local v5    # "environmentChoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v9    # "json":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;>;"
    :catch_0
    move-exception v3

    .line 324
    .local v3, "e":Ljava/lang/Exception;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Failed to load environments: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 331
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    new-instance v10, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$1;-><init>(Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;)V

    .line 338
    .local v10, "listener":Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->mResources:Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;

    iget v14, v14, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;->optionsContainerViewId:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 339
    .local v2, "container":Landroid/view/ViewGroup;
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/books/app/AbstractSetting;

    .line 340
    .local v11, "setting":Lcom/google/android/apps/books/app/AbstractSetting;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->overridesViaGservices()Z

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->createResources()Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v11, v0, v14, v15, v10}, Lcom/google/android/apps/books/app/AbstractSetting;->createView(Landroid/app/Activity;ZLcom/google/android/apps/books/app/BaseTestingOptionsActivity$Resources;Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;)Landroid/view/View;

    move-result-object v13

    .line 342
    .local v13, "view":Landroid/view/View;
    if-eqz v13, :cond_1

    .line 343
    invoke-virtual {v2, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 313
    .end local v2    # "container":Landroid/view/ViewGroup;
    .end local v10    # "listener":Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;
    .end local v11    # "setting":Lcom/google/android/apps/books/app/AbstractSetting;
    .end local v13    # "view":Landroid/view/View;
    .restart local v4    # "environment":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;
    .restart local v5    # "environmentChoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;>;"
    .restart local v8    # "is":Ljava/io/InputStream;
    .restart local v9    # "json":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;>;"
    :cond_2
    :try_start_1
    const-string v14, "BTOActivity"

    const/4 v15, 0x6

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 314
    const-string v14, "BTOActivity"

    const-string v15, "Outdated config file, try: "

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const-string v14, "BTOActivity"

    const-string v15, "adb push tablet/books-environments.json /sdcard/Download/"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_3
    new-instance v14, Ljava/lang/Exception;

    const-string v15, "environment missing some fields"

    invoke-direct {v14, v15}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v14

    .line 320
    .end local v4    # "environment":Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;
    :cond_4
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v14

    new-array v1, v14, [Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;

    .line 321
    .local v1, "choicesArray":[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    invoke-interface {v5, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 322
    new-instance v14, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;

    const-string v15, "Environment"

    sget-object v16, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->ENVIRONMENT_KEYS:[Lcom/google/android/apps/books/util/ConfigValue;

    move-object/from16 v0, v16

    invoke-direct {v14, v15, v0, v1}, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$Setting;-><init>(Ljava/lang/String;[Lcom/google/android/apps/books/util/ConfigValue;[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;)V

    invoke-interface {v12, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 346
    .end local v1    # "choicesArray":[Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;
    .end local v5    # "environmentChoices":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$SettingChoice;>;"
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v9    # "json":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/app/BaseTestingOptionsActivity$JsonEnvironment;>;"
    .restart local v2    # "container":Landroid/view/ViewGroup;
    .restart local v10    # "listener":Lcom/google/android/apps/books/app/AbstractSetting$OptionChangeListener;
    :cond_5
    return-void
.end method

.method protected onOptionChanged()V
    .locals 0

    .prologue
    .line 357
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 350
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 351
    const-string v0, "dirty"

    iget-boolean v1, p0, Lcom/google/android/apps/books/app/BaseTestingOptionsActivity;->mDirty:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 352
    return-void
.end method

.method protected abstract overridesViaGservices()Z
.end method
