.class Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;
.super Ljava/lang/Object;
.source "ForegroundBooksDataControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->saveDismissedRecommendations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

.field final synthetic val$dismissedRecs:Ljava/util/Set;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 410
    iput-object p1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;->val$dismissedRecs:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 414
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;->this$0:Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->mDataStore:Lcom/google/android/apps/books/model/BooksDataStore;
    invoke-static {v1}, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;->access$000(Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl;)Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/ForegroundBooksDataControllerImpl$7;->val$dismissedRecs:Ljava/util/Set;

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->setDismissedRecommendations(Ljava/util/Set;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "FgDataController"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    const-string v1, "FgDataController"

    const-string v2, "setDismissedRecommendations Failed"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
