.class public Lcom/google/android/apps/books/net/OfflineLicenseManager;
.super Ljava/lang/Object;
.source "OfflineLicenseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;
    }
.end annotation


# direct methods
.method static synthetic access$000(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 25
    invoke-static {p0, p1}, Lcom/google/android/apps/books/net/OfflineLicenseManager;->handleAccessResponse(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method

.method private static handleAccessResponse(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 5
    .param p0, "callbacks"    # Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;"
    const/4 v3, 0x0

    .line 83
    const/4 v0, 0x0

    .line 84
    .local v0, "downloadAccess":Lcom/google/android/apps/books/api/data/DownloadAccessResponse;
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/api/data/RequestAccessResponse;

    .line 86
    .local v2, "requestAccessResponse":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    if-eqz v2, :cond_0

    .line 87
    iget-object v0, v2, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->downloadAccess:Lcom/google/android/apps/books/api/data/DownloadAccessResponse;

    .line 90
    .end local v2    # "requestAccessResponse":Lcom/google/android/apps/books/api/data/RequestAccessResponse;
    :cond_0
    if-eqz v0, :cond_3

    .line 91
    iget-boolean v4, v0, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->restricted:Z

    if-eqz v4, :cond_2

    iget v1, v0, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->maxDownloadDevices:I

    .line 94
    .local v1, "maxDownloadDevices":I
    :goto_0
    iget-boolean v4, v0, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->deviceAllowed:Z

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    iget v4, v0, Lcom/google/android/apps/books/api/data/DownloadAccessResponse;->downloadsAcquired:I

    invoke-interface {p0, v3, v1, v4}, Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;->handleResultUi(III)V

    .line 101
    .end local v1    # "maxDownloadDevices":I
    :goto_1
    return-void

    .line 91
    :cond_2
    const v1, 0x7fffffff

    goto :goto_0

    .line 99
    :cond_3
    const/4 v4, -0x1

    invoke-interface {p0, v4, v3, v3}, Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;->handleResultUi(III)V

    goto :goto_1
.end method

.method public static requestOfflineLicense(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;Ljava/lang/String;Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 3
    .param p0, "callbacks"    # Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 64
    const-string v0, "OfflineLicenseManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "OfflineLicenseManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requesting offline license for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_0
    const-string v0, "DOWNLOAD"

    new-instance v1, Lcom/google/android/apps/books/net/OfflineLicenseManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/books/net/OfflineLicenseManager$1;-><init>(Lcom/google/android/apps/books/net/OfflineLicenseManager$Callbacks;)V

    invoke-interface {p2, p1, v0, v1}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    .line 79
    return-void
.end method
