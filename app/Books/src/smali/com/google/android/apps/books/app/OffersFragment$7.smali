.class Lcom/google/android/apps/books/app/OffersFragment$7;
.super Ljava/lang/Object;
.source "OffersFragment.java"

# interfaces
.implements Lcom/google/android/ublib/utils/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/OffersFragment;->dismissOffer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/ublib/utils/Consumer",
        "<",
        "Lcom/google/android/apps/books/data/BooksDataController;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/OffersFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/OffersFragment;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/apps/books/app/OffersFragment$7;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public take(Lcom/google/android/apps/books/data/BooksDataController;)V
    .locals 2
    .param p1, "t"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$7;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # getter for: Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;
    invoke-static {v0}, Lcom/google/android/apps/books/app/OffersFragment;->access$700(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/model/OfferData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/OfferData;->getOfferId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/BooksDataController;->dismissOffer(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/books/app/OffersFragment$7;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # invokes: Lcom/google/android/apps/books/app/OffersFragment;->getCallbacks()Lcom/google/android/apps/books/app/OffersFragment$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/app/OffersFragment;->access$900(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/app/OffersFragment$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/OffersFragment$7;->this$0:Lcom/google/android/apps/books/app/OffersFragment;

    # getter for: Lcom/google/android/apps/books/app/OffersFragment;->mCurrentOffer:Lcom/google/android/apps/books/model/OfferData;
    invoke-static {v1}, Lcom/google/android/apps/books/app/OffersFragment;->access$700(Lcom/google/android/apps/books/app/OffersFragment;)Lcom/google/android/apps/books/model/OfferData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/OfferData;->getOfferId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/app/OffersFragment$Callbacks;->onOfferDismissed(Ljava/lang/String;)V

    .line 251
    return-void
.end method

.method public bridge synthetic take(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 246
    check-cast p1, Lcom/google/android/apps/books/data/BooksDataController;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/app/OffersFragment$7;->take(Lcom/google/android/apps/books/data/BooksDataController;)V

    return-void
.end method
