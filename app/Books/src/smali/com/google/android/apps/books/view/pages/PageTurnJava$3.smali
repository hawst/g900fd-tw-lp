.class Lcom/google/android/apps/books/view/pages/PageTurnJava$3;
.super Ljava/lang/Object;
.source "PageTurnJava.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/view/pages/PageTurnJava;->setPagesEmpty()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/view/pages/PageTurnJava;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$3;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 196
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    const/4 v0, 0x6

    if-ge v6, v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$3;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    # invokes: Lcom/google/android/apps/books/view/pages/PageTurnJava;->normalizePageNumber(I)I
    invoke-static {v0, v6}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$200(Lcom/google/android/apps/books/view/pages/PageTurnJava;I)I

    move-result v1

    .line 198
    .local v1, "slot":I
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$3;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    const-string v2, "empty "

    const/4 v3, 0x1

    move v5, v4

    # invokes: Lcom/google/android/apps/books/view/pages/PageTurnJava;->maybeLogSpecialPageTypes(ILjava/lang/String;ZZZ)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$300(Lcom/google/android/apps/books/view/pages/PageTurnJava;ILjava/lang/String;ZZZ)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/books/view/pages/PageTurnJava$3;->this$0:Lcom/google/android/apps/books/view/pages/PageTurnJava;

    # getter for: Lcom/google/android/apps/books/view/pages/PageTurnJava;->mScene:Lcom/google/android/apps/books/view/pages/PageTurnScene;
    invoke-static {v0}, Lcom/google/android/apps/books/view/pages/PageTurnJava;->access$000(Lcom/google/android/apps/books/view/pages/PageTurnJava;)Lcom/google/android/apps/books/view/pages/PageTurnScene;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/view/pages/PageTurnScene;->setPageToEmpty(I)V

    .line 196
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 201
    .end local v1    # "slot":I
    :cond_0
    return-void
.end method
