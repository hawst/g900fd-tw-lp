.class public Lcom/google/android/apps/books/preference/RadioGroupPreference;
.super Landroid/widget/LinearLayout;
.source "RadioGroupPreference.java"

# interfaces
.implements Lcom/google/android/apps/books/preference/LightweightPreference;


# instance fields
.field private mBinding:Z

.field private mGroup:Landroid/widget/RadioGroup;

.field private final mGroupListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private final mKey:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

.field private mPrefs:Landroid/content/SharedPreferences;


# virtual methods
.method public bindPreference()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 83
    iput-boolean v2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mBinding:Z

    .line 85
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    if-nez v2, :cond_1

    .line 86
    .local v1, "value":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {p0, v1}, Lcom/google/android/apps/books/preference/RadioGroupPreference;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 88
    .local v0, "button":Landroid/widget/RadioButton;
    if-eqz v0, :cond_0

    .line 89
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    .end local v0    # "button":Landroid/widget/RadioButton;
    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mBinding:Z

    .line 95
    return-void

    .line 85
    .end local v1    # "value":Ljava/lang/String;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mKey:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v2

    iput-boolean v5, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mBinding:Z

    throw v2
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/preference/RadioGroupPreference;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mGroup:Landroid/widget/RadioGroup;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mGroupListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 49
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 55
    iget-object v2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2, p1}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 56
    iget-object v2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v0

    .line 57
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method

.method public setupPreference(Landroid/content/SharedPreferences;Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;)V
    .locals 0
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "listener"    # Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mPrefs:Landroid/content/SharedPreferences;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/books/preference/RadioGroupPreference;->mListener:Lcom/google/android/apps/books/preference/LightweightPreference$ChangeListener;

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/books/preference/RadioGroupPreference;->bindPreference()V

    .line 79
    return-void
.end method
