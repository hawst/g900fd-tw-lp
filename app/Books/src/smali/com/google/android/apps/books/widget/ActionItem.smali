.class public abstract Lcom/google/android/apps/books/widget/ActionItem;
.super Landroid/widget/ImageButton;
.source "ActionItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ActionItem;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ActionItem;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/widget/ActionItem;->performClickAction()V

    .line 46
    return-void
.end method

.method protected abstract performClickAction()V
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/apps/books/widget/ActionItem;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 37
    return-void
.end method
