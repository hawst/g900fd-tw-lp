.class Lcom/google/android/apps/books/data/UploadsControllerImpl$12;
.super Ljava/lang/Object;
.source "UploadsControllerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/UploadsControllerImpl;->cancelUpload(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

.field final synthetic val$id:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/UploadsControllerImpl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$12;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    iput-object p2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$12;->val$id:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 457
    iget-object v1, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$12;->this$0:Lcom/google/android/apps/books/data/UploadsControllerImpl;

    # getter for: Lcom/google/android/apps/books/data/UploadsControllerImpl;->mUploaders:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/data/UploadsControllerImpl;->access$200(Lcom/google/android/apps/books/data/UploadsControllerImpl;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/data/UploadsControllerImpl$12;->val$id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/upload/SingleBookUploader;

    .line 458
    .local v0, "uploader":Lcom/google/android/apps/books/upload/SingleBookUploader;
    if-eqz v0, :cond_0

    .line 459
    invoke-interface {v0}, Lcom/google/android/apps/books/upload/SingleBookUploader;->cancelUpload()V

    .line 461
    :cond_0
    return-void
.end method
