.class public Lcom/google/android/apps/books/render/PageCanvasPainterImpl;
.super Ljava/lang/Object;
.source "PageCanvasPainterImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/render/PageCanvasPainter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDensity:F

.field private final mFramedRectDrawable:Landroid/graphics/drawable/Drawable;

.field private final mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

.field private mMarginNoteIconRenderer:Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

.field private mMarginNoteIconSize:Landroid/graphics/Point;

.field private final mPressedRectDrawable:Landroid/graphics/drawable/Drawable;

.field private final mReusedRect:Landroid/graphics/Rect;

.field private final mReusedRegion:Landroid/graphics/Region;

.field private final mTempLabeledRect:Lcom/google/android/apps/books/render/LabeledRect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    new-instance v1, Landroid/graphics/Region;

    invoke-direct {v1}, Landroid/graphics/Region;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRegion:Landroid/graphics/Region;

    .line 191
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRect:Landroid/graphics/Rect;

    .line 206
    new-instance v1, Lcom/google/android/apps/books/render/LabeledRect;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/LabeledRect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mTempLabeledRect:Lcom/google/android/apps/books/render/LabeledRect;

    .line 418
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconSize:Landroid/graphics/Point;

    .line 196
    iput-object p1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mContext:Landroid/content/Context;

    .line 197
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 198
    .local v0, "res":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mDensity:F

    .line 201
    const v1, 0x7f02004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mFramedRectDrawable:Landroid/graphics/drawable/Drawable;

    .line 202
    const v1, 0x7f02004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mPressedRectDrawable:Landroid/graphics/drawable/Drawable;

    .line 203
    new-instance v1, Lcom/google/android/apps/books/render/HighlightPaintBox;

    invoke-direct {v1}, Lcom/google/android/apps/books/render/HighlightPaintBox;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

    .line 204
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/render/PageCanvasPainterImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/PageCanvasPainterImpl;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private drawMediaButton(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/books/render/TouchableItem;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "buttonDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "backgroundDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p4, "item"    # Lcom/google/android/apps/books/render/TouchableItem;

    .prologue
    const/4 v4, 0x0

    .line 388
    iget-object v0, p4, Lcom/google/android/apps/books/render/TouchableItem;->bounds:Landroid/graphics/Rect;

    .line 389
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v3

    .line 390
    .local v3, "minWidth":I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v2

    .line 391
    .local v2, "minHeight":I
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 393
    .local v1, "buttonBounds":Landroid/graphics/Rect;
    if-eqz p3, :cond_0

    .line 395
    invoke-virtual {p3, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 396
    invoke-virtual {p3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 400
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 401
    div-int/lit8 v4, v3, 0x2

    neg-int v4, v4

    div-int/lit8 v5, v2, 0x2

    neg-int v5, v5

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 402
    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 403
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 404
    return-void
.end method

.method private drawVolumeAnnotationRect(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/drawable/Drawable;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "rectangle"    # Landroid/graphics/Rect;
    .param p3, "rectDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/high16 v8, 0x40400000    # 3.0f

    .line 260
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 261
    const/high16 v6, 0x3f800000    # 1.0f

    iget v7, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mDensity:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 262
    .local v5, "topPadding":I
    const/high16 v6, 0x40000000    # 2.0f

    iget v7, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mDensity:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    add-int/lit8 v0, v6, 0x2

    .line 263
    .local v0, "botPadding":I
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mDensity:F

    mul-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 264
    .local v1, "leftPadding":I
    iget v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mDensity:F

    mul-float/2addr v6, v8

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 288
    .local v4, "rightPadding":I
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/2addr v7, v1

    add-int/2addr v7, v4

    sub-int v2, v6, v7

    .line 290
    .local v2, "needX":I
    if-lez v2, :cond_0

    .line 291
    shr-int/lit8 v6, v2, 0x1

    add-int/2addr v1, v6

    .line 292
    add-int/lit8 v6, v2, 0x1

    shr-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    .line 295
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v6

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/2addr v7, v5

    add-int/2addr v7, v0

    sub-int v3, v6, v7

    .line 297
    .local v3, "needY":I
    if-lez v3, :cond_1

    .line 298
    shr-int/lit8 v6, v3, 0x1

    add-int/2addr v5, v6

    .line 299
    add-int/lit8 v6, v3, 0x1

    shr-int/lit8 v6, v6, 0x1

    add-int/2addr v0, v6

    .line 302
    :cond_1
    iget v6, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v1

    iget v7, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v5

    iget v8, p2, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v4

    iget v9, p2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v0

    invoke-virtual {p3, v6, v7, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 304
    invoke-virtual {p3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 307
    .end local v0    # "botPadding":I
    .end local v1    # "leftPadding":I
    .end local v2    # "needX":I
    .end local v3    # "needY":I
    .end local v4    # "rightPadding":I
    .end local v5    # "topPadding":I
    :cond_2
    return-void
.end method

.method private getMarginNoteIconRenderer()Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconRenderer:Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

    if-nez v0, :cond_0

    .line 412
    new-instance v0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;-><init>(Lcom/google/android/apps/books/render/PageCanvasPainterImpl;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconRenderer:Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconRenderer:Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

    return-object v0
.end method


# virtual methods
.method public minimumMarginNoteIconSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconSize:Landroid/graphics/Point;

    if-nez v1, :cond_0

    .line 423
    invoke-direct {p0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->getMarginNoteIconRenderer()Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

    move-result-object v0

    .line 424
    .local v0, "renderer":Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->getMinimumSize()Landroid/graphics/Point;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconSize:Landroid/graphics/Point;

    .line 426
    .end local v0    # "renderer":Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mMarginNoteIconSize:Landroid/graphics/Point;

    return-object v1

    .line 424
    .restart local v0    # "renderer":Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v2, v2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0
.end method

.method public paintHighlightRects(Landroid/graphics/Canvas;Ljava/lang/String;ILcom/google/android/apps/books/widget/Walker;ZLandroid/graphics/Rect;Z)V
    .locals 9
    .param p1, "destCanvas"    # Landroid/graphics/Canvas;
    .param p2, "theme"    # Ljava/lang/String;
    .param p3, "color"    # I
    .param p5, "isSelected"    # Z
    .param p6, "clipRect"    # Landroid/graphics/Rect;
    .param p7, "coerceToAnnotationColor"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/books/widget/Walker",
            "<",
            "Lcom/google/android/apps/books/render/LabeledRect;",
            ">;Z",
            "Landroid/graphics/Rect;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p4, "rects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/LabeledRect;>;"
    const/4 v6, 0x0

    .line 214
    .local v6, "highlightPaint":Landroid/graphics/Paint;
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRegion:Landroid/graphics/Region;

    invoke-virtual {v0}, Landroid/graphics/Region;->setEmpty()V

    .line 217
    invoke-interface {p4}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 218
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mTempLabeledRect:Lcom/google/android/apps/books/render/LabeledRect;

    invoke-interface {p4, v0}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mTempLabeledRect:Lcom/google/android/apps/books/render/LabeledRect;

    iget-object v0, v0, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    invoke-static {p6, v0}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    if-nez v6, :cond_1

    .line 222
    const/4 v7, 0x0

    .line 223
    .local v7, "isIcon":Z
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

    const/4 v4, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p5

    move/from16 v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/render/HighlightPaintBox;->getHighlightPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;

    move-result-object v6

    .line 226
    .end local v7    # "isIcon":Z
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRegion:Landroid/graphics/Region;

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mTempLabeledRect:Lcom/google/android/apps/books/render/LabeledRect;

    iget-object v1, v1, Lcom/google/android/apps/books/render/LabeledRect;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    goto :goto_0

    .line 230
    :cond_2
    if-eqz v6, :cond_4

    .line 231
    const-string v0, "PageCanvasPainter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    const-string v0, "PageCanvasPainter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Painting highlights: selected="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_3
    new-instance v8, Landroid/graphics/RegionIterator;

    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRegion:Landroid/graphics/Region;

    invoke-direct {v8, v0}, Landroid/graphics/RegionIterator;-><init>(Landroid/graphics/Region;)V

    .line 235
    .local v8, "regionIterator":Landroid/graphics/RegionIterator;
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v0}, Landroid/graphics/RegionIterator;->next(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mReusedRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 239
    .end local v8    # "regionIterator":Landroid/graphics/RegionIterator;
    :cond_4
    return-void
.end method

.method public paintMarginNoteIcon(Landroid/graphics/Canvas;Ljava/lang/String;Landroid/graphics/Rect;IZ)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "theme"    # Ljava/lang/String;
    .param p3, "paintRect"    # Landroid/graphics/Rect;
    .param p4, "color"    # I
    .param p5, "isSelected"    # Z

    .prologue
    .line 321
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->getMarginNoteIconRenderer()Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;

    move-result-object v12

    .line 324
    .local v12, "renderer":Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v12}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v14, v1, v2

    .line 325
    .local v14, "xScale":F
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v12}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v15, v1, v2

    .line 326
    .local v15, "yScale":F
    invoke-static {v14, v15}, Ljava/lang/Math;->min(FF)F

    move-result v13

    .line 327
    .local v13, "scale":F
    invoke-virtual {v12}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v13

    float-to-int v9, v1

    .line 328
    .local v9, "noteWidth":I
    invoke-virtual {v12}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v13

    float-to-int v8, v1

    .line 329
    .local v8, "noteHeight":I
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    div-int/lit8 v2, v9, 0x2

    sub-int v10, v1, v2

    .line 330
    .local v10, "offsetX":I
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    div-int/lit8 v2, v8, 0x2

    sub-int v11, v1, v2

    .line 333
    .local v11, "offsetY":I
    const/4 v7, 0x1

    .line 334
    .local v7, "isIcon":Z
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->setTheme(Ljava/lang/String;)V

    .line 335
    add-int v1, v10, v9

    add-int v2, v11, v8

    invoke-virtual {v12, v10, v11, v1, v2}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->setBounds(IIII)V

    .line 336
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mHighlightPaintBox:Lcom/google/android/apps/books/render/HighlightPaintBox;

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object/from16 v2, p2

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/books/render/HighlightPaintBox;->getHighlightPaint(Ljava/lang/String;IZZZ)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v12, v1}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->setPaint(Landroid/graphics/Paint;)V

    .line 338
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->setPadding(I)V

    .line 339
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl$MarginNoteIconRenderer;->draw(Landroid/graphics/Canvas;)V

    .line 340
    return-void
.end method

.method public paintMediaViews(Landroid/graphics/Canvas;Ljava/util/List;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/render/TouchableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/render/TouchableItem;>;"
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 348
    const/4 v2, 0x0

    .line 349
    .local v2, "audioPlayButtonDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, 0x0

    .line 350
    .local v1, "audioFrameDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v5, 0x0

    .line 352
    .local v5, "videoPlayButtonDrawable":Landroid/graphics/drawable/Drawable;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/render/TouchableItem;

    .line 356
    .local v4, "item":Lcom/google/android/apps/books/render/TouchableItem;
    iget v6, v4, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 357
    if-nez v2, :cond_1

    .line 359
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/google/android/apps/books/R$styleable;->BooksTheme:[I

    invoke-virtual {v6, v9, v7, v8, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 361
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v6, 0x20

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 363
    const/16 v6, 0x23

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 364
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 366
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_1
    invoke-direct {p0, p1, v2, v1, v4}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->drawMediaButton(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/books/render/TouchableItem;)V

    goto :goto_0

    .line 367
    :cond_2
    iget v6, v4, Lcom/google/android/apps/books/render/TouchableItem;->type:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 368
    if-nez v5, :cond_3

    .line 370
    iget-object v6, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02004c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 373
    :cond_3
    invoke-direct {p0, p1, v5, v9, v4}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->drawMediaButton(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/books/render/TouchableItem;)V

    goto :goto_0

    .line 377
    .end local v4    # "item":Lcom/google/android/apps/books/render/TouchableItem;
    :cond_4
    return-void
.end method

.method public paintPressedVolumeAnnotationRect(Landroid/graphics/Canvas;Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "wrapper"    # Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;

    .prologue
    .line 311
    invoke-interface {p2}, Lcom/google/android/apps/books/render/PageCanvasPainter$RectWrapper;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->mPressedRectDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/books/render/PageCanvasPainterImpl;->drawVolumeAnnotationRect(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/drawable/Drawable;)V

    .line 312
    return-void
.end method
