.class Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;
.super Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;
.source "IframeRenderer.java"

# interfaces
.implements Lcom/google/android/apps/books/render/FixedLayoutPageHandle;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/IframeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FixedLayoutPageHandleImpl"
.end annotation


# instance fields
.field mIndices:Lcom/google/android/apps/books/render/PageIndices;

.field final synthetic this$0:Lcom/google/android/apps/books/render/IframeRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/IframeRenderer;Lcom/google/android/apps/books/model/VolumeMetadata;I)V
    .locals 1
    .param p2, "volumeMetadata"    # Lcom/google/android/apps/books/model/VolumeMetadata;
    .param p3, "bookPageIndex"    # I

    .prologue
    .line 450
    iput-object p1, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    .line 451
    # getter for: Lcom/google/android/apps/books/render/IframeRenderer;->mPageMap:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;
    invoke-static {p1}, Lcom/google/android/apps/books/render/IframeRenderer;->access$200(Lcom/google/android/apps/books/render/IframeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;

    move-result-object v0

    invoke-direct {p0, p2, v0, p3}, Lcom/google/android/apps/books/render/BaseFixedPaginationPageHandle;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Lcom/google/android/apps/books/render/prepaginated/FixedPaginationPageMap;I)V

    .line 452
    return-void
.end method


# virtual methods
.method public getGridRowIndex()I
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    # getter for: Lcom/google/android/apps/books/render/IframeRenderer;->mGridStructure:Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;
    invoke-static {v0}, Lcom/google/android/apps/books/render/IframeRenderer;->access$400(Lcom/google/android/apps/books/render/IframeRenderer;)Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->getBookPageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/render/prepaginated/FixedPaginationGridStructure;->bookPageIndexToGridRowIndex(I)I

    move-result v0

    return v0
.end method

.method public getHeight()I
    .locals 3

    .prologue
    .line 466
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    # getter for: Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/apps/books/render/IframeRenderer;->access$300(Lcom/google/android/apps/books/render/IframeRenderer;)[Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->getBookPageIndex()I

    move-result v2

    aget-object v0, v1, v2

    .line 467
    .local v0, "targetRectF":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    return v1
.end method

.method public getIndices()Lcom/google/android/apps/books/render/PageIndices;
    .locals 3

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    if-nez v0, :cond_0

    .line 478
    new-instance v0, Lcom/google/android/apps/books/render/PageIndices;

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->getBookPageIndex()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/render/PageIndices;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->mIndices:Lcom/google/android/apps/books/render/PageIndices;

    return-object v0
.end method

.method public getWidth()I
    .locals 3

    .prologue
    .line 458
    iget-object v1, p0, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->this$0:Lcom/google/android/apps/books/render/IframeRenderer;

    # getter for: Lcom/google/android/apps/books/render/IframeRenderer;->mPagesRectF:[Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/google/android/apps/books/render/IframeRenderer;->access$300(Lcom/google/android/apps/books/render/IframeRenderer;)[Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/books/render/IframeRenderer$FixedLayoutPageHandleImpl;->getBookPageIndex()I

    move-result v2

    aget-object v0, v1, v2

    .line 459
    .local v0, "targetRectF":Landroid/graphics/RectF;
    iget v1, v0, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    return v1
.end method
