.class Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;
.super Ljava/lang/Object;
.source "BasicReaderController.java"

# interfaces
.implements Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

.field final synthetic val$finalPassage:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;I)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->val$finalPassage:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(II)V
    .locals 8
    .param p1, "passageIndex"    # I
    .param p2, "pageIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 296
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->val$finalPassage:I

    if-ne v0, p1, :cond_0

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # invokes: Lcom/google/android/apps/books/render/BasicReaderController;->allocateRequestId()I
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$300(Lcom/google/android/apps/books/render/BasicReaderController;)I

    move-result v6

    .line 298
    .local v6, "newRequestId":I
    new-instance v7, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mRequest:Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->access$400(Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;)Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v2, v2, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mExternalRequestId:I

    invoke-direct {v7, v0, v1, v2, v6}, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;II)V

    .line 301
    .local v7, "r":Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mRequests:Ljava/util/PriorityQueue;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$500(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/PriorityQueue;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 310
    .end local v6    # "newRequestId":I
    .end local v7    # "r":Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;
    :goto_0
    return-void

    .line 303
    :cond_0
    const-string v0, "ReaderController"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    const-string v0, "ReaderController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected passage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->val$finalPassage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but got: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget-object v0, v0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOutboundListener:Lcom/google/android/apps/books/render/ReaderListener;
    invoke-static {v0}, Lcom/google/android/apps/books/render/BasicReaderController;->access$600(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest$1;->this$1:Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;

    iget v1, v1, Lcom/google/android/apps/books/render/BasicReaderController$ReadableItemsRequest;->mExternalRequestId:I

    const/4 v2, -0x1

    move-object v4, v3

    move-object v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/books/render/ReaderListener;->onPassageTextReady(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V

    goto :goto_0
.end method
