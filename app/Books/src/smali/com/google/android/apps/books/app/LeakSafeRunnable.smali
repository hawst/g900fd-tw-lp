.class public Lcom/google/android/apps/books/app/LeakSafeRunnable;
.super Ljava/lang/Object;
.source "LeakSafeRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private mConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/google/android/ublib/utils/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/ublib/utils/Consumer",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/apps/books/app/LeakSafeRunnable;, "Lcom/google/android/apps/books/app/LeakSafeRunnable<TT;>;"
    .local p1, "target":Ljava/lang/Object;, "TT;"
    .local p2, "consumer":Lcom/google/android/ublib/utils/Consumer;, "Lcom/google/android/ublib/utils/Consumer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p2, p0, Lcom/google/android/apps/books/app/LeakSafeRunnable;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 19
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/LeakSafeRunnable;->mRef:Ljava/lang/ref/WeakReference;

    .line 20
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/apps/books/app/LeakSafeRunnable;, "Lcom/google/android/apps/books/app/LeakSafeRunnable<TT;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/app/LeakSafeRunnable;->mRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 25
    .local v0, "target":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    .line 26
    iget-object v1, p0, Lcom/google/android/apps/books/app/LeakSafeRunnable;->mConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v1, v0}, Lcom/google/android/ublib/utils/Consumer;->take(Ljava/lang/Object;)V

    .line 28
    :cond_0
    return-void
.end method
