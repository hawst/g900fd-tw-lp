.class Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$1;
.super Ljava/lang/Object;
.source "AndroidSpeechSynthesizer.java"

# interfaces
.implements Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$TtsLanguage;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;ZLjava/util/Locale;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$1;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLanguage()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$1;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    # getter for: Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->access$000(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public isLanguageAvailable(Ljava/util/Locale;)I
    .locals 1
    .param p1, "bookLocale"    # Ljava/util/Locale;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer$1;->this$0:Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;

    # getter for: Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->mTts:Landroid/speech/tts/TextToSpeech;
    invoke-static {v0}, Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;->access$000(Lcom/google/android/apps/books/tts/AndroidSpeechSynthesizer;)Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    return v0
.end method
