.class public Lcom/google/android/apps/books/tts/TextToSpeechController;
.super Lcom/google/android/apps/books/app/ReadAlongController;
.source "TextToSpeechController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$TtsReadableItemsRequestData;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;,
        Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;
    }
.end annotation


# instance fields
.field private final mAltTextPrefix:Ljava/lang/String;

.field private mAutoAdvance:Z

.field private final mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

.field private final mEnqueuedPhrases:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;",
            ">;"
        }
    .end annotation
.end field

.field private mFindStartPositionRequestId:I

.field private mLastEnqueuedPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

.field private mLastPingId:Ljava/lang/String;

.field private mPassageCount:I

.field private final mPassageToText:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/tts/SegmentedText;",
            ">;"
        }
    .end annotation
.end field

.field private final mPingScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

.field private mPingSequenceNumber:I

.field private final mPositionToPassage:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/books/common/Position;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mReader:Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;

.field private final mRequestedPassages:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;",
            ">;"
        }
    .end annotation
.end field

.field private mSequenceNumber:I

.field private mStartPassage:I

.field private mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

.field private mStartPosition:Lcom/google/android/apps/books/common/Position;

.field private final mSynthesisCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

.field private mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

.field private final mSynthesizerFactory:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;

.field private mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

.field private mUseNetworkTts:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;ZLjava/lang/String;)V
    .locals 7
    .param p1, "dataSource"    # Lcom/google/android/apps/books/app/ReadAlongController$DataSource;
    .param p2, "synthesizerFactory"    # Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;
    .param p3, "reader"    # Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;
    .param p4, "useNetworkTts"    # Z
    .param p5, "altTextPrefix"    # Ljava/lang/String;

    .prologue
    .line 358
    invoke-static {}, Lcom/google/android/apps/books/util/HandlerPeriodicTaskExecutor;->getFactory()Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/tts/TextToSpeechController;-><init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;ZLjava/lang/String;Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;)V

    .line 360
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/books/app/ReadAlongController$DataSource;Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;ZLjava/lang/String;Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;)V
    .locals 2
    .param p1, "dataSource"    # Lcom/google/android/apps/books/app/ReadAlongController$DataSource;
    .param p2, "synthesizerFactory"    # Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;
    .param p3, "reader"    # Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;
    .param p4, "useNetworkTts"    # Z
    .param p5, "altTextPrefix"    # Ljava/lang/String;
    .param p6, "executorFactory"    # Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;

    .prologue
    const/4 v1, -0x1

    .line 367
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadAlongController;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUseNetworkTts:Z

    .line 119
    iput v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    .line 121
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageToText:Ljava/util/Map;

    .line 165
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    .line 173
    iput v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPassage:I

    .line 188
    sget-object v0, Lcom/google/android/apps/books/tts/TtsUnit;->SENTENCE:Lcom/google/android/apps/books/tts/TtsUnit;

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    .line 212
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPositionToPassage:Ljava/util/Map;

    .line 334
    invoke-static {}, Lcom/google/common/collect/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    .line 1121
    new-instance v0, Lcom/google/android/apps/books/tts/TextToSpeechController$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/tts/TextToSpeechController$2;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController;)V

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesisCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

    .line 368
    iput-object p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    .line 369
    iput-object p2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizerFactory:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;

    .line 370
    iput-object p3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mReader:Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;

    .line 371
    iput-boolean p4, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUseNetworkTts:Z

    .line 372
    iput-object p5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAltTextPrefix:Ljava/lang/String;

    .line 374
    new-instance v0, Lcom/google/android/apps/books/tts/TextToSpeechController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/tts/TextToSpeechController$1;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController;)V

    const/16 v1, 0x1388

    invoke-interface {p6, v0, v1}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Factory;->createExecutor(Lcom/google/android/apps/books/util/PeriodicTaskExecutor$Callbacks;I)Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPingScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    .line 385
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/tts/TextToSpeechController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSpeaking:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/tts/TextToSpeechController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->enqueuePing()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/books/tts/TextToSpeechController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->tearDownSynthesizer()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/tts/TextToSpeechController;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->checkPing(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/books/tts/TextToSpeechController;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/tts/TextToSpeechController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;
    .param p1, "x1"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/books/tts/TextToSpeechController;Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;
    .param p1, "x1"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->focusOnPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/books/tts/TextToSpeechController;)Lcom/google/android/apps/books/app/ReadAlongController$StopReason;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/tts/TextToSpeechController;)Lcom/google/android/apps/books/app/ReadAlongController$StopReason;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/books/tts/TextToSpeechController;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/tts/TextToSpeechController;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueue()V

    return-void
.end method

.method private checkPing(Ljava/lang/String;)Z
    .locals 4
    .param p1, "phraseId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 858
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastPingId:Ljava/lang/String;

    .line 864
    .local v0, "lastPingId":Ljava/lang/String;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastPingId:Ljava/lang/String;

    .line 866
    const-string v2, "BOOKS_TTS_PING_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 867
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPingScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    const-wide/16 v2, 0x1388

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->delay(J)V

    .line 868
    const/4 v1, 0x0

    .line 876
    :cond_0
    :goto_0
    return v1

    .line 871
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 875
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->onQueueFlushed()V

    goto :goto_0
.end method

.method private clearInternalQueue()V
    .locals 2

    .prologue
    .line 641
    const-string v0, "BooksTTS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    const-string v0, "BooksTTS"

    const-string v1, "Clearing TTS controller internal queue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 645
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastEnqueuedPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    .line 646
    iget v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSequenceNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSequenceNumber:I

    .line 647
    return-void
.end method

.method private clearSynthesizerQueue()V
    .locals 2

    .prologue
    .line 631
    const-string v0, "BooksTTS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    const-string v0, "BooksTTS"

    const-string v1, "Clearing TTS synthesizer queue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    if-eqz v0, :cond_1

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    invoke-interface {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;->clearQueue()V

    .line 637
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->clearInternalQueue()V

    .line 638
    return-void
.end method

.method private enqueuePhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;
    .locals 6
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 803
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getPhraseText(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Ljava/lang/String;

    move-result-object v2

    .line 804
    .local v2, "text":Ljava/lang/String;
    new-instance v1, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    iget v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSequenceNumber:I

    invoke-direct {v1, p1, v3}, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;I)V

    .line 808
    .local v1, "key":Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 809
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->focusOnPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    .line 812
    :cond_0
    const-string v3, "BooksTTS"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 813
    const-string v3, "BooksTTS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Speaking "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;->getId()Ljava/lang/String;

    move-result-object v0

    .line 816
    .local v0, "id":Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getSynthesizer()Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    move-result-object v3

    invoke-interface {v3, v2, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;->enqueuePhrase(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    iput-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastEnqueuedPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    .line 820
    return-object v1
.end method

.method private enqueuePing()V
    .locals 3

    .prologue
    .line 824
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BOOKS_TTS_PING_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPingSequenceNumber:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPingSequenceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastPingId:Ljava/lang/String;

    .line 825
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getSynthesizer()Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastPingId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;->enqueuePhrase(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    const-string v0, "BooksTTS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    const-string v0, "BooksTTS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding a ping with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastPingId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    :cond_0
    return-void
.end method

.method private enqueueStartPhrase()Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 720
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    if-eqz v3, :cond_1

    .line 721
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getStartPassage()I

    move-result v1

    .line 722
    .local v1, "startPassage":I
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    iget-object v4, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    invoke-direct {p0, v1, v3, v4}, Lcom/google/android/apps/books/tts/TextToSpeechController;->findPhrase(ILcom/google/android/apps/books/tts/TtsUnit;Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    move-result-object v0

    .line 729
    .end local v1    # "startPassage":I
    .local v0, "phraseSearchResult":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    :goto_0
    iget-object v3, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    if-eqz v3, :cond_2

    .line 730
    iget-object v2, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->enqueuePhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    move-result-object v2

    .line 741
    .end local v0    # "phraseSearchResult":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    :cond_0
    :goto_1
    return-object v2

    .line 723
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    if-eqz v3, :cond_0

    .line 724
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->findPhraseStartingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    move-result-object v0

    .restart local v0    # "phraseSearchResult":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    goto :goto_0

    .line 731
    :cond_2
    iget-object v3, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-eqz v3, :cond_0

    .line 732
    iget-object v3, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    sget-object v4, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->END_OF_BOOK:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-ne v3, v4, :cond_3

    .line 734
    iget-object v3, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    goto :goto_1

    .line 737
    :cond_3
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Failed to find phrase"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private fillPhraseQueue()V
    .locals 1

    .prologue
    .line 782
    iget-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSpeaking:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-eqz v0, :cond_1

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z

    if-eqz v0, :cond_2

    .line 787
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueueForAutoAdvance()V

    goto :goto_0

    .line 789
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->enqueueStartPhrase()Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    goto :goto_0
.end method

.method private fillPhraseQueueForAutoAdvance()V
    .locals 4

    .prologue
    .line 753
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mLastEnqueuedPhrase:Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    .line 755
    .local v0, "lastExaminedPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_2

    .line 756
    if-eqz v0, :cond_3

    .line 757
    iget-object v2, v0, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->findPhraseAfterPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    move-result-object v1

    .line 759
    .local v1, "nextSentence":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    iget-object v2, v1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    if-eqz v2, :cond_1

    .line 760
    iget-object v2, v1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->enqueuePhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    move-result-object v0

    goto :goto_0

    .line 761
    :cond_1
    iget-object v2, v1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    if-eqz v2, :cond_2

    .line 762
    iget-object v2, v1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;->reason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    invoke-direct {p0, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 775
    .end local v1    # "nextSentence":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    :cond_2
    :goto_1
    return-void

    .line 769
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->enqueueStartPhrase()Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    move-result-object v0

    .line 770
    if-nez v0, :cond_0

    goto :goto_1
.end method

.method private findPhrase(ILcom/google/android/apps/books/tts/TtsUnit;Lcom/google/android/apps/books/common/Position;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    .locals 8
    .param p1, "passageIndex"    # I
    .param p2, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;
    .param p3, "position"    # Lcom/google/android/apps/books/common/Position;

    .prologue
    const/4 v7, -0x1

    .line 981
    iget v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    if-ne v5, v7, :cond_0

    .line 983
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    invoke-direct {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>()V

    .line 1044
    :goto_0
    return-object v5

    .line 985
    :cond_0
    move-object v1, p3

    .line 986
    .local v1, "originalPosition":Lcom/google/android/apps/books/common/Position;
    invoke-virtual {p3}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v0

    .line 988
    .local v0, "currentPositionString":Ljava/lang/String;
    :goto_1
    iget v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    if-lt p1, v5, :cond_1

    .line 990
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    sget-object v6, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->END_OF_BOOK:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    goto :goto_0

    .line 992
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    invoke-interface {v5, p1}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->isPassageForbidden(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 994
    add-int/lit8 p1, p1, 0x1

    .line 999
    const/4 v0, 0x0

    goto :goto_1

    .line 1001
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getPassageSegments(ILjava/lang/String;)Lcom/google/android/apps/books/tts/SegmentedText;

    move-result-object v4

    .line 1003
    .local v4, "segments":Lcom/google/android/apps/books/tts/SegmentedText;
    if-eqz v4, :cond_7

    .line 1006
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPositionToPassage:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1008
    .local v2, "passageContainingPosition":Ljava/lang/Integer;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ge v5, p1, :cond_4

    .line 1016
    invoke-virtual {v4, p2}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemCount(Lcom/google/android/apps/books/tts/TtsUnit;)I

    move-result v5

    if-lez v5, :cond_3

    .line 1017
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    new-instance v6, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    const/4 v7, 0x0

    invoke-direct {v6, p1, p2, v7}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    goto :goto_0

    .line 1021
    :cond_3
    add-int/lit8 p1, p1, 0x1

    .line 1022
    const/4 v0, 0x0

    goto :goto_1

    .line 1025
    :cond_4
    invoke-virtual {v4, p2, v1}, Lcom/google/android/apps/books/tts/SegmentedText;->getNearestItemIndex(Lcom/google/android/apps/books/tts/TtsUnit;Lcom/google/android/apps/books/common/Position;)Landroid/util/Pair;

    move-result-object v3

    .line 1027
    .local v3, "positionSearchResult":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1030
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPositionToPassage:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1032
    :cond_5
    iget-object v5, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v5, v7, :cond_6

    .line 1033
    new-instance v6, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    new-instance v7, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget-object v5, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v7, p1, p2, v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    invoke-direct {v6, v7}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    move-object v5, v6

    goto :goto_0

    .line 1037
    :cond_6
    add-int/lit8 p1, p1, 0x1

    .line 1038
    const/4 v0, 0x0

    goto :goto_1

    .line 1044
    .end local v2    # "passageContainingPosition":Ljava/lang/Integer;
    .end local v3    # "positionSearchResult":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Boolean;Ljava/lang/Integer;>;"
    :cond_7
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    invoke-direct {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>()V

    goto/16 :goto_0
.end method

.method private findPhraseAfterPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    .locals 4
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 968
    new-instance v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v1, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    iget v3, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->phraseIndex:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    .line 970
    .local v0, "next":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->findPhraseStartingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    move-result-object v1

    return-object v1
.end method

.method private findPhraseStartingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;
    .locals 8
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 913
    iget v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 915
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    invoke-direct {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>()V

    .line 951
    :goto_0
    return-object v5

    .line 917
    :cond_0
    iget v1, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    .line 918
    .local v1, "passageIndex":I
    iget v2, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->phraseIndex:I

    .line 920
    .local v2, "phraseIndex":I
    :goto_1
    iget v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    if-lt v1, v5, :cond_1

    .line 922
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    sget-object v6, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->END_OF_BOOK:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    goto :goto_0

    .line 923
    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    invoke-interface {v5, v1}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->isPassageForbidden(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 925
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 927
    :cond_2
    const/4 v5, 0x0

    invoke-direct {p0, v1, v5}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getPassageSegments(ILjava/lang/String;)Lcom/google/android/apps/books/tts/SegmentedText;

    move-result-object v4

    .line 928
    .local v4, "segments":Lcom/google/android/apps/books/tts/SegmentedText;
    if-eqz v4, :cond_6

    .line 929
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemCount(Lcom/google/android/apps/books/tts/TtsUnit;)I

    move-result v5

    if-lt v2, v5, :cond_3

    .line 931
    add-int/lit8 v1, v1, 0x1

    .line 932
    const/4 v2, 0x0

    goto :goto_1

    .line 933
    :cond_3
    if-gez v2, :cond_5

    .line 935
    iget-object v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemCount(Lcom/google/android/apps/books/tts/TtsUnit;)I

    move-result v3

    .line 936
    .local v3, "phrasesInPassage":I
    add-int v0, v3, v2

    .line 937
    .local v0, "newPhraseIndex":I
    if-gez v0, :cond_4

    .line 939
    add-int/lit8 v1, v1, -0x1

    .line 940
    move v2, v0

    goto :goto_1

    .line 942
    :cond_4
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    new-instance v6, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget-object v7, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-direct {v6, v1, v7, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    goto :goto_0

    .line 946
    .end local v0    # "newPhraseIndex":I
    .end local v3    # "phrasesInPassage":I
    :cond_5
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    new-instance v6, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget-object v7, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-direct {v6, v1, v7, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    invoke-direct {v5, v6}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V

    goto :goto_0

    .line 951
    :cond_6
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;

    invoke-direct {v5}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseSearchResult;-><init>()V

    goto :goto_0
.end method

.method private focusOnPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)V
    .locals 6
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 1058
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mReader:Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;

    if-nez v3, :cond_1

    .line 1076
    :cond_0
    :goto_0
    return-void

    .line 1062
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageToText:Ljava/util/Map;

    iget v4, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/tts/SegmentedText;

    .line 1063
    .local v2, "segments":Lcom/google/android/apps/books/tts/SegmentedText;
    if-nez v2, :cond_2

    .line 1065
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "missing passage data for passage "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1069
    :cond_2
    iget-object v3, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    iget v4, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->phraseIndex:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemRange(Lcom/google/android/apps/books/tts/TtsUnit;I)Lcom/google/android/apps/books/util/Range;

    move-result-object v1

    .line 1070
    .local v1, "phraseRange":Lcom/google/android/apps/books/util/Range;
    iget v3, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/books/tts/SegmentedText;->getNormalizedRange(Lcom/google/android/apps/books/util/Range;I)Lcom/google/android/apps/books/annotations/TextLocationRange;

    move-result-object v0

    .line 1073
    .local v0, "normalizedPhraseRange":Lcom/google/android/apps/books/annotations/TextLocationRange;
    if-eqz v0, :cond_0

    .line 1074
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mReader:Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;

    invoke-interface {v3, p1, v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;->beganSpeakingPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/annotations/TextLocationRange;)V

    goto :goto_0
.end method

.method private getPassageSegments(ILjava/lang/String;)Lcom/google/android/apps/books/tts/SegmentedText;
    .locals 6
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Ljava/lang/String;

    .prologue
    .line 667
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageToText:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/tts/SegmentedText;

    .line 668
    .local v2, "result":Lcom/google/android/apps/books/tts/SegmentedText;
    new-instance v0, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;-><init>(ILjava/lang/String;)V

    .line 669
    .local v0, "params":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    if-nez v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 670
    const-string v3, "BooksTTS"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 671
    const-string v3, "BooksTTS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Requesting text for passage "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    new-instance v4, Lcom/google/android/apps/books/tts/TextToSpeechController$TtsReadableItemsRequestData;

    invoke-direct {v4, p1, p2}, Lcom/google/android/apps/books/tts/TextToSpeechController$TtsReadableItemsRequestData;-><init>(ILjava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v1

    .line 675
    .local v1, "requestId":I
    if-eqz p2, :cond_1

    .line 676
    iput v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFindStartPositionRequestId:I

    .line 678
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    .end local v1    # "requestId":I
    :cond_2
    return-object v2
.end method

.method private getPhraseText(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;)Ljava/lang/String;
    .locals 3
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .prologue
    .line 794
    iget v1, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController;->getPassageSegments(ILjava/lang/String;)Lcom/google/android/apps/books/tts/SegmentedText;

    move-result-object v0

    .line 795
    .local v0, "segments":Lcom/google/android/apps/books/tts/SegmentedText;
    iget-object v1, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    iget v2, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->phraseIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemText(Lcom/google/android/apps/books/tts/TtsUnit;I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getStartPassage()I
    .locals 3

    .prologue
    .line 689
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    if-eqz v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPositionToPassage:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 691
    .local v0, "precisePassage":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 692
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 698
    .end local v0    # "precisePassage":Ljava/lang/Integer;
    :goto_0
    return v1

    .line 695
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    if-eqz v1, :cond_1

    .line 696
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v1, v1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    goto :goto_0

    .line 698
    :cond_1
    iget v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPassage:I

    goto :goto_0
.end method

.method private getSynthesizer()Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    if-nez v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizerFactory:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;

    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesisCallbacks:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizerFactory;->make(Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesisCallbacks;)Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    return-object v0
.end method

.method private onQueueFlushed()V
    .locals 3

    .prologue
    .line 837
    const-string v1, "BooksTTS"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 838
    const-string v1, "BooksTTS"

    const-string v2, "Resubmitting phrases after queue flush"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 849
    :goto_0
    return-void

    .line 845
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;

    iget-object v0, v1, Lcom/google/android/apps/books/tts/TextToSpeechController$UtteranceKey;->phrase:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 847
    .local v0, "startPhrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-virtual {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->stopSpeaking()V

    .line 848
    iget-boolean v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/books/tts/TextToSpeechController;->startSpeakingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Z)V

    goto :goto_0
.end method

.method private declared-synchronized onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    .locals 1
    .param p1, "reason"    # Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .prologue
    .line 706
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mEnqueuedPhrases:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 707
    iput-object p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 712
    :goto_0
    monitor-exit p0

    return-void

    .line 709
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSpeaking:Z

    .line 710
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mReader:Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/tts/TextToSpeechController$VisualReader;->finishedSpeaking(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 706
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private tearDownSynthesizer()V
    .locals 1

    .prologue
    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->stopSpeaking()V

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    invoke-interface {v0}, Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;->shutdown()V

    .line 559
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSynthesizer:Lcom/google/android/apps/books/tts/TextToSpeechController$SpeechSynthesizer;

    .line 561
    :cond_0
    return-void
.end method


# virtual methods
.method public getUseNetworkTts()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUseNetworkTts:Z

    return v0
.end method

.method public nearestPhraseWithUnit(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Lcom/google/android/apps/books/tts/TtsUnit;)Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .locals 9
    .param p1, "phrase"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .param p2, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 481
    iget-object v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    if-ne v5, p2, :cond_0

    .line 532
    .end local p1    # "phrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    :goto_0
    return-object p1

    .line 485
    .restart local p1    # "phrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageToText:Ljava/util/Map;

    iget v6, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/tts/SegmentedText;

    .line 486
    .local v3, "segments":Lcom/google/android/apps/books/tts/SegmentedText;
    iget v2, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->phraseIndex:I

    .line 488
    .local v2, "phraseIndex":I
    if-nez v3, :cond_3

    .line 492
    if-eqz v2, :cond_1

    if-ne v2, v7, :cond_2

    .line 493
    :cond_1
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v6, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-direct {v5, v6, p2, v2}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    move-object p1, v5

    goto :goto_0

    .line 495
    :cond_2
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Attempt to convert phrase in non-loaded passage"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 499
    :cond_3
    iget-object v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemCount(Lcom/google/android/apps/books/tts/TtsUnit;)I

    move-result v0

    .line 502
    .local v0, "itemCount":I
    if-gez v2, :cond_4

    .line 503
    add-int/2addr v2, v0

    .line 508
    :cond_4
    if-gez v2, :cond_6

    .line 509
    iget v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    if-nez v5, :cond_5

    .line 512
    new-instance p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .end local p1    # "phrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    invoke-direct {p1, v8, p2, v8}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    goto :goto_0

    .line 514
    .restart local p1    # "phrase":Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    :cond_5
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v6, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    add-int/lit8 v6, v6, -0x1

    invoke-direct {v5, v6, p2, v7}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    move-object p1, v5

    goto :goto_0

    .line 520
    :cond_6
    if-lt v2, v0, :cond_8

    .line 521
    iget v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    iget v6, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    add-int/lit8 v6, v6, -0x1

    if-ne v5, v6, :cond_7

    .line 523
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v6, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-direct {v5, v6, p2, v7}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    move-object p1, v5

    goto :goto_0

    .line 526
    :cond_7
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v6, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    add-int/lit8 v6, v6, 0x1

    invoke-direct {v5, v6, p2, v8}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    move-object p1, v5

    goto :goto_0

    .line 530
    :cond_8
    iget-object v5, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    invoke-virtual {v3, v5, v2}, Lcom/google/android/apps/books/tts/SegmentedText;->getItemRange(Lcom/google/android/apps/books/tts/TtsUnit;I)Lcom/google/android/apps/books/util/Range;

    move-result-object v5

    iget v4, v5, Lcom/google/android/apps/books/util/Range;->start:I

    .line 531
    .local v4, "startOffset":I
    invoke-virtual {v3, p2, v4}, Lcom/google/android/apps/books/tts/SegmentedText;->getNearestItemIndex(Lcom/google/android/apps/books/tts/TtsUnit;I)I

    move-result v1

    .line 532
    .local v1, "newItemIndex":I
    new-instance v5, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    iget v6, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->passageIndex:I

    invoke-direct {v5, v6, p2, v1}, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;-><init>(ILcom/google/android/apps/books/tts/TtsUnit;I)V

    move-object p1, v5

    goto :goto_0
.end method

.method public onPendingRequestsCanceled()V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .prologue
    .line 1091
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1094
    .local v2, "newPassagesRequests":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->isSpeaking()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1095
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1097
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;

    .line 1098
    .local v4, "params":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    new-instance v6, Lcom/google/android/apps/books/tts/TextToSpeechController$TtsReadableItemsRequestData;

    iget v7, v4, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->passage:I

    iget-object v8, v4, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->position:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/books/tts/TextToSpeechController$TtsReadableItemsRequestData;-><init>(ILjava/lang/String;)V

    invoke-interface {v5, v6}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->requestReadableItems(Lcom/google/android/apps/books/app/ReadAlongController$ReadableItemsRequestData;)I

    move-result v3

    .line 1100
    .local v3, "newRequestId":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1102
    const-string v5, "BooksTTS"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1103
    const-string v6, "BooksTTS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onPendingRequestsCanceled replacing #"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " by #"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for passage "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v4, Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;->passage:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    :cond_1
    iget v6, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFindStartPositionRequestId:I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v6, v5, :cond_0

    .line 1109
    iput v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFindStartPositionRequestId:I

    goto :goto_0

    .line 1114
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "newRequestId":I
    .end local v4    # "params":Lcom/google/android/apps/books/tts/TextToSpeechController$PassageRequestParams;
    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->clear()V

    .line 1116
    invoke-virtual {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->isSpeaking()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1117
    iget-object v5, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1119
    :cond_3
    return-void
.end method

.method public setPassageCount(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 400
    iget v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 401
    iput p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageCount:I

    .line 402
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueue()V

    .line 404
    :cond_0
    return-void
.end method

.method public setPassageText(IILjava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Lcom/google/android/apps/books/model/LabelMap;)V
    .locals 14
    .param p1, "requestId"    # I
    .param p2, "passageIndex"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "positionOffsets"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p5, "altStringsOffsets"    # Lcom/google/android/apps/books/model/LabelMap;

    .prologue
    .line 575
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 576
    .local v8, "requestKey":Ljava/lang/Integer;
    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-interface {v11, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 577
    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mRequestedPassages:Ljava/util/Map;

    invoke-interface {v11, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    if-nez p4, :cond_3

    .line 584
    :cond_1
    sget-object v11, Lcom/google/android/apps/books/app/ReadAlongController$StopReason;->CONTENT_ERROR:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    invoke-direct {p0, v11}, Lcom/google/android/apps/books/tts/TextToSpeechController;->onStopReason(Lcom/google/android/apps/books/app/ReadAlongController$StopReason;)V

    .line 625
    .end local p3    # "text":Ljava/lang/String;
    :goto_1
    return-void

    .line 578
    .restart local p3    # "text":Ljava/lang/String;
    :cond_2
    const-string v11, "BooksTTS"

    const/4 v12, 0x6

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 579
    const-string v11, "BooksTTS"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "setPassageText: unexpected answer for passage: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " requestId: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 588
    :cond_3
    iget v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFindStartPositionRequestId:I

    if-ne p1, v11, :cond_8

    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    if-eqz v11, :cond_8

    .line 589
    const-string v11, "BooksTTS"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 590
    const-string v11, "BooksTTS"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Found start position in passage "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    :cond_4
    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPositionToPassage:Ljava/util/Map;

    iget-object v12, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 601
    :cond_5
    :goto_2
    invoke-static {}, Lcom/google/android/ublib/utils/SystemUtils;->runningOnLaterThanJellyBeanMR1()Z

    move-result v11

    if-nez v11, :cond_9

    const/4 v7, 0x1

    .line 602
    .local v7, "replaceU2019":Z
    :goto_3
    new-instance v10, Lcom/google/android/apps/books/tts/SegmentedText;

    if-eqz v7, :cond_6

    const/16 v11, 0x2019

    const/16 v12, 0x27

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p3

    .end local p3    # "text":Ljava/lang/String;
    :cond_6
    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    invoke-interface {v11}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->getLocale()Ljava/util/Locale;

    move-result-object v11

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {v10, v0, v1, v11}, Lcom/google/android/apps/books/tts/SegmentedText;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V

    .line 606
    .local v10, "visibleText":Lcom/google/android/apps/books/tts/SegmentedText;
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/apps/books/model/LabelMap;->getNumberOfLabels()I

    move-result v2

    .line 608
    .local v2, "altStringsCount":I
    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAltTextPrefix:Ljava/lang/String;

    if-eqz v11, :cond_7

    if-gtz v2, :cond_a

    .line 609
    :cond_7
    move-object v9, v10

    .line 623
    .local v9, "textToVoice":Lcom/google/android/apps/books/tts/SegmentedText;
    :goto_4
    iget-object v11, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageToText:Ljava/util/Map;

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueue()V

    goto/16 :goto_1

    .line 594
    .end local v2    # "altStringsCount":I
    .end local v7    # "replaceU2019":Z
    .end local v9    # "textToVoice":Lcom/google/android/apps/books/tts/SegmentedText;
    .end local v10    # "visibleText":Lcom/google/android/apps/books/tts/SegmentedText;
    .restart local p3    # "text":Ljava/lang/String;
    :cond_8
    const-string v11, "BooksTTS"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 595
    const-string v11, "BooksTTS"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Received text for passage "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 601
    :cond_9
    const/4 v7, 0x0

    goto :goto_3

    .line 611
    .end local p3    # "text":Ljava/lang/String;
    .restart local v2    # "altStringsCount":I
    .restart local v7    # "replaceU2019":Z
    .restart local v10    # "visibleText":Lcom/google/android/apps/books/tts/SegmentedText;
    :cond_a
    new-array v5, v2, [Ljava/lang/String;

    .line 612
    .local v5, "decoratedAltStrings":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "ii":I
    :goto_5
    if-ge v6, v2, :cond_c

    .line 613
    move-object/from16 v0, p5

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/model/LabelMap;->getLabelAt(I)Ljava/lang/String;

    move-result-object v3

    .line 614
    .local v3, "curAlt":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAltTextPrefix:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    if-eqz v7, :cond_b

    const/16 v12, 0x2019

    const/16 v13, 0x27

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    .end local v3    # "curAlt":Ljava/lang/String;
    :cond_b
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    .line 612
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 618
    :cond_c
    new-instance v4, Lcom/google/android/apps/books/model/LabelMap;

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/apps/books/model/LabelMap;->cloneOffsets()[I

    move-result-object v11

    invoke-direct {v4, v11, v5}, Lcom/google/android/apps/books/model/LabelMap;-><init>([I[Ljava/lang/String;)V

    .line 621
    .local v4, "decoratedAltMap":Lcom/google/android/apps/books/model/LabelMap;
    invoke-static {v10, v4}, Lcom/google/android/apps/books/tts/AltSegmentedText;->create(Lcom/google/android/apps/books/tts/SegmentedText;Lcom/google/android/apps/books/model/LabelMap;)Lcom/google/android/apps/books/tts/AltSegmentedText;

    move-result-object v9

    .restart local v9    # "textToVoice":Lcom/google/android/apps/books/tts/SegmentedText;
    goto :goto_4
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->tearDownSynthesizer()V

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPassageToText:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 552
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    invoke-interface {v0}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->shutDown()V

    .line 553
    return-void
.end method

.method public startSpeakingAtPhrase(Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;Z)V
    .locals 4
    .param p1, "phraseIdentifier"    # Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;
    .param p2, "autoAdvance"    # Z

    .prologue
    const/4 v3, 0x0

    .line 453
    const-string v0, "BooksTTS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    const-string v0, "BooksTTS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting TTS at phrase "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->clearInternalQueue()V

    .line 459
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPassage:I

    .line 460
    iput-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    .line 461
    iput-object p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 462
    iget-object v0, p1, Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;->unit:Lcom/google/android/apps/books/tts/TtsUnit;

    iput-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    .line 463
    iput-boolean p2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z

    .line 465
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSpeaking:Z

    .line 466
    iput-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .line 468
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueue()V

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPingScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    .line 470
    return-void
.end method

.method public startSpeakingAtPosition(ILcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/tts/TtsUnit;Z)V
    .locals 4
    .param p1, "passageIndex"    # I
    .param p2, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p3, "unit"    # Lcom/google/android/apps/books/tts/TtsUnit;
    .param p4, "autoAdvance"    # Z

    .prologue
    const/4 v3, 0x0

    .line 415
    const-string v0, "BooksTTS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 416
    const-string v0, "BooksTTS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting TTS at passage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", unit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->clearInternalQueue()V

    .line 428
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mDataSource:Lcom/google/android/apps/books/app/ReadAlongController$DataSource;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/app/ReadAlongController$DataSource;->isPassageForbidden(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    :goto_0
    return-void

    .line 432
    :cond_1
    iput p1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPassage:I

    .line 433
    iput-object p2, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    .line 434
    iput-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPhraseId:Lcom/google/android/apps/books/tts/TextToSpeechController$PhraseIdentifier;

    .line 435
    iput-object p3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mUnit:Lcom/google/android/apps/books/tts/TtsUnit;

    .line 436
    iput-boolean p4, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mAutoAdvance:Z

    .line 438
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSpeaking:Z

    .line 439
    iput-object v3, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .line 441
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->fillPhraseQueue()V

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mPingScheduler:Lcom/google/android/apps/books/util/PeriodicTaskExecutor;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/PeriodicTaskExecutor;->schedule()V

    goto :goto_0
.end method

.method public stopSpeaking()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 539
    invoke-direct {p0}, Lcom/google/android/apps/books/tts/TextToSpeechController;->clearSynthesizerQueue()V

    .line 540
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPassage:I

    .line 541
    iput-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mStartPosition:Lcom/google/android/apps/books/common/Position;

    .line 542
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mSpeaking:Z

    .line 543
    iput-object v1, p0, Lcom/google/android/apps/books/tts/TextToSpeechController;->mFillPhraseQueueStopReason:Lcom/google/android/apps/books/app/ReadAlongController$StopReason;

    .line 544
    return-void
.end method
