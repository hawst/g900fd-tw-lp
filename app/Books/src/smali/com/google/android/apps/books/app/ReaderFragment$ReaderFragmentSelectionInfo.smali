.class Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;
.super Ljava/lang/Object;
.source "ReaderFragment.java"

# interfaces
.implements Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/ReaderFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReaderFragmentSelectionInfo"
.end annotation


# instance fields
.field private mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

.field final synthetic this$0:Lcom/google/android/apps/books/app/ReaderFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ReaderFragment;Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 0
    .param p2, "annotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 5279
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5280
    iput-object p2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 5281
    return-void
.end method

.method private insideExistingAnnotation()Z
    .locals 1

    .prologue
    .line 5436
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->firstAnnotationInSelection()Lcom/google/android/apps/books/annotations/Annotation;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private overlappingExistingAnnotation()Z
    .locals 3

    .prologue
    .line 5440
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/widget/PagesViewController;->VISIBLE_USER_LAYERS:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/app/SelectionState;->getOverlappingAnnotation(Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/util/Set;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1

    .prologue
    .line 5308
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SelectionState;->createNewClipboardCopy()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1
    .param p1, "androidColor"    # I
    .param p2, "marginNoteText"    # Ljava/lang/String;

    .prologue
    .line 5303
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/books/app/SelectionState;->createNewHighlight(ILjava/lang/String;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;
    .locals 1

    .prologue
    .line 5313
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 5314
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 5316
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->firstAnnotationInSelection()Lcom/google/android/apps/books/annotations/Annotation;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    goto :goto_0
.end method

.method public getHighlightScreenRect()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;
    .locals 15

    .prologue
    const/4 v12, 0x0

    .line 5322
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/UnloadableEventual;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/books/util/UnloadableEventual;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 5323
    .local v8, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 5325
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v11

    if-eqz v11, :cond_0

    if-nez v8, :cond_1

    :cond_0
    move-object v11, v12

    .line 5370
    :goto_0
    return-object v11

    .line 5329
    :cond_1
    if-nez v0, :cond_2

    .line 5330
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/books/widget/PagesViewController;->getSelectionBounds()Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    move-result-object v11

    goto :goto_0

    .line 5333
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v6

    .line 5341
    .local v6, "localId":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 5342
    .local v7, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    new-instance v10, Landroid/graphics/Region;

    invoke-direct {v10}, Landroid/graphics/Region;-><init>()V

    .line 5343
    .local v10, "region":Landroid/graphics/Region;
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v11

    iget-object v13, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewControllerCallbacks:Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;
    invoke-static {v13}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController$ReaderDelegate;

    move-result-object v13

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getLocalId()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v7, v13, v14}, Lcom/google/android/apps/books/render/Renderer;->getHighlightRects(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/widget/AnnotationIndex;Ljava/lang/String;)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v9

    .line 5346
    .local v9, "passageRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    new-instance v3, Lcom/google/android/apps/books/widget/HighlightsSharingColor;

    invoke-direct {v3}, Lcom/google/android/apps/books/widget/HighlightsSharingColor;-><init>()V

    .line 5348
    .local v3, "colorRect":Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    :cond_4
    invoke-interface {v9, v3}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 5349
    new-instance v5, Lcom/google/android/apps/books/render/LabeledRect;

    invoke-direct {v5}, Lcom/google/android/apps/books/render/LabeledRect;-><init>()V

    .line 5350
    .local v5, "labeledRect":Lcom/google/android/apps/books/render/LabeledRect;
    :cond_5
    :goto_1
    iget-object v11, v3, Lcom/google/android/apps/books/widget/HighlightsSharingColor;->highlightRects:Lcom/google/android/apps/books/widget/Walker;

    invoke-interface {v11, v5}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 5352
    iget-object v11, v5, Lcom/google/android/apps/books/render/LabeledRect;->label:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 5353
    invoke-virtual {v5}, Lcom/google/android/apps/books/render/LabeledRect;->getRect()Landroid/graphics/Rect;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/graphics/Region;->union(Landroid/graphics/Rect;)Z

    goto :goto_1

    .line 5358
    .end local v5    # "labeledRect":Lcom/google/android/apps/books/render/LabeledRect;
    :cond_6
    invoke-virtual {v10}, Landroid/graphics/Region;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 5359
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v10}, Landroid/graphics/Region;->getBounds()Landroid/graphics/Rect;

    move-result-object v11

    invoke-direct {v2, v11}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 5361
    .local v2, "boundsF":Landroid/graphics/RectF;
    iget-object v11, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v11}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v11

    invoke-virtual {v11, v2}, Lcom/google/android/apps/books/widget/PagesViewController;->pageToViewMatrixMapRect(Landroid/graphics/RectF;)V

    .line 5363
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 5364
    .local v1, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v2, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 5365
    new-instance v11, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;

    const/4 v13, 0x0

    invoke-direct {v11, v1, v13, v12}, Lcom/google/android/apps/books/widget/SelectionPopupImpl$SelectionBoundsInfo;-><init>(Landroid/graphics/Rect;ILandroid/graphics/Point;)V

    goto/16 :goto_0

    .end local v1    # "bounds":Landroid/graphics/Rect;
    .end local v2    # "boundsF":Landroid/graphics/RectF;
    .end local v3    # "colorRect":Lcom/google/android/apps/books/widget/HighlightsSharingColor;
    .end local v7    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    .end local v9    # "passageRects":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/widget/HighlightsSharingColor;>;"
    .end local v10    # "region":Landroid/graphics/Region;
    :cond_7
    move-object v11, v12

    .line 5370
    goto/16 :goto_0
.end method

.method public getMarginNoteScreenRect()Landroid/graphics/Rect;
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x1

    .line 5375
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mCurrentDevicePages:Lcom/google/android/apps/books/util/UnloadableEventual;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$7400(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/util/UnloadableEventual;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/books/util/UnloadableEventual;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 5376
    .local v6, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/DevicePageRendering;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->getAnnotation()Lcom/google/android/apps/books/annotations/Annotation;

    move-result-object v0

    .line 5378
    .local v0, "annotation":Lcom/google/android/apps/books/annotations/Annotation;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v9

    if-eqz v9, :cond_0

    if-eqz v6, :cond_0

    if-nez v0, :cond_1

    .line 5415
    :cond_0
    :goto_0
    return-object v8

    .line 5383
    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    invoke-virtual {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090156

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 5386
    .local v2, "iconSize":I
    new-instance v7, Lcom/google/android/apps/books/render/MarginNote;

    invoke-direct {v7}, Lcom/google/android/apps/books/render/MarginNote;-><init>()V

    .line 5388
    .local v7, "tempMarginNote":Lcom/google/android/apps/books/render/MarginNote;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # invokes: Lcom/google/android/apps/books/app/ReaderFragment;->displayTwoPages()Z
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12500(Lcom/google/android/apps/books/app/ReaderFragment;)Z

    move-result v9

    if-eqz v9, :cond_2

    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->LEFT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 5401
    .local v5, "pagePosition":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/widget/DevicePageRendering;

    .line 5402
    .local v4, "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    iget-object v9, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mRenderer:Lcom/google/android/apps/books/render/Renderer;
    invoke-static {v9}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12300(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/render/Renderer;

    move-result-object v9

    new-array v10, v12, [Lcom/google/android/apps/books/annotations/Annotation;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v9, v10, v4, v2, v12}, Lcom/google/android/apps/books/render/Renderer;->getMarginNoteIcons(Ljava/util/List;Lcom/google/android/apps/books/widget/DevicePageRendering;IZ)Lcom/google/android/apps/books/widget/Walker;

    move-result-object v3

    .line 5404
    .local v3, "iconsWalker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    invoke-interface {v3}, Lcom/google/android/apps/books/widget/Walker;->reset()V

    .line 5406
    invoke-interface {v3, v7}, Lcom/google/android/apps/books/widget/Walker;->next(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 5408
    iget-object v8, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mPagesViewController:Lcom/google/android/apps/books/widget/PagesViewController;
    invoke-static {v8}, Lcom/google/android/apps/books/app/ReaderFragment;->access$500(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/widget/PagesViewController;

    move-result-object v8

    invoke-virtual {v8, v4, v5, v7}, Lcom/google/android/apps/books/widget/PagesViewController;->getMarginNoteScreenRect(Lcom/google/android/apps/books/widget/DevicePageRendering;Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;Lcom/google/android/apps/books/render/MarginNote;)Landroid/graphics/Rect;

    move-result-object v8

    goto :goto_0

    .line 5388
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "iconsWalker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    .end local v4    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    .end local v5    # "pagePosition":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_2
    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->FULL_SCREEN:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    goto :goto_1

    .line 5412
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "iconsWalker":Lcom/google/android/apps/books/widget/Walker;, "Lcom/google/android/apps/books/widget/Walker<Lcom/google/android/apps/books/render/MarginNote;>;"
    .restart local v4    # "page":Lcom/google/android/apps/books/widget/DevicePageRendering;
    .restart local v5    # "pagePosition":Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;
    :cond_3
    sget-object v5, Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;->RIGHT_PAGE_OF_TWO:Lcom/google/android/apps/books/render/Renderer$PagePositionOnScreen;

    .line 5413
    goto :goto_2
.end method

.method public getNormalizedSelectedText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 5294
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 5295
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getNormalizedSelectedText()Ljava/lang/String;

    move-result-object v0

    .line 5297
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SelectionState;->getNormalizedSelectedText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSelectedText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 5285
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    if-eqz v0, :cond_0

    .line 5286
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    invoke-virtual {v0}, Lcom/google/android/apps/books/annotations/Annotation;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    .line 5288
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mSelectionState:Lcom/google/android/apps/books/app/SelectionState;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$11900(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/SelectionState;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/app/SelectionState;->getSelectedText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isClipboardCopyingLimited()Z
    .locals 1

    .prologue
    .line 5451
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/app/ClipboardCopyController;->isCopyingLimited()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClipboardCopyingPermitted(I)Z
    .locals 1
    .param p1, "textLength"    # I

    .prologue
    .line 5457
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mClipboardCopyController:Lcom/google/android/apps/books/app/ClipboardCopyController;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1000(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/app/ClipboardCopyController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/app/ClipboardCopyController;->isClipboardCopyingPermitted(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 5420
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v2}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->isSample()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5431
    :cond_0
    :goto_0
    return v0

    .line 5422
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    if-eqz v2, :cond_2

    move v0, v1

    .line 5423
    goto :goto_0

    .line 5431
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->insideExistingAnnotation()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->overlappingExistingAnnotation()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public isQuoteSharingPermitted()Z
    .locals 1

    .prologue
    .line 5463
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    if-nez v0, :cond_1

    .line 5465
    :cond_0
    const/4 v0, 0x0

    .line 5467
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v0}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/model/VolumeData;->isQuoteSharingAllowed()Z

    move-result v0

    goto :goto_0
.end method

.method public isSearchingPermitted()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5472
    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    if-nez v1, :cond_1

    .line 5477
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->getSelectedText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/16 v2, 0x32

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->isUploaded()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v1}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/books/model/VolumeData;->isPublicDomain()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isTranslationPermitted()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 5483
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    move v3, v5

    .line 5499
    :goto_0
    return v3

    .line 5487
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->this$0:Lcom/google/android/apps/books/app/ReaderFragment;

    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->mMetadata:Lcom/google/android/apps/books/model/VolumeMetadata;
    invoke-static {v3}, Lcom/google/android/apps/books/app/ReaderFragment;->access$1200(Lcom/google/android/apps/books/app/ReaderFragment;)Lcom/google/android/apps/books/model/VolumeMetadata;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getPublisher()Ljava/lang/String;

    move-result-object v2

    .line 5488
    .local v2, "publisher":Ljava/lang/String;
    if-nez v2, :cond_2

    move v3, v4

    .line 5489
    goto :goto_0

    .line 5492
    :cond_2
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 5493
    .local v1, "lowerCasePublisher":Ljava/lang/String;
    # getter for: Lcom/google/android/apps/books/app/ReaderFragment;->TRANSLATION_RESTRICTED_PUBLISHER_PATTERNS:Ljava/util/Set;
    invoke-static {}, Lcom/google/android/apps/books/app/ReaderFragment;->access$12600()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 5494
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/regex/Pattern;>;"
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5495
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v5

    .line 5496
    goto :goto_0

    :cond_4
    move v3, v4

    .line 5499
    goto :goto_0
.end method

.method public onAnnotationChanged(Lcom/google/android/apps/books/annotations/Annotation;)V
    .locals 0
    .param p1, "newAnnotation"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 5446
    iput-object p1, p0, Lcom/google/android/apps/books/app/ReaderFragment$ReaderFragmentSelectionInfo;->mAnnotation:Lcom/google/android/apps/books/annotations/Annotation;

    .line 5447
    return-void
.end method
