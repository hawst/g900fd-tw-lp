.class Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;
.super Lcom/google/android/apps/books/render/SnapshottingWebView;
.source "SnapshottingWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/SnapshottingWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SnapshottingWebViewHCnICS"
.end annotation


# instance fields
.field private mDrawingCacheBitmap:Landroid/graphics/Bitmap;

.field private mDrawingCacheCanvas:Landroid/graphics/Canvas;

.field private mDrawingCacheContentInvalid:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "drawDelegate"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/render/SnapshottingWebView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V

    .line 238
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;Lcom/google/android/apps/books/render/SnapshottingWebView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;
    .param p3, "x2"    # Lcom/google/android/apps/books/render/SnapshottingWebView$1;

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/render/SnapshottingWebView$DrawDelegate;)V

    return-void
.end method


# virtual methods
.method public getDrawingCache()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDrawingCache(Z)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "autoScale"    # Z

    .prologue
    .line 341
    const/4 v0, 0x0

    return-object v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheContentInvalid:Z

    .line 336
    invoke-super {p0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->invalidate()V

    .line 337
    return-void
.end method

.method public invalidate(IIII)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheContentInvalid:Z

    .line 330
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/books/render/SnapshottingWebView;->invalidate(IIII)V

    .line 331
    return-void
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "dirty"    # Landroid/graphics/Rect;

    .prologue
    .line 323
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheContentInvalid:Z

    .line 324
    invoke-super {p0, p1}, Lcom/google/android/apps/books/render/SnapshottingWebView;->invalidate(Landroid/graphics/Rect;)V

    .line 325
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 246
    iput-object v1, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheBitmap:Landroid/graphics/Bitmap;

    .line 248
    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/books/render/SnapshottingWebView$SnapshottingWebViewHCnICS;->mDrawingCacheCanvas:Landroid/graphics/Canvas;

    .line 249
    invoke-super {p0}, Lcom/google/android/apps/books/render/SnapshottingWebView;->onDetachedFromWindow()V

    .line 250
    return-void
.end method
