.class public Lcom/google/android/apps/books/provider/StaleContentDeleter;
.super Ljava/lang/Object;
.source "StaleContentDeleter.java"


# direct methods
.method private static computeStaleContentSize(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/util/Set;)J
    .locals 6
    .param p0, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/BooksDataStore;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "volumesToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const-wide/16 v0, 0x0

    .line 70
    .local v0, "bytesToRecover":J
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 71
    .local v3, "volumeId":Ljava/lang/String;
    invoke-interface {p0, v3}, Lcom/google/android/apps/books/model/BooksDataStore;->getContentSize(Ljava/lang/String;)J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 72
    goto :goto_0

    .line 73
    .end local v3    # "volumeId":Ljava/lang/String;
    :cond_0
    return-wide v0
.end method

.method public static deleteStaleContentLocked(Lcom/google/android/apps/books/model/BooksDataStore;J)V
    .locals 13
    .param p0, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p1, "keepAge"    # J

    .prologue
    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long v4, v10, p1

    .line 47
    .local v4, "cutoffTime":J
    invoke-interface {p0, v4, v5}, Lcom/google/android/apps/books/model/BooksDataStore;->getStaleVolumeIdsToDelete(J)Ljava/util/Set;

    move-result-object v9

    .line 50
    .local v9, "volumesToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p0}, Lcom/google/android/apps/books/model/BooksDataStore;->getFileStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 51
    .local v0, "baseDir":Ljava/io/File;
    invoke-static {p0, v9}, Lcom/google/android/apps/books/provider/StaleContentDeleter;->computeStaleContentSize(Lcom/google/android/apps/books/model/BooksDataStore;Ljava/util/Set;)J

    move-result-wide v2

    .line 52
    .local v2, "bytesToRecover":J
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->freeBytesOnFilesystem(Ljava/io/File;)J

    move-result-wide v6

    .line 53
    .local v6, "freeSpaceInBytes":J
    long-to-float v10, v2

    long-to-float v11, v6

    const v12, 0x3dcccccd    # 0.1f

    mul-float/2addr v11, v12

    cmpg-float v10, v10, v11

    if-gez v10, :cond_1

    .line 60
    :cond_0
    return-void

    .line 57
    :cond_1
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 58
    .local v8, "volumeId":Ljava/lang/String;
    invoke-interface {p0, v8}, Lcom/google/android/apps/books/model/BooksDataStore;->deleteContent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static deleteUnwantedContent(Lcom/google/android/apps/books/model/BooksDataStore;Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)V
    .locals 6
    .param p0, "dataStore"    # Lcom/google/android/apps/books/model/BooksDataStore;
    .param p1, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;

    .prologue
    .line 106
    invoke-static {p1}, Lcom/google/android/apps/books/provider/StaleContentDeleter;->getUnwantedVolumeIds(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)Ljava/util/Set;

    move-result-object v1

    .line 107
    .local v1, "idsToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 108
    .local v2, "volumeId":Ljava/lang/String;
    const-string v3, "StaleContentDeleter"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 109
    const-string v3, "StaleContentDeleter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting unwanted content for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    invoke-interface {p0, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->deleteContent(Ljava/lang/String;)V

    goto :goto_0

    .line 113
    .end local v2    # "volumeId":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public static getUnwantedVolumeIds(Lcom/google/android/apps/books/model/MyEbooksVolumesResults;)Ljava/util/Set;
    .locals 8
    .param p0, "myEbooksVolumesResults"    # Lcom/google/android/apps/books/model/MyEbooksVolumesResults;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/MyEbooksVolumesResults;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 83
    .local v1, "idsToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->myEbooksVolumes:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/VolumeData;

    .line 84
    .local v4, "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    invoke-interface {v4}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v5

    .line 85
    .local v5, "volumeId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->localVolumeData:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/LocalVolumeData;

    .line 87
    .local v3, "myVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    iget-object v6, p0, Lcom/google/android/apps/books/model/MyEbooksVolumesResults;->downloadProgress:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    .line 90
    .local v2, "myDownloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    if-eqz v3, :cond_0

    sget-object v6, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->RELEASE:Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/LocalVolumeData;->getLicenseAction()Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->isPartiallyOrFullyDownloaded()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 94
    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    .end local v2    # "myDownloadProgress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .end local v3    # "myVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    .end local v4    # "volumeData":Lcom/google/android/apps/books/model/VolumeData;
    .end local v5    # "volumeId":Ljava/lang/String;
    :cond_1
    return-object v1
.end method
