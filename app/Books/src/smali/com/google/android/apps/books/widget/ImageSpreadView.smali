.class public Lcom/google/android/apps/books/widget/ImageSpreadView;
.super Lcom/google/android/apps/books/widget/BaseSpreadView;
.source "ImageSpreadView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;
    }
.end annotation


# instance fields
.field private final mBackgroundColor:I

.field private final mDisplayTwoPages:Z

.field private final mPageCellSize:Landroid/graphics/Point;

.field final mSpreadContentRect:Landroid/graphics/Rect;

.field private final mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZI)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayTwoPages"    # Z
    .param p3, "backgroundColor"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/BaseSpreadView;-><init>()V

    .line 34
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mPageCellSize:Landroid/graphics/Point;

    .line 37
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mSpreadContentRect:Landroid/graphics/Rect;

    .line 43
    new-instance v2, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;-><init>(Lcom/google/android/apps/books/widget/ImageSpreadView;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    .line 45
    iput-boolean p2, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mDisplayTwoPages:Z

    .line 46
    iput p3, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mBackgroundColor:I

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;->from(Landroid/content/res/Resources;)Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;

    move-result-object v0

    .line 50
    .local v0, "bookmarkMeasurements":Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->LEFT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .line 51
    .local v1, "firstViewSide":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/books/widget/ImageSpreadView;->addPageView(Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 52
    if-eqz p2, :cond_0

    .line 53
    sget-object v2, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/books/widget/ImageSpreadView;->addPageView(Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 55
    :cond_0
    return-void

    .line 50
    .end local v1    # "firstViewSide":Lcom/google/android/apps/books/render/Renderer$SideOfSpine;
    :cond_1
    sget-object v1, Lcom/google/android/apps/books/render/Renderer$SideOfSpine;->RIGHT:Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/ImageSpreadView;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ImageSpreadView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mPageCellSize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/ImageSpreadView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/ImageSpreadView;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mDisplayTwoPages:Z

    return v0
.end method

.method private addPageView(Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V
    .locals 2
    .param p1, "bookmarkMeasurements"    # Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;
    .param p2, "sideOfSpine"    # Lcom/google/android/apps/books/render/Renderer$SideOfSpine;

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/apps/books/widget/PageView;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/books/widget/PageView;-><init>(Landroid/content/Context;Lcom/google/android/apps/books/view/pages/BookmarkMeasurements;)V

    .line 65
    .local v0, "result":Lcom/google/android/apps/books/widget/PageView;
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/widget/PageView;->setSideOfSpine(Lcom/google/android/apps/books/render/Renderer$SideOfSpine;)V

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->addView(Landroid/view/View;)V

    .line 67
    return-void
.end method

.method private getPageView(I)Lcom/google/android/apps/books/widget/PageView;
    .locals 2
    .param p1, "pageOffset"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getViewIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/PageView;

    return-object v0
.end method

.method private getViewIndex(I)I
    .locals 1
    .param p1, "pageOffset"    # I

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    .line 102
    add-int/lit8 p1, p1, 0x1

    .line 104
    .end local p1    # "pageOffset":I
    :cond_0
    return p1
.end method


# virtual methods
.method public clearContent()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/books/widget/PageView;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 72
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/books/widget/PageView;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->invalidate()V

    .line 76
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    return-object v0
.end method

.method public invalidatePageContent()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PageView;->invalidateContent()V

    .line 194
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mDisplayTwoPages:Z

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PageView;->invalidateContent()V

    .line 197
    :cond_0
    return-void
.end method

.method public onBookmarkChanged(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/PageView;->onBookmarksChanged()V

    .line 183
    return-void
.end method

.method public scaleAndScroll(FFF)V
    .locals 2
    .param p1, "scale"    # F
    .param p2, "scrollX"    # F
    .param p3, "scrollY"    # F

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setScaleX(F)V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setScaleY(F)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setPivotX(F)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setPivotY(F)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    neg-float v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setTranslationX(F)V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/books/widget/ImageSpreadView;->mView:Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;

    neg-float v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/widget/ImageSpreadView$MyViewGroup;->setTranslationY(F)V

    .line 207
    return-void
.end method

.method public setPageContent(ILcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V
    .locals 1
    .param p1, "pageOffset"    # I
    .param p2, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p3, "bookmark"    # Lcom/google/android/apps/books/view/pages/BookmarkAnimator;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/books/widget/PageView;->setContent(Lcom/google/android/apps/books/util/SimpleDrawable;Lcom/google/android/apps/books/view/pages/BookmarkAnimator;)V

    .line 91
    return-void
.end method

.method public setPageLoading(ILandroid/graphics/Point;)V
    .locals 1
    .param p1, "screenOffset"    # I
    .param p2, "size"    # Landroid/graphics/Point;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/widget/PageView;->setLoading(Landroid/graphics/Point;)V

    .line 81
    return-void
.end method

.method public setPageToSpecialPage(ILcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V
    .locals 1
    .param p1, "screenOffset"    # I
    .param p2, "pageType"    # Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;
    .param p3, "content"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p4, "displaySize"    # Landroid/graphics/Point;

    .prologue
    .line 188
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/ImageSpreadView;->getPageView(I)Lcom/google/android/apps/books/widget/PageView;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/apps/books/widget/PageView;->setPageToSpecialPage(Lcom/google/android/apps/books/widget/PagesView$SpecialPageDisplayType;Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;)V

    .line 189
    return-void
.end method
