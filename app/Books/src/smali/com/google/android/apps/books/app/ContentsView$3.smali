.class Lcom/google/android/apps/books/app/ContentsView$3;
.super Lcom/google/android/ublib/widget/TabFactory;
.source "ContentsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/ContentsView;->createTabFactories()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/app/ContentsView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/ContentsView;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 269
    iput-object p1, p0, Lcom/google/android/apps/books/app/ContentsView$3;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    invoke-direct {p0, p2}, Lcom/google/android/ublib/widget/TabFactory;-><init>(I)V

    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$3;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    # invokes: Lcom/google/android/apps/books/app/ContentsView;->createBookmarkView(Landroid/view/ViewGroup;)Landroid/view/View;
    invoke-static {v0, p1}, Lcom/google/android/apps/books/app/ContentsView;->access$300(Lcom/google/android/apps/books/app/ContentsView;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onGainingPrimaryStatus(Landroid/view/View;)V
    .locals 3
    .param p1, "createdView"    # Landroid/view/View;

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/google/android/ublib/widget/TabFactory;->onGainingPrimaryStatus(Landroid/view/View;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/books/app/ContentsView$3;->this$0:Lcom/google/android/apps/books/app/ContentsView;

    const v1, 0x7f0e00d9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;->VIEW_BOOKMARK_LIST:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;

    # invokes: Lcom/google/android/apps/books/app/ContentsView;->logTabListView(Landroid/view/View;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/app/ContentsView;->access$200(Lcom/google/android/apps/books/app/ContentsView;Landroid/view/View;Lcom/google/android/apps/books/app/BooksAnalyticsTracker$TocAction;)V

    .line 280
    return-void
.end method
