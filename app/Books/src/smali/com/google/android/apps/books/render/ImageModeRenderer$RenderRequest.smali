.class Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;
.super Ljava/lang/Object;
.source "ImageModeRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/ImageModeRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderRequest"
.end annotation


# instance fields
.field final consumerRef:Ljava/lang/ref/Reference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/Reference",
            "<",
            "Lcom/google/android/apps/books/render/RenderResponseConsumer;",
            ">;"
        }
    .end annotation
.end field

.field final position:Lcom/google/android/apps/books/render/RenderPosition;

.field final sequenceNumber:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/render/ImageModeRenderer;Lcom/google/android/apps/books/render/RenderPosition;Lcom/google/android/apps/books/render/RenderResponseConsumer;I)V
    .locals 1
    .param p2, "p"    # Lcom/google/android/apps/books/render/RenderPosition;
    .param p3, "c"    # Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .param p4, "sequenceNumber"    # I

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    .line 212
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->consumerRef:Ljava/lang/ref/Reference;

    .line 213
    iput p4, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->sequenceNumber:I

    .line 214
    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->getConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->isCanceled()Z

    move-result v0

    return v0
.end method

.method private getConsumer()Lcom/google/android/apps/books/render/RenderResponseConsumer;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->consumerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/render/RenderResponseConsumer;

    return-object v0
.end method

.method private isCanceled()Z
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->this$0:Lcom/google/android/apps/books/render/ImageModeRenderer;

    invoke-virtual {v0}, Lcom/google/android/apps/books/render/ImageModeRenderer;->getSequenceNumber()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->sequenceNumber:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->consumerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 226
    invoke-static {p0}, Lcom/google/common/base/Objects;->toStringHelper(Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "position"

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->position:Lcom/google/android/apps/books/render/RenderPosition;

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    const-string v1, "consumer"

    iget-object v2, p0, Lcom/google/android/apps/books/render/ImageModeRenderer$RenderRequest;->consumerRef:Ljava/lang/ref/Reference;

    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/base/Objects$ToStringHelper;->add(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/Objects$ToStringHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/Objects$ToStringHelper;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
