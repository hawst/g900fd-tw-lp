.class public abstract Lcom/google/android/apps/books/app/TableOfContentsActivity;
.super Lcom/google/android/apps/books/app/SimpleFragmentActivity;
.source "TableOfContentsActivity.java"


# static fields
.field private static mNextArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/books/app/SimpleFragmentActivity;-><init>()V

    return-void
.end method

.method private getArguments()Lcom/google/android/apps/books/app/ContentsView$Arguments;
    .locals 3

    .prologue
    .line 85
    sget-object v1, Lcom/google/android/apps/books/app/TableOfContentsActivity;->mNextArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "volumeId"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/books/app/TableOfContentsActivity;->mNextArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    iget-object v2, v2, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v2}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    sget-object v0, Lcom/google/android/apps/books/app/TableOfContentsActivity;->mNextArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .line 91
    .local v0, "result":Lcom/google/android/apps/books/app/ContentsView$Arguments;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/apps/books/app/TableOfContentsActivity;->mNextArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .line 92
    return-object v0

    .line 89
    .end local v0    # "result":Lcom/google/android/apps/books/app/ContentsView$Arguments;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "result":Lcom/google/android/apps/books/app/ContentsView$Arguments;
    goto :goto_0
.end method

.method public static getMoveType(Landroid/os/Bundle;)Lcom/google/android/apps/books/app/MoveType;
    .locals 1
    .param p0, "result"    # Landroid/os/Bundle;

    .prologue
    .line 33
    const-string v0, "action"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/app/MoveType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/MoveType;

    move-result-object v0

    return-object v0
.end method

.method public static getPosition(Landroid/os/Bundle;)Lcom/google/android/apps/books/common/Position;
    .locals 2
    .param p0, "result"    # Landroid/os/Bundle;

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/apps/books/common/Position;

    const-string v1, "position"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/common/Position;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getTitleExtra()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static prepareTableOfContentsActivity(Landroid/content/Context;Lcom/google/android/apps/books/app/ContentsView$Arguments;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "arguments"    # Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .prologue
    .line 51
    sput-object p1, Lcom/google/android/apps/books/app/TableOfContentsActivity;->mNextArguments:Lcom/google/android/apps/books/app/ContentsView$Arguments;

    .line 53
    new-instance v2, Lcom/google/android/apps/books/preference/LocalPreferences;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/preference/LocalPreferences;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/apps/books/preference/LocalPreferences;->getReaderTheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/util/ReaderUtils;->shouldUseDarkTheme(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    const-class v0, Lcom/google/android/apps/books/app/TableOfContentsActivityDark;

    .line 58
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/android/apps/books/app/TableOfContentsActivity;>;"
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v1, "result":Landroid/content/Intent;
    const-string v2, "volumeId"

    iget-object v3, p1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v2, "title"

    iget-object v3, p1, Lcom/google/android/apps/books/app/ContentsView$Arguments;->metadata:Lcom/google/android/apps/books/model/VolumeMetadata;

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeMetadata;->getVolumeData()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/books/model/VolumeData;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    return-object v1

    .line 56
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/android/apps/books/app/TableOfContentsActivity;>;"
    .end local v1    # "result":Landroid/content/Intent;
    :cond_0
    const-class v0, Lcom/google/android/apps/books/app/TableOfContentsActivityLight;

    .restart local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/google/android/apps/books/app/TableOfContentsActivity;>;"
    goto :goto_0
.end method


# virtual methods
.method protected addFragments()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 97
    invoke-direct {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getArguments()Lcom/google/android/apps/books/app/ContentsView$Arguments;

    move-result-object v0

    .line 98
    .local v0, "arguments":Lcom/google/android/apps/books/app/ContentsView$Arguments;
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 100
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 101
    .local v3, "ft":Landroid/support/v4/app/FragmentTransaction;
    new-instance v2, Lcom/google/android/apps/books/app/TableOfContentsFragment;

    invoke-direct {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->getTitleExtra()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Lcom/google/android/apps/books/app/TableOfContentsFragment;-><init>(Lcom/google/android/apps/books/app/ContentsView$Arguments;Ljava/lang/String;)V

    .line 103
    .local v2, "fragment":Lcom/google/android/apps/books/app/TableOfContentsFragment;
    invoke-virtual {v2, v5}, Lcom/google/android/apps/books/app/TableOfContentsFragment;->setRetainInstance(Z)V

    .line 104
    const v4, 0x7f0e01f7

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 105
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 113
    .end local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v2    # "fragment":Lcom/google/android/apps/books/app/TableOfContentsFragment;
    .end local v3    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :goto_0
    return v5

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-static {p0}, Lcom/google/android/apps/books/app/BaseBooksActivity;->getOrientation(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->setRequestedOrientation(I)V

    .line 68
    const v0, 0x7f0400cf

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->setContentView(I)V

    .line 69
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->finish()V

    .line 124
    const/4 v0, 0x1

    return v0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 129
    invoke-super {p0}, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->onStart()V

    .line 130
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStart(Landroid/app/Activity;)V

    .line 131
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 135
    invoke-super {p0}, Lcom/google/android/apps/books/app/SimpleFragmentActivity;->onStop()V

    .line 136
    invoke-static {p0}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker;->activityStop(Landroid/app/Activity;)V

    .line 137
    return-void
.end method

.method public setResult(Lcom/google/android/apps/books/common/Position;Lcom/google/android/apps/books/app/MoveType;)V
    .locals 3
    .param p1, "position"    # Lcom/google/android/apps/books/common/Position;
    .param p2, "action"    # Lcom/google/android/apps/books/app/MoveType;

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 73
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "position"

    invoke-virtual {p1}, Lcom/google/android/apps/books/common/Position;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string v1, "action"

    invoke-virtual {p2}, Lcom/google/android/apps/books/app/MoveType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->setResult(ILandroid/content/Intent;)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/TableOfContentsActivity;->finish()V

    .line 77
    return-void
.end method
