.class public Lcom/google/android/apps/books/util/InterfaceCallLogger;
.super Ljava/lang/Object;
.source "InterfaceCallLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;,
        Lcom/google/android/apps/books/util/InterfaceCallLogger$Quiet;,
        Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;
    }
.end annotation


# static fields
.field private static final mIndent:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/ThreadLocal;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/apps/books/util/InterfaceCallLogger;->mIndent:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method private static castAsReplacement(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1, "replacement"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 134
    .local p0, "original":Ljava/lang/Object;, "TT;"
    return-object p1
.end method

.method public static getLoggingInstance(Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p0, "logger"    # Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;",
            "TT;)TT;"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "obj":Ljava/lang/Object;, "TT;"
    if-nez p1, :cond_0

    .line 139
    const/4 v4, 0x0

    .line 153
    :goto_0
    return-object v4

    .line 142
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 143
    .local v0, "allInterfaces":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<*>;>;"
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 144
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    const-class v4, Ljava/lang/Object;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 145
    invoke-virtual {v1}, Ljava/lang/Class;->getInterfaces()[Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 146
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_1

    .line 149
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/Class;

    invoke-interface {v0, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Class;

    .line 151
    .local v2, "ifList":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;

    invoke-direct {v5, p1, p0}, Lcom/google/android/apps/books/util/InterfaceCallLogger$LoggingInvocationHandler;-><init>(Ljava/lang/Object;Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;)V

    invoke-static {v4, v2, v5}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v3

    .line 153
    .local v3, "proxy":Ljava/lang/Object;
    invoke-static {p1, v3}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->castAsReplacement(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_0
.end method

.method public static getLoggingInstance(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "ITT;)TT;"
        }
    .end annotation

    .prologue
    .line 167
    .local p2, "obj":Ljava/lang/Object;, "TT;"
    invoke-static {p0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 181
    .end local p2    # "obj":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p2

    .line 171
    .restart local p2    # "obj":Ljava/lang/Object;, "TT;"
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/util/InterfaceCallLogger$1;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/books/util/InterfaceCallLogger$1;-><init>(ILjava/lang/String;)V

    .line 181
    .local v0, "logger":Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;
    invoke-static {v0, p2}, Lcom/google/android/apps/books/util/InterfaceCallLogger;->getLoggingInstance(Lcom/google/android/apps/books/util/InterfaceCallLogger$MessageLogger;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_0
.end method
