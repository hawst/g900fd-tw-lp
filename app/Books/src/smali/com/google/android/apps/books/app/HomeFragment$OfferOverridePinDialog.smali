.class public Lcom/google/android/apps/books/app/HomeFragment$OfferOverridePinDialog;
.super Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/HomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OfferOverridePinDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2022
    invoke-direct {p0}, Lcom/google/android/apps/books/app/BookConfirmationDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected onOkClicked(Z)V
    .locals 6
    .param p1, "checkboxChecked"    # Z

    .prologue
    .line 2025
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$OfferOverridePinDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 2026
    .local v2, "args":Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getAccount(Landroid/os/Bundle;)Landroid/accounts/Account;

    move-result-object v0

    .line 2027
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {v2}, Lcom/google/android/apps/books/util/VolumeArguments;->getId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    .line 2028
    .local v3, "volumeId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/books/app/HomeFragment$OfferOverridePinDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2030
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 2031
    invoke-static {v1, v0}, Lcom/google/android/apps/books/app/BooksApplication;->getForegroundDataController(Landroid/content/Context;Landroid/accounts/Account;)Lcom/google/android/apps/books/data/ForegroundBooksDataController;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v3, v5}, Lcom/google/android/apps/books/data/ForegroundBooksDataController;->setForceDownload(Ljava/lang/String;Z)V

    .line 2034
    :cond_0
    return-void
.end method
