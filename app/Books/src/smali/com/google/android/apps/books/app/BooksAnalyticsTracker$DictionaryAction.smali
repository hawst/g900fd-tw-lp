.class public final enum Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;
.super Ljava/lang/Enum;
.source "BooksAnalyticsTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/app/BooksAnalyticsTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DictionaryAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

.field public static final enum OFFLINE_DICTIONARY_DOWNLOAD_FINISHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

.field public static final enum OFFLINE_DICTIONARY_DOWNLOAD_STARTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

.field public static final enum OFFLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

.field public static final enum OFFLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

.field public static final enum ONLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

.field public static final enum ONLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 549
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    const-string v1, "OFFLINE_DICTIONARY_DOWNLOAD_STARTED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_DOWNLOAD_STARTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .line 554
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    const-string v1, "OFFLINE_DICTIONARY_DOWNLOAD_FINISHED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_DOWNLOAD_FINISHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .line 559
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    const-string v1, "OFFLINE_DICTIONARY_LOOKUP_SUCCEEDED"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .line 564
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    const-string v1, "OFFLINE_DICTIONARY_LOOKUP_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .line 569
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    const-string v1, "ONLINE_DICTIONARY_LOOKUP_SUCCEEDED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .line 574
    new-instance v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    const-string v1, "ONLINE_DICTIONARY_LOOKUP_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    .line 545
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_DOWNLOAD_STARTED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_DOWNLOAD_FINISHED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->OFFLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_SUCCEEDED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->ONLINE_DICTIONARY_LOOKUP_FAILED:Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 545
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 545
    const-class v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;
    .locals 1

    .prologue
    .line 545
    sget-object v0, Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->$VALUES:[Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/app/BooksAnalyticsTracker$DictionaryAction;

    return-object v0
.end method
