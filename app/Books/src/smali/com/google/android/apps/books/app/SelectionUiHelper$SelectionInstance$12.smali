.class Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$12;
.super Ljava/lang/Object;
.source "SelectionUiHelper.java"

# interfaces
.implements Lcom/google/android/apps/books/app/HighlightColorSelector$ColorSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->createNoteMenu(Landroid/content/Context;Landroid/view/View;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$12;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public colorSelected(I)V
    .locals 2
    .param p1, "prefColorIndex"    # I

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$12;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->highlightSelectionFromPrefColorId(IZ)V

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance$12;->this$1:Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;

    # getter for: Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->mNotePopupMenu:Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;
    invoke-static {v0}, Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;->access$1300(Lcom/google/android/apps/books/app/SelectionUiHelper$SelectionInstance;)Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ublib/cardlib/layout/LegacyPopupMenu;->dismiss()V

    .line 783
    return-void
.end method
