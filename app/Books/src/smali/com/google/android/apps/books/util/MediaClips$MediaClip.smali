.class public Lcom/google/android/apps/books/util/MediaClips$MediaClip;
.super Ljava/lang/Object;
.source "MediaClips.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/MediaClips;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaClip"
.end annotation


# instance fields
.field public devicePage:I

.field public foundInPassageContent:Z

.field private final mAudioResource:Lcom/google/android/apps/books/model/Resource;

.field private final mClipBeginMs:I

.field private final mClipEndMs:I

.field private final mElementId:Ljava/lang/String;

.field private final mReadingPosition:Ljava/lang/String;

.field private final mSegmentId:Ljava/lang/String;

.field public passageIndex:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;II)V
    .locals 0
    .param p1, "segmentId"    # Ljava/lang/String;
    .param p2, "readingPosition"    # Ljava/lang/String;
    .param p3, "elementId"    # Ljava/lang/String;
    .param p4, "audioResource"    # Lcom/google/android/apps/books/model/Resource;
    .param p5, "clipBeginMs"    # I
    .param p6, "clipEndMs"    # I

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p1, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mSegmentId:Ljava/lang/String;

    .line 170
    iput-object p2, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mReadingPosition:Ljava/lang/String;

    .line 171
    iput-object p3, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mElementId:Ljava/lang/String;

    .line 172
    iput-object p4, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mAudioResource:Lcom/google/android/apps/books/model/Resource;

    .line 173
    iput p5, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mClipBeginMs:I

    .line 174
    iput p6, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mClipEndMs:I

    .line 175
    return-void
.end method

.method public static createSilentDelayClip(I)Lcom/google/android/apps/books/util/MediaClips$MediaClip;
    .locals 7
    .param p0, "durationMs"    # I

    .prologue
    const/4 v1, 0x0

    .line 206
    new-instance v0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;

    const/4 v5, 0x0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/books/util/MediaClips$MediaClip;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;II)V

    return-object v0
.end method


# virtual methods
.method public getAudioResource()Lcom/google/android/apps/books/model/Resource;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mAudioResource:Lcom/google/android/apps/books/model/Resource;

    return-object v0
.end method

.method public getClipBeginMs()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mClipBeginMs:I

    return v0
.end method

.method public getClipEndMs()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mClipEndMs:I

    return v0
.end method

.method public getElementId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mElementId:Ljava/lang/String;

    return-object v0
.end method

.method public getSegmentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/books/util/MediaClips$MediaClip;->mSegmentId:Ljava/lang/String;

    return-object v0
.end method
