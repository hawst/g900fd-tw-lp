.class public Lcom/google/android/apps/books/widget/SegmentPositionResolver;
.super Ljava/lang/Object;
.source "SegmentPositionResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;,
        Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    }
.end annotation


# instance fields
.field private final mLeftCenter:I

.field private final mRightCenter:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "leftCenter"    # I
    .param p2, "rightCenter"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mLeftCenter:I

    .line 70
    iput p2, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mRightCenter:I

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/widget/SegmentPositionResolver;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mLeftCenter:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/widget/SegmentPositionResolver;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/SegmentPositionResolver;

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mRightCenter:I

    return v0
.end method

.method private totalBetweenCenters(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;>;"
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->totalWidth(Ljava/util/List;)I

    move-result v1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getLeftHalfWidth()I

    move-result v0

    sub-int/2addr v1, v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    invoke-virtual {v0}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->getRightHalfWidth()I

    move-result v0

    sub-int v0, v1, v0

    return v0
.end method

.method private totalWidth(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;>;"
    const/4 v2, 0x0

    .line 121
    .local v2, "total":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .line 122
    .local v1, "segment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    iget v3, v1, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->width:I

    add-int/2addr v2, v3

    .line 123
    goto :goto_0

    .line 124
    .end local v1    # "segment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    :cond_0
    return v2
.end method


# virtual methods
.method public resolvePositions(Ljava/util/List;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "segments":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    .line 83
    .local v7, "size":I
    if-eqz v7, :cond_0

    iget v8, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mRightCenter:I

    iget v9, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mLeftCenter:I

    sub-int/2addr v8, v9

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->totalBetweenCenters(Ljava/util/List;)I

    move-result v9

    if-ge v8, v9, :cond_1

    .line 84
    :cond_0
    const/4 v8, 0x0

    .line 111
    :goto_0
    return v8

    .line 87
    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 89
    .local v0, "clusterList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;>;"
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 90
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;

    .line 92
    .local v6, "segment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    iget v8, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mLeftCenter:I

    iget v9, p0, Lcom/google/android/apps/books/widget/SegmentPositionResolver;->mRightCenter:I

    invoke-virtual {v6, v8, v9}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;->constrainTo(II)V

    .line 93
    new-instance v8, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;

    invoke-direct {v8, p0, v6}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;-><init>(Lcom/google/android/apps/books/widget/SegmentPositionResolver;Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;)V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 98
    .end local v6    # "segment":Lcom/google/android/apps/books/widget/SegmentPositionResolver$Segment;
    :cond_2
    const/4 v1, 0x0

    .line 99
    .local v1, "conflicting":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_3

    .line 100
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;

    .line 101
    .local v2, "firstCluster":Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
    add-int/lit8 v8, v3, 0x1

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;

    .line 102
    .local v5, "secondCluster":Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
    invoke-virtual {v2, v5}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->isConflicting(Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 103
    invoke-virtual {v2, v5}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->merge(Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;)V

    .line 104
    invoke-interface {v0, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 105
    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;->resolve()V

    .line 106
    const/4 v1, 0x1

    .line 110
    .end local v2    # "firstCluster":Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
    .end local v5    # "secondCluster":Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
    :cond_3
    if-nez v1, :cond_2

    .line 111
    const/4 v8, 0x1

    goto :goto_0

    .line 99
    .restart local v2    # "firstCluster":Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
    .restart local v5    # "secondCluster":Lcom/google/android/apps/books/widget/SegmentPositionResolver$SegmentCluster;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method
