.class Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;
.super Ljava/lang/Object;
.source "PageTurnController.java"

# interfaces
.implements Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageTurnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartTurn"
.end annotation


# instance fields
.field final mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

.field final mFinishAutomatically:Z

.field final mSequenceNumber:I

.field final synthetic this$0:Lcom/google/android/apps/books/widget/PageTurnController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;Z)V
    .locals 1
    .param p2, "direction"    # Lcom/google/android/apps/books/util/ScreenDirection;
    .param p3, "finishAutomatically"    # Z

    .prologue
    .line 387
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 388
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    .line 389
    iput-boolean p3, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mFinishAutomatically:Z

    .line 390
    # ++operator for: Lcom/google/android/apps/books/widget/PageTurnController;->mEnqueuedTurnSequenceNumber:I
    invoke-static {p1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$004(Lcom/google/android/apps/books/widget/PageTurnController;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mSequenceNumber:I

    .line 391
    return-void
.end method


# virtual methods
.method public canBeCanceled()Z
    .locals 2

    .prologue
    .line 441
    iget v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mSequenceNumber:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I
    invoke-static {v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$200(Lcom/google/android/apps/books/widget/PageTurnController;)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canRun()Z
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mTurnDirection:Lcom/google/android/apps/books/util/ScreenDirection;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$100(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/util/ScreenDirection;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDirection()Lcom/google/android/apps/books/util/ScreenDirection;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    return-object v0
.end method

.method public run()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x3

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mSequenceNumber:I

    # setter for: Lcom/google/android/apps/books/widget/PageTurnController;->mStartedTurnSequenceNumber:I
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$202(Lcom/google/android/apps/books/widget/PageTurnController;I)I

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    # getter for: Lcom/google/android/apps/books/widget/PageTurnController;->mCallbacks:Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;
    invoke-static {v0}, Lcom/google/android/apps/books/widget/PageTurnController;->access$300(Lcom/google/android/apps/books/widget/PageTurnController;)Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/widget/PageTurnController$Callbacks;->canTurn(Lcom/google/android/apps/books/util/ScreenDirection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    const-string v0, "PageTurnController"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const-string v0, "PageTurnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting turn in direction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with sequence number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mSequenceNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    # invokes: Lcom/google/android/apps/books/widget/PageTurnController;->startTurn(Lcom/google/android/apps/books/util/ScreenDirection;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/books/widget/PageTurnController;->access$400(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;)V

    .line 425
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mFinishAutomatically:Z

    if-eqz v0, :cond_3

    .line 426
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 427
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    new-instance v0, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->this$0:Lcom/google/android/apps/books/widget/PageTurnController;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mSequenceNumber:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/widget/PageTurnController$EndTurnDirectional;-><init>(Lcom/google/android/apps/books/widget/PageTurnController;Lcom/google/android/apps/books/util/ScreenDirection;ZFI)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    :goto_1
    return-object v6

    .line 414
    .end local v6    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    :cond_2
    const-string v0, "PageTurnController"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    const-string v0, "PageTurnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Suppressing turn in direction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mDirection:Lcom/google/android/apps/books/util/ScreenDirection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with sequence number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/widget/PageTurnController$StartTurn;->mSequenceNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 429
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .restart local v6    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/PageTurnController$PageTurnTask;>;"
    goto :goto_1
.end method
