.class Lcom/google/android/apps/books/tts/WordSegmentation;
.super Lcom/google/android/apps/books/tts/SubstringSegmentation;
.source "WordSegmentation.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "positionMap"    # Lcom/google/android/apps/books/model/LabelMap;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/tts/SubstringSegmentation;-><init>(Ljava/lang/String;Lcom/google/android/apps/books/model/LabelMap;Ljava/util/Locale;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected buildSegmentation(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/util/Range;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v6, p0, Lcom/google/android/apps/books/tts/WordSegmentation;->mLocale:Ljava/util/Locale;

    invoke-static {p1, v6}, Lcom/google/android/apps/books/util/TextSegmentation;->findWords(Ljava/lang/String;Ljava/util/Locale;)Lcom/google/android/apps/books/util/TextSegmentation;

    move-result-object v6

    iget-object v3, v6, Lcom/google/android/apps/books/util/TextSegmentation;->offsets:Ljava/util/List;

    .line 30
    .local v3, "offsets":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 31
    .local v0, "count":I
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 32
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/util/Range;>;"
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 34
    add-int/lit8 v6, v0, -0x1

    if-ne v2, v6, :cond_1

    .line 35
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 39
    .local v1, "end":I
    :goto_1
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 40
    .local v5, "start":I
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/books/tts/WordSegmentation;->shouldIncludeItem(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 41
    new-instance v6, Lcom/google/android/apps/books/util/Range;

    invoke-direct {v6, v5, v1}, Lcom/google/android/apps/books/util/Range;-><init>(II)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 37
    .end local v1    # "end":I
    .end local v5    # "start":I
    :cond_1
    add-int/lit8 v6, v2, 0x1

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .restart local v1    # "end":I
    goto :goto_1

    .line 44
    .end local v1    # "end":I
    :cond_2
    return-object v4
.end method
