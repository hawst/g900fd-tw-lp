.class public Lcom/google/android/apps/books/util/LanguageUtil;
.super Ljava/lang/Object;
.source "LanguageUtil.java"


# direct methods
.method public static shouldSubstringSearch(Ljava/lang/String;)Z
    .locals 1
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    .line 36
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "th"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static stringToLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 8
    .param p0, "locale"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "result":Ljava/util/Locale;
    if-eqz p0, :cond_0

    .line 50
    const-string v3, "[_-]"

    invoke-virtual {p0, v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "tokens":[Ljava/lang/String;
    array-length v0, v2

    .line 52
    .local v0, "numTokens":I
    if-ne v0, v5, :cond_1

    .line 53
    new-instance v1, Ljava/util/Locale;

    .end local v1    # "result":Ljava/util/Locale;
    aget-object v3, v2, v4

    invoke-direct {v1, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 61
    .end local v0    # "numTokens":I
    .end local v2    # "tokens":[Ljava/lang/String;
    .restart local v1    # "result":Ljava/util/Locale;
    :cond_0
    :goto_0
    return-object v1

    .line 54
    .restart local v0    # "numTokens":I
    .restart local v2    # "tokens":[Ljava/lang/String;
    :cond_1
    if-ne v0, v6, :cond_2

    .line 55
    new-instance v1, Ljava/util/Locale;

    .end local v1    # "result":Ljava/util/Locale;
    aget-object v3, v2, v4

    aget-object v4, v2, v5

    invoke-direct {v1, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v1    # "result":Ljava/util/Locale;
    goto :goto_0

    .line 56
    :cond_2
    if-lt v0, v7, :cond_0

    .line 57
    new-instance v1, Ljava/util/Locale;

    .end local v1    # "result":Ljava/util/Locale;
    aget-object v3, v2, v4

    aget-object v4, v2, v5

    aget-object v5, v2, v6

    invoke-direct {v1, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v1    # "result":Ljava/util/Locale;
    goto :goto_0
.end method
