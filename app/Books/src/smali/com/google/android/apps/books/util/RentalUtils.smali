.class public Lcom/google/android/apps/books/util/RentalUtils;
.super Ljava/lang/Object;
.source "RentalUtils.java"


# direct methods
.method public static canBePurchasedOrRented(Lcom/google/android/apps/books/playcards/BookDocument$Saleability;)Z
    .locals 1
    .param p0, "saleability"    # Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->FOR_SALE_AND_RENTAL:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->FOR_RENTAL_ONLY:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/books/playcards/BookDocument$Saleability;->FOR_SALE:Lcom/google/android/apps/books/playcards/BookDocument$Saleability;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getBannerDescription(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 71
    invoke-static {p1}, Lcom/google/android/apps/books/util/RentalUtils;->isRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/util/RentalUtils;->getTimeLeftBadgeDescription(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 74
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f01c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 78
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getBannerText(Landroid/content/Context;Lcom/google/android/apps/books/model/VolumeData;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "data"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 54
    invoke-static {p1}, Lcom/google/android/apps/books/util/RentalUtils;->isRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v2

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/books/util/RentalUtils;->getTimeLeftBadge(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 57
    :cond_0
    invoke-interface {p1}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getNextTimeLeftBadgeTransition(JJ)J
    .locals 8
    .param p0, "nowMillis"    # J
    .param p2, "expirationTimeMillis"    # J

    .prologue
    const-wide/32 v6, 0x36ee80

    .line 162
    sub-long v2, p2, p0

    .line 165
    .local v2, "timeLeft":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    .line 167
    const-wide v4, 0x7fffffffffffffffL

    .line 187
    :goto_0
    return-wide v4

    .line 168
    :cond_0
    cmp-long v4, v2, v6

    if-gez v4, :cond_1

    .line 170
    move-wide v0, v2

    .line 187
    .local v0, "nextTransitionMillis":J
    :goto_1
    add-long v4, p0, v0

    goto :goto_0

    .line 171
    .end local v0    # "nextTransitionMillis":J
    :cond_1
    const-wide/32 v4, 0x5265c0

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    .line 173
    sub-long v0, v2, v6

    .restart local v0    # "nextTransitionMillis":J
    goto :goto_1

    .line 174
    .end local v0    # "nextTransitionMillis":J
    :cond_2
    const-wide/32 v4, 0xa4cb800

    cmp-long v4, v2, v4

    if-gez v4, :cond_3

    .line 176
    const-wide/32 v4, 0x1b7740

    add-long/2addr v4, v2

    rem-long v0, v4, v6

    .restart local v0    # "nextTransitionMillis":J
    goto :goto_1

    .line 177
    .end local v0    # "nextTransitionMillis":J
    :cond_3
    const-wide/32 v4, 0xa83a680

    cmp-long v4, v2, v4

    if-gez v4, :cond_4

    .line 179
    rem-long v0, v2, v6

    .restart local v0    # "nextTransitionMillis":J
    goto :goto_1

    .line 185
    .end local v0    # "nextTransitionMillis":J
    :cond_4
    const-wide/32 v0, 0x36ee80

    .restart local v0    # "nextTransitionMillis":J
    goto :goto_1
.end method

.method public static getSaleOrRentalText(Lcom/google/android/apps/books/app/PurchaseInfo;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 4
    .param p0, "purchaseInfo"    # Lcom/google/android/apps/books/app/PurchaseInfo;
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 238
    iget-boolean v1, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->isRentable:Z

    if-eqz v1, :cond_0

    const v0, 0x7f0f0065

    .line 240
    .local v0, "buttonTextId":I
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/app/PurchaseInfo;->lowestPriceString:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 238
    .end local v0    # "buttonTextId":I
    :cond_0
    const v0, 0x7f0f0064

    goto :goto_0
.end method

.method public static getTimeLeftBadge(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nowMillis"    # J
    .param p3, "expirationTimeMillis"    # J

    .prologue
    const-wide/32 v8, 0x36ee80

    .line 99
    sub-long v4, p3, p1

    .line 100
    .local v4, "timeLeft":J
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 108
    .local v2, "resources":Landroid/content/res/Resources;
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 109
    const v3, 0x7f0f01c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 120
    :goto_0
    return-object v3

    .line 110
    :cond_0
    cmp-long v3, v4, v8

    if-gez v3, :cond_1

    .line 111
    const v3, 0x7f0f01c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 112
    :cond_1
    const-wide/32 v6, 0xa4cb800

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 113
    const-wide/32 v6, 0x1b7740

    add-long/2addr v6, v4

    div-long/2addr v6, v8

    long-to-int v0, v6

    .line 114
    .local v0, "numUnits":I
    const v1, 0x7f110008

    .line 120
    .local v1, "resourceId":I
    :goto_1
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 116
    .end local v0    # "numUnits":I
    .end local v1    # "resourceId":I
    :cond_2
    const-wide/32 v6, 0x2932e00

    add-long/2addr v6, v4

    const-wide/32 v8, 0x5265c00

    div-long/2addr v6, v8

    long-to-int v0, v6

    .line 117
    .restart local v0    # "numUnits":I
    const v1, 0x7f11000a

    .restart local v1    # "resourceId":I
    goto :goto_1
.end method

.method public static getTimeLeftBadgeDescription(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nowMillis"    # J
    .param p3, "expirationTimeMillis"    # J

    .prologue
    const-wide/32 v8, 0x36ee80

    .line 129
    sub-long v4, p3, p1

    .line 130
    .local v4, "timeLeft":J
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 137
    .local v2, "resources":Landroid/content/res/Resources;
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 138
    const v3, 0x7f0f01c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 149
    :goto_0
    return-object v3

    .line 139
    :cond_0
    cmp-long v3, v4, v8

    if-gez v3, :cond_1

    .line 140
    const v3, 0x7f0f01c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 141
    :cond_1
    const-wide/32 v6, 0xa4cb800

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 142
    const-wide/32 v6, 0x1b7740

    add-long/2addr v6, v4

    div-long/2addr v6, v8

    long-to-int v0, v6

    .line 143
    .local v0, "numUnits":I
    const v1, 0x7f110009

    .line 149
    .local v1, "resourceId":I
    :goto_1
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 145
    .end local v0    # "numUnits":I
    .end local v1    # "resourceId":I
    :cond_2
    const-wide/32 v6, 0x2932e00

    add-long/2addr v6, v4

    const-wide/32 v8, 0x5265c00

    div-long/2addr v6, v8

    long-to-int v0, v6

    .line 146
    .restart local v0    # "numUnits":I
    const v1, 0x7f11000b

    .restart local v1    # "resourceId":I
    goto :goto_1
.end method

.method public static isExpired(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 6
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 87
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "rentalState":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 89
    .local v0, "now":J
    if-eqz v2, :cond_0

    const-string v3, "EXPIRED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalExpiration()J

    move-result-wide v4

    cmp-long v3, v4, v0

    if-ltz v3, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalStart()J

    move-result-wide v4

    cmp-long v3, v4, v0

    if-lez v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isExpiredNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 1
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 205
    invoke-static {p0}, Lcom/google/android/apps/books/util/RentalUtils;->isNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/books/util/RentalUtils;->isExpired(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNonSampleRental(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 1
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 198
    invoke-static {p0}, Lcom/google/android/apps/books/util/RentalUtils;->isRental(Lcom/google/android/apps/books/model/VolumeData;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->isLimitedPreview()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRental(Lcom/google/android/apps/books/model/VolumeData;)Z
    .locals 1
    .param p0, "volumeData"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 191
    invoke-interface {p0}, Lcom/google/android/apps/books/model/VolumeData;->getRentalState()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
