.class public Lcom/google/android/apps/books/model/BooksDataStoreImpl;
.super Ljava/lang/Object;
.source "BooksDataStoreImpl.java"

# interfaces
.implements Lcom/google/android/apps/books/model/BooksDataStore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$ResourceResourcesQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$SegmentResourceQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$ResourceQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$SectionQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$PageQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$ChapterQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$CollectionVolumesQuery;,
        Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;
    }
.end annotation


# static fields
.field private static final CCBOX_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final COLLECTION_MEMBERSHIP_OPERATIONS:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final CONTENT_KEY_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final FULL_DOWNLOAD_PROGRESS_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final GENERIC_RESOURCE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final LOCAL_PAGE_STATE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final OWNED_VOLUMES_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final PAGE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final VOLUMES_FOR_LICENSE_RENEWAL_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final VOLUME_ID_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final VOLUME_VERSION_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

.field private static final mHomeRecommendationFileLock:Ljava/lang/Object;

.field private static final sStopWords:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAssetManager:Landroid/content/res/AssetManager;

.field private final mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

.field private final mClock:Lcom/google/android/apps/books/model/Clock;

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

.field private final mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

.field private final mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

.field private final mLogger:Lcom/google/android/apps/books/util/Logger;

.field private final mSyncController:Lcom/google/android/apps/books/sync/SyncController;

.field private final mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

.field private final mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 134
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "volume_id"

    aput-object v2, v1, v4

    const-string v2, "content_version"

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->VOLUME_VERSION_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 137
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "volume_id"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->VOLUME_ID_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 139
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "volume_id"

    aput-object v2, v1, v4

    const-string v2, "title"

    aput-object v2, v1, v5

    const-string v2, "pinned"

    aput-object v2, v1, v6

    const-string v2, "has_offline_license"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->VOLUMES_FOR_LICENSE_RENEWAL_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 142
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "volume_id"

    aput-object v2, v1, v4

    const-string v2, "viewability"

    aput-object v2, v1, v5

    const-string v2, "buy_url"

    aput-object v2, v1, v6

    const-string v2, "open_access"

    aput-object v2, v1, v3

    const-string v2, "rental_state"

    aput-object v2, v1, v7

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->OWNED_VOLUMES_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 148
    invoke-static {}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildStopWords()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->sStopWords:Ljava/util/HashSet;

    .line 1123
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "page_id"

    aput-object v2, v1, v4

    const-string v2, "page_order"

    aput-object v2, v1, v5

    const-string v2, "content_status"

    aput-object v2, v1, v6

    const-string v2, "title"

    aput-object v2, v1, v3

    const-string v2, "remote_url"

    aput-object v2, v1, v7

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->PAGE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 1151
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "cc_box_x"

    aput-object v2, v1, v4

    const-string v2, "cc_box_y"

    aput-object v2, v1, v5

    const-string v2, "cc_box_w"

    aput-object v2, v1, v6

    const-string v2, "cc_box_h"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->CCBOX_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 1211
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "storage_format"

    aput-object v2, v1, v4

    const-string v2, "session_key_id"

    aput-object v2, v1, v5

    const-string v2, "content_status"

    aput-object v2, v1, v6

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->CONTENT_KEY_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 1215
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->CONTENT_KEY_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "structure_status"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith([Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->LOCAL_PAGE_STATE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 2080
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$DownloadProgressQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "preferred_mode"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;->augmentedWith([Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeQuery;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->FULL_DOWNLOAD_PROGRESS_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 2083
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "remote_url"

    aput-object v2, v1, v4

    const-string v2, "resource_type"

    aput-object v2, v1, v5

    const-string v2, "language"

    aput-object v2, v1, v6

    const-string v2, "md5_hash"

    aput-object v2, v1, v3

    const-string v2, "is_shared"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "is_default"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "overlay"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->GENERIC_RESOURCE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 2123
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mHomeRecommendationFileLock:Ljava/lang/Object;

    .line 2429
    new-instance v0, Lcom/google/android/apps/books/model/StringSafeQuery;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "volume_id"

    aput-object v2, v1, v4

    const-string v2, "dirty"

    aput-object v2, v1, v5

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/model/StringSafeQuery;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->COLLECTION_MEMBERSHIP_OPERATIONS:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/accounts/Account;Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;Lcom/google/android/apps/books/common/VolumeUsageManager;Lcom/google/android/apps/books/sync/SyncAccountsState;Lcom/google/android/apps/books/common/FileStorageManager;Lcom/google/android/apps/books/sync/SyncController;Lcom/google/android/apps/books/model/Clock;Lcom/google/android/apps/books/util/Logger;Lcom/google/android/apps/books/provider/VolumeContentStore;Landroid/content/res/AssetManager;Lcom/google/android/apps/books/model/DataControllerStore;)V
    .locals 0
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "broadcaster"    # Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;
    .param p4, "volumeUsageManager"    # Lcom/google/android/apps/books/common/VolumeUsageManager;
    .param p5, "syncState"    # Lcom/google/android/apps/books/sync/SyncAccountsState;
    .param p6, "fileStorageManager"    # Lcom/google/android/apps/books/common/FileStorageManager;
    .param p7, "syncController"    # Lcom/google/android/apps/books/sync/SyncController;
    .param p8, "clock"    # Lcom/google/android/apps/books/model/Clock;
    .param p9, "logger"    # Lcom/google/android/apps/books/util/Logger;
    .param p10, "contentStore"    # Lcom/google/android/apps/books/provider/VolumeContentStore;
    .param p11, "assetManager"    # Landroid/content/res/AssetManager;
    .param p12, "dataControllerStore"    # Lcom/google/android/apps/books/model/DataControllerStore;

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    .line 186
    iput-object p2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    .line 187
    iput-object p3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    .line 188
    iput-object p4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    .line 189
    iput-object p5, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    .line 190
    iput-object p6, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    .line 191
    iput-object p7, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    .line 192
    iput-object p8, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    .line 193
    iput-object p9, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mLogger:Lcom/google/android/apps/books/util/Logger;

    .line 194
    iput-object p10, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    .line 195
    iput-object p11, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAssetManager:Landroid/content/res/AssetManager;

    .line 196
    iput-object p12, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    .line 197
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Lcom/google/android/apps/books/common/FileStorageManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mFileStorageManager:Lcom/google/android/apps/books/common/FileStorageManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/model/BooksDataStoreImpl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/apps/books/model/Page;
    .param p3, "x3"    # Ljava/lang/Boolean;
    .param p4, "x4"    # Ljava/lang/Boolean;
    .param p5, "x5"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    .line 133
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalPageState(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/books/model/SessionKeyId;)V

    return-void
.end method

.method static synthetic access$200()Lcom/google/android/apps/books/model/StringSafeQuery;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->GENERIC_RESOURCE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/model/BooksDataStoreImpl;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private addResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 12
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1304
    .local p3, "resources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/Resource;>;"
    if-nez p3, :cond_0

    .line 1320
    :goto_0
    return-void

    .line 1307
    :cond_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 1308
    .local v6, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1309
    .local v10, "resourceDirUri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 1310
    .local v8, "index":I
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Resource;

    .line 1311
    .local v2, "resource":Lcom/google/android/apps/books/model/Resource;
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-eqz p2, :cond_1

    move v4, v8

    :goto_2
    const/4 v5, 0x2

    move-object v1, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/provider/DataConversions;->resourceToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Ljava/lang/String;II)Landroid/content/ContentValues;

    move-result-object v11

    .line 1314
    .local v11, "values":Landroid/content/ContentValues;
    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    .line 1316
    .local v9, "op":Landroid/content/ContentProviderOperation;
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1317
    add-int/lit8 v8, v8, 0x1

    .line 1318
    goto :goto_1

    .line 1311
    .end local v9    # "op":Landroid/content/ContentProviderOperation;
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_1
    const/4 v4, -0x1

    goto :goto_2

    .line 1319
    .end local v2    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_2
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->applyBatch(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private addResourcesToBatch(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/List;)V
    .locals 12
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1815
    .local p3, "batch":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v9

    .line 1816
    .local v9, "resourceIdToSegmentId":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegmentResources()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/books/model/SegmentResource;

    .line 1817
    .local v10, "segmentResource":Lcom/google/android/apps/books/model/SegmentResource;
    invoke-interface {v10}, Lcom/google/android/apps/books/model/SegmentResource;->getResourceId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1818
    invoke-interface {v10}, Lcom/google/android/apps/books/model/SegmentResource;->getResourceId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10}, Lcom/google/android/apps/books/model/SegmentResource;->getSegmentId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1824
    .end local v10    # "segmentResource":Lcom/google/android/apps/books/model/SegmentResource;
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 1825
    .local v8, "resourceDirUri":Landroid/net/Uri;
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/IdentifiableCollection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/Resource;

    .line 1826
    .local v2, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v2}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1828
    .local v3, "relatedSegmentId":Ljava/lang/String;
    const/4 v7, -0x1

    .line 1830
    .local v7, "order":I
    invoke-interface {v2}, Lcom/google/android/apps/books/model/Resource;->getIsShared()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x3

    .line 1832
    .local v5, "contentStatus":I
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, -0x1

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/provider/DataConversions;->resourceToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;Ljava/lang/String;II)Landroid/content/ContentValues;

    move-result-object v11

    .line 1834
    .local v11, "values":Landroid/content/ContentValues;
    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1830
    .end local v5    # "contentStatus":I
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_2
    const/4 v5, 0x2

    goto :goto_2

    .line 1837
    .end local v2    # "resource":Lcom/google/android/apps/books/model/Resource;
    .end local v3    # "relatedSegmentId":Ljava/lang/String;
    .end local v7    # "order":I
    :cond_3
    return-void
.end method

.method private addSegmentResourcesToBatch(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/List;)V
    .locals 14
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeManifest;",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1841
    .local p3, "batch":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentProviderOperation;>;"
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getResources()Lcom/google/android/apps/books/util/IdentifiableCollection;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/books/util/IdentifiableCollection;->getIdToValue()Ljava/util/Map;

    move-result-object v5

    .line 1843
    .local v5, "resourceIdToResource":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/Resource;>;"
    iget-object v12, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v12, p1}, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;->buildDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1844
    .local v10, "uri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 1845
    .local v1, "currentSegmentId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1846
    .local v6, "resourceIndexInSegment":I
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegmentResources()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/books/model/SegmentResource;

    .line 1847
    .local v9, "segmentResource":Lcom/google/android/apps/books/model/SegmentResource;
    invoke-interface {v9}, Lcom/google/android/apps/books/model/SegmentResource;->getResourceId()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v5, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/model/Resource;

    .line 1848
    .local v4, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v4}, Lcom/google/android/apps/books/model/Resource;->getMimeType()Ljava/lang/String;

    move-result-object v12

    const-string v13, "text/css"

    invoke-static {v12, v13}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1849
    invoke-interface {v9}, Lcom/google/android/apps/books/model/SegmentResource;->getSegmentId()Ljava/lang/String;

    move-result-object v8

    .line 1850
    .local v8, "segmentId":Ljava/lang/String;
    invoke-static {v8, v1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 1852
    const/4 v6, 0x0

    .line 1853
    move-object v1, v8

    .line 1855
    :cond_1
    iget-object v12, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v12, v12, Landroid/accounts/Account;->name:Ljava/lang/String;

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "resourceIndexInSegment":I
    .local v7, "resourceIndexInSegment":I
    invoke-static {v12, p1, v9, v6}, Lcom/google/android/apps/books/provider/DataConversions;->segmentResourceToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/SegmentResource;I)Landroid/content/ContentValues;

    move-result-object v11

    .line 1857
    .local v11, "values":Landroid/content/ContentValues;
    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1858
    .local v3, "op":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v3, v11}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v6, v7

    .end local v7    # "resourceIndexInSegment":I
    .restart local v6    # "resourceIndexInSegment":I
    goto :goto_0

    .line 1861
    .end local v3    # "op":Landroid/content/ContentProviderOperation$Builder;
    .end local v4    # "resource":Lcom/google/android/apps/books/model/Resource;
    .end local v8    # "segmentId":Ljava/lang/String;
    .end local v9    # "segmentResource":Lcom/google/android/apps/books/model/SegmentResource;
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_2
    return-void
.end method

.method private static appendIfChanged(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "oldVal"    # Ljava/lang/String;
    .param p3, "newVal"    # Ljava/lang/String;

    .prologue
    .line 582
    invoke-static {p2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 583
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    const-string v0, ", "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    :cond_1
    return-void
.end method

.method private applyBatch(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1324
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "com.google.android.apps.books"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1330
    return-void

    .line 1325
    :catch_0
    move-exception v0

    .line 1326
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Lcom/google/android/ublib/utils/WrappedIoException;

    const-string v2, "Error applying operations"

    invoke-direct {v1, v2, v0}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1327
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1328
    .local v0, "e":Landroid/content/OperationApplicationException;
    new-instance v1, Lcom/google/android/ublib/utils/WrappedIoException;

    const-string v2, "Error applying operations"

    invoke-direct {v1, v2, v0}, Lcom/google/android/ublib/utils/WrappedIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static booleanToProviderInt(Z)I
    .locals 1
    .param p0, "b"    # Z

    .prologue
    .line 266
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private buildCollectionVolumesSyncher(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/TableSynchronizer;
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 607
    new-instance v0, Lcom/google/android/apps/books/sync/CollectionVolumesServerSynchronizable;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-wide/16 v4, 0x7

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/apps/books/sync/CollectionVolumesServerSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;J)V

    .line 610
    .local v0, "synchronizable":Lcom/google/android/apps/books/sync/CollectionVolumesServerSynchronizable;
    new-instance v1, Lcom/google/android/apps/books/sync/TableSynchronizer;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/sync/TableSynchronizer;-><init>(Lcom/google/android/apps/books/sync/Synchronizable;)V

    return-object v1
.end method

.method private buildLocalVolumeData(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;ZJ)Lcom/google/android/apps/books/model/LocalVolumeData;
    .locals 16
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "computeDefaultTextZoom"    # Z
    .param p4, "timestamp"    # J

    .prologue
    .line 1077
    const-string v2, "last_mode"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v7

    .line 1078
    .local v7, "lastMode":Lcom/google/android/apps/books/model/VolumeManifest$Mode;
    const-string v2, "pinned"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 1079
    .local v4, "pinned":Z
    const-string v2, "has_offline_license"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    .line 1080
    .local v14, "hasOfflineLicense":Z
    const-string v2, "force_download"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 1081
    .local v5, "forceDownload":Z
    const-string v2, "text_zoom"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getFloat(Ljava/lang/String;)F

    move-result v8

    .line 1082
    .local v8, "textZoom":F
    const-string v2, "line_height"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getFloat(Ljava/lang/String;)F

    move-result v9

    .line 1083
    .local v9, "lineHeight":F
    const-string v2, "last_local_access"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 1084
    .local v12, "lastLocalAccess":J
    const-string v2, "license_action"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->fromDbValue(Ljava/lang/String;)Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    move-result-object v6

    .line 1086
    .local v6, "licenseAction":Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;
    const-string v2, "fit_width"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    .line 1088
    .local v15, "fitWidth":Z
    const/4 v2, 0x0

    cmpg-float v2, v8, v2

    if-gtz v2, :cond_0

    if-eqz p3, :cond_0

    .line 1092
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/books/provider/VolumeStatesUtils;->getMedianTextZoom(Landroid/accounts/Account;Landroid/content/ContentResolver;)F

    move-result v8

    .line 1093
    const/4 v2, 0x0

    cmpl-float v2, v8, v2

    if-lez v2, :cond_0

    .line 1096
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-static {v8, v2, v0, v3}, Lcom/google/android/apps/books/provider/VolumeStatesUtils;->saveTextZoomForVolume(FLandroid/accounts/Account;Ljava/lang/String;Landroid/content/ContentResolver;)V

    .line 1101
    :cond_0
    const/4 v2, 0x0

    cmpg-float v2, v9, v2

    if-gtz v2, :cond_1

    .line 1102
    const/high16 v9, 0x3fc00000    # 1.5f

    .line 1107
    :cond_1
    new-instance v3, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    move-wide/from16 v10, p4

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;-><init>(ZZLcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;Lcom/google/android/apps/books/model/VolumeManifest$Mode;FFJJZZ)V

    return-object v3
.end method

.method private buildStatesSyncher(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/TableSynchronizer;
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 614
    new-instance v0, Lcom/google/android/apps/books/sync/TableSynchronizer;

    new-instance v1, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/books/sync/StatesServerSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;-><init>(Lcom/google/android/apps/books/sync/Synchronizable;)V

    return-object v0
.end method

.method private static buildStopWords()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "a"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "an"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "the"

    aput-object v2, v0, v1

    .line 155
    .local v0, "stopwords":[Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private buildVolumeUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1969
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildVolumeUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static chapterInsertionOperation(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Chapter;Ljava/util/List;Ljava/util/List;ILandroid/net/Uri;)Landroid/content/ContentProviderOperation;
    .locals 8
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "chapter"    # Lcom/google/android/apps/books/model/Chapter;
    .param p5, "index"    # I
    .param p6, "chapterDirUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/Chapter;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Segment;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Page;",
            ">;I",
            "Landroid/net/Uri;",
            ")",
            "Landroid/content/ContentProviderOperation;"
        }
    .end annotation

    .prologue
    .line 1782
    .local p3, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    .local p4, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    iget-object v0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/provider/DataConversions;->chapterToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Chapter;Ljava/util/List;Ljava/util/List;I)Landroid/content/ContentValues;

    move-result-object v7

    .line 1784
    .local v7, "values":Landroid/content/ContentValues;
    invoke-static {p6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    .line 1786
    .local v6, "op":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method private clearInvalidatedContent(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)V
    .locals 5
    .param p1, "result"    # Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    .prologue
    .line 519
    iget-object v3, p1, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->modifiedValues:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 520
    .local v1, "modifiedVolumeIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 521
    .local v2, "volumeId":Ljava/lang/String;
    iget-object v3, p1, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->originals:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    iget-object v4, p1, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->modifiedValues:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->contentNowInvalid(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 523
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->deleteInvalidContent(Ljava/lang/String;)V

    goto :goto_0

    .line 526
    .end local v2    # "volumeId":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private static clearVolumeColumns(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/ContentProviderOperation;
    .locals 4
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1722
    invoke-static {p0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildVolumeUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1723
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getValuesToClearManifestContentColumns()Landroid/content/ContentValues;

    move-result-object v2

    .line 1724
    .local v2, "values":Landroid/content/ContentValues;
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1725
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 1726
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    return-object v3
.end method

.method private contentFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "internalFile"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;
    .param p3, "metadataUri"    # Landroid/net/Uri;
    .param p4, "contentStatusColumn"    # Ljava/lang/String;

    .prologue
    .line 2632
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    new-instance v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;

    invoke-direct {v1, p0, p4, p3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$6;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V

    return-object v0
.end method

.method public static contentNowInvalid(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)Z
    .locals 20
    .param p0, "volumeId"    # Ljava/lang/String;
    .param p1, "originals"    # Landroid/content/ContentValues;
    .param p2, "changes"    # Landroid/content/ContentValues;

    .prologue
    .line 531
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 532
    :cond_0
    const/4 v8, 0x0

    .line 577
    :cond_1
    :goto_0
    return v8

    .line 534
    :cond_2
    const-string v17, "viewability"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v12

    .line 535
    .local v12, "hasViewability":Z
    const-string v17, "buy_url"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v9

    .line 536
    .local v9, "hasBuyUrl":Z
    const-string v17, "open_access"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v10

    .line 537
    .local v10, "hasOpenAccess":Z
    const-string v17, "content_version"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v11

    .line 539
    .local v11, "hasVersion":Z
    const-string v17, "viewability"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 540
    .local v2, "oldViewability":Ljava/lang/String;
    const-string v17, "buy_url"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "oldBuyUrl":Ljava/lang/String;
    const-string v17, "open_access"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 542
    .local v4, "oldOpenAccess":Ljava/lang/String;
    const-string v17, "content_version"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 545
    .local v14, "oldVersion":Ljava/lang/String;
    if-eqz v12, :cond_7

    const-string v17, "viewability"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 547
    .local v5, "newViewability":Ljava/lang/String;
    :goto_1
    if-eqz v9, :cond_8

    const-string v17, "buy_url"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 548
    .local v6, "newBuyUrl":Ljava/lang/String;
    :goto_2
    if-eqz v10, :cond_9

    const-string v17, "open_access"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 550
    .local v7, "newOpenAccess":Ljava/lang/String;
    :goto_3
    const-string v17, "content_version"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 554
    .local v13, "newVersion":Ljava/lang/String;
    if-eqz v11, :cond_a

    if-eqz v13, :cond_3

    if-eqz v14, :cond_a

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_a

    :cond_3
    const/16 v16, 0x1

    .line 557
    .local v16, "versionChanged":Z
    :goto_4
    if-nez v16, :cond_4

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->isContentInvalid(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_b

    :cond_4
    const/4 v8, 0x1

    .line 562
    .local v8, "contentInvalid":Z
    :goto_5
    if-eqz v8, :cond_1

    const-string v17, "BooksDataStore"

    const/16 v18, 0x3

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 564
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    .local v15, "sb":Ljava/lang/StringBuilder;
    const-string v17, "viewability"

    move-object/from16 v0, v17

    invoke-static {v15, v0, v2, v5}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->appendIfChanged(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const-string v17, "buyUrl"

    move-object/from16 v0, v17

    invoke-static {v15, v0, v3, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->appendIfChanged(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v17, "open access"

    move-object/from16 v0, v17

    invoke-static {v15, v0, v4, v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->appendIfChanged(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    if-eqz v16, :cond_6

    .line 569
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->length()I

    move-result v17

    if-eqz v17, :cond_5

    .line 570
    const-string v17, ", "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 572
    :cond_5
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "version: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " -> "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 574
    :cond_6
    const-string v17, "BooksDataStore"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "update(): clearing content for volume "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " due to "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v5    # "newViewability":Ljava/lang/String;
    .end local v6    # "newBuyUrl":Ljava/lang/String;
    .end local v7    # "newOpenAccess":Ljava/lang/String;
    .end local v8    # "contentInvalid":Z
    .end local v13    # "newVersion":Ljava/lang/String;
    .end local v15    # "sb":Ljava/lang/StringBuilder;
    .end local v16    # "versionChanged":Z
    :cond_7
    move-object v5, v2

    .line 545
    goto/16 :goto_1

    .restart local v5    # "newViewability":Ljava/lang/String;
    :cond_8
    move-object v6, v3

    .line 547
    goto/16 :goto_2

    .restart local v6    # "newBuyUrl":Ljava/lang/String;
    :cond_9
    move-object v7, v4

    .line 548
    goto/16 :goto_3

    .line 554
    .restart local v7    # "newOpenAccess":Ljava/lang/String;
    .restart local v13    # "newVersion":Ljava/lang/String;
    :cond_a
    const/16 v16, 0x0

    goto/16 :goto_4

    .line 557
    .restart local v16    # "versionChanged":Z
    :cond_b
    const/4 v8, 0x0

    goto/16 :goto_5
.end method

.method private coverFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "internalFile"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    .prologue
    .line 2406
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    new-instance v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$5;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Ljava/lang/String;)V

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V

    return-object v0
.end method

.method private createCollectionVolumesDirUri(J)Landroid/net/Uri;
    .locals 1
    .param p1, "collectionId"    # J

    .prologue
    .line 2433
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->dirUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksContract;->markAsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private deleteAllVolumes()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2941
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildAccountVolumesDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    .line 2942
    .local v2, "volumesDirUri":Landroid/net/Uri;
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->VOLUME_ID_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v7

    .line 2946
    .local v7, "oldCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->getCount()I

    .line 2949
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 2950
    .local v6, "count":I
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->getCount()I

    move-result v0

    if-ne v0, v6, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Bad delete count"

    invoke-static {v0, v1}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 2956
    :goto_1
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2957
    const-string v0, "volume_id"

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->deleteContent(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2960
    .end local v6    # "count":I
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v0

    .line 2950
    .restart local v6    # "count":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2960
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 2962
    return-void
.end method

.method private deleteInvalidContent(Ljava/lang/String;)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 591
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 592
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 594
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildVolumeUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 595
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getValuesToClearManifestContentColumns()Landroid/content/ContentValues;

    move-result-object v1

    .line 596
    .local v1, "values":Landroid/content/ContentValues;
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 598
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->deleteContent(Ljava/lang/String;)V

    .line 599
    return-void
.end method

.method private static filterToLastAccessValues(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation

    .prologue
    .line 623
    .local p0, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 624
    .local v0, "filteredList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    .line 625
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "position"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 626
    .local v3, "position":Ljava/lang/String;
    if-nez v3, :cond_0

    const-string v5, "last_access"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 628
    :cond_0
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 631
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 632
    .local v2, "licenseOnlyValues":Landroid/content/ContentValues;
    const-string v5, "volume_id"

    const-string v6, "volume_id"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    const-string v5, "has_offline_license"

    const-string v6, "has_offline_license"

    invoke-virtual {v4, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 635
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 638
    .end local v2    # "licenseOnlyValues":Landroid/content/ContentValues;
    .end local v3    # "position":Ljava/lang/String;
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_2
    return-object v0
.end method

.method private getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private getDismissedOffersFile(Z)Ljava/io/File;
    .locals 3
    .param p1, "shouldMkdir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2223
    new-instance v0, Ljava/io/File;

    const-string v1, "offers_cache"

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getNamedCacheDir(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v1

    const-string v2, "dismissed_offers"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getDismissedRecommendationsFile(Z)Ljava/io/File;
    .locals 3
    .param p1, "shouldMkdir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2175
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getRecommendationsCacheDir(Z)Ljava/io/File;

    move-result-object v0

    .line 2176
    .local v0, "recsCacheDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "dismissed_recs"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private getHomeRecommendationsFile(Z)Ljava/io/File;
    .locals 3
    .param p1, "shouldMkdir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2118
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getRecommendationsCacheDir(Z)Ljava/io/File;

    move-result-object v0

    .line 2119
    .local v0, "recsCacheDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "home_recs.json"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method private getLocalContentState(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "metadataUri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 2737
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->CONTENT_KEY_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v6

    .line 2740
    .local v6, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2741
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    .line 2742
    invoke-direct {p0, p1, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->localContentStateFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2751
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :goto_0
    return-object v0

    .line 2748
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->UNKNOWN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/SessionKeyId;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2751
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v0
.end method

.method private getNamedCacheDir(Ljava/lang/String;Z)Ljava/io/File;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "shouldMkdir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2100
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getAccountDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2101
    .local v0, "cacheDir":Ljava/io/File;
    if-eqz p2, :cond_0

    .line 2102
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 2104
    :cond_0
    return-object v0
.end method

.method private getVolumeManifestNoLogging(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 39
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/model/VolumeManifest;"
        }
    .end annotation

    .prologue
    .line 1432
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "outLocalSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "outLocalResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p5, "outLocalPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p6, "outLocalStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest;->builder()Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    move-result-object v15

    .line 1435
    .local v15, "builder":Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v31

    .line 1436
    .local v31, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1437
    .local v6, "segmentDirUri":Landroid/net/Uri;
    sget-object v4, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SectionQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "segment_order ASC"

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v36

    .line 1439
    .local v36, "segmentsCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    const/16 v35, 0x0

    .line 1441
    .local v35, "segmentsCount":I
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v34

    .line 1442
    .local v34, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1443
    new-instance v30, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    invoke-direct/range {v30 .. v30}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;-><init>()V

    .line 1444
    .local v30, "segmentBuilder":Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    const/16 v32, 0x0

    .local v32, "segmentIndex":I
    move/from16 v33, v32

    .line 1446
    .end local v32    # "segmentIndex":I
    .local v33, "segmentIndex":I
    :goto_0
    move-object/from16 v0, v36

    move-object/from16 v1, v30

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToSegment(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;Ljava/util/Set;)V

    .line 1447
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->build()Lcom/google/android/apps/books/model/Segment;

    move-result-object v29

    .line 1448
    .local v29, "segment":Lcom/google/android/apps/books/model/Segment;
    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1449
    invoke-interface/range {v29 .. v29}, Lcom/google/android/apps/books/model/Segment;->getId()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v32, v33, 0x1

    .end local v33    # "segmentIndex":I
    .restart local v32    # "segmentIndex":I
    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->reset()V

    .line 1451
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_e

    .line 1453
    .end local v29    # "segment":Lcom/google/android/apps/books/model/Segment;
    .end local v30    # "segmentBuilder":Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .end local v32    # "segmentIndex":I
    :cond_0
    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->withEagerMap(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setSegments(Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1455
    invoke-interface/range {v34 .. v34}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v35

    .line 1457
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1461
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v23

    .line 1462
    .local v23, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 1463
    .local v8, "pageDirUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/google/android/apps/books/model/BooksDataStoreImpl$PageQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v12, "page_order ASC"

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v28

    .line 1465
    .local v28, "pagesCursor":Landroid/database/Cursor;
    const/16 v27, 0x0

    .line 1467
    .local v27, "pagesCount":I
    :try_start_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v26

    .line 1468
    .local v26, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1469
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutablePage;->builder()Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    move-result-object v22

    .line 1470
    .local v22, "pageBuilder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    const/16 v24, 0x0

    .local v24, "pageIndex":I
    move/from16 v25, v24

    .line 1472
    .end local v24    # "pageIndex":I
    .local v25, "pageIndex":I
    :goto_1
    move-object/from16 v0, v28

    move-object/from16 v1, v22

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToPage(Landroid/database/Cursor;Lcom/google/android/apps/books/model/ImmutablePage$Builder;Ljava/util/Set;Ljava/util/Set;)V

    .line 1473
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->build()Lcom/google/android/apps/books/model/Page;

    move-result-object v21

    .line 1474
    .local v21, "page":Lcom/google/android/apps/books/model/Page;
    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1475
    invoke-interface/range {v21 .. v21}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v24, v25, 0x1

    .end local v25    # "pageIndex":I
    .restart local v24    # "pageIndex":I
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1476
    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->reset()V

    .line 1477
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_d

    .line 1479
    .end local v21    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v22    # "pageBuilder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .end local v24    # "pageIndex":I
    :cond_1
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->withEagerMap(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setPages(Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1480
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v27

    .line 1482
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    .line 1491
    if-nez v35, :cond_2

    if-nez v27, :cond_2

    .line 1492
    const/4 v4, 0x0

    .line 1565
    :goto_2
    return-object v4

    .line 1457
    .end local v8    # "pageDirUri":Landroid/net/Uri;
    .end local v23    # "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v26    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    .end local v27    # "pagesCount":I
    .end local v28    # "pagesCursor":Landroid/database/Cursor;
    .end local v34    # "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    :catchall_0
    move-exception v4

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v4

    .line 1482
    .restart local v8    # "pageDirUri":Landroid/net/Uri;
    .restart local v23    # "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v27    # "pagesCount":I
    .restart local v28    # "pagesCursor":Landroid/database/Cursor;
    .restart local v34    # "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    :catchall_1
    move-exception v4

    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1496
    .restart local v26    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryVolumes(Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v38

    .line 1498
    .local v38, "volumeCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_2
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/apps/books/model/StringSafeCursor;->getCount()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_8

    const/4 v4, 0x1

    :goto_3
    const-string v5, "unexpected volumes cursor"

    invoke-static {v4, v5}, Lcom/google/common/base/Preconditions;->checkState(ZLjava/lang/Object;)V

    .line 1499
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1500
    const-string v4, "first_chapter_start_segment_id"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setFirstChapterStartSegmentIndex(I)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1503
    const-string v4, "preferred_mode"

    move-object/from16 v0, v38

    invoke-static {v0, v4}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setPreferredMode(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1505
    const-string v4, "has_text_mode"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    :goto_4
    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setHasTextMode(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1506
    const-string v4, "has_image_mode"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, 0x1

    :goto_5
    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setHasImageMode(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1508
    const-string v4, "is_right_to_left"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_b

    const/4 v4, 0x1

    :goto_6
    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setIsRightToLeft(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1509
    const-string v4, "has_media_overlays"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_7
    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setHasMediaOverlays(Z)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1510
    const-string v4, "media_overlay_active_class"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1512
    .local v20, "mediaOverlayActiveClass":Ljava/lang/String;
    if-nez v20, :cond_3

    .line 1513
    const-string v20, "-epub-media-overlay-active media-overlay-active"

    .line 1515
    :cond_3
    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/books/util/BooksTextUtils;->transformCssForWebView(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1517
    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setMediaOverlayActiveClass(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1519
    const-string v4, "content_version"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setContentVersion(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1521
    new-instance v19, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    const-string v4, "image_mode_first_book_body_page"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "image_mode_last_book_body_page"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1524
    .local v19, "imageModePositions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setImageModePositions(Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1526
    new-instance v37, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    const-string v4, "text_mode_first_book_body_page"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "text_mode_last_book_body_page"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v37

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1529
    .local v37, "textModePositions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    move-object/from16 v0, v37

    invoke-virtual {v15, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setTextModePositions(Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1530
    const-string v4, "orientation"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setRenditionOrientation(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1532
    const-string v4, "spread"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setRenditionSpread(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1535
    .end local v19    # "imageModePositions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    .end local v20    # "mediaOverlayActiveClass":Ljava/lang/String;
    .end local v37    # "textModePositions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    :cond_4
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1539
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->buildChapterDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1540
    .local v10, "chapterDirUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v11, Lcom/google/android/apps/books/model/BooksDataStoreImpl$ChapterQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-string v14, "chapter_order ASC"

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1543
    .local v18, "chaptersCursor":Landroid/database/Cursor;
    :try_start_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_5

    .line 1544
    const-string v4, "BooksDataStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "missing chapters for "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1547
    :cond_5
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v17

    .line 1548
    .local v17, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1550
    :cond_6
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableChapter;->builder()Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    move-result-object v16

    .line 1551
    .local v16, "chapterBuilder":Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    move-object/from16 v0, v18

    move-object/from16 v1, v23

    move-object/from16 v2, v31

    move-object/from16 v3, v16

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToChapter(Landroid/database/Cursor;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/books/model/ImmutableChapter$Builder;)V

    .line 1553
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->build()Lcom/google/android/apps/books/model/Chapter;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1554
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->reset()V

    .line 1555
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1557
    .end local v16    # "chapterBuilder":Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    :cond_7
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setChapters(Ljava/util/List;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1559
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1562
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getVolumeResources(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setResources(Ljava/util/Collection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1563
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getVolumeSegmentResources(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->setSegmentResources(Ljava/util/Collection;)Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;

    .line 1565
    invoke-virtual {v15}, Lcom/google/android/apps/books/model/ImmutableVolumeManifest$Builder;->build()Lcom/google/android/apps/books/model/VolumeManifest;

    move-result-object v4

    goto/16 :goto_2

    .line 1498
    .end local v10    # "chapterDirUri":Landroid/net/Uri;
    .end local v17    # "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Chapter;>;"
    .end local v18    # "chaptersCursor":Landroid/database/Cursor;
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 1505
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 1506
    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 1508
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 1509
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 1535
    :catchall_2
    move-exception v4

    invoke-virtual/range {v38 .. v38}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v4

    .line 1559
    .restart local v10    # "chapterDirUri":Landroid/net/Uri;
    .restart local v18    # "chaptersCursor":Landroid/database/Cursor;
    :catchall_3
    move-exception v4

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v4

    .end local v10    # "chapterDirUri":Landroid/net/Uri;
    .end local v18    # "chaptersCursor":Landroid/database/Cursor;
    .end local v38    # "volumeCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    .restart local v21    # "page":Lcom/google/android/apps/books/model/Page;
    .restart local v22    # "pageBuilder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .restart local v24    # "pageIndex":I
    :cond_d
    move/from16 v25, v24

    .end local v24    # "pageIndex":I
    .restart local v25    # "pageIndex":I
    goto/16 :goto_1

    .end local v8    # "pageDirUri":Landroid/net/Uri;
    .end local v21    # "page":Lcom/google/android/apps/books/model/Page;
    .end local v22    # "pageBuilder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .end local v23    # "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v25    # "pageIndex":I
    .end local v26    # "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    .end local v27    # "pagesCount":I
    .end local v28    # "pagesCursor":Landroid/database/Cursor;
    .restart local v29    # "segment":Lcom/google/android/apps/books/model/Segment;
    .restart local v30    # "segmentBuilder":Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .restart local v32    # "segmentIndex":I
    :cond_e
    move/from16 v33, v32

    .end local v32    # "segmentIndex":I
    .restart local v33    # "segmentIndex":I
    goto/16 :goto_0
.end method

.method private getVolumeSegmentResources(Ljava/lang/String;)Ljava/util/Collection;
    .locals 9
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/SegmentResource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1918
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 1920
    .local v8, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/SegmentResource;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$SegmentResources;->buildDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1922
    .local v2, "segmentResourceDirUri":Landroid/net/Uri;
    const-string v7, "segment_id,resource_order"

    .line 1923
    .local v7, "order":Ljava/lang/String;
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$SegmentResourceQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const-string v5, "segment_id,resource_order"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v6

    .line 1927
    .local v6, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1929
    :cond_0
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableSegmentResource;->builder()Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    move-result-object v0

    const-string v1, "segment_id"

    invoke-virtual {v6, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setSegmentId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    move-result-object v0

    const-string v1, "resource_id"

    invoke-virtual {v6, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setResourceId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    move-result-object v0

    const-string v1, "css_class"

    invoke-virtual {v6, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setCssClass(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v6, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutableSegmentResource$Builder;->build()Lcom/google/android/apps/books/model/SegmentResource;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1934
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1937
    :cond_1
    if-eqz v6, :cond_2

    .line 1938
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1942
    :cond_2
    return-object v8

    .line 1937
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 1938
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_3
    throw v0
.end method

.method private immediatelyBroadcastContentChange()V
    .locals 1

    .prologue
    .line 2313
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 2314
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v0}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->flushNotifications()V

    .line 2315
    return-void
.end method

.method private isLocalResource(Lcom/google/android/apps/books/model/StringSafeCursor;)Z
    .locals 2
    .param p1, "resourcesCursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 1914
    const-string v0, "content_status"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private localContentStateFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 2777
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->sessionKeyIdFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    .line 2778
    .local v2, "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    const-string v3, "content_status"

    invoke-virtual {p2, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2779
    .local v1, "contentStatusInt":I
    invoke-static {v1}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->fromInteger(I)Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    move-result-object v0

    .line 2780
    .local v0, "contentStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    new-instance v3, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$LocalContentStateImpl;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/SessionKeyId;)V

    return-object v3
.end method

.method private makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 270
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 271
    .local v0, "result":Landroid/content/ContentValues;
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    return-object v0
.end method

.method private makeVolumeOperation(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/content/ContentProviderOperation;
    .locals 10
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;

    .prologue
    const/4 v9, 0x1

    .line 1733
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildVolumeUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 1734
    .local v6, "uri":Landroid/net/Uri;
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1735
    .local v0, "builder":Landroid/content/ContentProviderOperation$Builder;
    const-string v7, "content_version"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getContentVersion()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1736
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->hasTextMode()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1737
    const-string v7, "has_text_mode"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1739
    :cond_0
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->hasImageMode()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1740
    const-string v7, "has_image_mode"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1742
    :cond_1
    const-string v7, "preferred_mode"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getPreferredMode()Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->toInteger(Lcom/google/android/apps/books/model/VolumeManifest$Mode;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1743
    const-string v7, "first_chapter_start_segment_id"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getFirstChapterStartSegmentIndex()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1745
    const-string v7, "is_right_to_left"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->isRightToLeft()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1746
    const-string v7, "has_media_overlays"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->hasMediaOverlays()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1747
    const-string v7, "media_overlay_active_class"

    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getMediaOverlayActiveClass()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1749
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 1750
    .local v2, "language":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 1751
    const-string v7, "language"

    invoke-virtual {v0, v7, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1753
    :cond_2
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getRenditionOrientation()Ljava/lang/String;

    move-result-object v3

    .line 1754
    .local v3, "orientation":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 1755
    const-string v7, "orientation"

    invoke-virtual {v0, v7, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1757
    :cond_3
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getRenditionSpread()Ljava/lang/String;

    move-result-object v4

    .line 1758
    .local v4, "spread":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 1759
    const-string v7, "spread"

    invoke-virtual {v0, v7, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1761
    :cond_4
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getImageModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v1

    .line 1762
    .local v1, "imageModePositions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    if-eqz v1, :cond_5

    .line 1763
    const-string v7, "image_mode_first_book_body_page"

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->getContentStart()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1765
    const-string v7, "image_mode_last_book_body_page"

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->getContentEnd()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1768
    :cond_5
    invoke-interface {p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getTextModePositions()Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;

    move-result-object v5

    .line 1769
    .local v5, "textModePositions":Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;
    if-eqz v5, :cond_6

    .line 1770
    const-string v7, "text_mode_first_book_body_page"

    invoke-virtual {v5}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->getContentStart()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1772
    const-string v7, "text_mode_last_book_body_page"

    invoke-virtual {v5}, Lcom/google/android/apps/books/model/VolumeManifest$ContentPositions;->getContentEnd()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1776
    :cond_6
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    return-object v7
.end method

.method private normalizeAuthorForSort(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "author"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 402
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 403
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 404
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 406
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 408
    :cond_0
    return-object p1
.end method

.method private normalizeTitleForSort(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 380
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 381
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 382
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->sStopWords:Ljava/util/HashSet;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 383
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 385
    :cond_0
    return-object p1
.end method

.method private pageContentFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;ZLcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p3, "forImage"    # Z
    .param p4, "internalFile"    # Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    .prologue
    .line 1275
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    new-instance v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;ZLjava/lang/String;Lcom/google/android/apps/books/model/Page;)V

    invoke-direct {v0, p0, p4, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V

    return-object v0
.end method

.method private static pageInsertionOperation(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Landroid/net/Uri;)Landroid/content/ContentProviderOperation;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p3, "pageDirUri"    # Landroid/net/Uri;

    .prologue
    .line 1800
    iget-object v2, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, p1, p2}, Lcom/google/android/apps/books/provider/DataConversions;->pageToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Landroid/content/ContentValues;

    move-result-object v1

    .line 1802
    .local v1, "values":Landroid/content/ContentValues;
    invoke-static {p3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1803
    .local v0, "op":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    return-object v2
.end method

.method private pageUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 1258
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private persistVolumeSearches(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2359
    .local p2, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2360
    const-string v2, "BooksDataStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Persisting this list "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2362
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getVolumeSearchesFile(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    .line 2363
    .local v0, "file":Ljava/io/File;
    new-instance v2, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v2}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v4, "UTF-8"

    invoke-static {v4}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonGenerator(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)Lcom/google/api/client/json/JsonGenerator;

    move-result-object v1

    .line 2365
    .local v1, "jsonGenerator":Lcom/google/api/client/json/JsonGenerator;
    invoke-virtual {v1, p2}, Lcom/google/api/client/json/JsonGenerator;->serialize(Ljava/lang/Object;)V

    .line 2366
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonGenerator;->close()V

    .line 2367
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v2}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 2368
    return-void
.end method

.method private prepareServerValuesForSyncher(JLjava/util/List;)V
    .locals 19
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317
    .local p3, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    invoke-interface {v14}, Lcom/google/android/apps/books/model/Clock;->currentTime()J

    move-result-wide v8

    .line 318
    .local v8, "millis":J
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/ContentValues;

    .line 320
    .local v13, "values":Landroid/content/ContentValues;
    const-string v14, "buy_url"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 322
    const-string v14, "buy_url"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 325
    :cond_0
    const-string v14, "title"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 326
    .local v12, "title":Ljava/lang/String;
    if-eqz v12, :cond_1

    .line 327
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->normalizeTitleForSort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 328
    .local v11, "sortableTitle":Ljava/lang/String;
    const-string v14, "sortable_title"

    invoke-virtual {v13, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    .end local v11    # "sortableTitle":Ljava/lang/String;
    :cond_1
    const-string v14, "creator"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 333
    .local v2, "author":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 334
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->normalizeAuthorForSort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 335
    .local v10, "sortableAuthor":Ljava/lang/String;
    const-string v14, "sortable_creator"

    invoke-virtual {v13, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    .end local v10    # "sortableAuthor":Ljava/lang/String;
    :cond_2
    const-string v14, "viewability"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    .line 342
    .local v6, "hasViewability":Z
    const-string v14, "open_access"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    .line 343
    .local v5, "hasOpenAccess":Z
    const-string v14, "buy_url"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    .line 344
    .local v4, "hasBuyUrl":Z
    if-ne v6, v5, :cond_3

    if-eq v6, v4, :cond_4

    .line 345
    :cond_3
    const-string v14, "BooksDataStore"

    const-string v15, ""

    new-instance v16, Ljava/lang/IllegalArgumentException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Incomplete access information in : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static/range {v14 .. v16}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 349
    :cond_4
    const-string v14, "cover_url"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 350
    .local v3, "coverUrl":Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 351
    const-string v14, "cover_url"

    invoke-virtual {v13, v14, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :cond_5
    const-string v14, "account_name"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v15, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v14, "collection_id"

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 358
    const-string v14, "timestamp"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 359
    const-string v14, "dirty"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 360
    const-string v14, "last_action"

    invoke-virtual {v13, v14}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 362
    .end local v2    # "author":Ljava/lang/String;
    .end local v3    # "coverUrl":Ljava/lang/String;
    .end local v4    # "hasBuyUrl":Z
    .end local v5    # "hasOpenAccess":Z
    .end local v6    # "hasViewability":Z
    .end local v12    # "title":Ljava/lang/String;
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_6
    return-void
.end method

.method private queryMyEbooks(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 7
    .param p1, "query"    # Lcom/google/android/apps/books/model/StringSafeQuery;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 877
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->myEBooksDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    .line 878
    .local v2, "volumeDirUri":Landroid/net/Uri;
    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v5, "last_interaction DESC"

    move-object v0, p1

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v6

    .line 882
    .local v6, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    if-nez v6, :cond_0

    .line 883
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error loading volumes: null cursor"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_0
    return-object v6
.end method

.method private queryPage(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "query"    # Lcom/google/android/apps/books/model/StringSafeQuery;

    .prologue
    const/4 v3, 0x0

    .line 1254
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->pageUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v0, p3

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method

.method private queryResourceResources(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resId"    # Ljava/lang/String;
    .param p3, "query"    # Lcom/google/android/apps/books/model/StringSafeQuery;

    .prologue
    const/4 v3, 0x0

    .line 1379
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;->buildUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v0, p3

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method

.method private queryResources(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;
    .locals 10
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1889
    .local p4, "localResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 1890
    .local v8, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1891
    .local v2, "resourceDirUri":Landroid/net/Uri;
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$ResourceQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const-string v5, "resource_order"

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v9

    .line 1894
    .local v9, "resourcesCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1895
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableResource;->builder()Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    move-result-object v7

    .line 1897
    .local v7, "resourceBuilder":Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    :cond_0
    invoke-static {v9, v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V

    .line 1898
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->build()Lcom/google/android/apps/books/model/Resource;

    move-result-object v6

    .line 1899
    .local v6, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1900
    if-eqz p4, :cond_1

    invoke-direct {p0, v9}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->isLocalResource(Lcom/google/android/apps/books/model/StringSafeCursor;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1901
    invoke-interface {v6}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1903
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->reset()V

    .line 1904
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1907
    .end local v6    # "resource":Lcom/google/android/apps/books/model/Resource;
    .end local v7    # "resourceBuilder":Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    :cond_2
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1910
    return-object v8

    .line 1907
    :catchall_0
    move-exception v0

    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v0
.end method

.method private queryVolumes(Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1569
    sget-object v0, Lcom/google/android/apps/books/model/VolumesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryVolumes(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method

.method private queryVolumes(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "query"    # Lcom/google/android/apps/books/model/StringSafeQuery;

    .prologue
    const/4 v3, 0x0

    .line 1965
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildVolumeUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v0, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    return-object v0
.end method

.method private resourcesFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Ljava/util/List;
    .locals 3
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/StringSafeCursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1367
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1368
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableResource;->builder()Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    move-result-object v0

    .line 1369
    .local v0, "builder":Lcom/google/android/apps/books/model/ImmutableResource$Builder;
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1370
    invoke-static {p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToReferencedResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V

    .line 1371
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->build()Lcom/google/android/apps/books/model/Resource;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1372
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->reset()V

    goto :goto_0

    .line 1374
    :cond_0
    return-object v1
.end method

.method private static rowToChapter(Landroid/database/Cursor;Ljava/util/Map;Ljava/util/Map;Lcom/google/android/apps/books/model/ImmutableChapter$Builder;)V
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p3, "builder"    # Lcom/google/android/apps/books/model/ImmutableChapter$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/books/model/ImmutableChapter$Builder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1577
    .local p1, "pageIdToPageIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p2, "segmentIdToSegmentIndex":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    .line 1578
    const/4 v1, 0x1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    .line 1579
    const/4 v1, 0x2

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1581
    .local v0, "segmentIndex":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 1582
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p3, v1}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setStartSegmentIndex(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    .line 1584
    :cond_0
    const/4 v1, 0x3

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p3, v1}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setStartPageIndex(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    .line 1586
    const/4 v1, 0x4

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {p3, v1}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setDepth(I)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    .line 1587
    const/4 v1, 0x5

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/google/android/apps/books/model/ImmutableChapter$Builder;->setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableChapter$Builder;

    .line 1588
    return-void
.end method

.method private static rowToGenericResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V
    .locals 1
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .prologue
    .line 1648
    const-string v0, "remote_url"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1649
    const-string v0, "resource_type"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setMimeType(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1650
    const-string v0, "language"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1651
    const-string v0, "md5_hash"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setMd5Hash(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1652
    const-string v0, "is_shared"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setShared(Z)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1653
    const-string v0, "is_default"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setIsDefault(Z)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1654
    const-string v0, "overlay"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setOverlay(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1655
    return-void
.end method

.method private static rowToPage(Landroid/database/Cursor;Lcom/google/android/apps/books/model/ImmutablePage$Builder;Ljava/util/Set;Ljava/util/Set;)V
    .locals 7
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/google/android/apps/books/model/ImmutablePage$Builder;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "outLocalPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "outLocalStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1619
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1620
    .local v1, "pageId":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->pageId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1621
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1622
    const/4 v5, 0x2

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1623
    .local v0, "contentStatus":I
    if-eq v0, v4, :cond_0

    move v3, v4

    :cond_0
    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->isViewable(Z)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1624
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->pageOrder(I)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1625
    const/4 v3, 0x4

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->remoteUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1626
    if-eqz p2, :cond_1

    if-ne v0, v6, :cond_1

    .line 1627
    invoke-interface {p2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1629
    :cond_1
    const/4 v3, 0x5

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1630
    .local v2, "structureStatus":I
    if-eqz p3, :cond_2

    if-ne v2, v4, :cond_2

    .line 1631
    invoke-interface {p3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1633
    :cond_2
    return-void
.end method

.method private static rowToReferencedResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V
    .locals 1
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .prologue
    .line 1642
    const-string v0, "referenced_res_id"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1643
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToGenericResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V

    .line 1644
    return-void
.end method

.method private static rowToResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V
    .locals 1
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .prologue
    .line 1636
    const-string v0, "resource_id"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/books/model/ImmutableResource$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableResource$Builder;

    .line 1637
    invoke-static {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToGenericResource(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableResource$Builder;)V

    .line 1638
    return-void
.end method

.method private static rowToSegment(Lcom/google/android/apps/books/model/StringSafeCursor;Lcom/google/android/apps/books/model/ImmutableSegment$Builder;Ljava/util/Set;)V
    .locals 4
    .param p0, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p1, "builder"    # Lcom/google/android/apps/books/model/ImmutableSegment$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/StringSafeCursor;",
            "Lcom/google/android/apps/books/model/ImmutableSegment$Builder;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "outLocalSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 1597
    const-string v3, "segment_id"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1598
    .local v1, "segmentId":Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1599
    const-string v3, "title"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1600
    const-string v3, "start_position"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setStartPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1601
    const-string v3, "page_count"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setPageCount(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1602
    const-string v3, "content_status"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1603
    .local v0, "contentStatus":I
    if-eq v0, v2, :cond_1

    :goto_0
    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setViewable(Z)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1604
    const-string v2, "fixed_layout_version"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutVersion(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1605
    const-string v2, "fixed_viewport_width"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutViewportWidth(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1606
    const-string v2, "fixed_viewport_height"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setFixedLayoutViewportHeight(I)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1607
    if-eqz p2, :cond_0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 1608
    invoke-interface {p2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1610
    :cond_0
    const-string v2, "remote_url"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/books/model/ImmutableSegment$Builder;->setUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableSegment$Builder;

    .line 1611
    return-void

    .line 1603
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private rowToVolumeData(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;JLandroid/accounts/Account;Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Lcom/google/android/apps/books/model/VolumeData;
    .locals 15
    .param p1, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;
    .param p2, "readingPos"    # Ljava/lang/String;
    .param p3, "lastAccess"    # J
    .param p5, "account"    # Landroid/accounts/Account;
    .param p6, "builder"    # Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .prologue
    .line 748
    if-nez p1, :cond_0

    .line 749
    new-instance v11, Ljava/lang/NullPointerException;

    invoke-direct {v11}, Ljava/lang/NullPointerException;-><init>()V

    throw v11

    .line 751
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/model/StringSafeCursor;->isClosed()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 752
    new-instance v11, Ljava/lang/IllegalArgumentException;

    invoke-direct {v11}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v11

    .line 754
    :cond_1
    const-string v11, "volume_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 755
    .local v10, "volumeId":Ljava/lang/String;
    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 756
    const-string v11, "title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setTitle(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 757
    const-string v11, "creator"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setAuthor(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 758
    const-string v11, "page_count"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setPageCount(I)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 767
    const-string v11, "viewability"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 768
    .local v9, "viewability":Ljava/lang/String;
    move-object/from16 v0, p6

    invoke-virtual {v0, v9}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setViewability(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 769
    const-string v11, "buy_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 770
    .local v5, "buyUrl":Ljava/lang/String;
    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setBuyUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 771
    const-string v11, "open_access"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 772
    .local v7, "openAccess":Ljava/lang/String;
    invoke-static {v9, v7, v5}, Lcom/google/android/apps/books/model/VolumeData$Access;->getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v4

    .line 773
    .local v4, "access":Lcom/google/android/apps/books/model/VolumeData$Access;
    sget-object v11, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v11, v4}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setIsLimitedPreview(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 774
    const-string v11, "http://schemas.google.com/books/2008#enabled"

    invoke-static {v7, v11}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setIsPublicDomain(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 776
    const-string v11, "is_uploaded"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setIsUploaded(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 778
    const-string v11, "canonical_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setCanonicalUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 779
    const-string v11, "language"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setLanguage(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 781
    move-object/from16 v0, p6

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setReadingPosition(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 782
    move-object/from16 v0, p6

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setLastAccess(J)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 783
    const-string v11, "version"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setEtag(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 784
    const-string v11, "publisher"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setPublisher(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 785
    const-string v11, "date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setDate(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 786
    const-string v11, "description"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setDescription(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 787
    const-string v11, "cover_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setServerCoverUri(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 788
    move-object/from16 v0, p5

    invoke-static {v0, v10}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildCoverUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setLocalCoverUri(Landroid/net/Uri;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 789
    const-string v11, "tts_permission"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/apps/books/model/VolumeDataUtils;->stringToTtsPermission(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$TtsPermission;

    move-result-object v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setTtsPermission(Lcom/google/android/apps/books/model/VolumeData$TtsPermission;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 792
    const-string v11, "rental_state"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->isNull(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 793
    const/4 v8, 0x0

    .line 802
    .local v8, "rentalState":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p6

    invoke-virtual {v0, v8}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setRentalState(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 803
    const-string v11, "rental_start"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->isNull(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    const-wide/16 v12, 0x0

    :goto_1
    move-object/from16 v0, p6

    invoke-virtual {v0, v12, v13}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setRentalStart(J)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 805
    const-string v11, "rental_expiration"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->isNull(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    const-wide v12, 0x7fffffffffffffffL

    :goto_2
    move-object/from16 v0, p6

    invoke-virtual {v0, v12, v13}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setRentalExpiration(J)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 808
    const-string v11, "explicit_offline_license"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setUsesExplicitOfflineLicenseManagement(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 811
    const-string v11, "max_offline_devices"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->isNull(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    const v11, 0x7fffffff

    :goto_3
    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setMaxDownloadDevices(I)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 815
    const-string v11, "quote_sharing_allowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->setIsQuoteSharingAllowed(Z)Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    .line 817
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->build()Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v11

    return-object v11

    .line 795
    .end local v8    # "rentalState":Ljava/lang/String;
    :cond_2
    const-string v11, "rental_state"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 796
    .local v6, "databaseValue":Ljava/lang/String;
    const-string v11, "ACTIVE"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 797
    const-string v8, "ACTIVE"

    .restart local v8    # "rentalState":Ljava/lang/String;
    goto :goto_0

    .line 799
    .end local v8    # "rentalState":Ljava/lang/String;
    :cond_3
    const-string v8, "EXPIRED"

    .restart local v8    # "rentalState":Ljava/lang/String;
    goto :goto_0

    .line 803
    .end local v6    # "databaseValue":Ljava/lang/String;
    :cond_4
    const-string v11, "rental_start"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    goto :goto_1

    .line 805
    :cond_5
    const-string v11, "rental_expiration"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    goto :goto_2

    .line 811
    :cond_6
    const-string v11, "max_offline_devices"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v11

    goto :goto_3
.end method

.method private static segmentInsertionOperation(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;ILandroid/net/Uri;)Landroid/content/ContentProviderOperation;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segment"    # Lcom/google/android/apps/books/model/Segment;
    .param p3, "index"    # I
    .param p4, "segmentDirUri"    # Landroid/net/Uri;

    .prologue
    .line 1791
    iget-object v2, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, p1, p2, p3}, Lcom/google/android/apps/books/provider/DataConversions;->segmentToContentValues(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;I)Landroid/content/ContentValues;

    move-result-object v1

    .line 1793
    .local v1, "values":Landroid/content/ContentValues;
    invoke-static {p4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1795
    .local v0, "op":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    return-object v2
.end method

.method private sessionKeyIdFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/SessionKeyId;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "cursor"    # Lcom/google/android/apps/books/model/StringSafeCursor;

    .prologue
    .line 2788
    const-string v2, "session_key_id"

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLongObject(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 2789
    .local v1, "sessionKeyRowId":Ljava/lang/Long;
    const-string v2, "storage_format"

    invoke-virtual {p2, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;->fromDatabaseValues(Ljava/lang/Integer;Ljava/lang/Long;)Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;

    move-result-object v0

    .line 2791
    .local v0, "format":Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/books/model/SessionKeyIds;->keyIdFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$StorageFormat;Ljava/lang/Long;)Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    return-object v2
.end method

.method private syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z
    .locals 3
    .param p1, "result"    # Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    .prologue
    .line 412
    iget-object v2, p1, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->modifiedValues:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    .line 413
    .local v1, "modifiedValues":Landroid/content/ContentValues;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 414
    const/4 v2, 0x1

    .line 417
    .end local v1    # "modifiedValues":Landroid/content/ContentValues;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private syncVolumeRows(Ljava/util/List;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;)",
            "Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;"
        }
    .end annotation

    .prologue
    .line 507
    .local p1, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    const-wide/16 v2, 0x7

    invoke-direct {p0, v2, v3, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->prepareServerValuesForSyncher(JLjava/util/List;)V

    .line 511
    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->volumeTableSynchronizer()Lcom/google/android/apps/books/sync/TableSynchronizer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRows(Ljava/lang/Iterable;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    move-result-object v0

    .line 514
    .local v0, "result":Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->clearInvalidatedContent(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)V

    .line 515
    return-object v0
.end method

.method private updateCollectionRow(JLjava/lang/String;I)V
    .locals 7
    .param p1, "collectionId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;
    .param p4, "dirty"    # I

    .prologue
    .line 2482
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2483
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "account_name"

    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2484
    const-string v3, "volume_id"

    invoke-virtual {v2, v3, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2485
    const-string v3, "collection_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2486
    const-string v3, "timestamp"

    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    invoke-interface {v4}, Lcom/google/android/apps/books/model/Clock;->currentTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2487
    const-string v3, "dirty"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2489
    new-instance v0, Lcom/google/android/apps/books/sync/CollectionVolumesLocalSynchronizable;

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v0, v3, v4, p1, p2}, Lcom/google/android/apps/books/sync/CollectionVolumesLocalSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;J)V

    .line 2492
    .local v0, "syncable":Lcom/google/android/apps/books/sync/CollectionVolumesLocalSynchronizable;
    new-instance v1, Lcom/google/android/apps/books/sync/TableSynchronizer;

    invoke-direct {v1, v0}, Lcom/google/android/apps/books/sync/TableSynchronizer;-><init>(Lcom/google/android/apps/books/sync/Synchronizable;)V

    .line 2494
    .local v1, "syncher":Lcom/google/android/apps/books/sync/TableSynchronizer;
    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRow(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    .line 2495
    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v3}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 2496
    return-void
.end method

.method private updateLocalPageState(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/google/android/apps/books/model/SessionKeyId;)V
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;
    .param p3, "isImageDownloaded"    # Ljava/lang/Boolean;
    .param p4, "isStructureDownloaded"    # Ljava/lang/Boolean;
    .param p5, "sessionKeyId"    # Lcom/google/android/apps/books/model/SessionKeyId;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1178
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getLocalPageState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;

    move-result-object v2

    .line 1179
    .local v2, "state":Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v0

    .line 1180
    .local v0, "existingSessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->imageIsLocal()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v2}, Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;->structureIsDownloaded()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v1, v5

    .line 1184
    .local v1, "somethingStored":Z
    :goto_0
    if-eqz v1, :cond_2

    invoke-static {v0, p5}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1185
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Trying to store image and structure with different keys"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v1    # "somethingStored":Z
    :cond_1
    move v1, v6

    .line 1180
    goto :goto_0

    .line 1189
    .restart local v1    # "somethingStored":Z
    :cond_2
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->isViewable()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1190
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1191
    .local v3, "values":Landroid/content/ContentValues;
    if-eqz p3, :cond_3

    .line 1192
    const-string v7, "content_status"

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x3

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1195
    :cond_3
    if-eqz p4, :cond_4

    .line 1196
    const-string v7, "structure_status"

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v5

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1201
    :cond_4
    invoke-static {p5, v3}, Lcom/google/android/apps/books/model/SessionKeyIds;->addToContentValues(Lcom/google/android/apps/books/model/SessionKeyId;Landroid/content/ContentValues;)V

    .line 1203
    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v4, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updatePageRow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    .line 1205
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_5
    return-void

    .line 1192
    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_6
    const/4 v4, 0x2

    goto :goto_1

    :cond_7
    move v4, v6

    .line 1196
    goto :goto_2
.end method

.method private updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "syncContent"    # Z
    .param p4, "notifyContentChanged"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 278
    new-instance v0, Lcom/google/android/apps/books/sync/TableSynchronizer;

    new-instance v1, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/books/sync/StatesLocalSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;-><init>(Lcom/google/android/apps/books/sync/Synchronizable;)V

    .line 280
    .local v0, "syncher":Lcom/google/android/apps/books/sync/TableSynchronizer;
    invoke-virtual {v0, p2}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRow(Landroid/content/ContentValues;)Landroid/content/ContentValues;

    .line 282
    if-eqz p3, :cond_0

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mSyncController:Lcom/google/android/apps/books/sync/SyncController;

    new-array v2, v5, [Ljava/lang/String;

    aput-object p1, v2, v4

    invoke-interface {v1, v5, v2}, Lcom/google/android/apps/books/sync/SyncController;->requestManualVolumeContentSync(Z[Ljava/lang/String;)V

    .line 286
    :cond_0
    if-eqz p4, :cond_1

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v1}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 289
    :cond_1
    return-void
.end method

.method private updatePageRow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 1208
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->pageUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p3, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private volumeTableSynchronizer()Lcom/google/android/apps/books/sync/TableSynchronizer;
    .locals 6

    .prologue
    .line 602
    new-instance v0, Lcom/google/android/apps/books/sync/TableSynchronizer;

    new-instance v1, Lcom/google/android/apps/books/sync/VolumesSynchronizable;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mVolumeUsageManager:Lcom/google/android/apps/books/common/VolumeUsageManager;

    iget-object v5, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/sync/VolumesSynchronizable;-><init>(Landroid/content/ContentResolver;Landroid/accounts/Account;Lcom/google/android/apps/books/common/VolumeUsageManager;Lcom/google/android/apps/books/provider/VolumeContentStore;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;-><init>(Lcom/google/android/apps/books/sync/Synchronizable;)V

    return-object v0
.end method


# virtual methods
.method public addToCollection(JLjava/lang/String;)V
    .locals 1
    .param p1, "collectionId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2421
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateCollectionRow(JLjava/lang/String;I)V

    .line 2422
    return-void
.end method

.method public cleanTempDirectory()V
    .locals 2

    .prologue
    .line 2833
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getTempDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->recursiveDeleteContents(Ljava/io/File;)Z

    .line 2834
    return-void
.end method

.method public clearLastDictionaryMetadataSyncTime()V
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->clearLastDictionaryMetadataSyncTime()V

    .line 962
    return-void
.end method

.method public deleteAllContent()V
    .locals 22

    .prologue
    .line 2906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 2908
    .local v4, "accountName":Ljava/lang/String;
    sget-object v10, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->CONTENT_URI:Landroid/net/Uri;

    .line 2909
    .local v10, "collectionVolumesUri":Landroid/net/Uri;
    const-string v8, "account_name=?"

    .line 2910
    .local v8, "collectionVolumesSelection":Ljava/lang/String;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v9, v0, [Ljava/lang/String;

    const/16 v20, 0x0

    aput-object v4, v9, v20

    .line 2911
    .local v9, "collectionVolumesSelectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v20, v0

    const-string v21, "account_name=?"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v10, v1, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2914
    sget-object v13, Lcom/google/android/apps/books/provider/BooksContract$Collections;->CONTENT_URI:Landroid/net/Uri;

    .line 2915
    .local v13, "collectionsUri":Landroid/net/Uri;
    const-string v11, "account_name=?"

    .line 2916
    .local v11, "collectionsSelection":Ljava/lang/String;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v12, v0, [Ljava/lang/String;

    const/16 v20, 0x0

    aput-object v4, v12, v20

    .line 2917
    .local v12, "collectionsSelectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v20, v0

    const-string v21, "account_name=?"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v13, v1, v12}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2919
    sget-object v19, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates;->CONTENT_URI:Landroid/net/Uri;

    .line 2920
    .local v19, "statesUri":Landroid/net/Uri;
    const-string v17, "account_name=?"

    .line 2921
    .local v17, "statesSelection":Ljava/lang/String;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v20, 0x0

    aput-object v4, v18, v20

    .line 2922
    .local v18, "statesSelectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v20, v0

    const-string v21, "account_name=?"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2924
    sget-object v16, Lcom/google/android/apps/books/provider/BooksContract$SessionKeys;->CONTENT_URI:Landroid/net/Uri;

    .line 2925
    .local v16, "keysUri":Landroid/net/Uri;
    const-string v14, "account_name=?"

    .line 2926
    .local v14, "keysSelection":Ljava/lang/String;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v15, v0, [Ljava/lang/String;

    const/16 v20, 0x0

    aput-object v4, v15, v20

    .line 2927
    .local v15, "keysSelectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v20, v0

    const-string v21, "account_name=?"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2, v15}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2929
    sget-object v7, Lcom/google/android/apps/books/provider/BooksContract$Accounts;->CONTENT_URI:Landroid/net/Uri;

    .line 2930
    .local v7, "accountsUri":Landroid/net/Uri;
    const-string v5, "account_name=?"

    .line 2931
    .local v5, "accountsSelection":Ljava/lang/String;
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v6, v0, [Ljava/lang/String;

    const/16 v20, 0x0

    aput-object v4, v6, v20

    .line 2932
    .local v6, "accountsSelectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v20, v0

    const-string v21, "account_name=?"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v7, v1, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Lcom/google/android/apps/books/model/DataControllerStore;->deleteDictionaryMetadataForAccount(Landroid/accounts/Account;)V

    .line 2935
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/google/android/apps/books/model/DataControllerStore;->clearLastDictionaryMetadataSyncTime()V

    .line 2937
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->deleteAllVolumes()V

    .line 2938
    return-void
.end method

.method public deleteContent(Ljava/lang/String;)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 250
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->clearAllContentFiles(Ljava/lang/String;Ljava/lang/String;)Z

    .line 253
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getManifestContentUris(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 254
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 256
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public deleteDictionaryMetadataForAccount(Landroid/accounts/Account;)V
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->deleteDictionaryMetadataForAccount(Landroid/accounts/Account;)V

    .line 957
    return-void
.end method

.method public ensureMyEbooksCollection()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 1019
    const-string v3, "BooksDataStore"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1020
    const-string v3, "BooksDataStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "started ensureMyEbooks() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    :cond_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1024
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "account_name"

    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    const-string v3, "collection_id"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1026
    const-string v3, "title"

    const-string v4, "My eBooks"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    const-string v3, "summary"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    new-instance v1, Lcom/google/android/apps/books/sync/CollectionsSynchronizable;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/books/sync/CollectionsSynchronizable;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 1031
    .local v1, "synchronizable":Lcom/google/android/apps/books/sync/CollectionsSynchronizable;
    new-instance v0, Lcom/google/android/apps/books/sync/TableSynchronizer;

    invoke-direct {v0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;-><init>(Lcom/google/android/apps/books/sync/Synchronizable;)V

    .line 1032
    .local v0, "syncher":Lcom/google/android/apps/books/sync/TableSynchronizer;
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRows(Ljava/lang/Iterable;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    .line 1034
    const-string v3, "BooksDataStore"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1035
    const-string v3, "BooksDataStore"

    const-string v4, "finished ensureMyEbooks()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    :cond_1
    return-void
.end method

.method public getAccountDirectory()Ljava/io/File;
    .locals 2

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksProvider;->getFileStorageDirectory(Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/books/util/StorageUtils;->buildAccountDir(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getAccountSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2500
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getAccountSessionKey(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    return-object v0
.end method

.method public getAccountSessionKey(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 9
    .param p1, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 2505
    const-string v7, "account_name=?"

    .line 2506
    .local v7, "selection":Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 2510
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/apps/books/provider/BooksContract$Accounts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/books/model/AccountSessionKeyId$Query;->PROJECTION:[Ljava/lang/String;

    const-string v3, "account_name=?"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2513
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v8, :cond_0

    const-string v0, "BooksDataStore"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2514
    const-string v0, "BooksDataStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">1 session key rows for account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2516
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2517
    invoke-static {v6}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/LocalSessionKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 2520
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2522
    :goto_0
    return-object v5

    .line 2520
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getCachedOffers()Lcom/google/android/apps/books/model/BooksDataStore$CachedOffers;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2228
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getOffersFile(Z)Ljava/io/File;

    move-result-object v2

    .line 2229
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2231
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 2232
    .local v4, "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2233
    .local v3, "is":Ljava/io/InputStream;
    new-instance v5, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v5}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    invoke-virtual {v5, v3}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonParser(Ljava/io/InputStream;)Lcom/google/api/client/json/JsonParser;

    move-result-object v5

    const-class v7, Lcom/google/android/apps/books/api/data/ApiaryOffers;

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Lcom/google/api/client/json/JsonParser;->parseAndClose(Ljava/lang/Class;Lcom/google/api/client/json/CustomizeJsonParser;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/ApiaryOffers;

    .line 2236
    .local v0, "apiaryOffers":Lcom/google/android/apps/books/api/data/ApiaryOffers;
    const-string v5, "BooksDataStore"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2237
    const-string v5, "BooksDataStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Restored "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " offers."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2240
    :cond_0
    new-instance v5, Lcom/google/android/apps/books/model/CachedOffersImpl;

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    invoke-direct {v5, v0, v8, v9}, Lcom/google/android/apps/books/model/CachedOffersImpl;-><init>(Lcom/google/android/apps/books/api/data/ApiaryOffers;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2257
    .end local v0    # "apiaryOffers":Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "offers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/OfferData;>;"
    :goto_0
    return-object v5

    .line 2241
    :catch_0
    move-exception v1

    .line 2243
    .local v1, "e":Ljava/io/IOException;
    throw v1

    .line 2244
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 2248
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "BooksDataStore"

    const/4 v7, 0x6

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2249
    const-string v5, "BooksDataStore"

    const-string v7, "Error parsing cached offers"

    invoke-static {v5, v7, v1}, Lcom/google/android/apps/books/util/LogUtil;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2252
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    move-object v5, v6

    .line 2257
    goto :goto_0
.end method

.method public getCachedRecommendations()Lcom/google/android/apps/books/model/BooksDataStore$CachedRecommendations;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2128
    new-instance v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/model/BooksDataStoreImpl$1;)V

    .line 2129
    .local v1, "cachedRecs":Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;
    sget-object v5, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mHomeRecommendationFileLock:Ljava/lang/Object;

    monitor-enter v5

    .line 2130
    const/4 v6, 0x0

    :try_start_0
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getHomeRecommendationsFile(Z)Ljava/io/File;

    move-result-object v3

    .line 2131
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2132
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    iput-wide v6, v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;->lastModifiedMillis:J

    .line 2133
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;->getRecommendedBooks()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2135
    .local v0, "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    :try_start_1
    invoke-static {v0, v3}, Lcom/google/android/apps/books/app/data/JsonRecommendedBook;->parseRecommendationsJson(Ljava/util/List;Ljava/io/File;)V
    :try_end_1
    .catch Landroid/util/MalformedJsonException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2148
    :try_start_2
    const-string v4, "BooksDataStore"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2149
    const-string v4, "BooksDataStore"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Restored "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " read now recommendations."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2151
    :cond_0
    monitor-exit v5

    .line 2155
    .end local v0    # "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    .end local v1    # "cachedRecs":Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;
    :goto_0
    return-object v1

    .line 2136
    .restart local v0    # "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    .restart local v1    # "cachedRecs":Lcom/google/android/apps/books/model/BooksDataStoreImpl$CachedRecommendationsImpl;
    :catch_0
    move-exception v2

    .line 2137
    .local v2, "e":Ljava/io/IOException;
    :goto_1
    const-string v6, "BooksDataStore"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2138
    const-string v6, "BooksDataStore"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2140
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2141
    const-string v6, "BooksDataStore"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2142
    const-string v6, "BooksDataStore"

    const-string v7, "Could not remove malformed recommendation JSON"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2145
    :cond_2
    monitor-exit v5

    move-object v1, v4

    goto :goto_0

    .line 2153
    .end local v0    # "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    monitor-exit v5

    move-object v1, v4

    .line 2155
    goto :goto_0

    .line 2153
    .end local v3    # "file":Ljava/io/File;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 2136
    .restart local v0    # "books":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/widget/RecommendedAdapter$RecommendedBook;>;"
    .restart local v3    # "file":Ljava/io/File;
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public getCcBox(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/CcBox;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1156
    sget-object v6, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->CCBOX_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, p1, p2, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryPage(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    .line 1158
    .local v0, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1159
    const-string v6, "cc_box_w"

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1160
    .local v2, "w":I
    const-string v6, "cc_box_h"

    invoke-virtual {v0, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 1161
    .local v1, "h":I
    if-nez v2, :cond_0

    if-nez v1, :cond_0

    .line 1172
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .end local v1    # "h":I
    .end local v2    # "w":I
    :goto_0
    return-object v5

    .line 1165
    .restart local v1    # "h":I
    .restart local v2    # "w":I
    :cond_0
    :try_start_1
    const-string v5, "cc_box_x"

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1166
    .local v3, "x":I
    const-string v5, "cc_box_y"

    invoke-virtual {v0, v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1167
    .local v4, "y":I
    new-instance v5, Lcom/google/android/apps/books/model/ImmutableCcBox;

    invoke-direct {v5, v3, v4, v2, v1}, Lcom/google/android/apps/books/model/ImmutableCcBox;-><init>(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1172
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    goto :goto_0

    .end local v1    # "h":I
    .end local v2    # "w":I
    .end local v3    # "x":I
    .end local v4    # "y":I
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v5
.end method

.method public getCollectionChanges(J)Ljava/util/Collection;
    .locals 11
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2439
    const-string v9, "dirty!=?"

    .line 2440
    .local v9, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 2441
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->createCollectionVolumesDirUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 2442
    .local v2, "uri":Landroid/net/Uri;
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->COLLECTION_MEMBERSHIP_OPERATIONS:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "dirty!=?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v6

    .line 2444
    .local v6, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 2446
    .local v8, "result":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;>;"
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2449
    :cond_0
    const-string v0, "dirty"

    invoke-virtual {v6, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 2450
    .local v7, "dirtyState":I
    packed-switch v7, :pswitch_data_0

    .line 2458
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected dirty state value "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2466
    .end local v7    # "dirtyState":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 2467
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_1
    throw v0

    .line 2452
    .restart local v7    # "dirtyState":I
    :pswitch_1
    :try_start_1
    sget-object v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;->ADD:Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;

    .line 2461
    .local v10, "type":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;
    :goto_0
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;

    const-string v1, "volume_id"

    invoke-virtual {v6, v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v10, v1}, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChange;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;Ljava/lang/String;)V

    invoke-interface {v8, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 2463
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2466
    .end local v7    # "dirtyState":I
    .end local v10    # "type":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;
    :cond_2
    if-eqz v6, :cond_3

    .line 2467
    invoke-virtual {v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 2470
    :cond_3
    return-object v8

    .line 2455
    .restart local v7    # "dirtyState":I
    :pswitch_2
    :try_start_2
    sget-object v10, Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;->DELETE:Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2456
    .restart local v10    # "type":Lcom/google/android/apps/books/model/BooksDataStore$CollectionChangeType;
    goto :goto_0

    .line 2450
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getContentSize(Ljava/lang/String;)J
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2881
    invoke-virtual {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getFileStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 2882
    .local v1, "baseDir":Ljava/io/File;
    iget-object v4, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 2883
    .local v0, "accountName":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 2885
    .local v2, "contentSize":J
    invoke-static {v1, v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/FileUtils;->totalSizeMaybeMissing(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 2889
    invoke-static {v1, v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildResContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/FileUtils;->totalSizeMaybeMissing(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 2893
    invoke-static {v1, v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildSectionContentDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/FileUtils;->totalSizeMaybeMissing(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 2897
    invoke-static {v1, v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$Files;->buildPageStructureDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/books/util/FileUtils;->totalSizeMaybeMissing(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 2901
    return-wide v2
.end method

.method public getDismissedOffers()Ljava/util/Set;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2272
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    .line 2273
    .local v0, "dismissedOffers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 2275
    .local v3, "is":Ljava/io/InputStream;
    const/4 v7, 0x0

    :try_start_0
    invoke-direct {p0, v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getDismissedOffersFile(Z)Ljava/io/File;

    move-result-object v1

    .line 2276
    .local v1, "file":Ljava/io/File;
    new-instance v4, Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2277
    .end local v3    # "is":Ljava/io/InputStream;
    .local v4, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v4}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    move-result-object v6

    .line 2278
    .local v6, "offers":Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    invoke-virtual {v6}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->getDismissedOffersList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;

    .line 2279
    .local v5, "offer":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    invoke-virtual {v5}, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;->getOfferId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 2281
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "offer":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    .end local v6    # "offers":Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    :catch_0
    move-exception v7

    move-object v3, v4

    .line 2284
    .end local v1    # "file":Ljava/io/File;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :goto_1
    if-eqz v3, :cond_0

    .line 2285
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 2288
    :cond_0
    :goto_2
    return-object v0

    .line 2284
    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v6    # "offers":Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    :cond_1
    if-eqz v4, :cond_3

    .line 2285
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    goto :goto_2

    .line 2284
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "offers":Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v3, :cond_2

    .line 2285
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_2
    throw v7

    .line 2284
    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    goto :goto_3

    .line 2281
    .end local v1    # "file":Ljava/io/File;
    :catch_1
    move-exception v7

    goto :goto_1

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v6    # "offers":Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;
    :cond_3
    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public getDismissedRecommendations()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2181
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    .line 2183
    .local v0, "dismissedRecs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v6, 0x0

    :try_start_0
    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getDismissedRecommendationsFile(Z)Ljava/io/File;

    move-result-object v1

    .line 2184
    .local v1, "file":Ljava/io/File;
    new-instance v3, Ljava/io/FileInputStream;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2187
    .local v3, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v3}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->parseFrom(Ljava/io/InputStream;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2189
    .local v5, "recs":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 2191
    invoke-virtual {v5}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->getDismissedRecsList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    .line 2192
    .local v4, "rec":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    invoke-virtual {v4}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->getVolumeId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2194
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "rec":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .end local v5    # "recs":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;
    :catch_0
    move-exception v6

    .line 2197
    :cond_0
    return-object v0

    .line 2189
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v6

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v6
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
.end method

.method public getDownloadProgress(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 1947
    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->FULL_DOWNLOAD_PROGRESS_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryVolumes(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    .line 1949
    .local v0, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    if-eqz v0, :cond_2

    .line 1950
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1951
    invoke-static {v0}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1957
    if-eqz v0, :cond_0

    .line 1958
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1961
    :cond_0
    :goto_0
    return-object v1

    .line 1952
    :cond_1
    :try_start_1
    const-string v1, "BooksDataStore"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1953
    const-string v1, "BooksDataStore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDownloadProgress query returned empty result for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1957
    :cond_2
    if-eqz v0, :cond_3

    .line 1958
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1961
    :cond_3
    sget-object v1, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->NO_PROGRESS:Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    goto :goto_0

    .line 1957
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_4

    .line 1958
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_4
    throw v1
.end method

.method public getDownloadedDictionaryFile(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Ljava/io/File;
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 951
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->getDownloadedDictionaryFile(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getFileStorageDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksProvider;->getFileStorageDirectory(Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getLocalDictionaryMetadataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 890
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->getLocalDictionaryMetadataList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLocalPageState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalPageState;
    .locals 8
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    .line 1220
    sget-object v4, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->LOCAL_PAGE_STATE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryPage(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v1

    .line 1222
    .local v1, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1223
    const-string v4, "content_status"

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->fromInteger(I)Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    move-result-object v0

    .line 1225
    .local v0, "contentStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    const-string v4, "invalid status"

    invoke-static {v0, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1228
    const-string v4, "structure_status"

    invoke-virtual {v1, v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1229
    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->LOCAL:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .line 1237
    .local v3, "structureStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :goto_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->localContentStateFromContentRow(Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;->getSessionKeyId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    .line 1239
    .local v2, "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    new-instance v4, Lcom/google/android/apps/books/model/ImmutableLocalPageState;

    invoke-direct {v4, v0, v3, v2}, Lcom/google/android/apps/books/model/ImmutableLocalPageState;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/SessionKeyId;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1249
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .end local v0    # "contentStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .end local v2    # "sessionKeyId":Lcom/google/android/apps/books/model/SessionKeyId;
    .end local v3    # "structureStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :goto_1
    return-object v4

    .line 1230
    .restart local v0    # "contentStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :cond_0
    :try_start_1
    sget-object v4, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->FORBIDDEN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    if-ne v0, v4, :cond_1

    .line 1231
    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->FORBIDDEN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .restart local v3    # "structureStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    goto :goto_0

    .line 1235
    .end local v3    # "structureStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :cond_1
    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->NETWORK:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    .restart local v3    # "structureStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    goto :goto_0

    .line 1245
    .end local v0    # "contentStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    .end local v3    # "structureStatus":Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;
    :cond_2
    new-instance v4, Lcom/google/android/apps/books/model/ImmutableLocalPageState;

    sget-object v5, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->UNKNOWN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    sget-object v6, Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;->UNKNOWN:Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/books/model/ImmutableLocalPageState;-><init>(Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/BooksDataStore$ContentStatusEnum;Lcom/google/android/apps/books/model/SessionKeyId;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1249
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v4
.end method

.method public getLocalResourceState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 2671
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getLocalContentState(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;

    move-result-object v0

    return-object v0
.end method

.method public getLocalSegmentState(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 2665
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getLocalContentState(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/android/apps/books/model/BooksDataStore$LocalContentState;

    move-result-object v0

    return-object v0
.end method

.method public getLocalVolumeData(Ljava/lang/String;)Lcom/google/android/apps/books/model/LocalVolumeData;
    .locals 12
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1045
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates;->buildItemUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1046
    .local v2, "stateUri":Landroid/net/Uri;
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v5

    .line 1049
    .local v5, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    if-nez v5, :cond_0

    .line 1050
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null cursor"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055
    :cond_0
    const-wide/16 v10, 0x0

    .line 1057
    .local v10, "timestamp":J
    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1058
    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildLocalVolumeData(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;ZJ)Lcom/google/android/apps/books/model/LocalVolumeData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1061
    invoke-virtual {v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1064
    :goto_0
    return-object v0

    .line 1061
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 1064
    sget-object v0, Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;->DEFAULT:Lcom/google/android/apps/books/model/ImmutableLocalVolumeData;

    goto :goto_0

    .line 1061
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v0
.end method

.method public getMyEbooksVolumeIdsWithMaybeVersions(I)Ljava/util/List;
    .locals 9
    .param p1, "maxResults"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 823
    sget-object v6, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->VOLUME_VERSION_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryMyEbooks(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v4

    .line 825
    .local v4, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->getCount()I

    move-result v3

    .line 826
    .local v3, "count":I
    if-lez p1, :cond_0

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 827
    .local v2, "answerSize":I
    :goto_0
    invoke-static {v2}, Lcom/google/common/collect/Lists;->newArrayListWithExpectedSize(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 829
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    const/4 v6, -0x1

    invoke-virtual {v4, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToPosition(I)V

    .line 830
    const/4 v0, 0x0

    .local v0, "added":I
    move v1, v0

    .line 831
    .end local v0    # "added":I
    .local v1, "added":I
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "added":I
    .restart local v0    # "added":I
    if-ge v1, v2, :cond_2

    .line 832
    new-instance v6, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;

    const-string v7, "volume_id"

    invoke-virtual {v4, v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "content_version"

    invoke-virtual {v4, v8}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v0

    .end local v0    # "added":I
    .restart local v1    # "added":I
    goto :goto_1

    .end local v1    # "added":I
    .end local v2    # "answerSize":I
    .end local v5    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    :cond_0
    move v2, v3

    .line 826
    goto :goto_0

    .restart local v1    # "added":I
    .restart local v2    # "answerSize":I
    .restart local v5    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    :cond_1
    move v0, v1

    .line 837
    .end local v1    # "added":I
    .restart local v0    # "added":I
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-object v5

    .end local v0    # "added":I
    .end local v2    # "answerSize":I
    .end local v3    # "count":I
    .end local v5    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/annotations/VolumeWithMaybeVersion;>;"
    :catchall_0
    move-exception v6

    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v6
.end method

.method public getMyEbooksVolumes(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 684
    .local p1, "localVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    .local p2, "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 685
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getMyEbooksVolumesRange(ILjava/util/List;Ljava/util/Map;Ljava/util/Map;)V

    .line 686
    return-object v0
.end method

.method public getMyEbooksVolumesRange(ILjava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 1
    .param p1, "maxVolumes"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 693
    .local p2, "localVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    .local p3, "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 694
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getMyEbooksVolumesRange(ILjava/util/List;Ljava/util/Map;Ljava/util/Map;)V

    .line 695
    return-object v0
.end method

.method public getMyEbooksVolumesRange(ILjava/util/List;Ljava/util/Map;Ljava/util/Map;)V
    .locals 19
    .param p1, "maxVolumes"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/VolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/LocalVolumeData;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/books/model/VolumeDownloadProgress;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 703
    .local p2, "volumeData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/VolumeData;>;"
    .local p3, "localVolumeDataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/LocalVolumeData;>;"
    .local p4, "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeDownloadProgress;>;"
    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStoreImpl$CollectionVolumesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryMyEbooks(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v4

    .line 705
    .local v4, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    const/4 v3, -0x1

    :try_start_0
    invoke-virtual {v4, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToPosition(I)V

    .line 706
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableVolumeData;->builder()Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    move-result-object v9

    .line 707
    .local v9, "builder":Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    const/16 v16, 0x0

    .line 708
    .local v16, "numVolumes":I
    :cond_0
    :goto_0
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    if-ltz p1, :cond_1

    move/from16 v0, v16

    move/from16 v1, p1

    if-ge v0, v1, :cond_4

    .line 709
    :cond_1
    const-string v3, "position"

    invoke-virtual {v4, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 710
    .local v5, "readingPosition":Ljava/lang/String;
    const-string v3, "last_access"

    invoke-virtual {v4, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 711
    .local v6, "lastAccess":J
    const-string v3, "timestamp"

    invoke-virtual {v4, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 712
    .local v14, "timestamp":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToVolumeData(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;JLandroid/accounts/Account;Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Lcom/google/android/apps/books/model/VolumeData;

    move-result-object v18

    .line 714
    .local v18, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;->reset()V

    .line 715
    add-int/lit8 v16, v16, 0x1

    .line 716
    if-eqz p2, :cond_2

    .line 717
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 719
    :cond_2
    invoke-interface/range {v18 .. v18}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v12

    .line 720
    .local v12, "volumeId":Ljava/lang/String;
    if-eqz p4, :cond_3

    .line 721
    invoke-static {v4}, Lcom/google/android/apps/books/model/VolumeDownloadProgress;->fromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Lcom/google/android/apps/books/model/VolumeDownloadProgress;

    move-result-object v17

    .line 723
    .local v17, "progress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    .end local v17    # "progress":Lcom/google/android/apps/books/model/VolumeDownloadProgress;
    :cond_3
    if-eqz p3, :cond_0

    .line 726
    const/4 v13, 0x0

    move-object/from16 v10, p0

    move-object v11, v4

    invoke-direct/range {v10 .. v15}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildLocalVolumeData(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;ZJ)Lcom/google/android/apps/books/model/LocalVolumeData;

    move-result-object v2

    .line 728
    .local v2, "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    move-object/from16 v0, p3

    invoke-interface {v0, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 733
    .end local v2    # "localVolumeData":Lcom/google/android/apps/books/model/LocalVolumeData;
    .end local v5    # "readingPosition":Ljava/lang/String;
    .end local v6    # "lastAccess":J
    .end local v9    # "builder":Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .end local v12    # "volumeId":Ljava/lang/String;
    .end local v14    # "timestamp":J
    .end local v16    # "numVolumes":I
    .end local v18    # "volume":Lcom/google/android/apps/books/model/VolumeData;
    :catchall_0
    move-exception v3

    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v3

    .restart local v9    # "builder":Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    .restart local v16    # "numVolumes":I
    :cond_4
    invoke-virtual {v4}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-void
.end method

.method public getOffersFile(Z)Ljava/io/File;
    .locals 3
    .param p1, "shouldMkdir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2219
    new-instance v0, Ljava/io/File;

    const-string v1, "offers_cache"

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getNamedCacheDir(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v1

    const-string v2, "offers.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getOwnedVolumeIds()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 843
    sget-object v6, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->OWNED_VOLUMES_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryMyEbooks(Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v2

    .line 845
    .local v2, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 846
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v6, -0x1

    invoke-virtual {v2, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToPosition(I)V

    .line 852
    :cond_0
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 853
    const-string v6, "viewability"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 854
    .local v5, "viewability":Ljava/lang/String;
    const-string v6, "buy_url"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 855
    .local v1, "buyUrl":Ljava/lang/String;
    const-string v6, "open_access"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 856
    .local v3, "openAccess":Ljava/lang/String;
    invoke-static {v5, v3, v1}, Lcom/google/android/apps/books/model/VolumeData$Access;->getInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData$Access;

    move-result-object v0

    .line 857
    .local v0, "access":Lcom/google/android/apps/books/model/VolumeData$Access;
    sget-object v6, Lcom/google/android/apps/books/model/VolumeData$Access;->SAMPLE:Lcom/google/android/apps/books/model/VolumeData$Access;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/books/model/VolumeData$Access;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 860
    const-string v6, "rental_state"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 863
    const-string v6, "volume_id"

    invoke-virtual {v2, v6}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 867
    .end local v0    # "access":Lcom/google/android/apps/books/model/VolumeData$Access;
    .end local v1    # "buyUrl":Ljava/lang/String;
    .end local v3    # "openAccess":Ljava/lang/String;
    .end local v4    # "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "viewability":Ljava/lang/String;
    :catchall_0
    move-exception v6

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v6

    .restart local v4    # "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-object v4
.end method

.method public getPage(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Page;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 1128
    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->PAGE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryPage(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v1

    .line 1130
    .local v1, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1131
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutablePage;->builder()Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    move-result-object v0

    .line 1132
    .local v0, "builder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    const-string v3, "page_id"

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->pageId(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1133
    const-string v3, "page_order"

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->pageOrder(I)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1134
    const-string v3, "content_status"

    invoke-virtual {v1, v3}, Lcom/google/android/apps/books/model/StringSafeCursor;->getInt(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v2, :cond_0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->isViewable(Z)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1135
    const-string v2, "title"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1136
    const-string v2, "remote_url"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->remoteUrl(Ljava/lang/String;)Lcom/google/android/apps/books/model/ImmutablePage$Builder;

    .line 1137
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/ImmutablePage$Builder;->build()Lcom/google/android/apps/books/model/Page;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1142
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .end local v0    # "builder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    :goto_1
    return-object v2

    .line 1134
    .restart local v0    # "builder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1139
    .end local v0    # "builder":Lcom/google/android/apps/books/model/ImmutablePage$Builder;
    :cond_1
    const/4 v2, 0x0

    .line 1142
    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v2
.end method

.method public getPageImageFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 1263
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getPageContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->pageContentFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;ZLcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getPageStructureFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/google/android/apps/books/model/Page;

    .prologue
    .line 1269
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/apps/books/model/Page;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getPageStructureContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->pageContentFile(Ljava/lang/String;Lcom/google/android/apps/books/model/Page;ZLcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getRecentVolumeSearches(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2319
    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getVolumeSearchesFile(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    .line 2320
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2321
    new-instance v3, Lcom/google/android/apps/books/model/BooksDataStoreImpl$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$4;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)V

    .line 2322
    .local v3, "typeRef":Lorg/codehaus/jackson/type/TypeReference;, "Lorg/codehaus/jackson/type/TypeReference<Ljava/util/List<Ljava/lang/String;>;>;"
    new-instance v1, Lorg/codehaus/jackson/map/ObjectMapper;

    invoke-direct {v1}, Lorg/codehaus/jackson/map/ObjectMapper;-><init>()V

    .line 2323
    .local v1, "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    invoke-virtual {v1, v0, v3}, Lorg/codehaus/jackson/map/ObjectMapper;->readValue(Ljava/io/File;Lorg/codehaus/jackson/type/TypeReference;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 2324
    .local v2, "searches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "BooksDataStore"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2325
    const-string v4, "BooksDataStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Restored : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2330
    .end local v1    # "mapper":Lorg/codehaus/jackson/map/ObjectMapper;
    .end local v2    # "searches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "typeRef":Lorg/codehaus/jackson/type/TypeReference;, "Lorg/codehaus/jackson/type/TypeReference<Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-static {}, Lcom/google/common/collect/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v2

    goto :goto_0
.end method

.method public getRecommendationsCacheDir(Z)Ljava/io/File;
    .locals 1
    .param p1, "shouldMkdir"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2171
    const-string v0, "recs_cache"

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getNamedCacheDir(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getRequestedDictionaryMetadataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 920
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->getRequestedDictionaryMetadataList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResource(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/Resource;
    .locals 6
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2820
    const-string v2, "resource_id=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object p2, v3, v4

    invoke-direct {p0, p1, v2, v3, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryResources(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 2824
    .local v0, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v5, :cond_0

    .line 2827
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/books/model/Resource;

    goto :goto_0
.end method

.method public getResourceContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 2684
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getResourceContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Resources;->buildResourceUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "content_status"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->contentFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getResourceResources(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1351
    sget-object v1, Lcom/google/android/apps/books/model/BooksDataStoreImpl$ResourceResourcesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    .line 1352
    .local v1, "query":Lcom/google/android/apps/books/model/StringSafeQuery;
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryResourceResources(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/StringSafeQuery;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v0

    .line 1354
    .local v0, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->resourcesFromCursor(Lcom/google/android/apps/books/model/StringSafeCursor;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1356
    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    throw v2
.end method

.method public getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;

    .prologue
    .line 2677
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getSegmentContentFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionUri(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "content_status"

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->contentFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getServerDictionaryMetadataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/dictionary/DictionaryMetadata;",
            ">;"
        }
    .end annotation

    .prologue
    .line 895
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->getServerDictionaryMetadataList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 3
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 2385
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getSharedResourceContentFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V

    return-object v0
.end method

.method public getSharedResourceMD5File(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 3
    .param p1, "resId"    # Ljava/lang/String;

    .prologue
    .line 2390
    new-instance v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getSharedResourceMD5File(Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$StoredContentFile;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;Lcom/google/android/apps/books/data/InternalVolumeContentFile;Lcom/google/android/apps/books/data/InternalVolumeContentFile$OnContentSaveListener;)V

    return-object v0
.end method

.method public getStaleVolumeIdsToDelete(J)Ljava/util/Set;
    .locals 15
    .param p1, "cutoffTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2846
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "volume_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "max_collection_volumes_timestamp"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "max_last_local_access"

    aput-object v3, v2, v0

    .line 2853
    .local v2, "columns":[Ljava/lang/String;
    const-string v11, "max_collection_volumes_timestamp IS NULL"

    .line 2854
    .local v11, "onNoShelf":Ljava/lang/String;
    const-string v10, "(max_last_local_access IS NULL OR max_last_local_access < CAST(? AS INTEGER))"

    .line 2856
    .local v10, "notReadRecently":Ljava/lang/String;
    const-string v8, "max_collection_volumes_timestamp < CAST(? AS INTEGER)"

    .line 2858
    .local v8, "notAddedRecently":Ljava/lang/String;
    const-string v9, "pinned=0"

    .line 2859
    .local v9, "notPinned":Ljava/lang/String;
    const-string v12, "max_collection_volumes_timestamp IS NULL OR (pinned=0 AND max_collection_volumes_timestamp < CAST(? AS INTEGER) AND (max_last_local_access IS NULL OR max_last_local_access < CAST(? AS INTEGER)))"

    .line 2861
    .local v12, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v0

    const/4 v0, 0x1

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v0

    .line 2865
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildAccountVolumesDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    .line 2866
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "max_collection_volumes_timestamp IS NULL OR (pinned=0 AND max_collection_volumes_timestamp < CAST(? AS INTEGER) AND (max_last_local_access IS NULL OR max_last_local_access < CAST(? AS INTEGER)))"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2868
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v7

    .line 2869
    .local v7, "idsToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2870
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 2871
    .local v13, "volumeId":Ljava/lang/String;
    invoke-interface {v7, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2875
    .end local v7    # "idsToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "volumeId":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .restart local v7    # "idsToDelete":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getUserSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 930
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->getUserSessionKey()Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    return-object v0
.end method

.method public getVolume(Ljava/lang/String;)Lcom/google/android/apps/books/model/VolumeData;
    .locals 10
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 643
    const/4 v9, 0x0

    .line 644
    .local v9, "volumeCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    const/4 v8, 0x0

    .line 646
    .local v8, "statesCursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryVolumes(Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v9

    .line 648
    if-nez v9, :cond_2

    .line 649
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Null cursor"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_0

    .line 671
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 673
    :cond_0
    if-eqz v8, :cond_1

    .line 674
    invoke-virtual {v8}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_1
    throw v0

    .line 652
    :cond_2
    :try_start_1
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 659
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl$VolumeStatesQuery;->STRING_SAFE_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates;->buildItemUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v8

    .line 661
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 662
    const-string v0, "position"

    invoke-virtual {v8, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 663
    .local v3, "readingPosition":Ljava/lang/String;
    const-string v0, "last_access"

    invoke-virtual {v8, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 664
    .local v4, "lastAccess":J
    invoke-static {}, Lcom/google/android/apps/books/model/ImmutableVolumeData;->builder()Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;

    move-result-object v7

    .line 665
    .local v7, "builder":Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    iget-object v6, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object v1, p0

    move-object v2, v9

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->rowToVolumeData(Lcom/google/android/apps/books/model/StringSafeCursor;Ljava/lang/String;JLandroid/accounts/Account;Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;)Lcom/google/android/apps/books/model/VolumeData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 670
    if-eqz v9, :cond_3

    .line 671
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 673
    :cond_3
    if-eqz v8, :cond_4

    .line 674
    invoke-virtual {v8}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 677
    .end local v3    # "readingPosition":Ljava/lang/String;
    .end local v4    # "lastAccess":J
    .end local v7    # "builder":Lcom/google/android/apps/books/model/ImmutableVolumeData$Builder;
    :cond_4
    :goto_0
    return-object v0

    .line 670
    :cond_5
    if-eqz v9, :cond_6

    .line 671
    invoke-virtual {v9}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    .line 673
    :cond_6
    if-eqz v8, :cond_7

    .line 674
    invoke-virtual {v8}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_7
    move-object v0, v6

    .line 677
    goto :goto_0
.end method

.method public getVolumeCoverFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2380
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->coverFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeIdsForLicenseRenewal()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 995
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 996
    .local v8, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildAccountVolumesDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v2

    .line 997
    .local v2, "uri":Landroid/net/Uri;
    const-string v6, "pinned!=0 OR has_offline_license!=0"

    .line 1000
    .local v6, "VOLUMES_FOR_LICENSE_RENEWAL_SELECTION":Ljava/lang/String;
    sget-object v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->VOLUMES_FOR_LICENSE_RENEWAL_QUERY:Lcom/google/android/apps/books/model/StringSafeQuery;

    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "pinned!=0 OR has_offline_license!=0"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/books/model/StringSafeQuery;->query(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/model/StringSafeCursor;

    move-result-object v7

    .line 1005
    .local v7, "cursor":Lcom/google/android/apps/books/model/StringSafeCursor;
    :goto_0
    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    const-string v0, "volume_id"

    invoke-virtual {v7, v0}, Lcom/google/android/apps/books/model/StringSafeCursor;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1007
    .local v9, "volumeId":Ljava/lang/String;
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1011
    .end local v9    # "volumeId":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_0

    .line 1012
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_0
    throw v0

    .line 1011
    :cond_1
    if-eqz v7, :cond_2

    .line 1012
    invoke-virtual {v7}, Lcom/google/android/apps/books/model/StringSafeCursor;->close()V

    :cond_2
    return-object v8
.end method

.method public getVolumeManifest(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/google/android/apps/books/model/VolumeManifest;
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/books/model/VolumeManifest;"
        }
    .end annotation

    .prologue
    .line 1412
    .local p2, "resourceTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "outLocalSegmentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "outLocalResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p5, "outLocalPageIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p6, "outLocalStructureIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v2, "load manifest"

    new-instance v3, Lcom/google/android/apps/books/model/BooksDataStoreImpl$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$2;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)V

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v0

    .line 1421
    .local v0, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :try_start_0
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getVolumeManifestNoLogging(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)Lcom/google/android/apps/books/model/VolumeManifest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1425
    invoke-interface {v0}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    throw v1
.end method

.method public getVolumeNetworkResources(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1362
    const-string v0, "content_status=2"

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryResources(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeResources(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1868
    .local p2, "types":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "outLocalResourceIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1869
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1870
    .local v4, "whereBuilder":Ljava/lang/StringBuilder;
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v1

    .line 1871
    .local v1, "typeCount":I
    const/4 v2, 0x0

    .local v2, "typeIndex":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1872
    if-eqz v2, :cond_0

    .line 1873
    const-string v5, " OR "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1875
    :cond_0
    const-string v5, "resource_type=?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1871
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1877
    :cond_1
    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1878
    .local v3, "where":Ljava/lang/String;
    new-array v0, v1, [Ljava/lang/String;

    .line 1879
    .local v0, "args":[Ljava/lang/String;
    invoke-interface {p2, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1884
    .end local v1    # "typeCount":I
    .end local v2    # "typeIndex":I
    .end local v4    # "whereBuilder":Ljava/lang/StringBuilder;
    :goto_1
    invoke-direct {p0, p1, v3, v0, p3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->queryResources(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/util/Set;)Ljava/util/List;

    move-result-object v5

    return-object v5

    .line 1881
    .end local v0    # "args":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    .line 1882
    .restart local v3    # "where":Ljava/lang/String;
    const/4 v0, 0x0

    .restart local v0    # "args":[Ljava/lang/String;
    goto :goto_1
.end method

.method public getVolumeSearchesFile(Ljava/lang/String;Z)Ljava/io/File;
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "mkDirs"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2371
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeSearchesFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/books/data/InternalVolumeContentFile;->getFile()Ljava/io/File;

    move-result-object v0

    .line 2372
    .local v0, "searchesFile":Ljava/io/File;
    if-eqz p2, :cond_0

    .line 2373
    invoke-static {v0}, Lcom/google/android/apps/books/util/FileUtils;->ensureParent(Ljava/io/File;)Ljava/io/File;

    .line 2375
    :cond_0
    return-object v0
.end method

.method public getVolumeSessionKey(Lcom/google/android/apps/books/model/VolumeSessionKeyId;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 7
    .param p1, "id"    # Lcom/google/android/apps/books/model/VolumeSessionKeyId;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/VolumeSessionKeyId;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/VolumeSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2577
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->getRowId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/apps/books/provider/BooksContract$SessionKeys;->buildSessionKeyUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 2578
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/apps/books/model/VolumeSessionKeyId$Query;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2581
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582
    invoke-static {v6}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->keyFromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2585
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2587
    :goto_0
    return-object v3

    .line 2585
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getVolumeThumbnailFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;
    .locals 2
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2400
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/books/provider/VolumeContentStore;->getVolumeCoverThumbnailFile(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/InternalVolumeContentFile;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->coverFile(Ljava/lang/String;Lcom/google/android/apps/books/data/InternalVolumeContentFile;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    return-object v0
.end method

.method public insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 915
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->insertDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 916
    return-void
.end method

.method public isVolumeCoverLocal(Ljava/lang/String;)Z
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2395
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getVolumeCoverFile(Ljava/lang/String;)Lcom/google/android/apps/books/data/VolumeContentFile;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/books/data/VolumeContentFile;->exists()Z

    move-result v0

    return v0
.end method

.method public openFallbackCoverInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2838
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAssetManager:Landroid/content/res/AssetManager;

    const-string v1, "fallback_cover.png"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public purgeDeletedCollectionVolumes(JLjava/util/Collection;)V
    .locals 5
    .param p1, "collectionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2475
    .local p3, "volumeIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dirty!=1 AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "volume_id"

    invoke-static {p3, v2}, Lcom/google/android/apps/books/sync/SyncUtil;->buildInClauseFromStrings(Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2478
    .local v0, "whereClause":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->createCollectionVolumesDirUri(J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2479
    return-void
.end method

.method public removeDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 905
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->removeDictionaryMetadata(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 906
    return-void
.end method

.method public removeFromCollection(JLjava/lang/String;)V
    .locals 1
    .param p1, "collectionId"    # J
    .param p3, "volumeId"    # Ljava/lang/String;

    .prologue
    .line 2426
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateCollectionRow(JLjava/lang/String;I)V

    .line 2427
    return-void
.end method

.method public removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V
    .locals 1
    .param p1, "metadata"    # Lcom/google/android/apps/books/dictionary/DictionaryMetadata;

    .prologue
    .line 900
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->removeLocalDictionary(Lcom/google/android/apps/books/dictionary/DictionaryMetadata;)V

    .line 901
    return-void
.end method

.method public removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/AccountSessionKeyId;)V
    .locals 3
    .param p1, "id"    # Lcom/google/android/apps/books/model/AccountSessionKeyId;

    .prologue
    const/4 v2, 0x0

    .line 2812
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v1}, Lcom/google/android/apps/books/provider/BooksContract$AccountContent;->buildClearContentUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2814
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/apps/books/model/DataControllerStore;->deleteDictionaryMetadataForAccount(Landroid/accounts/Account;)V

    .line 2815
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->clearLastDictionaryMetadataSyncTime()V

    .line 2816
    return-void
.end method

.method public removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/VolumeSessionKeyId;)V
    .locals 5
    .param p1, "id"    # Lcom/google/android/apps/books/model/VolumeSessionKeyId;

    .prologue
    const/4 v4, 0x0

    .line 2619
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentStore:Lcom/google/android/apps/books/provider/VolumeContentStore;

    iget-object v1, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->getVolumeId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/books/provider/VolumeContentStore;->clearAllContentFiles(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2620
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {p1}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->getRowId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$SessionKeys;->buildSessionKeyUri(J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2621
    return-void
.end method

.method public removeUserSessionKey()V
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0}, Lcom/google/android/apps/books/model/DataControllerStore;->removeUserSessionKey()V

    .line 946
    return-void
.end method

.method public revokeDictionaryLanguage(Ljava/lang/String;)V
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 910
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->revokeDictionaryLanguage(Ljava/lang/String;)V

    .line 911
    return-void
.end method

.method public saveAccountSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 4
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2527
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2528
    .local v1, "values":Landroid/content/ContentValues;
    sget-object v0, Lcom/google/android/apps/books/provider/BooksContract$Accounts;->CONTENT_URI:Landroid/net/Uri;

    .line 2529
    .local v0, "uri":Landroid/net/Uri;
    const-string v2, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2530
    const-string v2, "BooksDataStore"

    const-string v3, "AccountSessionKeyId inserting new row"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2532
    :cond_0
    const-string v2, "account_name"

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533
    invoke-static {p1, v1}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V

    .line 2534
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2535
    invoke-static {}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->get()Lcom/google/android/apps/books/model/AccountSessionKeyId;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->create(Lcom/google/android/apps/books/model/SessionKeyId;Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v2

    return-object v2
.end method

.method public saveUserSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;
    .locals 1
    .param p1, "key"    # Lcom/google/android/apps/books/model/SessionKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/SessionKey;",
            ")",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->saveUserSessionKey(Lcom/google/android/apps/books/model/SessionKey;)Lcom/google/android/apps/books/model/LocalSessionKey;

    move-result-object v0

    return-object v0
.end method

.method public setCachedOffers(Lcom/google/android/apps/books/api/data/ApiaryOffers;)V
    .locals 5
    .param p1, "apiaryOffers"    # Lcom/google/android/apps/books/api/data/ApiaryOffers;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2262
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getOffersFile(Z)Ljava/io/File;

    move-result-object v0

    .line 2263
    .local v0, "file":Ljava/io/File;
    new-instance v2, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v2}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v4, "UTF-8"

    invoke-static {v4}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonGenerator(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)Lcom/google/api/client/json/JsonGenerator;

    move-result-object v1

    .line 2265
    .local v1, "jsonGenerator":Lcom/google/api/client/json/JsonGenerator;
    invoke-virtual {v1, p1}, Lcom/google/api/client/json/JsonGenerator;->serialize(Ljava/lang/Object;)V

    .line 2266
    invoke-virtual {v1}, Lcom/google/api/client/json/JsonGenerator;->close()V

    .line 2267
    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->immediatelyBroadcastContentChange()V

    .line 2268
    return-void
.end method

.method public setCachedRecommendations(Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;)V
    .locals 6
    .param p1, "books"    # Lcom/google/android/apps/books/app/data/JsonRecommendedBook$Books;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2160
    sget-object v3, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mHomeRecommendationFileLock:Ljava/lang/Object;

    monitor-enter v3

    .line 2161
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getHomeRecommendationsFile(Z)Ljava/io/File;

    move-result-object v1

    .line 2162
    .local v1, "recommendationsFile":Ljava/io/File;
    new-instance v2, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v2}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v5, "UTF-8"

    invoke-static {v5}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/api/client/json/jackson/JacksonFactory;->createJsonGenerator(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)Lcom/google/api/client/json/JsonGenerator;

    move-result-object v0

    .line 2164
    .local v0, "jsonGenerator":Lcom/google/api/client/json/JsonGenerator;
    invoke-virtual {v0, p1}, Lcom/google/api/client/json/JsonGenerator;->serialize(Ljava/lang/Object;)V

    .line 2165
    invoke-virtual {v0}, Lcom/google/api/client/json/JsonGenerator;->close()V

    .line 2166
    monitor-exit v3

    .line 2167
    return-void

    .line 2166
    .end local v0    # "jsonGenerator":Lcom/google/api/client/json/JsonGenerator;
    .end local v1    # "recommendationsFile":Ljava/io/File;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public setCcBox(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/books/model/CcBox;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pageId"    # Ljava/lang/String;
    .param p3, "box"    # Lcom/google/android/apps/books/model/CcBox;

    .prologue
    .line 1148
    invoke-static {p3}, Lcom/google/android/apps/books/provider/DataConversions;->ccBoxToContentValues(Lcom/google/android/apps/books/model/CcBox;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updatePageRow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    .line 1149
    return-void
.end method

.method public setDismissedOffers(Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2293
    .local p1, "offerIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getDismissedOffersFile(Z)Ljava/io/File;

    move-result-object v1

    .line 2294
    .local v1, "file":Ljava/io/File;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->newBuilder()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;

    move-result-object v0

    .line 2295
    .local v0, "builder":Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2296
    .local v4, "offerId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;->newBuilder()Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer$Builder;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer$Builder;->setOfferId(Ljava/lang/String;)Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer$Builder;->build()Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;

    move-result-object v3

    .line 2298
    .local v3, "offer":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    invoke-virtual {v0, v3}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->addDismissedOffers(Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;)Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;

    goto :goto_0

    .line 2300
    .end local v3    # "offer":Lcom/google/android/apps/books/app/proto/DismissedOffer$Offer;
    .end local v4    # "offerId":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x0

    .line 2302
    .local v5, "os":Ljava/io/OutputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2303
    .end local v5    # "os":Ljava/io/OutputStream;
    .local v6, "os":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList$Builder;->build()Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/google/android/apps/books/app/proto/DismissedOffer$DismissedOfferList;->writeTo(Ljava/io/OutputStream;)V

    .line 2304
    invoke-direct {p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->immediatelyBroadcastContentChange()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2306
    if-eqz v6, :cond_1

    .line 2307
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 2310
    :cond_1
    return-void

    .line 2306
    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v5    # "os":Ljava/io/OutputStream;
    :catchall_0
    move-exception v7

    :goto_1
    if-eqz v5, :cond_2

    .line 2307
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    :cond_2
    throw v7

    .line 2306
    .end local v5    # "os":Ljava/io/OutputStream;
    .restart local v6    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "os":Ljava/io/OutputStream;
    .restart local v5    # "os":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public setDismissedRecommendations(Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2202
    .local p1, "volumeIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getDismissedRecommendationsFile(Z)Ljava/io/File;

    move-result-object v1

    .line 2203
    .local v1, "file":Ljava/io/File;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->newBuilder()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    move-result-object v0

    .line 2205
    .local v0, "builder":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2206
    .local v5, "volumeId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;->newBuilder()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->setVolumeId(Ljava/lang/String;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation$Builder;->build()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;

    move-result-object v4

    .line 2207
    .local v4, "rec":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    invoke-virtual {v0, v4}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->addDismissedRecs(Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;)Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;

    goto :goto_0

    .line 2209
    .end local v4    # "rec":Lcom/google/android/apps/books/app/proto/DismissedRecommendation$Recommendation;
    .end local v5    # "volumeId":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 2211
    .local v3, "os":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList$Builder;->build()Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/android/apps/books/app/proto/DismissedRecommendation$DismissedRecommendationList;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2213
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 2215
    return-void

    .line 2213
    :catchall_0
    move-exception v6

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v6
.end method

.method public setFitWidth(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "fitWidth"    # Z

    .prologue
    const/4 v2, 0x0

    .line 3019
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing/empty volumeId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3021
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3022
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3023
    const-string v3, "fit_width"

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3025
    const-string v1, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3026
    const-string v1, "BooksDataStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting fitWidth "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3028
    :cond_0
    invoke-direct {p0, p1, v0, v2, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 3029
    return-void

    :cond_1
    move v1, v2

    .line 3023
    goto :goto_0
.end method

.method public setForceDownload(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "forceDownload"    # Z

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 224
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "force_download"

    invoke-static {p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->booleanToProviderInt(Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 225
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 226
    return-void
.end method

.method public setHasOfflineLicense(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "hasOfflineLicense"    # Z

    .prologue
    const/4 v3, 0x0

    .line 230
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 231
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "has_offline_license"

    invoke-static {p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->booleanToProviderInt(Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 232
    invoke-direct {p0, p1, v0, v3, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 233
    return-void
.end method

.method public setLastVolumeSearch(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lastSearch"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2336
    const-string v1, "BooksDataStore"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2337
    const-string v1, "BooksDataStore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Persisting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2339
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getRecentVolumeSearches(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2341
    .local v0, "recentSearches":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0, p2}, Lcom/google/android/apps/books/model/RecentSearchesUtil;->setLastVolumeSearch(Ljava/util/List;Ljava/lang/String;)V

    .line 2342
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->persistVolumeSearches(Ljava/lang/String;Ljava/util/List;)V

    .line 2343
    return-void
.end method

.method public setLicenseAction(Ljava/lang/String;Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;

    .prologue
    const/4 v3, 0x0

    .line 237
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    .line 238
    .local v1, "values":Landroid/content/ContentValues;
    invoke-virtual {p2}, Lcom/google/android/apps/books/provider/BooksContract$VolumeStates$LicenseAction;->getDbValue()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "licenseActionValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 240
    const-string v2, "license_action"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :goto_0
    invoke-direct {p0, p1, v1, v3, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 245
    return-void

    .line 242
    :cond_0
    const-string v2, "license_action"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setLineHeight(Ljava/lang/String;F)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lineHeight"    # F

    .prologue
    const/4 v2, 0x0

    .line 2987
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing/empty volumeId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2988
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Illegal lineHeight value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 2990
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2991
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2992
    const-string v1, "line_height"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2994
    const-string v1, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2995
    const-string v1, "BooksDataStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "persisting lineHeight "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2998
    :cond_0
    invoke-direct {p0, p1, v0, v2, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 2999
    return-void

    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_1
    move v1, v2

    .line 2988
    goto :goto_0
.end method

.method public setMyEbooksVolumes(Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;)Z
    .locals 18
    .param p1, "response"    # Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;

    .prologue
    .line 423
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 424
    .local v10, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->getVolumes()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/books/model/VolumeData;

    .line 425
    .local v12, "volume":Lcom/google/android/apps/books/model/VolumeData;
    invoke-static {v12}, Lcom/google/android/apps/books/provider/DataConversions;->volumeDataToContentValues(Lcom/google/android/apps/books/model/VolumeData;)Landroid/content/ContentValues;

    move-result-object v9

    .line 436
    .local v9, "values":Landroid/content/ContentValues;
    invoke-interface {v12}, Lcom/google/android/apps/books/model/VolumeData;->getVolumeId()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/android/apps/books/sync/SyncVolumeLicensesResponse;->getContentVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 437
    .local v3, "contentVersion":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 438
    const-string v14, "content_version"

    invoke-virtual {v9, v14, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_0
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 444
    .end local v3    # "contentVersion":Ljava/lang/String;
    .end local v9    # "values":Landroid/content/ContentValues;
    .end local v12    # "volume":Lcom/google/android/apps/books/model/VolumeData;
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncVolumeRows(Ljava/util/List;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    move-result-object v13

    .line 446
    .local v13, "volumesResult":Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildCollectionVolumesSyncher(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/TableSynchronizer;

    move-result-object v14

    invoke-virtual {v14, v10}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRows(Ljava/lang/Iterable;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    move-result-object v2

    .line 449
    .local v2, "collectionsResult":Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    invoke-static {v10}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->filterToLastAccessValues(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 450
    .local v11, "valuesWithLastAccess":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildStatesSyncher(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/TableSynchronizer;

    move-result-object v8

    .line 451
    .local v8, "statesSynchronizer":Lcom/google/android/apps/books/sync/TableSynchronizer;
    invoke-virtual {v8, v11}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRows(Ljava/lang/Iterable;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    move-result-object v7

    .line 454
    .local v7, "statesResult":Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v15, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v14, v15}, Lcom/google/android/apps/books/sync/SyncAccountsState;->getLastMyEbooksFetchTime(Ljava/lang/String;)J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_7

    const/4 v4, 0x1

    .line 458
    .local v4, "firstFetch":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mSyncState:Lcom/google/android/apps/books/sync/SyncAccountsState;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v15, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mClock:Lcom/google/android/apps/books/model/Clock;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/google/android/apps/books/model/Clock;->currentTime()J

    move-result-wide v16

    invoke-interface/range {v14 .. v17}, Lcom/google/android/apps/books/sync/SyncAccountsState;->setLastMyEbooksFetchTime(Ljava/lang/String;J)V

    .line 462
    if-eqz v4, :cond_2

    .line 463
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v15}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->buildAccountVolumesDirUri(Landroid/accounts/Account;)Landroid/net/Uri;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v14 .. v17}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 468
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v15, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v15}, Lcom/google/android/apps/books/provider/BooksContract$CollectionVolumes;->myEBooksDirUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v14 .. v17}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 481
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z

    move-result v14

    if-nez v14, :cond_3

    iget-object v14, v2, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->orphanRows:Ljava/util/Collection;

    invoke-interface {v14}, Ljava/util/Collection;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z

    move-result v14

    if-nez v14, :cond_3

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z

    move-result v14

    if-eqz v14, :cond_8

    :cond_3
    const/4 v6, 0x1

    .line 487
    .local v6, "notify":Z
    :goto_2
    if-eqz v6, :cond_5

    .line 488
    const-string v14, "BooksDataStore"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 489
    const-string v15, "BooksDataStore"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "notify content changed: collections="

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", collections orphans="

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v14, v2, Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;->orphanRows:Ljava/util/Collection;

    invoke-interface {v14}, Ljava/util/Collection;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_9

    const/4 v14, 0x1

    :goto_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", states="

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ", volumes="

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncResultHasModifiedValues(Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;)Z

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v15, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mBroadcaster:Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;

    invoke-interface {v14}, Lcom/google/android/apps/books/service/BooksUserContentService$DelayedBroadcaster;->notifyContentChanged()V

    .line 499
    :cond_5
    const-string v14, "BooksDataStore"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 500
    const-string v14, "BooksDataStore"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Updated \"My Books\" for "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "; notifyContentChanged="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    :cond_6
    return v6

    .line 454
    .end local v4    # "firstFetch":Z
    .end local v6    # "notify":Z
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 481
    .restart local v4    # "firstFetch":Z
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 489
    .restart local v6    # "notify":Z
    :cond_9
    const/4 v14, 0x0

    goto :goto_3
.end method

.method public setPinned(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 202
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pinned"

    invoke-static {p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->booleanToProviderInt(Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    if-eqz p2, :cond_0

    .line 204
    const-string v1, "last_local_access"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 206
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 207
    return-void
.end method

.method public setPinnedAndOfflineLicense(Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "pinned"    # Z
    .param p3, "hasOfflineLicense"    # Z

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 213
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pinned"

    invoke-static {p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->booleanToProviderInt(Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    const-string v1, "has_offline_license"

    invoke-static {p3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->booleanToProviderInt(Z)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215
    if-eqz p2, :cond_0

    .line 216
    const-string v1, "last_local_access"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 218
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 219
    return-void
.end method

.method public setPosition(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/books/api/OceanApiaryUrls$Action;Z)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "position"    # Ljava/lang/String;
    .param p3, "lastAccess"    # J
    .param p5, "lastAction"    # Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;
    .param p6, "notifyContentChanged"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2967
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "missing/empty volumeId: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 2968
    const-wide/16 v4, 0x0

    cmp-long v2, p3, v4

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "illegal lastAccess: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 2969
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "missing/empty lastAction: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p5, v2}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2970
    invoke-virtual {p5}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->getOceanName()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lastAction is not a valid Ocean action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2973
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2975
    .local v0, "lastAccessValue":Ljava/lang/Long;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2976
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "volume_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2977
    const-string v2, "position"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2978
    const-string v2, "last_access"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2979
    const-string v2, "last_local_access"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2980
    const-string v2, "last_action"

    invoke-virtual {p5}, Lcom/google/android/apps/books/api/OceanApiaryUrls$Action;->getOceanName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2982
    invoke-direct {p0, p1, v1, v3, p6}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 2983
    return-void

    .end local v0    # "lastAccessValue":Ljava/lang/Long;
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_0
    move v2, v3

    .line 2968
    goto :goto_0
.end method

.method public setRequestedDictionaryLanguages(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 925
    .local p1, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->setRequestedDictionaryLanguages(Ljava/util/List;)V

    .line 926
    return-void
.end method

.method public setResourceResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "resourceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1335
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 1336
    .local v0, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v5, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-static {v5, p1}, Lcom/google/android/apps/books/provider/BooksContract$ResourceResources;->buildDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1337
    .local v4, "resourceResourceDirUri":Landroid/net/Uri;
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/books/model/Resource;

    .line 1338
    .local v3, "resource":Lcom/google/android/apps/books/model/Resource;
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 1340
    .local v2, "resResOper":Landroid/content/ContentProviderOperation$Builder;
    const-string v5, "account_name"

    iget-object v6, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1341
    const-string v5, "volume_id"

    invoke-virtual {v2, v5, p1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1342
    const-string v5, "compound_res_id"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1343
    const-string v5, "referenced_res_id"

    invoke-interface {v3}, Lcom/google/android/apps/books/model/Resource;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1344
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1346
    .end local v2    # "resResOper":Landroid/content/ContentProviderOperation$Builder;
    .end local v3    # "resource":Lcom/google/android/apps/books/model/Resource;
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->applyBatch(Ljava/util/ArrayList;)V

    .line 1347
    return-void
.end method

.method public setResources(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .param p1, "volumeId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1294
    .local p2, "resources":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/apps/books/model/Resource;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->addResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1295
    return-void
.end method

.method public setSegmentResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "segmentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1289
    .local p3, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Resource;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->addResources(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1290
    return-void
.end method

.method public setTextZoom(Ljava/lang/String;F)V
    .locals 5
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "textZoom"    # F

    .prologue
    const/4 v2, 0x0

    .line 3003
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "missing/empty volumeId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3004
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Illegal textZoom value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/common/base/Preconditions;->checkArgument(ZLjava/lang/Object;)V

    .line 3006
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3007
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3008
    const-string v1, "text_zoom"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3010
    const-string v1, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3011
    const-string v1, "BooksDataStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "persisting textZoom "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3014
    :cond_0
    invoke-direct {p0, p1, v0, v2, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 3015
    return-void

    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_1
    move v1, v2

    .line 3004
    goto :goto_0
.end method

.method public setUserSelectedMode(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest$Mode;)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "mode"    # Lcom/google/android/apps/books/model/VolumeManifest$Mode;

    .prologue
    const/4 v3, 0x0

    .line 260
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeValuesForVolume(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 261
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "last_mode"

    invoke-virtual {p2}, Lcom/google/android/apps/books/model/VolumeManifest$Mode;->getIntegerValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 262
    invoke-direct {p0, p1, v0, v3, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 263
    return-void
.end method

.method public setVolume(Lcom/google/android/apps/books/model/VolumeData;)V
    .locals 6
    .param p1, "volume"    # Lcom/google/android/apps/books/model/VolumeData;

    .prologue
    .line 295
    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/ContentValues;

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/apps/books/provider/DataConversions;->volumeDataToContentValues(Lcom/google/android/apps/books/model/VolumeData;)Landroid/content/ContentValues;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/google/common/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 298
    .local v2, "valuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-direct {p0, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->syncVolumeRows(Ljava/util/List;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    .line 300
    invoke-static {v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->filterToLastAccessValues(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 301
    .local v1, "statesValues":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, v3}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->buildStatesSyncher(Landroid/accounts/Account;)Lcom/google/android/apps/books/sync/TableSynchronizer;

    move-result-object v0

    .line 302
    .local v0, "statesSynchronizer":Lcom/google/android/apps/books/sync/TableSynchronizer;
    invoke-virtual {v0, v1}, Lcom/google/android/apps/books/sync/TableSynchronizer;->syncRows(Ljava/lang/Iterable;)Lcom/google/android/apps/books/sync/TableSynchronizer$SyncResult;

    .line 303
    return-void
.end method

.method public setVolumeManifest(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)V
    .locals 22
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/google/android/apps/books/model/VolumeManifest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1659
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mLogger:Lcom/google/android/apps/books/util/Logger;

    const-string v4, "save manifest"

    new-instance v21, Lcom/google/android/apps/books/model/BooksDataStoreImpl$3;

    invoke-direct/range {v21 .. v22}, Lcom/google/android/apps/books/model/BooksDataStoreImpl$3;-><init>(Lcom/google/android/apps/books/model/BooksDataStoreImpl;)V

    move-object/from16 v0, v21

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/books/util/Logging;->startTracker(Lcom/google/android/apps/books/util/Logger;Ljava/lang/String;Lcom/google/android/apps/books/util/Logging$CompletionCallback;)Lcom/google/android/apps/books/util/Logging$PerformanceTracker;

    move-result-object v19

    .line 1673
    .local v19, "tracker":Lcom/google/android/apps/books/util/Logging$PerformanceTracker;
    :try_start_0
    invoke-static {}, Lcom/google/common/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 1679
    .local v10, "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->clearVolumeColumns(Landroid/accounts/Account;Ljava/lang/String;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1680
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/books/provider/BooksContract$Volumes;->getManifestContentUris(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/net/Uri;

    .line 1681
    .local v20, "uri":Landroid/net/Uri;
    invoke-static/range {v20 .. v20}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1714
    .end local v10    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v20    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v3

    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    throw v3

    .line 1684
    .restart local v10    # "batch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v12    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->makeVolumeOperation(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1686
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getSegments()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v6

    .line 1687
    .local v6, "segments":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Segment;>;"
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getPages()Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/books/util/OrderedIdentifiableCollection;->getList()Ljava/util/List;

    move-result-object v7

    .line 1688
    .local v7, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/apps/books/model/Page;>;"
    const/4 v8, 0x0

    .line 1689
    .local v8, "chapterIndex":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/books/provider/BooksContract$Chapters;->buildChapterDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1690
    .local v9, "chapterDirUri":Landroid/net/Uri;
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/books/model/VolumeManifest;->getChapters()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/books/model/Chapter;

    .line 1691
    .local v5, "chapter":Lcom/google/android/apps/books/model/Chapter;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    add-int/lit8 v11, v8, 0x1

    .end local v8    # "chapterIndex":I
    .local v11, "chapterIndex":I
    move-object/from16 v4, p1

    invoke-static/range {v3 .. v9}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->chapterInsertionOperation(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Chapter;Ljava/util/List;Ljava/util/List;ILandroid/net/Uri;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v8, v11

    .line 1693
    .end local v11    # "chapterIndex":I
    .restart local v8    # "chapterIndex":I
    goto :goto_1

    .line 1696
    .end local v5    # "chapter":Lcom/google/android/apps/books/model/Chapter;
    :cond_1
    const/16 v17, 0x0

    .line 1697
    .local v17, "segmentIndex":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/books/provider/BooksContract$Segments;->buildSectionDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1698
    .local v16, "segmentDirUri":Landroid/net/Uri;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move/from16 v18, v17

    .end local v17    # "segmentIndex":I
    .local v18, "segmentIndex":I
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/books/model/Segment;

    .line 1699
    .local v15, "segment":Lcom/google/android/apps/books/model/Segment;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    add-int/lit8 v17, v18, 0x1

    .end local v18    # "segmentIndex":I
    .restart local v17    # "segmentIndex":I
    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v16

    invoke-static {v3, v0, v15, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->segmentInsertionOperation(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Segment;ILandroid/net/Uri;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v18, v17

    .line 1701
    .end local v17    # "segmentIndex":I
    .restart local v18    # "segmentIndex":I
    goto :goto_2

    .line 1703
    .end local v15    # "segment":Lcom/google/android/apps/books/model/Segment;
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/books/provider/BooksContract$Pages;->buildPageDirUri(Landroid/accounts/Account;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 1704
    .local v14, "pageDirUri":Landroid/net/Uri;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/books/model/Page;

    .line 1705
    .local v13, "page":Lcom/google/android/apps/books/model/Page;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p1

    invoke-static {v3, v0, v13, v14}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->pageInsertionOperation(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/apps/books/model/Page;Landroid/net/Uri;)Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1708
    .end local v13    # "page":Lcom/google/android/apps/books/model/Page;
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v10}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->addResourcesToBatch(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/List;)V

    .line 1710
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v10}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->addSegmentResourcesToBatch(Ljava/lang/String;Lcom/google/android/apps/books/model/VolumeManifest;Ljava/util/List;)V

    .line 1712
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->applyBatch(Ljava/util/ArrayList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1714
    invoke-interface/range {v19 .. v19}, Lcom/google/android/apps/books/util/Logging$PerformanceTracker;->done()J

    .line 1716
    return-void
.end method

.method public updateAccountSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/AccountSessionKeyId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2540
    .local p1, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/AccountSessionKeyId;>;"
    const-string v2, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2541
    const-string v2, "BooksDataStore"

    const-string v3, "Updating account key row"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2543
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2544
    .local v0, "values":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/books/model/AccountSessionKeyId;->setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V

    .line 2545
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 2548
    .local v1, "whereArgs":[Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/google/android/apps/books/provider/BooksContract$Accounts;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "account_name=?"

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2550
    return-void
.end method

.method public updateLastLocalAccess(Ljava/lang/String;JZ)V
    .locals 4
    .param p1, "volumeId"    # Ljava/lang/String;
    .param p2, "lastLocalAccess"    # J
    .param p4, "notifyContentChanged"    # Z

    .prologue
    .line 3035
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "missing/empty volumeId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/apps/books/util/BooksPreconditions;->checkNotEmpty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 3037
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3038
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3039
    const-string v1, "last_local_access"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3041
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p4}, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->updateLocalState(Ljava/lang/String;Landroid/content/ContentValues;ZZ)V

    .line 3042
    return-void
.end method

.method public updateUserSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/UserSessionKeyId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 940
    .local p1, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/UserSessionKeyId;>;"
    iget-object v0, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mDataControllerStore:Lcom/google/android/apps/books/model/DataControllerStore;

    invoke-interface {v0, p1}, Lcom/google/android/apps/books/model/DataControllerStore;->updateUserSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 941
    return-void
.end method

.method public updateVolumeSessionKey(Lcom/google/android/apps/books/model/LocalSessionKey;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/model/LocalSessionKey",
            "<",
            "Lcom/google/android/apps/books/model/VolumeSessionKeyId;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "key":Lcom/google/android/apps/books/model/LocalSessionKey;, "Lcom/google/android/apps/books/model/LocalSessionKey<Lcom/google/android/apps/books/model/VolumeSessionKeyId;>;"
    const/4 v4, 0x0

    .line 2608
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2609
    .local v1, "values":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->getId()Lcom/google/android/apps/books/model/SessionKeyId;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/books/model/VolumeSessionKeyId;

    invoke-virtual {v2}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->getRowId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/apps/books/provider/BooksContract$SessionKeys;->buildSessionKeyUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 2610
    .local v0, "uri":Landroid/net/Uri;
    const-string v2, "BooksDataStore"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2611
    const-string v2, "BooksDataStore"

    const-string v3, "VolumeSessionKeyId updating existing row"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2613
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/books/model/LocalSessionKey;->getKey()Lcom/google/android/apps/books/model/SessionKey;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/books/model/VolumeSessionKeyId;->setKeyContentValues(Lcom/google/android/apps/books/model/SessionKey;Landroid/content/ContentValues;)V

    .line 2614
    iget-object v2, p0, Lcom/google/android/apps/books/model/BooksDataStoreImpl;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2615
    return-void
.end method
