.class Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;
.super Ljava/lang/Object;
.source "PageView.java"

# interfaces
.implements Lcom/google/android/apps/books/util/SimpleDrawable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TransformingDrawable"
.end annotation


# instance fields
.field private final mInnerContent:Lcom/google/android/apps/books/util/SimpleDrawable;

.field private final mMatrix:Landroid/graphics/Matrix;

.field private final mSize:Landroid/graphics/Point;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/SimpleDrawable;Landroid/graphics/Point;Landroid/graphics/Matrix;)V
    .locals 0
    .param p1, "innerContent"    # Lcom/google/android/apps/books/util/SimpleDrawable;
    .param p2, "size"    # Landroid/graphics/Point;
    .param p3, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mInnerContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    .line 105
    iput-object p2, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mSize:Landroid/graphics/Point;

    .line 106
    iput-object p3, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mMatrix:Landroid/graphics/Matrix;

    .line 107
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 117
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 118
    .local v0, "checkpoint":I
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mInnerContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    invoke-interface {v1, p1}, Lcom/google/android/apps/books/util/SimpleDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 120
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 121
    return-void
.end method

.method public getSize(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 2
    .param p1, "outSize"    # Landroid/graphics/Point;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mSize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 112
    return-object p1
.end method

.method public getViewLayerType()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/books/widget/PageView$TransformingDrawable;->mInnerContent:Lcom/google/android/apps/books/util/SimpleDrawable;

    invoke-interface {v0}, Lcom/google/android/apps/books/util/SimpleDrawable;->getViewLayerType()I

    move-result v0

    return v0
.end method
