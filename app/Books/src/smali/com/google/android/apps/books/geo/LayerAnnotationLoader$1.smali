.class Lcom/google/android/apps/books/geo/LayerAnnotationLoader$1;
.super Ljava/lang/Object;
.source "LayerAnnotationLoader.java"

# interfaces
.implements Lcom/google/common/base/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/geo/LayerAnnotationLoader;-><init>(Lcom/google/android/apps/books/model/VolumeMetadata;Ljava/lang/String;Lcom/google/android/apps/books/annotations/AnnotationController;Lcom/google/android/apps/books/geo/AnnotationSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/base/Predicate",
        "<",
        "Lcom/google/android/apps/books/annotations/Annotation;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$1;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/apps/books/annotations/Annotation;)Z
    .locals 2
    .param p1, "a"    # Lcom/google/android/apps/books/annotations/Annotation;

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/google/android/apps/books/annotations/Annotation;->getLayerId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$1;->this$0:Lcom/google/android/apps/books/geo/LayerAnnotationLoader;

    # getter for: Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->mLayerId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader;->access$000(Lcom/google/android/apps/books/geo/LayerAnnotationLoader;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 66
    check-cast p1, Lcom/google/android/apps/books/annotations/Annotation;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/books/geo/LayerAnnotationLoader$1;->apply(Lcom/google/android/apps/books/annotations/Annotation;)Z

    move-result v0

    return v0
.end method
