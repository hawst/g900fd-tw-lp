.class Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;
.super Ljava/lang/Object;
.source "ResourceContentSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/ResourceContentSubcontroller;->localResourceRetriever(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/books/data/ResourceContentSubcontroller$ResourceRetriever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

.field final synthetic val$resourceId:Ljava/lang/String;

.field final synthetic val$volumeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/ResourceContentSubcontroller;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;->this$0:Lcom/google/android/apps/books/data/ResourceContentSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;->val$volumeId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;->val$resourceId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Lcom/google/android/apps/books/data/ControlTaskServices;)Ljava/util/List;
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/data/ControlTaskServices;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/books/model/Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 207
    invoke-interface {p1}, Lcom/google/android/apps/books/data/ControlTaskServices;->getDataStore()Lcom/google/android/apps/books/model/BooksDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;->val$volumeId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/books/data/ResourceContentSubcontroller$2;->val$resourceId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/books/model/BooksDataStore;->getResourceResources(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
