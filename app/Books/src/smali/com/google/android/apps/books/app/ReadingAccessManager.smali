.class public Lcom/google/android/apps/books/app/ReadingAccessManager;
.super Ljava/lang/Object;
.source "ReadingAccessManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;,
        Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;
    }
.end annotation


# instance fields
.field private final mAccessConsumer:Lcom/google/android/ublib/utils/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/ublib/utils/Consumer",
            "<",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDataController:Lcom/google/android/apps/books/data/BooksDataController;

.field private final mHandler:Landroid/os/Handler;

.field private final mReader:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

.field private final mRequestOnlyOnlineLicense:Z

.field private mRunning:Z

.field private final mVolumeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;Ljava/lang/String;Landroid/content/Context;ZLcom/google/android/apps/books/data/BooksDataController;)V
    .locals 3
    .param p1, "reader"    # Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;
    .param p2, "volumeId"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "requestOnlyOnlineLicense"    # Z
    .param p5, "dataController"    # Lcom/google/android/apps/books/data/BooksDataController;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Lcom/google/android/apps/books/app/ReadingAccessManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/app/ReadingAccessManager$1;-><init>(Lcom/google/android/apps/books/app/ReadingAccessManager;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mAccessConsumer:Lcom/google/android/ublib/utils/Consumer;

    .line 74
    const-string v0, "missing reader"

    invoke-static {p1, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mReader:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    .line 75
    const-string v0, "missing volume ID"

    invoke-static {p2, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mVolumeId:Ljava/lang/String;

    .line 76
    const-string v0, "missing Context"

    invoke-static {p3, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mContext:Landroid/content/Context;

    .line 78
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/books/app/ReadingAccessManager$HandlerCallback;-><init>(Lcom/google/android/apps/books/app/ReadingAccessManager;Lcom/google/android/apps/books/app/ReadingAccessManager$1;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mHandler:Landroid/os/Handler;

    .line 79
    iput-boolean p4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mRequestOnlyOnlineLicense:Z

    .line 80
    const-string v0, "missing data controller"

    invoke-static {p5, v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/data/BooksDataController;

    iput-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    .line 81
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/books/app/ReadingAccessManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingAccessManager;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/books/app/ReadingAccessManager;->requestAccess()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/app/ReadingAccessManager;Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/app/ReadingAccessManager;
    .param p1, "x1"    # Lcom/google/android/apps/books/util/ExceptionOr;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/app/ReadingAccessManager;->handleAccessResponse(Lcom/google/android/apps/books/util/ExceptionOr;)V

    return-void
.end method

.method private handleAccessResponse(Lcom/google/android/apps/books/util/ExceptionOr;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/books/util/ExceptionOr",
            "<",
            "Lcom/google/android/apps/books/api/data/RequestAccessResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "accessResponse":Lcom/google/android/apps/books/util/ExceptionOr;, "Lcom/google/android/apps/books/util/ExceptionOr<Lcom/google/android/apps/books/api/data/RequestAccessResponse;>;"
    const/4 v6, 0x0

    .line 143
    .local v6, "allowUnknown":Z
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->isFailure()Z

    move-result v9

    .line 144
    .local v9, "isFailure":Z
    if-nez v9, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 145
    :cond_0
    sget-object v8, Lcom/google/android/apps/books/net/DeviceAccess;->sUnknown:Lcom/google/android/apps/books/net/DeviceAccess;

    .line 146
    .local v8, "deviceAccess":Lcom/google/android/apps/books/net/DeviceAccess;
    move v6, v9

    .line 159
    :goto_0
    invoke-direct {p0, v8, v6}, Lcom/google/android/apps/books/app/ReadingAccessManager;->handleDeviceAccess(Lcom/google/android/apps/books/net/DeviceAccess;Z)V

    .line 160
    return-void

    .line 148
    .end local v8    # "deviceAccess":Lcom/google/android/apps/books/net/DeviceAccess;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/books/util/ExceptionOr;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/api/data/RequestAccessResponse;

    iget-object v7, v0, Lcom/google/android/apps/books/api/data/RequestAccessResponse;->concurrentAccess:Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;

    .line 155
    .local v7, "concurrentAccess":Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;
    iget-boolean v0, v7, Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;->restricted:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-boolean v1, v7, Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;->deviceAllowed:Z

    iget v2, v7, Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;->maxConcurrentDevices:I

    int-to-long v2, v2

    iget v4, v7, Lcom/google/android/apps/books/api/data/ConcurrentAccessRestriction;->timeWindowSeconds:I

    int-to-long v4, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/books/net/DeviceAccess;->newInstance(ZZJJ)Lcom/google/android/apps/books/net/DeviceAccess;

    move-result-object v8

    .restart local v8    # "deviceAccess":Lcom/google/android/apps/books/net/DeviceAccess;
    goto :goto_0

    .end local v8    # "deviceAccess":Lcom/google/android/apps/books/net/DeviceAccess;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private handleDeviceAccess(Lcom/google/android/apps/books/net/DeviceAccess;Z)V
    .locals 8
    .param p1, "access"    # Lcom/google/android/apps/books/net/DeviceAccess;
    .param p2, "allowUnknownAccess"    # Z

    .prologue
    .line 189
    const-string v4, "ReadingAccessManager"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 190
    const-string v4, "ReadingAccessManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got device access "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/books/net/DeviceAccess;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for object "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/books/util/NetUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mRequestOnlyOnlineLicense:Z

    if-eqz v4, :cond_2

    .line 196
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mReader:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;->offlineAccessDenied()V

    .line 218
    :cond_1
    :goto_0
    return-void

    .line 202
    :cond_2
    iget-boolean v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mRunning:Z

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/books/net/DeviceAccess;->isUnrestricted()Z

    move-result v4

    if-nez v4, :cond_1

    .line 206
    if-nez p2, :cond_3

    sget-object v4, Lcom/google/android/apps/books/net/DeviceAccess;->sUnknown:Lcom/google/android/apps/books/net/DeviceAccess;

    if-ne p1, v4, :cond_3

    .line 207
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mReader:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    invoke-interface {v4}, Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;->licenseError()V

    goto :goto_0

    .line 211
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/DeviceAccess;->isAllowed()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 212
    invoke-virtual {p1}, Lcom/google/android/apps/books/net/DeviceAccess;->getSeconds()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long v0, v4, v6

    .line 213
    .local v0, "millis":J
    const-wide/16 v4, 0x3a98

    sub-long v4, v0, v4

    const-wide/32 v6, 0xea60

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 214
    .local v2, "safeMillis":J
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 216
    .end local v0    # "millis":J
    .end local v2    # "safeMillis":J
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mReader:Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;

    invoke-virtual {p1}, Lcom/google/android/apps/books/net/DeviceAccess;->getMaxDevices()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-interface {v4, v5}, Lcom/google/android/apps/books/app/ReadingAccessManager$Reader;->accessDenied(I)V

    goto :goto_0
.end method

.method private requestAccess()V
    .locals 5

    .prologue
    .line 126
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/books/util/BooksGservicesHelper;->shouldGetAccessLock(Landroid/content/Context;)Z

    move-result v1

    .line 127
    .local v1, "shouldGetAccessLock":Z
    if-nez v1, :cond_1

    .line 128
    const-string v2, "ReadingAccessManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const-string v2, "ReadingAccessManager"

    const-string v3, "Skipping concurrent access check due to Gservices"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    sget-object v2, Lcom/google/android/apps/books/net/DeviceAccess;->sUnknown:Lcom/google/android/apps/books/net/DeviceAccess;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/app/ReadingAccessManager;->handleDeviceAccess(Lcom/google/android/apps/books/net/DeviceAccess;Z)V

    .line 138
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mRequestOnlyOnlineLicense:Z

    if-eqz v2, :cond_2

    const-string v0, "CONCURRENT"

    .line 137
    .local v0, "licenseType":Ljava/lang/String;
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mDataController:Lcom/google/android/apps/books/data/BooksDataController;

    iget-object v3, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mVolumeId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mAccessConsumer:Lcom/google/android/ublib/utils/Consumer;

    invoke-interface {v2, v3, v0, v4}, Lcom/google/android/apps/books/data/BooksDataController;->getVolumeAccess(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/ublib/utils/Consumer;)V

    goto :goto_0

    .line 135
    .end local v0    # "licenseType":Ljava/lang/String;
    :cond_2
    const-string v0, "BOTH"

    goto :goto_1
.end method


# virtual methods
.method public start()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 84
    iput-boolean v1, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mRunning:Z

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 88
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mRunning:Z

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/books/app/ReadingAccessManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 93
    return-void
.end method
