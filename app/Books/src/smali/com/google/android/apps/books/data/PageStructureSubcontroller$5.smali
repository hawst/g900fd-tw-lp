.class Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;
.super Ljava/lang/Object;
.source "PageStructureSubcontroller.java"

# interfaces
.implements Lcom/google/android/apps/books/data/ControlTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/books/data/PageStructureSubcontroller;->handleExpiredKeyOnControlThread(Lcom/google/android/apps/books/data/NetworkTaskServices;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/model/LocalSessionKey;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

.field final synthetic val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

.field final synthetic val$continuation:Ljava/lang/Runnable;

.field final synthetic val$e:Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;

.field final synthetic val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/model/LocalSessionKey;Ljava/lang/Runnable;Lcom/google/android/apps/books/sync/VolumeContentId;Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iput-object p2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    iput-object p3, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$continuation:Ljava/lang/Runnable;

    iput-object p4, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iput-object p5, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$e:Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/google/android/apps/books/data/ControlTaskServices;)V
    .locals 3
    .param p1, "services"    # Lcom/google/android/apps/books/data/ControlTaskServices;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$sessionKey:Lcom/google/android/apps/books/model/LocalSessionKey;

    invoke-interface {p1, v0}, Lcom/google/android/apps/books/data/ControlTaskServices;->removeSessionKeyAndWipeContents(Lcom/google/android/apps/books/model/LocalSessionKey;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$continuation:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$continuation:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->this$0:Lcom/google/android/apps/books/data/PageStructureSubcontroller;

    iget-object v1, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$contentId:Lcom/google/android/apps/books/sync/VolumeContentId;

    iget-object v2, p0, Lcom/google/android/apps/books/data/PageStructureSubcontroller$5;->val$e:Lcom/google/android/apps/books/util/SessionKeyFactory$SessionKeyExpiredException;

    # invokes: Lcom/google/android/apps/books/data/PageStructureSubcontroller;->publishFailure(Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/books/data/PageStructureSubcontroller;->access$100(Lcom/google/android/apps/books/data/PageStructureSubcontroller;Lcom/google/android/apps/books/sync/VolumeContentId;Ljava/lang/Exception;)V

    goto :goto_0
.end method
