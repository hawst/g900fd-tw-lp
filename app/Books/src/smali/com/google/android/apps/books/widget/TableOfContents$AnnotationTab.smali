.class public Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;
.super Ljava/lang/Object;
.source "TableOfContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/TableOfContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AnnotationTab"
.end annotation


# instance fields
.field private final mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

.field private final mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

.field private final mEmptyView:Landroid/view/View;

.field private final mListView:Landroid/widget/ListView;

.field private final mLoadingView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;Landroid/view/View;Landroid/view/View;Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;Lcom/google/android/apps/books/app/ContentsView$Callbacks;)V
    .locals 0
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "zeroAnnotationsView"    # Landroid/view/View;
    .param p3, "loadingView"    # Landroid/view/View;
    .param p4, "adapter"    # Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;
    .param p5, "callbacks"    # Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    iput-object p1, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mListView:Landroid/widget/ListView;

    .line 263
    iput-object p2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mEmptyView:Landroid/view/View;

    .line 264
    iput-object p3, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mLoadingView:Landroid/view/View;

    .line 265
    iput-object p4, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    .line 266
    iput-object p5, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    .line 267
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->adjustViewVisibilities()V

    return-void
.end method

.method private adjustViewVisibilities()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 283
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->isLoading()Z

    move-result v1

    .line 284
    .local v1, "loading":Z
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->isEmpty()Z

    move-result v0

    .line 286
    .local v0, "empty":Z
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mLoadingView:Landroid/view/View;

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->setVisibility(Landroid/view/View;Z)V

    .line 287
    iget-object v5, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mEmptyView:Landroid/view/View;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    move v2, v3

    :goto_0
    invoke-direct {p0, v5, v2}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->setVisibility(Landroid/view/View;Z)V

    .line 288
    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mListView:Landroid/widget/ListView;

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    :goto_1
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->setVisibility(Landroid/view/View;Z)V

    .line 289
    return-void

    :cond_0
    move v2, v4

    .line 287
    goto :goto_0

    :cond_1
    move v3, v4

    .line 288
    goto :goto_1
.end method

.method private setVisibility(Landroid/view/View;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "shouldBeVisible"    # Z

    .prologue
    .line 292
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 293
    return-void

    .line 292
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public setUp()V
    .locals 3

    .prologue
    .line 270
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mCallbacks:Lcom/google/android/apps/books/app/ContentsView$Callbacks;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->getOnItemClickListener(Lcom/google/android/apps/books/app/ContentsView$Callbacks;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    .line 271
    .local v0, "listener":Landroid/widget/AdapterView$OnItemClickListener;
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 272
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 273
    invoke-direct {p0}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->adjustViewVisibilities()V

    .line 274
    iget-object v1, p0, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;->mAdapter:Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;

    new-instance v2, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab$1;-><init>(Lcom/google/android/apps/books/widget/TableOfContents$AnnotationTab;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/books/widget/SortedUserAnnotationAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 280
    return-void
.end method
