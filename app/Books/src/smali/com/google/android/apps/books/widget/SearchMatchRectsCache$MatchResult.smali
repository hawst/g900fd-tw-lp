.class public final enum Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
.super Ljava/lang/Enum;
.source "SearchMatchRectsCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/widget/SearchMatchRectsCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MatchResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

.field public static final enum HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

.field public static final enum NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

.field public static final enum NO_RESULT_YET:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    const-string v1, "NO_MATCH"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    new-instance v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    const-string v1, "NO_RESULT_YET"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_RESULT_YET:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    new-instance v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    const-string v1, "HAS_MATCH"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->NO_RESULT_YET:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->HAS_MATCH:Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->$VALUES:[Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->$VALUES:[Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    invoke-virtual {v0}, [Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/books/widget/SearchMatchRectsCache$MatchResult;

    return-object v0
.end method
