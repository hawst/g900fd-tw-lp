.class Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;
.super Landroid/animation/AnimatorListenerAdapter;
.source "UIThreadTaskManager.java"

# interfaces
.implements Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/util/UIThreadTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimatorStateBusyProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/books/util/UIThreadTaskManager;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 313
    invoke-virtual {p1, p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->weaklyAddBusyProvider(Lcom/google/android/apps/books/util/UIThreadTaskManager$BusyProvider;)V

    .line 314
    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # getter for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatingAnimators:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$1100(Lcom/google/android/apps/books/util/UIThreadTaskManager;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # getter for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatingAnimators:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$1100(Lcom/google/android/apps/books/util/UIThreadTaskManager;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 324
    invoke-virtual {p0}, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    invoke-virtual {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->onBusyEnded()V

    .line 327
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/apps/books/util/UIThreadTaskManager$AnimatorStateBusyProvider;->this$0:Lcom/google/android/apps/books/util/UIThreadTaskManager;

    # getter for: Lcom/google/android/apps/books/util/UIThreadTaskManager;->mAnimatingAnimators:Ljava/util/Set;
    invoke-static {v0}, Lcom/google/android/apps/books/util/UIThreadTaskManager;->access$1100(Lcom/google/android/apps/books/util/UIThreadTaskManager;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 319
    return-void
.end method
