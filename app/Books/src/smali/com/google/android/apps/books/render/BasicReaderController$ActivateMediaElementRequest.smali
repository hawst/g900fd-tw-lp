.class Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;
.super Lcom/google/android/apps/books/render/BasicReaderController$Request;
.source "BasicReaderController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/books/render/BasicReaderController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivateMediaElementRequest"
.end annotation


# instance fields
.field private final mElementId:Ljava/lang/String;

.field private final mPageOffset:I

.field private final mPassageIndex:I

.field final synthetic this$0:Lcom/google/android/apps/books/render/BasicReaderController;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/books/render/BasicReaderController;IILjava/lang/String;I)V
    .locals 6
    .param p2, "passageIndex"    # I
    .param p3, "pageOffset"    # I
    .param p4, "elementId"    # Ljava/lang/String;
    .param p5, "taskRequestId"    # I

    .prologue
    .line 1244
    iput-object p1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    .line 1245
    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object v0, p0

    move-object v1, p1

    move v2, p5

    move v3, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/books/render/BasicReaderController$Request;-><init>(Lcom/google/android/apps/books/render/BasicReaderController;IIZI)V

    .line 1247
    iput p2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPassageIndex:I

    .line 1248
    iput p3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPageOffset:I

    .line 1249
    iput-object p4, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mElementId:Ljava/lang/String;

    .line 1250
    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    .prologue
    .line 1232
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPassageIndex:I

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    .prologue
    .line 1232
    iget v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPageOffset:I

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;

    .prologue
    .line 1232
    iget-object v0, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mElementId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1254
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderIsBusy:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/books/render/BasicReaderController;->access$102(Lcom/google/android/apps/books/render/BasicReaderController;Z)Z

    .line 1255
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReaderDataModel:Lcom/google/android/apps/books/render/ReaderDataModel;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$200(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/ReaderDataModel;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPassageIndex:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/books/render/ReaderDataModel;->isPassageReady(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1256
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPassageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPageOffset:I

    iget-object v4, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mElementId:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mTaskRequestId:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/android/apps/books/render/TextReader;->activateMediaElement(IILjava/lang/String;I)V

    .line 1272
    :goto_0
    return-void

    .line 1259
    :cond_0
    new-instance v0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest$1;-><init>(Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;)V

    .line 1268
    .local v0, "handler":Lcom/google/android/apps/books/render/BasicReaderController$OnPageLoadedHandler;
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mOnPageLoadedHandlers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$700(Lcom/google/android/apps/books/render/BasicReaderController;)Ljava/util/Map;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mTaskRequestId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1270
    iget-object v1, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->this$0:Lcom/google/android/apps/books/render/BasicReaderController;

    # getter for: Lcom/google/android/apps/books/render/BasicReaderController;->mReader:Lcom/google/android/apps/books/render/TextReader;
    invoke-static {v1}, Lcom/google/android/apps/books/render/BasicReaderController;->access$900(Lcom/google/android/apps/books/render/BasicReaderController;)Lcom/google/android/apps/books/render/TextReader;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mPassageIndex:I

    iget v3, p0, Lcom/google/android/apps/books/render/BasicReaderController$ActivateMediaElementRequest;->mTaskRequestId:I

    invoke-interface {v1, v2, v4, v4, v3}, Lcom/google/android/apps/books/render/TextReader;->loadPage(IIII)V

    goto :goto_0
.end method
