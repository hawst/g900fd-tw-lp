.class public Lcom/google/android/apps/books/util/MultiTouchGestureDetector;
.super Landroid/view/ScaleGestureDetector;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;,
        Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;
    }
.end annotation


# instance fields
.field private mAngle:F

.field private mCenterX:F

.field private mCenterY:F

.field private mPriorAngle:F

.field private mPriorCenterX:F

.field private mPriorCenterY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;

    invoke-direct {v0, p2}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector$WrapGestureListener;-><init>(Lcom/google/android/apps/books/util/MultiTouchGestureDetector$OnMultiTouchGestureListener;)V

    invoke-direct {p0, p1, v0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    .line 57
    return-void
.end method

.method private static getEventAngle(Landroid/view/MotionEvent;)F
    .locals 6
    .param p0, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 128
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 129
    const/4 v2, 0x0

    .line 135
    :goto_0
    return v2

    .line 132
    :cond_0
    invoke-virtual {p0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p0, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    .line 133
    .local v0, "dx":F
    invoke-virtual {p0, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    .line 135
    .local v1, "dy":F
    float-to-double v2, v1

    float-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v2, v2

    goto :goto_0
.end method

.method private getEventCenterX(Landroid/view/MotionEvent;)F
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 79
    const/4 v2, 0x0

    .line 80
    .local v2, "totalX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 81
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 82
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float/2addr v2, v3

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_0
    int-to-float v3, v0

    div-float v3, v2, v3

    return v3
.end method

.method private getEventCenterY(Landroid/view/MotionEvent;)F
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 88
    const/4 v2, 0x0

    .line 89
    .local v2, "totalY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 90
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 91
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float/2addr v2, v3

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    :cond_0
    int-to-float v3, v0

    div-float v3, v2, v3

    return v3
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mCenterX:F

    iput v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mPriorCenterX:F

    .line 67
    iget v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mCenterY:F

    iput v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mPriorCenterY:F

    .line 68
    iget v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mAngle:F

    iput v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mPriorAngle:F

    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->getEventCenterX(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mCenterX:F

    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->getEventCenterY(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mCenterY:F

    .line 72
    invoke-static {p1}, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->getEventAngle(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/books/util/MultiTouchGestureDetector;->mAngle:F

    .line 74
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
